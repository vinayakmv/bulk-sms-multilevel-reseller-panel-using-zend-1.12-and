#!/usr/bin/php
<?php
require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");
$today = date("d-m-y");

while (true) {
  $cdateTime =date("Y-m-d H:i:s");
  $totalProcess = 100;
  $freeProcess = 0;
  $runningProcess = shell_exec("ps -ef | grep cronJobNumberFinal.php | grep -v grep | wc -l");
  $freeProcess = $totalProcess - $runningProcess;

  $select_distinct_campaign = "SELECT DISTINCT `user_id` FROM `campaigns_sms` WHERE `status` ='0' AND `start_date`<='$cdateTime'";
  $select_distinct_campaign_result = $smsDb->rawQuery($select_distinct_campaign);
  $distinct_count = $smsDb->count;
  if ($freeProcess) {
      if ($freeProcess < $distinct_count) {
		  $output = $select_distinct_campaign_result;
		  for ($x = $freeProcess; $x >= 0; $x--) {
			  $userId = $output[$x];
			  $userId = $userId['user_id'];
			  
			  
			  $smsDb->where ('status', 0);
			  $smsDb->where ('user_id', $userId);
			  $row = $smsDb->getOne('campaigns_sms');
			  $totalLeads = $smsDb->count;

			 // echo "SINGLE - campaigID = $userId = $totalLeads\sn";
			  $id = $row['id'];
			  $data = Array ('status' => '2');
			  $smsDb->where('id', $id);
			  $smsDb->where ('start_date', $cdateTime, "<=");
			  $smsDb->update ('campaigns_sms', $data);

			  if ($totalLeads > 0) {
			  		shell_exec('/usr/bin/php /var/www/html/webpanel/cronjob/sms/cronJobNumberFinal.php '.$id.' 2>> /var/log/webpanel/smscron-final-error-'.$today.'.log >> /var/log/webpanel/smscron-final-success-'.$today.'.log  &');
			  }
			}
    	}else {

	      if ($distinct_count > 0) {
			  $select_per_campaign = ceil($freeProcess/$distinct_count);
			  foreach ($select_distinct_campaign_result as $distinct_row) {
			      $userId = $distinct_row['user_id'];
			     
			      $smsDb->where ('status', 0);
			      $smsDb->where ('user_id', $userId);
			      $smsDb->where ('start_date', $cdateTime, "<=");
			      $results = $smsDb->get('campaigns_sms', Array(0, $select_per_campaign));
			      $totalLeads = $smsDb->count;

			     // echo "FULL - campaigID = $userId = $totalLeads\n";
			      if ($totalLeads > 0) {
					  foreach ($results as $row) {
					  	  $id = $row['id'];
					      $data = Array ('status' => '2');
					  	  $smsDb->where('id', $id);
					  	  $smsDb->update ('campaigns_sms', $data);
					  	  
							shell_exec('/usr/bin/php /var/www/html/webpanel/cronjob/sms/cronJobNumberFinal.php '.$id.' 2>> /var/log/webpanel/smscron-final-error-'.$today.'.log >> /var/log/webpanel/smscron-final-success-'.$today.'.log  &');
					  }
		           }
		   		}
			}
   		}
   	}
		
}
