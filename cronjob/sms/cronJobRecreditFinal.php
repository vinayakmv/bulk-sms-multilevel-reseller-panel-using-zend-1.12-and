#!/usr/bin/php
<?php
//sleep(5);
require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");


if (isset($argv[1], $argv[2])){

	$parentDomainId = $argv[1];
	$userId = $argv[2];
	
	$userDb->where ("id", "$userId");
	$userDb->where ("parent_domain_id", "$parentDomainId");
	$user = $userDb->getOne ("users");
	if ($userDb->count){
		$userDb->where ("id", "$parentDomainId");
		$domain = $userDb->getOne ("domains");
		$owner = $domain['owner_id'];
		
		
        $recreditDay = date('Y-m-d', strtotime('- 3 days'));
        $startBefore3Days = date("Y-m-d 00:00:00", strtotime('- 3 days'));
        $startBefore3DaysTimestamp = strtotime($startBefore3Days);

        $endBefore3Days = date("Y-m-d 23:59:59", strtotime('- 3 days'));
        $endBefore3DaysTimestamp = strtotime($endBefore3Days);
        
        $dlrUrl = $userId.'@%';
        $delivered = '%3ADELIVRD+err%';
        $nocredit = 'err%3ABBBB';
        $fake = 'FROUTE';
        
        
/* 
 * TRANSACTIONAL RECREDIT COUNT
 */       
        $select_distinct_account = "SELECT DISTINCT `account` FROM `sent_sms_trans_mt` WHERE `dlr_url` like '$dlrUrl' AND `time` >= '$startBefore3DaysTimestamp' AND `time` <= '$endBefore3DaysTimestamp'";
        $select_distinct_account_result = $smsDb->rawQuery($select_distinct_account);
        $distinct_account_count = $smsDb->count;
         
        foreach ($select_distinct_account_result as $distinctAccountRow){
        
        	$account = $distinctAccountRow['account'];
        	if ($account == '2'){
        		$smsType = 'SMS TRANS API';
        	}else{
        		$smsType = 'SMS TRANS';
        	}
	        $select_distinct_binfo = "SELECT DISTINCT `binfo` FROM `sent_sms_trans_mt` WHERE `account` = '$account' AND `dlr_url` like '$dlrUrl' AND `time` >= '$startBefore3DaysTimestamp' AND `time` <= '$endBefore3DaysTimestamp'";
	        $select_distinct_binfo_result = $smsDb->rawQuery($select_distinct_binfo);
	        $distinct_count = $smsDb->count;
	        
	        foreach ($select_distinct_binfo_result as $distinctBinfoRow){
		        	
		        $binfo = $distinctBinfoRow['binfo'];
		        
		        //Fake SMS
		        $smsDb->where ("dlr_url", "$dlrUrl", 'like');
		        $smsDb->where ("smsc_id", "$fake", 'like');
		        $smsDb->where ("time", "$startBefore3DaysTimestamp", '>=');
		        $smsDb->where ("time", "$endBefore3DaysTimestamp", '<=');
		        $smsDb->where ('binfo', "$binfo");
		        $smsDb->where ('account', "$account");
		        $transFake = $smsDb->getOne("sent_sms_trans_mt", 'count(*) as fake');
		        $smsTransFake = $transFake['fake'] * $binfo;
       
        		//Failed SMS
        		$smsDb->where ("dlr_url", "$dlrUrl", 'like');
        		$smsDb->where ("dlrdata", "$delivered", 'not like');
        		$smsDb->where ("dlrdata", "$nocredit", 'not like');
        		$smsDb->where ("smsc_id", "$fake", 'not like');
        		$smsDb->where ("time", "$startBefore3DaysTimestamp", '>=');
        		$smsDb->where ("time", "$endBefore3DaysTimestamp", '<=');
        		$smsDb->where ('status', '1');
        		$smsDb->where ('binfo', "$binfo");
        		$smsDb->where ('account', "$account");
        		$transFailed = $smsDb->getOne("sent_sms_trans_mt", 'count(*) as failed');
        		$smsTransFailed = $transFailed['failed'] * $binfo;
        		 
        		//Awaiting DLR
        		$smsDb->where ("dlr_url", "$dlrUrl", 'like');
        		$smsDb->where ("smsc_id", "$fake", 'not like');
        		$smsDb->where ("time", "$startBefore3DaysTimestamp", '>=');
        		$smsDb->where ("time", "$endBefore3DaysTimestamp", '<=');
        		$smsDb->where ('status', '0');
        		$smsDb->where ('binfo', "$binfo");
        		$smsDb->where ('account', "$account");
        		$transAwaitingDlr = $smsDb->getOne("sent_sms_trans_mt", 'count(*) as awaiting');
        		$smsTransAwaiting = $transAwaitingDlr['awaiting'] * $binfo;
        		 
        		//Delivered
        		$smsDb->where ("dlr_url", "$dlrUrl", 'like');
        		$smsDb->where ("dlrdata", "$delivered", 'like');
        		$smsDb->where ("smsc_id", "$fake", 'not like');
        		$smsDb->where ("time", "$startBefore3DaysTimestamp", '>=');
        		$smsDb->where ("time", "$endBefore3DaysTimestamp", '<=');
        		$smsDb->where ('status', '1');
        		$smsDb->where ('binfo', "$binfo");
        		$smsDb->where ('account', "$account");
        		$transDelivered = $smsDb->getOne("sent_sms_trans_mt", 'count(*) as delivered');
        		$smsTransDelivered = $transDelivered['delivered'] * $binfo;
        		 
        		$data = Array (	"user_id" => "$owner",
        				"client_id" => "$userId",
        				"type" => "$smsType",
        				"status" => '0',
        				"sms_count" => "$binfo",
        				"fake" => "$smsTransFake",
        				"failed" => "$smsTransFailed",
        				"awaiting_dlr" => "$smsTransAwaiting",
        				"delivered" => "$smsTransDelivered",
        				"total_recredit" => $smsTransFailed+$smsTransAwaiting,
        				"recredit_date" => "$recreditDay",
        				"created_date" => $smsDb->now(),
        				"updated_date" => $smsDb->now()
        		);
        		$smsDb->insert ('sms_recredit', $data);
        	}       	
        	
        }
        
 /*
  * PROMOTIONAL RECREDIT COUNT
  */       

        
        $select_distinct_account = "SELECT DISTINCT `account` FROM `sent_sms_promo_mt` WHERE `dlr_url` like '$dlrUrl' AND `time` >= '$startBefore3DaysTimestamp' AND `time` <= '$endBefore3DaysTimestamp'";
        $select_distinct_account_result = $smsDb->rawQuery($select_distinct_account);
        $distinct_account_count = $smsDb->count;
        	 
	    foreach ($select_distinct_account_result as $distinctAccountRow){
	    	
	        $account = $distinctAccountRow['account'];
	        if ($account == '2'){
	        	$smsType = 'SMS PROMO API';
	        }else{
	        	$smsType = 'SMS PROMO';
	        }
	        	
	        $select_distinct_binfo = "SELECT DISTINCT `binfo` FROM `sent_sms_promo_mt` WHERE `account` = '$account' AND `dlr_url` like '$dlrUrl' AND `time` >= '$startBefore3DaysTimestamp' AND `time` <= '$endBefore3DaysTimestamp'";
	        $select_distinct_binfo_result = $smsDb->rawQuery($select_distinct_binfo);
	        $distinct_count = $smsDb->count;
        		
        	foreach ($select_distinct_binfo_result as $distinctBinfoRow){
        		
        		$binfo = $distinctBinfoRow['binfo'];
        		
        		//Fake SMS
        		$smsDb->where ("dlr_url", "$dlrUrl", 'like');
        		$smsDb->where ("smsc_id", "$fake", 'like');
        		$smsDb->where ("time", "$startBefore3DaysTimestamp", '>=');
        		$smsDb->where ("time", "$endBefore3DaysTimestamp", '<=');
        		$smsDb->where ('binfo', "$binfo");
        		$smsDb->where ('account', "$account");
        		$promoFake = $smsDb->getOne("sent_sms_promo_mt", 'count(*) as fake');
        		$smsPromoFake = $promoFake['fake'] * $binfo;
        		
        		//Failed SMS
        		$smsDb->where ("dlr_url", "$dlrUrl", 'like');
        		$smsDb->where ("dlrdata", "$delivered", 'not like');
        		$smsDb->where ("dlrdata", "$nocredit", 'not like');
        		$smsDb->where ("time", "$startBefore3DaysTimestamp", '>=');
        		$smsDb->where ("time", "$endBefore3DaysTimestamp", '<=');
        		$smsDb->where ('status', '1');
        		$smsDb->where ('binfo', "$binfo");
        		$smsDb->where ('account', "$account");
        		$promoFailed = $smsDb->getOne("sent_sms_promo_mt", 'count(*) as failed');
        		$smsPromoFailed = $promoFailed['failed'] * $binfo;
        		 
        		//Awaiting DLR
        		$smsDb->where ("dlr_url", "$dlrUrl", 'like');
        		$smsDb->where ("time", "$startBefore3DaysTimestamp", '>=');
        		$smsDb->where ("time", "$endBefore3DaysTimestamp", '<=');
        		$smsDb->where ('status', '0');
        		$smsDb->where ('binfo', "$binfo");
        		$smsDb->where ('account', "$account");
        		$promoAwaitingDlr = $smsDb->getOne("sent_sms_promo_mt", 'count(*) as awaiting');
        		$smsPromoAwaiting = $promoAwaitingDlr['awaiting'] * $binfo;
        		 
        		//Delivered
        		$smsDb->where ("dlr_url", "$dlrUrl", 'like');
        		$smsDb->where ("dlrdata", "$delivered", 'like');
        		$smsDb->where ("time", "$startBefore3DaysTimestamp", '>=');
        		$smsDb->where ("time", "$endBefore3DaysTimestamp", '<=');
        		$smsDb->where ('status', '1');
        		$smsDb->where ('binfo', "$binfo");
        		$smsDb->where ('account', "$account");
        		$promoDelivered = $smsDb->getOne("sent_sms_promo_mt", 'count(*) as delivered');
        		$smsPromoDelivered = $promoDelivered['delivered'] * $binfo;
        		 
        		$data = Array (	"user_id" => "$owner",
        				"client_id" => "$userId",
        				"type" => "$smsType",
        				"status" => '0',
        				"sms_count" => "$binfo",
        				"fake" => "$smsPromoFake",
        				"failed" => "$smsPromoFailed",
        				"awaiting_dlr" => "$smsPromoAwaiting",
        				"delivered" => "$smsPromoDelivered",
        				"total_recredit" => $smsPromoFailed+$smsPromoAwaiting,
        				"recredit_date" => "$recreditDay",
        				"created_date" => $smsDb->now(),
        				"updated_date" => $smsDb->now()
        		);
        		$smsDb->insert ('sms_recredit', $data);
        	}
        	
        	
        }        
        
	}
	
}
?>
