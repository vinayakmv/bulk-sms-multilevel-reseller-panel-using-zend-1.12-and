<?php 
require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';

date_default_timezone_set("Asia/Kolkata");
$cdateTime =date("Y-m-d H:i:s");
if (isset($_POST['from'], $_POST['to'], $_POST['type']) && isset($_POST['userid'])){

  $from = $_POST['from']; 
  $to = $_POST['to'];
  $ubid = $_POST['userid'];// user Id
  $type = $_POST['type'];
  
  $endDate = strtotime($to . "+1 days midnight");
  $startDate = strtotime($from . "midnight");
  
  $startDateD=date('Y-m-d H:i:s',$startDate);
  $endDateD=date('Y-m-d H:i:s',$endDate);
  
  
  $diffDate = date_diff(date_create($startDateD),date_create($endDateD));
  
  $month = $diffDate->m;
  $day = $diffDate->d;
  
  if (($month == '1' && $day == '0') || ($month == '0' && $day <= '31')){
  
  $i=1;
  $j=1;
  //-------------------------------SET ERROR CODE IN ARRAY START--------------------------------\\
    $errorCode=array();//error code set as null
    $error_res = "SELECT *  FROM dlr_status where status='1'";
    $error = $userDb->rawQuery($error_res);

    foreach($error as $reportResult) {
       //$errorCode[$reportResult['status_dlr']]=$reportResult['error_code'];
       $errorCode[$reportResult['error_code']]=$reportResult['status_dlr'];
    }
  //-------------------------------SET ERROR CODE IN ARRAY END--------------------------------\\
    $csvName=strtotime("now").$ubid.".csv";
    $leadPath = '/var/www/html/webpanel/files/sms/report/';

        $file = $csvName;
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$file}");
        header("Expires: 0");
        header("Pragma: public");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        
        
        $fp = @fopen( 'php://output', 'w' );
        fputcsv($fp, array('#','Sender','Destination','Message','Status'), ',', '"');
  //-------------------------------Values fetch ina variable $DetailsFetch['']-------------------------------\\
      if (($type == 'TRANSACTIONAL') || ($type == 'PREMIUM')) {
      	$sentSmsMt = 'sent_sms_trans_mt';
      	$sentSmsDlr = 'sent_sms_trans_dlr';
      }elseif ($type == 'INTERNATIONAL') {
      	$sentSmsMt = 'sent_sms_inter_mt';
      	$sentSmsDlr = 'sent_sms_inter_dlr';
      }else{
      	$sentSmsMt = 'sent_sms_promo_mt';
      	$sentSmsDlr = 'sent_sms_promo_dlr';
      }

    	$dlr_url=$ubid.'@';// create Dlr URL first part
	  	$SuccessDLR_res = "SELECT `sender`,`receiver`,`msgdata`,`dlr_url`,`dlrdata`,`status`,`time` FROM $sentSmsMt WHERE `dlr_url` LIKE '$dlr_url%' AND time > '$startDate' AND time < '$endDate'";
	  	$SuccessDLR = $smsDb->rawQuery($SuccessDLR_res);
    	foreach($SuccessDLR as $SuccessDLRfetch){	
			$time = $SuccessDLRfetch['time'];
        	$delTime=date('d/m/Y H:i:s', $time);
        	$fetch = $SuccessDLRfetch['dlrdata'];
        	$statusMt = $SuccessDLRfetch['status'];
    			foreach ( $errorCode as  $key => $keyword ){
    	          	if (strpos($fetch, $key)!== FALSE ){ 
    	               	$status = $keyword;
    	               	break;
    		        }else{
    		           	$status = 'Unknown';
    		        }
    	       	}
    	        $message='';
                if(isset($status) && !empty($status)){
                 // continue;
                }else{
                  $status = 'Unknown';
                }
                if ($statusMt == '0'){
                	$status = 'Awaiting DLR';
                }
                $row[] = $i++;
                $row[] = $SuccessDLRfetch['sender'];
                $row[] = $SuccessDLRfetch['receiver']; 
                $row[] = urldecode($SuccessDLRfetch['msgdata']); 
                //$row[] = $messageMain;
                $row[] = $status;
                //$row[] = $delTime;
                //$row[] = $dlrTime1;
                //$row[] = $creditCount;
                //$row[] = $smsCount;
                
                fputcsv($fp, $row, ',', '"'); 
                $row = array(); // We must clear the previous values
          }


          	fclose($fp);
  }else{
  	$csvName=strtotime("now").$ubid.".csv";
  	$leadPath = '/var/www/html/webpanel/files/sms/report/';
  	
  	$file = $csvName;
  	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  	header('Content-Description: File Transfer');
  	header("Content-type: text/csv");
  	header("Content-Disposition: attachment; filename={$file}");
  	header("Expires: 0");
  	header("Pragma: public");
  	echo "\xEF\xBB\xBF"; // UTF-8 BOM
  	$fp = @fopen( 'php://output', 'w' );
  	fputcsv($fp, array('Date range should not exceed 31 days'));
  	fclose($fp);
  }
}
