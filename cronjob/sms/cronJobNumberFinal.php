#!/usr/bin/php
<?php 
//sleep(5);
require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");
$today = date("d-m-y");

function generateUuid()
	{
		mt_srand((double)microtime()*10000);
		$charid = strtolower(md5(uniqid(rand(), true)));
		$salt =  substr($charid, 0, 2).substr($charid, 4, 2).substr($charid,9, 2).substr($charid,12, 2);
		return $salt;
	}
function checkArray($rand, $array, $max){

	if (in_array($rand, $array)){
		$rand = rand(1, $max);
		$rand = checkArray($rand, $array, $max);
	}
	return $rand;

}
function checkArrayMain($rand, $array, $arrayMain, $max){

	if ((in_array($rand, $array)) || (in_array($rand, $arrayMain))){
		$rand = rand(1, $max);
		$rand = checkArrayMain($rand, $array, $arrayMain, $max);
	}
	return $rand;

}
if (isset($argv[1])) {
	
  $batchCount = 100;
  $cdateTime =date("Y-m-d H:i:s");
  $cdte1 = strtotime($cdateTime);
  $id = $argv[1];
  
  $smsDb->where ('status', 2);
  $smsDb->where ('id', $id);
  $row = $smsDb->getOne('campaigns_sms');
  $smsDb->count;
  if ($smsDb->count >0) {
    //*********** find the csv name ***************
    /* $smsDb->where ('smscampaign_id', $id);
    $row1 = $smsDb->getOne('campaigns_sms_leads');
    $lead = $row1['lead_id'].'.csv'; */
  	shell_exec('/bin/echo $(date) -- START CAMPAIGN-'.$id.' >> /var/log/webpanel/smscron-final-success-'.$today.'.log');
  	 
  	$lead = $row['leads_file'];
    $sender = $row['sender_id'];
    //****** $sender is name of sendrId ********************END
    //*********** find the route or smsc_id ***************
    $router = $row['route']; 
    $method = $row['method'];
    $smsPriority = $row['sms_priority'];
    $makeCount = $row['fake_count'];
    $smsCount = $row['sms_count'];
    $leadCount = $row['count'];
    $opCount = $leadCount - $makeCount;
    $type = $row['type'];
    $input = $row['input'];

    $name = $row['name'];
    $urlStatus = $row['url_status'];
    $tinyUrl = $row['tiny_url'];
    
    $make = ceil($makeCount*90/100);
    
    
    $cdte = date('Y-m-d H:i:s');
    $userId = $row['user_id'];
    
    $text = html_entity_decode($row['text']);      
    $campId = $row['id']; 
    $encode = $row['encoding'];
	if ($type == 'PROMOTIONAL'){
		$validity = null;
	}else{
		$validity = ($row['validity'] ? $row['validity'] : null);
	}	
	$flash = ($row['flash'] ? '0' : null);
    if ($encode == 'GSM_7BIT'){
      $text =urlencode($text);
      $ecoding = '0';
    }elseif($encode == 'UTF16') {
      $text = mb_convert_encoding($row['text'], "UTF-16", "auto");
      $unpack = unpack('H*', $text);
      $text = array_shift($unpack);
      $ecoding = '2';
    }else {
      $text =urlencode($text);
      $ecoding = '0';
    }
	
    $userDb->where ('id', $userId);
    $rowUser = $userDb->getOne('users');
    $parentDomainId = $rowUser['parent_domain_id'];
     
    $userDb->where ('id', $parentDomainId);
    $rowDomainParent = $userDb->getOne('domains');
    
    if ($type == 'TRANSACTIONAL') {
    	$domainPercentageMain = $rowDomainParent['sms_percentage_trans_main'];
    }elseif ($type == 'PREMIUM') {
    	$domainPercentageMain = $rowDomainParent['sms_percentage_premi_main'];
    }elseif ($type == 'TRANS-SCRUB') {
    	$domainPercentageMain = $rowDomainParent['sms_percentage_scrub_main'];
    }elseif ($type == 'PROMOTIONAL') {
    	$domainPercentageMain = $rowDomainParent['sms_percentage_promo_main'];
    }elseif ($type == 'INTERNATIONAL') {
    	$domainPercentageMain = $rowDomainParent['sms_percentage_inter_main'];
    }
    
    if ($makeCount > 0){
    	$makeCountMain = ceil($opCount*$domainPercentageMain/100);
    	$makeMain = ceil($makeCountMain*90/100);
    }else{
    	$makeCountMain = 0;
    	$makeMain = ceil($makeCountMain*90/100);
    }
    

    if (($handle = fopen('/var/www/html/webpanel/contactFiles/'.$lead, "r")) !== FALSE) {

      $fp = file('/var/www/html/webpanel/contactFiles/'.$lead);
      $count = count($fp);
      $inc=0;
//Fake generation logic      
      $counter=1;
      $max=$count;
      $randArray =array();
      while ($counter <= $makeCount){
      	$rand = rand(1, $max);
      	$rand = checkArray($rand, $randArray, $max);
      	$randArray[] = $rand;
      	$counter++;
      }
      $key = 1;
//Fake End

//Fake generation logic
      $counterMain=1;
      $maxMain=$count;
      $randArrayMain =array();
      while ($counterMain <= $makeCountMain){
      	$randMain = rand(1, $maxMain);
      	$randMain = checkArrayMain($randMain, $randArray, $randArrayMain, $maxMain);
      	$randArrayMain[] = $randMain;
      	$counterMain++;
      }
      $keyMain = 1;
//Fake End
 
      $sendCounter = '0';
      $sendData = array();
      $makeKey = 1;
      $makeMainKey = 1;
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {


				if ($urlStatus == '1'){
					$uuid = generateUuid();
					$text = urldecode($text);
					$textUrl = str_replace('http://mylnk.info/xxxxxxxxxx', "http://mylnk.info/?$uuid", "$text");
					$textUrl = urlencode($textUrl);
				}elseif($urlStatus == '2'){
					$uuid = generateUuid();
					$text = urldecode($text);
					$textUrl = str_replace('http://mylnk.info/xxxxxxxxxx', "http://mylnk.info/?$uuid", "$text");
					$textUrl = urlencode($textUrl);
				}elseif($urlStatus == '3'){
					$uuid = generateUuid();
					$text = urldecode($text);
					$textUrl = str_replace('http://mylnk.info/xxxxxxxxxx', "http://mylnk.info/?$uuid", "$text");
					$textUrl = urlencode($textUrl);
				}


				if ($input == 'DYNAMIC-COMPOSE' || $input == 'DYNAMIC-SCHEDULED'){
					$text = $data['1'];
					$smsCount = $data['2'];
					if($encode == 'UTF16') {
						$text = mb_convert_encoding($data['1'], "UTF-16", "auto");
						$unpack = unpack('H*', $text);
						$text = array_shift($unpack);
						$ecoding = '2';
					}else {
						$text =urlencode($text);
						$ecoding = '0';
					}
				}

				if ($urlStatus != '0'){
      				$textMsg = $textUrl;
      			}else{
      				$textMsg = $text;
      			}
		
		      	$userDb->where ('id', $userId);
		      	$rowUser = $userDb->getOne('users');
		      	
		      	$userStatus = $rowUser['status'];
		      	$domainId = $rowUser['domain_id'];
		      	$parentDomainId = $rowUser['parent_domain_id'];
		      	
		      	$userDb->where ('id', $parentDomainId);
		      	$rowDomain = $userDb->getOne('domains');
		      	$domainStatus = $rowDomain['status'];
		      	
		      	
		      	if ($type == 'TRANSACTIONAL') {
		      		if ($method == '2'){
						$credit = $rowDomain['sms_trans_credit_api'];
					}else{
						$credit = $rowDomain['sms_trans_credit'];
					}
		      		$sendSms = 'send_sms_trans';
		      		$sentSmsMt = 'sent_sms_trans_mt';
      				$sentSmsDlr = 'sent_sms_trans_dlr';
		      	}elseif ($type == 'PREMIUM') {
		      		if ($method == '2'){
						$credit = $rowDomain['sms_premi_credit_api'];
					}else{
						$credit = $rowDomain['sms_premi_credit'];
					}
		      		$sendSms = 'send_sms_trans';
		      		$sentSmsMt = 'sent_sms_trans_mt';
      				$sentSmsDlr = 'sent_sms_trans_dlr';
		      	}elseif ($type == 'TRANS-SCRUB') {
		      		if ($method == '2'){
						$credit = $rowDomain['sms_scrub_credit_api'];
					}else{
						$credit = $rowDomain['sms_scrub_credit'];
					}
		      		$sendSms = 'send_sms_promo';
		      		$sentSmsMt = 'sent_sms_promo_mt';
      				$sentSmsDlr = 'sent_sms_promo_dlr';
		      	}elseif ($type == 'PROMOTIONAL') {
		      		if ($method == '2'){
		      			$credit = $rowDomain['sms_promo_credit_api'];
		      		}else{
		      			$credit = $rowDomain['sms_promo_credit'];
		      		}
		      		$sendSms = 'send_sms_promo';
		      		$sentSmsMt = 'sent_sms_promo_mt';
      				$sentSmsDlr = 'sent_sms_promo_dlr';
		      	}elseif ($type == 'INTERNATIONAL') {
		      		if ($method == '2'){
		      			$credit = $rowDomain['sms_inter_credit_api'];
		      		}else{
		      			$credit = $rowDomain['sms_inter_credit'];
		      		}
		      		$sendSms = 'send_sms_inter';
		      		$sentSmsMt = 'sent_sms_inter_mt';
      				$sentSmsDlr = 'sent_sms_inter_dlr';
		      	}      	
		
		      	
		      	mt_srand((double)microtime()*10000);
		      	$charid = strtolower(md5(uniqid(rand(), true)));
		      	$hyphen = chr(45);// "-"
		      	$dlr_url = $userId.'@'.$campId.'-'.substr($charid, 0, 8).substr($charid,16, 4).substr($charid,20,12);
		      	 
		      	$datetime = date('Y-m-d H:i:s');
		      	$timestamp = strtotime($datetime);
		      	
		      	if (($domainStatus == '1') && ($userStatus == '1')){
		      		if ($credit >= $smsCount){	      			
		      			
		      			if (in_array($key, $randArray)) {
		      				
		      				$smsDb->where ('number', $data['0']);
		      				$rowPriority = $smsDb->getOne('priority_leads_sms');
		      				$smsDb->count;
		      				if ($smsDb->count >0) {
		      					
			      				if ($type == 'TRANSACTIONAL') {
				      				if ($method == '2'){
				      					$creditData = Array ('sms_trans_credit_api' => $userDb->dec($smsCount));
				      				}else{
				      					$creditData = Array ('sms_trans_credit' => $userDb->dec($smsCount));
				      				}
			      				
				      			}elseif ($type == 'PREMIUM') {
				      				if ($method == '2'){
				      					$creditData = Array ('sms_premi_credit_api' => $userDb->dec($smsCount));
				      				}else{
				      					$creditData = Array ('sms_premi_credit' => $userDb->dec($smsCount));
				      				}
			      				
				      			}elseif ($type == 'TRANS-SCRUB') {
				      				if ($method == '2'){
				      					$creditData = Array ('sms_scrub_credit_api' => $userDb->dec($smsCount));
				      				}else{
				      					$creditData = Array ('sms_scrub_credit' => $userDb->dec($smsCount));
				      				}
				      			}elseif ($type == 'PROMOTIONAL') {
				      				if ($method == '2'){
				      					$creditData = Array ('sms_promo_credit_api' => $userDb->dec($smsCount));
				      				}else{
				      					$creditData = Array ('sms_promo_credit' => $userDb->dec($smsCount));
				      				}
				      			}elseif ($type == 'INTERNATIONAL') {
				      				if ($method == '2'){
				      					$creditData = Array ('sms_inter_credit_api' => $userDb->dec($smsCount));
				      				}else{
				      					$creditData = Array ('sms_inter_credit' => $userDb->dec($smsCount));
				      				}
				      			}
		      					


		      					$userDb->where('id', $parentDomainId);
		      					$userDb->update ('domains', $creditData);
		      					
		      					$data1 = Array ("momt" => "MT",
		      							"sender" => "$sender",
		      							"smsc_id" => "$router",
		      							"receiver" => $data['0'],
		      							"msgdata" => "$textMsg",
		      							"meta_data" =>"",
		      							"sms_type" => '2',
		      							"time" => "$timestamp",
		      							"dlr_mask" => '19',
		      							"dlr_url" => "$dlr_url",
		      							"coding" => "$ecoding",
										"validity" => $validity,
		      							"priority" => "$smsPriority",
		      							"mclass" => $flash,
		      							"binfo" => "$smsCount",
		      							"account" => "$method",
		      					);
		      					$insertSendTable= $smsDb->insert("$sendSms", $data1);

                                                        if ($urlStatus){
                                                            $tinyUrlData = Array ("user_id" => "$userId",
                                                                            "campaign_name" => "$name",
                                                                            "campaign_id" => "$id",
                                                                            "receiver" => $data['0'],
                                                                            "tiny_url" => "$uuid",
                                                                            "data" =>"",
                                                                            "type" => "$urlStatus",
                                                                            "url" => "$tinyUrl",
                                                                            "status" => '0',
                                                                            "created_date" => $urlDb->now(),
                                                                            "updated_date" => $urlDb->now(),
                                                            );

                                                            $insertUrlTable= $urlDb->insert('url_report', $tinyUrlData);
                                                        }
		      						      				//TinyURL      				
			      				
		      					
		      				}else{
		      					//echo "FAK-".$num."\n";
		      					
		      					if ($makeKey <= $make){
				      					$data1 = Array ("momt" => "MT",
				      							"sender" => "$sender",
				      							"smsc_id" => "FROUTE",
				      							"receiver" => $data['0'],
				      							"msgdata" => "$textMsg",
				      							"meta_data" =>"",
				      							"sms_type" => '2',
				      							"time" => "$timestamp",
				      							"dlr_mask" => '19',
				      							"dlr_url" => "$dlr_url",
				      							"coding" => "$ecoding",
				      							"validity" => $validity,
				      							"priority" => "$smsPriority",
				      							"mclass" => $flash,
				      							"binfo" => "$smsCount",
				      							"dlrdata" => urlencode('stat:DELIVRD err:000 text:').mb_substr($textMsg, 0, 13),
				      							"status" => '1',
				      							"account" => "$method",
				      					);
				      					$insertSentTable= $smsDb->insert("$sentSmsMt", $data1);
		      					
		      						 
			      						$data1 = Array ("momt" => "DLR",
			      								"sender" => "$sender",
			      								"smsc_id" => "FROUTE",
			      								"receiver" => $data['0'],
			      								"msgdata" => urlencode('stat:DELIVRD err:000 text:').mb_substr($textMsg, 0, 13),
			      								"meta_data" =>"",
			      								"sms_type" => '3',
			      								"time" => "$timestamp",
			      								"dlr_mask" => '19',
			      								"dlr_url" => "$dlr_url",
			      								"coding" => "$ecoding",
			      								"validity" => $validity,
			      								"priority" => "$smsPriority",
			      								"mclass" => $flash,
			      								"binfo" => "$smsCount",
			      								"dlrdata" => "",
			      								"status" => '1',
			      								"account" => "$method",
			      						);
			      						$insertDlrTable= $smsDb->insert("$sentSmsDlr", $data1);

                                                                        if ($urlStatus){
                                                                            $tinyUrlData = Array ("user_id" => "$userId",
                                                                                            "campaign_name" => "$name",
                                                                                            "campaign_id" => "$id",
                                                                                            "receiver" => $data['0'],
                                                                                            "tiny_url" => "$uuid",
                                                                                            "data" =>"",
                                                                                            "type" => "$urlStatus",
                                                                                            "url" => "$tinyUrl",
                                                                                            "status" => '0',
                                                                                            "created_date" => $urlDb->now(),
                                                                                            "updated_date" => $urlDb->now(),
                                                                            );

                                                                            $insertUrlTable= $urlDb->insert('url_report', $tinyUrlData);
                                                                        }
			      							      				//TinyURL      				
					      				
		      						
		      					}else{
		      					
		      						$dlrMake = array(
		      								'stat:FAILED err:51B text:',
		      								'stat:FAILED err:569 text:',
		      						);
		      						
		      						$dlrKey = array_rand($dlrMake);
		      						$dlr = $dlrMake[$dlrKey];
		      						
		      						$data1 = Array ("momt" => "MT",
		      								"sender" => "$sender",
		      								"smsc_id" => "FROUTE",
		      								"receiver" => $data['0'],
		      								"msgdata" => "$textMsg",
		      								"meta_data" =>"",
		      								"sms_type" => '2',
		      								"time" => "$timestamp",
		      								"dlr_mask" => '19',
		      								"dlr_url" => "$dlr_url",
		      								"coding" => "$ecoding",
		      								"validity" => $validity,
		      								"priority" => "$smsPriority",
		      								"mclass" => $flash,
		      								"binfo" => "$smsCount",
		      								"dlrdata" => urlencode("$dlr").mb_substr($textMsg, 0, 13),
		      								"status" => '1',
		      								"account" => "$method",
		      						);
		      						$insertSentTable= $smsDb->insert("$sentSmsMt", $data1);
		      						 
		      						 
		      						$data1 = Array ("momt" => "DLR",
		      								"sender" => "$sender",
		      								"smsc_id" => "FROUTE",
		      								"receiver" => $data['0'],
		      								"msgdata" => urlencode("$dlr").mb_substr($textMsg, 0, 13),
		      								"meta_data" =>"",
		      								"sms_type" => '3',
		      								"time" => "$timestamp",
		      								"dlr_mask" => '19',
		      								"dlr_url" => "$dlr_url",
		      								"coding" => "$ecoding",
		      								"validity" => $validity,
		      								"priority" => "$smsPriority",
		      								"mclass" => $flash,
		      								"binfo" => "$smsCount",
		      								"dlrdata" => "",
		      								"status" => '1',
		      								"account" => "$method",
		      						);
		      						$insertDlrTable= $smsDb->insert("$sentSmsDlr", $data1);

                                                                if ($urlStatus){
                                                                    $tinyUrlData = Array ("user_id" => "$userId",
                                                                                    "campaign_name" => "$name",
                                                                                    "campaign_id" => "$id",
                                                                                    "receiver" => $data['0'],
                                                                                    "tiny_url" => "$uuid",
                                                                                    "data" =>"",
                                                                                    "type" => "$urlStatus",
                                                                                    "url" => "$tinyUrl",
                                                                                    "status" => '0',
                                                                                    "created_date" => $urlDb->now(),
                                                                                    "updated_date" => $urlDb->now(),
                                                                    );

                                                                    $insertUrlTable= $urlDb->insert('url_report', $tinyUrlData);
                                                                }
		      							      				//TinyURL      				
				      				
		      					}
		      					$makeKey++;
		      					
		      				}
		      				
		      				 
		      			}else{
		      				
		      				if ($type == 'TRANSACTIONAL') {
		      					if ($method == '2'){
		      						$creditData = Array ('sms_trans_credit_api' => $userDb->dec($smsCount));
		      					}else{
		      						$creditData = Array ('sms_trans_credit' => $userDb->dec($smsCount));
		      					}
		      				
		      				}elseif ($type == 'PREMIUM') {
		      					if ($method == '2'){
		      						$creditData = Array ('sms_premi_credit_api' => $userDb->dec($smsCount));
		      					}else{
		      						$creditData = Array ('sms_premi_credit' => $userDb->dec($smsCount));
		      					}
		      				
		      				}elseif ($type == 'TRANS-SCRUB') {
		      					if ($method == '2'){
		      						$creditData = Array ('sms_scrub_credit_api' => $userDb->dec($smsCount));
		      					}else{
		      						$creditData = Array ('sms_scrub_credit' => $userDb->dec($smsCount));
		      					}
		      				}elseif ($type == 'PROMOTIONAL') {
		      					if ($method == '2'){
		      						$creditData = Array ('sms_promo_credit_api' => $userDb->dec($smsCount));
		      					}else{
		      						$creditData = Array ('sms_promo_credit' => $userDb->dec($smsCount));
		      					}
		      				}elseif ($type == 'INTERNATIONAL') {
		      					if ($method == '2'){
		      						$creditData = Array ('sms_inter_credit_api' => $userDb->dec($smsCount));
		      					}else{
		      						$creditData = Array ('sms_inter_credit' => $userDb->dec($smsCount));
		      					}
		      				}
		      				 
		      				$userDb->where('id', $parentDomainId);
		      				$userDb->update ('domains', $creditData);
		      				
		      				if (in_array($key, $randArrayMain)) {
		      				
		      					$smsDb->where ('number', $data['0']);
		      					$rowPriority = $smsDb->getOne('priority_leads_sms');
		      					$smsDb->count;
		      					if ($smsDb->count >0) {
		      						 
		      						
		      						 
		      						$data1 = Array ("momt" => "MT",
		      								"sender" => "$sender",
		      								"smsc_id" => "$router",
		      								"receiver" => $data['0'],
		      								"msgdata" => "$textMsg",
		      								"meta_data" =>"",
		      								"sms_type" => '2',
		      								"time" => "$timestamp",
		      								"dlr_mask" => '19',
		      								"dlr_url" => "$dlr_url",
		      								"coding" => "$ecoding",
		      								"validity" => $validity,
		      								"priority" => "$smsPriority",
		      								"mclass" => $flash,
		      								"binfo" => "$smsCount",
		      								"account" => "$method",
		      						);
		      						$insertSendTable= $smsDb->insert("$sendSms", $data1);

                                                                if ($urlStatus){
                                                                    //TinyURL      				
                                                                    $tinyUrlData = Array ("user_id" => "$userId",
                                                                                    "campaign_name" => "$name",
                                                                                    "campaign_id" => "$id",
                                                                                    "receiver" => $data['0'],
                                                                                    "tiny_url" => "$uuid",
                                                                                    "data" =>"",
                                                                                    "type" => "$urlStatus",
                                                                                    "url" => "$tinyUrl",
                                                                                    "status" => '0',
                                                                                    "created_date" => $urlDb->now(),
                                                                                    "updated_date" => $urlDb->now(),
                                                                    );

                                                                    $insertUrlTable= $urlDb->insert('url_report', $tinyUrlData);
                                                                }
		      						
		      						 
		      					}else{
		      						//echo "FAK-MAIN-".$num."\n";
		      						 
		      						if ($makeMainKey <= $makeMain){
		      							$data1 = Array ("momt" => "MT",
		      									"sender" => "$sender",
		      									"smsc_id" => "MROUTE",
		      									"receiver" => $data['0'],
		      									"msgdata" => "$textMsg",
		      									"meta_data" =>"",
		      									"sms_type" => '2',
		      									"time" => "$timestamp",
		      									"dlr_mask" => '19',
		      									"dlr_url" => "$dlr_url",
		      									"coding" => "$ecoding",
		      									"validity" => $validity,
		      									"priority" => "$smsPriority",
		      									"mclass" => $flash,
		      									"binfo" => "$smsCount",
		      									"dlrdata" => urlencode('stat:DELIVRD err:000 text:').mb_substr($textMsg, 0, 13),
		      									"status" => '1',
		      									"account" => "$method",
		      							);
		      							$insertSentTable= $smsDb->insert("$sentSmsMt", $data1);
		      							 
		      							 
		      							$data1 = Array ("momt" => "DLR",
		      									"sender" => "$sender",
		      									"smsc_id" => "MROUTE",
		      									"receiver" => $data['0'],
		      									"msgdata" => urlencode('stat:DELIVRD err:000 text:').mb_substr($textMsg, 0, 13),
		      									"meta_data" =>"",
		      									"sms_type" => '3',
		      									"time" => "$timestamp",
		      									"dlr_mask" => '19',
		      									"dlr_url" => "$dlr_url",
		      									"coding" => "$ecoding",
		      									"validity" => $validity,
		      									"priority" => "$smsPriority",
		      									"mclass" => $flash,
		      									"binfo" => "$smsCount",
		      									"dlrdata" => "",
		      									"status" => '1',
		      									"account" => "$method",
		      							);
		      							$insertDlrTable= $smsDb->insert("$sentSmsDlr", $data1);

		      							if ($urlStatus){
                                                                            //TinyURL  
                                                                            $tinyUrlData = Array ("user_id" => "$userId",
					      						"campaign_name" => "$name",
					      						"campaign_id" => "$id",
					      						"receiver" => $data['0'],
					      						"tiny_url" => "$uuid",
					      						"data" =>"",
					      						"type" => "$urlStatus",
					      						"url" => "$tinyUrl",
					      						"status" => '0',
					      						"created_date" => $urlDb->now(),
			      								"updated_date" => $urlDb->now(),
                                                                            );

                                                                            $insertUrlTable= $urlDb->insert('url_report', $tinyUrlData);
                                                                        }
					      				
		      				
		      						}else{
		      							 
		      							$dlrMake = array(
		      									'stat:FAILED err:51B text:',
		      									'stat:FAILED err:569 text:',
		      							);
		      				
		      							$dlrKey = array_rand($dlrMake);
		      							$dlr = $dlrMake[$dlrKey];
		      				
		      							$data1 = Array ("momt" => "MT",
		      									"sender" => "$sender",
		      									"smsc_id" => "MROUTE",
		      									"receiver" => $data['0'],
		      									"msgdata" => "$textMsg",
		      									"meta_data" =>"",
		      									"sms_type" => '2',
		      									"time" => "$timestamp",
		      									"dlr_mask" => '19',
		      									"dlr_url" => "$dlr_url",
		      									"coding" => "$ecoding",
		      									"validity" => $validity,
		      									"priority" => "$smsPriority",
		      									"mclass" => $flash,
		      									"binfo" => "$smsCount",
		      									"dlrdata" => urlencode("$dlr").mb_substr($textMsg, 0, 13),
		      									"status" => '1',
		      									"account" => "$method",
		      							);
		      							$insertSentTable= $smsDb->insert("$sentSmsMt", $data1);
		      							 
		      							 
		      							$data1 = Array ("momt" => "DLR",
		      									"sender" => "$sender",
		      									"smsc_id" => "MROUTE",
		      									"receiver" => $data['0'],
		      									"msgdata" => urlencode("$dlr").mb_substr($textMsg, 0, 13),
		      									"meta_data" =>"",
		      									"sms_type" => '3',
		      									"time" => "$timestamp",
		      									"dlr_mask" => '19',
		      									"dlr_url" => "$dlr_url",
		      									"coding" => "$ecoding",
		      									"validity" => $validity,
		      									"priority" => "$smsPriority",
		      									"mclass" => $flash,
		      									"binfo" => "$smsCount",
		      									"dlrdata" => "",
		      									"status" => '1',
		      									"account" => "$method",
		      							);
		      							$insertDlrTable= $smsDb->insert("$sentSmsDlr", $data1);
                                                                            
                                                                        if ($urlStatus){
                                                                            //TinyURL      				
                                                                            $tinyUrlData = Array ("user_id" => "$userId",
                                                                                            "campaign_name" => "$name",
                                                                                            "campaign_id" => "$id",
                                                                                            "receiver" => $data['0'],
                                                                                            "tiny_url" => "$uuid",
                                                                                            "data" =>"",
                                                                                            "type" => "$urlStatus",
                                                                                            "url" => "$tinyUrl",
                                                                                            "status" => '0',
                                                                                            "created_date" => $urlDb->now(),
                                                                                            "updated_date" => $urlDb->now(),
                                                                            );

                                                                            $insertUrlTable= $urlDb->insert('url_report', $tinyUrlData);
                                                                        }
		      							

		      						}
		      						$makeMainKey++;
		      						 
		      					}
		      				
		      					 
		      				}else{
		      					//echo 'OPR-'.$num."\n";
		      					
		      					/*
		      					 $data1 = Array ("momt" => "MT",
		      					 "sender" => "$sender",
		      					 "smsc_id" => "$router",
		      					 "receiver" => $data['0'],
		      					 "msgdata" => "$text",
		      					 "meta_data" =>"",
		      					 "sms_type" => '2',
		      					 "time" => "$timestamp",
		      					 "dlr_mask" => '19',
		      					 "dlr_url" => "$dlr_url",
		      					 "coding" => "$ecoding",
		      					 "validity" => "$validity",
		      					 "priority" => "$smsPriority",
		      					 "mclass" => NULL,
		      					 "binfo" => "$smsCount",
		      					 );
		      					 $insertSendTable= $smsDb->insert("$sendSms", $data1);
		      					*/
		      					$sendCounter++;
		      					
		      					$sendData[] = Array ("momt" => "MT",
		      							"sender" => "$sender",
		      							"smsc_id" => "$router",
		      							"receiver" => $data['0'],
		      							"msgdata" => "$textMsg",
		      							"meta_data" =>"",
		      							"sms_type" => '2',
		      							"time" => "$timestamp",
		      							"dlr_mask" => '19',
		      							"dlr_url" => "$dlr_url",
		      							"coding" => "$ecoding",
		      							"validity" => $validity,
		      							"priority" => "$smsPriority",
		      							"mclass" => $flash,
		      							"binfo" => "$smsCount",
		      							"account" => "$method",
		      					);

		      					if ($urlStatus){
                                                                //TinyURL      
                                                                $tinyUrlData[] = Array ("user_id" => "$userId",
                                                                            "campaign_name" => "$name",
                                                                            "campaign_id" => "$id",
                                                                            "receiver" => $data['0'],
                                                                            "tiny_url" => "$uuid",
                                                                            "data" =>"",
                                                                            "type" => "$urlStatus",
                                                                            "url" => "$tinyUrl",
                                                                            "status" => '0',
                                                                            "created_date" => $urlDb->now(),
                                                                            "updated_date" => $urlDb->now(),
			      				);
                                                        }
			      				

	      				
		      					
		      					if ($sendCounter == $batchCount){
		      						//echo 'SEND='.$sendCounter;
		      						$insertSendTable= $smsDb->insertMulti($sendSms, $sendData);
		      						unset($sendData);
		      						$sendData = array();
                                                                
                                                                if ($urlStatus){
                                                                    $insertUrlTable= $urlDb->insertMulti('url_report', $tinyUrlData);
                                                                    unset($tinyUrlData);
                                                                    $tinyUrlData = array();
                                                                }
		      						

		      						$sendCounter = '0';
		      					}
		      				}
		      				
		      			}
		      			 
		      			$key++;
		      		}else {
		      			
		      			if ($sendCounter < $batchCount && $sendCounter != '0'){
		      				//echo 'NO-CREDIT='.$sendCounter;
		      				$insertSendTable= $smsDb->insertMulti("$sendSms", $sendData);
		      				unset($sendData);
		      				$sendData = array();

                                                if ($urlStatus){
                                                    $insertUrlTable= $urlDb->insertMulti('url_report', $tinyUrlData);
                                                    unset($tinyUrlData);
                                                    $tinyUrlData = array();
                                                }
		      				
		      			
		      				$sendCounter = '0';
		      			}
		      			$data1 = Array ("momt" => "MT",
		      					"sender" => "$sender",
		      					"smsc_id" => "$router",
		      					"receiver" => $data['0'],
		      					"msgdata" => "$textMsg",
		      					"meta_data" =>"",
		      					"sms_type" => '2',
		      					"time" => "$timestamp",
		      					"dlr_mask" => '19',
		      					"dlr_url" => "$dlr_url",
		      					"coding" => "$ecoding",
								"validity" => $validity,
		      					"priority" => "$smsPriority",
		      					"mclass" => $flash,
		      					"binfo" => "$smsCount",
		      					"dlrdata" => urlencode('stat:NOCREDIT err:BBBB text:').mb_substr($textMsg, 0, 13),
		      					"status" => '1',
		      					"account" => "$method",
		      			);
		      			$insertSentTable= $smsDb->insert("$sentSmsMt", $data1);
		      			
		      			$data1 = Array ("momt" => "DLR",
		      					"sender" => "$sender",
		      					"smsc_id" => "$router",
		      					"receiver" => $data['0'],
		      					"msgdata" => urlencode('stat:NOCREDIT err:BBBB text:').mb_substr($textMsg, 0, 13),
		      					"meta_data" =>"",
		      					"sms_type" => '3',
		      					"time" => "$timestamp",
		      					"dlr_mask" => '19',
		      					"dlr_url" => "$dlr_url",
		      					"coding" => "$ecoding",
								"validity" => $validity,
		      					"priority" => "$smsPriority",
		      					"mclass" => $flash,
		      					"binfo" => "$smsCount",
		      					"dlrdata" => "",
		      					"status" => '1',
		      					"account" => "$method",
		      			);
		      			$insertDlrTable= $smsDb->insert("$sentSmsDlr", $data1);

                                        if ($urlStatus){
                                            //TinyURL      				
                                            $tinyUrlData = Array ("user_id" => "$userId",
                                                            "campaign_name" => "$name",
                                                            "campaign_id" => "$id",
                                                            "receiver" => $data['0'],
                                                            "tiny_url" => "$uuid",
                                                            "data" =>"",
                                                            "type" => "$urlStatus",
                                                            "url" => "$tinyUrl",
                                                            "status" => '0',
                                                            "created_date" => $urlDb->now(),
                                                            "updated_date" => $urlDb->now(),
                                            );

                                            $insertUrlTable= $urlDb->insert('url_report', $tinyUrlData);
                                        }
		      			
		      		}
		      	}else {
		      		$data1 = Array ("momt" => "MT",
		      				"sender" => "$sender",
		      				"smsc_id" => "$router",
		      				"receiver" => $data['0'],
		      				"msgdata" => "$textMsg",
		      				"meta_data" =>"",
		      				"sms_type" => '2',
		      				"time" => "$timestamp",
		      				"dlr_mask" => '19',
		      				"dlr_url" => "$dlr_url",
		      				"coding" => "$ecoding",
							"validity" => $validity,
		      				"priority" => "$smsPriority",
		      				"mclass" => $flash,
		      				"binfo" => "$smsCount",
		      				"dlrdata" => urlencode('stat:BLOCKED err:DDDD text:').mb_substr($textMsg, 0, 13),
		      				"status" => '1',
		      				"account" => "$method",
		      		);
		      		$insertSentTable= $smsDb->insert("$sentSmsMt", $data1);
		      		
		      		$data1 = Array ("momt" => "DLR",
		      				"sender" => "$sender",
		      				"smsc_id" => "$router",
		      				"receiver" => $data['0'],
		      				"msgdata" => urlencode('stat:BLOCKED err:DDDD text:').mb_substr($textMsg, 0, 13),
		      				"meta_data" =>"",
		      				"sms_type" => '3',
		      				"time" => "$timestamp",
		      				"dlr_mask" => '19',
		      				"dlr_url" => "$dlr_url",
		      				"coding" => "$ecoding",
							"validity" => $validity,
		      				"priority" => "$smsPriority",
		      				"mclass" => $flash,
		      				"binfo" => "$smsCount",
		      				"dlrdata" => "",
		      				"status" => '1',
		      				"account" => "$method",
		      		);
		      		$insertDlrTable= $smsDb->insert("$sentSmsDlr", $data1);

                                if ($urlStatus){
                                    //TinyURL      				
                                        $tinyUrlData = Array ("user_id" => "$userId",
                                                    "campaign_name" => "$name",
                                                    "campaign_id" => "$id",
                                                    "receiver" => $data['0'],
                                                    "tiny_url" => "$uuid",
                                                    "data" =>"",
                                                    "type" => "$urlStatus",
                                                    "url" => "$tinyUrl",
                                                    "status" => '0',
                                                    "created_date" => $urlDb->now(),
                                                    "updated_date" => $urlDb->now(),
                                        );

                                        $insertUrlTable= $urlDb->insert('url_report', $tinyUrlData);
                                }
		      		
		      	}
		      	                 
		}
		fclose($handle);
		if ($sendCounter < $batchCount && $sendCounter != '0'){
			//echo 'LAST='.$sendCounter;
			$insertSendTable= $smsDb->insertMulti("$sendSms", $sendData);
			unset($sendData);
			$sendData = array();
                        
                        if ($urlStatus){
                            $insertUrlTable= $urlDb->insertMulti('url_report', $tinyUrlData);
                            unset($tinyUrlData);
                            $tinyUrlData = array();
                        }

			
			
			$sendCounter = '0';
		}

	}
}
	$data = Array ('status' => '1','report_status'=>'0');
	$smsDb->where('id', $id);
	$smsDb->update ('campaigns_sms', $data);
	
	shell_exec('/bin/echo $(date) -- END-CAMPAIGN-'.$id.' >> /var/log/webpanel/smscron-final-success-'.$today.'.log');	
}

?>
