#!/usr/bin/php
<?php
//sleep(5);
require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");


if (isset($argv[1], $argv[2], $argv[3])){
	$userId = $argv[1];
	$startDate = $argv[2];
	$endDate = $argv[3];
	$update = $argv[4];

$parentDomain = $userDb->rawQueryOne ("SELECT `id` FROM `domains` WHERE `owner_id`='$userId'");
$domainId = $parentDomain['id'];
$users = $userDb->rawQuery ("SELECT `id`,`username`,`domain_id` FROM `users` WHERE `parent_domain_id`='$domainId'");

	foreach ($users as $user){
		$underUserId = $user['id'];
		$underUserName = $user['username'];
		$underUserDomainId = $user['domain_id'];
		
		$domain = $userDb->rawQueryOne ("SELECT `domain` FROM `domains` WHERE `id`='$underUserDomainId'");
		$domainName = $domain['domain'];
		$dlrUrl = $underUserId.'@%';	
		$smsSentTrans = $smsDb->rawQueryOne ("SELECT count(*) AS SENT FROM `sent_sms_trans_mt` WHERE `time` >= '$startDate' AND `time` <= '$endDate' AND `dlr_url` LIKE '$dlrUrl'");
		$smsSentPromo = $smsDb->rawQueryOne ("SELECT count(*) AS SENT FROM `sent_sms_promo_mt` WHERE `time` >= '$startDate' AND `time` <= '$endDate' AND `dlr_url` LIKE '$dlrUrl'");
		$smsSentInter = $smsDb->rawQueryOne ("SELECT count(*) AS SENT FROM `sent_sms_inter_mt` WHERE `time` >= '$startDate' AND `time` <= '$endDate' AND `dlr_url` LIKE '$dlrUrl'");
	
		$transCount = $smsSentTrans['SENT'];
		$promoCount = $smsSentPromo['SENT'];
		$interCount = $smsSentInter['SENT'];
		
		if (($transCount != '0') || ($promoCount != '0') || ($interCount != '0')){
			
			if ($update){				
				
				$smsDb->where('user_id', $userId)->where('client_id', $underUserId)->where('domain', $domainName)->where('sent_date', $startDate);
				//$smsDb->update ('sms_user_usage', $data);
				$smsDb->delete('sms_user_usage');
				
				
			}

			$data = Array (
					"user_id" => "$userId",
					"client_id" => "$underUserId",
					"domain" => "$domainName",
					"username" => "$underUserName",
					"trans_sent" => "$transCount",
					"promo_sent" => "$promoCount",
					"inter_sent" => "$interCount",
					"sent_date" => $startDate,
					"created_date" => $smsDb->now(),
					"updated_date" => $smsDb->now()
			);
			$smsDb->insert ('sms_user_usage', $data);
			
		}
		
	}
}