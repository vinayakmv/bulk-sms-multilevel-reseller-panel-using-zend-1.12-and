  <?php
  require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';
  date_default_timezone_set("Asia/Kolkata"); 
  
  $startDate =date("2018-04-01 00:00:00");
  $endDate =date("2018-05-01 00:00:00");

if (isset($argv[1]))
{  
	$resellerId = $argv[1];
	
	$selectParent = "SELECT `id`,`username` FROM `users` where reseller_id = $resellerId";
	$result = $userDb->rawQuery($selectParent);
	foreach ($result as $row){
		$userId = $row['id'];
		$username = $row['username'];
		
		$startDateTimestamp = strtotime($startDate);
		$endDateTimestamp = strtotime($endDate);
	
		$dlrUrl = $userId.'@%';
		$delivered = '%3ADELIVRD+err%';
		$nocredit = 'err%3ABBBB';
		$fake = 'FROUTE';
		$submit = 'Submit+failed';
	
		/*
		 * TRANSACTIONAL RECREDIT COUNT
		 */
		$select_distinct_account = "SELECT DISTINCT `account` FROM `sent_sms_trans_mt` WHERE `dlr_url` like '$dlrUrl' AND `time` >= '$startDateTimestamp' AND `time` <= '$endDateTimestamp'";
		$select_distinct_account_result = $smsDb->rawQuery($select_distinct_account);
		$distinct_account_count = $smsDb->count;
	
		foreach ($select_distinct_account_result as $distinctAccountRow){
	
			$account = $distinctAccountRow['account'];
			if ($account == '2'){
				$smsType = 'SMS TRANS API';
			}else{
				$smsType = 'SMS TRANS';
			}
			$select_distinct_binfo = "SELECT DISTINCT `binfo` FROM `sent_sms_trans_mt` WHERE `account` = '$account' AND `dlr_url` like '$dlrUrl' AND `time` >= '$startDateTimestamp' AND `time` <= '$endDateTimestamp'";
			$select_distinct_binfo_result = $smsDb->rawQuery($select_distinct_binfo);
			$distinct_count = $smsDb->count;
			 
			foreach ($select_distinct_binfo_result as $distinctBinfoRow){
	
				$binfo = $distinctBinfoRow['binfo'];
	
				//MT
				$smsDb->where ("dlr_url", "$dlrUrl", 'like');
				$smsDb->where ("time", "$startDateTimestamp", '>=');
				$smsDb->where ("time", "$endDateTimestamp", '<=');
				$smsDb->where ('binfo', "$binfo");
				$smsDb->where ('account', "$account");
				$transMt = $smsDb->getOne("sent_sms_trans_mt", 'count(*) as mt');
				$smsTransMt = $transMt['mt'];
				 
				//Delivered
				$smsDb->where ("dlr_url", "$dlrUrl", 'like');
				$smsDb->where ("dlrdata", "$delivered", 'like');
				$smsDb->where ("time", "$startDateTimestamp", '>=');
				$smsDb->where ("time", "$endDateTimestamp", '<=');
				$smsDb->where ('binfo', "$binfo");
				$smsDb->where ('account', "$account");
				$transDelivered = $smsDb->getOne("sent_sms_trans_mt", 'count(*) as delivered');
				$smsTransDelivered = $transDelivered['delivered'];
	
echo "TIMESTAMP - $startDateTimestamp to $endDateTimestamp \n";
				 
				echo '['.$username.'-'.$userId.']---SMSTYPE='.$smsType.'---BINFO='.$binfo.'---MT='.$smsTransMt.'---DELIVERED='.($smsTransDelivered).'---RECREDIT='.($smsTransMt-$smsTransDelivered)."\n";
				 
			}
			 
		}
	
	}
}else{
	echo "Please pass the resellerId as the first argument eg : resellerReport.php 31\n";
}
  
