#!/bin/bash
x=$(date -d "0 days" +%d-%m-%Y)

stop()
{

$(/etc/init.d/smpptrans stop &)
$(/etc/init.d/smpppromo stop &)
$(/etc/init.d/smppinter stop &)

$(/etc/init.d/smstrans stop &)
$(/etc/init.d/smspromo stop &)
$(/etc/init.d/smsinter stop &)

}

backup()
{

$(/bin/mv /var/log/kannel /var/log/kannel/kannel-"$x")
$(/bin/mkdir /var/log/kannel &)

}

email()
{

$(/usr/bin/curl -L "http://94.130.134.249:19000/status.html?password=PTGi78" -o /var/log/kannel/$x-trans-status.html > /dev/null 2> /dev/null)
$(/usr/bin/curl -L "http://94.130.134.249:19002/status.html?password=PTGi78" -o /var/log/kannel/$x-promo-status.html > /dev/null 2> /dev/null)
#mail -s "$(echo -e "Smpp Report-$x\nContent-Type: text/html")" vinayakvasu.mv@gmail.com,zakeermace786@gmail.com < /var/log/kannel/status.html

}

start()
{

$(/etc/init.d/smpptrans start &)
$(/etc/init.d/smpppromo start &)
$(/etc/init.d/smppinter start &)

$(/etc/init.d/smstrans start &)
$(/etc/init.d/smspromo start &)
$(/etc/init.d/smsinter start &)

}

email
stop
backup
start

