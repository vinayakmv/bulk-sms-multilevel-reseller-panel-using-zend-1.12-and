#!/usr/bin/php
<?php
//sleep(5);
require_once '/var/www/html/webpanel/cronjob/smpp/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");

if (isset($argv[1])){

	$smscId = $argv[1];
	//$from = $argv[2];
	//$to = $argv[3];	
	
	$delivered = '%3ADELIVRD+err%';
	$nocredit = '%NACK%2F0x00000460%2FNo+credit+failed%';
	$sender = '%NACK%2F0x00000471%2FSource+not+allowed+failed%';
	$template = '%NACK%2F0x00000468%2FTemplate+not+allowed+failed%';
	$ndnc = '%NACK%2F0x00000462%2FDnd+destination+failed%';
	$black = '%NACK%2F0x00000434%2FBlacklist+destination+failed%';
	
	    if (isset($smscId)){
					
	        //$fromTimestamp = strtotime($from.' 00:00:00');
	        //$toTimestamp = strtotime($to.' 23:59:59');

	        $from = date("Y-m-d 00:00:00");
	        $fromTimestamp = strtotime($from);
	        
	        $to = date("Y-m-d 23:59:59");
	        $toTimestamp = strtotime($to);
	        
	        $sentSms =  $smppDb->rawQuery ("SELECT sender, count(*) AS SENT FROM smpp_log_mt_trans WHERE time >= '$fromTimestamp' AND time <= '$toTimestamp' AND smsc_id ='$smscId' GROUP BY sender");
	        $awaitingSms = $smppDb->rawQuery ("SELECT sender, count(*) AS AWAITING FROM smpp_log_mt_trans WHERE time >= '$fromTimestamp' AND time <= '$toTimestamp' AND smsc_id ='$smscId' AND status='0' GROUP BY sender");
	        $notSentSms = $smppDb->rawQuery ("SELECT sender, count(*) AS NOT_SENT FROM smpp_log_mt_trans WHERE time >= '$fromTimestamp' AND time <= '$toTimestamp' AND smsc_id ='$smscId' AND (dlrdata  LIKE '$nocredit' OR dlrdata LIKE '$ndnc' OR dlrdata  LIKE '$sender') GROUP BY sender");
	        $deliveredSms = $smppDb->rawQuery ("SELECT sender, count(*) AS DELIVERED FROM smpp_log_mt_trans WHERE time >= '$fromTimestamp' AND time <= '$toTimestamp' AND smsc_id ='$smscId' AND dlrdata LIKE '%DELIVRD%' GROUP BY sender");
	        $failedSms = $smppDb->rawQuery ("SELECT sender, count(*) AS FAILED FROM smpp_log_mt_trans WHERE time >= '$fromTimestamp' AND time <= '$toTimestamp' AND smsc_id ='$smscId' AND dlrdata NOT LIKE '%DELIVRD%' GROUP BY sender");
if (($sentSms)){	        
	        foreach ($sentSms as $send){
	       		$sender = $send['sender'];
	       		$count = $send['SENT'];
			$sms[$sender] = array('SENT' => $count);
		}
}else{
	$sms = array();
}
if (($awaitingSms)){
		foreach ($awaitingSms as $sent){
                        $sender = $sent['sender'];
                        $count = $sent['AWAITING'];
                        $awaiting[$sender] = array('AWAITING' => $count);
                }
}else{
        $awaiting = array();
}
if (($notSentSms)){
		foreach ($notSentSms as $sent){
                        $sender = $sent['sender'];
                        $count = $sent['NOT_SENT'];
                        $not[$sender] = array('NOT' => $count);
                }
}else{
        $not = array();
}
if (($deliveredSms)){
		foreach ($deliveredSms as $sent){
                        $sender = $sent['sender'];
                        $count = $sent['DELIVERED'];
                        $dlvrd[$sender] = array('DLVRD' => $count);
                }
}else{
        $dlvrd = array();
}
if (($failedSms)){
                foreach ($failedSms as $sent){
                        $sender = $sent['sender'];
                        $count = $sent['FAILED'];
                        $failed[$sender] = array('FAILED' => $count);
                }
}else{
        $failed = array();
}
	         $sentSenderReport = (array_merge_recursive($sms,$awaiting,$not,$dlvrd,$failed));
	         foreach ($sentSenderReport as $reportSender => $report){
			echo $reportSender.' => ';
			if (isset($report['SENT'])){
				echo 'SENT = '.$report['SENT'].' | ';
			}else{
				echo '----- | ';
			}
			if (isset($report['AWAITING'])){
                                echo 'AWAITING = '.$report['AWAITING'].' | ';
                        }else{
                                echo '----- | ';
                        }
			if (isset($report['NOT_SENT'])){
                                echo 'NOT = '.$report['NOT_SENT'].' | ';
                        }else{
                                echo '----- | ';
                        }
			if (isset($report['DLVRD'])){
                                echo 'DLVRD = '.$report['DLVRD'].' | ';
                        }else{
                                echo '----- | ';
                        }
                        if (isset($report['FAILED'])){
                                echo 'FAILED = '.$report['FAILED'].' | ';
                        }else{
                                echo '----- | ';
                        }

			echo "\n";
		 }
    }
    	
}
?>
