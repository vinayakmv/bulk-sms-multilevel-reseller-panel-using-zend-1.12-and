#!/usr/bin/php
<?php
//sleep(5);
require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");


if (isset($argv[1], $argv[2], $argv[3])){
	$userId = $argv[1];
        $resellerId = $argv[2];
        $domainId = $argv[3];
	$parentDomainId = $argv[4];
        
        $today = date('Y-m-d', strtotime('today'));
	$yesterDay = date('Y-m-d', strtotime('- 1 day'));
        


        $campaignsTrans = $smsDb->rawQuery ("SELECT sum(`count`) as sent, sum(credit) as credits FROM `campaigns_sms` WHERE `type` = 'TRANSACTIONAL' AND `start_date` < '$today' AND `start_date` >= '$yesterDay' AND `user_id`='$userId'");
        $campaignsPromo = $smsDb->rawQuery ("SELECT sum(`count`) as sent, sum(credit) as credits FROM `campaigns_sms` WHERE `type` = 'PROMOTIONAL' AND `start_date` < '$today' AND `start_date` >= '$yesterDay' AND `user_id`='$userId'");
        $campaignsScrub = $smsDb->rawQuery ("SELECT sum(`count`) as sent, sum(credit) as credits FROM `campaigns_sms` WHERE `type` = 'TRANS-SCRUB' AND `start_date` < '$today' AND `start_date` >= '$yesterDay' AND `user_id`='$userId'");
        $campaignsInter = $smsDb->rawQuery ("SELECT sum(`count`) as sent, sum(credit) as credits FROM `campaigns_sms` WHERE `type` = 'INTERNATIONAL' AND `start_date` < '$today' AND `start_date` >= '$yesterDay' AND `user_id`='$userId'");

        if (isset($campaignsTrans['sent'])){
            $campaignsTransSent = $campaignsTrans['sent'];
        }else{
            $campaignsTransSent = '0';
        }
        if (isset($campaignsPromo['sent'])){
            $campaignsPromoSent = $campaignsPromo['sent'];
        }else{
            $campaignsPromoSent = '0';
        }
        if (isset($campaignsScrub['sent'])){
            $campaignsScrubSent = $campaignsScrub['sent'];
        }else{
            $campaignsScrubSent = '0';
        }
        if (isset($campaignsInter['sent'])){
            $campaignsInterSent = $campaignsInter['sent'];
        }else{
            $campaignsInterSent = '0';
        }

        $data = Array (
                        "user_id" => "$userId",
                        "reseller_id" => "$resellerId",
                        "domain_id" => "$domainId",
                        "parent_domain_id" => "$parentDomainId",
                        "sent_sms_trans" => $campaignsTransSent,
                        "sent_sms_promo" => $campaignsPromoSent,
                        "sent_sms_scrub" => $campaignsScrubSent,
                        "sent_sms_inter" => $campaignsInterSent,
                        "report_date" => $yesterDay,
                        "created_date" => $smsDb->now(),
                        "updated_date" => $smsDb->now()
        );
        $smsDb->insert ('sms_dashboard', $data);
			
		
}