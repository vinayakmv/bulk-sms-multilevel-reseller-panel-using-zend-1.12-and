#!/usr/bin/php
<?php 
//sleep(5);
require_once '/var/www/html/webpanel/cronjob/voice/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");
$today = date("d-m-y");
function checkArray($rand, $array, $max){
	if (in_array($rand, $array)){
		$rand = rand(1, $max);
		$rand = checkArray($rand, $array, $max);
	}
	return $rand;
}
function checkArrayMain($rand, $array, $arrayMain, $max){

	if ((in_array($rand, $array)) || (in_array($rand, $arrayMain))){
		$rand = rand(1, $max);
		$rand = checkArrayMain($rand, $array, $arrayMain, $max);
	}
	return $rand;

}
if (isset($argv[1])){
	
	  $batchCount = 100;
	  $cdateTime =date("Y-m-d H:i:s");
	  $cdte1 = strtotime($cdateTime);
	  $id = $argv[1];
	  
	  $voiceDb->where ('status', 2);
	  $voiceDb->where ('id', $id);
	  $row = $voiceDb->getOne('campaigns_voice');
	  $voiceDb->count;
	  if ($voiceDb->count >0) {
	  	shell_exec('/bin/echo $(date) -- START CAMPAIGN-'.$id.' >> /var/log/webpanel/voicecron-final-success-'.$today.'.log');
	  	 
	    //*********** find the csv name ***************
	  	$lead = $row['leads_file'];
	  	$voice = $row['voice_file'];
	  	
	    $sender = '+91'.$row['sender_id'];
	    $router = $row['route'];
	    $method = $row['method'];
	    $voicePriority = ($row['voice_priority'] ? $row['voice_priority'] : '0');
	    $makeCount = $row['fake_count'];
	    $count = $row['count'];
	    $type = $row['type'];
	    $make = ceil($makeCount*90/100);
	    
	    $context = $row['context'];
	    $duration = $row['duration'];
	    
	 	if ($duration == '1') {
	    	$callDuration = '28';
	    }elseif ($duration == '2') {
	    	$callDuration = '58';
	    }elseif ($duration == '3') {
	    	$callDuration = '88';
	    }elseif ($duration == '4') {
	    	$callDuration = '118';
	    }else {
	    	$callDuration = '0';
	    }
	    
	    //****** $routerr is name of route ********************END
	    $cdte = date('Y-m-d H:i:s');
	    $userId = $row['user_id'];     
	    $campId = $row['id']; 
		
	    $userDb->where ('id', $userId);
	    $rowUser = $userDb->getOne('users');
	    $parentDomainId = $rowUser['parent_domain_id'];
	     
	    $userDb->where ('id', $parentDomainId);
	    $rowDomainParent = $userDb->getOne('domains');
	    
	    if ($type == 'TRANSACTIONAL') {
	    	$domainPercentageMain = $rowDomainParent['voice_percentage_trans_main'];
	    }elseif ($type == 'PROMOTIONAL') {
	    	$domainPercentageMain = $rowDomainParent['voice_percentage_promo_main'];
	    }
	    
	    if ($makeCount > 0){
	    	$makeCountMain = ceil($opCount*$domainPercentageMain/100);
	    	$makeMain = ceil($makeCountMain*90/100);
	    }else{
	    	$makeCountMain = 0;
	    	$makeMain = ceil($makeCountMain*90/100);
	    }
	
	
	    if (($handle = fopen('/var/www/html/webpanel/contactFiles/'.$lead, "r")) !== FALSE) {
	
	      $fp = file('/var/www/html/webpanel/contactFiles/'.$lead);
	      $count = count($fp);
	      $inc=0;
	      
	//Fake generation logic      
	      $counter=1;
	      $max=$count;
	      $randArray =array();
	      while ($counter <= $makeCount){
	      	$rand = rand(1, $max);
	      	$rand = checkArray($rand, $randArray, $max);
	      	$randArray[] = $rand;
	      	$counter++;
	      }
		$key = 1;
//Fake End

//Fake generation logic
		$counterMain=1;
		$maxMain=$count;
		$randArrayMain =array();
		while ($counterMain <= $makeCountMain){
			$randMain = rand(1, $maxMain);
			$randMain = checkArrayMain($randMain, $randArray, $randArrayMain, $maxMain);
			$randArrayMain[] = $randMain;
			$counterMain++;
		}
		$keyMain = 1;
		//Fake End
		
      $sendCounter = '0';
      $sendData = array();
      $makeKey = 1;
      $makeMainKey = 1;
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		      	
		
		      	$userDb->where ('id', $userId);
		      	$rowUser = $userDb->getOne('users');
		      	
		      	$userStatus = $rowUser['status'];
		      	$domainId = $rowUser['domain_id'];
		      	$parentDomainId = $rowUser['parent_domain_id'];
		      	
		      	$userDb->where ('id', $parentDomainId);
		      	$rowDomain = $userDb->getOne('domains');
		      	$domainStatus = $rowDomain['status'];
		      	
		      	if ($type == 'TRANSACTIONAL') {
		      		if ($method == '2'){
		      			$credit = $rowDomain['voice_trans_credit_api'];
		      		}else{
		      			$credit =  $rowDomain['voice_trans_credit'];
		      		}
		      		$sendVoice = 'send_voice_trans';
		      		$sentVoice = 'sent_voice_trans';
		      	}else {
		      		
		      		if ($method == '2'){
		      			$credit = $rowDomain['voice_promo_credit_api'];
		      		}else{
		      			$credit = $rowDomain['voice_promo_credit'];
		      		}
		      		$sendVoice = 'send_voice_promo';
		      		$sentVoice = 'sent_voice_promo';
		      	}      	
		
		      	mt_srand((double)microtime()*10000);
		      	$charid = strtolower(md5(uniqid(rand(), true)));
		      	$hyphen = chr(45);// "-"
		      	$dlr_url = $userId.'@'.$campId.'-'.substr($charid, 0, 8).substr($charid,16, 4).substr($charid,20,12);
		      	 
		      	$datetime = date('Y-m-d H:i:s');
		      	$timestamp = strtotime($datetime);
		      	
		      	if (($domainStatus == '1') && ($userStatus == '1')){
		      		if ($credit >= $duration){    			
		      			
		      			if (in_array($key, $randArray)) {
		      				
		      				$voiceDb->where ('number', $data['0']);
		      				$rowPriority = $voiceDb->getOne('priority_leads_voice');
		      				$voiceDb->count;
		      				if ($voiceDb->count >0) {
		      					
		      					if ($type == 'TRANSACTIONAL') {
		      						if ($method == '2'){
				      					$creditData = Array ('voice_trans_credit_api' => $userDb->dec($duration));
				      				}else{
				      					$creditData = Array ('voice_trans_credit' => $userDb->dec($duration));
				      				}
		      					}else {
		      						if ($method == '2'){
				      					$creditData = Array ('voice_promo_credit_api' => $userDb->dec($duration));
				      				}else{
				      					$creditData = Array ('voice_promo_credit' => $userDb->dec($duration));
				      				}
		      					}
		      					
		      					
		      					$userDb->where('id', $parentDomainId);
		      					$userDb->update ('domains', $creditData);
		      					 
		      					$destination = $data[0];					
								$dataOutgoing = Array ( "user_id" => "$userId",
										"voice_file" => "$voice",
										"sender" => "$sender",
										"receiver" => "$destination",
										"time" => "$timestamp",
										"route" => "$router",
										"campaign_id" => "$id",
										"context" => "$context",
										"priority" => "$voicePriority",
										"duration" => "$callDuration",
										"dlr_url" => "$dlr_url",
										"status" => '0',
								);
								$voiceDb->insert ($sentVoice, $dataOutgoing);
								$voiceDb->insert ($sendVoice, $dataOutgoing);
								
								
		      				}else{
		      					//echo "FAK-".$num."\n";
		      					if ($makeKey <= $make){
		      						$destination = $data[0];
		      						$dataOutgoing = Array ( "user_id" => "$userId",
		      								"voice_file" => "$voice",
		      								"sender" => "$sender",
		      								"receiver" => "$destination",
		      								"time" => "$timestamp",
		      								"route" => "FROUTE",
		      								"campaign_id" => "$id",
		      								"context" => "$context",
		      								"priority" => "$voicePriority",
		      								"duration" => "$callDuration",
		      								"dlr_url" => "$dlr_url",
		      								"dlrdata" => "ANSWERED",
		      								"status" => '1',
		      						);
		      						$voiceDb->insert ($sentVoice, $dataOutgoing);
		      						
		      					}else{
		      						$dlrMake = array(
		      								'NO ANSWER',
		      								'NETWORK BUSY',
		      								'BUSY',
		      						);
		      						
		      						$dlrKey = array_rand($dlrMake);
		      						$dlr = $dlrMake[$dlrKey];
		      						
		      						$destination = $data[0];
		      						$dataOutgoing = Array ( "user_id" => "$userId",
		      								"voice_file" => "$voice",
		      								"sender" => "$sender",
		      								"receiver" => "$destination",
		      								"time" => "$timestamp",
		      								"route" => "FROUTE",
		      								"campaign_id" => "$id",
		      								"context" => "$context",
		      								"priority" => "$voicePriority",
		      								"duration" => "$callDuration",
		      								"dlr_url" => "$dlr_url",
		      								"dlrdata" => "$dlr",
		      								"status" => '1',
		      						);
		      						$voiceDb->insert ($sentVoice, $dataOutgoing);
		      						
		      					}
		      					$makeKey++;
								
		      				}
		      				
		      				 
		      			}else{
		      				if ($type == 'TRANSACTIONAL') {
		      					if ($method == '2'){
		      						$creditData = Array ('voice_trans_credit_api' => $userDb->dec($duration));
		      					}else{
		      						$creditData = Array ('voice_trans_credit' => $userDb->dec($duration));
		      					}
		      				}else {
		      					if ($method == '2'){
		      						$creditData = Array ('voice_promo_credit_api' => $userDb->dec($duration));
		      					}else{
		      						$creditData = Array ('voice_promo_credit' => $userDb->dec($duration));
		      					}
		      				}
		      				 
		      				 
		      				$userDb->where('id', $parentDomainId);
		      				$userDb->update ('domains', $creditData);
		      				
		      				if (in_array($key, $randArrayMain)) {
		      					
		      					$voiceDb->where ('number', $data['0']);
		      					$rowPriority = $voiceDb->getOne('priority_leads_voice');
		      					$voiceDb->count;
		      					if ($voiceDb->count >0) {
		      					
		      						$destination = $data[0];
		      						$dataOutgoing = Array ( "user_id" => "$userId",
		      								"voice_file" => "$voice",
		      								"sender" => "$sender",
		      								"receiver" => "$destination",
		      								"time" => "$timestamp",
		      								"route" => "$router",
		      								"campaign_id" => "$id",
		      								"context" => "$context",
		      								"priority" => "$voicePriority",
		      								"duration" => "$callDuration",
		      								"dlr_url" => "$dlr_url",
		      								"status" => '0',
		      						);
		      						$voiceDb->insert ($sentVoice, $dataOutgoing);
		      						$voiceDb->insert ($sendVoice, $dataOutgoing);
		      						
		      					
		      					}else{
		      						
		      						//echo "FAK-MAIN-".$num."\n";
		      						if ($makeMainKey <= $makeMain){
		      							$destination = $data[0];
		      							$dataOutgoing = Array ( "user_id" => "$userId",
		      									"voice_file" => "$voice",
		      									"sender" => "$sender",
		      									"receiver" => "$destination",
		      									"time" => "$timestamp",
		      									"route" => "MROUTE",
		      									"campaign_id" => "$id",
		      									"context" => "$context",
		      									"priority" => "$voicePriority",
		      									"duration" => "$callDuration",
		      									"dlr_url" => "$dlr_url",
		      									"dlrdata" => "ANSWERED",
		      									"status" => '1',
		      							);
		      							$voiceDb->insert ($sentVoice, $dataOutgoing);
		      						}else{
		      							$dlrMake = array(
		      									'NO ANSWER',
		      									'NETWORK BUSY',
		      									'BUSY',
		      							);
		      							
		      							$dlrKey = array_rand($dlrMake);
		      							$dlr = $dlrMake[$dlrKey];
		      							
		      							$destination = $data[0];
		      							$dataOutgoing = Array ( "user_id" => "$userId",
		      									"voice_file" => "$voice",
		      									"sender" => "$sender",
		      									"receiver" => "$destination",
		      									"time" => "$timestamp",
		      									"route" => "MROUTE",
		      									"campaign_id" => "$id",
		      									"context" => "$context",
		      									"priority" => "$voicePriority",
		      									"duration" => "$callDuration",
		      									"dlr_url" => "$dlr_url",
		      									"dlrdata" => "$dlr",
		      									"status" => '1',
		      							);
		      							$voiceDb->insert ($sentVoice, $dataOutgoing);
		      						}
		      						$makeMainKey++;
		      					}
		      				}else{
		      					//echo 'OPR-'.$num."\n";
		      					
		      					$sendCounter++;
		      					 
		      					$destination = $data[0];
		      					$sendData[] = Array ( "user_id" => "$userId",
		      							"voice_file" => "$voice",
		      							"sender" => "$sender",
		      							"receiver" => "$destination",
		      							"time" => "$timestamp",
		      							"route" => "$router",
		      							"campaign_id" => "$id",
		      							"context" => "$context",
		      							"priority" => "$voicePriority",
		      							"duration" => "$callDuration",
		      							"dlr_url" => "$dlr_url",
		      							"status" => '0',
		      					);
		      					 
		      					if ($sendCounter == $batchCount){
		      						//echo 'SEND='.$sendCounter;
		      					
		      						$insertSendTable = $voiceDb->insertMulti($sendVoice, $sendData);
		      						$insertSentTable = $voiceDb->insertMulti($sentVoice, $sendData);
		      						unset($sendData);
		      						$sendData = array();
		      						$sendCounter = '0';
		      					}
		      				}
		      				
		      				
		      			} 
		      			$key++;
		      			
		      		}else {
		      			
		      			if ($sendCounter < $batchCount && $sendCounter != '0'){
		      				//echo 'NO-CREDIT='.$sendCounter;
		      				$insertSendTable= $voiceDb->insertMulti("$sendVoice", $sendData);
		      				$insertSentTable = $voiceDb->insertMulti($sentVoice, $sendData);
		      				unset($sendData);
		      				$sendData = array();
		      				$sendCounter = '0';
		      			}
		      			
	      					$destination = $data[0];
	      					$dataOutgoing = Array ( "user_id" => "$userId",
	      							"voice_file" => "$voice",
	      							"sender" => "$sender",
	      							"receiver" => "$destination",
	      							"time" => "$timestamp",
	      							"route" => "$router",
	      							"campaign_id" => "$id",
	      							"context" => "$context",
	      							"priority" => "$voicePriority",
	      							"duration" => "$callDuration",
	      							"dlr_url" => "$dlr_url",
	      							"dlrdata" => "NOCREDIT",
	      							"status" => '1',
	      					);
	      					
							$insert = $voiceDb->insert($sentVoice, $dataOutgoing);
														
		      		}
		      	}else {
	      					$destination = $data[0];
	      					$dataOutgoing = Array ( "user_id" => "$userId",
	      							"voice_file" => "$voice",
	      							"sender" => "$sender",
	      							"receiver" => "$destination",
	      							"time" => "$timestamp",
	      							"route" => "$router",
	      							"campaign_id" => "$id",
	      							"context" => "$context",
	      							"priority" => "$voicePriority",
	      							"duration" => "$callDuration",
	      							"dlr_url" => "$dlr_url",
	      							"dlrdata" => "BLOCKED",
	      							"status" => '1',
	      					);
	      				$voiceDb->insert ($sentVoice, $dataOutgoing);
		      	}   
		}
			fclose($handle);
			if ($sendCounter < $batchCount && $sendCounter != '0'){
				//echo 'LAST='.$sendCounter;
				$insertSendTable= $voiceDb->insertMulti("$sentVoice", $sendData);
				$insertSendTable= $voiceDb->insertMulti("$sendVoice", $sendData);
				unset($sendData);
				$sendData = array();
				$sendCounter = '0';
			}
			
		}
	}

	$data = Array ('status' => '1','report_status'=>'0');
	$voiceDb->where('id', $id);
	$voiceDb->update ('campaigns_voice', $data);
	
	shell_exec('/bin/echo $(date) -- END-CAMPAIGN-'.$id.' >> /var/log/webpanel/voicecron-final-success-'.$today.'.log');
	
}

?>
