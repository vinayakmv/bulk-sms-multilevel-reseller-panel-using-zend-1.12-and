#!/bin/bash
PID_S=$(ps -ef | grep -v grep | grep /sms/cronJobNumberInit.php)
RET_S=$?
if [ $RET_S -ne 0 ] ; then
        $(/usr/bin/php /var/www/html/webpanel/cronjob/sms/cronJobNumberInit.php < /dev/null > /dev/null &)
fi

PID_V=$(ps -ef | grep -v grep | grep tsmpp.conf)
RET_V=$?
if [ $RET_V -ne 0 ] ; then
        $(/usr/local/sbin/vsmppbox -v 4 /usr/local/sbin/tsmpp.conf &)
fi
