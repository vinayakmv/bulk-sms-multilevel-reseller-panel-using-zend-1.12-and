<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	/**
	 * Define custom class paths.
	 * This includes the extra autoloader for Doctrine.
	 */
	public function _initCustomClassPath()
	{
		require_once APPLICATION_PATH . '/../library/Doctrine/Common/ClassLoader.php';

		$autoloader = Zend_Loader_Autoloader::getInstance();
		$fmmAutoloader = new \Doctrine\Common\ClassLoader('Bisna');
		$autoloader->pushAutoloader(array($fmmAutoloader, 'loadClass'), 'Bisna');
		
		$classloaderUser = new \Doctrine\Common\ClassLoader('modules\user\models', APPLICATION_PATH);
		$classloaderUser->register();
		
		$classloaderSms = new \Doctrine\Common\ClassLoader('modules\sms\models', APPLICATION_PATH);
		$classloaderSms->register();
		
		$classloaderSmpp = new \Doctrine\Common\ClassLoader('modules\smpp\models', APPLICATION_PATH);
		$classloaderSmpp->register();
		
		$classloaderVoice = new \Doctrine\Common\ClassLoader('modules\voice\models', APPLICATION_PATH);
		$classloaderVoice->register();

		$classloaderUrl = new \Doctrine\Common\ClassLoader('modules\url\models', APPLICATION_PATH);
		$classloaderUrl->register();
		
	}
	
	/**
	 * Set up sessions to be saved to the database.
	 * Begin or resume a session.
	 */
	protected function _initSaveSession()
	{

		if (strpos(APPLICATION_ENV, 'setup') !== false)
			return;

		$this->bootstrap('doctrine');

		// We can use the same connection settings for Zend_Db_Adapter too!
		// So less configuration, more flexibility.
		$doctrine = $this->getResource('doctrine');
		$entityManager = $doctrine->getEntityManager('user');
		
		//Register Blob Type
		Doctrine\DBAL\Types\Type::addType('blob', 'Doctrine\DBAL\Types\BlobType');		
		$entityManager->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('BLOB', 'blob');
		
		$entityManager->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('ENUM', 'string');
		
		$dbParameters = $entityManager->getConnection()->getParams();

		// Hack
		// Doctrine uses 'user' instead of 'username' for connection params
		$dbParameters['username'] = $dbParameters['user'];

		$db = Zend_Db::factory('PDO_MYSQL', $dbParameters);
		Zend_Db_Table::setDefaultAdapter($db);

		$config = array(
			'name' => 'sessions',
			'primary' => 'id',
			'modifiedColumn' => 'modified',
			'dataColumn' => 'data',
			'lifetimeColumn' => 'lifetime',
			'db' => $db
			);

		//Tell Zend_Session to use your Save Handler
		$savehandler = new Zend_Session_SaveHandler_DbTable($config);
		$savehandler->setLifetime(1800)->setOverrideLifetime(true);
		Zend_Session::setSaveHandler($savehandler);
		Zend_Session::start();
	}
	
	protected function _initDoctrine2()
	{
		$this->bootstrap('doctrine');
		
	}
	/*
	protected function _initLayout()
	{
		$options = array('layout'     => 'layout',
						 'layoutPath' => '/layouts/views/',
						);
		$layout = Zend_Layout::startMvc($options);
	}
	*/
	
	protected function _initViewHelpers()
	{
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();
		$view->doctype('XHTML1_STRICT');
		$view->headMeta()->appendHttpEquiv('Content-Type', 'text/html;charset=utf-8');
	}
	
	protected function _initRestRoute()	
	{
		$this->bootstrap('Request');
		$front = $this->getResource('FrontController');
		$restRoute = new Zend_Rest_Route($front, array(), array(
				'license' => array('vsmppbox'),
				'api' => array('sms','voice','smpp')
			));
		$front->getRouter()->addRoute('rest', $restRoute);
	}
	
	protected function _initRequest()
	{
		$this->bootstrap('FrontController');
		$front = $this->getResource('FrontController');
		$request = $front->getRequest();
		if (null === $front->getRequest()) {
			$request = new Zend_Controller_Request_Http();
			$front->setRequest($request);
		}
		return $request;
	}
	
	/**
	 * used for handling top-level navigation
	 * @return Zend_Navigation
	 */
	protected function _initNavigation()
	{

		$doctrine = $this->getResource('doctrine');
		$entityManager = $doctrine->getEntityManager('user');
		
		$domain = Smpp_Utility::getFqdn();
		$domainRepo = $entityManager->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
		@$domainLayout = $domainRepo->layout;
		
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		if ($domainLayout != null){
			$layout->setLayout("$domainLayout");
		}
		
		$view = $layout->getView();
		

		if (@$roleId = $_SESSION['Zend_Auth']['storage']->role_id){
			$role = $entityManager->getRepository('modules\user\models\Role')->find($roleId);
			
			$userId = $_SESSION['Zend_Auth']['storage']->id;
			$user = $entityManager->getRepository('modules\user\models\User')->find($userId);
			$user->liveDate = new \DateTime('now');
			$entityManager->persist($user);
			$entityManager->flush();
			
			$resources = $role->resources;
			foreach ($resources as $resource){
				$res[$resource->id] = $resource->resource;
				$name = $resource->name;
				
			}

                        $smppDlrMap = array_search('system/dlrstatus/smpp', $res);
                        $smppDlrView = array_search('system/dlrstatus/smppview', $res);
			$smppRoutePrefix = array_search('smpp/route/prefix', $res);
			$smppRouteSender = array_search('smpp/route/sender', $res);
			$smppRoutePview = array_search('smpp/route/pview', $res);
			$smppRouteSview = array_search('smpp/route/sview', $res);
			$smppSentSms = array_search('smpp/esme/sentsms', $res);
			$smppSentSmsAll = array_search('smpp/esme/sentsmsall', $res);
			$smppEsme = array_search('smpp/esme/esmes', $res);
			$smppAllEsme = array_search('smpp/esme/allesmes', $res);
			$smppSmscList = array_search('smpp/smsc/smscs', $res);
			$smppSmscSentSms = array_search('smpp/smsc/sentsms', $res);
			$smsSmscList = array_search('sms/smsc/smscs', $res);
			$smppRoute = array_search('smpp/route/routes', $res);
			$smsURoute = array_search('sms/route/user', $res);
			$smsDRoute = array_search('sms/route/domain', $res);
			$smsRoute = array_search('sms/route/routes', $res);
			$esmeCreate = array_search('smpp/esme/create', $res);
			$smppSmscCreate = array_search('smpp/smsc/create', $res);
			$smppSearch = array_search('smpp/smsc/search', $res);
			$smsSmscCreate = array_search('sms/smsc/create', $res);
			$smppRouteCreate = array_search('smpp/route/create', $res);
			$smsRouteCreate = array_search('sms/route/create', $res);
			$smscStatus = array_search('smpp/smsc/status', $res);
			$esmeStatus = array_search('smpp/esme/status', $res);
			$userManage = array_search('user/manage/users', $res);			
			$domainList = array_search('user/manage/domains', $res);
			$createReseller = array_search('user/manage/create', $res);
			$createUser = array_search('user/manage/createuser', $res);
			$allUsers = array_search('user/manage/clients', $res);
			$allDomains = array_search('user/manage/resellers', $res);
			$creditTransfer = array_search('user/transaction/index', $res);
			$mainTransfer = array_search('user/transaction/main', $res);
			$creditIn = array_search('user/transaction/in', $res);
			$creditOut = array_search('user/transaction/out', $res);
			$domainPercentage = array_search('user/percentage/domain', $res);
			$userPercentage = array_search('user/percentage/user', $res);
			$smppCreditTransfer = array_search('smpp/transaction/index', $res);
			$smppCreditIn = array_search('smpp/transaction/in', $res);
			$smppCreditOut = array_search('smpp/transaction/out', $res);
			$userRole = array_search('role/manage/user', $res);
			$rolePermission = array_search('role/manage/permission', $res);
			$smsCompose = array_search('sms/campaign/index', $res);
			$smsDynamic = array_search('sms/campaign/dynamic', $res);
			$smsReport = array_search('sms/report/index', $res);
			$smsScheduled = array_search('sms/report/scheduled', $res);
			$smsTemplateRequest = array_search('sms/template/request', $res);
			$smsTemplateApproval = array_search('sms/template/approval', $res);
			$smsTemplateView = array_search('sms/template/view', $res);
			$smsTemplateViewAll = array_search('sms/template/viewall', $res);
			$voiceCompose = array_search('voice/campaign/index', $res);
			$voiceDyCompose = array_search('voice/campaign/dynamic', $res);
			$voiceReport = array_search('voice/report/index', $res);
			$voiceScheduled = array_search('voice/report/scheduled', $res);
			$dlrStatus = array_search('system/dlrstatus/index', $res);
			$dlrStatusView = array_search('system/dlrstatus/view', $res);
			$smsSenderIdRequest = array_search('sms/senderid/request', $res);
			$smsSenderIdView = array_search('sms/senderid/view', $res);
			$smsSenderIdApproval = array_search('sms/senderid/approval', $res);
			$smsSenderIdViewAll = array_search('sms/senderid/viewall', $res);
			$smsReportsAll = array_search('sms/report/all', $res);
			
			$voiceSenderIdRequest = array_search('voice/senderid/request', $res);
			$voiceSenderIdView = array_search('voice/senderid/view', $res);
			$voiceSenderIdApproval = array_search('voice/senderid/approval', $res);
			$voiceSenderIdViewAll = array_search('voice/senderid/viewall', $res);
			
			$smsUsage = array_search('user/manage/usage', $res);
			$smsRecredit = array_search('user/recredit/index', $res);
			$smsRecreditViewAll = array_search('user/recredit/viewall', $res);
			
			$contactCreate = array_search('user/contact/create', $res);
			$contactView = array_search('user/contact/index', $res);
			$contactDetailView = array_search('user/contact/view', $res);
			
			$smppSenderIdRequest = array_search('smpp/senderid/request', $res);
			$smppSentLog = array_search('smpp/esme/report', $res);
			$smppSentLogAll = array_search('smpp/esme/reportall', $res);
			$smppSenderIdView = array_search('smpp/senderid/view', $res);
			$smppSenderIdApproval = array_search('smpp/senderid/approval', $res);
			$smppSenderIdViewAll = array_search('smpp/senderid/viewall', $res);
			$smppTemplateRequest = array_search('smpp/template/request', $res);
			$smppTemplateApproval = array_search('smpp/template/approval', $res);
			$smppTemplateView = array_search('smpp/template/view', $res);
			$smppTemplateViewAll = array_search('smpp/template/viewall', $res);
			$systemLicenseCreate = array_search('system/license/create', $res);
			$systemLicenseView = array_search('system/license/view', $res);
			$systemLicenseViewAll = array_search('system/license/viewall', $res);
			$smsApiManagement = array_search('user/dev/index', $res);
			$smsApiRoute = array_search('user/dev/route', $res);
			$smsApiDocumentation = array_search('user/dev/smsdoc', $res);
			$voiceApiDocumentation = array_search('user/dev/voicedoc', $res);
			
			$smsSpamAdd = array_search('sms/spam/add', $res);
			$smsSpamView = array_search('sms/spam/view', $res);
			$smsSpamApprove = array_search('sms/spam/approval', $res);
			$smsSpamAssign = array_search('sms/spam/assign', $res);
			$smsSmscSearch = array_search('sms/smsc/search', $res);
			
			$webProfile = array_search('domain/manage/index', $res);
			$userProfile = array_search('user/profile/index', $res);

			$urlCreate = array_search('url/manage/create', $res);
			$urlView = array_search('url/manage/view', $res);
			$urlReport = array_search('url/report/index', $res);
                        $urlFormCreate = array_search('url/manage/createform', $res);
                        $urlFormView = array_search('url/manage/viewform', $res);
                        
                        
                        $rerouteCreate = array_search('smpp/route/reroute', $res);
                        $rerouteView = array_search('smpp/route/review', $res);
			
                        $spamSmppAdd = array_search('smpp/spam/add', $res);
                        $spamSmppView = array_search('smpp/spam/view', $res);
                        
			// Create the config object
			$config = new Zend_Config(array(), true);
			$config->setExtend('development', 'production');
			$config->nav = array();

			$config->nav->Dashboard = array();
			$config->nav->Dashboard->label = 'Dashboard';
			$config->nav->Dashboard->visible = '1';
			$config->nav->Dashboard->class = 'ti ti-dashboard';
			$config->nav->Dashboard->uri = '/user/dashboard';
				
			if ($smsCompose){
				$config->nav->SmsBroadcast = array();
				$config->nav->SmsBroadcast->label = 'Sms';
				$config->nav->SmsBroadcast->visible = '1';
				$config->nav->SmsBroadcast->class = 'ti ti-comment';
				$config->nav->SmsBroadcast->uri = '';
				$config->nav->SmsBroadcast->pages = array();
				if ($smsCompose){
					$config->nav->SmsBroadcast->pages->smsCompose = array();
					$config->nav->SmsBroadcast->pages->smsCompose->label = 'Quick';
					$config->nav->SmsBroadcast->pages->smsCompose->uri = '/sms/campaign/index';
					$config->nav->SmsBroadcast->pages->smsCompose->class = '';
				}
				if ($smsDynamic){
					$config->nav->SmsBroadcast->pages->smsDynamic = array();
					$config->nav->SmsBroadcast->pages->smsDynamic->label = 'Dynamic';
					$config->nav->SmsBroadcast->pages->smsDynamic->uri = '/sms/campaign/dynamic';
					$config->nav->SmsBroadcast->pages->smsDynamic->class = '';
				}
				if ($smsScheduled){
					$config->nav->SmsBroadcast->pages->smsScheduled = array();
					$config->nav->SmsBroadcast->pages->smsScheduled->label = 'Scheduled';
					$config->nav->SmsBroadcast->pages->smsScheduled->uri = '/sms/report/scheduled';
					$config->nav->SmsBroadcast->pages->smsScheduled->class = '';
				}
				if ($smsReport){
					$config->nav->SmsBroadcast->pages->smsReport = array();
					$config->nav->SmsBroadcast->pages->smsReport->label = 'Sent';
					$config->nav->SmsBroadcast->pages->smsReport->uri = '/sms/report/index';
					$config->nav->SmsBroadcast->pages->smsReport->class = '';
				}				
				if ($smsReportsAll){
					$config->nav->SmsBroadcast->pages->smsReportAll = array();
					$config->nav->SmsBroadcast->pages->smsReportAll->label = 'All Sent';
					$config->nav->SmsBroadcast->pages->smsReportAll->uri = '/sms/report/all';
					$config->nav->SmsBroadcast->pages->smsReportAll->class = '';
				}
				if ($smsSmscSearch){
					$config->nav->SmsBroadcast->pages->smsSearch = array();
					$config->nav->SmsBroadcast->pages->smsSearch->label = 'Search Sms';
					$config->nav->SmsBroadcast->pages->smsSearch->uri = '/sms/smsc/search';
					$config->nav->SmsBroadcast->pages->smsSearch->class = '';
				}
				if ($smsUsage){
					$config->nav->SmsBroadcast->pages->usage = array();
					$config->nav->SmsBroadcast->pages->usage->label = 'User Usage';
					$config->nav->SmsBroadcast->pages->usage->uri = '/user/manage/usage';
					$config->nav->SmsBroadcast->pages->usage->class = '';
				}
			}
			
			if ($voiceCompose){
				$config->nav->VoiceBroadcast = array();
				$config->nav->VoiceBroadcast->label = 'Voice';
				$config->nav->VoiceBroadcast->visible = '1';
				$config->nav->VoiceBroadcast->class = 'ti ti-announcement';
				$config->nav->VoiceBroadcast->uri = '';
				$config->nav->VoiceBroadcast->pages = array();
				if ($voiceCompose){
					$config->nav->VoiceBroadcast->pages->voiceCompose = array();
					$config->nav->VoiceBroadcast->pages->voiceCompose->label = 'Quick';
					$config->nav->VoiceBroadcast->pages->voiceCompose->uri = '/voice/campaign/index';
					$config->nav->VoiceBroadcast->pages->voiceCompose->class = '';
				}
				if ($voiceDyCompose){
					$config->nav->VoiceBroadcast->pages->voiceDynamic = array();
					$config->nav->VoiceBroadcast->pages->voiceDynamic->label = 'Dynamic';
					$config->nav->VoiceBroadcast->pages->voiceDynamic->uri = '/voice/campaign/dynamic';
					$config->nav->VoiceBroadcast->pages->voiceDynamic->class = '';
				}
				if ($voiceScheduled){
					$config->nav->VoiceBroadcast->pages->voiceScheduled = array();
					$config->nav->VoiceBroadcast->pages->voiceScheduled->label = 'Scheduled';
					$config->nav->VoiceBroadcast->pages->voiceScheduled->uri = '/voice/report/scheduled';
					$config->nav->VoiceBroadcast->pages->voiceScheduled->class = '';
				}
				if ($voiceReport){
					$config->nav->VoiceBroadcast->pages->voiceReport = array();
					$config->nav->VoiceBroadcast->pages->voiceReport->label = 'Sent';
					$config->nav->VoiceBroadcast->pages->voiceReport->uri = '/voice/report/index';
					$config->nav->VoiceBroadcast->pages->voiceReport->class = '';
				}
				
			}

			if (@$smppSentSms || @$smscCreate || @$esmeCreate || @$smppSmsc || @$smppEsme){
				$config->nav->Smpp = array();
				$config->nav->Smpp->label = 'Smpp';
				$config->nav->Smpp->visible = '1';
				$config->nav->Smpp->class = 'ti ti-comments';
				$config->nav->Smpp->uri = '';
				$config->nav->Smpp->pages = array();
				
				if (@$esmeCreate){
					$config->nav->Smpp->pages->cesme = array();
					$config->nav->Smpp->pages->cesme->label = 'Create';
					$config->nav->Smpp->pages->cesme->uri = '/smpp/esme/create';
					$config->nav->Smpp->pages->cesme->class = '';
				}
				if (@$smppSentLog){
					$config->nav->Smpp->pages->sentlog = array();
					$config->nav->Smpp->pages->sentlog->label = 'Sent';
					$config->nav->Smpp->pages->sentlog->uri = '/smpp/esme/report';
					$config->nav->Smpp->pages->sentlog->class = '';
				}

				if (@$smppSentLogAll){
					$config->nav->Smpp->pages->sentlogall = array();
					$config->nav->Smpp->pages->sentlogall->label = 'All Sent';
					$config->nav->Smpp->pages->sentlogall->uri = '/smpp/esme/reportall';
					$config->nav->Smpp->pages->sentlogall->class = '';
				}
				
				if (@$smppSearch){
					$config->nav->Smpp->pages->search = array();
					$config->nav->Smpp->pages->search->label = 'Search Sms';
					$config->nav->Smpp->pages->search->uri = '/smpp/smsc/search';
					$config->nav->Smpp->pages->search->class = '';
				}
								
				if (@$smppEsme){
					$config->nav->Smpp->pages->esmels = array();
					$config->nav->Smpp->pages->esmels->label = 'Esmes';
					$config->nav->Smpp->pages->esmels->uri = '/smpp/esme/esmes';
					$config->nav->Smpp->pages->esmels->class = '';
				}
				if (@$smppAllEsme){
					$config->nav->Smpp->pages->allesmels = array();
					$config->nav->Smpp->pages->allesmels->label = 'All Esmes';
					$config->nav->Smpp->pages->allesmels->uri = '/smpp/esme/allesmes';
					$config->nav->Smpp->pages->allesmels->class = '';
				}
				
				
			}

			if ($urlCreate){
				$config->nav->UrlManage = array();
				$config->nav->UrlManage->label = 'My Link';
				$config->nav->UrlManage->visible = '1';
				$config->nav->UrlManage->class = 'ti ti-link';
				$config->nav->UrlManage->uri = '';
				$config->nav->UrlManage->pages = array();
				if ($urlCreate){
					$config->nav->UrlManage->pages->urlCreate = array();
					$config->nav->UrlManage->pages->urlCreate->label = 'Create';
					$config->nav->UrlManage->pages->urlCreate->uri = '/url/manage/create';
					$config->nav->UrlManage->pages->urlCreate->class = 'fa fa-external-link';
				}
                                
                                if ($urlFormCreate){
					$config->nav->UrlManage->pages->urlCreateForm = array();
					$config->nav->UrlManage->pages->urlCreateForm->label = 'Create Form';
					$config->nav->UrlManage->pages->urlCreateForm->uri = '/url/manage/createform';
					$config->nav->UrlManage->pages->urlCreateForm->class = 'fa fa-external-link';
				}        
				if ($urlView){
					$config->nav->UrlManage->pages->urlVew = array();
					$config->nav->UrlManage->pages->urlVew->label = 'View';
					$config->nav->UrlManage->pages->urlVew->uri = '/url/manage/view';
					$config->nav->UrlManage->pages->urlVew->class = 'fa fa-external-link';
				}
                                if ($urlFormView){
					$config->nav->UrlManage->pages->FormView = array();
					$config->nav->UrlManage->pages->FormView->label = 'Form View';
					$config->nav->UrlManage->pages->FormView->uri = '/url/manage/viewform';
					$config->nav->UrlManage->pages->FormView->class = 'fa fa-external-link';
				}
				if ($urlReport){
					$config->nav->UrlManage->pages->urlReport = array();
					$config->nav->UrlManage->pages->urlReport->label = 'Reports';
					$config->nav->UrlManage->pages->urlReport->uri = '/url/report/index';
					$config->nav->UrlManage->pages->urlReport->class = 'fa fa-external-link';
				}
				
			}


			
			if ($smsSpamView || $smsSpamAdd){
				$config->nav->SmsSpam = array();
				$config->nav->SmsSpam->label = 'Spam';
				$config->nav->SmsSpam->visible = '1';
				$config->nav->SmsSpam->class = 'ti ti-shield';
				$config->nav->SmsSpam->uri = '';
				$config->nav->SmsSpam->pages = array();
				if ($smsSpamAdd){
					$config->nav->SmsSpam->pages->smsTemplateRequest = array();
					$config->nav->SmsSpam->pages->smsTemplateRequest->label = 'Sms Add';
					$config->nav->SmsSpam->pages->smsTemplateRequest->uri = '/sms/spam/add';
					$config->nav->SmsSpam->pages->smsTemplateRequest->class = '';
				}
                                
				if ($smsSpamView){
					$config->nav->SmsSpam->pages->smsTemplateView = array();
					$config->nav->SmsSpam->pages->smsTemplateView->label = 'Sms Keywords';
					$config->nav->SmsSpam->pages->smsTemplateView->uri = '/sms/spam/view';
					$config->nav->SmsSpam->pages->smsTemplateView->class = '';
				}
                                
				if ($smsSpamApprove){
					$config->nav->SmsSpam->pages->smsSpamApprove = array();
					$config->nav->SmsSpam->pages->smsSpamApprove->label = 'Sms Campaign';
					$config->nav->SmsSpam->pages->smsSpamApprove->uri = '/sms/spam/approval';
					$config->nav->SmsSpam->pages->smsSpamApprove->class = '';
				}
				if ($smsSpamAssign){
					$config->nav->SmsSpam->pages->smsTemplateViewAll = array();
					$config->nav->SmsSpam->pages->smsTemplateViewAll->label = 'Sms Assign';
					$config->nav->SmsSpam->pages->smsTemplateViewAll->uri = '/sms/spam/assign';
					$config->nav->SmsSpam->pages->smsTemplateViewAll->class = '';
				}
                                if ($spamSmppAdd){
					$config->nav->SmsSpam->pages->smppSpamRequest = array();
					$config->nav->SmsSpam->pages->smppSpamRequest->label = 'Smpp Add';
					$config->nav->SmsSpam->pages->smppSpamRequest->uri = '/smpp/spam/add';
					$config->nav->SmsSpam->pages->smppSpamRequest->class = '';
				}
                                if ($spamSmppView){
					$config->nav->SmsSpam->pages->smppKeywords = array();
					$config->nav->SmsSpam->pages->smppKeywords->label = 'Smpp Keywords';
					$config->nav->SmsSpam->pages->smppKeywords->uri = '/smpp/spam/view';
					$config->nav->SmsSpam->pages->smppKeywords->class = '';
				}
			}
			
			if ($smsTemplateView || $smppTemplateView){
				$config->nav->SmsTemplate = array();
				$config->nav->SmsTemplate->label = 'Templates';
				$config->nav->SmsTemplate->visible = '1';
				$config->nav->SmsTemplate->class = 'ti ti-layers-alt';
				$config->nav->SmsTemplate->uri = '';
				$config->nav->SmsTemplate->pages = array();
				if ($smsTemplateRequest){
					$config->nav->SmsTemplate->pages->smsTemplateRequest = array();
					$config->nav->SmsTemplate->pages->smsTemplateRequest->label = 'Sms Request';
					$config->nav->SmsTemplate->pages->smsTemplateRequest->uri = '/sms/template/request';
					$config->nav->SmsTemplate->pages->smsTemplateRequest->class = '';
				}
				if ($smsTemplateApproval){
					$config->nav->SmsTemplate->pages->smsTemplateApproval = array();
					$config->nav->SmsTemplate->pages->smsTemplateApproval->label = 'Sms Approval';
					$config->nav->SmsTemplate->pages->smsTemplateApproval->uri = '/sms/template/approval';
					$config->nav->SmsTemplate->pages->smsTemplateApproval->class = '';
				}
				if ($smsTemplateView){
					$config->nav->SmsTemplate->pages->smsTemplateView = array();
					$config->nav->SmsTemplate->pages->smsTemplateView->label = 'Sms Templates';
					$config->nav->SmsTemplate->pages->smsTemplateView->uri = '/sms/template/view';
					$config->nav->SmsTemplate->pages->smsTemplateView->class = '';
				}
				if ($smsTemplateViewAll){
					$config->nav->SmsTemplate->pages->smsTemplateViewAll = array();
					$config->nav->SmsTemplate->pages->smsTemplateViewAll->label = 'All Sms Templates';
					$config->nav->SmsTemplate->pages->smsTemplateViewAll->uri = '/sms/template/viewall';
					$config->nav->SmsTemplate->pages->smsTemplateViewAll->class = '';
				}
				
				if (@$smppTemplateRequest){
					$config->nav->SmsTemplate->pages->smppTemplateRequest = array();
					$config->nav->SmsTemplate->pages->smppTemplateRequest->label = 'Smpp Request';
					$config->nav->SmsTemplate->pages->smppTemplateRequest->uri = '/smpp/template/request';
					$config->nav->SmsTemplate->pages->smppTemplateRequest->class = '';
				}
				if (@$smppTemplateApproval){
					$config->nav->SmsTemplate->pages->smppTemplateApproval = array();
					$config->nav->SmsTemplate->pages->smppTemplateApproval->label = 'Smpp Approval';
					$config->nav->SmsTemplate->pages->smppTemplateApproval->uri = '/smpp/template/approval';
					$config->nav->SmsTemplate->pages->smppTemplateApproval->class = '';
				}
				if (@$smppTemplateView){
					$config->nav->SmsTemplate->pages->smppTemplateView = array();
					$config->nav->SmsTemplate->pages->smppTemplateView->label = 'Smpp Templates';
					$config->nav->SmsTemplate->pages->smppTemplateView->uri = '/smpp/template/view';
					$config->nav->SmsTemplate->pages->smppTemplateView->class = '';
				}
				if (@$smppTemplateViewAll){
					$config->nav->SmsTemplate->pages->smppTemplateViewAll = array();
					$config->nav->SmsTemplate->pages->smppTemplateViewAll->label = 'All Smpp Templates';
					$config->nav->SmsTemplate->pages->smppTemplateViewAll->uri = '/smpp/template/viewall';
					$config->nav->SmsTemplate->pages->smppTemplateViewAll->class = '';
				}
			}
				
			if ($smsSenderIdView || $smppSenderIdView || $voiceSenderIdView){
				$config->nav->SenderID = array();
				$config->nav->SenderID->label = 'Senders';
				$config->nav->SenderID->visible = '1';
				$config->nav->SenderID->class = 'ti ti-location-arrow';
				$config->nav->SenderID->uri = '';
				$config->nav->SenderID->pages = array();
				if ($smsSenderIdRequest){
					$config->nav->SenderID->pages->smsSenderIdRequest = array();
					$config->nav->SenderID->pages->smsSenderIdRequest->label = 'Sms Request';
					$config->nav->SenderID->pages->smsSenderIdRequest->uri = '/sms/senderid/request';
					$config->nav->SenderID->pages->smsSenderIdRequest->class = '';
				}
				if ($smsSenderIdApproval){
					$config->nav->SenderID->pages->SmsSenderIdApproval = array();
					$config->nav->SenderID->pages->SmsSenderIdApproval->label = 'Sms Approval';
					$config->nav->SenderID->pages->SmsSenderIdApproval->uri = '/sms/senderid/approval';
					$config->nav->SenderID->pages->SmsSenderIdApproval->class = '';
				}
				if ($smsSenderIdView){
					$config->nav->SenderID->pages->smsSenderIdView = array();
					$config->nav->SenderID->pages->smsSenderIdView->label = 'Sms Senders';
					$config->nav->SenderID->pages->smsSenderIdView->uri = '/sms/senderid/view';
					$config->nav->SenderID->pages->smsSenderIdView->class = '';
				}
				if ($smsSenderIdViewAll){
					$config->nav->SenderID->pages->smsSenderIdViewAll = array();
					$config->nav->SenderID->pages->smsSenderIdViewAll->label = 'All Sms Senders';
					$config->nav->SenderID->pages->smsSenderIdViewAll->uri = '/sms/senderid/viewall';
					$config->nav->SenderID->pages->smsSenderIdViewAll->class = '';
				}
				
				if ($voiceSenderIdRequest){
					$config->nav->SenderID->pages->voiceSenderIdRequest = array();
					$config->nav->SenderID->pages->voiceSenderIdRequest->label = 'Voice Request';
					$config->nav->SenderID->pages->voiceSenderIdRequest->uri = '/voice/senderid/request';
					$config->nav->SenderID->pages->voiceSenderIdRequest->class = '';
				}
				if ($voiceSenderIdApproval){
					$config->nav->SenderID->pages->voiceSenderIdApproval = array();
					$config->nav->SenderID->pages->voiceSenderIdApproval->label = 'Voice Approval';
					$config->nav->SenderID->pages->voiceSenderIdApproval->uri = '/voice/senderid/approval';
					$config->nav->SenderID->pages->voiceSenderIdApproval->class = '';
				}
				if ($voiceSenderIdView){
					$config->nav->SenderID->pages->voiceSenderIdView = array();
					$config->nav->SenderID->pages->voiceSenderIdView->label = 'Voice Senders';
					$config->nav->SenderID->pages->voiceSenderIdView->uri = '/voice/senderid/view';
					$config->nav->SenderID->pages->voiceSenderIdView->class = '';
				}
				if ($voiceSenderIdViewAll){
					$config->nav->SenderID->pages->voiceSenderIdViewAll = array();
					$config->nav->SenderID->pages->voiceSenderIdViewAll->label = 'All Voice Senders';
					$config->nav->SenderID->pages->voiceSenderIdViewAll->uri = '/voice/senderid/viewall';
					$config->nav->SenderID->pages->voiceSenderIdViewAll->class = '';
				}
				
				if (@$smppSenderIdRequest){
					$config->nav->SenderID->pages->smppSenderIdRequest = array();
					$config->nav->SenderID->pages->smppSenderIdRequest->label = 'Smpp Request';
					$config->nav->SenderID->pages->smppSenderIdRequest->uri = '/smpp/senderid/request';
					$config->nav->SenderID->pages->smppSenderIdRequest->class = '';
				}
				if (@$smppSenderIdApproval){
					$config->nav->SenderID->pages->smppSenderIdApproval = array();
					$config->nav->SenderID->pages->smppSenderIdApproval->label = 'Smpp Approval';
					$config->nav->SenderID->pages->smppSenderIdApproval->uri = '/smpp/senderid/approval';
					$config->nav->SenderID->pages->smppSenderIdApproval->class = '';
				}
				if (@$smppSenderIdView){
					$config->nav->SenderID->pages->smppSenderIdView = array();
					$config->nav->SenderID->pages->smppSenderIdView->label = 'Smpp Senders';
					$config->nav->SenderID->pages->smppSenderIdView->uri = '/smpp/senderid/view';
					$config->nav->SenderID->pages->smppSenderIdView->class = '';
				}
				if (@$smppSenderIdViewAll){
					$config->nav->SenderID->pages->smppSenderIdViewAll = array();
					$config->nav->SenderID->pages->smppSenderIdViewAll->label = 'All Smpp Senders';
					$config->nav->SenderID->pages->smppSenderIdViewAll->uri = '/smpp/senderid/viewall';
					$config->nav->SenderID->pages->smppSenderIdViewAll->class = '';
				}
			}
			
			if ($contactCreate || $contactView){
				$config->nav->Contact = array();
				$config->nav->Contact->label = 'Address Book';
				$config->nav->Contact->visible = '1';
				$config->nav->Contact->class = 'ti ti-agenda';
				$config->nav->Contact->uri = '';
				$config->nav->Contact->pages = array();
			
				if ($contactCreate){
					$config->nav->Contact->pages->ApiManagement = array();
					$config->nav->Contact->pages->ApiManagement->label = 'Create';
					$config->nav->Contact->pages->ApiManagement->uri = '/user/contact/create';
					$config->nav->Contact->pages->ApiManagement->class = '';
				}
				
				if ($contactView){
					$config->nav->Contact->pages->ApiRoute = array();
					$config->nav->Contact->pages->ApiRoute->label = 'Groups';
					$config->nav->Contact->pages->ApiRoute->uri = '/user/contact/index';
					$config->nav->Contact->pages->ApiRoute->class = '';
				}
			}
			
			if ($userManage || $domainList || $createReseller || $createUser || $allUsers || $allDomains){
				$config->nav->User = array();
				$config->nav->User->label = 'Users';
				$config->nav->User->visible = '1';
				$config->nav->User->class = 'ti ti-user';
				$config->nav->User->uri = '';
				$config->nav->User->pages = array();
			
				
				if ($userManage){
					$config->nav->User->pages->users = array();
					$config->nav->User->pages->users->label = 'Users';
					$config->nav->User->pages->users->uri = '/user/manage/users';
					$config->nav->User->pages->users->class = '';
				}
			
				if ($domainList){
					$config->nav->User->pages->domains = array();
					$config->nav->User->pages->domains->label = 'Domains';
					$config->nav->User->pages->domains->uri = '/user/manage/domains';
					$config->nav->User->pages->domains->class = '';
				}
			
				if ($createReseller){
					$config->nav->User->pages->reseller = array();
					$config->nav->User->pages->reseller->label = 'Create Reseller';
					$config->nav->User->pages->reseller->uri = '/user/manage/create';
					$config->nav->User->pages->reseller->class = '';
				}
			
				if ($createUser){
					$config->nav->User->pages->createuser = array();
					$config->nav->User->pages->createuser->label = 'Create User';
					$config->nav->User->pages->createuser->uri = '/user/manage/createuser';
					$config->nav->User->pages->createuser->class = '';
				}
			
				if ($allUsers){
					$config->nav->User->pages->allusers = array();
					$config->nav->User->pages->allusers->label = 'All Users';
					$config->nav->User->pages->allusers->uri = '/user/manage/clients';
					$config->nav->User->pages->allusers->class = '';
				}
			
				if ($allDomains){
					$config->nav->User->pages->alldomains = array();
					$config->nav->User->pages->alldomains->label = 'All Domains';
					$config->nav->User->pages->alldomains->uri = '/user/manage/resellers';
					$config->nav->User->pages->alldomains->class = '';
				}
			
			}
			
			if ($webProfile){
				$config->nav->ProfileManage = array();
				$config->nav->ProfileManage->label = 'Profile';
				$config->nav->ProfileManage->visible = '1';
				$config->nav->ProfileManage->class = 'ti ti-id-badge';
				$config->nav->ProfileManage->uri = '';
				$config->nav->ProfileManage->pages = array();
				if ($webProfile){
					$config->nav->ProfileManage->pages->webprofile = array();
					$config->nav->ProfileManage->pages->webprofile->label = 'Domain';
					$config->nav->ProfileManage->pages->webprofile->uri = '/domain/manage/index';
					$config->nav->ProfileManage->pages->webprofile->class = '';
				}
			
				if ($userProfile){
					$config->nav->ProfileManage->pages->userprofile = array();
					$config->nav->ProfileManage->pages->userprofile->label = 'User';
					$config->nav->ProfileManage->pages->userprofile->uri = '/user/profile/index';
					$config->nav->ProfileManage->pages->userprofile->class = '';
				}
			}
			
			if ($mainTransfer || $creditTransfer || $creditIn || $creditOut || $smppCreditTransfer || $smppCreditLog){
				$config->nav->Credit = array();
				$config->nav->Credit->label = 'Transactions';
				$config->nav->Credit->visible = '1';
				$config->nav->Credit->class = 'ti ti-credit-card';
				$config->nav->Credit->uri = '';
				$config->nav->Credit->pages = array();
				if ($mainTransfer){
					$config->nav->Credit->pages->mainfile = array();
					$config->nav->Credit->pages->mainfile->label = 'Main Transfer';
					$config->nav->Credit->pages->mainfile->uri = '/user/transaction/main';
					$config->nav->Credit->pages->mainfile->class = '';
				}
				if ($creditTransfer){
					$config->nav->Credit->pages->voicefile = array();
					$config->nav->Credit->pages->voicefile->label = 'User Transfer';
					$config->nav->Credit->pages->voicefile->uri = '/user/transaction/index';
					$config->nav->Credit->pages->voicefile->class = '';
				}
				if ($smppCreditTransfer){
					$config->nav->Credit->pages->smppCredit = array();
					$config->nav->Credit->pages->smppCredit->label = 'Smpp Transfer';
					$config->nav->Credit->pages->smppCredit->uri = '/smpp/transaction/index';
					$config->nav->Credit->pages->smppCredit->class = '';
				}
				if ($smsRecredit){
					$config->nav->Credit->pages->smsreCredit = array();
					$config->nav->Credit->pages->smsreCredit->label = 'Sms Recredit';
					$config->nav->Credit->pages->smsreCredit->uri = '/user/recredit/index';
					$config->nav->Credit->pages->smsreCredit->class = '';
				}
				if (@$smppSentSms){
					$config->nav->Credit->pages->sent = array();
					$config->nav->Credit->pages->sent->label = 'Smpp Recredit';
					$config->nav->Credit->pages->sent->uri = '/smpp/esme/sentsms';
					$config->nav->Credit->pages->sent->class = '';
				}
				
				if ($smsRecreditViewAll){
					$config->nav->Credit->pages->smsreCreditAll = array();
					$config->nav->Credit->pages->smsreCreditAll->label = 'All Sms Recredit';
					$config->nav->Credit->pages->smsreCreditAll->uri = '/user/recredit/viewall';
					$config->nav->Credit->pages->smsreCreditAll->class = '';
				}
				if (@$smppSentSmsAll){
					$config->nav->Credit->pages->sentall = array();
					$config->nav->Credit->pages->sentall->label = 'All Smpp Recredit';
					$config->nav->Credit->pages->sentall->uri = '/smpp/esme/sentsmsall';
					$config->nav->Credit->pages->sentall->class = '';
				}
				if ($creditIn){
					$config->nav->Credit->pages->userin = array();
					$config->nav->Credit->pages->userin->label = 'Credits In';
					$config->nav->Credit->pages->userin->uri = '/user/transaction/in';
					$config->nav->Credit->pages->userin->class = '';
				}
				if ($creditOut){
					$config->nav->Credit->pages->userout = array();
					$config->nav->Credit->pages->userout->label = 'Credits Out';
					$config->nav->Credit->pages->userout->uri = '/user/transaction/out';
					$config->nav->Credit->pages->userout->class = '';
				}
				if ($smppCreditIn){
					$config->nav->Credit->pages->smppIn = array();
					$config->nav->Credit->pages->smppIn->label = 'Smpp In';
					$config->nav->Credit->pages->smppIn->uri = '/smpp/transaction/in';
					$config->nav->Credit->pages->smppIn->class = '';
				}
				if ($smppCreditOut){
					$config->nav->Credit->pages->smppOut = array();
					$config->nav->Credit->pages->smppOut->label = 'Smpp Out';
					$config->nav->Credit->pages->smppOut->uri = '/smpp/transaction/out';
					$config->nav->Credit->pages->smppOut->class = '';
				}
			}
			
			if ($userPercentage || $domainPercentage){
				$config->nav->Percentage = array();
				$config->nav->Percentage->label = 'Percentage';
				$config->nav->Percentage->visible = '1';
				$config->nav->Percentage->class = 'ti ti-bar-chart';
				$config->nav->Percentage->uri = '';
				$config->nav->Percentage->pages = array();
				if ($userPercentage){
					$config->nav->Percentage->pages->voiceCompose = array();
					$config->nav->Percentage->pages->voiceCompose->label = 'User';
					$config->nav->Percentage->pages->voiceCompose->uri = '/user/percentage/user';
					$config->nav->Percentage->pages->voiceCompose->class = '';
				}
				if ($domainPercentage){
					$config->nav->Percentage->pages->voiceReport = array();
					$config->nav->Percentage->pages->voiceReport->label = 'Domain';
					$config->nav->Percentage->pages->voiceReport->uri = '/user/percentage/domain';
					$config->nav->Percentage->pages->voiceReport->class = '';
				}
			}
			
			if ($smppRouteCreate || $smsRouteCreate || $smppRoute || $smsRoute || $smsURoute || $smsDRoute){
				$config->nav->Route = array();
				$config->nav->Route->label = 'Routes';
				$config->nav->Route->visible = '1';
				$config->nav->Route->class = 'ti ti-direction-alt';
				$config->nav->Route->uri = '';
				$config->nav->Route->pages = array();
				if ($smppRouteCreate){
					$config->nav->Route->pages->croute = array();
					$config->nav->Route->pages->croute->label = 'Create Smpp';
					$config->nav->Route->pages->croute->uri = '/smpp/route/create';
					$config->nav->Route->pages->croute->class = '';
				}
				
				if ($smsRouteCreate){
					$config->nav->Route->pages->sroute = array();
					$config->nav->Route->pages->sroute->label = 'Create Sms';
					$config->nav->Route->pages->sroute->uri = '/sms/route/create';
					$config->nav->Route->pages->sroute->class = '';
				}
                                
                                if ($rerouteCreate){
					$config->nav->Route->pages->reroute = array();
					$config->nav->Route->pages->reroute->label = 'Smpp Reroute';
					$config->nav->Route->pages->reroute->uri = '/smpp/route/reroute';
					$config->nav->Route->pages->reroute->class = '';
				}
				
				if ($smppRoute){
					$config->nav->Route->pages->routelsmpp = array();
					$config->nav->Route->pages->routelsmpp->label = 'Smpp Routes';
					$config->nav->Route->pages->routelsmpp->uri = '/smpp/route/routes';
					$config->nav->Route->pages->routelsmpp->class = '';
				}
				if ($smsRoute){
					$config->nav->Route->pages->routelsms = array();
					$config->nav->Route->pages->routelsms->label = 'Sms Routes';
					$config->nav->Route->pages->routelsms->uri = '/sms/route/routes';
					$config->nav->Route->pages->routelsms->class = '';
				}
                                if ($rerouteView){
					$config->nav->Route->pages->reroutelsms = array();
					$config->nav->Route->pages->reroutelsms->label = 'Reroute Routes';
					$config->nav->Route->pages->reroutelsms->uri = '/smpp/route/review';
					$config->nav->Route->pages->reroutelsms->class = '';
				}
				if ($smppRoutePrefix){
					$config->nav->Route->pages->routelsmppprefix = array();
					$config->nav->Route->pages->routelsmppprefix->label = 'Smpp Prefix';
					$config->nav->Route->pages->routelsmppprefix->uri = '/smpp/route/prefix';
					$config->nav->Route->pages->routelsmppprefix->class = '';
				}
				if ($smppRouteSender){
					$config->nav->Route->pages->routelsmssender = array();
					$config->nav->Route->pages->routelsmssender->label = 'Smpp Sender';
					$config->nav->Route->pages->routelsmssender->uri = '/smpp/route/sender';
					$config->nav->Route->pages->routelsmssender->class = '';
				}
				if ($smppRoutePview){
					$config->nav->Route->pages->pview = array();
					$config->nav->Route->pages->pview->label = 'Prefix Routes';
					$config->nav->Route->pages->pview->uri = '/smpp/route/pview';
					$config->nav->Route->pages->pview->class = '';
				}
				
				if ($smppRouteSview){
					$config->nav->Route->pages->sview = array();
					$config->nav->Route->pages->sview->label = 'Sender Routes';
					$config->nav->Route->pages->sview->uri = '/smpp/route/sview';
					$config->nav->Route->pages->sview->class = '';
				}
				if ($smsURoute){
					$config->nav->Route->pages->routeuser = array();
					$config->nav->Route->pages->routeuser->label = 'User Assign';
					$config->nav->Route->pages->routeuser->uri = '/sms/route/user';
					$config->nav->Route->pages->routeuser->class = '';
				}
				if ($smsDRoute){
					$config->nav->Route->pages->routedomain = array();
					$config->nav->Route->pages->routedomain->label = 'Domain Assign';
					$config->nav->Route->pages->routedomain->uri = '/sms/route/domain';
					$config->nav->Route->pages->routedomain->class = '';
				}
			}
			
			if ($smppSmscCreate || $smsSmscCreate || $smppSmscList || $smsSmscList || $smppSmscSentSms){
				$config->nav->Smsc = array();
				$config->nav->Smsc->label = 'Smsc';
				$config->nav->Smsc->visible = '1';
				$config->nav->Smsc->class = 'ti ti-comment-alt';
				$config->nav->Smsc->uri = '';
				$config->nav->Smsc->pages = array();
				if ($smppRouteCreate){
					$config->nav->Smsc->pages->cSmsc = array();
					$config->nav->Smsc->pages->cSmsc->label = 'Create Smpp';
					$config->nav->Smsc->pages->cSmsc->uri = '/smpp/smsc/create';
					$config->nav->Smsc->pages->cSmsc->class = '';
				}
			
				if ($smsSmscCreate){
					$config->nav->Smsc->pages->sSmsc = array();
					$config->nav->Smsc->pages->sSmsc->label = 'Create Sms';
					$config->nav->Smsc->pages->sSmsc->uri = '/sms/smsc/create';
					$config->nav->Smsc->pages->sSmsc->class = '';
				}
				
				if ($smppSmscSentSms){
					$config->nav->Smsc->pages->SmscSentSms = array();
					$config->nav->Smsc->pages->SmscSentSms->label = 'Smsc Recredit';
					$config->nav->Smsc->pages->SmscSentSms->uri = '/smpp/smsc/sentsms';
					$config->nav->Smsc->pages->SmscSentSms->class = '';
				}
				
				if ($smppSmscList){
					$config->nav->Smsc->pages->Smsclsmpp = array();
					$config->nav->Smsc->pages->Smsclsmpp->label = 'Smpp Smscs';
					$config->nav->Smsc->pages->Smsclsmpp->uri = '/smpp/smsc/smscs';
					$config->nav->Smsc->pages->Smsclsmpp->class = '';
				}
				if ($smsSmscList){
					$config->nav->Smsc->pages->Smsclsms = array();
					$config->nav->Smsc->pages->Smsclsms->label = 'Sms Smscs';
					$config->nav->Smsc->pages->Smsclsms->uri = '/sms/smsc/smscs';
					$config->nav->Smsc->pages->Smsclsms->class = '';
				}
				
				
			}
			
					
			if ($smsApiManagement || $smsApiDocumentation || $smsApiRoute){
				$config->nav->Api = array();
				$config->nav->Api->label = 'Api';
				$config->nav->Api->visible = '1';
				$config->nav->Api->class = 'ti ti-plug';
				$config->nav->Api->uri = '';
				$config->nav->Api->pages = array();
			
				if ($smsApiManagement){
					$config->nav->Api->pages->ApiManagement = array();
					$config->nav->Api->pages->ApiManagement->label = 'Key';
					$config->nav->Api->pages->ApiManagement->uri = '/user/dev/index';
					$config->nav->Api->pages->ApiManagement->class = '';
				}
				if ($smsApiRoute){
					$config->nav->Api->pages->ApiRoute = array();
					$config->nav->Api->pages->ApiRoute->label = 'Routes';
					$config->nav->Api->pages->ApiRoute->uri = '/user/dev/route';
					$config->nav->Api->pages->ApiRoute->class = '';
				}
				if ($smsApiDocumentation){
					$config->nav->Api->pages->ApiDocumentation = array();
					$config->nav->Api->pages->ApiDocumentation->label = 'Sms Documentation';
					$config->nav->Api->pages->ApiDocumentation->uri = '/user/dev/smsdoc';
					$config->nav->Api->pages->ApiDocumentation->class = '';
				}
				if ($voiceApiDocumentation){
					$config->nav->Api->pages->VoiceApiDocumentation = array();
					$config->nav->Api->pages->VoiceApiDocumentation->label = 'Voice Documentation';
					$config->nav->Api->pages->VoiceApiDocumentation->uri = '/user/dev/voicedoc';
					$config->nav->Api->pages->VoiceApiDocumentation->class = '';
				}
			}
			
			if ($userRole || $rolePermission){
				$config->nav->Role = array();
				$config->nav->Role->label = 'Roles';
				$config->nav->Role->visible = '1';
				$config->nav->Role->class = 'ti ti-settings';
				$config->nav->Role->uri = '';
				$config->nav->Role->pages = array();
				if ($userRole){
					$config->nav->Role->pages->voicefile = array();
					$config->nav->Role->pages->voicefile->label = 'User';
					$config->nav->Role->pages->voicefile->uri = '/role/manage/user';
					$config->nav->Role->pages->voicefile->class = '';
				}
				if ($rolePermission){
					$config->nav->Role->pages->leadfile = array();
					$config->nav->Role->pages->leadfile->label = 'Permissions';
					$config->nav->Role->pages->leadfile->uri = '/role/manage/permission';
					$config->nav->Role->pages->leadfile->class = '';
				}
			}

			if ($dlrStatus){
				$config->nav->Dlr = array();
				$config->nav->Dlr->label = 'Disposition';
				$config->nav->Dlr->visible = '1';
				$config->nav->Dlr->class = 'ti ti-info-alt';
				$config->nav->Dlr->uri = '';
				$config->nav->Dlr->pages = array();
				if ($dlrStatus){
					$config->nav->Dlr->pages->index = array();
					$config->nav->Dlr->pages->index->label = 'Sms';
					$config->nav->Dlr->pages->index->uri = '/system/dlrstatus/index';
					$config->nav->Dlr->pages->index->class = '';
				}
				
				if ($dlrStatus){
					$config->nav->Dlr->pages->voice = array();
					$config->nav->Dlr->pages->voice->label = 'Voice';
					$config->nav->Dlr->pages->voice->uri = '/system/dlrstatus/index';
					$config->nav->Dlr->pages->voice->class = '';
				}
                                if ($smppDlrMap){
					$config->nav->Dlr->pages->smpp = array();
					$config->nav->Dlr->pages->smpp->label = 'Smpp';
					$config->nav->Dlr->pages->smpp->uri = '/system/dlrstatus/smpp';
					$config->nav->Dlr->pages->smpp->class = '';
				}
				if ($dlrStatusView){
					$config->nav->Dlr->pages->view = array();
					$config->nav->Dlr->pages->view->label = 'Sms List';
					$config->nav->Dlr->pages->view->uri = '/system/dlrstatus/view';
					$config->nav->Dlr->pages->view->class = '';
				}
				if ($dlrStatusView){
					$config->nav->Dlr->pages->voices = array();
					$config->nav->Dlr->pages->voices->label = 'Voice List';
					$config->nav->Dlr->pages->voices->uri = '/system/dlrstatus/voiceview';
					$config->nav->Dlr->pages->voices->class = '';
				}
                                if ($smppDlrView){
					$config->nav->Dlr->pages->smppview = array();
					$config->nav->Dlr->pages->smppview->label = 'Smpp List';
					$config->nav->Dlr->pages->smppview->uri = '/system/dlrstatus/smppview';
					$config->nav->Dlr->pages->smppview->class = '';
				}
			}
			
			if ($smscStatus || $esmeStatus){
				$config->nav->Monitor = array();
				$config->nav->Monitor->label = 'Monitor';
				$config->nav->Monitor->visible = '1';
				$config->nav->Monitor->class = 'ti ti-desktop';
				$config->nav->Monitor->uri = '';
				$config->nav->Monitor->pages = array();
			
				if ($smscStatus){
					$config->nav->Monitor->pages->smscst = array();
					$config->nav->Monitor->pages->smscst->label = 'Smsc Operator';
					$config->nav->Monitor->pages->smscst->uri = '/smpp/smsc/status';
					$config->nav->Monitor->pages->smscst->class = '';
				}

				if ($smscStatus){
					$config->nav->Monitor->pages->smscsts = array();
					$config->nav->Monitor->pages->smscsts->label = 'Smsc Client';
					$config->nav->Monitor->pages->smscsts->uri = '/smpp/smsc/cstatus';
					$config->nav->Monitor->pages->smscsts->class = '';
				}
			
				if ($esmeStatus){
					$config->nav->Monitor->pages->esmest = array();
					$config->nav->Monitor->pages->esmest->label = 'Smpp Server';
					$config->nav->Monitor->pages->esmest->uri = '/smpp/esme/status';
					$config->nav->Monitor->pages->esmest->class = '';
				}
			}
                        
                        if ($smscStatus || $esmeStatus){
				$config->nav->Logger = array();
				$config->nav->Logger->label = 'Live Logs';
				$config->nav->Logger->visible = '1';
				$config->nav->Logger->class = 'ti ti-desktop';
				$config->nav->Logger->uri = '';
				$config->nav->Logger->pages = array();
			
				if ($smscStatus){
					$config->nav->Logger->pages->smscst = array();
					$config->nav->Logger->pages->smscst->label = 'Server Logs';
					$config->nav->Logger->pages->smscst->uri = '/log/logger/index';
					$config->nav->Logger->pages->smscst->class = '';
				}
                                
			}
			


			$writer = new Zend_Config_Writer_Xml(array('config' => $config,
				'filename' => APPLICATION_PATH . '/configs/' . "$roleId.xml"));
			$writer->write();

			//$config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
			$config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/' . "$roleId.xml", 'nav');
			$container = new Zend_Navigation($config);
			$view->navigation($container);			
		}		
/*	
		$acl = Zend_Registry::get('ACL');
		Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($acl);
		Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole(
		Common_Controller_Plugin_Acl::DEFAULT_ROLE
		);
*/

}
}

