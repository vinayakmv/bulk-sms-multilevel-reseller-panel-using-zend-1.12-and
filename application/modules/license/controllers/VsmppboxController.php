<?php

use Doctrine\ORM\Query\AST\EmptyCollectionComparisonExpression;
class License_VsmppboxController extends Zend_Rest_Controller
{  
    public function init()
    {
	  $this->_helper->layout->disableLayout();
	  $bootstrap = $this->getInvokeArg('bootstrap');
	  
	  $options = $bootstrap->getOption('resources');

	  $contextSwitch = $this->_helper->getHelper('contextSwitch');
	  $contextSwitch->addActionContext('index', array('xml','json'))->initContext();
		
    }
	
    /**
     * The index action handles index/list requests; it should respond with a
     * list of the requested resources.
     */ 
    public function indexAction()
    {
   
	  if ($this->_request->isPost()) {
	  	
		$id = $this->_request->getParam('id');
		$auth = $this->_request->getParam('auth');
		$ip = $this->_request->getServer('REMOTE_ADDR');
		if (isset($id,$auth)) {
		    $utility = new Smpp_Utility();
		    $emUser = $utility->getEntityManager('user');		    
		    $apiRepo = $emUser->getRepository('Models\License')->findOneBy(array('serverIp'=>$ip));
		    $userAgent = $this->_request->getHeader('User-Agent');
		    
		    if ($apiRepo) {	
		    		$machineId = $apiRepo->machineId;
		    		$status = $apiRepo->status;
		    		$validity = $apiRepo->validity;
		    		$now = new DateTime("now");
		    	if ($machineId == 'EMPTY' && $status == '1' && $validity >= $now) {
		    		$apiRepo->machineId = $id;
		    		$emUser->persist($apiRepo);
					$emUser->flush();	
									
					$success = md5($auth.'011E99E8');
					$this->view->status = $success;
					$this->getResponse()->setHttpResponseCode(202);
		    	}else {
		    		if ($machineId == $id && $status == '1' && $validity >= $now) {
		    			
		    			$success = md5($auth.'011E99E8');
		    			$this->view->status = $success;
		    			$this->getResponse()->setHttpResponseCode(202);
		    			
		    		}else {
		    			$this->view->status = 'ERR401';
		    			$this->getResponse()->setHttpResponseCode(401);
		    		}
		    	}
		    } else {
				$this->view->status = 'ERR401';
				$this->getResponse()->setHttpResponseCode(401);
		    }
		}

	  } else {
			$this->view->status = 'ERR405';
			$this->getResponse()->setHttpResponseCode(405);
	  }
    }
    
    public function listAction()
    {
        $this->_forward('index');
    }
    
    public function getAction()
    {
		$this->_forward('index');
    }

    public function newAction() 
    {   	
		$this->_forward('index');
    }
	
    public function postAction() 
    {
		$this->_forward('index');
    }

    public function editAction() 
    {    	 
		$this->_forward('index');
    }
	
    public function putAction() 
    {
		$this->_forward('index');
    } 
	
    public function deleteAction() 
    {
		$this->_forward('index');
    }

}

