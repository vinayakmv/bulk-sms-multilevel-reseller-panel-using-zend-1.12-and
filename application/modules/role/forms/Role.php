<?php

class Role_Form_Role extends Zend_Form
{

    public function init()
    {
        $this->setAction('/role/manage/user');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	
		$role = new Zend_Form_Element_Text('roleName');
		$role->setDecorators(array('ViewHelper',
									'Description'));
		$role->setLabel('Role');
		$role->class = "form-control";
		$role->setRequired(false);
			
		//Add Validator
		//$campaign->addValidator(new Zend_Validate_StringLength(4,20));
		//Add Filter
		$role->addFilter(new Zend_Filter_HtmlEntities());
		$role->addFilter(new Zend_Filter_StripTags());
		$role->addFilter(new Zend_Filter_StringToUpper());
		//Add Username Element
		$this->addElement($role);
		
		//Select Role
		$selectRole = new Zend_Form_Element_Select('selectRole');
		$roles = array("1" => "Role One",
					   "2" => "Role Two",
					   "3" => "Role Three");
		$selectRole->addMultiOptions($roles);
		$selectRole->setDecorators(array('ViewHelper',
										'Description'));
		$selectRole->setLabel('Select Role');
		$selectRole->class = "form-control select2 custom-select";
		
		$selectRole->setRequired(false);
		//Add Validator
		//Add Filter
		$selectRole->addFilter(new Zend_Filter_HtmlEntities());
		$selectRole->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectRole);
		
		$selectUser = new Zend_Form_Element_Multiselect('my_multi_select3');
		$users = array("1" => "user 1",
								"2" => "user 2",
								"3" => "user 3",
								"4" => "user 4");
		$selectUser->addMultiOptions($users);
		$selectUser->setDecorators(array('ViewHelper',
											'Description'));
		$selectUser->setLabel('Select Resources');
		$selectUser->class = "form-control select2 custom-select";	
		$selectUser->setRequired(true);
		//Add Validator
		//Add Filter
		$selectUser->addFilter(new Zend_Filter_HtmlEntities());
		$selectUser->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectUser);
		
		//Create a submit button.
		$this->addElement('submit', 'submit');
		$submitElement = $this->getElement('submit');
		$submitElement->setAttrib('class',"btn-primary btn");
		$submitElement->setDecorators(array('ViewHelper',
											'Description'));
		$submitElement->setLabel('Assign Role');
		 
		$this->setDecorators(array('FormElements',
				'Form'
		));
    }


}

