<?php

class Role_Form_Resource extends Zend_Form
{

    public function init()
    {
        $this->setAction('/role/manage/permission');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal");
    	
    	//Create favorite radio buttons.
		$roleOptions = array("2" => "Select Role",
							"1" => "Create Role");
		$roletype = new Zend_Form_Element_Radio('roleType');
		$roletype->addMultiOptions($roleOptions);
		$roletype->setDecorators(array('ViewHelper',
									'Description'));
		$roletype->setLabel('Role Type')->setSeparator('')->setValue("2");
		$roletype->setRequired(true);
		//Add Account type
		$this->addElement($roletype);
    	
		$role = new Zend_Form_Element_Text('roleName');
		$role->setDecorators(array('ViewHelper',
									'Description'));
		$role->setLabel('Role');
		$role->class = "form-control";
		$role->setRequired(false);
			
		//Add Validator
		//$campaign->addValidator(new Zend_Validate_StringLength(4,20));
		//Add Filter
		$role->addFilter(new Zend_Filter_HtmlEntities());
		$role->addFilter(new Zend_Filter_StripTags());
		$role->addFilter(new Zend_Filter_StringToUpper());
		//Add Username Element
		$this->addElement($role);
		
		//Select Role
		$selectRole = new Zend_Form_Element_Select('selectRole');
		$roles = array("1" => "Role One",
						"2" => "Role Two",
						"3" => "Role Three");
		$selectRole->addMultiOptions($roles);
		$selectRole->setDecorators(array('ViewHelper',
										'Description'));
		$selectRole->setLabel('Select Role');
		$selectRole->class = "form-control select2 custom-select";
		
		$selectRole->setRequired(true);
		//Add Validator
		//Add Filter
		$selectRole->addFilter(new Zend_Filter_HtmlEntities());
		$selectRole->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectRole);
		
		$selectResource = new Zend_Form_Element_Multiselect('my_multi_select3');
		$resources = array("1" => "Resource 1",
								"2" => "Resource 2",
								"3" => "Resource 3",
								"4" => "Resource 4");
		$selectResource->addMultiOptions($resources);
		$selectResource->setDecorators(array('ViewHelper',
											'Description'));
		$selectResource->setLabel('Select Resources');
		$selectResource->class = "form-control select2 custom-select";
		
		$selectResource->setRequired(true);
		//Add Validator
		//Add Filter
		$selectResource->addFilter(new Zend_Filter_HtmlEntities());
		$selectResource->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectResource);
		
		//Create a submit button.
		$this->addElement('submit', 'submit');
		$submitElement = $this->getElement('submit');
		$submitElement->setAttrib('class',"btn-primary btn");
		$submitElement->setDecorators(array('ViewHelper',
											'Description'));
		$submitElement->setLabel('Assign Permission');
		 
		$this->setDecorators(array('FormElements',
				'Form'
		));
    }


}

