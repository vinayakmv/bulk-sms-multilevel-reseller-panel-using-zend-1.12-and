<?php

use modules\user\models\RoleManager;
use modules\user\models\ResourceManager;
use modules\user\models\Resource;
use modules\user\models\ActivityLogManager;
class Role_ManageController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $roleManage = $this->view->navigation()->findOneByLabel('Roles');
		if ($roleManage) {
		  $roleManage->setActive();
		}
		$action = $this->getRequest()->getActionName();
		$emUser = $this->getEntityManager('user');
		$domain = Smpp_Utility::getFqdn();
		$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
		$domainLayout = $domainRepo->layout;
		if ($domainLayout != null){
			$view = $domainLayout.'-'.$action;
			$this->_helper->viewRenderer("$view");
		}
		parent::init();
    }

    public function indexAction()
    {
    	
    	
    }

    public function permissionAction()
    {
        $this->view->subject="ROLE PERMISSION";
    	$page = $this->view->navigation()->findOneByUri('/role/manage/permission');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Roles | Permissions');
    	}
    	
    	
    	
    	$form = new Role_Form_Resource();
    	$emUser = $this->getEntityManager('user');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	
    	$roleRepos = $emUser->getRepository('modules\user\models\Role')->findAll();
    	
    	foreach ($roleRepos as $roles) {
    		$role[$roles->id] = $roles->role;
    	}
    	$form->getElement('selectRole')->setMultiOptions($role);
    	$resourceRepos = $emUser->getRepository('modules\user\models\Resource')->findAll();
    	foreach ($resourceRepos as $resources) {
    		$resource[$resources->id] = $resources->name;
    	}
    	$form->getElement('my_multi_select3')->setMultiOptions($resource);
    	$this->view->form = $form;
    	//Processing Assign Resource Permission
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    		$formValues = $form->getValues();
    		 
             if ($formValues['roleType'] == '2'){
                    if ($formValues['my_multi_select3']) {
                        $roleManager = new RoleManager();
                        $roleRepo = $roleManager-> assignPermission($formValues, $emUser);
                    }
                    
                    if ($roleRepo){
                        $activityManager = new ActivityLogManager();
                        $activityManager->createActivityLog($userRepo, $userRepo->id, $this->_module, $this->_controller, $this->_action, "Role Resources Update Successfully", $this->_domain, $this->_ip, $this->_device, $roleRepo->id, 'UPDATE', 'ROLE RESOURCE', $emUser);
                        $this->view->success = array('Resource' => array('isAssigned' => 'Permission assigned successfully'));
                    }
             }else{
                    if ($formValues['my_multi_select3']) {
                        $roleManager = new RoleManager();
                        $roleRepo = $roleManager->assignPermission($formValues, $emUser);
                    }
                    
                    if ($roleRepo){
                        $activityManager = new ActivityLogManager();
                        $activityManager->createActivityLog($userRepo, $userRepo->id, $this->_module, $this->_controller, $this->_action, "Role Resources Created Successfully", $this->_domain, $this->_ip, $this->_device, $roleRepo->id, 'UPDATE', 'ROLE RESOURCE', $emUser);
                        $this->view->success = array('Resource' => array('isAssigned' => 'Role created successfully'));
                    }
             }

    		
    		 
    	}else {
    		$this->view->errors = $form->getMessages();
    	}
    	$form->reset();
    }
    
    public function userAction()
    {
        $this->view->subject="USER ROLE";
    	$page = $this->view->navigation()->findOneByUri('/role/manage/user');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Roles | User');
    	}
/*    	
    	$form = new Role_Form_Resource();
    	$emUser = $this->getEntityManager('user');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	
    	$roleRepos = $emUser->getRepository('modules\user\models\Role')->findAll();
    	
    	foreach ($roleRepos as $roles) {
    		$role[$roles->id] = $roles->role;
    	}
    	$form->getElement('selectRole')->setMultiOptions($role);
    	$resourceRepos = $emUser->getRepository('modules\user\models\Resource')->findAll();
    	foreach ($resourceRepos as $resources) {
    		$resource[$resources->id] = $resources->name;
    	}
    	$form->getElement('my_multi_select3')->setMultiOptions($resource);
    	$this->view->form = $form;*/
    	
    	//Form User Role Assign
        $emUser = $this->getEntityManager('user');
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
        
        $roleRepos = $emUser->getRepository('modules\user\models\Role')->findAll();
        
        foreach ($roleRepos as $roles) {
            $role[$roles->id] = $roles->role;
        }

    	$formUser = new Role_Form_Role();
    	$formUser->getElement('selectRole')->setMultiOptions($role);
    	$allUsers = $emUser->getRepository('modules\user\models\User')->findAll();
    	
    	foreach ($allUsers as $users) {
    		$user[$users->id] = $users->username .'['.$users->domain->domain.']';
    	}
    	//exit();
    	$formUser->getElement('my_multi_select3')->setMultiOptions($user);
    	$this->view->formUser = $formUser;
    	
    	//Processing User Role
    	if ($this->getRequest()->isPost() && $formUser->isValid($this->getRequest()->getPost())) {
    		$formUserValues = $formUser->getValues();
    		
    		if ($formUserValues['my_multi_select3']) {    			
    			$roleManager = new RoleManager();
    			$roleRepo = $roleManager->assignRole($formUserValues, $emUser);    			
    		}
    		if ($roleRepo){    			
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userRepo->id, $this->_module, $this->_controller, $this->_action, "User Role Assigned Successfully", $this->_domain, $this->_ip, $this->_device, $roleRepo->id, 'UPDATE', 'USER ROLE', $emUser);
    			$this->view->success = array('Role' => array('isAssigned' => 'Role assigned successfully'));
    		}
    	}else {
    		$this->view->errors = $formUser->getMessages();
    	}
    	$formUser->reset();
    }


}