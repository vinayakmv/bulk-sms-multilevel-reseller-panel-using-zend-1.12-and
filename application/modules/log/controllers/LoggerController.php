<?php

class Log_LoggerController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $crediManage = $this->view->navigation()->findOneByLabel('Log Manage');
    	if ($crediManage) {
    		$crediManage->setActive();
    	}
        parent::init();
    }

    public function indexAction()
    {
    }
    public function smsctransAction()
    {
        $this->_helper->layout->disableLayout();
    }
    
    public function smscpromoAction()
    {
        $this->_helper->layout->disableLayout();
    }
    
    public function smpptransAction()
    {
        $this->_helper->layout->disableLayout();
    }
    
    public function smpppromoAction()
    {
        $this->_helper->layout->disableLayout();
    }
    
    public function promoaccessAction()
    {
        $this->_helper->layout->disableLayout();
    }
    
    public function transaccessAction()
    {
        $this->_helper->layout->disableLayout();
    }
    


}

