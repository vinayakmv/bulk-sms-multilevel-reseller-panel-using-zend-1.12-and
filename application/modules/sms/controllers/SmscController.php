<?php

use modules\user\models\ActivityLogManager;
use modules\sms\models\SmscManager;
class Sms_SmscController extends Smpp_Controller_BaseController
{

    public function init()
    {
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function createAction()
    {
        $this->view->subject="CREATE SMS SMSC";
    	$userManage = $this->view->navigation()->findOneByLabel('Smsc');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$page = $this->view->navigation()->findOneByUri('/sms/smsc/create');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Smsc | Create Sms');
    	}      	
        $emUser = $this->getEntityManager('user');
        $emSms = $this->getEntityManager('sms');
        
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $form = new Sms_Form_NewSmsc();
        $routeRepos = $emUser->getRepository('modules\user\models\SmsRoute')->findBy(array('status' => '1'));
         
        foreach ($routeRepos as $routes) {
        	$route[$routes->route] = $routes->route;
        }
        if (isset($route)) {
        	$form->getElement('route')->setMultiOptions($route);
        }
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        	$smppValues = $form->getValues();  

        	$routeRepo = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('route' => $smppValues['route']));
        	$type = $routeRepo->type;
        	
        	$smppManager = new SmscManager();
        	$smppCreated = $smppManager->createSmsc($smppValues, $type, $emSms);
        	if (isset($smppCreated)) {
        		$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
        		
        		$activityManager = new ActivityLogManager();
        		$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Sms Smsc Created Successfully', $this->_domain, $this->_ip, $this->_device, $smppCreated->id, 'CREATE', 'SMS SMSC ACCOUNT', $emUser);
			  	
        		$this->view->success = $this->view->success = array('user' => array('isSuccess' => 'Sms Smsc account created'));
        	}
        }else {
        	$this->view->errors = $form->getMessages();        
    	}
    	$form->reset();
    }

    public function smscsAction()
    {

        $this->view->subject="SMS SMSC LIST";
    	$userManage = $this->view->navigation()->findOneByLabel('Smsc');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$page = $this->view->navigation()->findOneByUri('/sms/smsc/smscs');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Smsc | Sms Smsc');
    	}
    	
    	$emSms = $this->getEntityManager('sms');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	          ->setItemCountPerPage(20);
    	
    	$this->view->entries = $paginator;
    	*/
    	// Block,Unblock,Activate Users,Resellers
    if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
    	
    		if (isset($button['active'])) {
    			//Block User
    			$smppManager = new SmscManager();
    			$type = $button['type'];
    			$reset = $smppManager->blockSmsc($button['active'], $type, $emSms); 
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Smsc Blocked Successfully', $this->_domain, $this->_ip, $this->_device, $button['active'], 'BLOCK', 'SMPP SMSC', $emUser);
    			 
    		}
    	
    		if (isset($button['blocked'])) {
    			//Unblock User
    			$smppManager = new SmscManager();
    			$type = $button['type'];
    			$reset = $smppManager->unblockSmsc($button['blocked'], $type, $emSms);    
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Smsc Activated Successfully', $this->_domain, $this->_ip, $this->_device, $button['blocked'], 'ACTIVATE', 'SMPP SMSC', $emUser);
    			 
    		}
    		
    		if (isset($button['delete'])) {
    			//Delete User
    			$smppManager = new SmscManager();
    			$type = $button['type'];
    			$reset = $smppManager->deleteSmsc($button['delete'], $type, $emSms);    	
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Smsc Deleted Successfully', $this->_domain, $this->_ip, $this->_device, $button['delete'], 'DELETE', 'SMPP SMSC', $emUser);
    			 
    		}
                
                if (isset($button['edit'])) {
                //Delete User
                $smppManager = new SmscManager();
                $type = $button['type'];
                $reset = $smppManager->editSmsc($button['edit'], $button['system_id'], $button['ip'], $button['port'], $button['password'], $button['type'], $button['bind'],$button['Tx'],$button['Rx'], $button['tps'], $emSms);       
                $activityManager = new ActivityLogManager();
                $activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Smsc Edited Successfully', $this->_domain, $this->_ip, $this->_device, $button['edit'], 'EDIT', 'SMPP SMSC', $emUser);
                 
            }
    	
    	}
    	$queryBuilder = $emSms->createQueryBuilder();
    	$queryBuilder->select('e')->from('modules\sms\models\Smsc', 'e');
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    }

    public function statusAction()
    {
        $this->view->subject="MONITOR";
    	$userManage = $this->view->navigation()->findOneByLabel('Monitor');
    	if ($userManage) {
    		$userManage->setActive();
    	}
        $page = $this->view->navigation()->findOneByLabel('Smsc Status');
    	if ($page && $userManage) {
    		$userManage->setActive();
    		$page->setActive();
    		$this->view->headTitle('Monitor | Smsc Status');
    	}
    	$form = new Smpp_Form_Smsc();
    	$emUser = $this->getEntityManager('user');
    	$smscRepos = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('status' => '1'));
    	foreach ($smscRepos as $smsc) {
        	$smscs[$smsc->route] = $smsc->route;
        }
        if (isset($smscs)) {
        	$form->getElement('smsc')->setMultiOptions($smscs);
        }

    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
    		if (isset($button['command'])) {
    			switch ($button['command']) {
    				case "start":
    					shell_exec("/usr/local/sbin/bearerbox -v 4 /usr/local/sbin/kannel.conf &");
    					$this->_redirect('/smpp/smsc/status');
    					break;
    				case "resume":
    					file_get_contents("http://78.47.77.92:10990/resume?password=VEv0xT3l");
    					break;
    				case "restart":
    					file_get_contents("http://78.47.77.92:10990/restart?password=VEv0xT3l");
    					break;
    				case "isolate":
    					file_get_contents("http://78.47.77.92:10990/isolate?password=VEv0xT3l");
    					break;
    				case "suspend":
    					file_get_contents("http://78.47.77.92:10990/suspend?password=VEv0xT3l");
    					break;
    				case "shutdown":
    					shell_exec('/bin/kill -9 $(pidof bearerbox)');
    					break;
    			}
    		}elseif (isset($button['smsc-command'])) {
    			$smsc = $button['smsc'];
    			switch ($button['smsc-command']) {
    				case "start":
    					file_get_contents("http://78.47.77.92:10990/start-smsc?password=VEv0xT3l&smsc=$smsc");
    					break;
    				case "stop":
    					file_get_contents("http://78.47.77.92:10990/stop-smsc?password=VEv0xT3l&smsc=$smsc");
    					break;
    				case "add":
    					file_get_contents("http://78.47.77.92:10990/add-smsc?password=VEv0xT3l&smsc=$smsc");
    					break;
    				case "remove":
    					file_get_contents("http://78.47.77.92:10990/remove-smsc?password=VEv0xT3l&smsc=$smsc");
    					break;
    			}
    		}elseif (isset($button['sqlbox-command'])) {
    			switch ($button['sqlbox-command']) {
	    				case "start":
	    					shell_exec("/usr/local/sbin/sqlbox -v 4 /usr/local/sbin/sqlbox.conf &");	    					
	    					break;
    					case "shutdown":
    						shell_exec('/bin/kill -9 $(pidof sqlbox)');
    						break;
    			}
    		}	
						
    	}
    	$ip = $this->_request->getServer('SERVER_ADDR');
    	$this->view->form = $form;
    }
    
    public function searchAction()
    {
    	$this->view->subject="SEARCH SENT SMS";
    	$userManage = $this->view->navigation()->findOneByLabel('Sms');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$page = $this->view->navigation()->findOneByLabel('Search Sms');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Sms | Search Sms');
    	}
    	$emSms = $this->getEntityManager('sms');
    	 
    	$form = new Sms_Form_Search(); 
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    
    	$form->getElement('datePicker')->setValue(date('m/d/Y'));
    	$this->view->form = $form;
    
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    		$smppValues = $form->getValues();
    		 
    		$sdate = $smppValues['datePicker'];
    		$mobile = trim($smppValues['mobile']);
    		$sender = trim($smppValues['sender']);
    		$type = $smppValues['type'];
    		$startDate=strtotime($sdate.'00:00:00');
    		$endDate=strtotime($sdate.'23:59:59');
    
    		if ($type == 'TRANSACTIONAL'){
    			$table = 'modules\sms\models\SentSmsTransMt';
    		}elseif ($type == 'PROMOTIONAL'){
    			$table = 'modules\sms\models\SentSmsPromoMt';
    		}elseif ($type == 'INTERNATIONAL'){
    			$table = 'modules\sms\models\SentSmsInterMt';
    		}
    
    		$userId = Zend_Auth::getInstance()->getIdentity()->id;
    		$queryBuilder = $emSms->createQueryBuilder();
    		if ((isset($sender) && isset($mobile)) && ($mobile != "" && $sender !== "")){
    
    			$queryBuilder->select('e')
    			->from("$table", 'e')
    			->where('e.receiver LIKE ?1')
    			->andwhere('e.sender = ?4')
    			->andwhere('e.time >= ?2')
    			->andwhere('e.time <= ?3')
    			->setParameter(2, $startDate)
    			->setParameter(3, $endDate)
    			->setParameter(1, '%'.$mobile)
    			->setParameter(4, $sender);
    
    			$query = $queryBuilder->getQuery();
    			$this->view->entries = $query->getResult();
    
    		}elseif(isset($mobile) && $mobile != ""){
    
    			$queryBuilder->select('e')
    			->from("$table", 'e')
    			->where('e.receiver LIKE ?1')
    			->andwhere('e.time >= ?2')
    			->andwhere('e.time <= ?3')
    			->setParameter(2, $startDate)
    			->setParameter(3, $endDate)
    			->setParameter(1, '%'.$mobile);
    
    			$query = $queryBuilder->getQuery();
    			$this->view->entries = $query->getResult();
    
    		}elseif (isset($sender) && $sender != ""){
    
    			$queryBuilder->select('e')
    			->from("$table", 'e')
    			->where('e.sender = ?1')
    			->andwhere('e.time >= ?2')
    			->andwhere('e.time <= ?3')
    			->setParameter(2, $startDate)
    			->setParameter(3, $endDate)
    			->setParameter(1, $sender);
    
    			$query = $queryBuilder->getQuery();
    			$this->view->entries = $query->getResult();
    
    		}else{
    			$this->view->entries = array();
    		}
    		 
    		 
    	}else{
    		$this->view->entries = array();
    	}
    }


}







