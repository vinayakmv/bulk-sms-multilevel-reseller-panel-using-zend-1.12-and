<?php

use modules\user\models\SmsRouteManager;
use modules\user\models\ActivityLogManager;
use modules\user\models\UserManager;
class Sms_RouteController extends Smpp_Controller_BaseController
{

	public function init()
    {
    	$userManage = $this->view->navigation()->findOneByLabel('Routes');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function createAction()
    {
        $this->view->subject="CREATE SMS ROUTES";
    	$page = $this->view->navigation()->findOneByUri('/sms/route/create');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Sms Route');
    	}
        $form = new Sms_Form_NewRoute();
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $emUser = $this->getEntityManager('user');
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        	$routeValues = $form->getValues();        	
        	
        	$smsManager = new SmsRouteManager();
        	$smsCreated = $smsManager->createRoute($routeValues, $emUser);
        	if ($smsCreated) { 
        		$activityManager = new ActivityLogManager();
        		$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Sms Route Created Successfully', $this->_domain, $this->_ip, $this->_device, $smsCreated->id, 'CREATE', 'SMS ROUTE', $emUser);
        		 
        		$this->view->success = array('route' => array('isSuccess' => 'Route created successfully'));
        	}else {
        		$this->view->errors = array('route' => array('isFailed' => 'Route create failed'));
        	}
        }else {
        	$this->view->errors = $form->getMessages();        
    	}
    	$form->reset();
    }

    public function routesAction()
    {
        $this->view->subject="SMS ROUTE LIST";
    	$page = $this->view->navigation()->findOneByUri('/sms/route/routes');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Sms Route');
    	}
    	
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
        $emUser = $this->getEntityManager('user');
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	          ->setItemCountPerPage(20);
    	
    	$this->view->entries = $paginator;
    	*/
    	// Block,Unblock,Activate Users,Resellers
    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
    	
    		if (isset($button['active'])) {
    			//Block User
    			$smppManager = new SmsRouteManager();
    			$reset = $smppManager->blockSmsc($button['active'], $emUser);
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Sms Route Blocked Successfully', $this->_domain, $this->_ip, $this->_device, $button['active'], 'BLOCK', 'SMS ROUTE', $emUser);
    			 
    		}
    	
    		if (isset($button['blocked'])) {
    			//Unblock User
    			$smppManager = new SmsRouteManager();
    			$reset = $smppManager->unblockSmsc($button['blocked'], $emUser); 
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Sms Route Unblocked Successfully', $this->_domain, $this->_ip, $this->_device, $button['blocked'], 'UNBLOCK', 'SMS ROUTE', $emUser);
    			
    		}
    		
    		if (isset($button['delete'])) {
    			//Delete User
    			$smppManager = new SmsRouteManager();
    			$reset = $smppManager->deleteSmsc($button['delete'], $emUser);  
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Sms Route Deleted Successfully', $this->_domain, $this->_ip, $this->_device, $button['delete'], 'DELETE', 'SMS ROUTE', $emUser);
    			
    		}
    	
    	}
    	$queryBuilder = $emUser->createQueryBuilder();
    	$queryBuilder->select('e')->from('modules\user\models\SmsRoute', 'e');
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    }

    public function userAction()
    {
    	$this->view->subject="USER ROUTE";
    	$page = $this->view->navigation()->findOneByUri('/sms/route/user');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | User Assign');
    	}
    	$form = new Sms_Form_UserRoute(); 
    
    	$emUser = $this->getEntityManager('user');
    	$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
    	/*
    	$routeRepo = $emUser->getRepository('modules\user\models\SmsRoute')->findAll();
    	$routes = array();
    	foreach ($routeRepo as $route) {
    		$routeId = $route->id;
    		$routename = $route->type.' | '.$route->name;
    		$routes[$routeId]=$routename;
    	}    	
    	if (isset($routes)) {
    		$form->getElement('route')->setMultiOptions($routes)->setRequired(true);
    	}
    	*/
    	
    	$queryBuilder = $emUser->createQueryBuilder()
    	->select('u.username, d.domain')
    	->from('modules\user\models\User', 'u')
    	->innerJoin('u.domain', 'd')
    	->where('u.reseller = ?2')
    	->setParameter("2", "$resellerId");
    
    	$underUserRepo = $queryBuilder->getQuery()->getResult();
    	$sender = array();
    	foreach ($underUserRepo as $underUser) {
    		$username = $underUser['username'].' | '.$underUser['domain'];
    		$sender[$username]=$username;
    	}
    
    	if (isset($sender)) {
    		$form->getElement('username')->setMultiOptions($sender)->setRequired(true);    		 
    	}
    
    	$this->view->form = $form;
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    		$percentageValues = $form->getValues();
    		$username = $percentageValues['username'];
    		$mode = $percentageValues['mode'];
            $mainType = $percentageValues['rtype'];
            $route = $this->getRequest()->getPost('route', null);
            if ($mainType == 'SMS'){
                $routeRepo = $emUser->getRepository('modules\user\models\Smsroute')->find($route);    		
    		
            }elseif($mainType == 'VOICE'){
                $routeRepo = $emUser->getRepository('modules\user\models\Voiceroute')->find($route);    		
    		
            }elseif($mainType == 'SMPP'){
                $routeRepo = $emUser->getRepository('modules\user\models\Smpproute')->find($route);    		
    		
            }
    		$type = $routeRepo->type;
    		
    		$arrayUsername = explode(chr(124), $username);
    		$username = $arrayUsername['0'];
    		$userManager = new UserManager();
    		$userId = Zend_Auth::getInstance()->getIdentity()->id;
    
    		$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    		$clientRepo = $emUser->getRepository('modules\user\models\User')->findOneBy(array('username' => $username));
    		$setRoute = $userManager->userRoute($mainType, $arrayUsername, $mode, $route, $type, $emUser);
    
    		if (isset($setRoute)){
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $clientRepo->id, $this->_module, $this->_controller, $this->_action, "$route User Route Set Successfully", $this->_domain, $this->_ip, $this->_device, $route, 'UPDATE', 'ROUTE', $emUser);
    			$this->view->success = array('route' => array('isSuccess' => 'Route set successfully'));
    		}else{
    			$this->view->errors = array('route' => array('isFailed' => 'Route set failed'));
    		}
    		//$form->reset();
    	}else{
            $this->view->errors = $form->getMessages();
        }
    
    }

    public function autosuggestAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $type = $this->getRequest()->getParam('rtype');
        $returnString = array();
        
            $emUser = $this->getEntityManager('user');
            $userId = Zend_Auth::getInstance()->getIdentity()->id;
            if ($type == 'SMS'){
                $users = $emUser->getRepository("modules\user\models\SmsRoute")->findBy(array('status' => '1' ));
            }elseif ($type == 'SMPP'){
                $users = $emUser->getRepository("modules\user\models\SmppRoute")->findBy(array('status' => '1' ));
            }elseif ($type == 'VOICE'){
                $users = $emUser->getRepository("modules\user\models\VoiceRoute")->findBy(array('status' => '1' ));
            }
            if($users){
                foreach($users as $user) {
                    $route = $user->name;
                    $id = $user->id;
                    //$route = $user->defaultSmsc;
                    
                    $returnString[$id] = $route;
                }
            }
        
        
        //echo ($returnString);
        echo Zend_Json::encode($returnString);
    }
    
    public function domainAction()
    {
    	$this->view->subject="DOMAIN ROUTE";
    	$page = $this->view->navigation()->findOneByUri('/sms/route/domain');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Domain Assign');
    	}
    
    	$form = new User_Form_DomainPercentage();
    	$this->view->form = $form;
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    		$percentageValues = $form->getValues();
    		$username = $percentageValues['username'];
    		$mode = $percentageValues['mode'];
    		$type = $percentageValues['type'];
    		$route = $percentageValues['route'];
    		$percentage = $percentageValues['percentage'];
    		$arrayUsername = explode('|', $username);
    		$domain = $arrayUsername['0'];
    		$userManager = new UserManager();
    		$userId = Zend_Auth::getInstance()->getIdentity()->id;
    		$emUser = $this->getEntityManager('user');
    		$userRepo = $em->getRepository('modules\user\models\User')->find($userId);
    		$setPercentage = $userManager->userPercentage($domain, $type, $mode, $route, $percentage, $emUser);
    
    		if (isset($setPercentage)){
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userRepo->id, $this->_module, $this->_controller, $this->_action, "$percentage% Domain Route Set Successfully", $this->_domain, $this->_ip, $this->_device, $route.'|'.$type, 'UPDATE', 'ROUTE', $emUser);
    			$this->view->success = array('route' => array('isSuccess' => 'Route set successfully'));
    		}else{
    			$this->view->errors = array('route' => array('isFailed' => 'Route set failed'));
    		}
    		$form->reset();
    	}
    
    
    
    }
    
    public function autouserAction()
    {
    	$this->_helper->layout()->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    
    	$key = $this->getRequest()->getParam('term');
    
    	$returnString = array();
    	if(!empty($key) && strlen($key) > 2) {
    		$emUser = $this->getEntityManager('user');
    		$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
    		$users = $emUser->getRepository("modules\user\models\User")->getUser("$resellerId", "$key");
    		if($users){
    			foreach($users as $user) {
    				$username = $user['username'];
    				$domain = $user['domain'];
    
    				$returnString[] = $username.' | '.$domain;
    			}
    		}
    	}
    
    	//echo ($returnString);
    	echo Zend_Json::encode($returnString);
    }
    
    public function autodomainAction()
    {
    	$this->_helper->layout()->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    
    	$key = $this->getRequest()->getParam('term');
    
    	$returnString = array();
    	if(!empty($key) && strlen($key) > 2) {
    		$emUser = $this->getEntityManager('user');
    		$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
    		$users = $emUser->getRepository("modules\user\models\Domain")->getDomain("$key");
    		if($users){
    			foreach($users as $user) {
    				$domain = $user['domain'];
    
    				$returnString[] = $domain;
    			}
    		}
    	}
    
    	//echo ($returnString);
    	echo Zend_Json::encode($returnString);
    }

}





