<?php
use modules\sms\models\SmsCampaignManager;
use modules\user\models\ActivityLogManager;
class Sms_CampaignController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $broadcast = $this->view->navigation()->findOneByLabel('Sms');
    	if ($broadcast) {
    		$broadcast->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
    	parent::init();
    }

    public function indexAction()
    {
    	
        $this->view->subject="CREATE CAMPAIGN";
    	$page = $this->view->navigation()->findOneByLabel('Quick');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Sms | Quick');
    	}
    	
    	$form = new Sms_Form_Compose();
    	$form->getElement('coding')->setValue('7bit');
    	$this->view->codingValue = '7bit';
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	$emSms = $this->getEntityManager('sms');
    	$emSmpp = $this->getEntityManager('smpp');
        $emUrl = $this->getEntityManager('url');
    	
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId); 
        //**************FIND SENDER ID START ****************************\\  
		
    	if ($userRepo->openSenderId){
    		$form->getElement('typeSenderId')->setRequired(true);
    		$this->view->senderId = $form->typeSenderId;
    	}else{
    		$senderIdRepo = $emSms->getRepository('modules\sms\models\SmsSenderId')->findBy(array('user'=>$userRepo->id,'type'=>'2','status'=>'1'),array('default' => 'asc'));
    		$DefaultSenderIdRepo = $emSms->getRepository('modules\sms\models\SmsSenderId')->findOneBy(array('user'=>$userRepo->id,'type'=>'2','status'=>'1', 'default' => '1'));
    		$default = array(); 
    		$sender = array();
    		if (isset($DefaultSenderIdRepo)){
    			$defaultSender = $DefaultSenderIdRepo->name;
    		}    		
    		foreach ($senderIdRepo as $senderId) {
    			if (isset($defaultSender) && $senderId->name == $defaultSender){
    				$default[$senderId->name]=$senderId->name;
    			}else{
    				$sender[$senderId->name]=$senderId->name;
    			}
    		}
    		if (!empty($default)){
    			$sender = array_merge($default, $sender);
    		}
    		
    		//reset($sender);
    		if (isset($sender)) {
    			$form->getElement('selectSenderId')->setMultiOptions($sender)->setRequired(true);
    			//$form->getElement('selectSenderId')->setValue($defaultSender);
    			$this->view->senderId = $form->selectSenderId;
    		}
    	}
    	//**************FIND SENDER ID END ****************************\\   
    	$urls = array();
        $urls["0"] = 'NULL';
        $urlRepo = $emUrl->getRepository('modules\url\models\Url')->findBy(array('user'=>$userRepo->id,'status'=>'1'),array('id' => 'asc'));
        foreach ($urlRepo as $url) {
            $urls["$url->id"] = $url->name;
        }
        if (isset($urls)) {
                $form->getElement('selectUrl')->setMultiOptions($urls)->setRequired(false);
                //$form->getElement('selectSenderId')->setValue($defaultSender);
                //$this->view->senderId = $form->selectSenderId;
            }

    	$groupRepos = $emUser->getRepository('modules\user\models\Lead')->findBy(array('user' => $userId));
    	$this->view->allgroups = $groupRepos;
    	
    	
    	$smsRouteRepo = $userRepo->smsRoutes;
    	//$routeRepo = $em->getRepository('Models\Route')->findBy(array('status'=>'1'));
    	//print_r($routeRepo);
    
    	foreach ($smsRouteRepo as $routes) {    		
    		$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    	}
    	if (empty($route)){
    		$trans = $userRepo->domain->transRoute;
    		$promo = $userRepo->domain->promoRoute;
    		$scrub = $userRepo->domain->scrubRoute;
    		$inter = $userRepo->domain->interRoute;
    		$premi = $userRepo->domain->premiRoute;
    		
    		if (isset($trans)){
    			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$trans,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    			}
    		}
    		if (isset($premi)){
    			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$premi,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    			}
    		}
    		if (isset($promo)){
    			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$promo,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    			}
    		}
    		if (isset($scrub)){
    			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$scrub,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    			}
    		}
    		if (isset($inter)){
    			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$inter,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    			}
    		}
    	}
    	if (isset($route)) {
    		$form->getElement('smsType')->setMultiOptions($route);
    	}
    	
    	
        //**************FIND LEADS ID START ****************************\\
        $leadRepo = $emUser->getRepository('modules\user\models\Lead')->findBy(array('user'=>$userRepo->id,'status'=>'1','type'=>'2'));
    	foreach ($leadRepo as $leads) {
    		$contacts[$leads->id] = $leads->name;
    	}
    	if (isset($contacts)) {
    		$form->getElement('selectContact')->setMultiOptions($contacts);
    	}  
        //**************FIND LEADS ID STOP ****************************\\   
        
        //**************FIND TEMPLATE ID START ****************************\\   
        $templateRepo = $emSms->getRepository('modules\sms\models\SmsTemplate')->findBy(array('user'=>$userRepo->id,'status'=>'1'));
        $templateArray[''] = "NULL";
        foreach ($templateRepo as $template) {
           // $template1 = explode('$/g', $template->template); 
           // $template2 = str_replace('/^','',$template1[0]);
            $stringCut = substr(urldecode($template->template), 0, 15);
            $templateArray[urldecode($template->template)] = $stringCut;
        }
        if (isset($templateArray)) {
           // $form->getElement('template')->setMultiOptions($templateArray);
            $this->view->templateArray = $templateArray;
        }         
        //**************FIND TEMPLATE ID STOP ****************************\\   

    	
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    		    		 
    		@$originalFilename1 = pathinfo($form->uploadContact->getFileName());
    		$newFilename1 = 'uploadContact-' . uniqid() . '.' . $originalFilename1['extension'];
    		$form->uploadContact->addFilter('Rename', $newFilename1);
    		 
    		$campaignValues = $form->getValues();
            $template=urlencode(strip_tags($campaignValues['text'])); 
            $coding = $campaignValues['coding'];
            $flash = $campaignValues['flashRadio'];
            
            if($campaignValues['scheduleRadio']=='on'){
                $getVal=$this->getRequest()->getPost();        
            	if ($campaignValues['datePicker'] == ""){
                	$campaignValues['datePicker']=date('Y-m-d');
                }else{
                	$campaignValues['datePicker']=date('Y-m-d',strtotime($getVal['datePicker']));
                }      
                if ($campaignValues['timePicker'] == ""){
                	$campaignValues['timePicker']=date('H:i:s');
                }else{
                	$campaignValues['timePicker']=date('H:i:s',strtotime($getVal['timePicker']));
                }
                $campaignValues['input'] = 'SCHEDULED';
            }
            else{
            	$campaignValues['datePicker']=date('Y-m-d');
            	$campaignValues['timePicker']=date('H:i:s');
                $campaignValues['input'] = 'COMPOSE';
            }            
    		//$text = $campaignValues['text'];
    		$text = urlencode(strip_tags($campaignValues['text']));
            $text = preg_replace('/(%E2%80%AC|%E2%80%AD|%C3%A2%C2%80%C2%AD|%C3%A2%C2%80%C2%AC|%C3%82%C2%AD|%C3%82+|%C3%A1%C2%9A%C2%80|%C3%A2%C2%80%C2%8B|%C3%A1+%C2%8E|%C3%A2%C2%80%C2%8A%C3%A2%C2%80%C2%8B|%C3%A2%C2%80%C2%8B|%C3%A2%C2%80%C2%8B%C3%A2%C2%80%C2%AF|%C3%A2%C2%81%C2%9F%C3%A2%C2%80%C2%8B|%C3%A3%C2%80%C2%80)/', '%0A', $text);
    		$text = urldecode(str_replace('%0D%0A', '%0A', $text));
    		/*
    		$smsCounter = new Smpp_Sms_SMSCounter();
    		$smsCounter = $smsCounter->count($text);
    		$smsCount =  $smsCounter->messages;
    		$smsLength =  $smsCounter->length;
    		$smsEncoding =  $smsCounter->encoding;
    		
    	
    		$smsCalculator = new Smpp_Sms_SMSCalculator();
    		$smsCount = $smsCalculator->getPartCount($text);
    		$smsEncoding = $smsCalculator->getCharset($text);
    		$smsLength = '0';
    		*/
    		if ($text == ""){
    			return $this->view->errors = array('Campaign' => array('isFailed' => 'Message content is empty'));
    		}
    		
    		$smsCountCalculator = new Smpp_Sms_SMSCountCalculator();
    		//Santize to GSM7 
    		$orgText = $text;
    		if ($coding == '7bit'){
    			$text = $smsCountCalculator->sanitizeToGSM($text);     		
	    		if ($text == " "){
	    			return $this->view->errors = array('Campaign' => array('isFailed' => 'Message content is unicode 16bit'));
	    		}
    		}
    		
    		if ($flash == '1'){
    			$campaignValues['flashRadio'] = '1';
    			//$text = $smsCountCalculator->sanitizeToGSM($text);
    			//if ($text == " "){
    			//	return $this->view->errors = array('Campaign' => array('isFailed' => 'Message content is unicode 16bit'));
    			//}
    		}else {
    			$campaignValues['flashRadio'] = '0';
    		}
    		$routeType = $campaignValues['smsType'];
    		$routes = explode ('|', $routeType);
    		if ($routes['0'] == 'INTERNATIONAL'){
    			$text = $text."\nOPTOUT 5258";
    		}
    		$campaignValues['text'] = $text;
    		//Count Calculation
    		$smsCounter = $smsCountCalculator->count($text);
    		$smsCount =  $smsCounter->messages;
    		$smsLength =  $smsCounter->length;
    		$smsEncoding =  $smsCounter->encoding;
    		    		
    		$campaignManager = new SmsCampaignManager();
    		$campaignCreated = $campaignManager->createCampaign($campaignValues, $orgText, $smsLength, $smsCount, $smsEncoding, $userId, false, 'WEB-PUSH', $emSms, $emUser, $emSmpp, $emUrl);
    		
    		$form->reset();
    		
    		if (isset($campaignCreated['Campaign']['uploadAudio'])) {
    			$this->view->errors = array('Campaign' => array('isFailed' => $campaignCreated['Campaign']['uploadAudio']));
    		}elseif (isset($campaignCreated['Campaign']['uploadContact'])){
    			$this->view->errors = array('Campaign' => array('isFailed' => $campaignCreated['Campaign']['uploadContact']));
    		}elseif (isset($campaignCreated['Campaign']['contactCount'])) {
    			$this->view->errors = array('Campaign' => array('isFailed' => $campaignCreated['Campaign']['contactCount']));
    		}elseif (isset($campaignCreated['Campaign']['credit'])) {
    			$this->view->errors = array('Campaign' => array('isFailed' => $campaignCreated['Campaign']['credit']));
    		}elseif (isset($campaignCreated['Campaign']['template'])) {
    			$this->view->errors = array('Campaign' => array('isFailed' => $campaignCreated['Campaign']['template']));
    		}elseif (isset($campaignCreated['Campaign']['sender'])) {
    			$this->view->errors = array('Campaign' => array('isFailed' => $campaignCreated['Campaign']['sender']));
    		}elseif (isset($campaignCreated['Campaign']['isSuccess'])) {
    			//Campaign Created
    			$campaignId = $campaignCreated['Campaign']['data'];
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'SMS Campaign Created Successfully', $this->_domain, $this->_ip, $this->_device, $campaignId, 'CREATE', 'SMS', $emUser);
    			
    			$this->view->success = array('Campaign' => array('isSuccess' => 'Campaign created successfully'));
    		}
    		
    	}else{
    		if (count($form->getErrors('token')) > 0) {
    			$this->_redirect('/sms/campaign/index');
    		}
    		$error = $form->getMessages();
    		if (!isset($error['captcha']['badCaptcha'])){
    			$form->reset();
    		}
    		
    		$this->view->errors = $error;
    		
    		if ($form->getElement('coding')->getValue() != NULL){
    			$this->view->codingValue = $form->getElement('coding')->getValue();
    		}
    	}
    	
    	
    	$this->view->form = $form;
    }
    
    public function dynamicAction()
    {
    	 
        $this->view->subject="CREATE DYNAMIC CAMPAIGN";
    	$page = $this->view->navigation()->findOneByLabel('Dynamic');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Sms | Dynamic');
    	}
    	
    	$form = new Sms_Form_Dynamic();
    	$form->getElement('coding')->setValue('7bit');
    	$this->view->codingValue = '7bit';
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	$emSms = $this->getEntityManager('sms');
    	$emSmpp = $this->getEntityManager('smpp');
        $emUrl = $this->getEntityManager('url');
    	
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId); 
        //**************FIND SENDER ID START ****************************\\  
		
    	if ($userRepo->openSenderId){
    		$form->getElement('typeSenderId')->setRequired(true);
    		$this->view->senderId = $form->typeSenderId;
    	}else{
    		$senderIdRepo = $emSms->getRepository('modules\sms\models\SmsSenderId')->findBy(array('user'=>$userRepo->id,'type'=>'2','status'=>'1'),array('default' => 'asc'));
    		$DefaultSenderIdRepo = $emSms->getRepository('modules\sms\models\SmsSenderId')->findOneBy(array('user'=>$userRepo->id,'type'=>'2','status'=>'1', 'default' => '1'));
    		$default = array(); 
    		$sender = array();
    		if (isset($DefaultSenderIdRepo)){
    			$defaultSender = $DefaultSenderIdRepo->name;
    		}    		
    		foreach ($senderIdRepo as $senderId) {
    			if (isset($defaultSender) && $senderId->name == $defaultSender){
    				$default[$senderId->name]=$senderId->name;
    			}else{
    				$sender[$senderId->name]=$senderId->name;
    			}
    		}
    		if (!empty($default)){
    			$sender = array_merge($default, $sender);
    		}
    		
    		//reset($sender);
    		if (isset($sender)) {
    			$form->getElement('selectSenderId')->setMultiOptions($sender)->setRequired(true);
    			//$form->getElement('selectSenderId')->setValue($defaultSender);
    			$this->view->senderId = $form->selectSenderId;
    		}
    	}
    	//**************FIND SENDER ID END ****************************\\  

        $urls = array();
        $urls["0"] = 'NULL';
        $urlRepo = $emUrl->getRepository('modules\url\models\Url')->findBy(array('user'=>$userRepo->id,'status'=>'1'),array('id' => 'asc'));
        foreach ($urlRepo as $url) {
            $urls["$url->id"] = $url->name;
        }
        if (isset($urls)) {
                $form->getElement('selectUrl')->setMultiOptions($urls)->setRequired(false);
                //$form->getElement('selectSenderId')->setValue($defaultSender);
                //$this->view->senderId = $form->selectSenderId;
            } 
    	
    	$groupRepos = $emUser->getRepository('modules\user\models\Lead')->findBy(array('user' => $userId));
    	$this->view->allgroups = $groupRepos;
    	
    	
    	$smsRouteRepo = $userRepo->smsRoutes;
    	//$routeRepo = $em->getRepository('Models\Route')->findBy(array('status'=>'1'));
    	//print_r($routeRepo);
    
    	foreach ($smsRouteRepo as $routes) {    		
    		$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    	}
    	if (empty($route)){
    		$trans = $userRepo->domain->transRoute;
    		$promo = $userRepo->domain->promoRoute;
    		$scrub = $userRepo->domain->scrubRoute;
    		$inter = $userRepo->domain->interRoute;
    		$premi = $userRepo->domain->premiRoute;
    		
    		if (isset($trans)){
    			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$trans,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    			}
    		}
    		if (isset($premi)){
    			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$premi,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    			}
    		}
    		if (isset($promo)){
    			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$promo,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    			}
    		}
    		if (isset($scrub)){
    			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$scrub,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    			}
    		}
    		if (isset($inter)){
    			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$inter,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
    			}
    		}
    	}
    	if (isset($route)) {
    		$form->getElement('smsType')->setMultiOptions($route);
    	}
    	 
        //**************FIND TEMPLATE ID START ****************************\\   
        $templateRepo = $emSms->getRepository('modules\sms\models\SmsTemplate')->findBy(array('user'=>$userRepo->id,'status'=>'1'));
        $templateArray[''] = "NULL";
        foreach ($templateRepo as $template) {
           // $template1 = explode('$/g', $template->template); 
           // $template2 = str_replace('/^','',$template1[0]);
            $stringCut = substr(urldecode($template->template), 0, 15);
            $templateArray[urldecode($template->template)] = $stringCut;
        }
        if (isset($templateArray)) {
           // $form->getElement('template')->setMultiOptions($templateArray);
            $this->view->templateArray = $templateArray;
        }         
        //**************FIND TEMPLATE ID STOP ****************************\\   

    	
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    		    		 
    		$originalFilename1 = pathinfo($form->uploadContact->getFileName());
    		$newFilename1 = 'uploadContact-' . uniqid() . '.' . $originalFilename1['extension'];
    		$form->uploadContact->addFilter('Rename', $newFilename1);
    		 
    		$campaignValues = $form->getValues();
            $template=urlencode(strip_tags($campaignValues['text'])); 
            $coding = $campaignValues['coding'];
            
            

            if($campaignValues['scheduleRadio']=='on'){
                $getVal=$this->getRequest()->getPost();  
                if ($campaignValues['datePicker'] == ""){
                	$campaignValues['datePicker']=date('Y-m-d');
                }else{
                	$campaignValues['datePicker']=date('Y-m-d',strtotime($getVal['datePicker']));
                }      
                if ($campaignValues['timePicker'] == ""){
                	$campaignValues['timePicker']=date('H:i:s');
                }else{
                	$campaignValues['timePicker']=date('H:i:s',strtotime($getVal['timePicker']));
                }
                $campaignValues['input'] = 'DYNAMIC-SCHEDULED';
            }
            else{
            	$campaignValues['datePicker']=date('Y-m-d');
            	$campaignValues['timePicker']=date('H:i:s');
                $campaignValues['input'] = 'DYNAMIC-COMPOSE';
            }            
    		//$text = $campaignValues['text'];
    		$text = urlencode(strip_tags($campaignValues['text']));
            $text = preg_replace('/(%E2%80%AC|%E2%80%AD|%C3%A2%C2%80%C2%AD|%C3%A2%C2%80%C2%AC|%C3%82%C2%AD|%C3%82+|%C3%A1%C2%9A%C2%80|%C3%A2%C2%80%C2%8B|%C3%A1+%C2%8E|%C3%A2%C2%80%C2%8A%C3%A2%C2%80%C2%8B|%C3%A2%C2%80%C2%8B|%C3%A2%C2%80%C2%8B%C3%A2%C2%80%C2%AF|%C3%A2%C2%81%C2%9F%C3%A2%C2%80%C2%8B|%C3%A3%C2%80%C2%80)/', '%0A', $text);
    		$text = urldecode(str_replace('%0D%0A', '%0A', $text));
    		/*
    		$smsCounter = new Smpp_Sms_SMSCounter();
    		$smsCounter = $smsCounter->count($text);
    		$smsCount =  $smsCounter->messages;
    		$smsLength =  $smsCounter->length;
    		$smsEncoding =  $smsCounter->encoding;
    		
    	
    		$smsCalculator = new Smpp_Sms_SMSCalculator();
    		$smsCount = $smsCalculator->getPartCount($text);
    		$smsEncoding = $smsCalculator->getCharset($text);
    		$smsLength = '0';
    		*/
    		if ($text == ""){
    			return $this->view->errors = array('Campaign' => array('isFailed' => 'Message content is empty'));
    		}
    		
    		$smsCountCalculator = new Smpp_Sms_SMSCountCalculator();
    		//Santize to GSM7 
    		$orgText = $text;
    		if ($coding == '7bit'){
    			$text = $smsCountCalculator->sanitizeToGSM($text);     		
	    		if ($text == " "){
	    			return $this->view->errors = array('Campaign' => array('isFailed' => 'Message content is unicode 16bit'));
	    		}
    		}
    		
    		if ($coding == 'Flash'){
    			$campaignValues['flashRadio'] = '1';
    			$text = $smsCountCalculator->sanitizeToGSM($text);
    			if ($text == " "){
    				return $this->view->errors = array('Campaign' => array('isFailed' => 'Message content is unicode 16bit'));
    			}
    		}else {
    			$campaignValues['flashRadio'] = '0';
    		}
    		
    		$campaignValues['text'] = $text;    		
    		//Count Calculation
    		$smsCounter = $smsCountCalculator->count($text);
    		$smsCount =  $smsCounter->messages;
    		$smsLength =  $smsCounter->length;
    		$smsEncoding =  $smsCounter->encoding;
    		$campaignValues['contactType'] = '4';

            
    		
    		$campaignManager = new SmsCampaignManager();
    		$campaignCreated = $campaignManager->createCampaign($campaignValues, $orgText, $smsLength, $smsCount, $smsEncoding, $userId, false, 'WEB-PUSH', $emSms, $emUser, $emSmpp);
    		
    		$form->reset();
    		
    		if (isset($campaignCreated['Campaign']['uploadAudio'])) {
    			$this->view->errors = array('Campaign' => array('isFailed' => $campaignCreated['Campaign']['uploadAudio']));
    		}elseif (isset($campaignCreated['Campaign']['uploadContact'])){
    			$this->view->errors = array('Campaign' => array('isFailed' => $campaignCreated['Campaign']['uploadContact']));
    		}elseif (isset($campaignCreated['Campaign']['contactCount'])) {
    			$this->view->errors = array('Campaign' => array('isFailed' => $campaignCreated['Campaign']['contactCount']));
    		}elseif (isset($campaignCreated['Campaign']['credit'])) {
    			$this->view->errors = array('Campaign' => array('isFailed' => $campaignCreated['Campaign']['credit']));
    		}elseif (isset($campaignCreated['Campaign']['template'])) {
    			$this->view->errors = array('Campaign' => array('isFailed' => $campaignCreated['Campaign']['template']));
    		}elseif (isset($campaignCreated['Campaign']['isSuccess'])) {
    			//Campaign Created
    			$campaignId = $campaignCreated['Campaign']['data'];
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Dynamic SMS Campaign Created Successfully', $this->_domain, $this->_ip, $this->_device, $campaignId, 'CREATE', 'DYNAMIC SMS', $emUser);
    			
    			$this->view->success = array('Campaign' => array('isSuccess' => 'Campaign created successfully'));
    		}
    		
    	}else{
    		if (count($form->getErrors('token')) > 0) {
    			$this->_redirect('/sms/campaign/dynamic');
    		}
    		$error = $form->getMessages();
    		if (!isset($error['captcha']['badCaptcha'])){
    			$form->reset();
    		}
    		
    		$this->view->errors = $error;
    	}
    	$this->view->form = $form;
    }


}

