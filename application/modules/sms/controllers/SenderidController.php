<?php
use modules\sms\models\SmsSenderIdManager;
class Sms_SenderidController extends Smpp_Controller_BaseController
{

  public function init()
  {
    $broadcast = $this->view->navigation()->findOneByLabel('Senders');
    if ($broadcast) {
      $broadcast->setActive();
    }
    $action = $this->getRequest()->getActionName();
    $emUser = $this->getEntityManager('user');
    $domain = Smpp_Utility::getFqdn();
    $domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    $domainLayout = $domainRepo->layout;
    if ($domainLayout != null){
    	$view = $domainLayout.'-'.$action;
    	$this->_helper->viewRenderer("$view");
    }
    parent::init();
  }

  public function indexAction()
  {
    // action body
  }

  public function requestAction()
  {
    $this->view->subject="SENDER ID REQUEST";

    $page = $this->view->navigation()->findOneByUri('/sms/senderid/request');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('Senders | Sms Request');
    }

    $form = new Sms_Form_Sender();
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    $emUser = $this->getEntityManager('user');
    $emSms = $this->getEntityManager('sms');
    $this->view->form = $form;

    if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {

      $getValues = $form->getValues();
      $name=$getValues['senderId'];
      $remark=$getValues['userremark'];
      $senderIdManager = new SmsSenderIdManager();
      $createSenderId = $senderIdManager->createSenderId($userId, $name, '2', $emSms, '0',$remark);

      if (isset($createSenderId['SenderId']['isCreated'])) {
        $this->view->success = array('SenderId' => array('isSuccess' => $createSenderId['SenderId']['isCreated']));
      }elseif (isset($createSenderId['SenderId']['senderCount'])) {
        $this->view->errors = array('SenderId' => array('isFailed' => $createSenderId['SenderId']['senderCount']));
      }else{
      	$this->view->errors = array('SenderId' => array('isFailed' => 'SenderId request failed'));
      }
    }else{
      $this->view->errors = $form->getMessages();
    }
    $form->reset();
  }
  public function approvalAction()
  {
    $this->view->subject="SENDER ID APPROVAL";
    $page = $this->view->navigation()->findOneByUri('/sms/senderid/approval');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('Senders | Sms Approval');
    }

    $emSms = $this->getEntityManager('sms');
    $emUser = $this->getEntityManager('user');
    $this->view->emUser = $emUser->getRepository('modules\user\models\User');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;

    if ($this->getRequest()->isPost()) {

      $getValues = $this->getRequest()->getPost();

      $senderIdManager = new SmsSenderIdManager();

      if(isset($getValues['approved'])){
        $manageSenderId = $senderIdManager->manageSenderId($getValues,'Approved', $userId, $emSms);
        $this->view->Success = array('approved' => array('success' => 'Approved successfully'));

      }
      else if (isset($getValues['Rejected'])) {
       $manageSenderId = $senderIdManager->manageSenderId($getValues,'Rejected', $userId, $emSms);
       $this->view->Success = array('rejected' => array('success' => 'Rejected Successfully'));
     }

   }

   $queryBuilder = $emSms->createQueryBuilder();
   $queryBuilder->select('e')
   ->from('modules\sms\models\SmsSenderId', 'e')
   ->where('e.status = ?1')
   ->orderBy('e.id', 'DESC')
   ->setParameter(1, '0');
   $query = $queryBuilder->getQuery();
   $this->view->entries = $query->getResult();
   /*
   $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
   $currentPage = 1;
  //$i = $this->_request->getParam('i');
   $i = $this->_request->getParam('p');
   if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i=='')
    {
      $this->view->pageIndex = $i;
    }
    else
    {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    } 

    $this->view->entries = $paginator;
    */
    $this->view->userId = $userId;
  }
  public function viewAction()
  {
    $this->view->subject="SENDER ID VIEW";
    $page = $this->view->navigation()->findOneByUri('/sms/senderid/view');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('Senders | Sms View');
    }

      //$reportManager = new SendSmsManager();
    $emSms = $this->getEntityManager('sms');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    if ($this->getRequest()->isPost()) {
    
    	$getValues = $this->getRequest()->getPost();
    	 
    	$senderIdManager = new SmsSenderIdManager();
    	if($getValues['submit'] == 'Delete'){
    		$manageSenderId = $senderIdManager->manageSenderId($getValues,'Delete', $userId, $emSms);
    		//$this->view->Success = array('approved' => array('success' => 'Approved successfully'));
    		 
    	}elseif ($getValues['submit'] == 'default') {
    		$manageSenderId = $senderIdManager->manageSenderId($getValues,'Default', $userId, $emSms);
    		//$this->view->Success = array('rejected' => array('success' => 'Rejected Successfully'));
    	}
    }
    $queryBuilder = $emSms->createQueryBuilder();
    $queryBuilder->select('e')
    ->from('modules\sms\models\SmsSenderId', 'e')
    ->where('e.user = ?1')
    ->andwhere('e.type = ?2')
    ->andwhere('e.status != 5')
    ->orderBy('e.id', 'DESC')
    ->setParameter(1, $userId)
    ->setParameter(2, 2);
    $query = $queryBuilder->getQuery();
    $this->view->entries = $query->getResult();
    /*
    $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    $currentPage = 1;
  //$i = $this->_request->getParam('i');
    $i = $this->_request->getParam('p');
    if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i=='')
    {
      $this->view->pageIndex = $i;
    }
    else
    {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    } 

    $this->view->entries = $paginator;
    */
    $this->view->userId = $userId;
    
  }
  public function viewallAction()
  {
    $this->view->subject="SENDER ID VIEW ALL";
    $page = $this->view->navigation()->findOneByUri('/sms/senderid/viewall');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('Senders | All Sms Senders');
    }
    
      //$reportManager = new SendSmsManager();
    $emSms = $this->getEntityManager('sms');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    if ($this->getRequest()->isPost()) {
    
    	$getValues = $this->getRequest()->getPost();
    
    	$senderIdManager = new SmsSenderIdManager();
    	if($getValues['submit'] == 'Delete'){
    		$manageSenderId = $senderIdManager->manageSenderId($getValues,'Delete', $userId, $emSms);
    		//$this->view->Success = array('approved' => array('success' => 'Approved successfully'));
    		 
    	}elseif ($getValues['submit'] == 'default') {
    		$manageSenderId = $senderIdManager->manageSenderId($getValues,'Default', $userId, $emSms);
    		//$this->view->Success = array('rejected' => array('success' => 'Rejected Successfully'));
    	}
    }
    $queryBuilder = $emSms->createQueryBuilder();
    $queryBuilder->select('e')
    ->from('modules\sms\models\SmsSenderId', 'e')
    ->where('e.type = ?1')
    ->orderBy('e.id', 'DESC')
    ->setParameter(1, 2);
    $query = $queryBuilder->getQuery();
    $this->view->entries = $query->getResult();
    /*
    $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    $currentPage = 1;
  //$i = $this->_request->getParam('i');
    $i = $this->_request->getParam('p');
    if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i=='')
    {
      $this->view->pageIndex = $i;
    }
    else
    {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    } 

    $this->view->entries = $paginator;
    */
    $this->view->userId = $userId;
  }


}







