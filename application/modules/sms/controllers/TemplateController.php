<?php
use Doctrine\ORM\Query\ResultSetMapping;
use modules\sms\models\SmsTemplateManager;
class Sms_TemplateController extends Smpp_Controller_BaseController
{

public function init()
{
    $broadcast = $this->view->navigation()->findOneByLabel('Templates');
    if ($broadcast) {
      $broadcast->setActive();
  	}
  	$action = $this->getRequest()->getActionName();
  	$emUser = $this->getEntityManager('user');
  	$domain = Smpp_Utility::getFqdn();
  	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
  	$domainLayout = $domainRepo->layout;
  	if ($domainLayout != null){
  		$view = $domainLayout.'-'.$action;
  		$this->_helper->viewRenderer("$view");
  	}
  	parent::init();
}

public function indexAction()
{
        // action body
}

public function requestAction()
{
  $this->view->subject="TEMPLATE REQUEST";

    $page = $this->view->navigation()->findOneByUri('/sms/template/request');
    if ($page) {
        $page->setActive();
        $this->view->headTitle('Templates | Sms Request');
    }

    $form = new Sms_Form_Template();
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    $emSms = $this->getEntityManager('sms');
    //$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    $this->view->form = $form;
    if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        $getValues = $form->getValues();
        $template=urlencode(strip_tags($getValues['text'])); 
        $getValues['text']= urldecode(str_replace('%0D%0A', '%0A', $template));


        if($getValues['text'] !='')
        { 
            $campaignManager = new SmsTemplateManager();
            $addTemplate = $campaignManager->addTemplate($getValues, $userId, $emSms);
            if(isset($addTemplate))
            {
                $this->view->success = array('Template' => array('success' => 'Template requested successfully'));
            }
        }
        else
        {
            $this->view->errors = array('Template' => array('isNull' => 'Template is null'));
        }    


    }else{
        $this->view->errors = $form->getMessages();
    }
    $form->reset();
}
public function approvalAction()
{
   $this->view->subject="TEMPLATE APPROVAL";
    $page = $this->view->navigation()->findOneByUri('/sms/template/approval');
    if ($page) {
        $page->setActive();
        $this->view->headTitle('Templates | Sms Approval');
    }

        //$reportManager = new SendSmsManager();
    $emSms = $this->getEntityManager('sms');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    if ($this->getRequest()->isPost()) {
       $templateDetail=$this->getRequest()->getPost();
       $campaignManager = new SmsTemplateManager();

       if(isset($templateDetail['approved']))
       {
        $updateRequestTemplate = $campaignManager->updateRequestTemplate($templateDetail, $userId, $emSms);
        if(isset($updateRequestTemplate))
        {
            $this->view->success = array('Template' => array('isApproved' => 'Template approved successfully'));

        }
    }
    else if(isset($templateDetail['rejected']))
    {
     $updateRequestTemplate = $campaignManager->updateRequestTemplate($templateDetail, $userId, $emSms);

     if(isset($updateRequestTemplate))
     {
         $this->view->success = array('Template' => array('isRejected' => 'Template rejected successfully'));
     }
 }   
}
$queryBuilder = $emSms->createQueryBuilder();
$queryBuilder->select('e')
->from('modules\sms\models\SmsTemplate', 'e')
->where('e.status = ?1')
->orderBy('e.id', 'DESC')
->setParameter(1, 0);
$query = $queryBuilder->getQuery();
$this->view->entries = $query->getResult();
/*
$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
$currentPage = 1;
  //$i = $this->_request->getParam('i');
$i = $this->_request->getParam('p');
if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
  }
  $perPage = 10;
  $paginator->setCurrentPageNumber($currentPage)
  ->setItemCountPerPage($perPage);
  $this->view->result1 = $paginator;
  if($i==1 || $i=='')
  {
      $this->view->pageIndex = $i;
  }
  else
  {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
  }   

  $this->view->entries = $paginator;
  */
  $this->view->userId = $userId;
}
public function viewAction()
{
   $this->view->subject="TEMPLATE VIEW";
    $page = $this->view->navigation()->findOneByUri('/sms/template/view');
    if ($page) {
        $page->setActive();
        $this->view->headTitle('Templates | Sms Templates');
    }

        //$reportManager = new SendSmsManager();
    $emSms = $this->getEntityManager('sms');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    if ($this->getRequest()->isPost()) {
       $templateDetail=$this->getRequest()->getPost();
       $campaignManager = new SmsTemplateManager();

       if(isset($templateDetail['Delete'])){
        $updateRequestTemplate = $campaignManager->updateRequestTemplate($templateDetail, $userId, $emSms);
        if(isset($updateRequestTemplate))
        {
            $this->view->success = array('Template' => array('isDeleted' => 'Template deleted successfully'));

        }
    }
    else if(isset($templateDetail['rejected'])){
     $updateRequestTemplate = $campaignManager->updateRequestTemplate($templateDetail, $userId, $emSms);

     if(isset($updateRequestTemplate))
     {
         $this->view->success = array('Template' => array('isRejected' => 'Template rejected successfully'));
     }
 	}   
}
$queryBuilder = $emSms->createQueryBuilder();
$queryBuilder->select('e')
->from('modules\sms\models\SmsTemplate', 'e')
->where('e.user = ?1')
->andWhere('e.status != ?2')
->orderBy('e.id', 'DESC')
->setParameter(1, $userId)
->setParameter(2, '5');
$query = $queryBuilder->getQuery();
$this->view->entries = $query->getResult();
/*
$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
$currentPage = 1;
  //$i = $this->_request->getParam('i');
$i = $this->_request->getParam('p');
if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
  }
  $perPage = 10;
  $paginator->setCurrentPageNumber($currentPage)
  ->setItemCountPerPage($perPage);
  $this->view->result1 = $paginator;
  if($i==1 || $i=='')
  {
      $this->view->pageIndex = $i;
  }
  else
  {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
  }   

  $this->view->entries = $paginator;
  */
  $this->view->userId = $userId;
}
public function viewallAction()
{
   $this->view->subject="TEMPLATE VIEW";
    $page = $this->view->navigation()->findOneByUri('/sms/template/viewall');
    if ($page) {
        $page->setActive();
        $this->view->headTitle('Templates | All Sms Templates');
    }

        //$reportManager = new SendSmsManager();
    $emSms = $this->getEntityManager('sms');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    
    if ($this->getRequest()->isPost()) {
    	$templateDetail=$this->getRequest()->getPost();
    	$campaignManager = new SmsTemplateManager();
    
    	if(isset($templateDetail['Delete'])){
    		$updateRequestTemplate = $campaignManager->updateRequestTemplate($templateDetail, $userId, $emSms);
    		if(isset($updateRequestTemplate))
    		{
    			$this->view->success = array('Template' => array('isDeleted' => 'Template deleted successfully'));
    
    		}
    	}
    	else if(isset($templateDetail['rejected'])){
    		$updateRequestTemplate = $campaignManager->updateRequestTemplate($templateDetail, $userId, $emSms);
    
    		if(isset($updateRequestTemplate))
    		{
    			$this->view->success = array('Template' => array('isRejected' => 'Template rejected successfully'));
    		}
    	}
    }
    
$queryBuilder = $emSms->createQueryBuilder();
$queryBuilder->select('e')
->from('modules\sms\models\SmsTemplate', 'e')
->orderBy('e.id', 'DESC');
$query = $queryBuilder->getQuery();
$this->view->entries = $query->getResult();
/*
$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
$currentPage = 1;
  //$i = $this->_request->getParam('i');
$i = $this->_request->getParam('p');
if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
  }
  $perPage = 10;
  $paginator->setCurrentPageNumber($currentPage)
  ->setItemCountPerPage($perPage);
  $this->view->result1 = $paginator;
  if($i==1 || $i=='')
  {
      $this->view->pageIndex = $i;
  }
  else
  {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
  }   

  $this->view->entries = $paginator;
  */
  $this->view->userId = $userId;
}

}
