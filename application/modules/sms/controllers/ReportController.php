<?php
use modules\sms\models\SmsCampaignManager;
use modules\user\models\AccountManager;
use modules\user\models\ActivityLogManager;
use Doctrine\ORM\Query\ResultSetMapping;
class Sms_ReportController extends Smpp_Controller_BaseController
{

  public function init()
  {
    $broadcast = $this->view->navigation()->findOneByLabel('Sms');
    if ($broadcast) {
      $broadcast->setActive();
    }
    $action = $this->getRequest()->getActionName();
    $emUser = $this->getEntityManager('user');
    $domain = Smpp_Utility::getFqdn();
    $domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    $domainLayout = $domainRepo->layout;
    if ($domainLayout != null){
    	$view = $domainLayout.'-'.$action;
    	$this->_helper->viewRenderer("$view");
    }
    parent::init();
  }

  public function indexAction()
  {
   $this->view->subject="CAMPAIGN REPORTS";
   $page = $this->view->navigation()->findOneByLabel('Sent');
   if ($page) {
    $page->setActive();
    $this->view->headTitle('Sms | Sent');
   }

    	//$reportManager = new SendSmsManager();
  if(isset($_request['sdate'])){
  	$sdate=$_request['sdate'];
  }else {
  	$sdate=date('m/d/Y',time());
  }
  if(isset($_request['edate'])) {
  	$edate=$_request['edate'];
  }else {
  	$edate=date('m/d/Y',time());
  }
  $emUser = $this->getEntityManager('user');
  $emSms = $this->getEntityManager('sms');
  $userId = Zend_Auth::getInstance()->getIdentity()->id;
  if ($this->getRequest()->isPost()) {
    $getValue=$this->getRequest()->getPost();

              //  print_r($getValue);exit();
    if(isset($getValue['search'])){
      $sdate=$getValue['from'];
      $edate=$getValue['to'];
    }else if(isset($getValue['gid'])){

      $sdate=$getValue['from'];
      $edate=$getValue['to'];
      $generateId=$getValue['gid'];
      
      $checkCampagin = $emSms->getRepository('modules\sms\models\SmsCampaign')->find($generateId);
              //  echo "sdfsd";exit();
      $cdateTime=strtotime(date('Y-m-d H:i:s')); 
      if($cdateTime >= strtotime($checkCampagin->endDate->format('d-m-Y H:i:s'))){
        if($checkCampagin->reportStatus == 1){
          $updateReport =$emSms->getRepository('modules\sms\models\SmsCampaign')->updateReport($getValue['gid'], 'greater');
          $this->view->success = array('Report' => array('Generate' => 'Report generating, please wait'));
        }
        else{
          $this->view->errors = array('Report' => array('inProgress' => "Report generating already started, please wait"));
        }
      }
      else{
        $this->view->errors = array('Report' => array('timeNotReach' => 'Please generate after '.$checkCampagin->endDate->format('d-m-Y H:i:s')));
      }
                //print_r($this->view->errors);

    }

    

  }
 
  $queryBuilder = $emSms->createQueryBuilder();

   $currentDate = date('Y-m-d H:i:s',time());
//   $yesterday = date('Y-m-d',strtotime($edate));
  $startDate=date('Y-m-d H:i:s',strtotime($sdate.'00:00:00'));
  $endDate=date('Y-m-d H:i:s',strtotime($edate.'23:59:59'));
  
  $diffDate = date_diff(date_create($startDate),date_create($endDate));
  
  $month = $diffDate->m;
  $day = $diffDate->d;
  
  if (($month == '1' && $day == '0') || ($month == '0' && $day <= '31')){
	  	$queryBuilder->select('e')
	  ->from('modules\sms\models\SmsCampaign', 'e')
	  ->where('e.user = ?1')
	  ->andwhere('e.startDate >= ?2')
	  ->andwhere('e.startDate <= ?3')
	  ->andwhere('e.startDate <= ?4')
	 // ->andwhere('e.createdDate <= ?4')
	  ->orderBy('e.id', 'DESC')
	  ->setParameter(1, $userId)
	  //->setParameter(2, $currentDate)
	  //->setParameter(3, $yesterday);
	  ->setParameter(2, $startDate)
	  ->setParameter(4, $currentDate)
	  ->setParameter(3, $endDate);
	  $query = $queryBuilder->getQuery();
	  $this->view->entries = $query->getResult();
  }else{
  	$this->view->entries = array();
  	$this->view->errors = array('Report' => array('inSearch' => "Date range should not exceed 31 days"));
  }
  
  $this->view->pageIndex = 1;
 
  /*
  $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
  $currentPage = 1;
  //$i = $this->_request->getParam('i');
   $i = $this->_request->getParam('p');
  if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i=='')
    {
      $this->view->pageIndex = $i;
    }
    else
    {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    }	
    $this->view->daterange=$sdate.' - '.$edate;
    $this->view->entries = $paginator;
    */
    $this->view->daterange=$sdate.'^'.$edate;
    $this->view->userId = $userId;
  }

  public function scheduledAction()
  {
   $this->view->subject="CAMPAIGN SCHEDULED";
   $page = $this->view->navigation()->findOneByLabel('Scheduled');
   if ($page) {
    $page->setActive();
    $this->view->headTitle('Sms | Scheduled');
   }

  if(isset($_request['sdate'])) $sdate=$_request['sdate'];
  else $sdate=date('m/d/Y');
  if(isset($_request['edate'])) $edate=$_request['edate'];
  else $edate=date('m/d/Y');
  $emSms = $this->getEntityManager('sms');
  $emUser = $this->getEntityManager('user');
  $userId = Zend_Auth::getInstance()->getIdentity()->id;
  if ($this->getRequest()->isPost()) {
    $getValue=$this->getRequest()->getPost();
    
    if(isset($getValue['gid'])){

      $generateId=$getValue['gid'];
      $smsCampaignManager = new SmsCampaignManager();

        $checkCampagin = $emSms->getRepository('modules\sms\models\SmsCampaign')->find($generateId);
      
        if($checkCampagin->status == 0){
        	
          $cancelCampaign = $smsCampaignManager->cancellCampaign($generateId, $emSms);
          if ($cancelCampaign){
          	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
          	$activityManager = new ActivityLogManager();
          	$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Campaign Cancelled Successfully', $this->_domain, $this->_ip, $this->_device, $generateId, 'CANCEL', 'CAMPAIGN', $emUser);
          	 
          	$accountManager = new AccountManager();
          	if ($cancelCampaign->type == 'TRANSACTIONAL'){
                if ($cancelCampaign->apiKey == 'WEB-PUSH'){
                  $account = '6';
                }else{
                  $account = '8';
                }
          		
          	}elseif ($cancelCampaign->type == 'PROMOTIONAL'){
          		
                if ($cancelCampaign->apiKey == 'WEB-PUSH'){
                  $account = '7';
                }else{
                  $account = '9';
                }
          	}elseif ($cancelCampaign->type == 'TRANS-SCRUB'){
                
                if ($cancelCampaign->apiKey == 'WEB-PUSH'){
                  $account = '12';
                }else{
                  $account = '13';
                }
            }
          	$accountArray = $accountManager->reCredit($userId, $account, $cancelCampaign->credit, $emUser);
          	$credit = $accountArray['user'];
          	$resellerRepo = $accountArray['reseller'];
          	$logRepo = $accountArray['log'];
          	if ($credit) {
          		$activityManager = new ActivityLogManager();
          		$activityManager->createActivityLog($resellerRepo, $credit->id, $this->_module, $this->_controller, $this->_action, 'Recredit Transfered Successfully', $this->_domain, $this->_ip, $this->_device, $logRepo->id, 'RECREDIT', 'ACCOUNT', $emUser);
          	}	 
          	$this->view->success = array('Report' => array('isSuccess' => 'Campaign cancelled successfully'));
          	
          }else{
          	$this->view->errors = array('Report' => array('inProgress' => "Campaign cancel failed "));
       	  }
      
   		 }else{
   		 	$this->view->errors = array('Report' => array('inProgress' => "Campaign cancel failed "));
   		 }	
    }
  }

  $queryBuilder = $emSms->createQueryBuilder();

  $currentDate=date('Y-m-d H:i:s',time());
  
  $queryBuilder->select('e')
  ->from('modules\sms\models\SmsCampaign', 'e')
  ->where('e.user = ?1')
  ->andwhere('e.startDate >= ?2')
  ->orderBy('e.id', 'DESC')
  ->setParameter(1, $userId)
  ->setParameter(2, $currentDate);
  $query = $queryBuilder->getQuery();
  $this->view->entries = $query->getResult();

  /*
  $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
  $currentPage = 1;
  //$i = $this->_request->getParam('i');
   $i = $this->_request->getParam('p');
  if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i=='')
    {
      $this->view->pageIndex = $i;
    }
    else
    {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    }	
    $this->view->daterange=$sdate.' - '.$edate;
    $this->view->entries = $paginator;
    */
    $this->view->userId = $userId;
  }

  public function downcsvAction()
  {
    $this->_helper->viewRenderer->setNoRender(true);
    $this->_helper->layout->disableLayout();
    $file=$this->_request->getParam('file');
    $fullPath = "/var/www/html/webpanel/files/sms/report/".$file;
    if(file_exists($fullPath)){
      if ($fd = fopen ($fullPath, "r")) {
        $fsize = filesize($fullPath);
        $path_parts = pathinfo($fullPath);
        $ext = strtolower($path_parts["extension"]);
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"".$path_parts["basename"]."\"");            
        header("Content-length: $fsize");
        header("Cache-control: private");

        while(!feof($fd)) {
          $buffer = fread($fd, 2048);
          echo $buffer;
        }
      }
      fclose ($fd);
    }
    else{
      $this->_redirect('sms/report/index');
    }

  }
  
  public function summaryAction()
  {
  	//$this->_helper->viewRenderer->setNoRender(true);
  	$this->_helper->layout->disableLayout();
  	$emUser = $this->getEntityManager('user');
  	$emSms = $this->getEntityManager('sms');
  	$userId = Zend_Auth::getInstance()->getIdentity()->id;
  	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
  	
  	$values = $this->_request->getParam('p');
  	$exValues = explode('^', $values);
  	
  	$campId = $exValues['0'];
    if (isset($exValues['1'])){
        $campUserId = $exValues['1'];
    }
  	if ($campId){
  		$smsCampagin = $emSms->getRepository('modules\sms\models\SmsCampaign')->find($campId);

  		$rsm = new ResultSetMapping();
  		// build rsm here
  		if ($userRepo->role->role == 'ADMIN'){
  			$dlrUrl = $campUserId.'@'."$campId".'-';
  		}else{
  			$dlrUrl = "$userId".'@'."$campId".'-';
  		}
  		
  		$queryBuilder = $emSms->createQueryBuilder();
  		if (($smsCampagin->type == 'TRANSACTIONAL') || ($smsCampagin->type == 'PREMIUM')){
  			$queryBuilder->select('u.sender,u.receiver,u.dlrdata,u.status')
  			->from('modules\sms\models\SentSmsTransMt', 'u')
  			->where('u.dlrUrl LIKE ?1')
  			->setParameter("1", $dlrUrl.'%');
  		}elseif ($smsCampagin->type == 'INTERNATIONAL'){
  			$queryBuilder->select('u.sender,u.receiver,u.dlrdata,u.status')
  			->from('modules\sms\models\SentSmsInterMt', 'u')
  			->where('u.dlrUrl LIKE ?1')
  			->setParameter("1", $dlrUrl.'%');
  		}else {
  			$queryBuilder->select('u.sender,u.receiver,u.dlrdata,u.status')
  			->from('modules\sms\models\SentSmsPromoMt', 'u')
  			->where('u.dlrUrl LIKE ?1')
  			->setParameter("1", $dlrUrl.'%');
  		}
		
	
		$results = $queryBuilder->getQuery()->getResult();
		$send = 0;
		$dlrReceived = 0;
		$delivered = 0;
		$failed = 0;
		 	
		foreach ($results as $row){
			$send++;
			if ($row['status'] == '1'){
				$dlrReceived++;
				$dlrdata = urldecode($row['dlrdata']);
				if (strpos($dlrdata, 'stat:DELIVRD')!== FALSE ){
					$delivered++;
				}else{
					$failed++;
				}
			}
		}
                
                $queryBuilder->update('modules\sms\models\SmsCampaign', 'u')
                             ->set('u.deliveredCount', '?1')
                             ->set('u.failedCount', '?2')
                             ->set('u.awaitingCount', '?3')
                              ->where('u.id = ?4')
                              ->setParameter(1, $delivered)
                              ->setParameter(2, $failed)
                              ->setParameter(3, ($send-$dlrReceived))
                              ->setParameter(4, $campId);
                $queryBuilder->getQuery()->execute();

		$this->view->send = $send;
		$this->view->dlrReceived = $dlrReceived;
		$this->view->delivered = $delivered;
		$this->view->failed = $failed;
		$this->view->submit = $send-$dlrReceived;

  	}
  }

  public function allAction()
  {
  	$this->view->subject="ALL CAMPAIGN REPORTS";
  	$page = $this->view->navigation()->findOneByLabel('All Sent');
  	if ($page) {
  		$page->setActive();
  		$this->view->headTitle('Sms | All Sent');
  	}
  
  	//$reportManager = new SendSmsManager();
  	if(isset($_request['sdate'])){
  		$sdate=$_request['sdate'];
  	}else {
  		$sdate=date('m/d/Y',time());
  	}
  	if(isset($_request['edate'])) {
  		$edate=$_request['edate'];
  	}else {
  		$edate=date('m/d/Y',time());
  	}
  	$emUser = $this->getEntityManager('user');
  	$emSms = $this->getEntityManager('sms');
  	$userId = Zend_Auth::getInstance()->getIdentity()->id;
  	if ($this->getRequest()->isPost()) {
  		$getValue=$this->getRequest()->getPost();
  
  		//  print_r($getValue);exit();
  		if(isset($getValue['search'])){
  			$sdate=$getValue['from'];
  			$edate=$getValue['to'];
  		}else if(isset($getValue['update'])){
  			$campUserId=$getValue['userid'];
  			$campId=$getValue['campid'];
  			$percentage = $getValue['percentage'];
  			$type = $getValue['type'];
  			
  			if ($type == 'TRANSACTIONAL' || $type == 'PREMIUM' || $type == 'TRANS-SCRUB'){
  				$table = 'sent_sms_trans_mt';
  			}elseif ($type == 'PROMOTIONAL'){
  				$table = 'sent_sms_promo_mt';
  			}elseif ($type == 'INTERNATIONAL'){
  				$table = 'sent_sms_inter_mt';
  			}
  
  			$sql = "UPDATE $table SET dlrdata='stat%3ADELIVRD' where dlr_url LIKE '$campUserId@$campId-%' AND dlrdata LIKE '%stat%3AUNDELIV%' and RAND() < $percentage;";
  			$numRows = $emSms->getConnection()->exec($sql);
  			
  			$this->view->success = array('update' => array('success' => "$numRows sms updated"));
  		}
  	}
  
  	$queryBuilder = $emSms->createQueryBuilder();
  
  	$currentDate = date('Y-m-d H:i:s',time());
  	//   $yesterday = date('Y-m-d',strtotime($edate));
  	$startDate=date('Y-m-d H:i:s',strtotime($sdate.'00:00:00'));
  	$endDate=date('Y-m-d H:i:s',strtotime($edate.'23:59:59'));
  
  	$diffDate = date_diff(date_create($startDate),date_create($endDate));
  
  	$month = $diffDate->m;
  	$day = $diffDate->d;
  
  	if (($month == '1' && $day == '0') || ($month == '0' && $day <= '31')){
  		
  		$queryBuilder->select('e')
  		->from('modules\sms\models\SmsCampaign', 'e')
  		->where('e.startDate >= ?2')
  		->andwhere('e.startDate <= ?3')
  		->andwhere('e.startDate <= ?4')
  	  ->andwhere('e.name != ?5')
  		->andwhere('e.name != ?6')
  		->orderBy('e.id', 'DESC')
  		->setParameter(5, 'API-POST')
  		->setParameter(6, 'API-GET')
  		->setParameter(2, $startDate)
  		->setParameter(4, $currentDate)
  		->setParameter(3, $endDate);
  		
  		
  		$query = $queryBuilder->getQuery();
  		$this->view->entries = $query->getResult();
  	}else{
  		$this->view->entries = array();
  		$this->view->errors = array('Report' => array('inSearch' => "Date range should not exceed 31 days"));
  	}
  
  	$this->view->pageIndex = 1;
  
  	/*
  	 $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
  	 $currentPage = 1;
  	 //$i = $this->_request->getParam('i');
  	 $i = $this->_request->getParam('p');
  	 if(!empty($i))
  	 { //Where i is the current page
  	 $currentPage = $this->_request->getParam('p');
  	 }
  	 $perPage = 10;
  	 $paginator->setCurrentPageNumber($currentPage)
  	 ->setItemCountPerPage($perPage);
  	 $this->view->result1 = $paginator;
  	 if($i==1 || $i=='')
  	 {
  	 $this->view->pageIndex = $i;
  	 }
  	 else
  	 {
  	 $this->view->pageIndex = (($i-1)*$perPage)+1;
  	 }
  	 $this->view->daterange=$sdate.' - '.$edate;
  	 $this->view->entries = $paginator;
  	 */
  	$this->view->daterange=$sdate.'^'.$edate;
  	$this->view->userId = $userId;
  }
  


}

