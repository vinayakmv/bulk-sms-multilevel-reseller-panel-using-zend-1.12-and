<?php
use Doctrine\ORM\Query\ResultSetMapping;
use modules\sms\models\SmsSpamManager;
use modules\user\models\UserManager;
use modules\user\models\ActivityLogManager;
class Sms_SpamController extends Smpp_Controller_BaseController
{

public function init()
{
    $broadcast = $this->view->navigation()->findOneByLabel('Spam');
    if ($broadcast) {
      $broadcast->setActive();
  	}
  	$action = $this->getRequest()->getActionName();
  	$emUser = $this->getEntityManager('user');
  	$domain = Smpp_Utility::getFqdn();
  	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
  	$domainLayout = $domainRepo->layout;
  	if ($domainLayout != null){
  		$view = $domainLayout.'-'.$action;
  		$this->_helper->viewRenderer("$view");
  	}
  	parent::init();
}

public function indexAction()
{
        // action body
}

public function addAction()
{
  $this->view->subject="ADD KEYWORDS";

    $page = $this->view->navigation()->findOneByUri('/sms/spam/add');
    if ($page) {
        $page->setActive();
        $this->view->headTitle('Spam | Add');
    }

    $form = new Sms_Form_Spam();
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    $emSms = $this->getEntityManager('sms');
    //$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    $this->view->form = $form;
    if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        $getValues = $form->getValues();
        $template=urlencode(strip_tags($getValues['text'])); 
        $getValues['text']= urldecode(str_replace('%0D%0A', '%0A', $template));


        if($getValues['text'] !='')
        { 
            $campaignManager = new SmsSpamManager();
            $addTemplate = $campaignManager->addSpam($getValues, $userId, $emSms);
            if(isset($addTemplate))
            {
                $this->view->success = array('Spam' => array('success' => 'Keyword added successfully'));
            }
        }
        else
        {
            $this->view->errors = array('Spam' => array('isNull' => 'Keyword is null'));
        }    


    }else{
        $this->view->errors = $form->getMessages();
    }
    $form->reset();
}

public function viewAction()
{
   $this->view->subject="KEYWORD VIEW";
    $page = $this->view->navigation()->findOneByUri('/sms/spam/view');
    if ($page) {
        $page->setActive();
        $this->view->headTitle('Spam | View');
    }

        //$reportManager = new SendSmsManager();
    $emSms = $this->getEntityManager('sms');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    if ($this->getRequest()->isPost()) {
       $spamDetail=$this->getRequest()->getPost();
       $campaignManager = new SmsSpamManager();

       if(isset($spamDetail['delete'])){
	        $deleteSpam = $campaignManager->updateKeyword($spamDetail, $userId, $emSms);
	        if(isset($deleteSpam)){
	            $this->view->success = array('Spam' => array('isSuccess' => 'Keywords deleted successfully'));
			}
    	}elseif(isset($spamDetail['activate'])){
     		$activateSpam = $campaignManager->updateKeyword($spamDetail, $userId, $emSms);
			if(isset($activateSpam)){
		         $this->view->success = array('Spam' => array('isSuccess' => 'Keywords activated successfully'));
		     }
 		}elseif(isset($spamDetail['deactivate'])){
     		$deactivateSpam = $campaignManager->updateKeyword($spamDetail, $userId, $emSms);
			if(isset($deactivateSpam)){
		         $this->view->success = array('Spam' => array('isSuccess' => 'Keywords deactivated successfully'));
		     }
 		}elseif(isset($spamDetail['edit'])){
     		$editSpam = $campaignManager->updateKeyword($spamDetail, $userId, $emSms);
			if(isset($editSpam)){
		         $this->view->success = array('Spam' => array('isSuccess' => 'Keywords edited successfully'));
		     }
 		}   
}
$queryBuilder = $emSms->createQueryBuilder();
$queryBuilder->select('e')
->from('modules\sms\models\SmsSpam', 'e')
->orderBy('e.id', 'DESC');
$query = $queryBuilder->getQuery();
$this->view->entries = $query->getResult();
/*
$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
$currentPage = 1;
  //$i = $this->_request->getParam('i');
$i = $this->_request->getParam('p');
if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
  }
  $perPage = 10;
  $paginator->setCurrentPageNumber($currentPage)
  ->setItemCountPerPage($perPage);
  $this->view->result1 = $paginator;
  if($i==1 || $i=='')
  {
      $this->view->pageIndex = $i;
  }
  else
  {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
  }   

  $this->view->entries = $paginator;
  */
  $this->view->userId = $userId;
}

public function approvalAction()
{
	$this->view->subject="SPAM CAMPAIGNS";
	$page = $this->view->navigation()->findOneByUri('/sms/spam/approval');
	if ($page) {
		$page->setActive();
		$this->view->headTitle('Spam | Campaign');
	}

	//$reportManager = new SendSmsManager();
	if(isset($_request['sdate'])){
		$sdate=$_request['sdate'];
	}else {
		$sdate=date('m/d/Y',time());
	}
	if(isset($_request['edate'])) {
		$edate=$_request['edate'];
	}else {
		$edate=date('m/d/Y',time());
	}
	$emUser = $this->getEntityManager('user');
	$emSms = $this->getEntityManager('sms');
	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	if ($this->getRequest()->isPost()) {
		$getValue=$this->getRequest()->getPost();

		//  print_r($getValue);exit();
		if(isset($getValue['search'])){
			$sdate=$getValue['from'];
			$edate=$getValue['to'];
		}
		$spamDetail=$this->getRequest()->getPost();
		$campaignManager = new SmsSpamManager();
		
		if(isset($spamDetail['approve'])){
			$activateSpam = $campaignManager->updateCampaign($spamDetail, $userId, $emSms);
			if(isset($activateSpam)){
				$this->view->success = array('Spam' => array('isSuccess' => 'Campaign approved successfully'));
			}
		}elseif(isset($spamDetail['reject'])){
			$deactivateSpam = $campaignManager->updateCampaign($spamDetail, $userId, $emSms);
			if(isset($deactivateSpam)){
				$this->view->success = array('Spam' => array('isSuccess' => 'Campaign rejected successfully'));
			}
		}
	}

	$queryBuilder = $emSms->createQueryBuilder();

	$currentDate = date('Y-m-d H:i:s',time());
	//   $yesterday = date('Y-m-d',strtotime($edate));
	$startDate=date('Y-m-d H:i:s',strtotime($sdate.'00:00:00'));
	$endDate=date('Y-m-d H:i:s',strtotime($edate.'23:59:59'));

	$diffDate = date_diff(date_create($startDate),date_create($endDate));

	$month = $diffDate->m;
	$day = $diffDate->d;

	if (($month == '1' && $day == '0') || ($month == '0' && $day <= '31')){

		$queryBuilder->select('e')
		->from('modules\sms\models\SmsCampaign', 'e')
		->where('e.startDate >= ?2')
		->andwhere('e.startDate <= ?3')
		->andwhere('e.startDate <= ?4')
		->andwhere('e.status = ?5')
		->orderBy('e.id', 'DESC')
		->setParameter(5, '6')
		->setParameter(2, $startDate)
		->setParameter(4, $currentDate)
		->setParameter(3, $endDate);


		$query = $queryBuilder->getQuery();
		$this->view->entries = $query->getResult();
	}else{
		$this->view->entries = array();
		$this->view->errors = array('Report' => array('inSearch' => "Date range should not exceed 31 days"));
	}

	$this->view->pageIndex = 1;

	/*
	 $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
	 $currentPage = 1;
	 //$i = $this->_request->getParam('i');
	 $i = $this->_request->getParam('p');
	 if(!empty($i))
	 { //Where i is the current page
	 $currentPage = $this->_request->getParam('p');
	 }
	 $perPage = 10;
	 $paginator->setCurrentPageNumber($currentPage)
	 ->setItemCountPerPage($perPage);
	 $this->view->result1 = $paginator;
	 if($i==1 || $i=='')
	 {
	 $this->view->pageIndex = $i;
	 }
	 else
	 {
	 $this->view->pageIndex = (($i-1)*$perPage)+1;
	 }
	 $this->view->daterange=$sdate.' - '.$edate;
	 $this->view->entries = $paginator;
	 */
	$this->view->daterange=$sdate.'^'.$edate;
	$this->view->userId = $userId;
}

public function assignAction()
{
	$this->view->subject="USER SPAM ASSIGN";
	$page = $this->view->navigation()->findOneByUri('/sms/spam/assign');
	if ($page) {
		$page->setActive();
		$this->view->headTitle('Spam | Assign');
	}
	$form = new Sms_Form_UserSpam();
	 
	$emUser = $this->getEntityManager('user');
	$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
	$queryBuilder = $emUser->createQueryBuilder()
	->select('u.username, d.domain')
	->from('modules\user\models\User', 'u')
	->innerJoin('u.domain', 'd')
	->where('u.id != ?2')
	->setParameter("2", "$resellerId");

	$underUserRepo = $queryBuilder->getQuery()->getResult();
	$sender = array();
	foreach ($underUserRepo as $underUser) {
		$username = $underUser['username'].' | '.$underUser['domain'];
		$sender[$username]=$username;
	}
	 
	if (isset($sender)) {
		$form->getElement('username')->setMultiOptions($sender)->setRequired(true);
		 
	}
	 
	$this->view->form = $form;
	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {

		$percentageValues = $form->getValues();

		$username = $percentageValues['username'];
		$mode = $percentageValues['mode'];
		$type = '1';
		$status = $percentageValues['status'];
		$arrayUsername = explode('|', $username);
		$username = $arrayUsername['0'];
		$domain = trim($arrayUsername['1']);
		$spamManager = new SmsSpamManager();
		$userId = Zend_Auth::getInstance()->getIdentity()->id;

		$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => $domain));
		$domainId = $domainRepo->id;
		
		$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
		$parentDomainId = $userRepo->domain->id;

		$clientRepo = $emUser->getRepository('modules\user\models\User')->findOneBy(array('username' => $username, 'parentDomain' => $parentDomainId, 'domain' => $domainId));
		$setSpamCheck = $spamManager->userAssign($clientRepo, $mode, $status, $emUser);

		if (isset($setSpamCheck)){
			$activityManager = new ActivityLogManager();
			$activityManager->createActivityLog($userRepo, $clientRepo->id, $this->_module, $this->_controller, $this->_action, "Spam Check Set $status Successfully", $this->_domain, $this->_ip, $this->_device, $clientRepo->id, 'UPDATE', 'SPAM', $emUser);
			$this->view->success = array('Spam' => array('isSuccess' => 'Spam check set successfully'));
		}else{
			$this->view->errors = array('Spam' => array('isFailed' => 'Spam check set failed'));
		}
		$form->reset();
	}else{
		$this->view->errors = $form->getErrorMessages();
	}
	 
}

}
