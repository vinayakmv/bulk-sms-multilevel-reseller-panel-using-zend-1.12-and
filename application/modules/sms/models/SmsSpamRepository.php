<?php
namespace modules\sms\models;
use Doctrine\ORM\EntityRepository;

/**
 * 
 * @author Vinayak
 *
 */
class SmsSpamRepository extends \Smpp_Doctrine_EntityRepository
{
	
	public function addKeywords($getArray,$userId)
	{
		
	}

	public function updateRequestSpam($getArray,$userId)
	{	
		$id=$getArray['id'];
		$cdate=date("Y-m-d H:i:s");
		$remark=$getArray['remark'];

		if(isset($getArray['rejected']))
		{
			$status='2';
		}
		else if(isset($getArray['approved']))
		{	
			$status='1';
			$sql1 = "SELECT `template` FROM `pushsms_template_request` WHERE `id` = '$id'"; 
			$em = $this->getEntityManager('sms'); 
			$stmt = $em->getConnection()->prepare($sql1);
			$stmt->execute();
			$statement= $stmt->fetchAll();

			$template = "/^".$statement['0']['template']."$/g";
			//$template = $statement['0']['template']; 
		}

		
		$sql = "UPDATE `pushsms_template_request` SET `remark` = '$remark',`status`='$status',`updated_by`='$userId',`updated_date`='$cdate' ,`template` = '$template' WHERE `id` = '$id'" ;
		$em = $this->getEntityManager('sms');
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		return $stmt;
	
	}
}