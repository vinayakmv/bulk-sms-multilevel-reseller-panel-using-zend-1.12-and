<?php
namespace modules\sms\models;

class SmsDashboardManager extends \Smpp_Doctrine_BaseManager
{
	public function createAccount($em)
	{
		if (isset($em)){
			$account = new Account();
			$account->smppPromoCredit = '0';
			$account->smppTransCredit = '0';
			$account->smppScrubCredit = '0';
			$account->smppInterCredit = '0';
			$account->smsPromoCredit = '0';
			$account->smsScrubCredit = '0';
			$account->smsTransCredit = '0';
			$account->smsInterCredit = '0';
			$account->voicePromoCredit = '0';
			$account->voiceTransCredit = '0';
			$account->createdDate = new \DateTime('now');
			$account->updatedDate = new \DateTime('now');
			$em->persist($account);
			$em->flush();
			
			return $account;
		}
		return false;
	}
	
	public function reCredit($userId, $account, $amount, $em)
	{
		if (isset($em)){
			
			$userRepo = $em->getRepository('modules\user\models\User')->find($userId);
			switch ($account) {
				case 1:
					//PROMOTIONAL
					$userBalance = $userRepo->account->smppPromoCredit;
					if ($amount){
						$userBalanceAfter = $userBalance + $amount;
						$userRepo->account->smppPromoCredit = $userBalance + $amount;
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
					}
					break;
				case 2:
					//TRANSACTIONAL
					$userBalance = $userRepo->account->smppTransCredit;
					if ($amount){
						$userBalanceAfter = $userBalance + $amount;
						$userRepo->account->smppTransCredit = $userBalance + $amount;
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
					}
					break;
				case 3:
					//TRANS-SCRUB
					$userBalance = $userRepo->account->smppScrubCredit;
					if ($amount){
						$userBalanceAfter = $userBalance + $amount;
						$userRepo->account->smppScrubCredit = $userBalance + $amount;
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
					}
					break;
				case 4:
					//PROMO VOICE
					$userBalance = $userRepo->account->voicePromoCredit;
					if ($amount){
						$userBalanceAfter = $userBalance + $amount;
						$userRepo->account->voicePromoCredit = $userBalance + $amount;
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
					}
					break;
				case 5:
					//TRANS VOICE
					$userBalance = $userRepo->account->voiceTransCredit;
					if ($amount){
						$userBalanceAfter = $userBalance + $amount;
						$userRepo->account->voiceTransCredit = $userBalance + $amount;
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
					}
					break;
				case 6:
					//TRANS SMS
					$userBalance = $userRepo->account->smsTransCredit;
					if ($amount){
						$userBalanceAfter = $userBalance + $amount;
						$userRepo->account->smsTransCredit = $userBalance + $amount;
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
					}
					break;
				case 7:
					//PROMO SMS
					$userBalance = $userRepo->account->smsPromoCredit;
					if ($amount){
						$userBalanceAfter = $userBalance + $amount;
						$userRepo->account->smsPromoCredit = $userBalance + $amount;
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
					}
					break;
				case 10:
						//PROMO VOICE API
						$userBalance = $userRepo->account->voiceTransApiCredit;
						if ($amount){
							$userBalanceAfter = $userBalance + $amount;
							$userRepo->account->voiceTransApiCredit = $userBalance + $amount;
							$em->persist($userRepo);
							$em->flush();
								
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
						}
						break;
				case 11:
						//PROMO VOICE API
						$userBalance = $userRepo->account->voicePromoApiCredit;
						if ($amount){
							$userBalanceAfter = $userBalance + $amount;
							$userRepo->account->voicePromoApiCredit = $userBalance + $amount;
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
						}
						break;
				case 12:
						//SCRUB SMS
						$userBalance = $userRepo->account->smsScrubCredit;
						if ($amount){
							$userBalanceAfter = $userBalance + $amount;
							$userRepo->account->smsScrubCredit = $userBalance + $amount;
							$em->persist($userRepo);
							$em->flush();
								
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
						}
						break;
				case 13:
						//SCRUB SMS API
						$userBalance = $userRepo->account->smsScrubApiCredit;
						if ($amount){
							$userBalanceAfter = $userBalance + $amount;
							$userRepo->account->smsScrubApiCredit = $userBalance + $amount;
							$em->persist($userRepo);
							$em->flush();
								
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
						}
						break;

				case 8:
						//TRANS SMS API
						$userBalance = $userRepo->account->smsTransApiCredit;
						if ($amount){
							$userBalanceAfter = $userBalance + $amount;
							$userRepo->account->smsTransApiCredit = $userBalance + $amount;
							$em->persist($userRepo);
							$em->flush();
								
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
						}
						break;

				case 9:
						//PROMO SMS API
						$userBalance = $userRepo->account->smsPromoApiCredit;
						if ($amount){
							$userBalanceAfter = $userBalance + $amount;
							$userRepo->account->smsPromoApiCredit = $userBalance + $amount;
							$em->persist($userRepo);
							$em->flush();
								
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
						}
						break;		
				case 14:
					//PREMI SMS
					$userBalance = $userRepo->account->smsPremiCredit;
					if ($amount){
						$userBalanceAfter = $userBalance + $amount;
						$userRepo->account->smsPremiCredit = $userBalance + $amount;
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
					}
					break;

				case 15:
					//PREMI API SMS
					$userBalance = $userRepo->account->smsPremiApiCredit;
					if ($amount){
						$userBalanceAfter = $userBalance + $amount;
						$userRepo->account->smsPremiApiCredit = $userBalance + $amount;
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $userRepo, $amount, '0', '0', $amount, '0', '0', $userBalance, $userBalanceAfter, 'RECREDIT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $userRepo, 'log' => $createLog);
					}
					break;
			}
		}
	}
	
	public function setCredit($transactionValues, $resellerId, $account, $em)
	{
		$amount = $transactionValues['amount'];
		$tax = $transactionValues['tax'];
		$rate = $transactionValues['rate'];
		$total = $transactionValues['total'];
		$userDomain = explode(chr(124),$transactionValues['username']);
		$username = trim($userDomain['0']);
		$domain = trim($userDomain['1']);		
		$domainRepo = $em->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
		$resellerRepo = $em->getRepository('modules\user\models\User')->find($resellerId);
		if (isset($domainRepo)){
			$userRepo = $em->getRepository('modules\user\models\User')->findOneBy(array('username' => "$username",'domain' => "$domainRepo->id",'reseller' => "$resellerId"));
		}
		if (isset($userRepo) && isset($domainRepo)){
			switch ($account) {
					case 1:
						//PROMOTIONAL
						$resellerBalance = $resellerRepo->account->smppPromoCredit;
						$userBalance = $userRepo->account->smppPromoCredit;
						if ($resellerBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance - $amount;
							$userBalanceAfter = $userBalance + $amount;
							$resellerRepo->account->smppPromoCredit = $resellerBalanceAfter;
							$userRepo->account->smppPromoCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
							
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 2:
						//TRANSACTIONAL
						$resellerBalance = $resellerRepo->account->smppTransCredit;
						$userBalance = $userRepo->account->smppTransCredit;
						if ($resellerBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance - $amount;
							$userBalanceAfter = $userBalance + $amount;
							$resellerRepo->account->smppTransCredit = $resellerBalanceAfter;
							$userRepo->account->smppTransCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
							
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 3:
						//TRANS-SCRUB
						$resellerBalance = $resellerRepo->account->smppScrubCredit;
						$userBalance = $userRepo->account->smppScrubCredit;
						if ($resellerBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance - $amount;
							$userBalanceAfter = $userBalance + $amount;
							$resellerRepo->account->smppScrubCredit = $resellerBalanceAfter;
							$userRepo->account->smppScrubCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
	
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 4:
						//PROMO VOICE
						$resellerBalance = $resellerRepo->account->voicePromoCredit;
						$userBalance = $userRepo->account->voicePromoCredit;
						if ($resellerBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance - $amount;
							$userBalanceAfter = $userBalance + $amount;
							$resellerRepo->account->voicePromoCredit = $resellerBalanceAfter;
							$userRepo->account->voicePromoCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 5:
						//TRANS VOICE
						$resellerBalance = $resellerRepo->account->voiceTransCredit;
						$userBalance = $userRepo->account->voiceTransCredit;
						if ($resellerBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance - $amount;
							$userBalanceAfter = $userBalance + $amount;
							$resellerRepo->account->voiceTransCredit = $resellerBalanceAfter;
							$userRepo->account->voiceTransCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 6:
						//TRANS SMS
						$resellerBalance = $resellerRepo->account->smsTransCredit;
						$userBalance = $userRepo->account->smsTransCredit;
						if ($resellerBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance - $amount;
							$userBalanceAfter = $userBalance + $amount;
							$resellerRepo->account->smsTransCredit = $resellerBalanceAfter;
							$userRepo->account->smsTransCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 7:
						//PROMO SMS
						$resellerBalance = $resellerRepo->account->smsPromoCredit;
						$userBalance = $userRepo->account->smsPromoCredit;
						if ($resellerBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance - $amount;
							$userBalanceAfter = $userBalance + $amount;
							$resellerRepo->account->smsPromoCredit = $resellerBalanceAfter;
							$userRepo->account->smsPromoCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 8:
							//TRANS SMS API
							$resellerBalance = $resellerRepo->account->smsTransApiCredit;
							$userBalance = $userRepo->account->smsTransApiCredit;
							if ($resellerBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance - $amount;
								$userBalanceAfter = $userBalance + $amount;
								$resellerRepo->account->smsTransApiCredit = $resellerBalanceAfter;
								$userRepo->account->smsTransApiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;						
					case 9:
							//PROMO SMS API
							$resellerBalance = $resellerRepo->account->smsPromoApiCredit;
							$userBalance = $userRepo->account->smsPromoApiCredit;
							if ($resellerBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance - $amount;
								$userBalanceAfter = $userBalance + $amount;
								$resellerRepo->account->smsPromoApiCredit = $resellerBalanceAfter;
								$userRepo->account->smsPromoApiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
					case 10:
								//TRANS VOICE API
								$resellerBalance = $resellerRepo->account->voiceTransApiCredit;
								$userBalance = $userRepo->account->voiceTransApiCredit;
								if ($resellerBalance >= $amount){
									//Add Balance To User
									$resellerBalanceAfter = $resellerBalance - $amount;
									$userBalanceAfter = $userBalance + $amount;
									$resellerRepo->account->voiceTransApiCredit = $resellerBalanceAfter;
									$userRepo->account->voiceTransApiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
										
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;
					case 11:
								//PROMO VOICE API
								$resellerBalance = $resellerRepo->account->voicePromoApiCredit;
								$userBalance = $userRepo->account->voicePromoApiCredit;
								if ($resellerBalance >= $amount){
									//Add Balance To User
									$resellerBalanceAfter = $resellerBalance - $amount;
									$userBalanceAfter = $userBalance + $amount;
									$resellerRepo->account->voicePromoApiCredit = $resellerBalanceAfter;
									$userRepo->account->voicePromoApiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
										
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;	
						case 12:
									//SCRUB SMS 
									$resellerBalance = $resellerRepo->account->smsScrubCredit;
									$userBalance = $userRepo->account->smsScrubCredit;
									if ($resellerBalance >= $amount){
										//Add Balance To User
										$resellerBalanceAfter = $resellerBalance - $amount;
										$userBalanceAfter = $userBalance + $amount;
										$resellerRepo->account->smsScrubCredit = $resellerBalanceAfter;
										$userRepo->account->smsScrubCredit = $userBalanceAfter;
										$em->persist($resellerRepo);
										$em->persist($userRepo);
										$em->flush();
								
										$accountLog = new AccountLogManager();
										$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
										return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
									}
									break;
						case 13:
										//SCRUB SMS API
										$resellerBalance = $resellerRepo->account->smsScrubApiCredit;
										$userBalance = $userRepo->account->smsScrubApiCredit;
										if ($resellerBalance >= $amount){
											//Add Balance To User
											$resellerBalanceAfter = $resellerBalance - $amount;
											$userBalanceAfter = $userBalance + $amount;
											$resellerRepo->account->smsScrubApiCredit = $resellerBalanceAfter;
											$userRepo->account->smsScrubApiCredit = $userBalanceAfter;
											$em->persist($resellerRepo);
											$em->persist($userRepo);
											$em->flush();
									
											$accountLog = new AccountLogManager();
											$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
											return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
										}
										break;
						case 29:
							//PREMI SMS 
							$resellerBalance = $resellerRepo->account->smsPremiCredit;
							$userBalance = $userRepo->account->smsPremiCredit;
							if ($resellerBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance - $amount;
								$userBalanceAfter = $userBalance + $amount;
								$resellerRepo->account->smsPremiCredit = $resellerBalanceAfter;
								$userRepo->account->smsPremiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
						case 30:
								//PREMI SMS API
								$resellerBalance = $resellerRepo->account->smsPremiApiCredit;
								$userBalance = $userRepo->account->smsPremiApiCredit;
								if ($resellerBalance >= $amount){
									//Add Balance To User
									$resellerBalanceAfter = $resellerBalance - $amount;
									$userBalanceAfter = $userBalance + $amount;
									$resellerRepo->account->smsPremiApiCredit = $resellerBalanceAfter;
									$userRepo->account->smsPremiApiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
										
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;
								
						case 31:
							//PREMI VOICE
							$resellerBalance = $resellerRepo->account->voicePremiCredit;
							$userBalance = $userRepo->account->voicePremiCredit;
							if ($resellerBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance - $amount;
								$userBalanceAfter = $userBalance + $amount;
								$resellerRepo->account->voicePremiCredit = $resellerBalanceAfter;
								$userRepo->account->voicePremiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
						case 32:
							//PREMI VOICE API
							$resellerBalance = $resellerRepo->account->voicePremiApiCredit;
							$userBalance = $userRepo->account->voicePremiApiCredit;
							if ($resellerBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance - $amount;
								$userBalanceAfter = $userBalance + $amount;
								$resellerRepo->account->voicePremiApiCredit = $resellerBalanceAfter;
								$userRepo->account->voicePremiApiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
						
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
							case 26:
											//INTER SMS
											$resellerBalance = $resellerRepo->account->smsInterCredit;
											$userBalance = $userRepo->account->smsInterCredit;
											if ($resellerBalance >= $amount){
												//Add Balance To User
												$resellerBalanceAfter = $resellerBalance - $amount;
												$userBalanceAfter = $userBalance + $amount;
												$resellerRepo->account->smsInterCredit = $resellerBalanceAfter;
												$userRepo->account->smsInterCredit = $userBalanceAfter;
												$em->persist($resellerRepo);
												$em->persist($userRepo);
												$em->flush();
										
												$accountLog = new AccountLogManager();
												$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
												return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
											}
											break;
							case 27:
											//INTER SMS API
											$resellerBalance = $resellerRepo->account->smsInterApiCredit;
											$userBalance = $userRepo->account->smsInterApiCredit;
											if ($resellerBalance >= $amount){
												//Add Balance To User
												$resellerBalanceAfter = $resellerBalance - $amount;
												$userBalanceAfter = $userBalance + $amount;
												$resellerRepo->account->smsInterApiCredit = $resellerBalanceAfter;
												$userRepo->account->smsInterApiCredit = $userBalanceAfter;
												$em->persist($resellerRepo);
												$em->persist($userRepo);
												$em->flush();
													
												$accountLog = new AccountLogManager();
												$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
												return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
											}
											break;
							case 28:
												//INTER SMPP
												$resellerBalance = $resellerRepo->account->smppInterCredit;
												$userBalance = $userRepo->account->smppInterCredit;
												if ($resellerBalance >= $amount){
													//Add Balance To User
													$resellerBalanceAfter = $resellerBalance - $amount;
													$userBalanceAfter = $userBalance + $amount;
													$resellerRepo->account->smppInterCredit = $resellerBalanceAfter;
													$userRepo->account->smppInterCredit = $userBalanceAfter;
													$em->persist($resellerRepo);
													$em->persist($userRepo);
													$em->flush();
														
													$accountLog = new AccountLogManager();
													$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
													return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
												}
												break;
			}
		}		
		return false;
	}
	
	public function setDebit($transactionValues, $resellerId, $account, $em)
	{
		$amount = $transactionValues['amount'];
		$tax = $transactionValues['tax'];
		$rate = $transactionValues['rate'];
		$total = $transactionValues['total'];
		$userDomain = explode(chr(124),$transactionValues['username']);
		$username = trim($userDomain['0']);
		$domain = trim($userDomain['1']);		
		$domainRepo = $em->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
		$resellerRepo = $em->getRepository('modules\user\models\User')->find($resellerId);
		if (isset($domainRepo)){
			$userRepo = $em->getRepository('modules\user\models\User')->findOneBy(array('username' => "$username",'domain' => "$domainRepo->id",'reseller' => "$resellerId"));
		}
		if (isset($userRepo) && isset($domainRepo)){
			switch ($account) {
					case 1:
						//PROMOTIONAL
						$resellerBalance = $resellerRepo->account->smppPromoCredit;
						$userBalance = $userRepo->account->smppPromoCredit;
						if ($userBalance >= $amount){
							//Deduct Balance From User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->account->smppPromoCredit = $resellerBalanceAfter;
							$userRepo->account->smppPromoCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
							
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 2:
						//TRANSACTIONAL
						$resellerBalance = $resellerRepo->account->smppTransCredit;
						$userBalance = $userRepo->account->smppTransCredit;
						if ($userBalance >= $amount){
							//Deduct Balance From User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->account->smppTransCredit = $resellerBalanceAfter;
							$userRepo->account->smppTransCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
							
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 3:
						//TRANS-SCRUB
						$resellerBalance = $resellerRepo->account->smppScrubCredit;
						$userBalance = $userRepo->account->smppScrubCredit;
						if ($userBalance >= $amount){
							//Deduct Balance From User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->account->smppScrubCredit = $resellerBalanceAfter;
							$userRepo->account->smppScrubCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
							
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 4:
						//PROMO VOICE
						$resellerBalance = $resellerRepo->account->voicePromoCredit;
						$userBalance = $userRepo->account->voicePromoCredit;
						if ($userBalance >= $amount){
							//Deduct Balance From User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->account->voicePromoCredit = $resellerBalanceAfter;
							$userRepo->account->voicePromoCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 5:
						//TRANS VOICE
						$resellerBalance = $resellerRepo->account->voiceTransCredit;
						$userBalance = $userRepo->account->voiceTransCredit;
						if ($userBalance >= $amount){
							//Deduct Balance From User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->account->voiceTransCredit = $resellerBalanceAfter;
							$userRepo->account->voiceTransCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 6:
						//TRANS SMS
						$resellerBalance = $resellerRepo->account->smsTransCredit;
						$userBalance = $userRepo->account->smsTransCredit;
						if ($userBalance >= $amount){
							//Deduct Balance From User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->account->smsTransCredit = $resellerBalanceAfter;
							$userRepo->account->smsTransCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 7:
						//PROMO SMS
						$resellerBalance = $resellerRepo->account->smsPromoCredit;
						$userBalance = $userRepo->account->smsPromoCredit;
						if ($userBalance >= $amount){
							//Deduct Balance From User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->account->smsPromoCredit = $resellerBalanceAfter;
							$userRepo->account->smsPromoCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
						
					case 8:
							//TRANS SMS API
							$resellerBalance = $resellerRepo->account->smsTransApiCredit;
							$userBalance = $userRepo->account->smsTransApiCredit;
							if ($userBalance >= $amount){
								//Deduct Balance From User
								$resellerBalanceAfter = $resellerBalance + $amount;
								$userBalanceAfter = $userBalance - $amount;
								$resellerRepo->account->smsTransApiCredit = $resellerBalanceAfter;
								$userRepo->account->smsTransApiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
					case 9:
							//PROMO SMS API
							$resellerBalance = $resellerRepo->account->smsPromoApiCredit;
							$userBalance = $userRepo->account->smsPromoApiCredit;
							if ($userBalance >= $amount){
								//Deduct Balance From User
								$resellerBalanceAfter = $resellerBalance + $amount;
								$userBalanceAfter = $userBalance - $amount;
								$resellerRepo->account->smsPromoApiCredit = $resellerBalanceAfter;
								$userRepo->account->smsPromoApiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
							
					 case 10:
								//PROMO VOICE
								$resellerBalance = $resellerRepo->account->voicePromoApiCredit;
								$userBalance = $userRepo->account->voicePromoApiCredit;
								if ($userBalance >= $amount){
									//Deduct Balance From User
									$resellerBalanceAfter = $resellerBalance + $amount;
									$userBalanceAfter = $userBalance - $amount;
									$resellerRepo->account->voicePromoApiCredit = $resellerBalanceAfter;
									$userRepo->account->voicePromoApiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
										
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;
						case 11:
								//TRANS VOICE
								$resellerBalance = $resellerRepo->account->voiceTransApiCredit;
								$userBalance = $userRepo->account->voiceTransApiCredit;
								if ($userBalance >= $amount){
									//Deduct Balance From User
									$resellerBalanceAfter = $resellerBalance + $amount;
									$userBalanceAfter = $userBalance - $amount;
									$resellerRepo->account->voiceTransApiCredit = $resellerBalanceAfter;
									$userRepo->account->voiceTransApiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
										
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;
					case 12:
									//TRANS VOICE
									$resellerBalance = $resellerRepo->account->smsScrubCredit;
									$userBalance = $userRepo->account->smsScrubCredit;
									if ($userBalance >= $amount){
										//Deduct Balance From User
										$resellerBalanceAfter = $resellerBalance + $amount;
										$userBalanceAfter = $userBalance - $amount;
										$resellerRepo->account->smsScrubCredit = $resellerBalanceAfter;
										$userRepo->account->smsScrubCredit = $userBalanceAfter;
										$em->persist($resellerRepo);
										$em->persist($userRepo);
										$em->flush();
								
										$accountLog = new AccountLogManager();
										$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
										return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
									}
									break;
						case 13:
										//TRANS VOICE
										$resellerBalance = $resellerRepo->account->smsScrubApiCredit;
										$userBalance = $userRepo->account->smsScrubApiCredit;
										if ($userBalance >= $amount){
											//Deduct Balance From User
											$resellerBalanceAfter = $resellerBalance + $amount;
											$userBalanceAfter = $userBalance - $amount;
											$resellerRepo->account->smsScrubApiCredit = $resellerBalanceAfter;
											$userRepo->account->smsScrubApiCredit = $userBalanceAfter;
											$em->persist($resellerRepo);
											$em->persist($userRepo);
											$em->flush();
									
											$accountLog = new AccountLogManager();
											$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
											return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
										}
										break;
						case 29:
							//PREMI SMS
							$resellerBalance = $resellerRepo->account->smsPremiCredit;
							$userBalance = $userRepo->account->smsPremiCredit;
							if ($userBalance >= $amount){
								//Deduct Balance From User
								$resellerBalanceAfter = $resellerBalance + $amount;
								$userBalanceAfter = $userBalance - $amount;
								$resellerRepo->account->smsPremiCredit = $resellerBalanceAfter;
								$userRepo->account->smsPremiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
						case 30:
							//PREMI SMS API
							$resellerBalance = $resellerRepo->account->smsPremiApiCredit;
							$userBalance = $userRepo->account->smsPremiApiCredit;
							if ($userBalance >= $amount){
								//Deduct Balance From User
								$resellerBalanceAfter = $resellerBalance + $amount;
								$userBalanceAfter = $userBalance - $amount;
								$resellerRepo->account->smsPremiApiCredit = $resellerBalanceAfter;
								$userRepo->account->smsPremiApiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
							case 31:
								//PREMI VOICE
								$resellerBalance = $resellerRepo->account->voicePremiCredit;
								$userBalance = $userRepo->account->voicePremiCredit;
								if ($userBalance >= $amount){
									//Deduct Balance From User
									$resellerBalanceAfter = $resellerBalance + $amount;
									$userBalanceAfter = $userBalance - $amount;
									$resellerRepo->account->voicePremiCredit = $resellerBalanceAfter;
									$userRepo->account->voicePremiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
										
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;
							case 32:
								//PREMI VOICE API
								$resellerBalance = $resellerRepo->account->voicePremiApiCredit;
								$userBalance = $userRepo->account->voicePremiApiCredit;
								if ($userBalance >= $amount){
									//Deduct Balance From User
									$resellerBalanceAfter = $resellerBalance + $amount;
									$userBalanceAfter = $userBalance - $amount;
									$resellerRepo->account->voicePremiApiCredit = $resellerBalanceAfter;
									$userRepo->account->voicePremiApiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
										
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;
							case 26:
								//TRANS VOICE
								$resellerBalance = $resellerRepo->account->smsInterCredit;
								$userBalance = $userRepo->account->smsInterCredit;
								if ($userBalance >= $amount){
									//Deduct Balance From User
									$resellerBalanceAfter = $resellerBalance + $amount;
									$userBalanceAfter = $userBalance - $amount;
									$resellerRepo->account->smsInterCredit = $resellerBalanceAfter;
									$userRepo->account->smsInterCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
							
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;
							case 27:
											//TRANS VOICE
											$resellerBalance = $resellerRepo->account->smsInterApiCredit;
											$userBalance = $userRepo->account->smsInterApiCredit;
											if ($userBalance >= $amount){
												//Deduct Balance From User
												$resellerBalanceAfter = $resellerBalance + $amount;
												$userBalanceAfter = $userBalance - $amount;
												$resellerRepo->account->smsInterApiCredit = $resellerBalanceAfter;
												$userRepo->account->smsInterApiCredit = $userBalanceAfter;
												$em->persist($resellerRepo);
												$em->persist($userRepo);
												$em->flush();
													
												$accountLog = new AccountLogManager();
												$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
												return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
											}
											break;
											
								case 28:
												//INTER SMPP
												$resellerBalance = $resellerRepo->account->smppInterCredit;
												$userBalance = $userRepo->account->smppInterCredit;
												if ($userBalance >= $amount){
													//Deduct Balance From User
													$resellerBalanceAfter = $resellerBalance + $amount;
													$userBalanceAfter = $userBalance - $amount;
													$resellerRepo->account->smppInterCredit = $resellerBalanceAfter;
													$userRepo->account->smppInterCredit = $userBalanceAfter;
													$em->persist($resellerRepo);
													$em->persist($userRepo);
													$em->flush();
														
													$accountLog = new AccountLogManager();
													$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
													return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
												}
												break;
				
			}
		}		
		return false;
	}
	
	public function setMainCredit($transactionValues, $resellerId, $account, $em)
	{
		$amount = $transactionValues['amount'];
		$tax = $transactionValues['tax'];
		$rate = $transactionValues['rate'];
		$total = $transactionValues['total'];
		$userDomain = explode(chr(124),$transactionValues['username']);
		$username = trim($userDomain['0']);
		$domain = trim($userDomain['1']);
		$domainRepo = $em->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
		$resellerRepo = $em->getRepository('modules\user\models\User')->find($resellerId);
		if (isset($domainRepo)){
			$userRepo = $em->getRepository('modules\user\models\User')->findOneBy(array('username' => "$username",'domain' => "$domainRepo->id",'reseller' => "$resellerId"));
		}
		if (isset($userRepo) && isset($domainRepo)){
			switch ($account) {
					
				case 14:
					//PROMO VOICE
					$resellerBalance = $resellerRepo->domain->voicePromoCredit;
					$userBalance = $userRepo->domain->voicePromoCredit;
					if ($resellerBalance >= $amount){
						//Add Balance To User
						$resellerBalanceAfter = $resellerBalance - $amount;
						$userBalanceAfter = $userBalance + $amount;
						$resellerRepo->domain->voicePromoCredit = $resellerBalanceAfter;
						$userRepo->domain->voicePromoCredit = $userBalanceAfter;
						$em->persist($resellerRepo);
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
					}
					break;
				case 15:
					//TRANS VOICE
					$resellerBalance = $resellerRepo->domain->voiceTransCredit;
					$userBalance = $userRepo->domain->voiceTransCredit;
					if ($resellerBalance >= $amount){
						//Add Balance To User
						$resellerBalanceAfter = $resellerBalance - $amount;
						$userBalanceAfter = $userBalance + $amount;
						$resellerRepo->domain->voiceTransCredit = $resellerBalanceAfter;
						$userRepo->domain->voiceTransCredit = $userBalanceAfter;
						$em->persist($resellerRepo);
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
					}
					break;
				case 16:
					//TRANS SMS
					$resellerBalance = $resellerRepo->domain->smsTransCredit;
					$userBalance = $userRepo->domain->smsTransCredit;
					if ($resellerBalance >= $amount){
						//Add Balance To User
						$resellerBalanceAfter = $resellerBalance - $amount;
						$userBalanceAfter = $userBalance + $amount;
						$resellerRepo->domain->smsTransCredit = $resellerBalanceAfter;
						$userRepo->domain->smsTransCredit = $userBalanceAfter;
						$em->persist($resellerRepo);
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
					}
					break;
				case 17:
					//PROMO SMS
					$resellerBalance = $resellerRepo->domain->smsPromoCredit;
					$userBalance = $userRepo->domain->smsPromoCredit;
					if ($resellerBalance >= $amount){
						//Add Balance To User
						$resellerBalanceAfter = $resellerBalance - $amount;
						$userBalanceAfter = $userBalance + $amount;
						$resellerRepo->domain->smsPromoCredit = $resellerBalanceAfter;
						$userRepo->domain->smsPromoCredit = $userBalanceAfter;
						$em->persist($resellerRepo);
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
					}
					break;
					case 18:
						//TRANS SMS API
						$resellerBalance = $resellerRepo->domain->smsTransApiCredit;
						$userBalance = $userRepo->domain->smsTransApiCredit;
						if ($resellerBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance - $amount;
							$userBalanceAfter = $userBalance + $amount;
							$resellerRepo->domain->smsTransApiCredit = $resellerBalanceAfter;
							$userRepo->domain->smsTransApiCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
								
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 19:
						//PROMO SMS API
						$resellerBalance = $resellerRepo->domain->smsPromoApiCredit;
						$userBalance = $userRepo->domain->smsPromoApiCredit;
						if ($resellerBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance - $amount;
							$userBalanceAfter = $userBalance + $amount;
							$resellerRepo->domain->smsPromoApiCredit = $resellerBalanceAfter;
							$userRepo->domain->smsPromoApiCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
								
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;	
						case 20:
							//TRANS VOICE API
							$resellerBalance = $resellerRepo->domain->voiceTransApiCredit;
							$userBalance = $userRepo->domain->voiceTransApiCredit;
							if ($resellerBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance - $amount;
								$userBalanceAfter = $userBalance + $amount;
								$resellerRepo->domain->voiceTransApiCredit = $resellerBalanceAfter;
								$userRepo->domain->voiceTransApiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
						
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
						case 21:
							//PROMO VOICE API
							$resellerBalance = $resellerRepo->domain->voicePromoApiCredit;
							$userBalance = $userRepo->domain->voicePromoApiCredit;
							if ($resellerBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance - $amount;
								$userBalanceAfter = $userBalance + $amount;
								$resellerRepo->domain->voicePromoApiCredit = $resellerBalanceAfter;
								$userRepo->domain->voicePromoApiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
						
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;		
						case 22:
								//SCRUB SMS
								$resellerBalance = $resellerRepo->domain->smsScrubCredit;
								$userBalance = $userRepo->domain->smsScrubCredit;
								if ($resellerBalance >= $amount){
									//Add Balance To User
									$resellerBalanceAfter = $resellerBalance - $amount;
									$userBalanceAfter = $userBalance + $amount;
									$resellerRepo->domain->smsScrubCredit = $resellerBalanceAfter;
									$userRepo->domain->smsScrubCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
							
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;
						case 23:
									//SCRUB SMS API
									$resellerBalance = $resellerRepo->domain->smsScrubApiCredit;
									$userBalance = $userRepo->domain->smsScrubApiCredit;
									if ($resellerBalance >= $amount){
										//Add Balance To User
										$resellerBalanceAfter = $resellerBalance - $amount;
										$userBalanceAfter = $userBalance + $amount;
										$resellerRepo->domain->smsScrubApiCredit = $resellerBalanceAfter;
										$userRepo->domain->smsScrubApiCredit = $userBalanceAfter;
										$em->persist($resellerRepo);
										$em->persist($userRepo);
										$em->flush();
								
										$accountLog = new AccountLogManager();
										$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
										return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
									}
									break;
						case 24:
										//INTER
										$resellerBalance = $resellerRepo->domain->smsInterCredit;
										$userBalance = $userRepo->domain->smsInterCredit;
										if ($resellerBalance >= $amount){
											//Add Balance To User
											$resellerBalanceAfter = $resellerBalance - $amount;
											$userBalanceAfter = $userBalance + $amount;
											$resellerRepo->domain->smsInterCredit = $resellerBalanceAfter;
											$userRepo->domain->smsInterCredit = $userBalanceAfter;
											$em->persist($resellerRepo);
											$em->persist($userRepo);
											$em->flush();
									
											$accountLog = new AccountLogManager();
											$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
											return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
										}
										break;
						case 25:
										//INTER API
										$resellerBalance = $resellerRepo->domain->smsInterApiCredit;
										$userBalance = $userRepo->domain->smsInterApiCredit;
										if ($resellerBalance >= $amount){
											//Add Balance To User
											$resellerBalanceAfter = $resellerBalance - $amount;
											$userBalanceAfter = $userBalance + $amount;
											$resellerRepo->domain->smsInterApiCredit = $resellerBalanceAfter;
											$userRepo->domain->smsInterApiCredit = $userBalanceAfter;
											$em->persist($resellerRepo);
											$em->persist($userRepo);
											$em->flush();
									
											$accountLog = new AccountLogManager();
											$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
											return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
										}
										break;	
						case 33:
							//PREMI
							$resellerBalance = $resellerRepo->domain->smsPremiCredit;
							$userBalance = $userRepo->domain->smsPremiCredit;
							if ($resellerBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance - $amount;
								$userBalanceAfter = $userBalance + $amount;
								$resellerRepo->domain->smsPremiCredit = $resellerBalanceAfter;
								$userRepo->domain->smsPremiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
						case 34:
							//PREMI API
							$resellerBalance = $resellerRepo->domain->smsPremiApiCredit;
							$userBalance = $userRepo->domain->smsPremiApiCredit;
							if ($resellerBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance - $amount;
								$userBalanceAfter = $userBalance + $amount;
								$resellerRepo->domain->smsPremiApiCredit = $resellerBalanceAfter;
								$userRepo->domain->smsPremiApiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
							case 35:
								//PREMI VOICE
								$resellerBalance = $resellerRepo->domain->voicePremiCredit;
								$userBalance = $userRepo->domain->voicePremiCredit;
								if ($resellerBalance >= $amount){
									//Add Balance To User
									$resellerBalanceAfter = $resellerBalance - $amount;
									$userBalanceAfter = $userBalance + $amount;
									$resellerRepo->domain->voicePremiCredit = $resellerBalanceAfter;
									$userRepo->domain->voicePremiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
										
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;
							case 36:
								//PREMI VOICE API
								$resellerBalance = $resellerRepo->domain->voicePremiApiCredit;
								$userBalance = $userRepo->domain->voicePremiApiCredit;
								if ($resellerBalance >= $amount){
									//Add Balance To User
									$resellerBalanceAfter = $resellerBalance - $amount;
									$userBalanceAfter = $userBalance + $amount;
									$resellerRepo->domain->voicePremiApiCredit = $resellerBalanceAfter;
									$userRepo->domain->voicePremiApiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
										
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;							
						
			}
		}
		return false;
	}
	
	public function setMainDebit($transactionValues, $resellerId, $account, $em)
	{
		$amount = $transactionValues['amount'];
		$tax = $transactionValues['tax'];
		$rate = $transactionValues['rate'];
		$total = $transactionValues['total'];
		$userDomain = explode(chr(124),$transactionValues['username']);
		$username = trim($userDomain['0']);
		$domain = trim($userDomain['1']);
		$domainRepo = $em->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
		$resellerRepo = $em->getRepository('modules\user\models\User')->find($resellerId);
		if (isset($domainRepo)){
			$userRepo = $em->getRepository('modules\user\models\User')->findOneBy(array('username' => "$username",'domain' => "$domainRepo->id",'reseller' => "$resellerId"));
		}
		if (isset($userRepo) && isset($domainRepo)){
			switch ($account) {
					
				case 14:
					//PROMO VOICE
					$resellerBalance = $resellerRepo->domain->voicePromoCredit;
					$userBalance = $userRepo->domain->voicePromoCredit;
					if ($userBalance >= $amount){
						//Deduct Balance From User
						$resellerBalanceAfter = $resellerBalance + $amount;
						$userBalanceAfter = $userBalance - $amount;
						$resellerRepo->domain->voicePromoCredit = $resellerBalanceAfter;
						$userRepo->domain->voicePromoCredit = $userBalanceAfter;
						$em->persist($resellerRepo);
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
					}
					break;
				case 15:
					//TRANS VOICE
					$resellerBalance = $resellerRepo->domain->voiceTransCredit;
					$userBalance = $userRepo->domain->voiceTransCredit;
					if ($userBalance >= $amount){
						//Deduct Balance From User
						$resellerBalanceAfter = $resellerBalance + $amount;
						$userBalanceAfter = $userBalance - $amount;
						$resellerRepo->domain->voiceTransCredit = $resellerBalanceAfter;
						$userRepo->domain->voiceTransCredit = $userBalanceAfter;
						$em->persist($resellerRepo);
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
					}
					break;
				case 16:
					//TRANS SMS
					$resellerBalance = $resellerRepo->domain->smsTransCredit;
					$userBalance = $userRepo->domain->smsTransCredit;
					if ($userBalance >= $amount){
						//Deduct Balance From User
						$resellerBalanceAfter = $resellerBalance + $amount;
						$userBalanceAfter = $userBalance - $amount;
						$resellerRepo->domain->smsTransCredit = $resellerBalanceAfter;
						$userRepo->domain->smsTransCredit = $userBalanceAfter;
						$em->persist($resellerRepo);
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
					}
					break;
				case 17:
					//PROMO SMS
					$resellerBalance = $resellerRepo->domain->smsPromoCredit;
					$userBalance = $userRepo->domain->smsPromoCredit;
					if ($userBalance >= $amount){
						//Deduct Balance From User
						$resellerBalanceAfter = $resellerBalance + $amount;
						$userBalanceAfter = $userBalance - $amount;
						$resellerRepo->domain->smsPromoCredit = $resellerBalanceAfter;
						$userRepo->domain->smsPromoCredit = $userBalanceAfter;
						$em->persist($resellerRepo);
						$em->persist($userRepo);
						$em->flush();
							
						$accountLog = new AccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
					}
					break;
					case 18:
						//TRANS SMS API
						$resellerBalance = $resellerRepo->domain->smsTransApiCredit;
						$userBalance = $userRepo->domain->smsTransApiCredit;
						if ($userBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->domain->smsTransApiCredit = $resellerBalanceAfter;
							$userRepo->domain->smsTransApiCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 19:
						//PROMO SMS API
						$resellerBalance = $resellerRepo->domain->smsPromoApiCredit;
						$userBalance = $userRepo->domain->smsPromoApiCredit;
						if ($userBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->domain->smsPromoApiCredit = $resellerBalanceAfter;
							$userRepo->domain->smsPromoApiCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 20:
						//TRANS VOICE API
						$resellerBalance = $resellerRepo->domain->voiceTransApiCredit;
						$userBalance = $userRepo->domain->voiceTransApiCredit;
						if ($userBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->domain->voiceTransApiCredit = $resellerBalanceAfter;
							$userRepo->domain->voiceTransApiCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 21:
						//PROMO VOICE API
						$resellerBalance = $resellerRepo->domain->voicePromoApiCredit;
						$userBalance = $userRepo->domain->voicePromoApiCredit;
						if ($userBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->domain->voicePromoApiCredit = $resellerBalanceAfter;
							$userRepo->domain->voicePromoApiCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 22:
						//PROMO VOICE API
						$resellerBalance = $resellerRepo->domain->smsScrubCredit;
						$userBalance = $userRepo->domain->smsScrubCredit;
						if ($userBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->domain->smsScrubCredit = $resellerBalanceAfter;
							$userRepo->domain->smsScrubCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
								
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
					case 23:
						//PROMO VOICE API
						$resellerBalance = $resellerRepo->domain->smsScrubApiCredit;
						$userBalance = $userRepo->domain->smsScrubApiCredit;
						if ($userBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->domain->smsScrubApiCredit = $resellerBalanceAfter;
							$userRepo->domain->smsScrubApiCredit = $userBalanceAfter;
							$em->persist($resellerRepo);
							$em->persist($userRepo);
							$em->flush();
					
							$accountLog = new AccountLogManager();
							$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						}
						break;
				    case 24:
							//INTER
							$resellerBalance = $resellerRepo->domain->smsInterCredit;
							$userBalance = $userRepo->domain->smsInterCredit;
							if ($userBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance + $amount;
								$userBalanceAfter = $userBalance - $amount;
								$resellerRepo->domain->smsInterCredit = $resellerBalanceAfter;
								$userRepo->domain->smsInterCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
					case 25:
								//INTER API
								$resellerBalance = $resellerRepo->domain->smsInterApiCredit;
								$userBalance = $userRepo->domain->smsInterApiCredit;
								if ($userBalance >= $amount){
									//Add Balance To User
									$resellerBalanceAfter = $resellerBalance + $amount;
									$userBalanceAfter = $userBalance - $amount;
									$resellerRepo->domain->smsInterApiCredit = $resellerBalanceAfter;
									$userRepo->domain->smsInterApiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
										
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;
						case 33:
							//PREMI 
							$resellerBalance = $resellerRepo->domain->smsPremiCredit;
							$userBalance = $userRepo->domain->smsPremiCredit;
							if ($userBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance + $amount;
								$userBalanceAfter = $userBalance - $amount;
								$resellerRepo->domain->smsPremiCredit = $resellerBalanceAfter;
								$userRepo->domain->smsPremiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
									
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
						case 34:
							//PREMI API
							$resellerBalance = $resellerRepo->domain->smsPremiApiCredit;
							$userBalance = $userRepo->domain->smsPremiApiCredit;
							if ($userBalance >= $amount){
								//Add Balance To User
								$resellerBalanceAfter = $resellerBalance + $amount;
								$userBalanceAfter = $userBalance - $amount;
								$resellerRepo->domain->smsPremiApiCredit = $resellerBalanceAfter;
								$userRepo->domain->smsPremiApiCredit = $userBalanceAfter;
								$em->persist($resellerRepo);
								$em->persist($userRepo);
								$em->flush();
						
								$accountLog = new AccountLogManager();
								$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
								return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
							}
							break;
							case 35:
								//PREMI
								$resellerBalance = $resellerRepo->domain->voicePremiCredit;
								$userBalance = $userRepo->domain->voicePremiCredit;
								if ($userBalance >= $amount){
									//Add Balance To User
									$resellerBalanceAfter = $resellerBalance + $amount;
									$userBalanceAfter = $userBalance - $amount;
									$resellerRepo->domain->voicePremiCredit = $resellerBalanceAfter;
									$userRepo->domain->voicePremiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
										
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;
							case 36:
								//PREMI API
								$resellerBalance = $resellerRepo->domain->voicePremiApiCredit;
								$userBalance = $userRepo->domain->voicePremiApiCredit;
								if ($userBalance >= $amount){
									//Add Balance To User
									$resellerBalanceAfter = $resellerBalance + $amount;
									$userBalanceAfter = $userBalance - $amount;
									$resellerRepo->domain->voicePremiApiCredit = $resellerBalanceAfter;
									$userRepo->domain->voicePremiApiCredit = $userBalanceAfter;
									$em->persist($resellerRepo);
									$em->persist($userRepo);
									$em->flush();
							
									$accountLog = new AccountLogManager();
									$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
									return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
								}
								break;							
			}
		}
		return false;
	}
	

}
