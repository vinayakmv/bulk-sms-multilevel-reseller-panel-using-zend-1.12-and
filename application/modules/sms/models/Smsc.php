<?php
namespace modules\sms\models;

use Doctrine\DBAL\Types\IntegerType;
/**
 * @Entity(repositoryClass="modules\sms\models\SmscRepository")
 * @Table(name="smsc_accounts")
 * @author Vinayak
 *
 */
class Smsc
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="system_id", type="string")
	 * @var text
	 */
	private $systemId;
	
	/**
	 * @Column(name="server", type="string")
	 * @var text
	 */
	private $server;
		
	/**
	 * @Column(name="password", type="string")
	 * @var string
	 */
	private $password;
		
	/**
	 * @Column(name="throughput", type="float")
	 * @var float
	 */
	private $throughput;
		
	/**
	 * @Column(name="smsc_id", type="string")
	 * @var string
	 */
	private $route;

	/**
	 * @Column(name="type", type="string")
	 * @var string
	 */
	private $type;
	
	/**
	 * @Column(name="port", type="integer")
	 * @var integer
	 */
	private $port;

	/**
	 * @Column(name="max_binds", type="integer")
	 * @var integer
	 */
	private $maxBinds;
        
        /**
	 * @Column(name="max_tx", type="integer", nullable=true)
	 * @var integer
	 */
	private $maxTx;
        
        /**
	 * @Column(name="max_rx", type="integer", nullable=true)
	 * @var integer
	 */
	private $maxRx;
	
	/**
	 * @column(name="status", type="integer")
	 * @var integer
	 */
	private $status;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;

	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}