<?php
namespace modules\sms\models;

/**
 * @Entity(repositoryClass="modules\sms\models\PriorityLeadRepository")
 * @Table(name="priority_leads_sms")
 * @author Vinayak
 *
 */
class SmsPriorityLead
{

	/**
	 * @Column(name="number", type="bigint", nullable=false)
	 * @Id
	 * @var string
	 */
	private $number;
	
	/**
	 * @Column(name="user_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $user;

	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}
