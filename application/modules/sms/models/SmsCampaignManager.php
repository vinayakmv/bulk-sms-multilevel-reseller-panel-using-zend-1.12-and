<?php
namespace modules\sms\models;

use modules\user\models\LeadManager;
class SmsCampaignManager extends \Smpp_Doctrine_BaseManager
{
	public function createCampaign($campaignValues, $orgText, $smsLength, $smsCount, $smsEncoding, $userId, $api, $apiKey, $emSms, $emUser, $emSmpp, $emUrl)
	{
		if (isset($campaignValues)) {
			
			if ($api){
				$method = '2';
			}else{
				$method = '1';
			}
			
			$campaignName = $campaignValues['campaignName'];
			$routeType = $campaignValues['smsType'];
			$input = $campaignValues['input'];
			$flash = $campaignValues['flashRadio'];
			$routes = explode ('|', $routeType);
			$userRepo = $emUser->getRepository('modules\user\models\User')->findOneBy(array('id' => $userId));
			$parentDomain = $userRepo->parentDomain;
			$parentDomainRepo = $emUser->getRepository('modules\user\models\Domain')->find($parentDomain);
			$parentRecredit = $parentDomainRepo->recredit;
			$dynamic = false;
			$spamMatch = '0';

			$urlStatus = $campaignValues['tinyRadio'];			

			if ($urlStatus == '1'){
				$tinyUrl = $campaignValues['selectUrl'];
				$urlRepo = $emUrl->getRepository('modules\url\models\Url')->find($tinyUrl);
				$urlType = $urlRepo->type;
				$urlLink = $urlRepo->url;					
			}else{
				  $urlLink = null;
				  $urlType = '0';		
			}

			
			switch ($routes['0']) {
				case 'PREMIUM':
					$route = 5;
					$fake = $userRepo->smsPercentagePremi;
					break;
				case 'TRANSACTIONAL':
					$route = 2;
					$fake = $userRepo->smsPercentageTrans;
					break;
				case 'PROMOTIONAL':
					$route = 1;
					$fake = $userRepo->smsPercentagePromo;
					break;
				case 'TRANS-SCRUB':
					$route = 3;
					$fake = $userRepo->smsPercentageScrub;
					break;
				case 'INTERNATIONAL':
					$route = 4;
					$fake = $userRepo->smsPercentageInter;
					break;
				default:
					return array('Campaign' => array('route' => 'Routing error failed'));
					break;
			}

			
			$openTemplate = $userRepo->openTemplate;
			$openSenderId = $userRepo->openSenderId;
			$checkSpam = $userRepo->checkSpam;
			
			$text = urlencode($campaignValues['text']); 
			$text = urldecode(str_replace('%0D%0A', '%0A', $text));
			//$text = urlencode($text);
			
			$orgText = urlencode($orgText);
			$orgText = urldecode(str_replace('%0D%0A', '%0A', $orgText));
			
			if ($openSenderId){
				$senderId = $campaignValues['typeSenderId'];
			}else{
				$senderId = $campaignValues['selectSenderId'];
			}
			
			$contactType = $campaignValues['contactType'];
			$date=date('Y-m-d', strtotime($campaignValues['datePicker']));
            $dateTime=$date.' '.$campaignValues['timePicker'];
			$duration='+15 seconds'; 
            $after = date('Y-m-d H:i:s', strtotime($duration, strtotime($dateTime))); 
			$type = '2';
			$creditValue = $smsCount;

			if (($route == '2') || ($route == '5')) {
				if ($openTemplate == '0'){
					$smsTemplateRepos = $emSms->getRepository('modules\sms\models\SmsTemplate')->findAll(array('user' => "$userId", 'status' => '1'));
					$templateMatch = '1';
					foreach ($smsTemplateRepos as $smsTemplateRepo) {
						$template = $smsTemplateRepo->template;
						$compareTemplate = $this->templateMatch($template, $text);
						if ($compareTemplate){
							$templateMatch = '0';
							break;
						}
					}
					
					if ($templateMatch != '0'){
						return array('Campaign' => array('template' => 'Template is not approved'));
						break;
					}
				}
				
				if ($openSenderId == '0'){
					$senderMatch = '1';
					$senderIdRepos = $emSms->getRepository('modules\sms\models\SmsSenderId')->findOneBy(array('name' => $senderId,'user' => $userId, 'status' => '1'));
					
					if(isset($senderIdRepos)){
						$sender = $senderIdRepos->name;
						$senderMatch = strcmp($sender, $senderId);
						if ($senderMatch != '0'){
							$senderMatch = strcasecmp($sender, $senderId);
						}
					}
					
					if ($senderMatch != '0'){
						return array('Campaign' => array('sender' => 'SenderID is not approved'));
					}
				}
				
				
				
				if ($checkSpam == '1'){
					$smsSpamRepos = $emSms->getRepository('modules\sms\models\SmsSpam')->findAll(array('status' => '1'));
					
					foreach ($smsSpamRepos as $smsSpamRepo) {
						$keywords = $smsSpamRepo->keywords;
						$checkSpamKeywords = $this->msgSpamCheck($keywords, $text);
						if ($checkSpamKeywords){
							$spamMatch = '1';
							break;
						}
					}
				}
			}
	
			if ($contactType == '1') {
				//Select Contact
				$selectContact = $campaignValues['selectContact'];
				$groups = explode(',', $selectContact);
				$leadPath = '../contactFiles/';
				$uuid = $this->generateUuid();
				$newLead = $uuid.'.csv';
				$file = $leadPath.$newLead;
				$fp = fopen("$file", 'w');
				foreach ($groups as $group){
					
					if ($api){
						$leadRepos = $emUser->getRepository('modules\user\models\Lead')->findOneBy(array('id' => $group, 'user' => $userId));
					}else{
						$leadRepos = $emUser->getRepository('modules\user\models\Lead')->findOneBy(array('name' => $group, 'user' => $userId));
					}
					$leadId = $leadRepos->id;
					$leadDetailRepos = $emUser->getRepository('modules\user\models\LeadDetail')->findBy(array('lead' => $leadId));
					//$leadRepo = $emUser->getRepository('Models\Lead')->findById(array($selectContact));
					foreach ($leadDetailRepos as $leadDetail){
						$line = $leadDetail->mobile;	
						$val = explode(",",$line);
						fputcsv($fp, $val);
					}	
				}
				fclose($fp);
				$leadManager = new LeadManager();
				$count = $leadManager->countCsv($newLead, $route, $emSmpp);
				
				if (!$count){
					return array('Campaign' => array('contactCount' => 'groups is invalid'));
				}
				
			}elseif ($contactType == '2') {
				//Upload Contact
				$contactName = $campaignValues['contactName'];
				$uploadContact = $campaignValues['uploadContact'];
				//$saveContact = $campaignValues['saveContact'];
				if (!$uploadContact) {
					//return false;
					return array('Campaign' => array('uploadContact' => 'failed'));
				}
	
				$leadManager = new LeadManager();
				$count = $leadManager->countCsv($uploadContact, $route, $emSmpp);
				if (!$count){
					if ($route == '2'){
						return array('Campaign' => array('contactCount' => 'contacts is invalid'));
					}else{
						return array('Campaign' => array('contactCount' => 'contacts is ndnc registered or invalid'));
					}
					
				}else {
					if ($count > 50000) {
						return array('Campaign' => array('contactCount' => 'contacts is more than 50,000'));
					}
				}
				
	
				/* $leadRepo = $leadManager->createLead($userRepo, $type, $saveContact, $contactName, $emUser);
				if ($leadRepo) {
					$fileName = $leadRepo->id; */
					$uuid = $this->generateUuid();
					$fileName = $uuid.'.csv';
					$newLead = $leadManager->renameCsv($uploadContact, $fileName);
					//$count = $leadManager->countCsv($newLead, $route, $emUser);
				/* 		
					$leadRepo->count = $count;
					$em->persist($leadRepo);
					$em->flush();
				}
				$leadRepo = array($leadRepo); */
					
			}elseif ($contactType == '3') {
				//Text Contact
				$textContact = $campaignValues['textContact'];
				//$saveContact = $campaignValues['saveContact'];
				
				$uuid = $this->generateUuid();
				$fileName = $uuid.'.csv';
				$leadManager = new LeadManager();
				$newLead = $leadManager->createCsv($textContact, $fileName);
				$count = $leadManager->countCsv($newLead, $route, $emSmpp);
				
				if (!$count){
					if ($route == '2'){
						return array('Campaign' => array('contactCount' => 'contacts is invalid'));
					}else{
						return array('Campaign' => array('contactCount' => 'contacts is ndnc registered or invalid'));
					}
					
				}else {
					if ($count > 50000) {
						return array('Campaign' => array('contactCount' => 'contacts is more than 50,000'));
					}
				}
	
				/* $leadRepo = $leadManager->createLead($userRepo, $type, $saveContact, $contactName, $em);
				if ($leadRepo) {
					$newName = $leadRepo->id;
					$newLead = $leadManager->renameCsv($newLead, $newName);
						
					$leadRepo->count = $count;
					$em->persist($leadRepo);
					$em->flush();
				}
				$leadRepo = array($leadRepo); */
			//Dynamic Logic
			}elseif ($contactType == '4') {
				$dynamic = true;
				//Upload Contact
					
				if (isset($campaignValues['textContact'])){
					
					$textContact = $campaignValues['textContact'];
					$uuid = $this->generateUuid();
					$fileName = $uuid.'.csv';
					$leadManager = new LeadManager();
					$uploadContact = $leadManager->createCsv($textContact, $fileName);
					
				}else{
					$uploadContact = $campaignValues['uploadContact'];
				}
				
				//$saveContact = $campaignValues['saveContact'];
				if (!$uploadContact) {
					//return false;
					return array('Campaign' => array('uploadContact' => 'failed'));
				}
	
				$leadManager = new LeadManager();
				$dynamicData = $leadManager->countDynamicCsv($uploadContact, $route, $text, $smsEncoding, $emSmpp);
				
				if (!$dynamicData){
					if (($route == '2') || ($route == '5')){
						return array('Campaign' => array('contactCount' => 'contacts is invalid'));
					}else{
						return array('Campaign' => array('contactCount' => 'contacts is ndnc registered or invalid'));
					}
					
				}else {
					$count = $dynamicData['count'];
					if ($count > 50000) {
						return array('Campaign' => array('contactCount' => 'contacts is more than 50,000'));
					}
				}				
					$uuid = $this->generateUuid();
					$fileName = $uuid.'.csv';
					$newLead = $leadManager->renameCsv($uploadContact, $fileName);					
			}
			if ($dynamic){
				$credit = $dynamicData['credit'];
				$creditValue = $dynamicData['credit'];
			}else{
				$credit = $count*$creditValue;
			}
			$fakeCount = ceil($count*($fake/100));
			
			if ($creditValue > 0 || $creditValue != NULL){
				$isCampaignAllowed = $this->isCampaignAllowed($userId, $count, $creditValue, $dynamic, $route, $api, $emUser);
			}else{
				$isCampaignAllowed = false;
				return array('Campaign' => array('text' => 'sms message is empty'));
			} 				
			
			if ($isCampaignAllowed) {
				$isCampaignAllowed = false;
				
				$today =new \DateTime('now');
				$startDate = new \DateTime($dateTime);
				if (($count <= '10') && ($startDate <= $today)){
					if ($spamMatch == '1'){
						$status = '6';
					}else{
						$status = '4';
					}
					//Create Campaign
					$smsCampaign = new SmsCampaign();
					$smsCampaign->name = $campaignName;
					$smsCampaign->user = $userRepo->id;
                                        $smsCampaign->reseller = $userRepo->reseller;
                                        $smsCampaign->domain = $userRepo->domain->id;
                                        $smsCampaign->parentDomain =$userRepo->parentDomain;
					$smsCampaign->smsPriority = '3';
					$smsCampaign->senderId = $senderId;
					$smsCampaign->leadsFile = $newLead;
				        $smsCampaign->urlStatus = $urlType;
					$smsCampaign->tinyUrl = $urlLink;
					$smsCampaign->text = html_entity_decode($text);
					$smsCampaign->original = html_entity_decode($orgText);
					$smsCampaign->count = $count;
					$smsCampaign->flash = $flash;
					$smsCampaign->fakeCount = '0';
					$smsCampaign->smsCount = $smsCount;
					$smsCampaign->charCount = $smsLength;
					$smsCampaign->credit = $credit;
					$smsCampaign->encoding = $smsEncoding;
					$smsCampaign->route = $routes['1'];
					$smsCampaign->routeName = $routes['2'];
					$smsCampaign->type = $routes['0'];
					$smsCampaign->input = $input;
					$smsCampaign->status = $status;
					$smsCampaign->validity = $userRepo->validity;
					$smsCampaign->method = $method;
					$smsCampaign->apiKey = $apiKey;
					$smsCampaign->recredit = $parentRecredit;
					$smsCampaign->startDate = new \DateTime($dateTime);
					$smsCampaign->endDate = new \DateTime($after);
					$smsCampaign->createdDate = new \DateTime('now');
					$smsCampaign->updatedDate = new \DateTime('now');
					$emSms->persist($smsCampaign);
					$emSms->flush();	
					
					shell_exec("/usr/bin/php /var/www/html/webpanel/cronjob/sms/cronJobNumberPriority.php $smsCampaign->id 2> /dev/null > /dev/null  &");
			
				}else {
					if ($spamMatch == '1'){
						$status = '6';
					}else{
						$status = '0';
					}
					//Create Campaign
					$smsCampaign = new SmsCampaign();
					$smsCampaign->name = $campaignName;
					$smsCampaign->user = $userRepo->id;
                                        $smsCampaign->reseller = $userRepo->reseller;
                                        $smsCampaign->domain = $userRepo->domain->id;
                                        $smsCampaign->parentDomain =$userRepo->parentDomain;
					$smsCampaign->smsPriority = $userRepo->smsPriority;
					$smsCampaign->senderId = $senderId;
					$smsCampaign->leadsFile = $newLead;
				    $smsCampaign->urlStatus = $urlType;
					$smsCampaign->tinyUrl = $urlLink;
					$smsCampaign->text = html_entity_decode($text);
					$smsCampaign->original = html_entity_decode($orgText);
					$smsCampaign->count = $count;
					$smsCampaign->flash = $flash;
					if ($count <= '10'){
						$smsCampaign->fakeCount = '0';
					}else{
						$smsCampaign->fakeCount = $fakeCount;
					}					
					$smsCampaign->smsCount = $smsCount;
					$smsCampaign->charCount = $smsLength;
					$smsCampaign->credit = $credit;
					$smsCampaign->encoding = $smsEncoding;
					$smsCampaign->route = $routes['1'];
					$smsCampaign->routeName = $routes['2'];
					$smsCampaign->type = $routes['0'];
					$smsCampaign->input = $input;
					$smsCampaign->startDate = new \DateTime($dateTime);
					$smsCampaign->status = $status;
					$smsCampaign->validity = $userRepo->validity;
					$smsCampaign->method = $method;
					$smsCampaign->apiKey = $apiKey;
					$smsCampaign->recredit = $parentRecredit;
					$smsCampaign->endDate = new \DateTime($after);
					$smsCampaign->createdDate = new \DateTime('now');
					$smsCampaign->updatedDate = new \DateTime('now');
					$emSms->persist($smsCampaign);
					$emSms->flush();
				
				}
				
				return array('Campaign' => array('isSuccess' => 'Campaign created','data' => $smsCampaign->id, 'leads' => $count));
			}else {
				return array('Campaign' => array('credit' => 'Insufficient credit'));
			}
		}
		return false;
	
	}
	
	public function isCampaignAllowed($userId, $leadsCount, $creditValue, $dynamic, $route, $api, $emUser)
	{
		if ($route == '1'){ 
			if ($dynamic){
				$leads = $creditValue;
			}else {
				$leads = $leadsCount*$creditValue;
			}
			$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
			if ($api){
				$userBalance = $userRepo->account->smsPromoApiCredit;
			}else{
				$userBalance = $userRepo->account->smsPromoCredit;
			}
			
			if ($userBalance >= $leads){
				$currentBalance = $userBalance - $leads;
				if ($api){
					$userRepo->account->smsPromoApiCredit = $currentBalance;
				}else{
					$userRepo->account->smsPromoCredit = $currentBalance;
				}
				
				$emUser->persist($userRepo);
				$emUser->flush();
			
				return true;
			}
			return false;
		}elseif ($route == '2'){
			if ($dynamic){
				$leads = $creditValue;
			}else {
				$leads = $leadsCount*$creditValue;
			}
			$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
			if ($api){
				$userBalance = $userRepo->account->smsTransApiCredit;
			}else{
				$userBalance = $userRepo->account->smsTransCredit;
			}
			
			if ($userBalance >= $leads){
				$currentBalance = $userBalance - $leads;
				if ($api){
					$userRepo->account->smsTransApiCredit = $currentBalance;
				}else{
					$userRepo->account->smsTransCredit = $currentBalance;
				}
				
				$emUser->persist($userRepo);
				$emUser->flush();
				
				return true;
			}
			return false;
		}elseif ($route == '3'){
			if ($dynamic){
				$leads = $creditValue;
			}else {
				$leads = $leadsCount*$creditValue;
			}
			$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
			if ($api){
				$userBalance = $userRepo->account->smsScrubApiCredit;
			}else{
				$userBalance = $userRepo->account->smsScrubCredit;
			}
			
			if ($userBalance >= $leads){
				$currentBalance = $userBalance - $leads;
				if ($api){
					$userRepo->account->smsScrubApiCredit = $currentBalance;
				}else{
					$userRepo->account->smsScrubCredit = $currentBalance;
				}
				
				$emUser->persist($userRepo);
				$emUser->flush();
					
				return true;
			}
			return false;
		}elseif ($route == '4'){
			if ($dynamic){
				$leads = $creditValue;
			}else {
				$leads = $leadsCount*$creditValue;
			}
			$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
			if ($api){
				$userBalance = $userRepo->account->smsInterApiCredit;
			}else{
				$userBalance = $userRepo->account->smsInterCredit;
			}
			
			if ($userBalance >= $leads){
				$currentBalance = $userBalance - $leads;
				if ($api){
					$userRepo->account->smsInterApiCredit = $currentBalance;
				}else{
					$userRepo->account->smsInterCredit = $currentBalance;
				}
				
				$emUser->persist($userRepo);
				$emUser->flush();
					
				return true;
			}
			return false;
		}elseif ($route == '5'){
			if ($dynamic){
				$leads = $creditValue;
			}else {
				$leads = $leadsCount*$creditValue;
			}
			$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
			if ($api){
				$userBalance = $userRepo->account->smsPremiApiCredit;
			}else{
				$userBalance = $userRepo->account->smsPremiCredit;
			}
			
			if ($userBalance >= $leads){
				$currentBalance = $userBalance - $leads;
				if ($api){
					$userRepo->account->smsPremiApiCredit = $currentBalance;
				}else{
					$userRepo->account->smsPremiCredit = $currentBalance;
				}
				
				$emUser->persist($userRepo);
				$emUser->flush();
				
				return true;
			}
			return false;
		}
	}
	
	public function cancellCampaign($campaignId, $emSms)
	{
		if (isset($emSms)) {			
			$smsCampaign = $emSms->getRepository('modules\sms\models\SmsCampaign')->find($campaignId);
			if ($smsCampaign->status == '0'){
				$smsCampaign->status = 3;
				$emSms->persist($smsCampaign);
				$emSms->flush();
				
				return $smsCampaign;
			}
		}
		return false;
	}
	
	public function createReportCsv($userId, $csvName,$emArray,$search_type)
	{
		$em = $emArray['emit'];
		$leadPath = '/var/www/html/webpanel/files/sms/report/';
		$file = $leadPath.$csvName;

		if ($userId) {
			$cont = preg_split('/\r\n|[\r\n]/', $userId);
			$csvNamee= explode('.',$csvName);

			$csvNamee1 = $csvNamee[0];
			$csvNamee2 = $csvNamee[1];

			$fp = fopen("$file", 'w');


			$errcodedlr =$em->getRepository('sms')->errcodedlr();
			$dlr_MT =$em->getRepository('sms')->dlrMT($csvNamee1,$errcodedlr,$fp,$emArray,$search_type);

			fclose($fp);

			$csvNamee= explode('@',$csvNamee1);
			$csvName1= strtotime("now").$csvNamee[1].".csv";
			$file1=$leadPath.$csvName1;

			rename($file,$file1);
			return $csvName1;
		}
	}
}
