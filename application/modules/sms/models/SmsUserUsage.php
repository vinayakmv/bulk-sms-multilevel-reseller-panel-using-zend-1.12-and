<?php
namespace modules\sms\models;

/**
 * @Entity(repositoryClass="modules\sms\models\SmsUserUsageRepository")
 * @Table(name="sms_user_usage")
 * @author Vinayak
 *
 */
class SmsUserUsage
{
	/**
	* @Column(name="id", type="integer", nullable=false)
	* @Id
	* @GeneratedValue(strategy="IDENTITY")
	* @var integer
	*/
	private $id;
	
	/**
	 * @column(name="user_id", type="integer", nullable="false")
	 * @var integer
	 */
	private $user;

	/**
     * @column(name="client_id", type="integer", nullable="false")
     * @var integer
     */
	private $client;
	
	/**
	 * @column(name="domain", type="string", nullable="false")
	 * @var unknown
	 */
	private $domain;
		
	/**
	 * @column(name="username", type="string", nullable="false")
	 * @var unknown
	 */
	private $username;
	
	/**
	 * @column(name="trans_sent", type="integer", nullable="false")
	 * @var unknown
	 */
	private $transSent;
	
	/**
	 * @column(name="promo_sent", type="integer", nullable="false")
	 * @var unknown
	 */
	private $promoSent;	

	/**
	 * @column(name="inter_sent", type="integer", nullable="false")
	 * @var unknown
	 */
	private $interSent;
	
	/**
	 * @Column(name="sent_date", type="integer", nullable=true)
	 * @var datetime
	 */
	private $sentDate;	
		
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
