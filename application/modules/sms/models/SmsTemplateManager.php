<?php
namespace modules\sms\models;
/**
 * 
 * @author ANI
 *
 */
class SmsTemplateManager  extends \Smpp_Doctrine_BaseManager
{
	public function addTemplate($getValues, $userId, $em)
	{
		if (isset($getValues)) {
			$createTemplate = new SmsTemplate();
			$createTemplate->user = $userId;
			$createTemplate->template = urlencode($getValues['text']);
			$createTemplate->status = '0';
			$createTemplate->remark = "Requested";
			$createTemplate->createdDate = new \DateTime('now');
			$createTemplate->updatedDate = new \DateTime('now');
			$em->persist($createTemplate);
			$em->flush();
			return $createTemplate;
		}
	}
	public function updateRequestTemplate($templateDetail, $userId, $em)
	{
		$id=$templateDetail['id'];
		
		$updateTemplate = $em->getRepository('modules\sms\models\SmsTemplate')->find($id);
		
		if(isset($templateDetail['rejected']))
		{
			$status='4';
			$remark=$templateDetail['Rejectlist'];
		}
		else if(isset($templateDetail['approved']))
		{	

			$remark="Approved";
			$status='1';
			//$template = "/^".$template."$/g";
		}else if(isset($templateDetail['Delete'])){
			//$remark="Deleted";
			//$status = '5';
			$em->remove($updateTemplate);
			$em->flush();
			
			return $updateTemplate;
		}

		$updateTemplate->status = $status;
		$updateTemplate->remark = $remark;
		$updateTemplate->updatedDate = new \DateTime('now');
		$em->persist($updateTemplate);
		$em->flush();
		return $updateTemplate;
	}

}
