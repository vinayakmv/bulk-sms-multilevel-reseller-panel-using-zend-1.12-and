<?php
namespace modules\sms\models;
use Doctrine\ORM\EntityRepository;
/**
 *
 * @author Vinayak
 *
 */
class SmsSenderIdRepository extends \Smpp_Doctrine_EntityRepository
{
	public function checkSender($sender, $userId){
		$queryBuilder = $this->getEntityManager('sms')->createQueryBuilder()
		->select('u.name')
		->from('modules\sms\models\SmsSenderId', 'u')
		->where('u.name = ?1')
		->andWhere('u.user = ?2')
		->andWhere('u.status = ?3')
		->setParameter("1", $sender)
		->setParameter("2", $userId)
		->setParameter("3", '1');
	
		return $queryBuilder->getQuery()->getResult();
	}
}