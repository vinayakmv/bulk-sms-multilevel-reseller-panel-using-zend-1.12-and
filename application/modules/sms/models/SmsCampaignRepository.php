<?php
namespace modules\sms\models;
use Doctrine\ORM\EntityRepository;

class SmsCampaignRepository extends \Smpp_Doctrine_EntityRepository
{
	public function updateReport($campaignId,$type)
	{
		$cdate=date("Y-m-d H:i:s");
		if($type =='less')
		{
			$sql = "UPDATE campaigns_sms SET `report_status`='1',`updated_date`='$cdate' WHERE id='$campaignId' ";
			$em = $this->getEntityManager('sms');
			$stmt = $em->getConnection()->prepare($sql);
			$stmt->execute(); 
			return $stmt;
		}
		else if($type == 'greater')
		{
			$sql = "UPDATE campaigns_sms SET `report_status`='0',`updated_date`='$cdate' WHERE id='$campaignId'";
			$em = $this->getEntityManager('sms');
			$stmt = $em->getConnection()->prepare($sql);
			$stmt->execute(); 
			return $stmt;	
		}
	}
	
	public function getScheduledCampaigns($userId)
	{		
		$cdate=date("Y-m-d H:i:s");
		$sql = "SELECT id,start_date,type,count,credit FROM campaigns_sms WHERE `start_date` > '$cdate' AND `status`='0' AND (`input`='DYNAMIC-SCHEDULED' OR `input`='SCHEDULED') AND user_id='$userId' ";
		$em = $this->getEntityManager('sms');
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll();
	}

}