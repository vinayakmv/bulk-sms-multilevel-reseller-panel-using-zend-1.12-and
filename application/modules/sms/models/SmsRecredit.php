<?php
namespace modules\sms\models;

/**
 * @Entity(repositoryClass="modules\sms\models\SmsRecreditRepository")
 * @Table(name="sms_recredit")
 * @author Vinayak
 *
 */
class SmsRecredit
{
	/**
	* @Column(name="id", type="integer", nullable=false)
	* @Id
	* @GeneratedValue(strategy="IDENTITY")
	* @var integer
	*/
	private $id;
	
	/**
	 * @column(name="user_id", type="integer", nullable="false")
	 * @var integer
	 */
	private $user;

	/**
     * @column(name="client_id", type="integer", nullable="false")
     * @var integer
     */
	private $client;
	
	/**
	 * @column(name="type", type="string", nullable="false")
	 * @var unknown
	 */
	private $type;
	
	/**
	 * @column(name="sms_count", type="integer", nullable="false")
	 * @var unknown
	 */
	private $smsCount;
		
	/**
	 * @column(name="fake", type="integer", nullable="false")
	 * @var unknown
	 */
	private $fake;
	
	/**
	 * @column(name="failed", type="integer", nullable="false")
	 * @var unknown
	 */
	private $failed;
	
	/**
	 * @column(name="awaiting_dlr", type="integer", nullable="false")
	 * @var unknown
	 */
	private $awaitingDlr;	

	/**
	 * @column(name="delivered", type="integer", nullable="false")
	 * @var unknown
	 */
	private $delivered;

	/**
	 * @column(name="total_recredit", type="integer", nullable="false")
	 * @var unknown
	 */
	private $totalRecredit;
	
	/**
	 * @column(name="status", type="integer", nullable="false")
	 * @var unknown
	 */
	private $status;
	
	/**
	 * @Column(name="recredit_date", type="date", nullable=true)
	 * @var datetime
	 */
	private $recreditDate;	
		
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
