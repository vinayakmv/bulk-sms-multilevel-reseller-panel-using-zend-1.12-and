<?php
namespace modules\sms\models;

/**
 * @Entity(repositoryClass="modules\sms\models\SmsSenderIdRepository")
 * @Table(name="sender_ids_sms")
 * @author Vinayak
 *
 */
class SmsSenderId
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="user_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $user;
	
	/**
	 * @Column(name="name", type="string", nullable=false)
	 * @var string
	 */
	private $name;
	
	/**
	 * @Column(name="type", type="integer", nullable=false)
	 * @var integer
	 */
	private $type;
	
	/**
	 * @Column(name="default_sender", type="integer", nullable=true)
	 * @var integer
	 */
	private $default;
	
	/**
	 * @Column(name="status", type="integer", nullable=false)
	 * @var integer
	 */
	private $status;
	
	/**
	 * @Column(name="remark", type="string", nullable=true)
	 * @var string
	 */
	private $remark;
	
	/**
	 * @Column(name="description", type="string", nullable=true)
	 * @var string
	 */
	private $description;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}
