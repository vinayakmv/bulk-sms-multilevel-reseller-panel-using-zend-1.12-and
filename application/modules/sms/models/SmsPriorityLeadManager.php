<?php
namespace modules\sms\models;

class SmsPriorityLeadManager extends \Smpp_Doctrine_BaseManager
{
	public function createLead($userRepo, $type, $status, $leadName, $em)
	{
		if (isset($em)) {			
			$lead = new SmsPriorityLead();
			$lead->name = $leadName;
			$lead->user = $userRepo;
			$lead->count = '0';
			$lead->type = $type;
			$lead->status = $status;
			$lead->createdDate = new \DateTime('now');
			$lead->updatedDate = new \DateTime('now');
			$em->persist($lead);
			$em->flush();
			
			return $lead;
		}
		return false;
		
	}
	
	public function createCsv($contacts, $csvName)
	{
		$leadPath = '../contactFiles/';
		$file = $leadPath.$csvName;
		if ($contacts) {
			$cont = preg_split('/\r\n|[\r\n]/', $contacts);
			$fp = fopen("$file", 'w');
    		foreach($cont as $line){
             	$val = explode(",",$line);
             	fputcsv($fp, $val);
    		}
    		fclose($fp);
    		
			return $csvName;
		}
		return false;		
	}
	
	public function renameCsv($oldName, $newName)
	{
		$leadPath = '../contactFiles/';
		$oldFile = $leadPath.$oldName;
		$newFile = $leadPath.$newName.'.csv';
		$move = shell_exec("mv -f $oldFile $newFile");
		if (!$move) {
			return $newName;
		}
		
		return false;
	}
	
	public function countCsv($csvName, $route, $em)
	{
		$leadPath = '../contactFiles/';
		$csv = $leadPath.$csvName;
		$tmp = $leadPath.'tmp_'.$csvName;
		
		if ($route == '1') {
			if (($handle = fopen("$csv", "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					$numbers[] = $data[0];
				}
			}
			$contacts = $this->checkDND($numbers, $em);
			if ($contacts) {
				$csv = $this->createCsv($contacts, $csvName);
				$csv = $leadPath.$csvName;
			}else {
				shell_exec("echo '' > $csv");
			}
			
		}
		shell_exec("awk -F',' '!seen[$0]++' $csv | awk 'NF' | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' | awk -F',' 'length($1)==10' > $tmp && mv $tmp $csv");
		//shell_exec("awk -F',' '!seen[$0]++' $csv | awk 'NF' | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' > $tmp");
		//shell_exec("awk 'NF' $csv > $tmp && mv $tmp $csv");
		$fp = file($csv, FILE_SKIP_EMPTY_LINES);
		if ($fp){
			$count = count($fp);
			return $count;
		}	
		return false;
	}

	public function checkDND($numbers, $em) 
	{	
		$dndRepo = $em->getRepository("modules\smpp\models\Dnd");
		$dndNumbers = $dndRepo->getDnd($numbers);		
		$nonDnd = $this->flip_isset_diff($numbers, $dndNumbers);
		$nonDnd = implode("\r\n", $nonDnd);
		
		return $nonDnd;
	}
	
	public function flip_isset_diff($b, $a) {
		$at = array_flip($a);
		$d = array();
		foreach ($b as $i)
		if (!isset($at[$i]))
			$d[] = $i;
		return $d;
	}
	
}
