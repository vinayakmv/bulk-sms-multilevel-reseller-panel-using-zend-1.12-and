<?php
namespace modules\sms\models;

/**
 * @Entity(repositoryClass="modules\sms\models\SmsDashboardRepository")
 * @Table(name="sms_dashboard")
 * @author Vinayak
 *
 */
class SmsDashboard
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
         * @Id
         * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @column(name="user_id", type="integer", nullable="false")
	 * @var integer
	 */
	private $user;
	
	/**
	 * @column(name="domain_id", type="integer", nullable="true")
	 * @var integer
	 */
	private $domain;
        
        /**
	 * @column(name="parent_domain_id", type="integer", nullable="true")
	 * @var integer
	 */
	private $parentDomain;
	
	/**
	 * @column(name="reseller_id", type="integer", nullable="true")
	 * @var integer
	 */
	private $reseller;
	
	/**
	 * @column(name="sent_sms_trans", type="integer", nullable="true")
	 * @var integer
	 */
	private $sentSmsTrans;
	
	/**
	 * @column(name="delivered_sms_trans", type="integer", nullable="true")
	 * @var integer
	 */
	private $deliveredSmsTrans;

	/**
	 * @column(name="failed_sms_trans", type="integer", nullable="true")
	 * @var integer
	 */
	private $failedSmsTrans;

	/**
	 * @column(name="sent_sms_promo", type="integer", nullable="true")
	 * @var integer
	 */
	private $sentSmsPromo;
	
	/**
	 * @column(name="delivered_sms_promo", type="integer", nullable="true")
	 * @var integer
	 */
	private $deliveredSmsPromo;

	/**
	 * @column(name="failed_sms_promo", type="integer", nullable="true")
	 * @var integer
	 */
	private $failedSmsPromo;

	/**
	 * @column(name="sent_sms_scrub", type="integer", nullable="true")
	 * @var integer
	 */
	private $sentSmsScrub;
	
	/**
	 * @column(name="delivered_sms_scrub", type="integer", nullable="true")
	 * @var integer
	 */
	private $deliveredSmsScrub;

	/**
	 * @column(name="failed_sms_scrub", type="integer", nullable="true")
	 * @var integer
	 */
	private $failedSmsScrub;

	/**
	 * @column(name="sent_sms_inter", type="integer", nullable="true")
	 * @var integer
	 */
	private $sentSmsInter;
	
	/**
	 * @column(name="delivered_sms_inter", type="integer", nullable="true")
	 * @var integer
	 */
	private $deliveredSmsInter;

	/**
	 * @column(name="failed_sms_inter", type="integer", nullable="true")
	 * @var integer
	 */
	private $failedSmsInter;

	/**
	 * @Column(name="report_date", type="date", nullable=true)
	 * @var datetime
	 */
	private $reportDate;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=true)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=true)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
	     return $this->$property;
	}
	
	function toArray ()
        {
            return get_object_vars($this);
        }
	
}
