<?php
namespace modules\sms\models;

/**
 * @Entity(repositoryClass="modules\sms\models\SmsTemplateRepository")
 * @Table(name="sms_template",indexes={
 * @Index(name="search_user", columns={"user_id"}),
 * @Index(name="search_status", columns={"status"})
 * })
 * @author Vinayak
 *
 */
class SmsTemplate
{
	/**
	 * @Column(name="id", type="bigint", nullable=true)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var bigint
	 */
	private $id;

	/**
	 * @Column(name="user_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $user;
	
	/**
	 * @Column(name="template", type="text")
	 * @var text
	 */
	private $template;
	
	/**
	 * @Column(name="remark", type="text")
	 * @var text
	 */
	private $remark;

	/**
	 * @Column(name="status", type="integer")
	 * @var integer
	 */
	private $status;
	
	/**
	 * @column(name="created_date", type="datetime", nullable="false")
	 * @var datetime
	*/	 
	private $createdDate;

	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	*/
	 
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}