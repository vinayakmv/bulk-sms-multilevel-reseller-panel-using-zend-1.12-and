<?php
namespace modules\sms\models;

class SmsRecreditManager extends \Smpp_Doctrine_BaseManager
{
	public function createActivityLog($userRepo, $clientRepo, $recreditDate, $type, $status, $em)
	{
		if (isset($em)){
			$recredit = new SmsRecredit();
			$recredit->user = $userRepo;
			$recredit->client = $clientRepo;
			$recredit->type = $type;
			$recredit->status = $status;
			$recredit->recreditDate = $recreditDate;
			$recredit->createdDate = new \DateTime('now');
			$recredit->updatedDate = new \DateTime('now');
			$em->persist($recredit);
			$em->flush();
				
			return $recredit;
		}
		return false;
	}

}
