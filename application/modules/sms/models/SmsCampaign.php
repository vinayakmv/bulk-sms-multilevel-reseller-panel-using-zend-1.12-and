<?php
namespace modules\sms\models;

/**
 * @Entity(repositoryClass="modules\sms\models\SmsCampaignRepository")
 * @Table(name="campaigns_sms",indexes={@Index(name="search_idx", columns={"status"})})
 * @author Vinayak
 *
 */
class SmsCampaign
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="user_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $user;
        
        /**
	 * @Column(name="reseller_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $reseller;
        
        /**
	 * @Column(name="domain_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $domain;
        
         /**
	 * @Column(name="parent_domain_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $parentDomain;
	
	/**
	 * @Column(name="name", type="string", nullable=false)
	 * @var string
	 */
	private $name;
	
	/**
	 * @Column(name="sms_priority", type="integer", nullable=false)
	 * @var string
	 */
	private $smsPriority;
	
	/**
	 * @Column(name="leads_file", type="string", nullable=false)
	 * @var integer
	 */
	private $leadsFile;
	
	/**
	 * @Column(name="sender_id", type="string", nullable=false)
	 * @var integer
	 */
	private $senderId;
	
	/**
	 * @Column(name="text", type="text", nullable=false)
	 * @var text
	 */
	private $text;
	
	/**
	 * @Column(name="text_original", type="text", nullable=true)
	 * @var text
	 */
	private $original;
	
	/**
	 * @Column(name="encoding", type="string", nullable=false)
	 * @var string
	 */
	private $encoding;
	
	/**
	 * @Column(name="validity", type="integer", nullable=true)
	 * @var string
	 */
	private $validity;
	
	/**
	 * @Column(name="count", type="string", nullable=false)
	 * @var string
	 */
	private $count;
        
        /**
	 * @Column(name="delivered_count", type="string", nullable=true)
	 * @var string
	 */
	private $deliveredCount;
        
        /**
	 * @Column(name="failed_count", type="string", nullable=true)
	 * @var string
	 */
	private $failedCount;
        
        /**
	 * @Column(name="awaiting_count", type="string", nullable=true)
	 * @var string
	 */
	private $awaitingCount;
	
	/**
	 * @Column(name="flash", type="boolean", nullable=false)
	 * @var string
	 */
	private $flash;
	
	/**
	 * @Column(name="sms_count", type="string", nullable=false)
	 * @var string
	 */
	private $smsCount;
	
	/**
	 * @Column(name="char_count", type="string", nullable=false)
	 * @var string
	 */
	private $charCount;
	
	/**
	 * @Column(name="credit", type="string", nullable=true)
	 * @var string
	 */
	private $credit;
	
	/**
	 * @Column(name="route", type="string", nullable=false)
	 * @var string
	 */
	private $route;
	
	/**
	 * @Column(name="route_name", type="string", nullable=true)
	 * @var string
	 */
	private $routeName;
	
	/**
	 * @Column(name="type", type="string", nullable=false)
	 * @var string
	 */
	private $type;
	
		/**
	 * @Column(name="input", type="string", nullable=true)
	 * @var string
	 */
	private $input;
	
	/**
	 * @Column(name="fake_count", type="integer", nullable=false)
	 * @var string
	 */
	private $fakeCount;
	
	/**
	 * @Column(name="status", type="integer", nullable=false)
	 * @var integer
	 */
	private $status;
	
	/**
	 * @Column(name="method", type="integer", nullable=true)
	 * @var integer
	 */
	private $method;

	/**
	 * @Column(name="url_status", type="integer", nullable=true)
	 * @var integer
	 */
	private $urlStatus;

	/**
	 * @Column(name="tiny_url", type="string", nullable=true)
	 * @var integer
	 */
	private $tinyUrl;
	
	/**
	 * @Column(name="api_key", type="string", nullable=true)
	 * @var string
	 */
	private $apiKey;
	
	/**
	 * @Column(name="recredit", type="integer", nullable=false)
	 * @var integer
	 */
	private $recredit;
	
	/**
	 * @column(name="report_status", type="integer", nullable="false")
	 * @var integer
	*/	 
	private $reportStatus;

	/**
	 * @column(name="report_file", type="string", nullable="false")
	 * @var string
	*/	 
	private $reportFile;

	/**
	 * @Column(name="csv_gen_date", type="datetime", nullable=true)
	 * @var datetime
	 */
	private $csvGenDate;
	
	/**
	 * @Column(name="start_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $startDate;
	
	/**
	 * @Column(name="end_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $endDate;
		
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}