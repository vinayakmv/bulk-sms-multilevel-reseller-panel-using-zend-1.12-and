<?php
namespace modules\sms\models;
/**
 * 
 * @author Vinayak
 *
 */
class SmsSpamManager  extends \Smpp_Doctrine_BaseManager
{
	public function addSpam($getValues, $userId, $em)
	{
		if (isset($getValues)) {
			$createTemplate = new SmsSpam();
			$createTemplate->user = $userId;
			$createTemplate->keywords = urlencode($getValues['text']);
			$createTemplate->status = '0';
			$createTemplate->remark = "Requested";
			$createTemplate->createdDate = new \DateTime('now');
			$createTemplate->updatedDate = new \DateTime('now');
			$em->persist($createTemplate);
			$em->flush();
			
			return $createTemplate;
		}
	}
	public function updateKeyword($spamDetail, $userId, $em)
	{
		$id=$spamDetail['id'];		
		$updateSpam = $em->getRepository('modules\sms\models\SmsSpam')->find($id);
		
		if(isset($spamDetail['delete'])){
			$em->remove($updateSpam);
			$em->flush();
			
			return $updateSpam;
		}elseif(isset($spamDetail['activate'])){
			$updateSpam->status = '1';
			$em->persist($updateSpam);
			$em->flush();
			
			return $updateSpam;
		}elseif(isset($spamDetail['deactivate'])){
			$updateSpam->status = '0';
			$em->persist($updateSpam);
			$em->flush();
				
			return $updateSpam;
		}elseif(isset($spamDetail['edit'])){
			$updateSpam->keywords = $spamDetail['keywords'];
			$em->persist($updateSpam);
			$em->flush();
				
			return $updateSpam;
		}
		
		
	}
	
	public function updateCampaign($spamDetail, $userId, $em)
	{
		$id=$spamDetail['id'];
		$updateSpam = $em->getRepository('modules\sms\models\SmsCampaign')->find($id);
	
		if(isset($spamDetail['approve'])){
			$updateSpam->status = '0';
			$em->persist($updateSpam);
			$em->flush();
				
			return $updateSpam;
		}elseif(isset($spamDetail['reject'])){
			$updateSpam->status = '7';
			$em->persist($updateSpam);
			$em->flush();
	
			return $updateSpam;
		}
	
	
	}
	
	public function userAssign($userRepo, $mode, $status, $em){
		if (isset($userRepo)){
	
			//$userRepo = $em->getRepository('modules\user\models\User')->findOneBy(array('username' => "$username", 'parentDomain' => "$parentDomain"));
			
				if ($mode == '1'){
					
					$userRepo->checkSpam = $status;						
					$em->persist($userRepo);
					$em->flush();
				}else {
					$userRepo->checkSpam = $status;
						
					$em->persist($userRepo);
					$em->flush();
					
					$underUsersRepo = $em->getRepository('modules\user\models\User')->findBy(array('reseller' => "$userRepo->id"));
					foreach ($underUsersRepo as $underUserRepo){
						
						$underUserRepo->checkSpam = $status;	
						$em->persist($underUserRepo);
						$em->flush();
							
						$role = $underUserRepo->role->role;
						if ($role == 'RESELLER' || $role == 'L1-RESELLER'){
							$this->userAssign($underUserRepo, $mode, $status, $em);
						}
					}
				}
	
			
			return $userRepo;
		}
		return false;
	}

}
