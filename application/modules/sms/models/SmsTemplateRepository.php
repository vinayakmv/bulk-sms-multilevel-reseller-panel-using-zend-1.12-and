<?php
namespace modules\sms\models;
use Doctrine\ORM\EntityRepository;

/**
 * 
 * @author ANI
 *
 */
class SmsTemplateRepository extends \Smpp_Doctrine_EntityRepository
{
	
	public function addTemplate($getArray,$userId)
	{
		
		//$user=$getArray['user'];
		$template=$getArray['template'];
		$cdate=date("Y-m-d H:i:s");
		$sql = "INSERT INTO `pushsms_template_request` (`id`, `ub_id`, `template`, `status`,`remark`,`create_by`,`create_date`,`updated_by`,`updated_date`) VALUES (NULL, '$userId', '$template', '0','Requested','$userId','$cdate','$userId','$cdate')" ;
		$em = $this->getEntityManager('sms');
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute(); 
		return $stmt;

	
	}

	public function updateRequestTemplate($getArray,$userId)
	{	
		$id=$getArray['id'];
		$cdate=date("Y-m-d H:i:s");
		$remark=$getArray['remark'];

		if(isset($getArray['rejected']))
		{
			$status='2';
		}
		else if(isset($getArray['approved']))
		{	
			$status='1';
			$sql1 = "SELECT `template` FROM `pushsms_template_request` WHERE `id` = '$id'"; 
			$em = $this->getEntityManager('sms'); 
			$stmt = $em->getConnection()->prepare($sql1);
			$stmt->execute();
			$statement= $stmt->fetchAll();

			$template = "/^".$statement['0']['template']."$/g";
			//$template = $statement['0']['template']; 
		}

		
		$sql = "UPDATE `pushsms_template_request` SET `remark` = '$remark',`status`='$status',`updated_by`='$userId',`updated_date`='$cdate' ,`template` = '$template' WHERE `id` = '$id'" ;
		$em = $this->getEntityManager('sms');
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		return $stmt;
	
	}
}