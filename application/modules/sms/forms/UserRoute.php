<?php

class Sms_Form_UserRoute extends Zend_Form
{

    public function init()
    {
        $this->setAction('/sms/route/user');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	$username = new Zend_Form_Element_Select('username');
    	$username->setDecorators(array('ViewHelper',
						    			'Description'));
    	$username->setLabel('Username ');
    	$username->class = "form-control select2 custom-select";		
    	$username->setAttrib('id', 'routeUser');
    	$username->setRequired(true);
    	
    	//Add Validator
    	//$username->addValidator(new Zend_Validate_StringLength(4,20));
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);

        $selectType = new Zend_Form_Element_Select('rtype');
        $types = array("SELECT TYPE" => "SELECT TYPE", "SMPP" => "SMPP", "SMS" => "SMS", "VOICE" => "VOICE");
        $selectType->addMultiOptions($types);
        $selectType->setDecorators(array('ViewHelper','Description'));
        $selectType->setLabel('Select Route');
        $selectType->class = "form-control select2 custom-select";
         
        $selectType->setRequired(true);
         
        $selectType->addFilter(new Zend_Filter_HtmlEntities());
        $selectType->addFilter(new Zend_Filter_StripTags());
        //Add Username Element
        $this->addElement($selectType);
    	/*
    	$selectRole = new Zend_Form_Element_Select('route');
    	$roles = array("NO ROUTE" => "NO ROUTE");
    	$selectRole->addMultiOptions($roles);
    	$selectRole->setDecorators(array('ViewHelper','Description'));
    	$selectRole->setLabel('Select Route');
    	$selectRole->class = "form-control select2";
    	 
    	$selectRole->setRequired(false);
    	 
    	$selectRole->addFilter(new Zend_Filter_HtmlEntities());
    	$selectRole->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($selectRole);
    	*/
    	$modeOptions = array("1" => "Single",
    			"2" => "Multi-level");
    	$mode = new Zend_Form_Element_Radio('mode');
    	$mode->addMultiOptions($modeOptions)->setSeparator('');
    	$mode->setDecorators(array('ViewHelper','Description'));
    	$mode->setLabel('Type');
    	$mode->setRequired(true);
    	//Add Account type
    	$this->addElement($mode);
    		
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
											'Description',
											'Errors'));
		$submitElement->setLabel('Set Route');
    	
    	$this->setDecorators(array('FormElements',
    			'Form'
    	));
    }

}

