<?php

class Sms_Form_Search extends Zend_Form
{

	public function init()
    {
        $this->setAction('search');
        $this->setMethod('post');
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        $this->setAttrib('class', "form-horizontal row-border");
        //$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    
        $username = new Zend_Form_Element_Text('mobile');
        $username->setDecorators(array('ViewHelper','Description'));
        $username->setLabel('Mobile');
        $username->class = "form-control";
        $username->setAttrib('placeholder', 'Mobile');
        //$username->addErrorMessage('Please enter a username');
        $username->setRequired(false);
    
        //Add Validator
        $username->addValidator(new Zend_Validate_StringLength(9, 12));
                
        //Add Filter
        $username->addFilter(new Zend_Filter_HtmlEntities());
        $username->addFilter(new Zend_Filter_StripTags());
        //$username->addFilter(new Zend_Filter_StringToUpper());
        //Add Username Element
        $this->addElement($username);
        
        $sender = new Zend_Form_Element_Text('sender');
        $sender->setDecorators(array('ViewHelper','Description'));
        $sender->setLabel('Sender');
        $sender->class = "form-control";
        $sender->setAttrib('placeholder', 'Sender');
        //$username->addErrorMessage('Please enter a username');
        $sender->setRequired(false);
        
        //Add Validator
        //$sender->addValidator(new Zend_Validate_StringLength(9, 12));
        
        //Add Filter
        $sender->addFilter(new Zend_Filter_HtmlEntities());
        $sender->addFilter(new Zend_Filter_StripTags());
        //$username->addFilter(new Zend_Filter_StringToUpper());
        //Add Username Element
        $this->addElement($sender);
        
        $name = new Zend_Form_Element_Text('datePicker');
        $name->setDecorators(array('ViewHelper','Description'));
        $name->setLabel('Date');
        $name->class = "form-control";
        //$name->setAttrib('placeholder', '');
        //$username->addErrorMessage('Please enter a username');
        $name->setRequired(true);
        
        //Add Validator
        $name->addValidator(new Zend_Validate_StringLength(4, 20));
        
        //Add Filter
        $name->addFilter(new Zend_Filter_HtmlEntities());
        $name->addFilter(new Zend_Filter_StripTags());
        //Add Username Element
        $this->addElement($name);
        
        //Create Password Object.
        $selectRole = new Zend_Form_Element_Select('type');
        $roles = array("TRANSACTIONAL" => "TRANSACTIONAL",
                        "TRANS-SCRUB" => "TRANS-SCRUB",
                       "PROMOTIONAL" => "PROMOTIONAL",
                        "INTERNATIONAL" => "INTERNATIONAL"
                        );
        $selectRole->addMultiOptions($roles);
        $selectRole->setDecorators(array('ViewHelper','Description'));
        $selectRole->setLabel('Select Route');
        $selectRole->class = "form-control select2 custom-select";
        
        $selectRole->setRequired(true);

        $selectRole->addFilter(new Zend_Filter_HtmlEntities());
        $selectRole->addFilter(new Zend_Filter_StripTags());
        //Add Username Element
        $this->addElement($selectRole);
        
        //Create a submit button.
        $this->addElement('submit', 'submit');
        $submitElement = $this->getElement('submit');
        $submitElement->setAttrib('class',"btn-primary btn");
        $submitElement->setDecorators(array('ViewHelper',
                                            'Description',
                                            'Errors'));
        $submitElement->setLabel('Create');
        
        $this->setDecorators(array('FormElements',
                                    'Form'));
    }


}

