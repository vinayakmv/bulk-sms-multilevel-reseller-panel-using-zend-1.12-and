<?php

class Sms_Form_UserSpam extends Zend_Form
{

    public function init()
    {
        $this->setAction('/sms/spam/assign');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	$username = new Zend_Form_Element_Select('username');
    	$username->setDecorators(array('ViewHelper',
						    			'Description'));
    	$username->setLabel('Username ');
    	$username->class = "form-control select2 custom-select";		
    	$username->setAttrib('id', 'centUser');
    	$username->setRequired(true);
    	
    	//Add Validator
    	//$username->addValidator(new Zend_Validate_StringLength(4,20));
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);
    	
    	/*
    	$typeOptions = array("1" => "SMS",
    			"2" => "VOICE");
    	$modetype = new Zend_Form_Element_Radio('type');
    	$modetype->addMultiOptions($typeOptions)->setSeparator('');
    	$modetype->setDecorators(array('ViewHelper','Description'));
    	$modetype->setLabel('Type');
    	$modetype->setRequired(true);
    	//Add Account type
    	$this->addElement($modetype);*/
    	
    	$modeOptions = array("1" => "Single",
    			"2" => "Multi-level");
    	$mode = new Zend_Form_Element_Radio('mode');
    	$mode->addMultiOptions($modeOptions)->setSeparator('');
    	$mode->setDecorators(array('ViewHelper','Description'));
    	$mode->setLabel('Type');
    	$mode->setRequired(true);
    	//Add Account type
    	$this->addElement($mode);
    	
    	$enableOptions = array("1" => "Enable",
    			"0" => "Disable");
    	$status = new Zend_Form_Element_Radio('status');
    	$status->addMultiOptions($enableOptions)->setSeparator('');
    	$status->setDecorators(array('ViewHelper','Description'));
    	$status->setLabel('Status');
    	$status->setRequired(true);
    	//Add Account type
    	$this->addElement($status);
    		
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
											'Description',
											'Errors'));
		$submitElement->setLabel('Submit');
    	
    	$this->setDecorators(array('FormElements',
    			'Form'
    	));
    }

}

