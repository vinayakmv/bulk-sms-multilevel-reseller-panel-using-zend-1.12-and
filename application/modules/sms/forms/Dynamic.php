<?php

class Sms_Form_Dynamic extends Zend_Form
{

    public function init()
    {
        $this->setAction('/sms/campaign/dynamic');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal");

    	$campaign = new Zend_Form_Element_Text('campaignName');
    	$campaign->setDecorators(array('ViewHelper','Description'));
    	$campaign->setLabel('Campaign Name');
    	$campaign->class = "form-control";
    	//$campaign->setRequired(true);
    	$campaign->addFilter(new Zend_Filter_HtmlEntities());
    	$campaign->addFilter(new Zend_Filter_StripTags());
    	$campaign->addFilter(new Zend_Filter_StringToUpper());
    	$this->addElement($campaign);
    	
    	//Protection against CSRF attack
    	$token = new Zend_Form_Element_Hash('token',array('timeout' => '1800'));
    	$token->setSalt(md5(uniqid(rand(), TRUE)));
    	
    	//$token->setTimeout(Globals::getConfig()->authentication->timeout);
    	$token->setDecorators(array('ViewHelper','Description'));
    	$this->addElement($token);
    	/*
    	$captcha = new Zend_Form_Element_Captcha(
    			'captcha', // This is the name of the input field
    			array(
    					'captcha' => array( // Here comes the magic...
    							// First the type...
    							'captcha' => 'Image',
    							// Length of the word...
    							'wordLen' => 6,
    							// Captcha timeout, 5 mins
    							'timeout' => 300,
    							// What font to use...
    							'font' => '/var/www/html/webpanel/public/captcha/fonts/aller.ttf',
    							// Where to put the image
    							'imgDir' => '/var/www/html/webpanel/public/captcha/image',
    							// URL to the images
    							// This was bogus, here's how it should be... Sorry again :S
    							'imgUrl' => '/captcha/image',
    							'fontSize' => '24',
    							'height' => '80',
    							'width' => '165',
    							'dotNoiseLevel' => '0',
    							'lineNoiseLevel' => '0',
    					)));
    							$captcha->setDecorators(array('ViewHelper','Description'));
    	    							$captcha->removeDecorator('ViewHelper','Description');
    	    							$captcha->class = "form-control";
    	
    	$this->addElement($captcha);
    	*/
    	/*$smstypeOptions = array("1" => "Promotional","2" => "Transactional");
    	$smstype = new Zend_Form_Element_Radio('smsType');
    	$smstype->setMultiOptions($smstypeOptions)->setSeparator('&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp')->setValue("1");
    	$smstype->setDecorators(array('ViewHelper','Description'));
    	$smstype->setLabel('Sms Route');
    	$smstype->setRequired(true);
    	$this->addElement($smstype);*/

    	//$scheduleRadio = new Zend_Form_Element_Checkbox('scheduleRadio',array('onclick' => 'schedule(this.value);'));
    	$scheduleRadio = new Zend_Form_Element_Checkbox('scheduleRadio',array('onclick' =>
    	 	'if(this.checked){ 
    	 		document.getElementById("datepic").style.display="inline";
      			document.getElementById("timepic").style.display="inline";
      			document.getElementById("datePicker").required = true;
      			document.getElementById("timePicker").required = true;
      		}else{
      			document.getElementById("datepic").style.display="none";
  				document.getElementById("timepic").style.display="none";
      			document.getElementById("datePicker").required = false;
      			document.getElementById("timePicker").required = false;
      		}'
      	));
    	$scheduleRadio->setDecorators(array('ViewHelper','Description'));
    	$scheduleRadio->class = "checkbox-primary";
		$scheduleRadio->setCheckedValue("on");
		$scheduleRadio->setUncheckedValue("off");
		$this->addElement($scheduleRadio);

		$tinyRadio = new Zend_Form_Element_Checkbox('tinyRadio',array('onclick' =>
    	 	'if(this.checked){ 
    	 		document.getElementById("selectUrlDiv").style.display="block";    	 		   	 		
      			document.getElementById("selectUrl").required = true;
      		}else{
      			document.getElementById("selectUrlDiv").style.display="none";      			
      			document.getElementById("selectUrl").required = false;
      		}'
      	));
    	$tinyRadio->setDecorators(array('ViewHelper','Description'));
    	$tinyRadio->class = "checkbox-primary";
		$tinyRadio->setCheckedValue("1");
		$tinyRadio->setUncheckedValue("0");
		$this->addElement($tinyRadio);

		$selectUrl = new Zend_Form_Element_Select('selectUrl', array('onchange' => "insertAtCaret('text');"));
		$selectUrl->setDecorators(array('ViewHelper','Description'));
		$selectUrl->setLabel('Select Url');
		$selectUrl->id = "selectUrl";
		$selectUrl->setAttrib('style', 'width:100%');
		$selectUrl->class = "form-control select2 custom-select";
		//$selectSenderId->setRequired(true);
		$selectUrl->addFilter(new Zend_Filter_HtmlEntities());
		$selectUrl->addFilter(new Zend_Filter_StripTags());
		$this->addElement($selectUrl);
		
		$smstype = new Zend_Form_Element_Select('smsType');
		$smstype->setDecorators(array('ViewHelper','Description'));
		$smstype->setLabel('Select Audio');
		$smstype->id = "smstype";
		$smstype->class = "form-control select2 custom-select";
		$smstype->setRequired(true);
		//$smstype->addMultiOptions(array("0" => "NO ROUTE"));
		$smstype->addFilter(new Zend_Filter_HtmlEntities());
		$smstype->addFilter(new Zend_Filter_StripTags());
		$this->addElement($smstype);

		$selectSenderId = new Zend_Form_Element_Select('selectSenderId');
		$selectSenderId->setDecorators(array('ViewHelper','Description'));
		$selectSenderId->setLabel('Select Sender');
		$selectSenderId->id = "selectSenderId";
		$selectSenderId->class = "form-control select2 custom-select";
		//$selectSenderId->setRequired(true);
		$selectSenderId->addFilter(new Zend_Filter_HtmlEntities());
		$selectSenderId->addFilter(new Zend_Filter_StripTags());
		$this->addElement($selectSenderId);
		
		$typeSenderid = new Zend_Form_Element_Text('typeSenderId');
		$typeSenderid->setDecorators(array('ViewHelper','Description'));
		$typeSenderid->setLabel('Select Sender');
		$typeSenderid->class = "form-control";
		//$typeSenderid->setRequired(true);
		$typeSenderid->addFilter(new Zend_Filter_HtmlEntities());
		$typeSenderid->addFilter(new Zend_Filter_StripTags());
		$this->addElement($typeSenderid);
		
		$text = new Zend_Form_Element_Textarea('text');
		$text->setDecorators(array('ViewHelper','Description'));
		$text->setLabel('Text');
		$text->setAttrib('id', 'text');
		$text->setAttrib('rows', '10');
		$text->setAttrib('cols', '30');
		$text->class = "form-control";
		$text->setRequired(false);
		//$text->addFilter(new Zend_Filter_HtmlEntities());
		//$text->addFilter(new Zend_Filter_StripTags());
		$this->addElement($text);

		$coding = new Zend_Form_Element_Checkbox('coding', array('onchange' => 'getEncoding(this);'));
		$coding->setCheckedValue('16bit');
		$coding->setUncheckedValue('7bit');
		$coding->setDecorators(array('ViewHelper','Description'));
		$coding->setLabel('Unicode');
		$coding->class = "checkbox-primary";
		$coding->setRequired(false);
		$this->addElement($coding);
		
		$flashOptions = array("1" => "Yes","0" => "No");
		$flashRadio = new Zend_Form_Element_Radio('flashRadio');
		$flashRadio->setMultiOptions($flashOptions)->setSeparator('');
		$flashRadio->setDecorators(array('ViewHelper','Description'));
		$flashRadio->setLabel('Save Contacts');
		$flashRadio->setRequired(false);
		$flashRadio->addFilter(new Zend_Filter_HtmlEntities());
		$flashRadio->addFilter(new Zend_Filter_StripTags());
		$this->addElement($flashRadio);
		
		
		
		$uploadContact = new Zend_Form_Element_File('uploadContact', array('style'=>'display:none;', 'onchange'=>'$("#upload-file-primary").html($(this).val());'));
		$uploadContact->setDecorators(array('File'));
		$uploadContact->setLabel('Upload Contacts');
		$uploadContact->class = "file-upload-default";
		$uploadContact->setDestination('../contactFiles/');
		$uploadContact->addValidator('Count', false, 1);
		$uploadContact->addValidator('Extension', false, 'csv');
		$uploadContact->setMaxFileSize(10485760);
		$uploadContact->setRequired(false);
		$this->addElement($uploadContact);
		
		$data = array('SELECT COLUMN' => 'SELECT COLUMN','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		
		$selectData = new Zend_Form_Element_Select('selectData');
		$selectData->addMultiOptions($data);
		$selectData->setDecorators(array('ViewHelper','Description'));
		$selectData->setLabel('Select Data');
		$selectData->class = "form-control select2 custom-select";
		//$selectSenderId->setRequired(true);
		$selectData->addFilter(new Zend_Filter_HtmlEntities());
		$selectData->addFilter(new Zend_Filter_StripTags());
		$this->addElement($selectData);
		
		$typeSenderid = new Zend_Form_Element_Text('typeSenderId');
		$typeSenderid->setDecorators(array('ViewHelper','Description'));
		$typeSenderid->setLabel('Select Sender');
		$typeSenderid->class = "form-control";
		//$typeSenderid->setRequired(true);
		$typeSenderid->addFilter(new Zend_Filter_HtmlEntities());
		$typeSenderid->addFilter(new Zend_Filter_StripTags());
		$this->addElement($typeSenderid);

        $timePicker = new Zend_Form_Element_Text('timePicker');
        $timePicker->setDecorators(array('ViewHelper','Description'));
        //$timePicker->setAttrib('readonly', 'readonly');
        //$timePicker->setAttrib('placeholder', date('H:i'));
        $timePicker->setAttrib('data-target', "#timePicker");
		$timePicker->class = "form-control datetimepicker-input timepicker-24";
        $timePicker->addFilter(new Zend_Filter_HtmlEntities());
        $timePicker->addFilter(new Zend_Filter_StripTags());
        $this->addElement($timePicker);
        
        $datePicker = new Zend_Form_Element_Text('datePicker');
        $datePicker->class = "form-control";
        //$datePicker->name = "from";
        $datePicker->setDecorators(array('ViewHelper','Description'));
        //$datePicker->setAttrib('placeholder', date('m/d/Y'));
       //$datePicker->setValue(date('m/d/Y'));
        $datePicker->addFilter(new Zend_Filter_HtmlEntities());
        $datePicker->addFilter(new Zend_Filter_StripTags());
        $this->addElement($datePicker);

       
		$this->addElement('submit', 'send');
		$submitElement = $this->getElement('send');
		$submitElement->setAttrib('class',"btn-primary btn");
		$submitElement->setDecorators(array('ViewHelper','Description'));
		$submitElement->setLabel('Create Campaign');
		 
		$this->setDecorators(array('FormElements',
								   'Form'
		));
    }


}

