<?php

class Sms_Form_NewSmsc extends Zend_Form
{

public function init()
    {
        $this->setAction('create');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    
    	$username = new Zend_Form_Element_Text('username');
    	$username->setDecorators(array('ViewHelper',
						    			'Description'));
    	$username->setLabel('Username');
    	$username->class = "form-control";
    	//$username->setAttrib('placeholder', 'System ID');
    	//$username->addErrorMessage('Please enter a username');
    	$username->setRequired(true);
        	
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);
    	
    	//Create Password Object.
    	$password = new Zend_Form_Element_Text('password');
    	$password->setDecorators(array('ViewHelper',
						    			'Description'));
    	$password->setLabel('Password');
    	$password->class = "form-control";
    	//$password->setAttrib('placeholder', 'Password');
    	$password->setRequired(true);
    	
    	//Add Filter
    	$password->addFilter(new Zend_Filter_HtmlEntities());
    	$password->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($password);
    	
    	//Role Hidden element
    	$selectRole = new Zend_Form_Element_Select('route');
		$roles = array(null => "NO ROUTES");
		$selectRole->addMultiOptions($roles);
		$selectRole->setDecorators(array('ViewHelper',
										'Description'));
		$selectRole->setLabel('Select Route');
		$selectRole->class = "form-control select2 custom-select";
		
		$selectRole->setRequired(true);
		//Add Validator
		//Add Filter
		$selectRole->addFilter(new Zend_Filter_HtmlEntities());
		$selectRole->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectRole);
		
		$tps = new Zend_Form_Element_Text('tps');
		$tps->setDecorators(array('ViewHelper','Description'));
		$tps->setLabel('Throughput');
    	$tps->class = "form-control";
    	//$tps->setAttrib('placeholder', 'TPS');
    	//$tps->addErrorMessage('Please enter a username');
    	$tps->setRequired(true);
    
    	//Add Validator
    	$tps->addValidator(new Zend_Validate_Int(array('locale' => 'de')));
    	
    	//Add Filter
    	$tps->addFilter(new Zend_Filter_HtmlEntities());
    	$tps->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($tps);
    	
    	$ip = new Zend_Form_Element_Text('ip');
    	$ip->class = "form-control";
    	//$ip->setAttrib('placeholder', 'SMPP Server');
    	//$ip->addValidator('Ip', array('allowipv6' => false));
    	$ip->setDecorators(array('ViewHelper','Description'));
    	$ip->setRequired(true);
    	$this->addElement($ip);

    	$bind = new Zend_Form_Element_Text('bind');
    	$bind->setDecorators(array('ViewHelper','Description'));
    	$bind->setLabel('Bind/Session');
    	$bind->class = "form-control";
    	//$bind->setAttrib('placeholder', 'Session');
    	//$bind->addErrorMessage('Please enter a username');
    	$bind->setRequired(false);
    	
    	//Add Validator
    	$bind->addValidator(new Zend_Validate_Digits());
    	
    	//Add Filter
    	$bind->addFilter(new Zend_Filter_HtmlEntities());
    	$bind->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($bind);
        
        $rx = new Zend_Form_Element_Text('Rx');
    	$rx->setDecorators(array('ViewHelper','Description'));
    	$rx->setLabel('Bind/Session');
    	$rx->class = "form-control";
    	//$bind->setAttrib('placeholder', 'Session');
    	//$bind->addErrorMessage('Please enter a username');
    	$rx->setRequired(false);
    	
    	//Add Validator
    	$rx->addValidator(new Zend_Validate_Digits());
    	
    	//Add Filter
    	$rx->addFilter(new Zend_Filter_HtmlEntities());
    	$rx->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($rx);
    	
    	$port = new Zend_Form_Element_Text('port');
    	$port->setDecorators(array('ViewHelper','Description'));
    	$port->setLabel('Server Port');
    	$port->class = "form-control";
    	//$port->setAttrib('placeholder', 'SMPP Server Port');
    	//$bind->addErrorMessage('Please enter a username');
    	$port->setRequired(true);
    	 
    	//Add Validator
    	$port->addValidator(new Zend_Validate_Digits());
    	 
    	//Add Filter
    	$port->addFilter(new Zend_Filter_HtmlEntities());
    	$port->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($port);
        
        $tx = new Zend_Form_Element_Text('Tx');
    	$tx->setDecorators(array('ViewHelper','Description'));
    	$tx->setLabel('Bind/Session');
    	$tx->class = "form-control";
    	//$bind->setAttrib('placeholder', 'Session');
    	//$bind->addErrorMessage('Please enter a username');
    	$tx->setRequired(false);
    	
    	//Add Validator
    	$tx->addValidator(new Zend_Validate_Digits());
    	
    	//Add Filter
    	$tx->addFilter(new Zend_Filter_HtmlEntities());
    	$tx->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($tx);
    	    	
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors'));
    	$submitElement->setLabel('Create');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'));
    }


}

