<?php

class Sms_Form_DomainRoute extends Zend_Form
{

    public function init()
    {
        $this->setAction('/sms/route/domain');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	$username = new Zend_Form_Element_Text('username');
    	$username->setDecorators(array('ViewHelper',
						    			'Description'));
    	$username->setLabel('Domain ');
    	$username->class = "form-control";		
    	$username->setAttrib('id', 'centDomain');
    	$username->setRequired(true);
    	
    	//Add Validator
    	//$username->addValidator(new Zend_Validate_StringLength(4,20));
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);
    	
    	$selectRole = new Zend_Form_Element_Select('route');
    	$roles = array("TRANSACTIONAL" => "TRANSACTIONAL",
    			"TRANS-SCRUB" => "TRANS-SCRUB",
    			"PROMOTIONAL" => "PROMOTIONAL");
    	$selectRole->addMultiOptions($roles);
    	$selectRole->setDecorators(array('ViewHelper','Description'));
    	$selectRole->setLabel('Select Route');
    	$selectRole->class = "form-control select2 custom-select";
    	 
    	$selectRole->setRequired(true);
    	 
    	$selectRole->addFilter(new Zend_Filter_HtmlEntities());
    	$selectRole->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($selectRole);
    	
    	$percentage = new Zend_Form_Element_Text('percentage');
    	$percentage->setDecorators(array('ViewHelper','Description'));
    	$percentage->setLabel('Percentage');
    	$percentage->class = "form-control";
    	$percentage->setRequired(true);
    	
    	//Add Validator
    	$percentage->addValidator(new Zend_Validate_Digits());
    	//Add Filter
    	$percentage->addFilter(new Zend_Filter_HtmlEntities());
    	$percentage->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($percentage);
    	
    	$typeOptions = array("1" => "SMS",
    			"2" => "VOICE");
    	$modetype = new Zend_Form_Element_Radio('type');
    	$modetype->addMultiOptions($typeOptions)->setSeparator('');
    	$modetype->setDecorators(array('ViewHelper','Description'));
    	$modetype->setLabel('Type');
    	$modetype->setRequired(true);
    	//Add Account type
    	$this->addElement($modetype);
    	
    	$modeOptions = array("1" => "Single",
    			"2" => "Multi-level");
    	$mode = new Zend_Form_Element_Radio('mode');
    	$mode->addMultiOptions($modeOptions)->setSeparator('');
    	$mode->setDecorators(array('ViewHelper','Description'));
    	$mode->setLabel('Type');
    	$mode->setRequired(true);
    	//Add Account type
    	$this->addElement($mode);
		
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
											'Description',
											'Errors'));
		$submitElement->setLabel('Set Percentage');
    	
    	$this->setDecorators(array('FormElements',
    			'Form'
    	));
    }

}

