<?php

class Sms_Form_Spam extends Zend_Form
{

    public function init()
    {
    	$this->setAction('add');
    	$this->setMethod('post');        
        
        $text = new Zend_Form_Element_Textarea('text');
        $text->setDecorators(array('ViewHelper','Description'));
        $text->setLabel('Text');
        $text->setRequired(true);
        $text->setAttrib('rows', '10');
        $text->setAttrib('cols', '30');
        $text->class = "form-control";
        
        //Add Validator
        //Add Filter
        $text->addFilter(new Zend_Filter_HtmlEntities());
        $text->addFilter(new Zend_Filter_StripTags());
        //Add Username Element
        $this->addElement($text);
        
       
        //submit
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn  btn-primary ");
    	$submitElement->setDecorators(array('ViewHelper','Description'));
    	$submitElement->setLabel('Submit');
    	$this->setDecorators(array('FormElements','Form'));
    }


}

