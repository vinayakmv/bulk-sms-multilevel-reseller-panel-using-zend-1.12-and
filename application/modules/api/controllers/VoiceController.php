<?php

use modules\voice\models\VoiceCampaignManager;
use modules\user\models\ActivityLogManager;
use modules\user\models\AccountManager;
use modules\voice\models\VoiceFileManager;
class Api_VoiceController extends Zend_Rest_Controller{  
	
   public function validateDate($date, $format = 'Y-m-d H:i')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
	
  public function init()
  {
  	$this->_helper->layout->disableLayout();
  	$bootstrap = $this->getInvokeArg('bootstrap');
  	
  	$this->domain =  Smpp_Utility::getFqdn();
  	$this->userAgent = $bootstrap->getResource('useragent');
  	$this->ip = $_SERVER['REMOTE_ADDR'];
  	
  	$options = $bootstrap->getOption('resources');
  	$contextSwitch = $this->_helper->getHelper('contextSwitch');
  	$contextSwitch->addActionContext('index', array('xml','json'))->initContext();
  }
	
  /**
   * The index action handles index/list requests; it should respond with a
   * list of the requested resources.
   */ 
  public function indexAction()
  {
	  if ($this->_request->isGet() || $this->_request->isPost()) {
	  	$campaignValues = array();
	  	$proceed = true;
	  	
	  	if ($this->_request->isGet()) {
	  		$apiType = 'GET';
	  	} else {
	  		$apiType = 'POST';
	  	}
	  	
		  	$apiKey = $this->_request->getHeader('X-Authentication-Key'); 
		  	$apiMethod = $this->_request->getHeader('X-Api-Method');
		  	
		  	if (!$apiKey){
		  		$apiKey = $this->_request->getParam('key');
		  	}
		  	
		  	if (!$apiMethod){
		  		$apiMethod = $this->_request->getParam('method');
		  	}
		  	
		  	if (isset($apiMethod) && $apiMethod == 'MT'){
		  		
		  		$mobile = $this->_request->getParam('mobile');
		  		$group = $this->_request->getParam('group');
		  		
		  		$audio = $this->_request->getParam('audio');
		  		$saveAudio = $this->_request->getParam('save');
		  		$saveAudio = (!isset($saveAudio) ? '0' : '1');
		  		$text = $this->_request->getParam('text');
		  		$file = $this->_request->getParam('file');
		  		
		  		$route = $this->_request->getParam('route');
		  		$callDuration = $this->_request->getParam('duration');
		  		$voice = $this->_request->getParam('voice');
		  		$voice = (!isset($voice) ? 'voice_nitech_us_slt_arctic_hts' : $voice);
		  		$audioName = $this->_request->getParam('audioname');
		  		$audioName = (!isset($audioName) ? '' : $audioName);
		  		$dynamic = $this->_request->getParam('dynamic');
		  		$campType = $this->_request->getParam('camptype');
		  		$campaignType = ($campType == 'survey' ?  '2' : '1');
		  		$schedule = $this->_request->getParam('schedule');
		  		$callback = $this->_request->getParam('callback');
		  		$callback = urldecode($callback);
		  		$sender = $this->_request->getParam('sender');
		  		$sender = (!isset($sender) ? '0000000000' : $sender);
		  		$mobile = str_replace(',', "\n", $mobile);
		  		
		  		if (isset($_FILES['file'])){
		  					//$upload = new Zend_File_Transfer_Adapter_Http();
		  					$upload = new Zend_File_Transfer();
		  					$upload->addValidator('Extension', false, array('mp3,wav'));
		  					$files = $upload->getFileInfo();
		  					
		  					foreach ($files as $file => $info){
		  						
		  						$name = explode('.',$info['name']);
		  							
		  						$newFilename = 'apiAudio-' . uniqid() . '.' . $name['1'];
		  						
		  						$upload->addFilter('Rename',
		  								array('target' => '/var/www/html/webpanel/audioFiles/'."$newFilename",
		  										'overwrite' => true));
		  						
		  						move_uploaded_file($info['tmp_name'], "/var/www/html/webpanel/audioFiles/".$newFilename);
		  							
		  						if (!$upload->isUploaded($file)) {
		  							$proceed = false;
		  							$this->view->error = 'ERR2066';
		  							$this->view->data = array('message' => 'Audio Upload Failed');
		  							$this->getResponse()->setHttpResponseCode(200);
		  						}else{
		  							
		  							if (!$file) {
		  								$proceed = false;
		  								$this->view->error = 'ERR2065';
		  								$this->view->data = array('message' => 'Invalid Audio Format');
		  								$this->getResponse()->setHttpResponseCode(200);
		  							}else{
		  								//if ($upload->receive()) {
		  									if ($dynamic){
		  										$fileAudio[$file] = $newFilename;
		  									}else{
		  										$file = $newFilename;
		  									}
		  								//}
		  							}
		  						}
		  						
		  						
		  					}
		  			
		  		    		  		   
		  		}
		  		if (isset($schedule)){
		  			$schedule = urldecode($schedule);
		  			if (!$this->validateDate($schedule)){
		  				$proceed = false;
		  				$this->view->error = 'ERR2061';
		  				$this->view->data = array('message' => 'Invalid Scheduled Date or Time');
		  				$this->getResponse()->setHttpResponseCode(200);
		  				
		  			}else{
		  				
		  				$startTime = explode(' ', $schedule);
		  				$sheduledate = $startTime['0'];
		  				$scheduleTime = $startTime['1'];
		  				
		  			}
		  			
		  			if ($dynamic){
		  				$campaignValues['input'] = 'DYNAMIC-SCHEDULED';
		  			}else{
		  				$campaignValues['input'] = 'SCHEDULED';
		  			}
					
		  		}else {
		  			$sheduledate = date('Y-m-d');
		  			$scheduleTime = date('H:i');
		  			if ($dynamic){
		  				$campaignValues['input'] = 'DYNAMIC-COMPOSE';
		  			}else{
		  				$campaignValues['input'] = 'COMPOSE';
		  			}					
		  		}
		  		//$cdte = date('Y-m-d H:i:s');
		  		//$dateTime = $sheduledate.' '.$scheduleTime;
		  		
		  		if (!isset($apiKey, $route, $callDuration) && ($mobile || $group)){
		  			$this->view->error = 'ERR2060';
		  			$this->view->data = array('message' => 'Partial Content or Invalid Fields');
		  			$this->getResponse()->setHttpResponseCode(200);
		  		}elseif (!($audio || $text || $file)) {
		  			$this->view->error = 'ERR2060';
		  			$this->view->data = array('message' => 'Partial Content or Invalid Fields');
		  			$this->getResponse()->setHttpResponseCode(200);
		  		}else{
		  			$utility = new Smpp_Utility();
		  			$emUser = $utility->getEntityManager('user');
		  			$emVoice = $utility->getEntityManager('voice');
		  			$emSmpp = $utility->getEntityManager('smpp');
		  			//$apiRepo = $em->getRepository('Models\Apikey')->getApiDetails($apiKey);
		  			$apiRepo = $emUser->getRepository('modules\user\models\Apikey')->findOneBy(array('apiKey' => "$apiKey", 'status' => '1'));
		  		
		  			if ($apiRepo) {
		  		
		  				//$userId = $apiRepo[0]['user_id'];
		  				$userId = $apiRepo->user;
		  				$foundIp = true;
		  				//if ($apiRepo['0']['ip']){
		  				if ($apiRepo->ip){	
		  					$apiIp = explode(",",$ips);
		  					$ipAddress = $_SERVER['REMOTE_ADDR'];
		  		
		  					$keyIp = array_search($ipAddress, $apiIp, true);
		  		
		  					if ($keyIp == false){
		  						$foundIp = false;
		  					}
		  				}
		  				 
		  				if ($foundIp){
		  					$userRepo = $emUser->getRepository('modules\user\models\User')->getUserStatus($userId);
		  					//$usersRepo = $em->getRepository('Models\User')->find($userId);
		  					if ($userRepo[0]['status']=='1'){
		  						 
		  						if(strlen($sender)=='10' && is_numeric($sender)){
		  							//$em = $utility->getEntityManager();		  							
		  							 
		  							$campaignValues['campaignName'] = 'API-'.$apiType;
		  							 
		  							//Get API [trans/promo] route from user table
		  							$userApiRoute = $emUser->getRepository('modules\user\models\User')->getVoiceApiRoute($userId, $route);
		  							$voiceRouteId = $userApiRoute['0']['api_route'];
		  							$routes = $emUser->getRepository('modules\user\models\VoiceRoute')->findOneBy(array('id'=>$voiceRouteId,'status'=>'1'));
		  							if (isset($routes)){
		  								if ($route == 'TL' && $routes->type != 'TRANSACTIONAL'){
		  									$this->view->error = 'ERR4220';
			  								$this->view->data = array('message' => 'Route Mapping Failed');
			  								$this->getResponse()->setHttpResponseCode(200);
		  								}elseif ($route == 'SL' && $routes->type != 'TRANS-SCRUB'){
		  									$this->view->error = 'ERR4220';
	  										$this->view->data = array('message' => 'Route Mapping Failed');
	  										$this->getResponse()->setHttpResponseCode(200);
		  								}elseif($route == 'PL' && $routes->type != 'PROMOTIONAL'){
		  									$this->view->error = 'ERR4220';
	  										$this->view->data = array('message' => 'Route Mapping Failed');
	  										$this->getResponse()->setHttpResponseCode(200);
		  								}else {
		  											
		  										$voiceRoute = $routes->type.'|'.$routes->route.'|'.$routes->name;
		  											
		  										$campaignValues['voiceType'] = $voiceRoute;
		  										$campaignValues['typeSenderId'] = $sender;
		  										$campaignValues['selectSenderId'] = $sender;
		  										$campaignValues['saveAudio'] = $saveAudio;
		  										
		  										$campaignValues['campaignType'] = $campaignType;
		  										
		  										if ($callDuration == '30'){
		  											$campaignValues['selectDuration'] = '1';
		  										}elseif ($callDuration == '60'){
		  											$campaignValues['selectDuration'] = '2';
		  										}elseif ($callDuration == '90'){
		  											$campaignValues['selectDuration'] = '3';
		  										}elseif ($callDuration == '120'){
		  											$campaignValues['selectDuration'] = '4';
		  										}else{
		  											$campaignValues['selectDuration'] = '1';
		  										}
		  										
		  										if (isset($audio)){
		  											$campaignValues['audioType'] = '1';
		  											$campaignValues['selectAudio'] = $audio;
		  										}elseif (isset($text)){
		  											$campaignValues['audioType'] = '3';
		  											$campaignValues['audioName'] = $audioName;
		  											$campaignValues['text2speech'] = $text;
		  											$campaignValues['selectVoice'] = $voice;
		  										}elseif (isset($file)){
		  											$campaignValues['audioType'] = '2';
		  											$campaignValues['audioName'] = $audioName;
		  											$campaignValues['uploadAudio'] = $file;
		  											
		  											
		  										}
		  											
		  										// 1 => Select, 2 => Upload, 3 => Paste
	  											if ($dynamic){
	  												$campaignValues['contactType'] = '4';
	  												$campaignValues['audioType'] = '4';
	  												$campaignValues['text'] = $text;
	  												$campaignValues['audio'] = $fileAudio;
	  											}else{
	  												if ($group){
	  													$campaignValues['contactType'] = '1';
	  												}else{
	  													$campaignValues['contactType'] = '3';
	  												}
	  											}
		  										$campaignValues['saveContact'] = '0';
		  										$campaignValues['textContact'] = $mobile;
		  										$campaignValues['selectContact'] = $group;
		  										//$campaignValues['uploadContact'];
		  										$campaignValues['contactName'] = '';
		  										
		  										if ($proceed){
		  											$campaignValues['datePicker'] = $sheduledate;
		  											$campaignValues['timePicker'] = $scheduleTime;
		  											
		  											$voiceCampaignManager = new VoiceCampaignManager();
		  											$createCampaign = $voiceCampaignManager->createCampaign($campaignValues, $userId, true, $apiKey, $emVoice, $emUser, $emSmpp);
		  											
		  											//print_r($createCampaign);
		  											//exit();
		  											if (isset($createCampaign['Campaign']['isSuccess'])){
		  												$this->view->status = 'MT';
		  												$this->view->data = array('status' => "success", 'campid' => $createCampaign['Campaign']['data'], 'leads' => $createCampaign['Campaign']['leads'], 'duration' => $callDuration, 'date' => $sheduledate.' '.$scheduleTime);
		  												$this->getResponse()->setHttpResponseCode(200);
		  											}elseif (isset($createCampaign['Campaign']['credit'])){
		  												$this->view->error = 'ERR4020';
		  												$this->view->data = array('message' => 'Insufficient Credit');
		  												$this->getResponse()->setHttpResponseCode(200);
		  											}elseif(isset($createCampaign['Campaign']['sender'])){
		  												$this->view->error = 'ERR4091';
		  												$this->view->data = array('message' => 'Senderid Not Allowed');
		  												$this->getResponse()->setHttpResponseCode(200);
		  											}elseif(isset($createCampaign['Campaign']['template'])){
		  												$this->view->error = 'ERR4093';
		  												$this->view->data = array('message' => 'Template Not Allowed');
		  												$this->getResponse()->setHttpResponseCode(200);
		  											}elseif(isset($createCampaign['Campaign']['contactCount'])){
		  												$this->view->error = 'ERR2062';
		  												$this->view->data = array('message' => 'Contacts Is Invalid');
		  												$this->getResponse()->setHttpResponseCode(200);
			  										}
		  										}				
		  									
		  									
		  								}
		  		
		  							}else{
		  								$this->view->error = 'ERR4221';
		  								$this->view->data = array('message' => 'Route Not Defined');
		  								$this->getResponse()->setHttpResponseCode(200);
		  							}
		  							 
		  						}else{
		  							$this->view->error = 'ERR4092';
		  							$this->view->data = array('message' => 'Senderid Is Invalid');
		  							$this->getResponse()->setHttpResponseCode(200);
		  						}
		  		
		  					}
		  				}else {
		  					$this->view->error = 'ERR4510';
		  					$this->view->data = array('message' => 'Ip Not Accepted');
		  					$this->getResponse()->setHttpResponseCode(200);
		  				}
		  		
		  			}else{
		  				$this->view->error = 'ERR4010';
		  				$this->view->data = array('message' => 'Unauthorized Or Invalid Key');
		  				$this->getResponse()->setHttpResponseCode(200);
		  			}
		  		}
		  	}elseif (isset($apiMethod) && $apiMethod == 'DLR'){
		  		$campId = $this->_request->getParam('campid');
		  		$page = $this->_request->getParam('page');
		  		$page = (isset($page) ? $page : '1');
		  		
		  		if (!isset($apiKey, $campId)){
		  			$this->view->error = 'ERR2060';
		  			$this->view->data = array('message' => 'Partial Content or Invalid fields');
		  			$this->getResponse()->setHttpResponseCode(200);
		  		}else {
		  		
		  			$utility = new Smpp_Utility();
		  			$emUser = $utility->getEntityManager('user');
		  			$emVoice = $utility->getEntityManager('voice');
		  			//$apiRepo = $em->getRepository('Models\Apikey')->getApiDetails($apiKey);
		  			$apiRepo = $emUser->getRepository('modules\user\models\Apikey')->findOneBy(array('apiKey' => "$apiKey", 'status' => '1'));
		  		
		  			if ($apiRepo) {
		  				//$userId = $apiRepo[0]['user_id'];
		  				$userId = $apiRepo->user;
		  				$foundIp = true;
		  				//if ($apiRepo['0']['ip']){
		  				if ($apiRepo->ip){
		  					$apiIp = explode(",",$ips);
		  					$ipAddress = $_SERVER['REMOTE_ADDR'];
		  				
		  					$keyIp = array_search($ipAddress, $apiIp, true);
		  				
		  					if ($keyIp == false){
		  						$foundIp = false;
		  					}
		  				}
		  					
		  				if ($foundIp){
		  					$userRepo = $emUser->getRepository('modules\user\models\User')->getUserStatus($userId);
		  					//$usersRepo = $em->getRepository('Models\User')->find($userId);
		  					if ($userRepo[0]['status']=='1'){
		  						$campaignRepo = $emVoice->getRepository('modules\voice\models\VoiceCampaign')->findOneBy(array('id' =>$campId, 'user'=> $userId, 'status' => '1'));
		  						if($campaignRepo){
		  							$type = $campaignRepo->type;
		  							
		  							$queryBuilder = $emUser->createQueryBuilder();
		  							$queryBuilder->select('e')  ->from('modules\user\models\DispositionStatus', 'e')->where('e.status = ?1')->setParameter(1, '1');
		  							$error = $queryBuilder->getQuery()->getResult();
		  							foreach ($error as $reportResult){
		  								$errorCode[$reportResult->errorCode]=$reportResult->statusDLR;
		  							}
				  					  if ($type == 'TRANSACTIONAL') {
								      	$sentVoiceMt = 'sent_voice_trans';
								      }else {
								      	$sentVoiceMt = 'sent_voice_promo';
								      }
		  							$dlrUrl = $userId.'@'.$campId.'-%';
		  							//$dlrUrl = '7@'.$campId.'-%';
		  							$queryCount= "SELECT count(1) as counter FROM  `$sentVoiceMt` WHERE dlr_url LIKE '$dlrUrl'";
		  							$limit = '5000';
		  							$stmt = $emVoice->getConnection()->prepare($queryCount);
		  							$stmt->execute();
		  							$countRepo = $stmt->fetchAll(); 
		  							$totalCount = $countRepo['0']['counter'];
		  							$totalPage = ceil($totalCount/$limit);
		  							if($page == '1'){
		  								$i = '0';
		  								$startLimit = '0';
		  								$endLimit = '5000';
		  							}else{
		  								$i = $limit*($page-1);
		  								$startLimit = $limit*($page-1);
		  								$endLimit = $limit*$page;
		  							}
		  							
		  							$dlr_MT= "SELECT `sender`,`receiver`,`dlr_url`,`dlrdata`,`status`,`time` FROM  `$sentVoiceMt` WHERE dlr_url LIKE '$dlrUrl' LIMIT $startLimit, $endLimit";
		  							$stmt = $emVoice->getConnection()->prepare($dlr_MT);
		  							$stmt->execute();
		  							$voiceMTRepo= $stmt->fetchAll();
		  							if(isset($voiceMTRepo) && $voiceMTRepo){
		  								
		  								foreach ($voiceMTRepo as $SuccessDLRfetch) {
		  									
		  								$time = $SuccessDLRfetch['time'];
		  								$delTime=date('d/m/Y H:i:s', $time);
		  								$fetch = $SuccessDLRfetch['dlrdata'];
		  								$statusMt = $SuccessDLRfetch['status'];
		  								foreach ( $errorCode as  $key => $keyword ){
		  									if (strpos($fetch, $key)!== FALSE ){
		  										$status = $keyword;
		  										break;
		  									}else{
		  										$status = 'Unknown';
		  									}
		  								}
		  								$message='';
		  								if(isset($status) && !empty($status)){
		  									// continue;
		  								}else{
		  									$status = 'Unknown';
		  								}
		  								if ($statusMt == '0'){
		  									$status = 'Awaiting DLR';
		  								}
		  								$i++;
		  								//$row[] = $i;
		  								//$row[] = $SuccessDLRfetch['receiver'];
		  								//$row[] = $SuccessDLRfetch['sender'];
		  								//$row[] = $message;
		  								//$row[] = $messageMain;
		  								//$row[] = $status;
		  								//$row[] = $delTime;
		  								//$row[] = $dlrTime1;
		  								//$row[] = $creditCount;
		  								//$row[] = $voiceCount;
		  								$reportData[] = array('id' => $i,'mobile' => $SuccessDLRfetch['receiver'] , 'dlr' => $status);
		  							}
		  							}
		  							if (isset($reportData)){
		  								$this->view->status = 'DLR';
		  								$this->view->data = array('status' => "success",'total' => $totalCount,'pages'=>$totalPage,'current'=>$page,  'report' =>$reportData);
		  							}else{
		  								$this->view->error = 'ERR2065';
		  								$this->view->data = array('message' => 'Invalid Page');
		  								$this->getResponse()->setHttpResponseCode(200);
		  							}
		  							
		  						}else{
		  							$this->view->error = 'ERR2064';
		  							$this->view->data = array('message' => 'Invalid Campaign');
		  							$this->getResponse()->setHttpResponseCode(200);
		  						}
		  					}
		  				}else{
		  					$this->view->error = 'ERR4510';
		  					$this->view->data = array('message' => 'Ip Not Accepted');
		  					$this->getResponse()->setHttpResponseCode(200);
		  				}
		  			}else{
		  				$this->view->error = 'ERR4010';
		  				$this->view->data = array('message' => 'Unauthorized Or Invalid Key');
		  				$this->getResponse()->setHttpResponseCode(200);
		  			}
		  		}
	  		}elseif (isset($apiMethod) && $apiMethod == 'GET'){
	  			$route = $this->_request->getParam('route');
	  			$scheduled = $this->_request->getParam('scheduled');
	  			$campId = $this->_request->getParam('campid');
	  			
	  			if (!isset($apiKey)){
	  				$this->view->error = 'ERR2060';
	  				$this->view->data = array('message' => 'Partial Content or Invalid fields');
	  				$this->getResponse()->setHttpResponseCode(200);
	  			}else {
	  				$utility = new Smpp_Utility();
	  				$emUser = $utility->getEntityManager('user');
	  				$emVoice = $utility->getEntityManager('voice');
	  				//$apiRepo = $em->getRepository('Models\Apikey')->getApiDetails($apiKey);
	  				$apiRepo = $emUser->getRepository('modules\user\models\Apikey')->findOneBy(array('apiKey' => "$apiKey", 'status' => '1'));
	  			
	  				if ($apiRepo) {
	  					//$userId = $apiRepo[0]['user_id'];
	  					$userId = $apiRepo->user;
	  					$foundIp = true;
	  					//if ($apiRepo['0']['ip']){
	  					if ($apiRepo->ip){
	  						$apiIp = explode(",",$ips);
	  						$ipAddress = $_SERVER['REMOTE_ADDR'];
	  			
	  						$keyIp = array_search($ipAddress, $apiIp, true);
	  			
	  						if ($keyIp == false){
	  							$foundIp = false;
	  						}
	  					}
	  						
	  					if ($foundIp){
	  						$userRepo = $emUser->getRepository('modules\user\models\User')->getUserStatus($userId);
	  						//$usersRepo = $em->getRepository('Models\User')->find($userId);
	  						if ($userRepo[0]['status']=='1'){

	  							if (isset($route)){
	  								$accountRepo = $emUser->getRepository('modules\user\models\Account')->getBalance($userId);
	  								
	  								if ($route == 'TL'){
	  									$trans = $accountRepo[0]['voice_trans_credit'];
	  									$transApi = $accountRepo[0]['voice_trans_credit_api'];
	  									if ($accountRepo[0]['role'] == 'ADMIN' || $accountRepo[0]['role'] == 'L1-RESELLER' || $accountRepo[0]['role'] == 'P1-RESELLER'){
	  										$transMain = $accountRepo[0]['voice_trans_credit_main'];
	  										$transApiMain = $accountRepo[0]['voice_trans_credit_api_main'];
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'web'=>$trans,'api'=>$transApi,'web_main'=>$transMain,'api_main'=>$transApiMain);
	  									}else{
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'web'=>$trans,'api'=>$transApi);
	  									}
	  										
	  								}elseif ($route == 'PL'){
	  									$promo = $accountRepo[0]['voice_promo_credit'];
	  									$promoApi = $accountRepo[0]['voice_promo_credit_api'];
	  									if ($accountRepo[0]['role'] == 'ADMIN' || $accountRepo[0]['role'] == 'L1-RESELLER' || $accountRepo[0]['role'] == 'P1-RESELLER'){
	  										$promoMain = $accountRepo[0]['voice_promo_credit_main'];
	  										$promoApiMain = $accountRepo[0]['voice_promo_credit_api_main'];
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'web'=>$promo,'api'=>$promoApi,'web_main'=>$promoMain,'api_main'=>$promoApiMain);
	  											
	  									}else{
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'web'=>$promo,'api'=>$promoApi);
	  									}
	  								}else{
	  									$this->view->error = 'ERR4220';
	  									$this->view->data = array('message' => 'Route Mapping Failed');
	  									$this->getResponse()->setHttpResponseCode(200);
	  								}
	  							}elseif (isset($scheduled)){
	  							
	  								$scheduleRepos = $emVoice->getRepository('modules\voice\models\VoiceCampaign')->getScheduledCampaigns($userId);
	  								
	  								if(isset($scheduleRepos) && $scheduleRepos){
	  									$i = 0;
	  									foreach ($scheduleRepos as $scheduleRepo){
		  									$i++;
		  									$reportData[] = array('id' => $i,'campid' => $scheduleRepo['id'] , 'type' => $scheduleRepo['type'], 'schedule' => $scheduleRepo['start_date'], 'credit' => $scheduleRepo['credit']);
		  										
	  									}
	  									$this->view->status = 'GET';
	  									$this->view->data = array('status' => "success",'total' => $i,  'campaign' =>$reportData);
	  											
	  								}else{
	  									$this->view->error = 'ERR2063';
	  									$this->view->data = array('message' => 'No Scheduled Campaigns');
	  									$this->getResponse()->setHttpResponseCode(200);
	  								}
	  								
	  								
	  							}elseif (isset($campId)){
	  								
	  								$campaignRepo = $emVoice->getRepository('modules\voice\models\VoiceCampaign')->findOneBy(array('user' => $userId, 'id' => $campId));
	  									
	  								if(isset($campaignRepo) && $campaignRepo){
	  									
	  									$lead = $campaignRepo->leadsFile;
	  									if (file_exists('/var/www/html/webpanel/contactFiles/'.$lead)) {
	  										if (($handle = fopen('/var/www/html/webpanel/contactFiles/'.$lead, "r")) !== FALSE) {
	  											$i=0;
	  											while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	  												if (isset($data['0'])){
	  													if (isset($data['1'])){
	  														$i++;
	  														$leadsData[] = array('id' => $i,'mobile' => $data['0'] , 'text' => $data['0']);
	  															
	  													}else{
	  														$i++;
	  														$leadsData[] = array('id' => $i,'mobile' => $data['0']);
	  															
	  													}
	  												}
	  											}
	  										}
	  										
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'campid' => $campaignRepo->id,  'leads' =>$leadsData);
	  									}else{
	  										$this->view->error = 'ERR2066';
	  										$this->view->data = array('message' => 'Contacts Not Found');
	  										$this->getResponse()->setHttpResponseCode(200);
	  									}
	  									
	  									
	  								}else{
	  									$this->view->error = 'ERR2064';
	  									$this->view->data = array('message' => 'Invalid Campaign');
	  									$this->getResponse()->setHttpResponseCode(200);
	  								}
	  							}else{
	  								$this->view->error = 'ERR2060';
	  								$this->view->data = array('message' => 'Partial Content Or Invalid fields');
	  								$this->getResponse()->setHttpResponseCode(200);
	  							}
	  								
	  						}
	  					}else{
	  						$this->view->error = 'ERR4510';
	  						$this->view->data = array('message' => 'Ip Not Accepted');
	  						$this->getResponse()->setHttpResponseCode(200);
	  					}
	  				}else{
		  				$this->view->error = 'ERR4010';
		  				$this->view->data = array('message' => 'Unauthorized Or Invalid Key');
		  				$this->getResponse()->setHttpResponseCode(200);
		  			}
	  			}
	  		
		    
    		
    }elseif (isset($apiMethod) && $apiMethod == 'DEL'){
  		$campId = $this->_request->getParam('campid');
  		
  		if (!isset($apiKey, $campId)){
  			$this->view->error = 'ERR2060';
  			$this->view->data = array('message' => 'Partial Content or Invalid fields');
  			$this->getResponse()->setHttpResponseCode(200);
  		}else {
  		
  			$utility = new Smpp_Utility();
  			$emUser = $utility->getEntityManager('user');
  			$emVoice = $utility->getEntityManager('voice');
  			//$apiRepo = $em->getRepository('Models\Apikey')->getApiDetails($apiKey);
  			$apiRepo = $emUser->getRepository('modules\user\models\Apikey')->findOneBy(array('apiKey' => "$apiKey", 'status' => '1'));
  		
  			if ($apiRepo) {
  				//$userId = $apiRepo[0]['user_id'];
  				$userId = $apiRepo->user;
  				$foundIp = true;
  				//if ($apiRepo['0']['ip']){
  				if ($apiRepo->ip){
  					$apiIp = explode(",",$ips);
  					$ipAddress = $_SERVER['REMOTE_ADDR'];
  				
  					$keyIp = array_search($ipAddress, $apiIp, true);
  				
  					if ($keyIp == false){
  						$foundIp = false;
  					}
  				}
  					
  				if ($foundIp){
  					$userRepo = $emUser->getRepository('modules\user\models\User')->getUserStatus($userId);
  					//$usersRepo = $em->getRepository('Models\User')->find($userId);
  					if ($userRepo[0]['status']=='1'){
  						$campaignRepo = $emVoice->getRepository('modules\voice\models\VoiceCampaign')->findOneBy(array('id' =>$campId,'user'=>$userId, 'status' => '0'));
  						
  						if($campaignRepo){
  							$type = $campaignRepo->type;
  							$campaignManager = new VoiceCampaignManager();
  							$cancelCampaign = $campaignManager->cancellCampaign($campaignRepo->id, $emVoice);
  							
  							if ($cancelCampaign){
  								$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
  								$activityManager = new ActivityLogManager();
  								$activityManager->createActivityLog($userRepo, $userId, 'API', 'VOICE', 'DEL', 'Campaign Cancelled Successfully', $this->domain, $this->ip, $this->userAgent, $cancelCampaign->id , 'CANCEL', 'CAMPAIGN', $emUser);
  							
  								$accountManager = new AccountManager();
  								if ($cancelCampaign->type == 'TRANSACTIONAL'){
  									if ($cancelCampaign->apiKey == 'WEB-PUSH'){
  										$account = '5';
  									}else{
  										$account = '10';
  									}
  							
  								}elseif ($cancelCampaign->type == 'PROMOTIONAL'){
  							
  									if ($cancelCampaign->apiKey == 'WEB-PUSH'){
  										$account = '4';
  									}else{
  										$account = '11';
  									}
  								}
  								
  								$accountArray = $accountManager->reCredit($userId, $account, $cancelCampaign->credit, $emUser);
  								$credit = $accountArray['user'];
  								$resellerRepo = $accountArray['reseller'];
  								$logRepo = $accountArray['log'];
  								
  								if ($credit) {
  									$activityManager = new ActivityLogManager();
  									$activityManager->createActivityLog($resellerRepo, $credit->id, 'API', 'VOICE', 'DEL', 'Recredit Transfered Successfully', $this->domain, $this->ip, $this->userAgent, $logRepo->id, 'RECREDIT', 'ACCOUNT', $emUser);
  								}
  							
	  							$this->view->status = 'DEL';
	  							$this->view->data = array('status' => "success",'campid' => $cancelCampaign->id , 'type' => $cancelCampaign->type, 'recredit' => $cancelCampaign->credit);
  							
  							}
  						
  						}else{
  							$this->view->error = 'ERR2064';
  							$this->view->data = array('message' => 'Invalid Campaign');
  							$this->getResponse()->setHttpResponseCode(200);
  						}
  					}
  					
  				}else{
  					$this->view->error = 'ERR4510';
  					$this->view->data = array('message' => 'Ip Not Accepted');
  					$this->getResponse()->setHttpResponseCode(200);
  				}
  			}else{
  				$this->view->error = 'ERR4010';
  				$this->view->data = array('message' => 'Unauthorized Or Invalid Key');
  				$this->getResponse()->setHttpResponseCode(200);
  			}
  		}
	    }else{
			    	$this->view->error = 'ERR4051';
					$this->view->data = array('message' => 'Api Method Not Allowed');
					$this->getResponse()->setHttpResponseCode(200);
		}
  	}else{
    	$this->view->error = 'ERR4052';
		$this->view->data = array('message' => 'HTTP Method Not Allowed');
		$this->getResponse()->setHttpResponseCode(200);
    }
  }

  public function listAction()
  {
    $this->_forward('index');
  }
  
  public function getAction()
  {
		$this->_forward('index');
  }

  public function newAction()
  {   	
		$this->_forward('index');
  }
	
  public function postAction()
  {
		$this->_forward('index');
  }

  public function editAction()
  {    	 
		$this->_forward('index');
  }
	
  public function putAction()
  {
		$this->_forward('index');
  } 
	
  public function deleteAction()
  {
		$this->_forward('index');
  }


}

