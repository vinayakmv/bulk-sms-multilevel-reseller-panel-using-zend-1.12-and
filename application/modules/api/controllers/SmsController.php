<?php

use modules\sms\models\SmsCampaignManager;
use modules\user\models\ActivityLogManager;
use modules\user\models\AccountManager;
class Api_SmsController extends Zend_Rest_Controller{  
	public function validateDate($date, $format = 'Y-m-d H:i')
	{
		$d = DateTime::createFromFormat($format, $date);
    	return $d && $d->format($format) == $date;
	}
	
  public function init(){
  	$this->_helper->layout->disableLayout();
  	$bootstrap = $this->getInvokeArg('bootstrap');
  	$this->domain =  Smpp_Utility::getFqdn();
  	$this->userAgent = $bootstrap->getResource('useragent');
  	$this->ip = $_SERVER['REMOTE_ADDR'];
  	
  	$options = $bootstrap->getOption('resources');
  	$contextSwitch = $this->_helper->getHelper('contextSwitch');
  	$contextSwitch->addActionContext('index', array('xml','json'))->initContext();
  }
	
  /**
   * The index action handles index/list requests; it should respond with a
   * list of the requested resources.
   */ 
  public function indexAction(){
	  if ($this->_request->isGet() || $this->_request->isPost()) {
	  	$campaignValues = array();
	  	$proceed = true;
	  	
	  	if ($this->_request->isGet()) {
	  		$apiType = 'GET';
	  	} else {
	  		$apiType = 'POST';
	  	}
	  	
		  	$apiKey = $this->_request->getHeader('X-Authentication-Key'); 
		  	$apiMethod = $this->_request->getHeader('X-Api-Method');
		  	
		  	if (!$apiKey){
		  		$apiKey = $this->_request->getParam('key');
		  	}
		  	
		  	if (!$apiMethod){
		  		$apiMethod = $this->_request->getParam('method');
		  	}
		  	
		  	if (isset($apiMethod) && $apiMethod == 'MT'){
		  		
		  		$mobile = $this->_request->getParam('mobile');
		  		$group = $this->_request->getParam('group');
		  		$unicode = $this->_request->getParam('unicode');
		  		$route = $this->_request->getParam('route');
		  		$text = urldecode($this->_request->getParam('text'));
		  		$text = str_replace('%0D%0A', '%0A', urlencode($text));
		  		$text = urldecode($text);
		  		$text = strip_tags("$text");
		  		$orgText = $text;
		  		$flash = $this->_request->getParam('flash');
		  		$flash = (isset($flash) ? '1' : '0');
		  		$dynamic = $this->_request->getParam('dynamic');
		  		$dynamic = (isset($dynamic) ? '1' : '0');
		  		$schedule = $this->_request->getParam('schedule');
		  		$callback = $this->_request->getParam('callback');
		  		$callback = urldecode($callback);
		  		$sender = $this->_request->getParam('sender');
		  		
		  		if ($route == 'PL'){
		  			$sender = 'MSGHUB';
		  		}elseif($route == 'IL' && !isset($sender)){
		  			$sender = 'SMS INFO';
		  		}
		  		
		  		if ($route == 'IL'){
		  			$text = $text."\nOPTOUT 5258";
		  		}
		  		$mobile = str_replace(',', "\n", $mobile);
		  		 
		  		if (isset($schedule)){
		  			$schedule = urldecode($schedule);
		  			if (!$this->validateDate($schedule)){
		  				$proceed = false;
		  				$this->view->error = 'ERR2061';
		  				$this->view->data = array('message' => 'Invalid Scheduled Date or Time');
		  				$this->getResponse()->setHttpResponseCode(200);
		  				
		  			}else{
		  				
		  				$startTime = explode(' ', $schedule);
		  				$sheduledate = $startTime['0'];
		  				$scheduleTime = $startTime['1'];
		  				
		  			}
		  			
		  			if ($dynamic){
		  				$campaignValues['input'] = 'DYNAMIC-SCHEDULED';
		  			}else{
		  				$campaignValues['input'] = 'SCHEDULED';
		  			}
					
		  		}else {
		  			$sheduledate = date('Y-m-d');
		  			$scheduleTime = date('H:i');
		  			if ($dynamic){
		  				$campaignValues['input'] = 'DYNAMIC-COMPOSE';
		  			}else{
		  				$campaignValues['input'] = 'COMPOSE';
		  			}					
		  		}
		  		//$cdte = date('Y-m-d H:i:s');
		  		//$dateTime = $sheduledate.' '.$scheduleTime;
		  		
		  		
		  		if (!isset($apiKey, $text, $sender, $route) && ($mobile || $group)){
		  			$this->view->error = 'ERR2060';
		  			$this->view->data = array('message' => 'Partial Content or Invalid Fields');
		  			$this->getResponse()->setHttpResponseCode(200);
		  		}else {
		  			 
		  			$utility = new Smpp_Utility();
		  			$emUser = $utility->getEntityManager('user');
		  			$emSms = $utility->getEntityManager('sms');
		  			$emSmpp = $utility->getEntityManager('smpp');
		  			$emUrl = $utility->getEntityManager('url');

		  			//$apiRepo = $em->getRepository('Models\Apikey')->getApiDetails($apiKey);
		  			$apiRepo = $emUser->getRepository('modules\user\models\Apikey')->findOneBy(array('apiKey' => "$apiKey"));
		  		
		  			if ($apiRepo) {
		  		
		  				//$userId = $apiRepo[0]['user_id'];
		  				$userId = $apiRepo->user;
		  				$foundIp = true;
		  				//if ($apiRepo['0']['ip']){
		  				if ($apiRepo->ip){	
		  					$apiIp = explode(",",$ips);
		  					$ipAddress = $_SERVER['REMOTE_ADDR'];
		  		
		  					$keyIp = array_search($ipAddress, $apiIp, true);
		  		
		  					if ($keyIp == false){
		  						$foundIp = false;
		  					}
		  				}
		  				 
		  				if ($foundIp){
		  					$userRepo = $emUser->getRepository('modules\user\models\User')->getUserStatus($userId);
		  					//$usersRepo = $em->getRepository('Models\User')->find($userId);
		  					if ($userRepo[0]['status']=='1'){
		  						 
		  						if(strlen($sender) >= '6' || strlen($sender) <= '11'){
		  							//$em = $utility->getEntityManager();		  							
		  							 
		  							$campaignValues['campaignName'] = 'API-'.$apiType;
		  							 
		  							//Get API [trans/promo] route from user table
		  							$userApiRoute = $emUser->getRepository('modules\user\models\User')->getApiRoute($userId, $route);
		  							$smsRouteId = $userApiRoute['0']['api_route'];
		  							$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$smsRouteId,'status'=>'1'));
		  							if (isset($routes)){
		  								if ($route == 'TL' && $routes->type != 'TRANSACTIONAL'){
		  									$this->view->error = 'ERR4220';
			  								$this->view->data = array('message' => 'Route Mapping Failed');
			  								$this->getResponse()->setHttpResponseCode(200);
		  								}elseif ($route == 'SL' && $routes->type != 'TRANS-SCRUB'){
		  									$this->view->error = 'ERR4220';
	  										$this->view->data = array('message' => 'Route Mapping Failed');
	  										$this->getResponse()->setHttpResponseCode(200);
		  								}elseif($route == 'PL' && $routes->type != 'PROMOTIONAL'){
		  									$this->view->error = 'ERR4220';
	  										$this->view->data = array('message' => 'Route Mapping Failed');
	  										$this->getResponse()->setHttpResponseCode(200);
		  								}elseif($route == 'IL' && $routes->type != 'INTERNATIONAL'){
		  									$this->view->error = 'ERR4220';
	  										$this->view->data = array('message' => 'Route Mapping Failed');
	  										$this->getResponse()->setHttpResponseCode(200);
		  								}else {
		  									
		  									$smsCountCalculator = new Smpp_Sms_SMSCountCalculator();
		  									//Santize to GSM7
		  									
		  									if (!$unicode){
		  										$text = $smsCountCalculator->sanitizeToGSM($text);
		  										if ($text == " "){
		  											$this->view->status = 'ERR4094';
		  											$this->view->data = array('message' => 'Unicode 16bit Content Failed');
		  											$this->getResponse()->setHttpResponseCode(200);
		  										}else {
		  											$campaignValues['text'] = $text;
		  											//Count Calculation
		  											$smsCounter = $smsCountCalculator->count($text);
		  											$smsCount =  $smsCounter->messages;
		  											$smsLength =  $smsCounter->length;
		  											$smsEncoding =  $smsCounter->encoding;
		  											/*
		  											 $smsCounter = new Smpp_Sms_SMSCounter();
		  											 $smsCounter = $smsCounter->count($text);
		  											 $smsCount   = $smsCounter->messages;
		  											 $smsLength  = $smsCounter->length;
		  											 $smsEncoding = $smsCounter->encoding;
		  											 	
		  											 	
		  											 $smsCalculator = new Smpp_Sms_SMSCalculator();
		  											 $smsCount = $smsCalculator->getPartCount($text);
		  											 $smsEncoding = $smsCalculator->getCharset($text);
		  											 $smsLength = '0';
		  											 */
		  											
		  											$smsRoute = $routes->type.'|'.$routes->route.'|'.$routes->name;
		  											
		  											$campaignValues['smsType'] = $smsRoute;
		  											
		  											$campaignValues['flashRadio'] = $flash;
		  											$campaignValues['typeSenderId'] = $sender;
		  											$campaignValues['selectSenderId'] = $sender;
		  											$campaignValues['text'] = $text;
		  											
		  											
		  											// 1 => Select, 2 => Upload, 3 => Paste
		  											
		  											if ($dynamic){
		  												$campaignValues['contactType'] = '4';
		  											}else{
		  												if ($group){
		  													$campaignValues['contactType'] = '1';
		  												}else{
		  													$campaignValues['contactType'] = '3';
		  												}
		  												
		  											}
		  											$campaignValues['saveContact'] = '0';
		  											$campaignValues['textContact'] = $mobile;
		  											$campaignValues['selectContact'] = $group;
		  											//$campaignValues['uploadContact'];
		  											$campaignValues['contactName'] = '';
		  											
		  											
		  											if ($proceed){
		  												$campaignValues['datePicker'] = $sheduledate;
		  												$campaignValues['timePicker'] = $scheduleTime;
		  												
		  												$campaignValues['tinyRadio'] = '0';
		  												$campaignValues['selectUrl'] = null;

			  											$smsCampaignManager = new SmsCampaignManager();
			  											$createCampaign = $smsCampaignManager->createCampaign($campaignValues, $orgText, $smsLength, $smsCount, $smsEncoding, $userId, true, $apiKey, $emSms, $emUser, $emSmpp, $emUrl);
		  											
			  											//print_r($createCampaign);
			  											//exit();
			  											if (isset($createCampaign['Campaign']['isSuccess'])){
			  												$this->view->status = 'MT';
			  												$this->view->data = array('status' => "success", 'campid' => $createCampaign['Campaign']['data'], 'leads' => $createCampaign['Campaign']['leads'], 'smscount' => $smsCount, 'chars' => $smsLength, 'date' => $sheduledate.' '.$scheduleTime);
			  												$this->getResponse()->setHttpResponseCode(200);
			  											}elseif (isset($createCampaign['Campaign']['credit'])){
			  												$this->view->error = 'ERR4020';
			  												$this->view->data = array('message' => 'Insufficient Credit');
			  												$this->getResponse()->setHttpResponseCode(200);
			  											}elseif(isset($createCampaign['Campaign']['sender'])){
			  												$this->view->error = 'ERR4091';
			  												$this->view->data = array('message' => 'Senderid Not Allowed');
			  												$this->getResponse()->setHttpResponseCode(200);
			  											}elseif(isset($createCampaign['Campaign']['template'])){
			  												$this->view->error = 'ERR4093';
			  												$this->view->data = array('message' => 'Template Not Allowed');
			  												$this->getResponse()->setHttpResponseCode(200);
			  											}elseif(isset($createCampaign['Campaign']['contactCount'])){
			  												$this->view->error = 'ERR2062';
			  												$this->view->data = array('message' => 'Invalid Contacts');
			  												$this->getResponse()->setHttpResponseCode(200);
			  											}elseif(isset($createCampaign['Campaign']['text'])){
			  												$this->view->error = 'ERR2067';
			  												$this->view->data = array('message' => 'Message Is Empty');
			  												$this->getResponse()->setHttpResponseCode(200);
			  											}
		  											}
		  										}
		  									}else {
		  										$campaignValues['text'] = $text;
		  										//Count Calculation
		  										$smsCounter = $smsCountCalculator->count($text);
		  										$smsCount =  $smsCounter->messages;
		  										$smsLength =  $smsCounter->length;
		  										$smsEncoding =  $smsCounter->encoding;
		  										/*
		  										 $smsCounter = new Smpp_Sms_SMSCounter();
		  										 $smsCounter = $smsCounter->count($text);
		  										 $smsCount   = $smsCounter->messages;
		  										 $smsLength  = $smsCounter->length;
		  										 $smsEncoding = $smsCounter->encoding;
		  										 	
		  										 	
		  										 $smsCalculator = new Smpp_Sms_SMSCalculator();
		  										 $smsCount = $smsCalculator->getPartCount($text);
		  										 $smsEncoding = $smsCalculator->getCharset($text);
		  										 $smsLength = '0';
		  										 */
		  											
		  										$smsRoute = $routes->type.'|'.$routes->route.'|'.$routes->name;
		  											
		  										$campaignValues['smsType'] = $smsRoute;
		  											
		  										$campaignValues['flashRadio'] = $flash;
		  										$campaignValues['typeSenderId'] = $sender;
		  										$campaignValues['selectSenderId'] = $sender;
		  										$campaignValues['text'] = $text;
		  											
		  											
		  										// 1 => Select, 2 => Upload, 3 => Paste
	  											if ($dynamic){
	  												$campaignValues['contactType'] = '4';
	  											}else{
	  												if ($group){
	  													$campaignValues['contactType'] = '1';
	  												}else{
	  													$campaignValues['contactType'] = '3';
	  												}
	  											}
		  										$campaignValues['saveContact'] = '0';
		  										$campaignValues['textContact'] = $mobile;
		  										$campaignValues['selectContact'] = $group;
		  										//$campaignValues['uploadContact'];
		  										$campaignValues['contactName'] = '';
		  										
		  										if ($proceed){
		  											$campaignValues['datePicker'] = $sheduledate;
		  											$campaignValues['timePicker'] = $scheduleTime;
		  											
		  											$smsCampaignManager = new SmsCampaignManager();
		  											$createCampaign = $smsCampaignManager->createCampaign($campaignValues, $orgText, $smsLength, $smsCount, $smsEncoding, $userId, true, $apiKey, $emSms, $emUser, $emSmpp);
		  											//print_r($createCampaign);
		  											//exit();
		  											if (isset($createCampaign['Campaign']['isSuccess'])){
		  												$this->view->status = 'MT';
		  												$this->view->data = array('status' => "success", 'campid' => $createCampaign['Campaign']['data'], 'leads' => $createCampaign['Campaign']['leads'], 'smscount' => $smsCount, 'chars' => $smsLength, 'date' => $sheduledate.' '.$scheduleTime);
		  												$this->getResponse()->setHttpResponseCode(200);
		  											}elseif (isset($createCampaign['Campaign']['credit'])){
		  												$this->view->error = 'ERR4020';
		  												$this->view->data = array('message' => 'Insufficient Credit');
		  												$this->getResponse()->setHttpResponseCode(200);
		  											}elseif(isset($createCampaign['Campaign']['sender'])){
		  												$this->view->error = 'ERR4091';
		  												$this->view->data = array('message' => 'Senderid Not Allowed');
		  												$this->getResponse()->setHttpResponseCode(200);
		  											}elseif(isset($createCampaign['Campaign']['template'])){
		  												$this->view->error = 'ERR4093';
		  												$this->view->data = array('message' => 'Template Not Allowed');
		  												$this->getResponse()->setHttpResponseCode(200);
		  											}elseif(isset($createCampaign['Campaign']['contactCount'])){
		  												$this->view->error = 'ERR2062';
		  												$this->view->data = array('message' => 'Contacts Is Invalid');
		  												$this->getResponse()->setHttpResponseCode(200);
			  										}elseif(isset($createCampaign['Campaign']['text'])){
			  											$this->view->error = 'ERR2067';
			  											$this->view->data = array('message' => 'Message Is Empty');
			  											$this->getResponse()->setHttpResponseCode(200);
			  										}
		  										}
		  										
		  									}						
		  									
		  									
		  								}
		  		
		  							}else{
		  								$this->view->error = 'ERR4221';
		  								$this->view->data = array('message' => 'Route Not Defined');
		  								$this->getResponse()->setHttpResponseCode(200);
		  							}
		  							 
		  						}else{
		  							$this->view->error = 'ERR4092';
		  							$this->view->data = array('message' => 'Senderid Is Invalid');
		  							$this->getResponse()->setHttpResponseCode(200);
		  						}
		  		
		  					}
		  				}else {
		  					$this->view->error = 'ERR4510';
		  					$this->view->data = array('message' => 'Ip Not Accepted');
		  					$this->getResponse()->setHttpResponseCode(200);
		  				}
		  		
		  			}else{
		  				$this->view->error = 'ERR4010';
		  				$this->view->data = array('message' => 'Unauthorized Or Invalid Key');
		  				$this->getResponse()->setHttpResponseCode(200);
		  			}
		  		}
		  	}elseif (isset($apiMethod) && $apiMethod == 'DLR'){
		  		$campId = $this->_request->getParam('campid');
		  		$page = $this->_request->getParam('page');
		  		$page = (isset($page) ? $page : '1');
		  		
		  		$route = $this->_request->getParam('route');
		  		$from = $this->_request->getParam('from');
		  		$to = $this->_request->getParam('to');
		  		
		  		$from = (isset($from) ? $from : date('Y-m-d'));
		  		$to = (isset($to) ? $to : date('Y-m-d'));		  		
		  		
		  		if (!isset($apiKey)){
		  			$this->view->error = 'ERR2060';
		  			$this->view->data = array('message' => 'Partial Content or Invalid fields');
		  			$this->getResponse()->setHttpResponseCode(200);
		  		}else {
		  		
		  			$utility = new Smpp_Utility();
		  			$emUser = $utility->getEntityManager('user');
		  			$emSms = $utility->getEntityManager('sms');
		  			//$apiRepo = $em->getRepository('Models\Apikey')->getApiDetails($apiKey);
		  			$apiRepo = $emUser->getRepository('modules\user\models\Apikey')->findOneBy(array('apiKey' => "$apiKey", 'status' => '1'));
		  		
		  			if ($apiRepo) {
		  				//$userId = $apiRepo[0]['user_id'];
		  				$userId = $apiRepo->user;
		  				$foundIp = true;
		  				//if ($apiRepo['0']['ip']){
		  				if ($apiRepo->ip){
		  					$apiIp = explode(",",$ips);
		  					$ipAddress = $_SERVER['REMOTE_ADDR'];
		  				
		  					$keyIp = array_search($ipAddress, $apiIp, true);
		  				
		  					if ($keyIp == false){
		  						$foundIp = false;
		  					}
		  				}
		  					
		  				if ($foundIp){
		  					$userRepo = $emUser->getRepository('modules\user\models\User')->getUserStatus($userId);
		  					//$usersRepo = $em->getRepository('Models\User')->find($userId);
		  					if ($userRepo[0]['status']=='1'){
		  						
		  						if (isset($route)){
		  							$from = urldecode($from);
		  							$from = $from;
		  							$to = urldecode($to);
		  							$to = $to;
		  									  							
		  							if (!$this->validateDate($to, 'Y-m-d') || !$this->validateDate($from, 'Y-m-d')){
		  								
		  								$proceed = false;
		  								$this->view->error = 'ERR2061';
		  								$this->view->data = array('message' => 'Invalid Date');
		  								$this->getResponse()->setHttpResponseCode(200);
		  							
		  							}else{
		  								
		  								$endDate = strtotime($to . "+1 days midnight");		  								
		  								$startDate = strtotime($from . "midnight");		  

		  								if ($route == 'TL') {
		  									$sentSmsMt = 'sent_sms_trans_mt';
		  									$sentSmsDlr = 'sent_sms_trans_dlr';
		  								}elseif ($route == 'PL' || $type == 'SL'){
		  									$sentSmsMt = 'sent_sms_promo_mt';
		  									$sentSmsDlr = 'sent_sms_promo_dlr';
		  								}elseif ($route == 'IL') {
		  									$sentSmsMt = 'sent_sms_inter_mt';
		  									$sentSmsDlr = 'sent_sms_inter_dlr';
		  								}else{
		  									$this->view->error = 'ERR4220';
		  									$this->view->data = array('message' => 'Route Mapping Failed');
		  									$this->getResponse()->setHttpResponseCode(200);
		  								}
		  								
		  								$queryBuilder = $emUser->createQueryBuilder();
		  								$queryBuilder->select('e')  ->from('modules\user\models\DLRStatus', 'e')->where('e.status = ?1')->setParameter(1, '1');
		  								$error = $queryBuilder->getQuery()->getResult();
		  								foreach ($error as $reportResult){
		  									$errorCode[$reportResult->errorCode]=$reportResult->statusDLR;
		  								}
		  								
		  								$dlrUrl = $userId.'@%';
		  								//$dlrUrl = '7@'.$campId.'-%';
		  								$queryCount= "SELECT count(1) as counter FROM  `$sentSmsMt` WHERE dlr_url LIKE '$dlrUrl' AND time > '$startDate' AND time < '$endDate'";
		  								$limit = '5000';
		  								$stmt = $emSms->getConnection()->prepare($queryCount);
		  								$stmt->execute();
		  								$countRepo = $stmt->fetchAll();
		  								$totalCount = $countRepo['0']['counter'];
		  								$totalPage = ceil($totalCount/$limit);
		  								if($page == '1'){
		  									$i = '0';
		  									$startLimit = '0';
		  									$endLimit = '5000';
		  								}else{
		  									$i = $limit*($page-1);
		  									$startLimit = $limit*($page-1);
		  									$endLimit = $limit*$page;
		  								}
		  									
		  								$dlr_MT= "SELECT `sender`,`receiver`,`msgdata`,`dlr_url`,`dlrdata`,`status`,`time` FROM  `$sentSmsMt` WHERE dlr_url LIKE '$dlrUrl'  AND time > '$startDate' AND time < '$endDate' LIMIT $startLimit, $endLimit";
		  								$stmt = $emSms->getConnection()->prepare($dlr_MT);
		  								$stmt->execute();
		  								$smsMTRepo= $stmt->fetchAll();
		  								if(isset($smsMTRepo) && $smsMTRepo){
		  								
		  									foreach ($smsMTRepo as $SuccessDLRfetch) {
		  											
		  										$time = $SuccessDLRfetch['time'];
		  										$delTime=date('d/m/Y H:i:s', $time);
		  										$fetch = $SuccessDLRfetch['dlrdata'];
		  										$statusMt = $SuccessDLRfetch['status'];
		  										foreach ( $errorCode as  $key => $keyword ){
		  											if (strpos($fetch, $key)!== FALSE ){
		  												$status = $keyword;
		  												break;
		  											}else{
		  												$status = 'Unknown';
		  											}
		  										}
		  										$message='';
		  										if(isset($status) && !empty($status)){
		  											// continue;
		  										}else{
		  											$status = 'Unknown';
		  										}
		  										if ($statusMt == '0'){
		  											$status = 'Awaiting DLR';
		  										}
		  										$i++;
		  										//$row[] = $i;
		  										//$row[] = $SuccessDLRfetch['receiver'];
		  										//$row[] = $SuccessDLRfetch['sender'];
		  										//$row[] = $message;
		  										//$row[] = $messageMain;
		  										//$row[] = $status;
		  										//$row[] = $delTime;
		  										//$row[] = $dlrTime1;
		  										//$row[] = $creditCount;
		  										//$row[] = $smsCount;
		  										$reportData[] = array('id' => $i, 'sender' => $SuccessDLRfetch['sender'], 'mobile' => $SuccessDLRfetch['receiver'] ,'message' => urldecode($SuccessDLRfetch['msgdata']),'time' => date("Y-m-d H:i:s",$SuccessDLRfetch['time']), 'dlr' => $status);
		  									}
		  								}
		  								if (isset($reportData)){
		  									$this->view->status = 'DLR';
		  									$this->view->data = array('status' => "success",'total' => $totalCount,'pages'=>$totalPage,'current'=>$page,  'report' =>$reportData);
		  								}else{
		  									$this->view->error = 'ERR2065';
		  									$this->view->data = array('message' => 'No Report');
		  									$this->getResponse()->setHttpResponseCode(200);
		  								}
		  								
		  							}
		  						}elseif(isset($campId)){
		  							$campaignRepo = $emSms->getRepository('modules\sms\models\SmsCampaign')->findOneBy(array('id' =>$campId, 'user'=> $userId, 'status' => '1'));
		  							if($campaignRepo){
		  								$type = $campaignRepo->type;
		  									
		  								$queryBuilder = $emUser->createQueryBuilder();
		  								$queryBuilder->select('e')  ->from('modules\user\models\DLRStatus', 'e')->where('e.status = ?1')->setParameter(1, '1');
		  								$error = $queryBuilder->getQuery()->getResult();
		  								foreach ($error as $reportResult){
		  									$errorCode[$reportResult->errorCode]=$reportResult->statusDLR;
		  								}
		  								if ($type == 'TRANSACTIONAL') {
		  									$sentSmsMt = 'sent_sms_trans_mt';
		  									$sentSmsDlr = 'sent_sms_trans_dlr';
		  								}elseif ($type == 'PROMOTIONAL' || $type == 'TRANS-SCRUB'){
		  									$sentSmsMt = 'sent_sms_promo_mt';
		  									$sentSmsDlr = 'sent_sms_promo_dlr';
		  								}elseif ($type == 'INTERNATIONAL') {
		  									$sentSmsMt = 'sent_sms_inter_mt';
		  									$sentSmsDlr = 'sent_sms_inter_dlr';
		  								}
		  								$dlrUrl = $userId.'@'.$campId.'-%';
		  								//$dlrUrl = '7@'.$campId.'-%';
		  								$queryCount= "SELECT count(1) as counter FROM  `$sentSmsMt` WHERE dlr_url LIKE '$dlrUrl'";
		  								$limit = '5000';
		  								$stmt = $emSms->getConnection()->prepare($queryCount);
		  								$stmt->execute();
		  								$countRepo = $stmt->fetchAll();
		  								$totalCount = $countRepo['0']['counter'];
		  								$totalPage = ceil($totalCount/$limit);
		  								if($page == '1'){
		  									$i = '0';
		  									$startLimit = '0';
		  									$endLimit = '5000';
		  								}else{
		  									$i = $limit*($page-1);
		  									$startLimit = $limit*($page-1);
		  									$endLimit = $limit*$page;
		  								}
		  									
		  								$dlr_MT= "SELECT `sender`,`receiver`,`msgdata`,`dlr_url`,`dlrdata`,`status`,`time` FROM  `$sentSmsMt` WHERE dlr_url LIKE '$dlrUrl' LIMIT $startLimit, $endLimit";
		  								$stmt = $emSms->getConnection()->prepare($dlr_MT);
		  								$stmt->execute();
		  								$smsMTRepo= $stmt->fetchAll();
		  								if(isset($smsMTRepo) && $smsMTRepo){
		  							
		  									foreach ($smsMTRepo as $SuccessDLRfetch) {
		  											
		  										$time = $SuccessDLRfetch['time'];
		  										$delTime=date('d/m/Y H:i:s', $time);
		  										$fetch = $SuccessDLRfetch['dlrdata'];
		  										$statusMt = $SuccessDLRfetch['status'];
		  										foreach ( $errorCode as  $key => $keyword ){
		  											if (strpos($fetch, $key)!== FALSE ){
		  												$status = $keyword;
		  												break;
		  											}else{
		  												$status = 'Unknown';
		  											}
		  										}
		  										$message='';
		  										if(isset($status) && !empty($status)){
		  											// continue;
		  										}else{
		  											$status = 'Unknown';
		  										}
		  										if ($statusMt == '0'){
		  											$status = 'Awaiting DLR';
		  										}
		  										$i++;
		  										//$row[] = $i;
		  										//$row[] = $SuccessDLRfetch['receiver'];
		  										//$row[] = $SuccessDLRfetch['sender'];
		  										//$row[] = $message;
		  										//$row[] = $messageMain;
		  										//$row[] = $status;
		  										//$row[] = $delTime;
		  										//$row[] = $dlrTime1;
		  										//$row[] = $creditCount;
		  										//$row[] = $smsCount;
		  										$reportData[] = array('id' => $i,'mobile' => $SuccessDLRfetch['receiver'] , 'dlr' => $status);
		  									}
		  								}
		  								if (isset($reportData)){
		  									$this->view->status = 'DLR';
		  									$this->view->data = array('status' => "success",'total' => $totalCount,'pages'=>$totalPage,'current'=>$page,  'report' =>$reportData);
		  								}else{
		  									$this->view->error = 'ERR2065';
		  									$this->view->data = array('message' => 'No Report');
		  									$this->getResponse()->setHttpResponseCode(200);
		  								}
		  									
		  							}else{
		  								$this->view->error = 'ERR2064';
		  								$this->view->data = array('message' => 'Invalid Campaign');
		  								$this->getResponse()->setHttpResponseCode(200);
		  							}
		  						}
		  						
		  					}
		  				}else{
		  					$this->view->error = 'ERR4510';
		  					$this->view->data = array('message' => 'Ip Not Accepted');
		  					$this->getResponse()->setHttpResponseCode(200);
		  				}
		  			}else{
		  				$this->view->error = 'ERR4010';
		  				$this->view->data = array('message' => 'Unauthorized Or Invalid Key');
		  				$this->getResponse()->setHttpResponseCode(200);
		  			}
		  		}
	  		}elseif (isset($apiMethod) && $apiMethod == 'GET'){
	  			$route = $this->_request->getParam('route');
	  			$scheduled = $this->_request->getParam('scheduled');
	  			$campId = $this->_request->getParam('campid');
	  			
	  			if (!isset($apiKey)){
	  				$this->view->error = 'ERR2060';
	  				$this->view->data = array('message' => 'Partial Content or Invalid fields');
	  				$this->getResponse()->setHttpResponseCode(200);
	  			}else {
	  				$utility = new Smpp_Utility();
	  				$emUser = $utility->getEntityManager('user');
	  				$emSms = $utility->getEntityManager('sms');
	  				//$apiRepo = $em->getRepository('Models\Apikey')->getApiDetails($apiKey);
	  				$apiRepo = $emUser->getRepository('modules\user\models\Apikey')->findOneBy(array('apiKey' => "$apiKey"));
	  			
	  				if ($apiRepo) {
	  					//$userId = $apiRepo[0]['user_id'];
	  					$userId = $apiRepo->user;
	  					$foundIp = true;
	  					//if ($apiRepo['0']['ip']){
	  					if ($apiRepo->ip){
	  						$apiIp = explode(",",$ips);
	  						$ipAddress = $_SERVER['REMOTE_ADDR'];
	  			
	  						$keyIp = array_search($ipAddress, $apiIp, true);
	  			
	  						if ($keyIp == false){
	  							$foundIp = false;
	  						}
	  					}
	  						
	  					if ($foundIp){
	  						$userRepo = $emUser->getRepository('modules\user\models\User')->getUserStatus($userId);
	  						//$usersRepo = $em->getRepository('Models\User')->find($userId);
	  						if ($userRepo[0]['status']=='1'){

	  							if (isset($route)){
	  								$accountRepo = $emUser->getRepository('modules\user\models\Account')->getBalance($userId);
	  								
	  								if ($route == 'TL'){
	  									$trans = $accountRepo[0]['sms_trans_credit'];
	  									$transApi = $accountRepo[0]['sms_trans_credit_api'];
	  									if ($accountRepo[0]['role'] == 'ADMIN' || $accountRepo[0]['role'] == 'L1-RESELLER' || $accountRepo[0]['role'] == 'P1-RESELLER'){
	  										$transMain = $accountRepo[0]['sms_trans_credit_main'];
	  										$transApiMain = $accountRepo[0]['sms_trans_credit_api_main'];
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'web'=>$trans,'api'=>$transApi,'web_main'=>$transMain,'api_main'=>$transApiMain);
	  									}else{
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'web'=>$trans,'api'=>$transApi);
	  									}
	  										
	  								}elseif ($route == 'PL'){
	  									$promo = $accountRepo[0]['sms_promo_credit'];
	  									$promoApi = $accountRepo[0]['sms_promo_credit_api'];
	  									if ($accountRepo[0]['role'] == 'ADMIN' || $accountRepo[0]['role'] == 'L1-RESELLER' || $accountRepo[0]['role'] == 'P1-RESELLER'){
	  										$promoMain = $accountRepo[0]['sms_promo_credit_main'];
	  										$promoApiMain = $accountRepo[0]['sms_promo_credit_api_main'];
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'web'=>$promo,'api'=>$promoApi,'web_main'=>$promoMain,'api_main'=>$promoApiMain);
	  											
	  									}else{
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'web'=>$promo,'api'=>$promoApi);
	  									}
	  								}elseif ($route == 'SL'){
	  									$scrub = $accountRepo[0]['sms_scrub_credit'];
	  									$scrubApi = $accountRepo[0]['sms_scrub_credit_api'];
	  									if ($accountRepo[0]['role'] == 'ADMIN' || $accountRepo[0]['role'] == 'L1-RESELLER' || $accountRepo[0]['role'] == 'P1-RESELLER'){
	  										$scrubMain = $accountRepo[0]['sms_scrub_credit_main'];
	  										$scrubApiMain = $accountRepo[0]['sms_scrub_credit_api_main'];
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'web'=>$scrub,'api'=>$scrubApi,'web_main'=>$scrubMain,'api_main'=>$scrubApiMain);
	  											
	  									}else{
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'web'=>$scrub,'api'=>$scrubApi);
	  									}
	  								}else{
	  									$this->view->error = 'ERR4220';
	  									$this->view->data = array('message' => 'Route Mapping Failed');
	  									$this->getResponse()->setHttpResponseCode(200);
	  								}
	  							}elseif (isset($scheduled)){
	  							
	  								$scheduleRepos = $emSms->getRepository('modules\sms\models\SmsCampaign')->getScheduledCampaigns($userId);
	  								
	  								if(isset($scheduleRepos) && $scheduleRepos){
	  									$i = 0;
	  									foreach ($scheduleRepos as $scheduleRepo){
		  									$i++;
		  									$reportData[] = array('id' => $i,'campid' => $scheduleRepo['id'] , 'type' => $scheduleRepo['type'], 'schedule' => $scheduleRepo['start_date'], 'credit' => $scheduleRepo['credit']);
		  										
	  									}
	  									$this->view->status = 'GET';
	  									$this->view->data = array('status' => "success",'total' => $i,  'campaign' =>$reportData);
	  											
	  								}else{
	  									$this->view->error = 'ERR2063';
	  									$this->view->data = array('message' => 'No Scheduled Campaigns');
	  									$this->getResponse()->setHttpResponseCode(200);
	  								}
	  								
	  								
	  							}elseif (isset($campId)){
	  								
	  								$campaignRepo = $emSms->getRepository('modules\sms\models\SmsCampaign')->findOneBy(array('user' => $userId, 'id' => $campId));
	  									
	  								if(isset($campaignRepo) && $campaignRepo){
	  									
	  									$lead = $campaignRepo->leadsFile;
	  									if (file_exists('/var/www/html/webpanel/contactFiles/'.$lead)) {
	  										if (($handle = fopen('/var/www/html/webpanel/contactFiles/'.$lead, "r")) !== FALSE) {
	  											$i=0;
	  											while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	  												if (isset($data['0'])){
	  													if (isset($data['1'])){
	  														$i++;
	  														$leadsData[] = array('id' => $i,'mobile' => $data['0'] , 'text' => $data['0']);
	  															
	  													}else{
	  														$i++;
	  														$leadsData[] = array('id' => $i,'mobile' => $data['0']);
	  															
	  													}
	  												}
	  											}
	  										}
	  										
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'campid' => $campaignRepo->id,  'leads' =>$leadsData);
	  									}else{
	  										$this->view->error = 'ERR2066';
	  										$this->view->data = array('message' => 'Contacts Not Found');
	  										$this->getResponse()->setHttpResponseCode(200);
	  									}
	  									
	  									
	  								}else{
	  									$this->view->error = 'ERR2064';
	  									$this->view->data = array('message' => 'Invalid Campaign');
	  									$this->getResponse()->setHttpResponseCode(200);
	  								}
	  							}else{
	  								$this->view->error = 'ERR2060';
	  								$this->view->data = array('message' => 'Partial Content Or Invalid fields');
	  								$this->getResponse()->setHttpResponseCode(200);
	  							}
	  								
	  						}
	  					}else{
	  						$this->view->error = 'ERR4510';
	  						$this->view->data = array('message' => 'Ip Not Accepted');
	  						$this->getResponse()->setHttpResponseCode(200);
	  					}
	  				}else{
		  				$this->view->error = 'ERR4010';
		  				$this->view->data = array('message' => 'Unauthorized Or Invalid Key');
		  				$this->getResponse()->setHttpResponseCode(200);
		  			}
	  			}
	  		
		    
    		
    }elseif (isset($apiMethod) && $apiMethod == 'DEL'){
  		$campId = $this->_request->getParam('campid');
  		
  		if (!isset($apiKey, $campId)){
  			$this->view->error = 'ERR2060';
  			$this->view->data = array('message' => 'Partial Content or Invalid fields');
  			$this->getResponse()->setHttpResponseCode(200);
  		}else {
  		
  			$utility = new Smpp_Utility();
  			$emUser = $utility->getEntityManager('user');
  			$emSms = $utility->getEntityManager('sms');
  			//$apiRepo = $em->getRepository('Models\Apikey')->getApiDetails($apiKey);
  			$apiRepo = $emUser->getRepository('modules\user\models\Apikey')->findOneBy(array('apiKey' => "$apiKey"));
  		
  			if ($apiRepo) {
  				//$userId = $apiRepo[0]['user_id'];
  				$userId = $apiRepo->user;
  				$foundIp = true;
  				//if ($apiRepo['0']['ip']){
  				if ($apiRepo->ip){
  					$apiIp = explode(",",$ips);
  					$ipAddress = $_SERVER['REMOTE_ADDR'];
  				
  					$keyIp = array_search($ipAddress, $apiIp, true);
  				
  					if ($keyIp == false){
  						$foundIp = false;
  					}
  				}
  					
  				if ($foundIp){
  					$userRepo = $emUser->getRepository('modules\user\models\User')->getUserStatus($userId);
  					//$usersRepo = $em->getRepository('Models\User')->find($userId);
  					if ($userRepo[0]['status']=='1'){
  						$campaignRepo = $emSms->getRepository('modules\sms\models\SmsCampaign')->findOneBy(array('id' =>$campId,'user'=>$userId, 'status' => '0'));
  						if($campaignRepo){
  							$type = $campaignRepo->type;
  							$campaignManager = new SmsCampaignManager();
  							$cancelCampaign = $campaignManager->cancellCampaign($campaignRepo->id, $emSms);
  							
  							if ($cancelCampaign){
  								$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
  								$activityManager = new ActivityLogManager();
  								$activityManager->createActivityLog($userRepo, $userId, 'API', 'SMS', 'DEL', 'Campaign Cancelled Successfully', $this->domain, $this->ip, $this->userAgent, $cancelCampaign->id , 'CANCEL', 'CAMPAIGN', $emUser);
  							
  								$accountManager = new AccountManager();
  								if ($cancelCampaign->type == 'TRANSACTIONAL'){
  									if ($cancelCampaign->apiKey == 'WEB-PUSH'){
  										$account = '6';
  									}else{
  										$account = '8';
  									}
  							
  								}elseif ($cancelCampaign->type == 'PROMOTIONAL'){
  							
  									if ($cancelCampaign->apiKey == 'WEB-PUSH'){
  										$account = '7';
  									}else{
  										$account = '9';
  									}
  								}elseif ($cancelCampaign->type == 'TRANS-SCRUB'){
  							
  									if ($cancelCampaign->apiKey == 'WEB-PUSH'){
  										$account = '12';
  									}else{
  										$account = '13';
  									}
  								}elseif ($cancelCampaign->type == 'INTERNATIONAL'){
  							
  									if ($cancelCampaign->apiKey == 'WEB-PUSH'){
  										$account = '26';
  									}else{
  										$account = '27';
  									}
  								}
  								$accountArray = $accountManager->reCredit($userId, $account, $cancelCampaign->credit, $emUser);
  								$credit = $accountArray['user'];
  								$resellerRepo = $accountArray['reseller'];
  								$logRepo = $accountArray['log'];
  								
  								if ($credit) {
  									$activityManager = new ActivityLogManager();
  									$activityManager->createActivityLog($resellerRepo, $credit->id, 'API', 'SMS', 'DEL', 'Recredit Transfered Successfully', $this->domain, $this->ip, $this->userAgent, $logRepo->id, 'RECREDIT', 'ACCOUNT', $emUser);
  								}
  							
	  							$this->view->status = 'DEL';
	  							$this->view->data = array('status' => "success",'campid' => $cancelCampaign->id , 'type' => $cancelCampaign->type, 'recredit' => $cancelCampaign->credit);
  							
  							}
  						
  						}else{
  							$this->view->error = 'ERR2064';
  							$this->view->data = array('message' => 'Invalid Campaign');
  							$this->getResponse()->setHttpResponseCode(200);
  						}
  					}
  					
  				}else{
  					$this->view->error = 'ERR4510';
  					$this->view->data = array('message' => 'Ip Not Accepted');
  					$this->getResponse()->setHttpResponseCode(200);
  				}
  			}else{
  				$this->view->error = 'ERR4010';
  				$this->view->data = array('message' => 'Unauthorized Or Invalid Key');
  				$this->getResponse()->setHttpResponseCode(200);
  			}
  		}
	    }else{
			    	$this->view->error = 'ERR4051';
					$this->view->data = array('message' => 'Api Method Not Allowed');
					$this->getResponse()->setHttpResponseCode(200);
		}
  	}else{
    	$this->view->error = 'ERR4052';
		$this->view->data = array('message' => 'HTTP Method Not Allowed');
		$this->getResponse()->setHttpResponseCode(200);
    }
  }

  public function listAction(){
    $this->_forward('index');
  }
  
  public function getAction(){
		$this->_forward('index');
  }

  public function newAction(){   	
		$this->_forward('index');
  }
	
  public function postAction(){
		$this->_forward('index');
  }

  public function editAction(){    	 
		$this->_forward('index');
  }
	
  public function putAction(){
		$this->_forward('index');
  } 
	
  public function deleteAction(){
		$this->_forward('index');
  }


}

