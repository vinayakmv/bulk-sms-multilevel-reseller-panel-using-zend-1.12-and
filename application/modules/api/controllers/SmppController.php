<?php


use modules\user\models\ActivityLogManager;
use modules\user\models\AccountManager;
class Api_SmppController extends Zend_Rest_Controller{  
	public function validateDate($date, $format = 'Y-m-d H:i')
	{
		$d = DateTime::createFromFormat($format, $date);
    	return $d && $d->format($format) == $date;
	}
	
  public function init(){
  	$this->_helper->layout->disableLayout();
  	$bootstrap = $this->getInvokeArg('bootstrap');
  	$this->domain =  Smpp_Utility::getFqdn();
  	$this->userAgent = $bootstrap->getResource('useragent');
  	$this->ip = $_SERVER['REMOTE_ADDR'];
  	
  	$options = $bootstrap->getOption('resources');
  	$contextSwitch = $this->_helper->getHelper('contextSwitch');
  	$contextSwitch->addActionContext('index', array('xml','json'))->initContext();
  }
	
  /**
   * The index action handles index/list requests; it should respond with a
   * list of the requested resources.
   */ 
  public function indexAction(){
	  if ($this->_request->isGet() || $this->_request->isPost()) {
	  	$campaignValues = array();
	  	$proceed = true;
	  	
	  	if ($this->_request->isGet()) {
	  		$apiType = 'GET';
	  	} else {
	  		$apiType = 'POST';
	  	}
	  	
		  	$apiKey = $this->_request->getHeader('X-Authentication-Key'); 
		  	$apiMethod = $this->_request->getHeader('X-Api-Method');
		  	
		  	if (!$apiKey){
		  		$apiKey = $this->_request->getParam('key');
		  	}
		  	
		  	if (!$apiMethod){
		  		$apiMethod = $this->_request->getParam('method');
		  	}
		  	
		  	if (isset($apiMethod) && $apiMethod == 'GET'){
	  			$route = $this->_request->getParam('route');
	  			$esme = $this->_request->getParam('esme');
	  			
	  			if (!isset($apiKey)){
	  				$this->view->error = 'ERR2060';
	  				$this->view->data = array('message' => 'Partial Content or Invalid fields');
	  				$this->getResponse()->setHttpResponseCode(200);
	  			}else {
	  				$utility = new Smpp_Utility();
	  				$emUser = $utility->getEntityManager('user');
	  				$emSmpp = $utility->getEntityManager('smpp');
	  				//$apiRepo = $em->getRepository('Models\Apikey')->getApiDetails($apiKey);
	  				$apiRepo = $emUser->getRepository('modules\user\models\Apikey')->findOneBy(array('apiKey' => "$apiKey"));
	  			
	  				if ($apiRepo) {
	  					//$userId = $apiRepo[0]['user_id'];
	  					$userId = $apiRepo->user;
	  					$foundIp = true;
	  					//if ($apiRepo['0']['ip']){
	  					if ($apiRepo->ip){
	  						$apiIp = explode(",",$ips);
	  						$ipAddress = $_SERVER['REMOTE_ADDR'];
	  			
	  						$keyIp = array_search($ipAddress, $apiIp, true);
	  			
	  						if ($keyIp == false){
	  							$foundIp = false;
	  						}
	  					}
	  						
	  					if ($foundIp){
	  						$userRepo = $emUser->getRepository('modules\user\models\User')->getUserStatus($userId);
	  						//$usersRepo = $em->getRepository('Models\User')->find($userId);
	  						if ($userRepo[0]['status']=='1'){

	  							if (isset($route,$esme)){
	  								if ($route == 'TL'){
	  									$accountRepo = $emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getBalance($userId, $route, $esme);
	  									
	  									if ($accountRepo){
	  										$credit = $accountRepo[0]['credit'];
	  										$systemId = $accountRepo[0]['system_id'];
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'system_id'=>$systemId,'credit'=>$credit);
	  										
	  									}else{
	  										$this->view->error = 'ERR4220';
	  										$this->view->data = array('message' => 'Esme Mapping Failed');
	  										$this->getResponse()->setHttpResponseCode(200);
	  									}
	  									
	  										
	  								}elseif ($route == 'PL'){
	  									$accountRepo = $emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getBalance($userId, $route, $esme);
	  									
	  									if ($accountRepo){
	  										$credit = $accountRepo[0]['credit'];
	  										$systemId = $accountRepo[0]['system_id'];
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'system_id'=>$systemId,'credit'=>$credit);
	  											
	  									}else{
	  										$this->view->error = 'ERR4220';
	  										$this->view->data = array('message' => 'Esme Mapping Failed');
	  										$this->getResponse()->setHttpResponseCode(200);
	  									}
	  								}elseif ($route == 'SL'){
	  									$accountRepo = $emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getBalance($userId, $route, $esme);
	  									
	  									if ($accountRepo){
	  										$credit = $accountRepo[0]['credit'];
	  										$systemId = $accountRepo[0]['system_id'];
	  										$this->view->status = 'GET';
	  										$this->view->data = array('status' => "success",'route' => $route,'system_id'=>$systemId,'credit'=>$credit);
	  											
	  									}else{
	  										$this->view->error = 'ERR4220';
	  										$this->view->data = array('message' => 'Esme Mapping Failed');
	  										$this->getResponse()->setHttpResponseCode(200);
	  									}
	  								}else{
	  									$this->view->error = 'ERR4220';
	  									$this->view->data = array('message' => 'Route Mapping Failed');
	  									$this->getResponse()->setHttpResponseCode(200);
	  								}
	  							}else{
	  								$this->view->error = 'ERR2060';
	  								$this->view->data = array('message' => 'Partial Content Or Invalid fields');
	  								$this->getResponse()->setHttpResponseCode(200);
	  							}
	  								
	  						}
	  					}else{
	  						$this->view->error = 'ERR4510';
	  						$this->view->data = array('message' => 'Ip Not Accepted');
	  						$this->getResponse()->setHttpResponseCode(200);
	  					}
	  				}else{
		  				$this->view->error = 'ERR4010';
		  				$this->view->data = array('message' => 'Unauthorized Or Invalid Key');
		  				$this->getResponse()->setHttpResponseCode(200);
		  			}
	  			}
	  		
		    
    		
    }elseif (isset($apiMethod) && $apiMethod == 'DEL'){
  		$campId = $this->_request->getParam('campid');
  		
  		if (!isset($apiKey, $campId)){
  			$this->view->error = 'ERR2060';
  			$this->view->data = array('message' => 'Partial Content or Invalid fields');
  			$this->getResponse()->setHttpResponseCode(200);
  		}else {
  		
  			$utility = new Smpp_Utility();
  			$emUser = $utility->getEntityManager('user');
  			$emSms = $utility->getEntityManager('sms');
  			//$apiRepo = $em->getRepository('Models\Apikey')->getApiDetails($apiKey);
  			$apiRepo = $emUser->getRepository('modules\user\models\Apikey')->findOneBy(array('apiKey' => "$apiKey"));
  		
  			if ($apiRepo) {
  				//$userId = $apiRepo[0]['user_id'];
  				$userId = $apiRepo->user;
  				$foundIp = true;
  				//if ($apiRepo['0']['ip']){
  				if ($apiRepo->ip){
  					$apiIp = explode(",",$ips);
  					$ipAddress = $_SERVER['REMOTE_ADDR'];
  				
  					$keyIp = array_search($ipAddress, $apiIp, true);
  				
  					if ($keyIp == false){
  						$foundIp = false;
  					}
  				}
  					
  				if ($foundIp){
  					$userRepo = $emUser->getRepository('modules\user\models\User')->getUserStatus($userId);
  					//$usersRepo = $em->getRepository('Models\User')->find($userId);
  					if ($userRepo[0]['status']=='1'){
  						$campaignRepo = $emSms->getRepository('modules\sms\models\SmsCampaign')->findOneBy(array('id' =>$campId,'user'=>$userId, 'status' => '0'));
  						if($campaignRepo){
  							$type = $campaignRepo->type;
  							$campaignManager = new SmsCampaignManager();
  							$cancelCampaign = $campaignManager->cancellCampaign($campaignRepo->id, $emSms);
  							
  							if ($cancelCampaign){
  								$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
  								$activityManager = new ActivityLogManager();
  								$activityManager->createActivityLog($userRepo, $userId, 'API', 'SMS', 'DEL', 'Campaign Cancelled Successfully', $this->domain, $this->ip, $this->userAgent, $cancelCampaign->id , 'CANCEL', 'CAMPAIGN', $emUser);
  							
  								$accountManager = new AccountManager();
  								if ($cancelCampaign->type == 'TRANSACTIONAL'){
  									if ($cancelCampaign->apiKey == 'WEB-PUSH'){
  										$account = '6';
  									}else{
  										$account = '8';
  									}
  							
  								}elseif ($cancelCampaign->type == 'PROMOTIONAL'){
  							
  									if ($cancelCampaign->apiKey == 'WEB-PUSH'){
  										$account = '7';
  									}else{
  										$account = '9';
  									}
  								}elseif ($cancelCampaign->type == 'TRANS-SCRUB'){
  							
  									if ($cancelCampaign->apiKey == 'WEB-PUSH'){
  										$account = '12';
  									}else{
  										$account = '13';
  									}
  								}elseif ($cancelCampaign->type == 'INTERNATIONAL'){
  							
  									if ($cancelCampaign->apiKey == 'WEB-PUSH'){
  										$account = '26';
  									}else{
  										$account = '27';
  									}
  								}
  								$accountArray = $accountManager->reCredit($userId, $account, $cancelCampaign->credit, $emUser);
  								$credit = $accountArray['user'];
  								$resellerRepo = $accountArray['reseller'];
  								$logRepo = $accountArray['log'];
  								
  								if ($credit) {
  									$activityManager = new ActivityLogManager();
  									$activityManager->createActivityLog($resellerRepo, $credit->id, 'API', 'SMS', 'DEL', 'Recredit Transfered Successfully', $this->domain, $this->ip, $this->userAgent, $logRepo->id, 'RECREDIT', 'ACCOUNT', $emUser);
  								}
  							
	  							$this->view->status = 'DEL';
	  							$this->view->data = array('status' => "success",'campid' => $cancelCampaign->id , 'type' => $cancelCampaign->type, 'recredit' => $cancelCampaign->credit);
  							
  							}
  						
  						}else{
  							$this->view->error = 'ERR2064';
  							$this->view->data = array('message' => 'Invalid Campaign');
  							$this->getResponse()->setHttpResponseCode(200);
  						}
  					}
  					
  				}else{
  					$this->view->error = 'ERR4510';
  					$this->view->data = array('message' => 'Ip Not Accepted');
  					$this->getResponse()->setHttpResponseCode(200);
  				}
  			}else{
  				$this->view->error = 'ERR4010';
  				$this->view->data = array('message' => 'Unauthorized Or Invalid Key');
  				$this->getResponse()->setHttpResponseCode(200);
  			}
  		}
	    }else{
			    	$this->view->error = 'ERR4051';
					$this->view->data = array('message' => 'Api Method Not Allowed');
					$this->getResponse()->setHttpResponseCode(200);
		}
  	}else{
    	$this->view->error = 'ERR4052';
		$this->view->data = array('message' => 'HTTP Method Not Allowed');
		$this->getResponse()->setHttpResponseCode(200);
    }
  }

  public function listAction(){
    $this->_forward('index');
  }
  
  public function getAction(){
		$this->_forward('index');
  }

  public function newAction(){   	
		$this->_forward('index');
  }
	
  public function postAction(){
		$this->_forward('index');
  }

  public function editAction(){    	 
		$this->_forward('index');
  }
	
  public function putAction(){
		$this->_forward('index');
  } 
	
  public function deleteAction(){
		$this->_forward('index');
  }


}

