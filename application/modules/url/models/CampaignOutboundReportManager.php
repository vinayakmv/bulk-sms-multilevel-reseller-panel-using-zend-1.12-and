<?php
namespace modules\url\models;

class CampaignOutboundReportManager extends \Smpp_Doctrine_BaseManager
{
	public function updateReport($formValues, $em)
	{
		if (isset($formValues)) {
                        $reportId = $formValues['id'];
			$reportRepo = $em->getRepository('modules\outbound\models\CampaignOutboundReport')->find($reportId);
			foreach($formValues as $key=>$value){
                            if ($key != 'id'){ 
                                $reportRepo->$key = $value;
                            } 
                        }
                        $reportRepo->status = '1';
			$reportRepo->updatedDate = new \DateTime('now');
			$em->persist($reportRepo);
			$em->flush();
				
			return $reportRepo;
			
		}
		return false;
	}
	
	public function updateProfile($profileValues, $domainId, $em)
	{
		if (isset($em)){
			$domain = $em->getRepository('modules\user\models\DomainProfile')->findOneBy(array('domain' => "$domainId"));
			$domain->company = $profileValues['intro'];
			$domain->about = $profileValues['about'];
			
			$em->persist($domain);
			$em->flush();
	
			return $domain;
		}
		return false;
	}
	
}
