<?php
namespace modules\url\models;

class CampaignFormManager extends \Smpp_Doctrine_BaseManager
{
	public function createForm($createdForm, $formName, $userId, $resellerId, $em)
	{
		if (isset($createdForm)) {
			$domain = new CampaignForm();
			$domain->user = $userId;
			$domain->reseller = $resellerId;
			$domain->name = $formName;
			$domain->form = urlencode($createdForm);
                        $domain->status = '1';
			$domain->createdDate = new \DateTime('now');
			$domain->updatedDate = new \DateTime('now');
			$em->persist($domain);
			$em->flush();
				
			return $domain;
			
		}
		return false;
	}
	
	public function updateProfile($profileValues, $domainId, $em)
	{
		if (isset($em)){
			$domain = $em->getRepository('modules\user\models\DomainProfile')->findOneBy(array('domain' => "$domainId"));
			$domain->company = $profileValues['intro'];
			$domain->about = $profileValues['about'];
			
			$em->persist($domain);
			$em->flush();
	
			return $domain;
		}
		return false;
	}
	
}
