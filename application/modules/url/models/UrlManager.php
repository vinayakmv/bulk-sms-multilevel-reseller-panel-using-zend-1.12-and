<?php
namespace modules\url\models;

class UrlManager extends \Smpp_Doctrine_BaseManager
{
	public function createUrl($urlValues, $userId, $emUrl)
	{
		if (isset($emUrl)){
			
			$url = new Url();
			$url->name = $urlValues['name'];
			$url->user = $userId;
			$url->type = $urlValues['urlType'];
			if ($urlValues['urlType'] == '1'){
				$url->url = $urlValues['url'];
			}elseif($urlValues['urlType'] == '2'){
				$url->url = $urlValues['uploadFile'];
			}elseif($urlValues['urlType'] == '3'){
                            $url->url = $urlValues['selectForm'];
                        }
			$url->status = '1';
			$url->createdDate = new \DateTime('now');
			$url->updatedDate = new \DateTime('now');
			$emUrl->persist($url);
			$emUrl->flush();
				
			return $url;
		}
		return false;
		
	}

	public function deleteUrl($getValues, $method, $userId, $emUrl)
	{
		if (isset($emUrl)){
			$id =  $getValues['id'];
			$url = $emUrl->getRepository('modules\url\models\Url')->findOneBy(array('id'=>$id, 'user'=> $userId));

			$emUrl->remove($url);
			$emUrl->flush();
				
			return $url;
		}
		return false;
	}
}