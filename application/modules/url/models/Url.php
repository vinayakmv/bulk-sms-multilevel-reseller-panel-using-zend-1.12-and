<?php
namespace modules\url\models;

/**
 * @Entity(repositoryClass="modules\url\models\UrlRepository")
 * @Table(name="url",indexes={@Index(name="search_idx", columns={"user_id"})})
 * @author Vinayak
 *
 */
class Url
{
		
	/**
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;

	/**
	 * @Column(name="user_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $user;

	/**
	 * @Column(name="name", type="string", nullable=false)
	 * @var integer
	 */
	private $name;

	/**
	 * @Column(name="type", type="integer")
	 * @var text
	 */
	private $type;

	/**
	 * @Column(name="url", type="text", nullable=true)
	 * @var unknown
	 */
	private $url;
	
	/**
	 * @Column(name="status", type="integer")
	 * @var text
	 */
	private $status;

	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}