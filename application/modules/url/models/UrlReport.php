<?php
namespace modules\url\models;

/**
 * @Entity(repositoryClass="modules\url\models\UrlReportRepository")
 * @Table(name="url_report",indexes={@Index(name="search_idx", columns={"tiny_url"})})
 * @author Vinayak
 *
 */
class UrlReport
{
		
	/**
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;

	/**
	 * @Column(name="user_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $user;

		/**
	 * @Column(name="campaign_name", type="string", nullable=true)
	 * @var integer
	 */
	private $campaignName;

	/**
	 * @Column(name="campaign_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $campaign;

	/**
	 * @Column(name="receiver", type="string", nullable=false)
	 * @var unknown
	 */
	private $receiver;

	/**
	 * @Column(name="type", type="string", nullable=true)
	 * @var unknown
	 */
	private $type;

	/**
	 * @Column(name="url", type="string", nullable=true)
	 * @var unknown
	 */
	private $url;

	/**
	 * @Column(name="tiny_url", type="string", nullable=true)
	 * @var unknown
	 */
	private $tinyUrl;

	/**
	 * @Column(name="data", type="object", nullable=true)
	 * @var unknown
	 */
	private $data;
	
	/**
	 * @Column(name="status", type="integer")
	 * @var text
	 */
	private $status;

	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}