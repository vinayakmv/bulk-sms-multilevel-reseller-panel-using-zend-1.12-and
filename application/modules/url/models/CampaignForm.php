<?php
namespace modules\url\models;

/**
 * @Entity(repositoryClass="modules\url\models\CampaignFormRepository")
 * @Table(name="campaign_form")
 * @author Vinayak
 *
 */
class CampaignForm 
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     	 * @Id
     	 * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="user_id", type="integer", nullable=true)
	 * @var string
	 */
	private $user;
	
	/**
	 * @Column(name="reseller_id", type="integer", nullable=true)
	 * @var string
	 */
	private $reseller;
	
        /**
	 * @Column(name="campaign_id", type="integer", nullable=true)
	 * @var string
	 */
	private $campaign;
	
        
	/**
	 * @Column(name="name", type="string", nullable=true)
	 * @var string
	 */
	private $name;

	
	/**
	 * @Column(name="form", type="text", nullable=true)
	 * @var string
	 */
	private $form;
        
        /**
	 * @Column(name="status", type="integer", nullable=true)
	 * @var string
	 */
	private $status;

	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}
