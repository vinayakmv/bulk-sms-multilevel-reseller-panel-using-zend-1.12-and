<?php
namespace modules\url\models;

/**
 * @Entity(repositoryClass="modules\url\models\CampaignOutboundReportRepository")
 * @Table(name="campaign_outbound_report")
 * @author Vinayak
 *
 */
class CampaignOutboundReport 
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     	 * @Id
     	 * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="user_id", type="integer", nullable=true)
	 * @var string
	 */
	private $user;
        
        /**
	 * @Column(name="reseller_id", type="integer", nullable=true)
	 * @var string
	 */
	private $reseller;
        
        /**
	 * @Column(name="campaign_id", type="integer", nullable=true)
	 * @var string
	 */
	private $campaign;

	/**
	 * @Column(name="agent_report", type="integer", nullable=true)
	 * @var string
	 */
	private $agentReport;
	
	/**
	 * @Column(name="pri_id", type="integer", nullable=true)
	 * @var string
	 */
	private $pri;
	
	/**
	 * @Column(name="agent_extension", type="integer", nullable=true)
	 * @var string
	 */
	private $agentExtension;

	
	/**
	 * @Column(name="form", type="text", nullable=true)
	 * @var string
	 */
	private $form;

	/**
	 * @Column(name="data", type="text", nullable=true)
	 * @var string
	 */
	private $data;

	
	/**
	 * @Column(name="client_number", type="bigint", nullable=true)
	 * @var string
	 */
	private $clientNumber;
	
	/**
	 * @Column(name="callid", type="string", nullable=true)
	 * @var string
	 */
	private $callid;

	
	/**
	 * @Column(name="disposition", type="string", nullable=true)
	 * @var string
	 */
	private $disposition;
	
	/**
	 * @Column(name="record_file", type="string", nullable=true)
	 * @var string
	 */
	private $recordFile;

	/**
	 * @Column(name="credit", type="integer", nullable=true)
	 * @var string
	 */
	private $credit;

	/**
	 * @Column(name="billsec", type="integer", nullable=true)
	 * @var string
	 */
	private $billsec;

	/**
	 * @Column(name="notes", type="text", nullable=true)
	 * @var string
	 */
	private $notes;

	/**
	 * @Column(name="status", type="integer", nullable=true)
	 * @var string
	 */
	private $status;

      /**
       * @Column(name="A", type="text", nullable=true)
       * @var string
       */
       private $A;

      /**
       * @Column(name="B", type="text", nullable=true)
       * @var string
       */
       private $B;

      /**
       * @Column(name="C", type="text", nullable=true)
       * @var string
       */
       private $C;

      /**
       * @Column(name="D", type="text", nullable=true)
       * @var string
       */
       private $D;

      /**
       * @Column(name="E", type="text", nullable=true)
       * @var string
       */
       private $E;

      /**
       * @Column(name="F", type="text", nullable=true)
       * @var string
       */
       private $F;

      /**
       * @Column(name="G", type="text", nullable=true)
       * @var string
       */
       private $G;

      /**
       * @Column(name="H", type="text", nullable=true)
       * @var string
       */
       private $H;

      /**
       * @Column(name="I", type="text", nullable=true)
       * @var string
       */
       private $I;

      /**
       * @Column(name="J", type="text", nullable=true)
       * @var string
       */
       private $J;

      /**
       * @Column(name="K", type="text", nullable=true)
       * @var string
       */
       private $K;

      /**
       * @Column(name="L", type="text", nullable=true)
       * @var string
       */
       private $L;

      /**
       * @Column(name="M", type="text", nullable=true)
       * @var string
       */
       private $M;

      /**
       * @Column(name="N", type="text", nullable=true)
       * @var string
       */
       private $N;

      /**
       * @Column(name="O", type="text", nullable=true)
       * @var string
       */
       private $O;

      /**
       * @Column(name="P", type="text", nullable=true)
       * @var string
       */
       private $P;

      /**
       * @Column(name="Q", type="text", nullable=true)
       * @var string
       */
       private $Q;

      /**
       * @Column(name="R", type="text", nullable=true)
       * @var string
       */
       private $R;

      /**
       * @Column(name="S", type="text", nullable=true)
       * @var string
       */
       private $S;

      /**
       * @Column(name="T", type="text", nullable=true)
       * @var string
       */
       private $T;

      /**
       * @Column(name="U", type="text", nullable=true)
       * @var string
       */
       private $U;

      /**
       * @Column(name="V", type="text", nullable=true)
       * @var string
       */
       private $V;

      /**
       * @Column(name="W", type="text", nullable=true)
       * @var string
       */
       private $W;

      /**
       * @Column(name="X", type="text", nullable=true)
       * @var string
       */
       private $X;

      /**
       * @Column(name="Y", type="text", nullable=true)
       * @var string
       */
       private $Y;

      /**
       * @Column(name="Z", type="text", nullable=true)
       * @var string
       */
       private $Z;

	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}
