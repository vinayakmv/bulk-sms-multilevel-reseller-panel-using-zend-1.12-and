<?php

class Url_Form_Url extends Zend_Form
{

    public function init()
    {
        $this->setAction('create');
        $this->setMethod('post');
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        $this->setAttrib('class', "form-horizontal row-border");
         
        $urlOptions = array("3" => "Form",
                            "2" => "Url",
                             "1" => "Attachment");
        $urltype = new Zend_Form_Element_Radio('urlType');
        $urltype->setMultiOptions($urlOptions)->setSeparator('')->setValue("3");
        $urltype->setDecorators(array('ViewHelper','Description'));
        $urltype->setLabel('Url Type');
        $urltype->setRequired(true);
        $this->addElement($urltype);

        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper','Description'));
        $name->setLabel('Name');
        $name->class = "form-control";
        $name->setRequired(true);
        $name->addFilter(new Zend_Filter_HtmlEntities());
        $name->addFilter(new Zend_Filter_StripTags());
        //$name->addFilter(new Zend_Filter_StringToUpper());
        $this->addElement($name);

        $selectSenderId = new Zend_Form_Element_Select('selectForm');
        $selectSenderId->setDecorators(array('ViewHelper','Description'));
        $selectSenderId->setLabel('Select Sender');
        $selectSenderId->id = "selectSenderId";
        $selectSenderId->class = "form-control select2 custom-select";
        $selectSenderId->setAttrib('style', 'width:100%');
        //$selectSenderId->setRequired(true);
        $selectSenderId->addFilter(new Zend_Filter_HtmlEntities());
        $selectSenderId->addFilter(new Zend_Filter_StripTags());
        $this->addElement($selectSenderId);
                
        //text area
        $url = new Zend_Form_Element_Textarea('url');
        
        $url->setRequired(false);
        $url->setDecorators(array('ViewHelper','Description'));
        $url   ->setAttrib('cols', '40')
                    ->setAttrib('required', 'required')
                    ->setAttrib('rows', '4');
        $url->class = "form-control";
        $url->addFilter(new Zend_Filter_HtmlEntities());
        $this->addElement($url);

        $file = new Zend_Form_Element_File('uploadFile', array('style'=>'display:none;', 'onchange'=>'$("#upload-file-primary").html($(this).val());'));
        $file->setDecorators(array('File'));
        $file->setLabel('Upload File');
        $file->setDestination('../urlFiles/');
        $file->class = "file-upload-default";
        $file->addValidator('Count', false, 1);
        $file->addValidator('Extension', false, 'csv,pdf');
        $file->setMaxFileSize(10485760);
        $file->setRequired(false);
        $this->addElement($file);

        $this->addElement('submit', 'submit');
        $submitElement = $this->getElement('submit');
        $submitElement->setAttrib('class',"btn  btn-primary ");
        $submitElement->setDecorators(array('ViewHelper','Description'));
        $submitElement->setLabel('Submit');
        $this->setDecorators(array('FormElements','Form'));
    }


}

