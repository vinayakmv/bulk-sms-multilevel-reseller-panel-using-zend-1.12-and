<?php
use modules\url\models\UrlManager;
use modules\url\models\CampaignFormManager;
class Url_ManageController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $broadcast = $this->view->navigation()->findOneByLabel('My Link');
        if ($broadcast) {
            $broadcast->setActive();
        }
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
    	parent::init();
    }

    public function createAction()
    {
        // action body
        $this->view->subject="CREATE URL";
        $page = $this->view->navigation()->findOneByUri('/url/manage/create');
        if ($page) {
            $page->setActive();
            $this->view->headTitle('My Link | Create');
        }
         
        $form = new Url_Form_Url();
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $emUser = $this->getEntityManager('user');
        $emUrl = $this->getEntityManager('url');
        
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
        
        $senderIdRepo = $emUrl->getRepository('modules\url\models\CampaignForm')->findBy(array('user'=>$userRepo->id,'status'=>'1'),array('default' => 'asc'));
        foreach ($senderIdRepo as $senderId) {
                $sender[$senderId->form]=$senderId->name;

        }    		
        //reset($sender);
        if (isset($sender)) {
                $form->getElement('selectForm')->setMultiOptions($sender);
        }	
         $this->view->form = $form;
        
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {

            @$originalFilename1 = pathinfo($form->uploadFile->getFileName());
            $newFilename1 = uniqid() . '.' . $originalFilename1['extension'];
            $form->uploadFile->addFilter('Rename', $newFilename1);
            $urlValues = $form->getValues();            

            
            $urlManager = new UrlManager();
            $createUrl = $urlManager->createUrl($urlValues, $userId, $emUrl);
            
            if ($createUrl){
                $this->view->success = array('Url' => array('isSuccess' => "Url updated"));
            }else{
                $this->view->errors = array('Url' => array('isFailed' => "Url failed"));
            }
            
        }else{
            $this->view->errors = $form->getMessages();
            
        }
    }

    public function viewAction()
    {
                // action body
        $this->view->subject="VIEW URL";
        $page = $this->view->navigation()->findOneByUri('/url/manage/view');
        if ($page) {
            $page->setActive();
            $this->view->headTitle('My Link | View');
        }

        $emUrl = $this->getEntityManager('url');
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        if ($this->getRequest()->isPost()) {
        
            $getValues = $this->getRequest()->getPost();
             
            $urlManager = new UrlManager();
            if($getValues['submit'] == 'Delete'){
                $manageUrl = $urlManager->deleteUrl($getValues, 'Delete', $userId, $emUrl);
                //$this->view->Success = array('approved' => array('success' => 'Approved successfully'));
                 
            }
        }
        $queryBuilder = $emUrl->createQueryBuilder();
        $queryBuilder->select('e')
                    ->from('modules\url\models\Url', 'e')
                    ->where('e.user = ?1')
                    ->orderBy('e.id', 'DESC')
                    ->setParameter(1, $userId);
        $query = $queryBuilder->getQuery();
        $this->view->entries = $query->getResult();
    }
    
    public function formAction()
    {
        $this->_helper->layout->disableLayout();
        $dynamicForm =  $this->_request->getParam('p');
        $formName =  trim($this->_request->getParam('name'));
        
        $emUrl = $this->getEntityManager('url');
        $emUser = $this->getEntityManager('user');
                
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
        $resellerId = $userRepo->reseller;
        
        $arrayColumn = array('DATA_A','DATA_B','DATA_C','DATA_D','DATA_E','DATA_F','DATA_G','DATA_H','DATA_I','DATA_J','DATA_K','DATA_L','DATA_M','DATA_N','DATA_O','DATA_P','DATA_Q','DATA_R','DATA_S','DATA_T','DATA_U','DATA_V','DATA_W','DATA_X','DATA_Y','DATA_Z');
        $replace = array('<?php echo $entry->A; ?>','<?php echo $entry->B; ?>','<?php echo $entry->C; ?>','<?php echo $entry->D; ?>','<?php echo $entry->E; ?>','<?php echo $entry->F; ?>','<?php echo $entry->G; ?>','<?php echo $entry->H; ?>','<?php echo $entry->I; ?>','<?php echo $entry->J; ?>','<?php echo $entry->K; ?>','<?php echo $entry->L; ?>','<?php echo $entry->M; ?>','<?php echo $entry->N; ?>','<?php echo $entry->O; ?>','<?php echo $entry->P; ?>','<?php echo $entry->Q; ?>','<?php echo $entry->R; ?>','<?php echo $entry->S; ?>','<?php echo $entry->T; ?>','<?php echo $entry->U; ?>','<?php echo $entry->V; ?>','<?php echo $entry->W; ?>','<?php echo $entry->X; ?>','<?php echo $entry->Y; ?>','<?php echo $entry->Z; ?>');
        
        $createdForm =  str_replace($arrayColumn, $replace, $dynamicForm);
        
        $formManager = new CampaignFormManager();
        if (!empty($formName)){
            $formCreate = $formManager->createForm($dynamicForm, $formName, $userId, $resellerId, $emUrl);
                    
        }
    }
    
    public function createformAction()
    {
        $page = $this->view->navigation()->findOneByUri('/url/manage/createform');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('My Link | Create Form');
    	}
    }
    
    public function viewformAction()
    {
        $this->view->subject="FORM LIST";
        $userManage = $this->view->navigation()->findOneByLabel('Url');
        if ($userManage) {
            $userManage->setActive();
        }
    	$page = $this->view->navigation()->findOneByUri('/url/manage/viewform');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Url | Campaign');
    	}
    	
        $emUser = $this->getEntityManager('user');
        $emUrl = $this->getEntityManager('url');
        $this->view->emUrl = $emUrl;
        $this->view->emUser = $emUser;
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
       
       
        $queryBuilder = $emUrl->createQueryBuilder();
    	$queryBuilder->select('e')
    	             ->from('modules\url\models\CampaignForm', 'e')
    	             ->where('e.user = ?1')
    	             ->orderBy('e.id', 'DESC')
    				 ->setParameter(1, $userId);
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    }
}





