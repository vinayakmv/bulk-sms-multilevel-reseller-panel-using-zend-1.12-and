<?php

use modules\user\models\UserManager;
use modules\user\models\DomainManager;
use modules\user\models\ActivityLogManager;
class User_ManageController extends Smpp_Controller_BaseController
{

    public function init()
    {
    	
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
		parent::init();
    }

    public function indexAction()
    {
    	
    }

    public function createAction()
    {

        $this->view->subject="CREATE RESELLER";
        $userManage = $this->view->navigation()->findOneByLabel('Users');
        if ($userManage) {
            $userManage->setActive();
        }
    	$page = $this->view->navigation()->findOneByUri('/user/manage/create');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Users | Create Reseller');
    	}
    	
        $form = new User_Form_Reseller();
        $this->view->form = $form;
        
        if($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())){
        	$resellerValues = $form->getValues();
        
        	//Create Reseller
        	$userId = Zend_Auth::getInstance()->getIdentity()->id;
        	
        	$emUser = $this->getEntityManager('user');
        	$emSms = $this->getEntityManager('sms');
        	$emVoice = $this->getEntityManager('voice');
        	
        	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);     
        	
        	$userManager = new UserManager();
        	$resellerCreated = $userManager->createReseller($resellerValues, 'RESELLER', $emUser, $emSms, $emVoice);
        	if (isset($resellerCreated)) {
        		$username = $resellerCreated->username;
        		$token = $resellerCreated->activate;
        		$domain = $resellerCreated->domain->domain;
        		//$send = $this->sendToken($token, $username, $domain);
        		$activityManager = new ActivityLogManager();
        		$activityManager->createActivityLog($userRepo, $resellerCreated->id, $this->_module, $this->_controller, $this->_action, 'Reseller Created Successfully', $this->_domain, $this->_ip, $this->_device, NULL, 'CREATE', 'USER', $emUser);
        		
        		$ip = $this->_request->getServer('REMOTE_ADDR');
        		
        		$fp = fopen("/etc/httpd/conf.d/$domain.conf", "w");
        		fwrite($fp,"<VirtualHost *:80>\n");
        		fwrite($fp,"ServerAdmin support@smschub.com\n");
        		fwrite($fp,"DocumentRoot /var/www/html/webpanel/public/\n");
        		fwrite($fp,"ServerAlias $domain\n");
        		fwrite($fp,"SetEnv APPLICATION_ENV \"production\"\n");
        		fwrite($fp,"<Directory \"/var/www/html/webpanel/public\">\n");
        		fwrite($fp,"Options Indexes MultiViews FollowSymLinks\n");
        		fwrite($fp,"AllowOverride All\n");
        		fwrite($fp,"Order allow,deny\n");
        		fwrite($fp,"Allow from all\n");
        		fwrite($fp,"</Directory>\n");
        		fwrite($fp,"ErrorLog /var/www/html/webpanel/logs/$domain-error_log\n");
        		fwrite($fp,"CustomLog /var/www/html/webpanel/logs/$domain-access_log common\n");
        		fwrite($fp,"</VirtualHost>");
        		fclose($fp);
        		
        		//shell_exec("/etc/init.d/httpd restart");
        		//if ($send){
        			$this->view->success = array('Mail' => array('isSend' => 'Reseller account created successfully'));
        		//}
        	}
        
        }else{
        	$this->view->errors = $form->getMessages();
        }
        $form->reset();
    }
    
    public function createuserAction()
    {
    
    	$this->view->subject="CREATE USER";
        $userManage = $this->view->navigation()->findOneByLabel('Users');
        if ($userManage) {
            $userManage->setActive();
        }
    	$page = $this->view->navigation()->findOneByLabel('Create User');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Users | Create User');
    	}
    	
    	$form = new Application_Form_User();
    	    	
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
        $emUser = $this->getEntityManager('user');
        $emSms = $this->getEntityManager('sms');
        $emVoice = $this->getEntityManager('voice');
        
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
        
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	 
    	$form->setAction('createuser');
        $this->view->form = $form;
        if($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())){
			$userValues = $form->getValues();
			        	
        	//Create User
        	$userManager = new UserManager();
        	$userCreated = $userManager->createUser($userValues, 'RESELLER', $emUser, $emSms, $emVoice);        
        	if (isset($userCreated)) {
        		$username = $userCreated->username;
        		$token = $userCreated->activate;
        		$domain = $userCreated->domain->domain;
        		$activityManager = new ActivityLogManager();
        		$activityManager->createActivityLog($userRepo, $userCreated->id, $this->_module, $this->_controller, $this->_action, 'User Created Successfully', $this->_domain, $this->_ip, $this->_device, NULL, 'CREATE', 'USER', $emUser);
        		
        		/*
        		$send = $this->sendToken($token, $username, $domain);
        		if ($send){
        			$this->view->success = array('mail' => array('isSend' => 'Activation username send successfully',
        														 'username' => "$username"));
        		}
        		*/
        		$this->view->success = array('mail' => array('isSend' => 'User account created successfully',
        				'username' => "$username"));
        		
        		//$this->_redirect('/account/signin');
        	}

        }else{
        	if (count($form->getErrors('token')) > 0) {
        		//return $this->_forward('csrf-forbidden', 'error');
        		//throw new Zend_Controller_Dispatcher_Exception('CSRF Forbidden');
        		$this->_redirect('/user/manage/createuser');
        	}
        	$this->view->errors = $form->getMessages();
        }
        $form->reset();
    }

    public function blockAction()
    {
    	
    }

    public function unblockAction()
    {
    	
    }

    public function usersAction()
    {

        $this->view->subject="USERS LIST";
        $userManage = $this->view->navigation()->findOneByLabel('Users');
        if ($userManage) {
            $userManage->setActive();
        }
    	$page = $this->view->navigation()->findOneByUri('/user/manage/users');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Users | Users');
    	}
    	
        $emUser = $this->getEntityManager('user');
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $roleId = Zend_Auth::getInstance()->getIdentity()->role_id;
        $roleRepo = $emUser->getRepository('modules\user\models\Role')->find($roleId);
        $this->view->role = $roleRepo->role;
        
        $queryBuilder = $emUser->createQueryBuilder();
    	$queryBuilder->select('e')
    	             ->from('modules\user\models\User', 'e')
    	             ->where('e.reseller = ?1')
    	             ->andwhere('e.id != ?1')
    	             ->orderBy('e.id', 'DESC')
    				 ->setParameter(1, $userId);
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	          ->setItemCountPerPage(20);
    	
    	$this->view->entries = $paginator;
		*/
    	// Block,Unblock,Activate Users,Resellers
    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
    		
    		if (isset($button['active'])) {
    			//Block User
    			$userManager = new UserManager();
    			$userManager->blockUser($button['active'], $userId, $emUser);
    		}
    		
    		if (isset($button['blocked'])) {
    			//Unblock User
    			$userManager = new UserManager();
    			$userManager->unblockUser($button['blocked'], $userId, $emUser);
    		}
    		
    		if (isset($button['notActivated'])) {
    			//Activate User
    			$userManager = new UserManager();
    			$userManager->ResellerActivateUser($button['notActivated'], $userId, $emUser);
    		}
	    	
    	}
    }

    public function domainsAction()
    {

        $this->view->subject="DOMAIN LIST";
        $userManage = $this->view->navigation()->findOneByLabel('Users');
        if ($userManage) {
            $userManage->setActive();
        }
    	$page = $this->view->navigation()->findOneByUri('/user/manage/domains');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Users | Domains');
    	}
    	
        $emUser = $this->getEntityManager('user');
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $this->view->userRepo = $emUser->getRepository('modules\user\models\User')->find($userId)->role;
        $queryBuilder = $emUser->createQueryBuilder();
    	$queryBuilder->select('e')
    	             ->from('modules\user\models\Domain', 'e')
    	             ->where('e.reseller = ?1')
    	             ->andwhere('e.owner != ?1')
    	             ->orderBy('e.id', 'DESC')
    				 ->setParameter(1, $userId);
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	          ->setItemCountPerPage(20);
    	
    	$this->view->entries = $paginator;
    	*/
    	$this->view->em = $emUser;
    	// Block,Unblock Reseller Domains
    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
    	
    		if (isset($button['active'])) {
    			//Block Domain
    			$domainManager = new DomainManager();
    			$domainManager->blockDomain($button['active'], $userId, $emUser);
    		}
    	
    		if (isset($button['blocked'])) {
    			//Unblock Domain
    			$domainManager = new DomainManager();
    			$domainManager->unblockDomain($button['blocked'], $userId, $emUser);
    		}    		

    		if (isset($button['delete'])) {
    			//Activate User
    			$userManager = new UserManager();
    			$userManager->deleteUser($button['delete'], $emUser);
    		}
    		
    	
    	}
    }
    
    public function clientsAction()
    {
        $this->view->subject="ALL USERS";
        $userManage = $this->view->navigation()->findOneByLabel('Users');
        if ($userManage) {
            $userManage->setActive();
        }
    	$page = $this->view->navigation()->findOneByLabel('All Users');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Users | All Users');
    	}
    	
    	$emUser = $this->getEntityManager('user');
    	$this->view->em = $emUser;
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $roleId = Zend_Auth::getInstance()->getIdentity()->role_id;
        $roleRepo = $emUser->getRepository('modules\user\models\Role')->find($roleId);
        $this->view->role = $roleRepo->role;
        
        $queryBuilder = $emUser->createQueryBuilder();
    	$queryBuilder->select('e')
    	             ->from('modules\user\models\User', 'e')
    	             ->orderBy('e.id', 'DESC');
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	          ->setItemCountPerPage(20);
    	
    	$this->view->entries = $paginator;
		*/
    	// Block,Unblock,Activate Users,Resellers
    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
    		
    		if (isset($button['active'])) {
    			//Block User
    			$userManager = new UserManager();
    			$userManager->blockallUser($button['active'], $emUser);
    		}
    		
    		if (isset($button['blocked'])) {
    			//Unblock User
    			$userManager = new UserManager();
    			$userManager->unblockallUser($button['blocked'], $emUser);
    		}
    		
    		if (isset($button['notActivated'])) {
    			//Activate User
    			$userManager = new UserManager();
    			$userManager->ResellerActivateallUser($button['notActivated'], $emUser);
    		}
    		
    		if (isset($button['delete'])) {
    			//Activate User
    			$userManager = new UserManager();
    			$userManager->deleteUser($button['delete'], $emUser);
    		}
	    	
    	}
    }
    
    public function resellersAction()
    {
        $this->view->subject="ALL DOMAINS";
        $userManage = $this->view->navigation()->findOneByLabel('Users');
        if ($userManage) {
            $userManage->setActive();
        }
    	$page = $this->view->navigation()->findOneByLabel('All Domains');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Users | All Domains');
    	}
    	
    	$emUser = $this->getEntityManager('user');
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $this->view->userRepo = $emUser->getRepository('modules\user\models\User')->find($userId)->role;
        $queryBuilder = $emUser->createQueryBuilder();
    	$queryBuilder->select('e')
    	             ->from('modules\user\models\Domain', 'e')
    	             ->orderBy('e.id', 'DESC');
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	          ->setItemCountPerPage(20);
    	
    	$this->view->entries = $paginator;
    	*/
    	$this->view->em = $emUser;
    	// Block,Unblock Reseller Domains
    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
    	
    		if (isset($button['active'])) {
    			//Block Domain
    			$domainManager = new DomainManager();
    			$domainManager->blockallDomain($button['active'], $emUser);
    		}
    	
    		if (isset($button['blocked'])) {
    			//Unblock Domain
    			$domainManager = new DomainManager();
    			$domainManager->unblockallDomain($button['blocked'], $emUser);
    		}
    	
    	}
    }
    
    public function startImpersonateAction()
    {
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    	$impersonateId = $this->_request->getParam('id');
    
    	$this->startImpersonateUser($impersonateId);
    	 
    }


    public function usageAction()
    {
    	$this->view->subject="SMS USAGE";
        $userManage = $this->view->navigation()->findOneByLabel('Sms');
        if ($userManage) {
            $userManage->setActive();
        }
        
        $page = $this->view->navigation()->findOneByLabel('User Usage');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Sms | User Usage');
    	}
    	
    	$emUser = $this->getEntityManager('user');
    	$emSms = $this->getEntityManager('sms');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	 
    	//$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('owner' => $userId));
    	if ($this->getRequest()->isPost()) {
    		$getValue=$this->getRequest()->getPost();
    		$sdate = date('m/d/Y');
    		if(isset($getValue['search'])){
    			$sdate = $getValue['from'];
    		}elseif(isset($getValue['update'])){
    			$sdate = $getValue['from'];
    			$startDate=strtotime($sdate.'00:00:00');
    			$endDate=strtotime($sdate.'23:59:59');
    			
    			$smsRepo = $emSms->getRepository('modules\sms\models\SmsUserUsage')->findOneBy(array('user' => "$userId", 'sentDate' => "$startDate"));
    			
    			if (@$smsRepo->id){
    				$createdDate = $smsRepo->createdDate;
    				$createTimestamp = date_create($createdDate->format('d-m-Y H:i:s'));
    				$currentDateTime = date_create(date('Y-m-d H:i:s'));
    				
    				$dateDiff=date_diff($currentDateTime, $createTimestamp);
    				$minutes = ($dateDiff->format('%i'));
    				$interval = 15-$minutes;
    				$day = $dateDiff->format('%d');
    				if (($day > '0') || ($minutes >= '15')){
    					shell_exec("/var/www/html/webpanel/cronjob/sms/cronJobSmsUsage.php $userId $startDate $endDate 1 &");
    					$this->view->success = "Updating";
    				}else{
    					$this->view->errors = "Please update after $interval minutes";
    				}
    			}else{
    				shell_exec("/var/www/html/webpanel/cronjob/sms/cronJobSmsUsage.php $userId $startDate $endDate 0 &");
    			}
    			
    			
    		}
    		
    	}else{
    		$sdate = date('m/d/Y');
    	}
    	$startDate=strtotime($sdate.'00:00:00');
    	$endDate=strtotime($sdate.'23:59:59');
    	
    	$smsRepo = $emSms->getRepository('modules\sms\models\SmsUserUsage')->findBy(array('user' => "$userId", 'sentDate' => "$startDate"));
    	$this->view->entries = $smsRepo;
    	$this->view->daterange = $sdate;
    /*
    	$arrayUsers = array();
    	if ($domainRepo->id){
    
    		$parentDomain = $domainRepo->id;
    
    		$usersRepo = $emUser->getRepository('modules\user\models\User')->findBy(array('parentDomain' => $parentDomain));
    
    		foreach ($usersRepo as $userRepo){
    				
    			$underUserId = $userRepo->id;
    			$underUserName = $userRepo->username;
    			$underUserDomain = $userRepo->domain->domain;
    				
    			$dlrUrl = $underUserId.'@%';
    				
    			$queryBuilder = $emSms->createQueryBuilder();
    			$queryBuilder->select('count(u.sqlId)')
    			->from('modules\sms\models\SentSmsTransMt', 'u')
    			->where('u.dlrUrl LIKE ?1')
    			->andWhere('u.time >= ?2')
    			->andWhere('u.time <= ?3')
    			->setParameter("1", $dlrUrl)
    			->setParameter("2", $startDate)
    			->setParameter("3", $endDate);
    			$query = $queryBuilder->getQuery();
    			$result = $query->getResult();
    
    			if ($result['0']['1']){
    				$arrayUsers["$underUserName"]["$underUserDomain"]["TRANS"] = $result;
    			}
    			
    			$queryBuilder = $emSms->createQueryBuilder();
    			$queryBuilder->select('count(u.sqlId)')
    			->from('modules\sms\models\SentSmsPromoMt', 'u')
    			->where('u.dlrUrl LIKE ?1')
    			->andWhere('u.time >= ?2')
    			->andWhere('u.time <= ?3')
    			->setParameter("1", $dlrUrl)
    			->setParameter("2", $startDate)
    			->setParameter("3", $endDate);
    			$query = $queryBuilder->getQuery();
    			$result2 = $query->getResult();
    			
    			if ($result2['0']['1']){
    				$arrayUsers["$underUserName"]["$underUserDomain"]["PROMO/SCRUB"] = $result2;
    			}
    		}
    
    
    	}
    */
    	
    }
    

}











