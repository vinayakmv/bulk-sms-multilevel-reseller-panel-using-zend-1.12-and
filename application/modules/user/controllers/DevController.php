<?php
use modules\user\models\ApikeyManager;
use modules\user\models\UserManager;
use modules\user\models\ActivityLogManager;
class User_DevController extends Smpp_Controller_BaseController
{
    public function init()
    {
        $broadcast = $this->view->navigation()->findOneByLabel('Api');
        if ($broadcast) {
          $broadcast->setActive();
        }
        $emUser = $this->getEntityManager('user');
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $defaultApiKey = $emUser->getRepository('modules\user\models\Apikey')->findOneBy(array('user' => $userId, 'status' => '1'));
        if (!isset($defaultApiKey)){
        	$domain = Smpp_Utility::getFqdn();
        	$url = '<a style="color:red" href='."http://$domain/user/dev/index".' target='."_blank".'>API-KEY</a>';
        	$this->view->defaultKey = "{$url}"; 
        }else{
        	$this->view->defaultKey = $defaultApiKey->apiKey;
        }
        $action = $this->getRequest()->getActionName();
        $emUser = $this->getEntityManager('user');
        $domain = Smpp_Utility::getFqdn();
        $domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
        $domainLayout = $domainRepo->layout;
        if ($domainLayout != null){
        	$view = $domainLayout.'-'.$action;
        	$this->_helper->viewRenderer("$view");
        }
        parent::init();
    }
    public function indexAction()
    {
         $this->view->subject="API KEY";
	    $page = $this->view->navigation()->findOneByUri('/user/dev/index');
	    if ($page) {
	      $page->setActive();
	      $this->view->headTitle('Api | Key');
	    }
		$form=new User_Form_Apikey();
	    $this->view->form=$form;
        $domain = Smpp_Utility::getFqdn();
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
        $this->view->userId = $userId;
        
		$emUser = $this->getEntityManager('user');
		$emSms = $this->getEntityManager('sms');
		$emVoice = $this->getEntityManager('voice');
		
        
		$apiManager = new ApikeyManager();
		if ($this->getRequest()->isPost()) {
            $apiValue1=$this->getRequest()->getPost();
            $apiValue = $form->getValues();
            $apiValue=array_merge($apiValue,$apiValue1);
            if(isset($apiValue['submitValue'])) $apiValue['submit']=$apiValue['submitValue'];
            if($apiValue['submit']=="addIP"){
 				$addIp = $apiManager->addIp($apiValue, $emUser);
                //var_dump($addIp);
                if($addIp){
                    $this->view->Success = array('API' => array('Message' => 'Add IPs Successfully'));
                }else {
                    $this->view->errors = array('API' => array('Message' => 'Please Enter Correct IP Address'));
                   
                }
            }elseif($apiValue['submit']=="generate"){
                
                $countApi = $emUser->getRepository('modules\user\models\Apikey')->countApi($userId);
                //print_r($countApi);
                /*echo $countApi['0']['count'];
                exit();*/

                if($countApi['0']['count']<='9'){
                    $api = $this->generateUuid();
                    $apiArray=array('api'=>$api);
                    $keyCreated = $apiManager->createApi($apiArray, $userId, $emSms, $emUser, $emVoice);

                    if ($keyCreated) {
                        $this->view->Success = array('API' => array('Message' => 'API Key Generated Successfully '));                        
                    }
                }else{
                    $this->view->errors = array('API' => array('Message' => 'Maximum Limit Reached'));
                }
                
            }elseif($apiValue['submit']=="Delete"){
                //print_r($apiValue);
                $apiId = $apiValue['apiId'];
                $keyDelete = $apiManager->deleteApi($apiId,$emUser);
                if ($keyDelete) { 
                    $this->view->Success = array('API' => array('Message' => 'Deleted Successfully'));    
                        
                }

            }elseif($apiValue['submit']=="Regenerate"){
                //print_r($apiValue);
                $apiId = $apiValue['apiId'];
                $api = $this->generateUuid();
                $keyEdit = $apiManager->editApi($apiId,$emUser,$userId,$api);
                
            }else{

            }
        }
        $apiRepo = $emUser->getRepository('modules\user\models\Apikey')->getAllApi($userId);
        $this->view->apiRepo=$apiRepo;
        
    }
	
    public function smsdocAction()
    {
    	$this->view->subject="API DOC";
    	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('API Management | Sms Documentation');
    	}
    	$this->view->head = 'API Introduction';
    	//$this->_helper->layout->disableLayout();
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
		$domain = Smpp_Utility::getFqdn();
		$this->view->domain=$domain;
	 }
	 
	 public function smssendAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Sms Documentation');
	 	}
	 	$this->view->head = 'Send SMS';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function smsunicodeAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Sms Documentation');
	 	}
	 	$this->view->head = 'Unicode SMS';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function smsscheduledAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Sms Documentation');
	 	}
	 	$this->view->head = 'Scheduled SMS';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function smsdynamicAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Sms Documentation');
	 	}
	 	$this->view->head = 'Dynamic SMS';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function smsdeliveryAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Sms Documentation');
	 	}
	 	$this->view->head = 'Delivery Receipts';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function allsmsdeliveryAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Sms Documentation');
	 	}
	 	$this->view->head = 'Delivery Receipts';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function smscreditAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Sms Documentation');
	 	}
	 	$this->view->head = 'Credit Check';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function smsgroupAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Sms Documentation');
	 	}
	 	$this->view->head = 'Group SMS';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function smscampaignsAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Sms Documentation');
	 	}
	 	$this->view->head = 'Get Scheduled Campaigns';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function smscancelAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Sms Documentation');
	 	}
	 	$this->view->head = 'Cancel Scheduled Campaigns';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function smsleadsAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Sms Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Sms Documentation');
	 	}
	 	$this->view->head = 'Get Campaign Leads';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function routeAction()
	 {
	 	$this->view->subject="API ROUTE ASSIGN";
	 	$page = $this->view->navigation()->findOneByUri('/user/dev/route');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('Api | Routes');
	 	}
	 	 
	 	$form = new User_Form_Route();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$emUser = $this->getEntityManager('user');
	 	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
	 	
	 	
	 	$smsRouteRepo = $userRepo->smsRoutes;
	 	//$routeRepo = $emUser->getRepository('Models\Route')->findBy(array('status'=>'1'));
	 	//print_r($routeRepo);
	 	
	 	foreach ($smsRouteRepo as $routes) {
	 		$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
	 	}
	 	$apiTrans = $userRepo->transApiRoute;
	 	$apiPromo = $userRepo->promoApiRoute;
	 	$apiScrub = $userRepo->scrubApiRoute;
	 	$apiInter = $userRepo->interApiRoute;
	 	
	 	if ($apiTrans){
	 		$routesApiTrans = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$apiTrans,'status'=>'1'));
	 		$this->view->transType = $routesApiTrans->type;
	 		$this->view->transRoute = $routesApiTrans->route;
	 		$this->view->transName = $routesApiTrans->name;
	 	}
	 	if ($apiPromo){
	 		$routesApiPromo = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$apiPromo,'status'=>'1'));
	 		$this->view->promoType = $routesApiPromo->type;
	 		$this->view->promoRoute = $routesApiPromo->route;
	 		$this->view->promoName = $routesApiPromo->name;
	 	}
	 	if ($apiScrub){
	 		$routesApiScrub = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$apiScrub,'status'=>'1'));
	 		$this->view->scrubType = $routesApiScrub->type;
	 		$this->view->scrubRoute = $routesApiScrub->route;
	 		$this->view->scrubName = $routesApiScrub->name;
	 	}
	 	if ($apiInter){
	 		$routesApiInter = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$apiInter,'status'=>'1'));
	 		$this->view->interType = $routesApiInter->type;
	 		$this->view->interRoute = $routesApiInter->route;
	 		$this->view->interName = $routesApiInter->name;
	 	}
	 	if (empty($route)){
	 		$trans = $userRepo->domain->transRoute;
	 		$promo = $userRepo->domain->promoRoute;
	 		$scrub = $userRepo->domain->scrubRoute;
	 		$inter = $userRepo->domain->interRoute;
	 		
	 		if ($trans){
	 			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$trans,'status'=>'1'));
	 			$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
	 		}
	 		if ($promo){
	 			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$promo,'status'=>'1'));
	 			$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
	 		}
	 		if ($scrub){
	 			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$scrub,'status'=>'1'));
	 			$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
	 		}
	 		if ($inter){
	 			$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$inter,'status'=>'1'));
	 			$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
	 		}
	 	}
	 	if (isset($route)) {
	 		$form->getElement('smsType')->setMultiOptions($route);
	 	}
	 	
	 	$this->view->form = $form;
	 	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
	 		
	 		$apiValues = $form->getValues();
	 		$userManager = new UserManager();
	 		$routeAssigned = $userManager->setApi($apiValues, $userId, $emUser);
	 		if (isset($routeAssigned['Api']['isSuccess'])){
	 			$routeId = $routeAssigned['Api']['data'];
	 			$apiRoute = $routeAssigned['Api']['route'];
	 			$activityManager = new ActivityLogManager();
	 			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, "SMS API $apiRoute Route Assigned Successfully", $this->_domain, $this->_ip, $this->_device, $routeId, 'ASSIGNED', 'SMS-API', $emUser);
	 			 
	 			$this->view->success = array('Api' => array('isSuccess' => "$apiRoute Route assigned successfully"));
	 			
	 		}elseif (isset($routeAssigned['Api']['isFailed'])) {
	 			$this->view->errors = array('Api' => array('isFailed' => $routeAssigned['Api']['isFailed']));
	 		}else{
	 			$this->view->errors = array('Api' => array('isFailed' => 'Route assigned failed'));
	 		}
	 	}
	 }
	 
	 public function voicedocAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Voice Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Voice Documentation');
	 	}
	 	$this->view->head = 'API Introduction';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function voicesendAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Voice Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Voice Documentation');
	 	}
	 	$this->view->head = 'Send Voice';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function voicetextAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Voice Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Voice Documentation');
	 	}
	 	$this->view->head = 'Send Text2Speech';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function voicescheduledAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Voice Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Voice Documentation');
	 	}
	 	$this->view->head = 'Scheduled Voice';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function voicedynamicAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Voice Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Voice Documentation');
	 	}
	 	$this->view->head = 'Dynamic Voice';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function voicedeliveryAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Voice Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Voice Documentation');
	 	}
	 	$this->view->head = 'Delivery Receipts';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function voicecreditAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Voice Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Voice Documentation');
	 	}
	 	$this->view->head = 'Credit Check';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function voicegroupAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Voice Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Voice Documentation');
	 	}
	 	$this->view->head = 'Group Voice';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function voicecampaignsAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Voice Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Voice Documentation');
	 	}
	 	$this->view->head = 'Get Scheduled Campaigns';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function voicecancelAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Voice Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Voice Documentation');
	 	}
	 	$this->view->head = 'Cancel Scheduled Campaigns';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }
	 
	 public function voiceleadsAction()
	 {
	 	$this->view->subject="API DOC";
	 	$page = $this->view->navigation()->findOneByLabel('Voice Documentation');
	 	if ($page) {
	 		$page->setActive();
	 		$this->view->headTitle('API Management | Voice Documentation');
	 	}
	 	$this->view->head = 'Get Campaign Leads';
	 	//$this->_helper->layout->disableLayout();
	 	$userId = Zend_Auth::getInstance()->getIdentity()->id;
	 	$domain = Smpp_Utility::getFqdn();
	 	$this->view->domain=$domain;
	 }


}

