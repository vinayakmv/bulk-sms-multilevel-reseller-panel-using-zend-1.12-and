<?php

use modules\user\models\AccountManager;
use modules\user\models\Account;
use modules\user\models\ActivityLogManager;
class User_TransactionController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $creditManage = $this->view->navigation()->findOneByLabel('Transactions');
		if ($creditManage) {
		  $creditManage->setActive();
		}
		$action = $this->getRequest()->getActionName();
		$emUser = $this->getEntityManager('user');
		$domain = Smpp_Utility::getFqdn();
		$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
		$domainLayout = $domainRepo->layout;
		if ($domainLayout != null){
			$view = $domainLayout.'-'.$action;
			$this->_helper->viewRenderer("$view");
		}
		parent::init();
    }

    public function indexAction()
    {

        $this->view->subject="CREDIT TRANSFER";
    	$page = $this->view->navigation()->findOneByLabel('User Transfer');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Transactions | User Transfer');
    	}
    	$emUser = $this->getEntityManager('user');
    	$form = new User_Form_Transaction();
    	$roleId = Zend_Auth::getInstance()->getIdentity()->role_id;
    	$roleRepo = $emUser->getRepository('modules\user\models\Role')->find($roleId);
    	$resources = $roleRepo->resources;
    	foreach ($resources as $resource){
    		$res[$resource->id] = $resource->resource;
    		$name = $resource->name;
    	
    	}
    	$esmeCreate = array_search('smpp/esme/create', $res);
    	$smsCompose = array_search('sms/campaign/index', $res);
    	$voiceCompose = array_search('voice/campaign/index', $res);
    	//$types = array();
    	if ($esmeCreate){
    		$types[2] = 'SMPP TRANSACTIONAL';
    		$types[1] = 'SMPP PROMOTIONAL';
    		$types[3] = 'SMPP TRANS-SCRUB';
    		$types[28] = 'SMPP INTERNATIONAL';
    	}
    	if ($smsCompose){
    		$types[6] = 'SMS TRANSACTIONAL';
    		//$types[29] = 'SMS PREMIUM';
    		$types[7] = 'SMS PROMOTIONAL';
    		$types[12] = 'SMS TRANS-SCRUB';
    		$types[26] = 'SMS INTERNATIONAL';
    		$types[8] = 'SMS API TRANSACTIONAL';
    		//$types[30] = 'SMS API PREMIUM';
    		$types[9] = 'SMS API PROMOTIONAL';
    		$types[13] = 'SMS API TRANS-SCRUB';
    		$types[27] = 'SMS API INTERNATIONAL';
    	}
    	if ($voiceCompose){
    		$types[4] = 'VOICE PROMOTIONAL';
    		$types[5] = 'VOICE TRANSACTIONAL';
    		//$types[31] = 'VOICE PREMIUM';
    		$types[10] = 'VOICE API PROMOTIONAL';
    		$types[11] = 'VOICE API TRANSACTIONAL';
    		//$types[32] = 'VOICE API PREMIUM';
    	}
    	$form->getElement('account')->setMultiOptions($types)->setRequired(true);
    	$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
    	$queryBuilder = $emUser->createQueryBuilder()
    	->select('u.username, d.domain')
    	->from('modules\user\models\User', 'u')
    	->innerJoin('u.domain', 'd')
    	->where('u.reseller = ?2 AND u.id != ?2')
    	->setParameter("2", "$resellerId");
    	
    	$underUserRepo = $queryBuilder->getQuery()->getResult();
    	$sender = array();
    	foreach ($underUserRepo as $underUser) {
    		$username = $underUser['username'].' | '.$underUser['domain'];
    		$sender[$username]=$username;
    	}
    	 
    	if (isset($sender)) {
    		$form->getElement('username')->setMultiOptions($sender)->setRequired(true);
    	
    	}   	    	
        $this->view->form = $form;
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        	$transactionValues = $form->getValues();
        	$mode = $transactionValues['mode'];
        	$account = $transactionValues['account'];
        	$accountManager = new AccountManager();
        	$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
        	
        if ($mode == '1') {
        		//Credit
        		$accountArray = $accountManager->setCredit($transactionValues, $resellerId, $account, $emUser);
        		$credit = $accountArray['user'];
        		$resellerRepo = $accountArray['reseller'];
        		$logRepo = $accountArray['log'];
        		if ($credit) {
        			$activityManager = new ActivityLogManager();
        			$activityManager->createActivityLog($resellerRepo, $credit->id, $this->_module, $this->_controller, $this->_action, 'Credit Transfered Successfully', $this->_domain, $this->_ip, $this->_device, $logRepo->id, 'ADD', 'CREDIT', $emUser);
        			
        			switch ($account) {
	        				case 1:
	        					//PROMOTIONAL
	        					$balance = $credit->account->smppPromoCredit;
	        					$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
	        					break;
	        				case 2:
	        					//TRANSACTIONAL
	        					$balance = $credit->account->smppTransCredit;
	        					$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
	        					break;
	        				case 3:
	        					//TRANS-SCRUB
	        					$balance = $credit->account->smppScrubCredit;
	        					$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
	        					break;
        					case 4:
        						//VOICE
        						$balance = $credit->account->voicePromoCredit;
        						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        						break;
        					case 5:
        						//VOICE
        						$balance = $credit->account->voiceTransCredit;
        						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        						break;
        					case 6:
        						//SMS
        						$balance = $credit->account->smsTransCredit;
        						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        						break;
        					case 7:
        						//SMS
        						$balance = $credit->account->smsPromoCredit;
        						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        						break; 
        						case 8:
        							//SMS API
        							$balance = $credit->account->smsTransApiCredit;
        							$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        							break;
        						case 9:
        							//SMS
        							$balance = $credit->account->smsPromoApiCredit;
        							$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        							break;
        						case 10:
        							//VOICE
        							$balance = $credit->account->voicePromoApiCredit;
        							$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        							break;
        						case 11:
        							//VOICE
        							$balance = $credit->account->voiceTransApiCredit;
        							$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        							break;
        							case 12:
        								//SCRUB
        								$balance = $credit->account->smsScrubCredit;
        								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        								break;
        							case 13:
        								//SCRUB API
        								$balance = $credit->account->smsScrubApiCredit;
        								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        								break;
        								
        								case 29:
        									//PREMI
        									$balance = $credit->account->smsPremiCredit;
        									$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        									break;
        								case 30:
        									//PREMI API
        									$balance = $credit->account->smsPremiApiCredit;
        									$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        									break;
        									
        								case 31:
        									//PREMI VOICE
        									$balance = $credit->account->voicePremiCredit;
        									$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        									break;
        								case 32:
        									//PREMI VOICE API
        									$balance = $credit->account->voicePremiApiCredit;
        									$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        									break;
        								
        							case 26:
        								//INTER
        								$balance = $credit->account->smsInterCredit;
        								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        								break;
        							case 27:
        								//INTER API
        								$balance = $credit->account->smsInterApiCredit;
        								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        								break;
        							case 28:
        									//INTER SMPP
        									$balance = $credit->account->smppInterCredit;
        									$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        									break;
        			}
        			
        		}else{
        			$this->view->errors = array('Credit' => array('isFailed' => 'Transaction failed'));
        		}
        	}elseif ($mode == '2') {
        		//Debit
        		$accountArray = $accountManager->setDebit($transactionValues, $resellerId, $account, $emUser);
        		$debit = $accountArray['user'];
        		$resellerRepo = $accountArray['reseller'];
        		$logRepo = $accountArray['log'];
        		if ($debit) {
        			$activityManager = new ActivityLogManager();
        			$activityManager->createActivityLog($resellerRepo, $debit->id, $this->_module, $this->_controller, $this->_action, 'Credit Transfered Successfully', $this->_domain, $this->_ip, $this->_device, $logRepo->id, 'DEDUCT', 'CREDIT', $emUser);
        			
        			switch ($account) {
	        				case 1:
	        					//PROMOTIONAL
	        					$balance = $debit->account->smppPromoCredit;
	        					$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
	        					break;
	        				case 2:
	        					//TRANSACTIONAL
	        					$balance = $debit->account->smppTransCredit;
	        					$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
	        					break;
	        				case 3:
	        					//TRANS-SCRUB
	        					$balance = $debit->account->smppScrubCredit;
	        					$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
	        					break; 
        					case 4:
        						//VOICE
        						$balance = $debit->account->voicePromoCredit;
        						$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        						break;
        					case 5:
        						//VOICE
        						$balance = $debit->account->voiceTransCredit;
        						$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        						break;
        					case 6:
        						//SMS
        						$balance = $debit->account->smsTransCredit;
        						$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        						break;
        					case 7:
        						//SMS
        						$balance = $debit->account->smsPromoCredit;
        						$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        						break;
        					case 8:
        							//SMS API
        							$balance = $debit->account->smsTransApiCredit;
        							$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        							break;
        					case 9:
        							//SMS
        							$balance = $debit->account->smsPromoApiCredit;
        							$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        							break;
        					case 10:
        								//VOICE
        								$balance = $debit->account->voicePromoApiCredit;
        								$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        								break;
        					case 11:
        								//VOICE
        								$balance = $debit->account->voiceTransApiCredit;
        								$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        								break;
        					case 12:
        							   //SCRUB
        							   $balance = $debit->account->smsScrubCredit;
        							   $this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        							   break;
        					case 13:
        								//SCRUB API
        								$balance = $debit->account->smsScrubApiCredit;
        								$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        								break;
        								case 29:
        									//PREMI
        									$balance = $debit->account->smsPremiCredit;
        									$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        									break;
        								case 30:
        									//PREMI API
        									$balance = $debit->account->smsPremiApiCredit;
        									$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        									break;
        										
        								case 31:
        									//PREMI VOICE
        									$balance = $debit->account->voicePremiCredit;
        									$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        									break;
        								case 32:
        									//PREMI VOICE API
        									$balance = $debit->account->voicePremiApiCredit;
        									$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        									break;
        					case 26:
        							//INTER
        							$balance = $debit->account->smsInterCredit;
        							$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        							break;
        					case 27:
        							//INTER API
        							$balance = $debit->account->smsInterApiCredit;
        							$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        							break;
        					case 28:
        							//INTER SMPP
        							$balance = $debit->account->smppInterCredit;
        							$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        							break;
       				
        			}
        				
        		}else{
        			$this->view->errors = array('Debit' => array('isFailed' => 'Transaction failed'));
        		}
        	}        	
        }else{
        	if (count($form->getErrors('token')) > 0) {
        		$this->_redirect('/user/transaction/index');
        	}
       		$this->view->errors = $form->getMessages();
        }
        $form->reset();
    }
    
    public function mainAction()
    {
    
    	$this->view->subject="MAIN TRANSFER";
    	$page = $this->view->navigation()->findOneByLabel('Main Transfer');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Transactions | Main Transfer');
    	}
    	$emUser = $this->getEntityManager('user');
    	$form = new User_Form_MainTransaction();
    	$roleId = Zend_Auth::getInstance()->getIdentity()->role_id;
    	$roleRepo = $emUser->getRepository('modules\user\models\Role')->find($roleId);
    	$resources = $roleRepo->resources;
    	foreach ($resources as $resource){
    		$res[$resource->id] = $resource->resource;
    		$name = $resource->name;
    		 
    	}
    	$smsCompose = array_search('sms/campaign/index', $res);
    	$voiceCompose = array_search('voice/campaign/index', $res);
    	//$types = array();
    	if ($smsCompose){
    		$types[16] = 'SMS TRANSACTIONAL';
    		//$types[33] = 'SMS PREMIUM';
    		$types[17] = 'SMS PROMOTIONAL';
    		$types[22] = 'SMS TRANS-SCRUB';
    		$types[24] = 'SMS INTERNATIONAL';
    		$types[18] = 'SMS API TRANSACTIONAL';
    		//$types[34] = 'SMS API PREMIUM';
    		$types[19] = 'SMS API PROMOTIONAL';
    		$types[23] = 'SMS API TRANS-SCRUB';
    		$types[25] = 'SMS API INTERNATIONAL';
    	}
    	if ($voiceCompose){
    		$types[15] = 'VOICE TRANSACTIONAL';
    		//$types[35] = 'VOICE PREMIUM';
    		$types[14] = 'VOICE PROMOTIONAL';
    		$types[20] = 'VOICE API TRANSACTIONAL';
    		//$types[36] = 'VOICE API PREMIUM';
    		$types[21] = 'VOICE API PROMOTIONAL';
    	}
    	$form->getElement('account')->setMultiOptions($types)->setRequired(true);
    	
    	$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
    	
    	$roleLRepo = $emUser->getRepository("modules\user\models\Role")->findOneBy(array('role' => 'L1-RESELLER'));
    	
    	$rolePRepo = $emUser->getRepository("modules\user\models\Role")->findOneBy(array('role' => 'P1-RESELLER'));
    	
    	$roleARepo = $emUser->getRepository("modules\user\models\Role")->findOneBy(array('role' => 'L1-ADMIN'));
    	    	
    	if (isset($rolePRepo,$roleLRepo,$roleARepo)){
    		$rolePId = $rolePRepo->id;
    		$roleLId = $roleLRepo->id;
    		$roleAId = $roleARepo->id;
    		
    		$queryBuilder = $emUser->createQueryBuilder()
    		->select('u.username, d.domain')
    		->from('modules\user\models\User', 'u')
    		->innerJoin('u.domain', 'd')
    		->where('u.reseller = ?2 AND u.id != ?2 AND (u.role = ?3 OR u.role = ?4 OR u.role = ?5)')
    		->setParameter("2", "$resellerId")
    		->setParameter("3", "$roleLId")
    		->setParameter("4", "$rolePId")
    		->setParameter("5", "$roleAId");
    	}
    	
    	$underUserRepo = $queryBuilder->getQuery()->getResult();
    	
    	$sender = array();
    	foreach ($underUserRepo as $underUser) {
    		$username = $underUser['username'].' | '.$underUser['domain'];
    			$sender[$username]=$username;
    	}
    	
    	if (isset($sender)) {
    		$form->getElement('username')->setMultiOptions($sender)->setRequired(true);
    		
    	}
    	
    	$this->view->form = $form;
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    		$transactionValues = $form->getValues();
    		$mode = $transactionValues['mode'];
    		$account = $transactionValues['account'];
    		$accountManager = new AccountManager();
    		$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
    		
    		if ($mode == '1') {
    			//Credit
    			$accountArray = $accountManager->setMainCredit($transactionValues, $resellerId, $account, $emUser);
    			$credit = $accountArray['user'];
    			$resellerRepo = $accountArray['reseller'];
    			$logRepo = $accountArray['log'];
    			if ($credit) {
    				$activityManager = new ActivityLogManager();
    				$activityManager->createActivityLog($resellerRepo, $credit->id, $this->_module, $this->_controller, $this->_action, 'Credit Transfered Successfully', $this->_domain, $this->_ip, $this->_device, $logRepo->id, 'ADD', 'MAIN CREDIT', $emUser);
    				 
    				switch ($account) {
    					case 14:
    						//VOICE
    						$balance = $credit->domain->voicePromoCredit;
    						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    					case 15:
    						//VOICE
    						$balance = $credit->domain->voiceTransCredit;
    						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    					case 16:
    						//SMS
    						$balance = $credit->domain->smsTransCredit;
    						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    					case 17:
    						//SMS
    						$balance = $credit->domain->smsPromoCredit;
    						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    						case 18:
    							//SMS API
    							$balance = $credit->domain->smsTransApiCredit;
    							$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    							break;
    						case 19:
    							//SMS API
    							$balance = $credit->domain->smsPromoApiCredit;
    							$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    							break;
    							case 20:
    								//SMS API
    								$balance = $credit->domain->voiceTransApiCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    							case 21:
    								//SMS API
    								$balance = $credit->domain->voicePromoApiCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    							case 22:
    								//SCRUB 
    								$balance = $credit->domain->smsScrubCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    							case 23:
    								//SCRUB API
    								$balance = $credit->domain->smsScrubApiCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    							case 24:
    								//INT
    								$balance = $credit->domain->smsInterCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    							case 25:
    								//INT API
    								$balance = $credit->domain->smsInterApiCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    							case 33:
    								//PREMI
    								$balance = $credit->domain->smsPremiCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    							case 34:
    								//PREMI API
    								$balance = $credit->domain->smsPremiApiCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    							case 35:
    								//PREMI VOICE
    								$balance = $credit->domain->voicePremiCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    							case 36:
    								//PREMI VOICE API
    								$balance = $credit->domain->voicePremiApiCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    				}
    				 
    			}else{
    				$this->view->errors = array('Credit' => array('isFailed' => 'Transaction failed'));
    			}
    		}elseif ($mode == '2') {
    			//Debit
    			$accountArray = $accountManager->setMainDebit($transactionValues, $resellerId, $account, $emUser);
    			$debit = $accountArray['user'];
    			$resellerRepo = $accountArray['reseller'];
    			$logRepo = $accountArray['log'];
    			if ($debit) {
    				$activityManager = new ActivityLogManager();
    				$activityManager->createActivityLog($resellerRepo, $debit->id, $this->_module, $this->_controller, $this->_action, 'Credit Transfered Successfully', $this->_domain, $this->_ip, $this->_device, $logRepo->id, 'DEDUCT', 'MAIN CREDIT', $emUser);
    					
    				switch ($account) {
    					case 14:
    						//VOICE
    						$balance = $debit->domain->voicePromoCredit;
    						$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    					case 15:
    						//VOICE
    						$balance = $debit->domain->voiceTransCredit;
    						$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    					case 16:
    						//SMS
    						$balance = $debit->domain->smsTransCredit;
    						$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    					case 17:
    						//SMS
    						$balance = $debit->domain->smsPromoCredit;
    						$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    					case 18:
    							//SMS
    							$balance = $debit->domain->smsTransApiCredit;
    							$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    							break;
    					case 19:
    							//SMS
    							$balance = $debit->domain->smsPromoApiCredit;
    							$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    							break;
    					case 20:
    								//SMS
    								$balance = $debit->domain->voiceTransApiCredit;
    								$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    					case 21:
    								//SMS
    								$balance = $debit->domain->voicePromoApiCredit;
    								$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    					case 22:
    								//SMS
    								$balance = $debit->domain->smsScrubCredit;
    								$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    					case 23:
    								//SMS
    								$balance = $debit->domain->smsScrubApiCredit;
    								$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    					case 24:
    								//INT
    								$balance = $debit->domain->smsInterCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break;
    					case 25:
    								//INT API
    								$balance = $debit->domain->smsInterApiCredit;
    								$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    								break; 
    					case 33:
    						//PREMI
    						$balance = $debit->domain->smsPremiCredit;
    						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    					case 34:
    						//PREMI API
    						$balance = $debit->domain->smsPremiApiCredit;
    						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    					case 35:
    						//PREMI VOICE
    						$balance = $debit->domain->voicePremiCredit;
    						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    					case 36:
    						//PREMI VOICE API
    						$balance = $debit->domain->voicePremiApiCredit;
    						$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
    						break;
    						 
    				}
    
    			}else{
    				$this->view->errors = array('Debit' => array('isFailed' => 'Transaction failed'));
    			}
    		}
    	}else{
    		if (count($form->getErrors('token')) > 0) {
    			$this->_redirect('/user/transaction/main');
    		}
    		$this->view->errors = $form->getMessages();
    	}
    	$form->reset();
    }
    
    public function mainsuggestAction()
    {
    	$this->_helper->layout()->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    	 
    	$key = $this->getRequest()->getParam('term');
    	 
    	$returnString = array();
    	if(!empty($key) && strlen($key) > 0) {
    		$emUser = $this->getEntityManager('user');
    		$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
    		$users = $emUser->getRepository("modules\user\models\Account")->getReseller("$resellerId", "$key");
    		if($users){
    			foreach($users as $user) {
    				$username = $user['username'];
    				$domain = $user['domain'];
    
    				$returnString[] = $username.' | '.$domain;
    			}
    		}
    	}
    	 
    	//echo ($returnString);
    	echo Zend_Json::encode($returnString);
    }

    public function autosuggestAction()
    {
        $this->_helper->layout()->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    	
    	$key = $this->getRequest()->getParam('term');
    	
    	$returnString = array();
    	if(!empty($key) && strlen($key) > 0) {
    		$emUser = $this->getEntityManager('user');
    		$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
    		$users = $emUser->getRepository("modules\user\models\Account")->getUser("$resellerId", "$key");    		
    		if($users){
    			foreach($users as $user) {
    				$username = $user['username'];
    				$domain = $user['domain'];
    				
    				$returnString[] = $username.' | '.$domain;
    			}
    		}
    	}
    	
    	//echo ($returnString);
    	echo Zend_Json::encode($returnString);
    }
    
	public function inAction()
    {
         $this->view->subject="CREDIT LOG";
    	$page = $this->view->navigation()->findOneByLabel('Credits In');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Transactions | Credits In');
    	}
    	
    	//$reportManager = new SendSmsManager();
    	$emUser = $this->getEntityManager('user');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	//$roleId = Zend_Auth::getInstance()->getIdentity()->role_id;
    	//$role = $emUser->getRepository("modules\user\models\Role")->find($roleId);
    	$this->view->userId = $userId;
    	$queryBuilder = $emUser->createQueryBuilder();
    	$queryBuilder->select('e')
			    	->from('modules\user\models\AccountLog', 'e')
			    	->where('e.user = ?1 AND e.mode = ?2')
			    	->orWhere('e.client = ?1 AND e.mode = ?3')
			    	->orderBy('e.id', 'DESC')
			    	->setParameter(1, $userId)
			    	->setParameter(2, 'DEDUCT')
			    	->setParameter(3, 'ADD');
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	->setItemCountPerPage(20);
    	 
    	$this->view->entries = $paginator;
    	*/
    }
    
    public function outAction()
    {
    	$this->view->subject="CREDIT LOG";
    	$page = $this->view->navigation()->findOneByLabel('Credits Out');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Transactions | Credits Out');
    	}
    	 
    	//$reportManager = new SendSmsManager();
    	$emUser = $this->getEntityManager('user');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$this->view->userId = $userId;
    	$queryBuilder = $emUser->createQueryBuilder();
    	$queryBuilder->select('e')
			->from('modules\user\models\AccountLog', 'e')
 			->where('e.user = ?1 AND e.mode = ?2')
			->orWhere('e.client = ?1 AND e.mode = ?3')
			->orderBy('e.id', 'DESC')
			->setParameter(1, $userId)
			->setParameter(2, 'ADD')
			->setParameter(3, 'DEDUCT');
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	->setItemCountPerPage(20);
        
    	$this->view->entries = $queryBuilder;
    	*/
    }




}

