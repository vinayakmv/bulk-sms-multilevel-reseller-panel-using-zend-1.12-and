<?php
class User_RecreditController extends Smpp_Controller_BaseController
{
	public function init()
	{
		$creditManage = $this->view->navigation()->findOneByLabel('Transactions');
		if ($creditManage) {
			$creditManage->setActive();
		}
		$action = $this->getRequest()->getActionName();
		$emUser = $this->getEntityManager('user');
		$domain = Smpp_Utility::getFqdn();
		$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
		$domainLayout = $domainRepo->layout;
		if ($domainLayout != null){
			$view = $domainLayout.'-'.$action;
			$this->_helper->viewRenderer("$view");
		}
		parent::init();
	}
	
	public function indexAction()
	{
		$this->view->subject="RECREDIT TRANSFER";
		$page = $this->view->navigation()->findOneByLabel('Sms Recredit');
		if ($page) {
			$page->setActive();
			$this->view->headTitle('Transactions | Sms Recredit');
		}
		$emUser = $this->getEntityManager('user');
		$emSms = $this->getEntityManager('sms');
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
				
		$queryBuilder = $emSms->createQueryBuilder();
		$queryBuilder->select('e')
		->from('modules\sms\models\SmsRecredit', 'e')
		->where('e.user = ?1')
		->orderBy('e.id', 'DESC')
		->setParameter(1, $userId);
		$query = $queryBuilder->getQuery();
		$this->view->entries = $query->getResult();
		$this->view->emUser = $emUser;
		/*
		$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
		$currentPage = 1;
		$i = $this->_request->getParam('i');
		if(!empty($i)){ //Where i is the current page
			$currentPage = $this->_request->getParam('i');
		}
		$paginator->setCurrentPageNumber($currentPage)
		->setItemCountPerPage(20);
		
		$this->view->entries = $paginator;
		*/
	}
	
	public function viewallAction()
	{
		$this->view->subject="RECREDIT TRANSFER";
		$page = $this->view->navigation()->findOneByLabel('All Sms Recredit');
		if ($page) {
			$page->setActive();
			$this->view->headTitle('Transactions |All Sms Recredit');
		}
		$emUser = $this->getEntityManager('user');
		$emSms = $this->getEntityManager('sms');
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
	
		$queryBuilder = $emSms->createQueryBuilder();
		$queryBuilder->select('e')
		->from('modules\sms\models\SmsRecredit', 'e')
		->orderBy('e.id', 'DESC');
		$query = $queryBuilder->getQuery();
		$this->view->entries = $query->getResult();
		$this->view->emUser = $emUser;
		/*
		$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
		$currentPage = 1;
		$i = $this->_request->getParam('i');
		if(!empty($i)){ //Where i is the current page
			$currentPage = $this->_request->getParam('i');
		}
		$paginator->setCurrentPageNumber($currentPage)
		->setItemCountPerPage(20);
	
		$this->view->entries = $paginator;
		*/
	}
}