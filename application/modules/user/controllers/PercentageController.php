<?php
use modules\user\models\UserManager;
use modules\user\models\ActivityLogManager;
class User_PercentageController extends Smpp_Controller_BaseController
{

    public function init()
    {
       $userManage = $this->view->navigation()->findOneByLabel('Percentage');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
		parent::init();
    }

    public function indexAction()
    {
        
    }
    
    public function userAction()
    {
    	$this->view->subject="USER PERCENTAGE";
    	$page = $this->view->navigation()->findOneByUri('/user/percentage/user');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Percentage | User');
    	}
    	$form = new User_Form_UserPercentage();
    	
    	$emUser = $this->getEntityManager('user');
    	$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
    	$queryBuilder = $emUser->createQueryBuilder()
    	->select('u.username, d.domain')
    	->from('modules\user\models\User', 'u')
    	->innerJoin('u.domain', 'd')
    	->where('u.reseller = ?2 AND u.id != ?2')
    	->setParameter("2", "$resellerId");
    	 
    	$underUserRepo = $queryBuilder->getQuery()->getResult();
    	$sender = array();
    	foreach ($underUserRepo as $underUser) {
    		$username = $underUser['username'].' | '.$underUser['domain'];
    		$sender[$username]=$username;
    	}
    	
    	if (isset($sender)) {
    		$form->getElement('username')->setMultiOptions($sender)->setRequired(true);
    		 
    	}
    	
    	$this->view->form = $form;
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    		$percentageValues = $form->getValues();
    		$username = $percentageValues['username'];
    		$mode = $percentageValues['mode'];
    		$type = $percentageValues['type'];
    		$route = $percentageValues['route'];
    		$percentage = $percentageValues['percentage'];
    		$arrayUsername = explode('|', $username);
    		$username = $arrayUsername['0'];
    		$domain = trim($arrayUsername['1']);
    		$userManager = new UserManager();
    		$userId = Zend_Auth::getInstance()->getIdentity()->id;
    		
    		$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => $domain));
    		$domainId = $domainRepo->id;
    		
    		$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    		$parentDomainId = $userRepo->domain->id;
    		
    		$clientRepo = $emUser->getRepository('modules\user\models\User')->findOneBy(array('username' => $username, 'parentDomain' => $parentDomainId, 'domain' => $domainId));
    		
    		$setPercentage = $userManager->userPercentage($clientRepo, $type, $mode, $route, $percentage, $emUser);
    		
    		if (isset($setPercentage)){
    			
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $clientRepo->id, $this->_module, $this->_controller, $this->_action, "$percentage% User Percentage Set Successfully", $this->_domain, $this->_ip, $this->_device, $route.'|'.$type, 'UPDATE', 'PERCENTAGE', $emUser);
    			$this->view->success = array('Percent' => array('isSuccess' => 'Percentage set successfully'));
    		}else{
    			$this->view->errors = array('Percent' => array('isFailed' => 'Percentage set failed'));
    		}
    		$form->reset();
    	}
    	
    }
    
    public function domainAction()
    {
        $this->view->subject="DOMAIN PERCENTAGE";
    	$page = $this->view->navigation()->findOneByUri('/user/percentage/domain');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Percentage | Domain');
    	}
    	
    	$form = new User_Form_DomainPercentage();

        $emUser = $this->getEntityManager('user');
        $resellerId = Zend_Auth::getInstance()->getIdentity()->id;
        $resellerRepo = $emUser->getRepository('modules\user\models\User')->find($resellerId);
        $parentDomainId = $resellerRepo->domain->id;
        $queryBuilder = $emUser->createQueryBuilder()
        ->select('u.username, d.domain')
        ->from('modules\user\models\User', 'u')
        ->innerJoin('u.domain', 'd')
        ->where('d.parentDomain = ?2 AND d.id != ?2')
        ->setParameter("2", "$parentDomainId");
         
        $underUserRepo = $queryBuilder->getQuery()->getResult();
        $sender = array();
        foreach ($underUserRepo as $underUser) {
            $username = $underUser['domain'].' | '.$underUser['username'];
            $sender[$username]=$username;
        }
        
        if (isset($sender)) {
            $form->getElement('username')->setMultiOptions($sender)->setRequired(true);
             
        }

    	$this->view->form = $form;
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    		$percentageValues = $form->getValues();
    		$username = $percentageValues['username'];
    		$mode = $percentageValues['mode'];
    		$type = $percentageValues['type'];
    		$route = $percentageValues['route'];
    		$percentage = $percentageValues['percentage'];
    		$arrayUsername = explode('|', $username);
    		$domain = $arrayUsername['0'];
    		$userManager = new UserManager();
    		$userId = Zend_Auth::getInstance()->getIdentity()->id;
    		$emUser = $this->getEntityManager('user');
    		$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    		$setPercentage = $userManager->domainPercentage($domain, $type, $mode, $route, $percentage, $emUser);
    		
    		if (isset($setPercentage)){
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userRepo->id, $this->_module, $this->_controller, $this->_action, "$percentage% Domain Percentage Set Successfully", $this->_domain, $this->_ip, $this->_device, $route.'|'.$type, 'UPDATE', 'PERCENTAGE', $emUser);
    			$this->view->success = array('percent' => array('isSuccess' => 'Percentage set successfully'));
    		}else{
    			$this->view->errors = array('percent' => array('isFailed' => 'Percentage set failed'));
    		}
    		$form->reset();
    	}
    	
    	
    	
    }
    
    public function autouserAction()
    {
    	$this->_helper->layout()->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    	 
    	$key = $this->getRequest()->getParam('term');
    	 
    	$returnString = array();
    	if(!empty($key) && strlen($key) > 2) {
    		$emUser = $this->getEntityManager('user');
    		$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
    		$users = $emUser->getRepository("modules\user\models\User")->getUser("$resellerId", "$key"); 
    		if($users){
    			foreach($users as $user) {
    				$username = $user['username'];
    				$domain = $user['domain'];
    
    				$returnString[] = $username.' | '.$domain;
    			}
    		}
    	}
    	 
    	//echo ($returnString);
    	echo Zend_Json::encode($returnString);
    }
    
    public function autodomainAction()
    {
    	$this->_helper->layout()->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    	 
    	$key = $this->getRequest()->getParam('term');
    	 
    	$returnString = array();
    	if(!empty($key) && strlen($key) > 2) {
    		$emUser = $this->getEntityManager('user');
    		$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
    		$users = $emUser->getRepository("modules\user\models\Domain")->getDomain("$key");
    		if($users){
    			foreach($users as $user) {
    				$domain = $user['domain'];
    
    				$returnString[] = $domain;
    			}
    		}
    	}
    	 
    	//echo ($returnString);
    	echo Zend_Json::encode($returnString);
    }


}

