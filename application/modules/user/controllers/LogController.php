<?php

class User_LogController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $crediManage = $this->view->navigation()->findOneByLabel('Credit Manage');
    	if ($crediManage) {
    		$crediManage->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
        parent::init();
    }

    public function indexAction()
    {
                
    }
    
    


}

