<?php
use modules\user\models\LeadManager;
use Doctrine\ORM\Query\ResultSetMapping;
use modules\user\models\LeadDetailManager;
class User_ContactController extends Smpp_Controller_BaseController
{
	public function init()
	{
		$broadcast = $this->view->navigation()->findOneByLabel('Address Book');
		if ($broadcast) {
			$broadcast->setActive();
		}
		$action = $this->getRequest()->getActionName();
		$emUser = $this->getEntityManager('user');
		$domain = Smpp_Utility::getFqdn();
		$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
		$domainLayout = $domainRepo->layout;
		if ($domainLayout != null){
			$view = $domainLayout.'-'.$action;
			$this->_helper->viewRenderer("$view");
		}
		parent::init();
	}
	
	public function indexAction()
	{
		$this->view->subject="GROUP VIEW";
		$page = $this->view->navigation()->findOneByUri('/user/contact/index');
		if ($page) {
			$page->setActive();
			$this->view->headTitle('Address Book | Groups');
		}
		
		
		$emUser = $this->getEntityManager('user');
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
		
		if ($this->getRequest()->isPost()) {
			$getValues = $this->getRequest()->getPost();
			
			$leadManager = new LeadManager();
			if($getValues['submit'] == 'Delete'){
				$manageLead = $leadManager->manageLead($getValues,'Delete', $userId, $emUser);
				//$this->view->Success = array('approved' => array('success' => 'Approved successfully'));
					
			}elseif ($getValues['submit'] == 'Save') {
				$manageLead = $leadManager->manageLead($getValues,'Edit', $userId, $emUser);
				//$this->view->Success = array('rejected' => array('success' => 'Rejected Successfully'));
			}
		}
		
		
				$queryBuilder = $emUser->createQueryBuilder();
		
		
				$queryBuilder->select('e')
				->from('modules\user\models\Lead', 'e')
				->where('e.user = ?1')
				->orderBy('e.id', 'DESC')
				->setParameter(1, $userId);
				$query = $queryBuilder->getQuery();
				$this->view->entries = $query->getResult();
				/*
				$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
				$currentPage = 1;
				//$i = $this->_request->getParam('i');
				$i = $this->_request->getParam('p');
				if(!empty($i))
				{ //Where i is the current page
					$currentPage = $this->_request->getParam('p');
				}
				$perPage = 10;
				$paginator->setCurrentPageNumber($currentPage)
				->setItemCountPerPage($perPage);
				$this->view->entries = $paginator;
				if($i==1 || $i=='')
				{
					$this->view->pageIndex = 1;
				}
				else
				{
					$this->view->pageIndex = (($i-1)*$perPage)+1;
				}
				$this->view->userId = $userId;
				*/
				
	}
	
	public function createAction()
	{
		$this->view->subject="CREATE GROUP";
    	$page = $this->view->navigation()->findOneByUri('/user/contact/create');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Address Book | Groups');
    	}
    	
    	$form = new User_Form_Contact();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId); 

    	$update = $this->getRequest()->getPost();
    	if (isset($update['add'])){
    		$id = $update['add'];
    		$leadRepo = $emUser->getRepository('modules\user\models\Lead')->findOneBy(array('user' => $userId,'id' => $id));
    		$form->getElement('contactName')->setAttrib('readonly', 'readonly')->setValue($leadRepo->name);
    		
    		$form->getElement('submit')->setLabel('Update Group')->setValue('update');
    		
    	}else{
    		
    		if ($this->getRequest()->getPost()){
    			$updateValue = $this->getRequest()->getPost();
    			if ($updateValue['submit'] != 'Update Group'){
    				$form->getElement('contactName')->addValidator(new Smpp_Validate_Db_IsGroupExists());
    			}
    		}
    		
    		if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    			$originalFilename1 = pathinfo($form->uploadContact->getFileName());
    			$newFilename1 = 'uploadContact-' . uniqid() . '.' . $originalFilename1['extension'];
    			$form->uploadContact->addFilter('Rename', $newFilename1);
    			 
    			$contactValues = $form->getValues();
    			
    			$contactManager = new LeadManager();
    			
    			if ($updateValue['submit'] == 'Update Group'){
    				$contactCreated = $contactManager->updateGroup($contactValues, $userId, $emUser);
    			}else{
    				$contactCreated = $contactManager->createGroup($contactValues, $userId, $emUser);
    			}
    		
    			if($contactCreated){
    				$this->view->success = $contactCreated['Contact']['success'];
    			}else {
    				$this->view->errors = array();
    			}
    			
    		}else{
    			$this->view->errors = $form->getMessages();
    		}
    		
    		$form->reset();
    	}
    	
    	$this->view->form = $form;
	}
	
	public function viewAction()
	{
		$this->view->subject="GROUP VIEW";
		$page = $this->view->navigation()->findOneByLabel('Groups');
		if ($page) {
			$page->setActive();
			$this->view->headTitle('Address Book | Groups');
		}
		//$this->_helper->viewRenderer->setNoRender(true);
		//$this->_helper->layout->disableLayout();
		$emUser = $this->getEntityManager('user');
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
		if ($this->getRequest()->isPost()) {
			
			$getValues = $this->getRequest()->getPost();
			
			if (isset($getValues['submit'])){
				$leadManager = new LeadDetailManager();
				if($getValues['submit'] == 'Delete'){
					$manageLead = $leadManager->manageLead($getValues,'Delete', $userId, $emUser);
					//$this->view->Success = array('approved' => array('success' => 'Approved successfully'));
						
				}elseif ($getValues['submit'] == 'Save') {
					$manageLead = $leadManager->manageLead($getValues,'Edit', $userId, $emUser);
					//$this->view->Success = array('rejected' => array('success' => 'Rejected Successfully'));
				}
			}	
			
		}
		//$campId = $this->_request->getParam('id');
		$campId = $this->getRequest()->getPost('campid', null);
		if ($campId){
			$queryBuilder = $emUser->createQueryBuilder();
		
		
				$queryBuilder->select('e')
				->from('modules\user\models\LeadDetail', 'e')
				->where('e.user = ?1')
				->andwhere('e.lead = ?2')
				->orderBy('e.id', 'DESC')
				->setParameter(1, $userId)
				->setParameter(2, $campId);
				$query = $queryBuilder->getQuery();
				$this->view->entries = $query->getResult();
				/*
				$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
				$currentPage = 1;
				//$i = $this->_request->getParam('i');
				$i = $this->_request->getParam('p');
				if(!empty($i))
				{ //Where i is the current page
					$currentPage = $this->_request->getParam('p');
				}
				$perPage = 10;
				$paginator->setCurrentPageNumber($currentPage)
				->setItemCountPerPage($perPage);
				$this->view->entries = $paginator;
				if($i==1 || $i=='')
				{
					$this->view->pageIndex = 1;
				}
				else
				{
					$this->view->pageIndex = (($i-1)*$perPage)+1;
				}
				$this->view->userId = $userId;
				*/
		}else{
			$this->view->entries = array();
		}
	}
}