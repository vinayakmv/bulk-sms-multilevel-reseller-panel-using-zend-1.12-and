<?php

use modules\user\models\AccountManager;
use modules\user\models\UserManager;
use modules\user\models\UserProfileManager;
class User_ProfileController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $broadcast = $this->view->navigation()->findOneByLabel('Profile');
    	if ($broadcast) {
    		$broadcast->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
    	parent::init();
    }

    public function indexAction()
    {
    $this->view->subject="USER PROFILE";
    	$page = $this->view->navigation()->findOneByUri('/user/profile/index');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Profile | User');
    	}
    	 
    	$form = new User_Form_Profile();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	
    	$name = $userRepo->profile->name;
    	$email = $userRepo->profile->email;
    	$country = $userRepo->profile->country;
    	$mobile = $userRepo->profile->mobile;
    	
    	$form->getElement('name')->setValue($name);
    	$form->getElement('email')->setValue($email);
    	$form->getElement('country')->setValue($country);
    	$form->getElement('mobile')->setValue($mobile);

    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {

    		$profileValues = $form->getValues();
    		
    		$profileManager = new UserProfileManager();
    		$updateProfile = $profileManager->updateProfile($profileValues, $userRepo, $emUser);
    		
    		if ($updateProfile){
    			$this->view->success = array('Profile' => array('isSuccess' => "Profile updated"));
    		}else{
    			$this->view->errors = array('Profile' => array('isFailed' => "Update failed"));
    		}
    		
    	}else{
    		$this->view->errors = $form->getMessages();
    		
    	}
    }

    public function accountAction()
    {
    	$form = new User_Form_Account();
    	$this->view->formAccount = $form;
    	
    	if($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())){
    		$accountValues = $form->getValues();
    		
    		$emUser = $this->getEntityManager('user');
    		$userId = Zend_Auth::getInstance()->getIdentity()->id;
    		$firstName = $accountValues['fname']; 
    		$lastName = $accountValues['lname'];
    		$mobile = $accountValues['mobile'];
    		$accountManager = new AccountManager();
    		$accountManager->updateAccount($userId, $firstName, $lastName, $mobile, $emUser);
    		
    		if ($accountManager) {
    			//$this->view->success = 'Accounts updated successfully';
    			$this->view->success = array('Accounts' => array('isSuccess' => 'Accounts updated successfully'));
    		}else {
    			//$this->view->errors = 'Accounts update failed';
    			$this->view->errors = array('Accounts' => array('isFailed' => 'Accounts updating failed'));
    		}
    	}else{
    		$this->view->errors = $form->getMessages();
    	}
    	$form->reset();
    	
    }

    public function passwordAction()
    {
    	$this->view->subject="CHANGE PASSWORD";
    	$this->view->headTitle('Profile Manage | Change Password');
    	$form = new User_Form_Password();
    	$this->view->formPassword = $form;
    	
    	$formAlert = new User_Form_Threshold();
    	$this->view->formAlert = $formAlert;
    	
    	$formAccount = new User_Form_Account();
    	$this->view->formAccount = $formAccount;
    	
    	if($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())){

    		$passwordValues = $form->getValues();
    		$newPassword =  $passwordValues['password'];
    		$oldPassword = $passwordValues['oldpassword'];
    			
    		$userId = Zend_Auth::getInstance()->getIdentity()->id;
    		
    		$emUser = $this->getEntityManager('user');
    		$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    		$identity = $userRepo->username;
    		$userAuth = new \Smpp_Auth_User();
    		$login = $userAuth->login($identity, $oldPassword, $emUser);
    		if ($login == '1') {
    			$userManager = new UserManager();
    			$updatePassword = $userManager->updatePassword($userId, $newPassword, $emUser);
    			
    			if ($updatePassword) {
    				//$this->view->success = 'Password updated successfully';
    				$this->view->success = array('Accounts' => array('isSuccess' => 'Password updated successfully'));
    			}else {
    				//Error
    				//$this->view->errors = 'Password update failed';
    				$this->view->errors = array('Accounts' => array('isFailed' => 'Password updating failed'));
    			}
    			
    		}else {
    			$this->view->errors = array('Accounts' => array('isFailed' => 'Current password is incorrect, Forcefully Signing out'));
    			
    		}
    		
    		
    	}else {
    		$this->view->errors = $form->getMessages();
    	}
    	$form->reset();
    }

    public function alertAction()
    {
    	$form = new User_Form_Threshold();
    	$this->view->formAlert = $form;
    	
    	if($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())){
    		$alertValues = $form->getValues();
    		$alert = $alertValues['alert'];
    		$threshold = $alertValues['threshold'];
    		$emUser = $this->getEntityManager('user');
    		$userId = Zend_Auth::getInstance()->getIdentity()->id;
    		$accountManager = new AccountManager();
    		$updateAlert = $accountManager->setThreshold($userId, $alert, $threshold, $emUser);
    		
    		if ($updateAlert) {
    			//$this->view->success = 'Threshold updated successfully';
    			$this->view->success = array('Accounts' => array('isSuccess' => 'Threshold updated successfully'));
    		}else {
    			//Error
    			//$this->view->errors = 'Threshold update failed';
    			$this->view->errors = array('Accounts' => array('isFailed' => 'Threshold updating failed'));
    		}
    	}else{
    		$this->view->errors = $form->getMessages();
    	}
    	$form->reset();
    }


}