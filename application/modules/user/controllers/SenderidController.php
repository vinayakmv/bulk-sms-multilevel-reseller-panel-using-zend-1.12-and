<?php
use Models\SenderIdManager;
class User_SenderidController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $senderid = $this->view->navigation()->findOneByLabel('Sender IDs');
    	if ($senderid) {
    		$senderid->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
    	parent::init();
    }

    public function indexAction()
    {
   		$page = $this->view->navigation()->findOneByLabel('Request Voice Sender-ID');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Broadcast | Request Voice Sender-ID');
    	}
    	$form = new Voice_Form_Register();
        $this->view->form = $form;
    }
    
    public function approveAction() {
    	$page = $this->view->navigation()->findOneByLabel('Approve Sender-ID');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Broadcast | Approve Sender-ID');
    	}
    	$em = $this->getEntityManager();
    	$this->view->em = $em;
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$queryBuilder = $em->createQueryBuilder();
    	$queryBuilder->select('e')
    	->from('Models\SenderId', 'e')
    	->orderBy('e.id', 'DESC');
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	->setItemCountPerPage(20);
    	 
    	$this->view->entries = $paginator;
    	*/
    	// Block,Unblock,Activate Users,Resellers
    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
    	
    		if (isset($button['active'])) {
    			//Block User
    			$userManager = new SenderIdManager();
    			$userManager->blockSenderId($button['active'], $em);
    		}
    	
    		if (isset($button['blocked'])) {
    			//Unblock User
    			$userManager = new SenderIdManager();
    			$userManager->unblockSenderId($button['blocked'], $em);
    		}
    	
    		if (isset($button['notActivated'])) {
    			//Activate User
    			$userManager = new SenderIdManager();
    			$userManager->activateSenderId($button['notActivated'], $em);
    		}
    	
    		if (isset($button['delete'])) {
    			//Activate User
    			$userManager = new SenderIdManager();
    			$userManager->deleteSenderId($button['delete'], $em);
    		}
    	
    	}
    }


}

