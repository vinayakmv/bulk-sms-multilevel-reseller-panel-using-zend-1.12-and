<?php
use modules\user\models\UserManager;
class User_DashboardController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $page = $this->view->navigation()->findOneByLabel('Dashboard');
        if ($page) {
          $page->setActive();
          $this->view->headTitle('Dashboard');

        }
        $action = $this->getRequest()->getActionName();
        $emUser = $this->getEntityManager('user');
        $domain = Smpp_Utility::getFqdn();
        $domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
        $domainLayout = $domainRepo->layout;
        if ($domainLayout != null){
                $view = $domainLayout.'-'.$action;
                $this->_helper->viewRenderer("$view");
        }
        parent::init();
    }

    public function indexAction()
    {
        $this->view->subject="DASHBOARD";
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;        
        $emUser = $this->getEntityManager('user');
        $emSms = $this->getEntityManager('sms');
        $emVoice = $this->getEntityManager('voice');
        $emSmpp = $this->getEntityManager('smpp');
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
	$this->view->roleId = $userRepo->role->id;
        
        
        $sevenDays = date('Y-m-d', strtotime('- 7 days'));
        $startBefore7Days = date("Y-m-d 00:00:00", strtotime('- 7 days'));
        $startBefore7DaysTimestamp = strtotime($startBefore7Days);
        
        $yesterDay = date('Y-m-d', strtotime('- 1 day'));
        $startYesterDay = date("Y-m-d 00:00:00", strtotime('- 1 day'));
        $startYesterDayTimestamp = strtotime($startYesterDay);
        
        $queryBuilderLog = $emSms->createQueryBuilder()
    	->select('u')
    	->from('modules\sms\models\SmsDashboard', 'u')
    	->where('u.user = ?1 AND u.reportDate >= ?2')
    	->setParameter("1", "$userId")
        ->setParameter("2", "$sevenDays");
    	
    	$dashboardLogRepo = $queryBuilderLog->getQuery()->getResult();
        
        
        
        
        
    }


}

