<?php

use modules\user\models\UserManager;
use modules\user\models\UserRepository;
class User_AccountController extends Smpp_Controller_BaseController
{

    public function init()
    {
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function signupAction()
    {
    	$form = new User_Form_NewUser();
        $this->view->form = $form;
        if($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())){
			$userValues = $form->getValues();
			$emUser = $this->getEntityManager('user');
			$emSms = $this->getEntityManager('sms');
			$emVoice = $this->getEntityManager('voice');
        	//Create User
        	$userManager = new UserManager();
        	$userManager->createUser($userValues, $emUser, $emSms, $emVoice);        	
        }else{
        	$this->view->errors = $form->getMessages();
        }
        $form->reset();
        
    }



}









