<?php
namespace modules\user\models;
/**
 * 
 * @author Vinayak
 *
 */
class ApikeyManager  extends \Smpp_Doctrine_BaseManager
{
	public function createApi($apiArray, $userId, $emSms, $emUser, $emVoice)
	{
		if (isset($apiArray)){
			
       
			$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
			
				$key = new Apikey();
				$key-> user= $userRepo->id;
				$key-> apiKey=$apiArray['api'];
				$key-> status='1';
				$key-> createdBy=$userId;

				$key-> createdDate = new \DateTime('now');
				$key-> updatedBy=$userId;
				$key-> updatedDate=new \DateTime('now');
				$emUser->persist($key);
				$emUser->flush();
				
				$trans = $userRepo->domain->transRoute;
				$promo = $userRepo->domain->promoRoute;
				$scrub = $userRepo->domain->scrubRoute;
				$inter = $userRepo->domain->interRoute;
				
				$apiValues = array();
				if (isset($trans)){
					$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$trans,'status'=>'1'));
					$apiValues['smsType'] = "$routes->type".'|'."$routes->route";
					$apiValues['type'] = 'TL';
					
					$userManager = new UserManager();
					$routeAssigned = $userManager->setApi($apiValues, $userId, $emUser);
					
				}
				if (isset($promo)){
					$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$promo,'status'=>'1'));
					$apiValues['smsType'] = "$routes->type".'|'."$routes->route";
					$apiValues['type'] = 'PL';
					
					$userManager = new UserManager();
					$routeAssigned = $userManager->setApi($apiValues, $userId, $emUser);
				}
				if (isset($scrub)){
					$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$scrub,'status'=>'1'));
					$apiValues['smsType'] = "$routes->type".'|'."$routes->route";
					$apiValues['type'] = 'SL';
					
					$userManager = new UserManager();
					$routeAssigned = $userManager->setApi($apiValues, $userId, $emUser);
				}
				if (isset($inter)){
					$routes = $emUser->getRepository('modules\user\models\SmsRoute')->findOneBy(array('id'=>$inter,'status'=>'1'));
					$apiValues['smsType'] = "$routes->type".'|'."$routes->route";
					$apiValues['type'] = 'IL';
						
					$userManager = new UserManager();
					$routeAssigned = $userManager->setApi($apiValues, $userId, $emUser);
				}
				
				
				return $key;
		}
		return false;
	}
	public function addIp($apiArray, $em)
	{
		if (isset($apiArray)){
       		 $id=$apiArray['apiId'];
			 $apiRepo = $em->getRepository('modules\user\models\Apikey')->findOneBy(array('id' => "$id"));
				if ($apiRepo) {
					$ip = '';
					foreach(array_unique($apiArray['myInputs']) as $ipArray){
						if($ipArray!=''){
							if (filter_var($ipArray, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) {

								 if($ip!=''){
								 $ip.=',';
								}
									$ip.=$ipArray;
								}
								
							}
						
					}

				}	
				if ($ip!='') {
					$apiRepo-> ip=$ip;
					$em->persist($apiRepo);
					$em->flush();
					return $apiRepo;
				}
				else {
					return false;
				}

			}
		
		return false;
	}

	public function deleteApi($apiId, $em)
	{
			//print_r($apiArray);
       		 //$id=$apiArray['apiId'];
			 $apiRepo = $em->getRepository('modules\user\models\Apikey')->findOneBy(array('id' => "$apiId"));
			 //print_r($apiRepo);
				$apiRepo-> status='2';
				$em->persist($apiRepo);
				$em->flush();
				return $apiRepo;
			
	}
	public function editApi($apiId, $em,$userId,$api)
	{
			   $apiRepo = $em->getRepository('modules\user\models\Apikey')->findOneBy(array('id' => "$apiId"));
			   $apiRepo-> apiKey=$api;
			   $apiRepo-> updatedBy=$userId;
			   $apiRepo-> updatedDate=new \DateTime('now');
			   $em->persist($apiRepo);
			   $em->flush();
			   return $apiRepo;
	}
}
