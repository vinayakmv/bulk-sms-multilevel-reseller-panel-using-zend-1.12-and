<?php
namespace modules\user\models;

class DomainManager extends \Smpp_Doctrine_BaseManager
{
	public function createDomain($domainName, $em)
	{
		if (isset($domainName)) {
			$domainProfile = new DomainProfile();
			$domainProfile->introduction = null;
			$domainProfile->about = null;
			$domainProfile->address = null;
			$domainProfile->createdDate = new \DateTime('now');
			$domainProfile->updatedDate = new \DateTime('now');
			$em->persist($domainProfile);
			$em->flush();
			
			$domain = new Domain();
			$domain->domain = $domainName;
			$domain->domainProfile = $domainProfile;
			$domain->owner = '0';
			$domain->status = '1';
			$domain->recredit = '0';
			$domain->voiceRecredit = '0';
			$domain->voicePromoCredit = '0';
			$domain->voiceTransCredit = '0';
			$domain->smsPromoCredit = '0';
			$domain->smsScrubCredit = '0';
			$domain->smsTransCredit = '0';
			$domain->voicePromoApiCredit = '0';
			$domain->voiceTransApiCredit = '0';
			$domain->smsPromoApiCredit = '0';
			$domain->smsTransApiCredit = '0';
			$domain->smsScrubApiCredit = '0';
			$domain->createdDate = new \DateTime('now');
			$domain->updatedDate = new \DateTime('now');
			$em->persist($domain);
			$em->flush();
			
			return $domain;
			
		}
		return false;
	}
	
	public function updateProfile($profileValues, $domainId, $userId, $em)
	{
		
		if (isset($em)){
			$domain = $em->getRepository('modules\user\models\Domain')->findOneBy(array('id' => "$domainId", 'owner' => "$userId"));
			$domain->footer = $profileValues['company'];
                        if ($profileValues['theme'] == '1'){
                            $theme = 'boardster';
                        }elseif ($profileValues['theme'] == '2') {
                            $theme = 'adminto';
                        }elseif ($profileValues['theme'] == '3') {
                            $theme = 'flatlab';
                        }elseif ($profileValues['theme'] == '4') {
                            $theme = 'luna';
                        }
                        $domain->layout = $theme;
			if ($profileValues['uploadLogo']){
				$domain->logo = $profileValues['uploadLogo'];
			}			
			if ($domain->domainProfile == null){
				$domainProfile = new DomainProfile();
				$domainProfile->introduction = $profileValues['intro'];
				$domainProfile->about = $profileValues['about'];
				$domainProfile->address = $profileValues['address'];
				$domainProfile->createdDate = new \DateTime('now');
				$domainProfile->updatedDate = new \DateTime('now');
				$em->persist($domainProfile);
				$em->flush();
				$domain->domainProfile = $domainProfile;
			}else{
				$domain->domainProfile->introduction = $profileValues['intro'];
				$domain->domainProfile->about = $profileValues['about'];
				$domain->domainProfile->address = $profileValues['address'];
				$domain->domainProfile->updatedDate = new \DateTime('now');
			}
			$domain->updatedDate = new \DateTime('now');
			$em->persist($domain);
			$em->flush();
	
			return $domain;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $id
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function blockDomain($domainId, $resellerId, $em)
	{
		if (isset($resellerId)){
			$domain = $em->getRepository('modules\user\models\Domain')->findOneBy(array('id' => "$domainId", 'reseller' => "$resellerId"));
			$domain->status = '0';
			$em->persist($domain);
			$em->flush();
	
			return $domain;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $user
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function unblockDomain($domainId, $resellerId, $em)
	{
		if (isset($resellerId)){
			$domain = $em->getRepository('modules\user\models\Domain')->findOneBy(array('id' => "$domainId", 'reseller' => "$resellerId"));
			$domain->status = '1';
			$em->persist($domain);
			$em->flush();
	
			return $domain;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $id
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function blockallDomain($domainId, $em)
	{
		if (isset($domainId)){
			$domain = $em->getRepository('modules\user\models\Domain')->findOneBy(array('id' => "$domainId"));
			$domain->status = '0';
			$em->persist($domain);
			$em->flush();
	
			return $domain;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $user
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function unblockallDomain($domainId, $em)
	{
		if (isset($domainId)){
			$domain = $em->getRepository('modules\user\models\Domain')->findOneBy(array('id' => "$domainId"));
			$domain->status = '1';
			$em->persist($domain);
			$em->flush();
	
			return $domain;
		}
		return false;
	}
}