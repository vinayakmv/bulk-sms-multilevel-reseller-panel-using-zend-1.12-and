<?php
namespace modules\user\models;
use Doctrine\ORM\EntityRepository;

/**
 * 
 * @author Ani
 *
 */
class ApikeyRepository extends \Smpp_Doctrine_EntityRepository
{

	/**
	 * Get user details from API Key - To be used in API calls - Using Native SQL
	 * @param  String $apiKey API key to be verified
	 * @return Array          User details
	 */
	
	public function getApiDetails($apiKey)
	{
		$sql="SELECT user_id,country_code,ip FROM api_keys WHERE api_key = '$apiKey' AND status = '1'";
    	$stmt = $this ->getEntityManager()->getConnection()->prepare($sql);
    	$stmt->execute();
    	return $stmt->fetchAll();
	}
	public function getAllApi($userId)
	{
		$sql="SELECT * FROM api_keys WHERE user_id='$userId' AND status != '2'";
    	$stmt = $this ->getEntityManager()->getConnection()->prepare($sql);
    	$stmt->execute();
    	return $stmt->fetchAll();

	}
	public function countApi($userId)
	{
		$sql="SELECT count(id) as count FROM api_keys WHERE user_id='$userId' AND status != '2'";
    	$stmt = $this ->getEntityManager()->getConnection()->prepare($sql);
    	$stmt->execute();
    	return $stmt->fetchAll();

	}
	public function getresendApi($userId,$apiId)
	{
		$sql="SELECT api_key FROM api_keys WHERE user_id='$userId' AND id = '$apiId'";
    	$stmt = $this ->getEntityManager()->getConnection()->prepare($sql);
    	$stmt->execute();
    	return $stmt->fetchAll();
	}
}