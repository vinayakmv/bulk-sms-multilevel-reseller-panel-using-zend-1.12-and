<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\SmppRouteRepository")
 * @Table(name="smpp_routes")
 * @author Vinayak
 *
 */
class SmppRoute
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="smsc_id", type="string")
	 * @var text
	 */
	private $route;
	
	/**
	 * @Column(name="name", type="string")
	 * @var text
	 */
	private $name;
	
	/**
	 * @Column(name="type", type="string")
	 * @var text
	 */
	private $type;
	
	/**
	 * @ManyToMany(targetEntity="User", mappedBy="smppRoutes")
	 * @var integer
	 */
	private $users;
	
	/**
	 * @column(name="status", type="integer")
	 * @var integer
	 */
	private $status;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}