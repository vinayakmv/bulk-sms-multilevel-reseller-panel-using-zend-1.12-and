<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\ActivityLogRepository")
 * @Table(name="log_activity")
 * @author Vinayak
 *
 */
class ActivityLog
{
	/**
	* @Column(name="id", type="integer", nullable=false)
	* @Id
	* @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @ManyToOne(targetEntity="User", inversedBy="activityLogs")
	 * @JoinColumn(name="user_id", referencedColumnName="id")
	 * @var integer
	 */
	private $user;

	/**
	 * @column(name="client_id", type="string", nullable="true")
	 * @var unknown
	 */
	private $client;

	/**
	 * @column(name="module_name", type="string", nullable="false")
	 * @var unknown
	 */
	private $module;


	/**
	 * @column(name="controller_name", type="string", nullable="false")
	 * @var unknown
	 */
	private $controller;

	/**
	 * @column(name="action_name", type="string", nullable="false")
	 * @var unknown
	 */
	private $action;
	
	/**
	 * @column(name="activity_comment", type="string", nullable="false") 
	 * @var unknown
	 */
	private $activityComment;
	
	/**
	 * @column(name="domain_name", type="string", nullable="false") 
	 * @var unknown
	 */
	private $domain;
	
	/**
	 * @column(name="remote_ip", type="text", nullable="false")
	 * @var unknown
	 */
	private $remoteIp;
	
	/**
	 * @column(name="remote_device", type="object", nullable="false")
	 * @var unknown
	 */
	private $remoteDevice;

	
	/**
	 * @column(name="activity_id", type="string", nullable="true") 
	 * @var unknown
	 */
	private $data;
	
	/**
	 * @column(name="mode", type="string", nullable="false") 
	 * @var unknown
	 */
	private $mode;
	
	/**
	 * @column(name="type", type="string", nullable="false")
	 * @var integer
	 */
	private $type;
		
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
