<?php
namespace modules\user\models;

/**
 * @Entity
 * @Table(name="sessions")
 * @author Vinayak
 * 
 */
class Session
{
	/**
	 *
	 * @var integer $id
	 * @Column(name="id", type="string", length=32, nullable=false)
	 * @Id
	 *
	 */
	private $id;

	/**
	 * @var integer $modified
	 * @column(name="modified", type="integer")
	 */
	private $modified;

	/**
	 * @var integer $lifetime
	 * @column(name="lifetime", type="integer")
	 */
	private $lifetime;

	/**
	 * @var string $data
	 * @column(name="data", type="text")
	 */
	private $data;

}