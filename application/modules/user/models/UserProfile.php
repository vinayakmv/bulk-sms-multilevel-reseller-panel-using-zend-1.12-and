<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\UserRepository")
 * @Table(name="users_profile")
 * @author Vinayak
 *
 */
class UserProfile
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
		
	/**
	 * @column(name="name", type="string", nullable="false")
	 * @var string
	 */
	private $name;
	
	/**
	 * @column(name="email", type="string", nullable="false")
	 * @var string
	 */
	private $email;
	
	/**
	 * @column(name="mobile", type="string", nullable="false")
	 * @var string
	 */
	private $mobile;
	
	/**
	 * @column(name="country", type="string", nullable="false")
	 * @var string
	 */
	private $country;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=true)
	 * @var datetime
	 */
	private $updatedDate;

	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}