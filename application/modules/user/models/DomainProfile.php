<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\DomainProfileRepository")
 * @Table(name="domains_profile")
 * @author Vinayak
 *
 */
class DomainProfile 
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @OneToOne(targetEntity="Domain", mappedBy="domainProfile")
	 * @var integer
	 */
	private $domain;
	
	
	/**
	 * @Column(name="introduction", type="text", nullable=true)
	 * @var string
	 */
	private $introduction;
	
	/**
	 * @Column(name="about", type="text", nullable=true)
	 * @var string
	 */
	private $about;

		/**
	 * @Column(name="address", type="text", nullable=true)
	 * @var string
	 */
	private $address;

	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}
