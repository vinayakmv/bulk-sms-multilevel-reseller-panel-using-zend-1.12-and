<?php
namespace modules\user\models;

class ActivityLogManager extends \Smpp_Doctrine_BaseManager
{
	public function createActivityLog($userRepo, $client, $module, $controller, $action, $activityComment, $domain, $remoteIp, $remoteDevice, $data, $mode, $type, $em)
	{
		if (isset($em)){
			$activity = new ActivityLog();
			$activity->user = $userRepo;
			$activity->client = $client;
			$activity->module = $module;
			$activity->controller = $controller;
			$activity->action = $action;
			$activity->activityComment = $activityComment;
			$activity->data = $data;
			$activity->domain = $domain;
			$activity->remoteIp = $remoteIp;
			$activity->remoteDevice = $remoteDevice;
			$activity->mode = $mode;
			$activity->type = $type;
			$activity->createdDate = new \DateTime('now');
			$activity->updatedDate = new \DateTime('now');
			$em->persist($activity);
			$em->flush();
				
			return $activity;
		}
		return false;
	}

}
