<?php
namespace modules\user\models;
use Doctrine\ORM\EntityManager;

class DomainRepository extends \Smpp_Doctrine_EntityRepository
{
	/**
	 * 
	 * @param string $domain
	 */
	public function getDomainByFqdn($domain) 
	{
		$dql = "SELECT u FROM modules\user\models\Domain u WHERE u.domain = '".$domain."'";
		$query = $this->getEntityManager()->createQuery($dql);
		return $query->getResult();
	}
	

	public function getDomain($searchKey)
	{
		//$dql = "SELECT A FROM modules\user\models\Users A WHERE A.reseller = $userId AND A.user LIKE :keyword";
		//$query = $this->getEntityManager()->createQuery($dql);
		//$query->setParameter("keyword", '%'.$searchKey.'%');
		//return $query->getResult();
		$queryBuilder = $this->getEntityManager()->createQueryBuilder()
		->select('u.domain')
		->from('modules\user\models\Domain', 'u')
		->where('u.domain LIKE ?1')
		->setParameter("1", '%'.$searchKey.'%');
	
		return $queryBuilder->getQuery()->getResult();
	}
}