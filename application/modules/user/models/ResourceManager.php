<?php
namespace modules\user\models;

class ResourceManager extends \Smpp_Doctrine_BaseManager
{
	public function createResource($resourceValues, $em)
	{
		if (isset($resourceValues)) {
			//Create Resource
			$resourceName = $resourceValues['resourceName'];
			$resource = new Resource();
			$resource->name = $resourceName;
			$resource->createdDate = new \DateTime('now');
			$resource->updatedDate = new \DateTime('now');
			$em->persist($resource);
			$em->flush();
				
			return $resource;
		}
		return false;
	}
}