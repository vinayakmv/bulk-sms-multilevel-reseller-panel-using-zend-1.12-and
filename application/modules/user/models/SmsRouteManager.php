<?php
namespace modules\user\models;

class SmsRouteManager extends \Smpp_Doctrine_BaseManager
{
	public function createRoute($routeValues, $em)
	{
		if (isset($em)){
			$repo = $em->getRepository("modules\user\models\SmsRoute")->findOneBy(array('route' => $routeValues['route'])); 			
			if (!isset($repo->id)) {
				$route = new SmsRoute();
				$route->route = $routeValues['route'];
				$route->name = $routeValues['name'];
				$route->type = $routeValues['type'];
				$route->status = '1';
				$route->createdDate = new \DateTime('now');
				$route->updatedDate = new \DateTime('now');
				$em->persist($route);
				$em->flush();
				
				return $route;
			}
	
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $id
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function blockSmsc($id, $em)
	{
		if (isset($id)){
			$user = $em->getRepository('modules\user\models\SmsRoute')->find($id);
			$user->status = '0';
			$em->persist($user);
			$em->flush();
				
			return $user;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $user
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function unblockSmsc($id, $em)
	{
		if (isset($id)){
			$user = $em->getRepository('modules\user\models\SmsRoute')->find($id);
			$user->status = '1';
			$em->persist($user);
			$em->flush();
				
			return $user;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $user
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function deleteSmsc($id, $em)
	{
		if (isset($id)){
			$user = $em->getRepository('modules\user\models\SmsRoute')->find($id);
			$em->remove($user);
			$em->flush();				
				
			return $user;
		}
		return false;
	}
}