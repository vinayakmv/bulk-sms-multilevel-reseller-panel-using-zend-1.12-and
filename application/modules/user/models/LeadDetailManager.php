<?php
namespace modules\user\models;

class LeadDetailManager extends \Smpp_Doctrine_BaseManager
{
	public function createLead($contacts, $userId, $type, $status, $groupId, $em)
	{
		$batchSize = 100;
		$count = count($contacts);
		if (!is_array($contacts)){
			$contacts = array($contacts);
		}
		$i = 0;
		if (isset($em)) {
			foreach($contacts as $line){
				$val = explode(",",$line);
				$name = isset($val['1']) ? $val['1'] : null;
				if ($val['0']){
					$lead = new LeadDetail();
					$lead->user = $userId;
					$lead->lead = $groupId;
					$lead->mobile = $val['0'];
					$lead->name = $name;
					$lead->type = $type;
					$lead->createdDate = new \DateTime('now');
					$lead->updatedDate = new \DateTime('now');
					$em->persist($lead);
					if (($i % $batchSize) === 0) {
						$em->flush();
					}
					$i++;
				}
				
			}
			$em->flush();
			
			return $count;
		}
		
		return false;
	}
	
	public function manageLead($getValues, $status, $userId, $em)
	{
		if (isset($getValues)){
	
			if($status=='Delete'){
				$id=$getValues['id'];
				$gid=$getValues['campid'];
				$leadRepo = $em->getRepository('modules\user\models\LeadDetail')->findOneBy(array('id' => $id, 'user' => $userId));
				$leadRepos = $em->getRepository('modules\user\models\Lead')->findOneBy(array('id' => $gid, 'user' => $userId));
				$leadRepos->count = $leadRepos->count - 1;
				
				$em->remove($leadRepo);
				$em->flush();
				
				$em->persist($leadRepos);
				$em->flush();
				
			}else if($status=='Edit'){
				$id=$getValues['id'];
				$name=$getValues['name'];
				$mobile=$getValues['mobile'];
				$leadRepo = $em->getRepository('modules\user\models\LeadDetail')->findOneBy(array('user' => $userId, 'id' => $id));
					
				if (isset($mobile)){					
					$leadRepo->name = $name;
					$leadRepo->mobile = $mobile;
					$em->persist($leadRepo);
					$em->flush();
				}
				
				
			}
	
			return $leadRepo;
		}
		return false;
	}
	
}
