<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\LeadDetailRepository")
 * @Table(name="leads_detail")
 * @author Vinayak
 *
 */
class LeadDetail
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="user_id", type="integer", nullable=false)
	 * @var string
	 */
	private $user;
	
	/**
	 * @Column(name="lead_id", type="integer", nullable=false)
	 * @var string
	 */
	private $lead;
	
	/**
	 * @Column(name="mobile", type="string", nullable=false)
	 * @var string
	 */
	private $mobile;
	
	/**
	 * @Column(name="name", type="string", nullable=true)
	 * @var string
	 */
	private $name;
	
	/**
	 * @Column(name="type", type="integer", nullable=false)
	 * @var integer
	 */
	private $type;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}