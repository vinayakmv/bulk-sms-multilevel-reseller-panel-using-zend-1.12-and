<?php
namespace modules\user\models;
use Smpp_Sms_SMSCountCalculator;
class LeadManager extends \Smpp_Doctrine_BaseManager
{
	public function createLead($userRepo, $type, $status, $leadName, $em)
	{
		if (isset($em)) {			
			$lead = new Lead();
			$lead->name = $leadName;
			$lead->user = $userRepo;
			$lead->count = '0';
			$lead->type = $type;
			$lead->status = $status;
			$lead->createdDate = new \DateTime('now');
			$lead->updatedDate = new \DateTime('now');
			$em->persist($lead);
			$em->flush();
			
			return $lead;
		}
		return false;
		
	}
	
	public function manageLead($getValues, $status, $userId, $em)
	{
		if (isset($getValues)){	
				
			if($status=='Delete'){
				$id=$getValues['id'];
				$leadRepo = $em->getRepository('modules\user\models\Lead')->findOneBy(array('id' => $id, 'user' => $userId));
				$leadDetailRepos = $em->getRepository('modules\user\models\LeadDetail')->findBy(array('lead' => $id, 'user' => $userId));
				$em->remove($leadRepo);
				$em->flush();
				$query = $em->createQuery("DELETE modules\user\models\LeadDetail c WHERE c.lead = '".$id."' AND c.user = '".$userId."'");
				$query->execute();
			}else if($status=='Edit'){
				$id=$getValues['id'];
				$name=$getValues['name'];
				$leadRepo = $em->getRepository('modules\user\models\Lead')->findOneBy(array('user' => $userId, 'name' => $name));
				if (!$leadRepo){
					$leadRepo = $em->getRepository('modules\user\models\Lead')->findOneBy(array('id' => $id, 'user' => $userId));
					$leadRepo->name = $name;
					$em->persist($leadRepo);
					$em->flush();
				}
			}
	
			return $leadRepo;
		}
		return false;
	}
	
	//Use of function $contacts = array_map(array($this, 'trimToTen'),$contacts);
	public function trimToTen($num)
	{
		if ((strlen($num) >= '10') && (strlen($num) <= '12')){
			return substr($num, -10);
		}else{
			return $num;
		}
		
	}
	
	public function createGroupContacts($contacts, $groupId, $contactType, $userRepo, $em){
		
		if (isset($em)){
			if ($contactType == '2'){
				$leadPath = '../contactFiles/';
				$csv = $leadPath.$contacts;
				if ($contacts) {
					if (($handle = fopen("$csv", "r")) !== FALSE) {
						while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
							if (isset($data[1])){
								$contactDetails[] = $data[0].','.$data[1];
							}else{
								$contactDetails[] = $data[0];
							}
							
						}
					}
					fclose("$handle");
					$leadDetailManager = new LeadDetailManager();
					$count = $leadDetailManager->createLead($contactDetails, $userRepo->id, '1', '1', $groupId, $em);
						
					return $count;
				}
			}elseif ($contactType == '3'){
				$contacts = trim($contacts);
				if ($contacts) {			
					$cont = preg_split('/\r\n|[\r\n]/', $contacts);
					if ($cont == false){
						$cont = explode("\n", $contacts);
					}
					if ($cont == false){
						$cont = explode("\r", $contacts);
					}
					$conts = array_map('trim',$cont);
					
					
					$leadDetailManager = new LeadDetailManager();
					$count = $leadDetailManager->createLead($conts, $userRepo->id, '1', '1', $groupId, $em);
					
					return $count;
				}
			}
		}
		
		
		return false;
	}
	
	public function createGroup($contactValues, $userId, $em)
	{
		if (isset($em)){
			$userRepo = $em->getRepository('modules\user\models\User')->find($userId);
			$contactName = $contactValues['contactName'];
			$contactType = $contactValues['contactType'];
			
			if ($contactType == '2') {
				//Upload Contact
				$contactName = $contactValues['contactName'];
				$uploadContact = $contactValues['uploadContact'];
				//$saveContact = $contactValues['saveContact'];
				if (!$uploadContact) {
					//return false;
					return array('Contact' => array('uploadContact' => 'failed'));
				}
			
				$leadManager = new LeadManager();
				$createGroup = $leadManager->createLead($userRepo, '1', '1', $contactName, $em);
				if ($createGroup){
					$count = $leadManager->createGroupContacts($uploadContact, $createGroup->id, $contactType, $userRepo, $em);
					if ($count){
						$createGroup->count = $count;
						$em->persist($createGroup);
						$em->flush();
					}
				}
				
				if ($count) {
					return array('Contact' => array('success' => 'contacts group created'));
				}
				
					
			}elseif ($contactType == '3') {
				//Text Contact
				$contactName = $contactValues['contactName'];
				$textContact = $contactValues['textContact'];
				//$saveContact = $contactValues['saveContact'];
				
				$leadManager = new LeadManager();
				$createGroup = $leadManager->createLead($userRepo, '1', '1', $contactName, $em);
				if ($createGroup){
					$count = $leadManager->createGroupContacts($textContact, $createGroup->id, $contactType, $userRepo, $em);
					if ($count){
						$createGroup->count = $count;
						$em->persist($createGroup);
						$em->flush();
					}
				}
				
				if ($count) {
					return array('Contact' => array('success' => 'contacts group created'));
				}
			}
		}
		return false;
	}
	
	public function updateGroup($contactValues, $userId, $em)
	{
		if (isset($em)){
			$userRepo = $em->getRepository('modules\user\models\User')->find($userId);
			$contactType = $contactValues['contactType'];
				
			if ($contactType == '2') {
				//Upload Contact
				$uploadContact = $contactValues['uploadContact'];
				//$saveContact = $contactValues['saveContact'];
				if (!$uploadContact) {
					//return false;
					return array('Contact' => array('uploadContact' => 'failed'));
				}
				$leadRepo = $em->getRepository('modules\user\models\Lead')->findOneBy(array('user' => $userId, 'name' => $contactValues['contactName']));
				
				if ($leadRepo){
					$leadManager = new LeadManager();
					$count = $leadManager->createGroupContacts($uploadContact, $leadRepo->id, $contactType, $userRepo, $em);
					if ($count){
						$leadRepo->count = $leadRepo->count+$count;
						$em->persist($leadRepo);
						$em->flush();
					}
				}
	
				if ($count) {
					return array('Contact' => array('success' => 'contacts group updated'));
				}
	
					
			}elseif ($contactType == '3') {
				//Text Contact
				$textContact = $contactValues['textContact'];
				//$saveContact = $contactValues['saveContact'];
	
				$leadRepo = $em->getRepository('modules\user\models\Lead')->findOneBy(array('user' => $userId, 'name' => $contactValues['contactName']));
				
				if ($leadRepo){
					$leadManager = new LeadManager();
					$count = $leadManager->createGroupContacts($textContact, $leadRepo->id, $contactType, $userRepo, $em);
					if ($count){
						$leadRepo->count = $leadRepo->count+$count;
						$em->persist($leadRepo);
						$em->flush();
					}
				}
	
				if ($count) {
					return array('Contact' => array('success' => 'contacts group updated'));
				}
			}
		}
		return false;
	}
	
	
	public function createArrayCsv($contacts, $csvName)
	{
		$leadPath = '../contactFiles/';
		$file = $leadPath.$csvName;
		if ($contacts) {
			$contacts = array_map('trim',$contacts);
			
			$fp = fopen("$file", 'w');
			foreach($contacts as $line){
				$val = explode(",",$line);
				fputcsv($fp, $val);
			}
			fclose($fp);
	
			return $csvName;
		}
		return false;
	}
	
	public function createCsv($contacts, $csvName)
	{
		$leadPath = '../contactFiles/';
		$file = $leadPath.$csvName;
		if ($contacts) {			
			$cont = preg_split('/\r\n|[\r\n]/', $contacts);
			if ($cont == false){
				$cont = explode("\n", $contacts);
			}
			if ($cont == false){
				$cont = explode("\r", $contacts);
			}
			$conts = array_map('trim',$cont);
			
			$fp = fopen("$file", 'w');
    		foreach($conts as $line){
             	$val = explode(",",$line);
             	//$mobile = str_replace('-', ',', $val['0']);
             	$mobile = explode("^",$val['0']);
             	//$val = array($mobile);
             	if (count($mobile) > 1){
             		$val = $mobile;
             	}
             	$array = array_map('urldecode', $val);
             	fputcsv($fp, $array);
    		}
    		fclose($fp);
    		
			return $csvName;
		}
		return false;		
	}
	
	public function renameCsv($oldName, $newName)
	{
		$leadPath = '../contactFiles/';
		$oldFile = $leadPath.$oldName;
		$newFile = $leadPath.$newName;
		$move = shell_exec("mv -f $oldFile $newFile");
		if (!$move) {
			return $newName;
		}
		
		return false;
	}
	
	public function countDynamicCsv($csvName, $route, $message, $coding, $em)
	{
		$leadPath = '../contactFiles/';
		$dynamicCsv = 'dynamic_'.$csvName;
		$file = $leadPath.$dynamicCsv;
		$csv = $leadPath.$csvName;
		$csvUp = 'up_'.$csvName;
		$up = $leadPath.'aw_'.$csvName;
		$tmp = $leadPath.'tmp_'.$csvName;
		$dnd = $leadPath.'dnd_'.$csvName;
		$dyTmp = $leadPath.'dy_'.$csvName;
		
		$cmd = 'awk -F, \'{$1=substr($1,length($1)-9)}1\' OFS=, '."$csv > $up";
		shell_exec($cmd);
		if ($route == '1' || $route == '3') {
			if (($handle = fopen("$up", "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					$numbers[] = $data[0];
				}
			}
			$contacts = $this->checkDND($numbers, $em);
			if ($contacts) {
				$dnd = $this->createCsv($contacts, $dnd);
				
			}else {
				shell_exec("echo '' > $dnd");
			}
			
		}else{
			$dnd = $up;
		}
		shell_exec("awk 'NF' $dnd | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' | awk -F',' 'length($1)==10' > $tmp");
		//shell_exec("awk -F',' '!seen[$1]++' $dnd | awk 'NF' | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' | awk -F',' 'length($1)==10' > $tmp");
		//shell_exec("awk -F',' '!seen[$0]++' $csv | awk 'NF' | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' > $tmp");
		//shell_exec("awk 'NF' $csv > $tmp && mv $tmp $csv");
		
		shell_exec("awk -F, 'NR==FNR{seen[$1]++;next} ($1 in seen)' $tmp $up > $csv");
		//shell_exec("awk -F',' '!seen[$1]++' $dyTmp > $csv");
		shell_exec("rm -f $tmp $up $dnd");
		
		$fp = fopen("$file", 'w');
		//fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
		$columns = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$pattern = array();
		$replacement = array();
		$dynamic = array();
		$credit = 0;
		$counter = 0;
		if (($handle = fopen("$csv", "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				foreach ($data as $key=>$value){
					$column = $columns[$key];
					$pattern[$key] = '/\['.$column.'\]/';
					$replacement[$key] = $value;
				}
				ksort($pattern);
				ksort($replacement);
				$msg = preg_replace($pattern, $replacement, $message);
				
				$smsCountCalculator = new Smpp_Sms_SMSCountCalculator();
				//Santize to GSM7
				$orgText = $msg;
				if ($coding == 'GSM_7BIT' || $coding == 'GSM_7BIT_EX'){
					$msg = $smsCountCalculator->sanitizeToGSM($msg);
				}
				//Count Calculation
				$smsCounter = $smsCountCalculator->count($msg);
				$smsCount =  $smsCounter->messages;
				$smsLength =  $smsCounter->length;
				$smsEncoding =  $smsCounter->encoding;
				
				$dynamic[0] = $data[0];
				$dynamic[1] = $msg;
				$dynamic[2] = $smsCount;
				$dynamic[3] = $smsLength;
				$dynamic[4] = $smsEncoding;
				$credit = $credit+$smsCount;
				
				fputcsv($fp, $dynamic);	
				$counter++;
				
			}
		}
		fclose($fp);
		shell_exec("mv $file $csv");
		
		$fp = file($csv, FILE_SKIP_EMPTY_LINES);
		if ($fp){
			$count = count($fp);
			return array('count' => $counter, 'credit' => $credit);
		}
		return false;
	}
	
	public function countCsv($csvName, $route, $em)
	{
		$leadPath = '../contactFiles/';
		$csv = $leadPath.$csvName;
		$tmp = $leadPath.'tmp_'.$csvName;
		$numbers = array();
		/*
		shell_exec("/usr/bin/dos2unix $csv $tmp");
		
		if ($route == '4'){
			shell_exec("/bin/gawk --posix '$1 ~ /^[0-9]{9}$/' $tmp > $csv");
		}else{
			shell_exec("/bin/gawk --posix '$1 ~ /^[0-9]{10}$/' $tmp > $csv");
		}		
		*/
		if ($route == '1' || $route == '3') {
			if (($handle = fopen("$csv", "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					
					if ((strlen($data[0]) >= '10') && (strlen($data[0]) <= '12')){
						$numbers[] = substr($data[0], -10);
					}else{
						$numbers[] = $data[0];
					}
					
				}
			}
			$contacts = $this->checkDND($numbers, $em);
			if ($contacts) {
				$csv = $this->createCsv($contacts, $csvName);
				$csv = $leadPath.$csvName;
			}else {
				shell_exec("echo '' > $csv");
			}
			
		}else{
			if (($handle = fopen("$csv", "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					if ($route == '4'){
						if ((strlen($data[0]) >= '9') && (strlen($data[0]) <= '12')){
							$numbers[] = substr($data[0], -9);
						}else{
							$numbers[] = $data[0];
						}
					}else{
						if ((strlen($data[0]) >= '10') && (strlen($data[0]) <= '12')){
							$numbers[] = substr($data[0], -10);
						}else{
							$numbers[] = $data[0];
						}
					}	
					
						
				}
			}
			$numbers = implode("\r\n", $numbers);
			$csv = $this->createCsv($numbers, $csvName);
			$csv = $leadPath.$csvName;
		}
		if ($route == '4'){
			shell_exec("awk -F',' '!seen[$0]++' $csv | gawk --posix '$1 ~ /^[0-9]{9}$/' | awk 'NF' | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' | awk -F',' 'length($1)==9' > $tmp && mv $tmp $csv");
			//shell_exec("awk -F',' '!seen[$0]++' $csv | awk 'NF' | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' > $tmp");
			//shell_exec("awk 'NF' $csv > $tmp && mv $tmp $csv");
			
		}else{
			shell_exec("awk -F',' '!seen[$0]++' $csv | gawk --posix '$1 ~ /^[0-9]{10}$/' | awk 'NF' | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' | awk -F',' 'length($1)==10' > $tmp && mv $tmp $csv");
			//shell_exec("awk -F',' '!seen[$0]++' $csv | awk 'NF' | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' > $tmp");
			//shell_exec("awk 'NF' $csv > $tmp && mv $tmp $csv");
			
		}
		$fp = file($csv, FILE_SKIP_EMPTY_LINES);
		if ($fp){
			$count = count($fp);
			return $count;
		}	
		return false;
	}

	public function checkDND($numbers, $em) 
	{	
		$dndRepo = $em->getRepository("modules\smpp\models\Dnd");
		$dndNumbers = $dndRepo->getDnd($numbers);		
		$nonDnd = $this->flip_isset_diff($numbers, $dndNumbers);
		$nonDnd = implode("\r\n", $nonDnd);
		
		return $nonDnd;
	}
	
	public function flip_isset_diff($b, $a) {
		$at = array_flip($a);
		$d = array();
		foreach ($b as $i)
		if (!isset($at[$i]))
			$d[] = $i;
		return $d;
	}
	
}
