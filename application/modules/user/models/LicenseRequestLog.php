<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\LicenseRequestLogRepository")
 * @Table(name="license_request_logs")
 * @author Vinayak
 *
 */
class LicenseRequestLog
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	

	/**
	 * @OneToOne(targetEntity="User")
	 * @JoinColumn(name="user_id", referencedColumnName="id")
	 */
	private $user;
	
	/**
	 * @column(name="smpp_promo_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppPromoCredit;
	
	/**
	 * @column(name="smpp_trans_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppTransCredit;
	
	/**
	 * @column(name="smpp_scrub_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppScrubCredit;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
