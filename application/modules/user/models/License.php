<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\LicenseRepository")
 * @Table(name="licenses")
 * @author Vinayak
 *
 */
class License
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @column(name="username", type="string", nullable="false")
	 * @var string
	 */
	private $username;
	
	/**
	 * @column(name="company", type="text", nullable="false")
	 * @var text
	 */
	private $company;
	
	/**
	 * @column(name="server_ip", type="string", nullable="false")
	 * @var string
	 */
	private $serverIp;

	/**
	 * @column(name="machine_id", type="string", nullable="false")
	 * @var integer
	 */
	private $machineId;
		
	/**
	 * @Column(name="validity", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $validity;

	/**
	 * @column(name="status", type="integer", nullable="false")
	 * @var integer
	 */
	private $status;

	/**
	 * @column(name="created_by", type="integer", nullable="false")
	 * @var integer
	 */
	private $createdBy;

	/**
	 * @column(name="updated_by", type="integer", nullable="true")
	 * @var integer
	 */
	private $updatedBy;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}