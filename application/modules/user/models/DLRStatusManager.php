<?php
namespace modules\user\models;
/**
 * 
 * @author Ani
 *
 */
class DLRStatusManager  extends \Smpp_Doctrine_BaseManager
{
	public function insertDlrStatus($getval,$userId, $em)
	{
		if (isset($em)){
			$dlrStatus = new DLRStatus();
			$dlrStatus->errorCode = urlencode($getval['errorcode']);
			$dlrStatus->statusDLR = ucwords(strtolower($getval['dlrstatus']));
			$dlrStatus->status = '1';
			$dlrStatus->createdDate = new \DateTime('now');
			$dlrStatus->updatedDate = new \DateTime('now');
			$em->persist($dlrStatus);
			$em->flush();
				
			return $dlrStatus;
		}
		return false;
	}
}
