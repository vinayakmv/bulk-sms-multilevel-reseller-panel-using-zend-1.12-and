<?php
namespace modules\user\models;

class RoleManager extends \Smpp_Doctrine_BaseManager
{
	public function createRole($roleName, $em)
	{
		if (isset($roleName)) {
				//Create Role
				$role = new Role();
				$role->role = $roleName;
				$role->createdDate = new \DateTime('now');
				$role->updatedDate = new \DateTime('now');
				$em->persist($role);
				$em->flush();
				
				return $role;
		}
		return false;
	}
	
	public function assignPermission($roleValues, $em) 
	{
		if (isset($roleValues)) {
			$selectResource = $roleValues['my_multi_select3'];
			foreach ($selectResource as $key => $resourceId) {
				$resources[$key] = $em->getRepository('modules\user\models\Resource')->find($resourceId);
			}
			$roleType = $roleValues['roleType'];
			if ($roleType == '2') {
				//Select Role
				$roleId = $roleValues['selectRole'];
				$role = $em->getRepository('modules\user\models\Role')->find($roleId);
				$role->resources = $resources;
				$role->updatedDate = new \DateTime('now');
				$em->persist($role);
				$em->flush();
			}elseif ($roleType == '1') {
				//Create Role
				$roleName = $roleValues['roleName'];
				//$role = $this->createRole($roleName, $em);
				$role = new Role();
				$role->role = $roleName;
				$role->resources = $resources;
				$role->createdDate = new \DateTime('now');
				$role->updatedDate = new \DateTime('now');
				$em->persist($role);
				$em->flush();
			}
			return $role;
		}
		return false;
	}
	
	public function assignRole($formValues, $em)
	{
		if (isset($formValues)) {
			$selectUser = $formValues['my_multi_select3'];
    		$roleId = $formValues['selectRole'];
    		foreach ($selectUser as $key => $userId) {
    			$users[$key] = $em->getRepository('modules\user\models\User')->find($userId);
    		}
			$role = $em->getRepository('modules\user\models\Role')->find($roleId);
			foreach ($users as $user) {
				$user->role = $role;
				$user->updatedDate = new \DateTime('now');
				$em->persist($user);
				$em->flush();
			}
						
			return $user;
		}
		return false;		
	}
}