<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\UserRepository")
 * @Table(name="users")
 * @author Vinayak
 *
 */
class User
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
         * @Id
         * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @column(name="reseller_id", type="integer", nullable="false")
	 * @var integer
	 */
	private $reseller;
		
	/**
	 * @column(name="username", type="string", nullable="false")
	 * @var string
	 */
	private $username;
	
	/**
	 * @column(name="password", type="string", nullable="false")
	 * @var string
	 */
	private $password;
	
	/**
	 * @column(name="hash", type="string", nullable="false")
	 * @var string
	 */
	private $hash;
	
	/**
	 * @column(name="salt", type="string", nullable="false")
	 * @var string
	 */
	private $salt;
	
	/**
	 * @ManyToOne(targetEntity="Role", inversedBy="users", cascade={"persist"})
	 * @JoinColumn(name="role_id", referencedColumnName="id")
	 * @var integer
	 */
	private $role;
	
	/**
	 * @ManyToOne(targetEntity="Domain", inversedBy="users", cascade={"persist"})
	 * @JoinColumn(name="domain_id", referencedColumnName="id")
	 * @var integer
	 */
	private $domain;
	
	/**
	 * @column(name="parent_domain_id", type="integer", nullable="false")
	 * @var integer
	 */
	private $parentDomain;
	
	/**
	 * @column(name="sms_promo_api_route", type="integer", nullable="true")
	 * @var integer
	 */
	private $promoApiRoute;
	
	/**
	 * @column(name="sms_trans_api_route", type="integer", nullable="true")
	 * @var integer
	 */
	private $transApiRoute;
	

	/**
	 * @column(name="sms_premi_api_route", type="integer", nullable="true")
	 * @var integer
	 */
	private $premiApiRoute;
	
	/**
	 * @column(name="sms_scrub_api_route", type="integer", nullable="true")
	 * @var integer
	 */
	private $scrubApiRoute;
	
	/**
	 * @column(name="sms_inter_api_route", type="integer", nullable="true")
	 * @var integer
	 */
	private $interApiRoute;
	
	/**
	 * @column(name="voice_promo_api_route", type="integer", nullable="true")
	 * @var integer
	 */
	private $promoVoiceApiRoute;
	
	/**
	 * @column(name="voice_trans_api_route", type="integer", nullable="true")
	 * @var integer
	 */
	private $transVoiceApiRoute;

	/**
	 * @column(name="voice_premi_api_route", type="integer", nullable="true")
	 * @var integer
	 */
	private $premiVoiceApiRoute;
	
	/**
	 * @ManyToMany(targetEntity="SmsRoute", inversedBy="users", cascade={"persist", "remove"})
	 * @JoinTable(name="users_sms_routes")
	 * @var integer
	 */
	private $smsRoutes;
	
	/**
	 * @ManyToMany(targetEntity="VoiceRoute", inversedBy="users")
	 * @JoinTable(name="users_voice_routes")
	 * @var integer
	 */
	private $voiceRoutes;
	
	/**
	 * @ManyToMany(targetEntity="SmppRoute", inversedBy="users")
	 * @JoinTable(name="users_smpp_routes")
	 * @var integer
	 */
	private $smppRoutes;
	
	/**
	 * @column(name="status", type="integer", nullable="false")
	 * @var integer
	 */
	private $status;
	
	/**
	 * @Column(name="recredit", type="integer", nullable=false)
	 * @var integer
	 */
	private $recredit;
	
	/**
	 * @Column(name="recredit_date", type="date", nullable=true)
	 * @var integer
	 */
	private $recreditDate;
	
	/**
	 * @column(name="open_sender_id", type="boolean", nullable="true")
	 * @var integer
	 */
	private $openSenderId;
	
	/**
	 * @column(name="open_template", type="boolean", nullable="true")
	 * @var integer
	 */
	private $openTemplate;
	
	/**
	 * @column(name="check_spam", type="boolean", nullable="true")
	 * @var integer
	 */
	private $checkSpam;
	
	/**
	 * @column(name="sms_priority", type="integer", nullable="true")
	 * @var integer
	 */
	private $smsPriority;
	
	/**
	 * @column(name="voice_priority", type="integer", nullable="true")
	 * @var integer
	 */
	private $voicePriority;
	
	/**
	 * @Column(name="validity", type="integer", nullable=true)
	 * @var integer
	 */
	private $validity;
	
	/**
	 * @column(name="sms_percentage_promo", type="integer", nullable="true")
	 * @var integer
	 */
	private $smsPercentagePromo;
	
	/**
	 * @column(name="sms_percentage_scrub", type="integer", nullable="true")
	 * @var integer
	 */
	private $smsPercentageScrub;

	/**
	 * @column(name="sms_percentage_trans", type="integer", nullable="true")
	 * @var integer
	 */
	private $smsPercentageTrans;
	

	/**
	 * @column(name="sms_percentage_premi", type="integer", nullable="true")
	 * @var integer
	 */
	private $smsPercentagePremi;
	
	/**
	 * @column(name="sms_percentage_inter", type="integer", nullable="true")
	 * @var integer
	 */
	private $smsPercentageInter;
	
	/**
	 * @column(name="voice_percentage_promo", type="integer", nullable="true")
	 * @var integer
	 */
	private $voicePercentagePromo;
	
	/**
	 * @column(name="voice_percentage_trans", type="integer", nullable="true")
	 * @var integer
	 */
	private $voicePercentageTrans;

	/**
	 * @column(name="voice_percentage_premi", type="integer", nullable="true")
	 * @var integer
	 */
	private $voicePercentagePremi;
	
	/**
	 * @column(name="activate_token", type="string", nullable="true")
	 * @var string
	 */
	private $activate;
	
	/**
	 * @column(name="reset_token", type="string", nullable="true")
	 * @var string
	 */
	private $reset;
	
	/**
	 * @OneToOne(targetEntity="Account", cascade={"remove","persist"})
	 * @JoinColumn(name="account_id", referencedColumnName="id")
	 */
	private $account;
	
	/**
	 * @OneToOne(targetEntity="UserProfile", cascade={"remove","persist"})
	 * @JoinColumn(name="profile_id", referencedColumnName="id")
	 */
	private $profile;
		
	/**
	 * @OneToMany(targetEntity="AccountLog", mappedBy="user", cascade={"remove","persist"})
	 * @var integer
	 */
	private $accountLogs;
	
	/**
	 * @OneToMany(targetEntity="ActivityLog", mappedBy="user", cascade={"remove","persist"})
	 * @var integer
	 */
	private $activityLogs;

	/**
	 * @Column(name="live_date", type="datetime", nullable=true)
	 * @var datetime
	 */
	private $liveDate;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
