<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\LeadRepository")
 * @Table(name="leads")
 * @author Vinayak
 *
 */
class Lead
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer
	 */
	private $id;
	
	/**
	 * @ManyToOne(targetEntity="User", inversedBy="leads")
	 * @JoinColumn(name="user_id", referencedColumnName="id")
	 * @var integer
	 */
	private $user;
	
	/**
	 * @Column(name="name", type="string", nullable=false)
	 * @var string
	 */
	private $name;
	
	/**
	 * @Column(name="status", type="integer", nullable=false)
	 * @var integer
	 */
	private $status;
	
	/**
	 * @Column(name="type", type="integer", nullable=false)
	 * @var integer
	 */
	private $type;
	
	/**
	 * @Column(name="count", type="integer", nullable=false)
	 * @var integer
	 */
	private $count;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}