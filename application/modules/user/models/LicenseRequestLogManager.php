<?php
namespace modules\user\models;

class LicenseRequestLogManager extends \Smpp_Doctrine_BaseManager
{
	public function createLog($userRepo, $resellerRepo, $amount, $tobalance, $userBalance, $mode, $type, $em)
	{
		if (isset($em)){
			$account = new AccountLog();
			$account->user = $resellerRepo;
			$account->amount = $amount;
			$account->username = $userRepo->username;
			$account->userBalance = $userBalance;
			$account->toBalance = $tobalance;
			$account->mode = $mode;
			$account->type = $type;
			$account->createdDate = new \DateTime('now');
			$account->updatedDate = new \DateTime('now');
			$em->persist($account);
			$em->flush();
				
			return $account;
		}
		return false;
	}
}