<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\DispositionStatusRepository")
 * @Table(name="disposition_status",indexes={
 * @Index(name="search_time", columns={"status"})
 * })
 * @author Vinayak
 *
 */
class DispositionStatus
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;

		
	/**
	 * @Column(name="error_code", type="string", nullable=false)
	 * @var string
	 */
	private $errorCode;
		
	/**
	 * @Column(name="status_dlr", type="string", nullable=false)
	 * @var string
	 */
	private $statusDLR;

	/**
	 * @Column(name="status", type="integer", nullable=false)
	 * @var integer
	 */
	private $status;
	
	/**
	 * @column(name="created_date", type="datetime", nullable="false")
	 * @var datetime
	 */	 
	private $createdDate;

	/**
	 * @column(name="updated_date", type="datetime", nullable="true")
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}