<?php
namespace modules\user\models;
/**
 * 
 * @author Vinayak
 *
 */
class DispositionStatusManager  extends \Smpp_Doctrine_BaseManager
{
	public function insertDlrStatus($getval,$userId, $em)
	{
		if (isset($em)){
			$dlrStatus = new DispositionStatus();
			$dlrStatus->errorCode = $getval['errorcode'];
			$dlrStatus->statusDLR = ucwords(strtolower($getval['dlrstatus']));
			$dlrStatus->status = '1';
			$dlrStatus->createdDate = new \DateTime('now');
			$dlrStatus->updatedDate = new \DateTime('now');
			$em->persist($dlrStatus);
			$em->flush();
				
			return $dlrStatus;
		}
		return false;
	}
}
