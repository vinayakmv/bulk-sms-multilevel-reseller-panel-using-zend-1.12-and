<?php
namespace modules\user\models;
use modules\user\models\AccountManager;
use modules\sms\models\SmsSenderIdManager;
use modules\voice\models\VoiceSenderIdManager;
/**
 * 
 * @author Vinayak
 *
 */
class UserManager extends \Smpp_Doctrine_BaseManager
{
	public function createUser($userValues, $mode, $em, $emSms, $emVoice)
	{
		if (isset($em)){
			//print_r($userValues); exit();
			$crypt = new \Smpp_Auth_Crypt();
			$domain = \Zend_Registry::get('fqdn');
			$salt = $crypt->generateSalt();
			$token = $crypt->generateToken();
			$username = trim($userValues['username']);
			$email = trim($userValues['email']);
			$name = $userValues['name'];
			$mobile = $userValues['mobile'];
			$country = $userValues['country'];
			$password = $crypt->hashPassword($userValues['password'], $salt);
			$domainRepo = $em->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => $domain));
			$role = $domainRepo->userRole;
			if ($domainRepo->level == '0'){
				$roleRepo = $em->getRepository('modules\user\models\Role')->findOneBy(array('role' => 'L1-USER'));
			}else{
				$roleRepo = $em->getRepository('modules\user\models\Role')->find($role);
			}
			
			
			$accountManager = new AccountManager();
			$accountRepo = $accountManager->createAccount($em);
			$accountRepo->smppTps = $domainRepo->smppTps;
			$accountRepo->smppBind = $domainRepo->smppBind;
			$accountRepo->smppUser = $domainRepo->smppUser;
			$em->persist($accountRepo);
			$em->flush();
			
			$userProfile = new UserProfile();
			$userProfile->email = $email;
			$userProfile->name = $name;
			$userProfile->mobile = $mobile;
			$userProfile->country = $country;
			$userProfile->createdDate = new \DateTime('now');
			$userProfile->updatedDate = new \DateTime('now');
			$em->persist($userProfile);
			$em->flush();
			
			$user = new User();
			$user->username = $username;
			$user->password = $password;
			$user->hash = $userValues['password'];
			$user->domain = $domainRepo;
			$user->reseller = $domainRepo->owner;
			$user->role = $roleRepo;
			$user->recredit = $domainRepo->recredit;
			$user->recreditDate = new \DateTime('now');
			$user->parentDomain = $domainRepo->parentDomain;
			$user->smsPercentagePromo = $domainRepo->smsPercentagePromo; 
			$user->smsPercentageTrans = $domainRepo->smsPercentageTrans;
			$user->smsPercentagePremi = $domainRepo->smsPercentagePremi;
			$user->smsPercentageScrub = $domainRepo->smsPercentageScrub;
			$user->voicePercentagePromo = $domainRepo->voicePercentagePromo;
			$user->voicePercentageTrans = $domainRepo->voicePercentageTrans;
			$user->voicePercentagePremi = $domainRepo->voicePercentagePremi;
			$user->validity = $domainRepo->validity;
			$user->account = $accountRepo;
			$user->profile = $userProfile;
			if ($mode == 'RESELLER' || $mode == 'L1-RESELLER' || $mode == 'P1-RESELLER' || $mode == 'ADMIN'){
				$user->status = '1';
			}else{
				$user->status = '2';
			}
			$user->openSenderId = '0';
			$user->openTemplate = '1';
			$user->smsPriority = '0';
			$user->voicePriority = '0';
			$user->salt = $salt;
			if ($mode == 'RESELLER' || $mode == 'L1-RESELLER' || $mode == 'P1-RESELLER' || $mode == 'ADMIN'){
				$user->activate = '';
			}else{
				$user->activate = $token;
			}			
			$user->reset = $token;
			$user->liveDate = new \DateTime('now');
			$user->createdDate = new \DateTime('now');
			$user->updatedDate = new \DateTime('now');
			$em->persist($user);
			$em->flush();
			
			$smsSenderManager = new SmsSenderIdManager();
			$smsSenderManager->createSenderId($user->id, 'TXTHUB', '2', $emSms);
			
			
			$voiceSenderManager = new VoiceSenderIdManager();
			$voiceSenderManager->createSenderId($user->id, '9999999999', '1', $emVoice);
			
						
			return $user;
						
		}
		return false;
	}
	
	/**
	 * 
	 * @param unknown $userValue
	 * @param unknown $em
	 */
	public function createReseller($resellerValues, $mode, $em, $emSms, $emVoice)
	{
		if (isset($resellerValues)){
			
			$domainName = \Smpp_Utility::getFqdn($resellerValues['domain']);
			$domain = \Zend_Registry::get('fqdn');
			$username = trim($resellerValues['username']);
			$email = trim($resellerValues['email']);
			$name = $resellerValues['name'];
			$mobile = $resellerValues['mobile'];
			$country = $resellerValues['country'];
			$recredit = '0';
			$crypt = new \Smpp_Auth_Crypt();
			$salt = $crypt->generateSalt();
			$token = $crypt->generateToken();
			$password = $crypt->hashPassword($resellerValues['password'], $salt);
			
			if ($resellerValues['theme'] == '1'){
				$layout = 'boardster';
			}elseif ($resellerValues['theme'] == '2'){
				$layout = 'adminto';
			}elseif ($resellerValues['theme'] == '3'){
				$layout = 'flatlab';
			}
			//Create Domain
			$domainManager = new DomainManager();
			$domainRepo = $domainManager->createDomain($domainName, $em);
			$resellerDomainRepo = $em->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => $domain));
			if (isset($domainRepo)){
				$role = $resellerDomainRepo->resellerRole;
				if ($resellerDomainRepo->level == '0'){
					$roleRepo = $em->getRepository('modules\user\models\Role')->findOneBy(array('role' => 'L1-RESELLER'));
				}else{
					$roleRepo = $em->getRepository('modules\user\models\Role')->find($role);
				}
				
				$accountManager = new AccountManager();
				$accountRepo = $accountManager->createAccount($em);
				$accountRepo->smppTps = $resellerValues['tps'];
				$accountRepo->smppBind = $resellerValues['bind'];
				$accountRepo->smppUser = $resellerValues['smppUser'];
				$em->persist($accountRepo);
				$em->flush();
				
				$userProfile = new UserProfile();
				$userProfile->email = $email;
				$userProfile->name = $name;
				$userProfile->mobile = $mobile;
				$userProfile->country = $country;
				$userProfile->createdDate = new \DateTime('now');
				$em->persist($userProfile);
				$em->flush();
				
				//Create Reseller	
				$user = new User();
				$user->username = $username;
				$user->password = $password;
				$user->hash = $resellerValues['password'];
				$user->domain = $domainRepo;
				$user->reseller = $resellerDomainRepo->owner;
				$user->role = $roleRepo;
				$user->recredit = $resellerDomainRepo->recredit;
				$user->recreditDate = new \DateTime('now');
				$user->parentDomain = $resellerDomainRepo->parentDomain;
				$user->smsPercentagePromo = $resellerDomainRepo->smsPercentagePromo;
				$user->smsPercentageTrans = $resellerDomainRepo->smsPercentageTrans;
				$user->smsPercentagePremi = $resellerDomainRepo->smsPercentagePremi;
				$user->smsPercentageScrub = $resellerDomainRepo->smsPercentageScrub;
				$user->voicePercentagePromo = $resellerDomainRepo->voicePercentagePromo;
				$user->voicePercentageTrans = $resellerDomainRepo->voicePercentageTrans;
				$user->voicePercentagePremi = $resellerDomainRepo->voicePercentagePremi;
				$user->validity = $resellerDomainRepo->validity;
				$user->account = $accountRepo;
				$user->profile = $userProfile;
				if ($mode == 'RESELLER' || $mode == 'L1-RESELLER' || $mode == 'P1-RESELLER' || $mode == 'ADMIN'){
					$user->status = '1';
				}else{
					$user->status = '2';
				}
				
				$user->openSenderId = '0';
				$user->openTemplate = '1';
				$user->smsPriority = '0';
				$user->voicePriority = '0';
				$user->salt = $salt;
				if ($mode == 'RESELLER' || $mode == 'L1-RESELLER' || $mode == 'P1-RESELLER' || $mode == 'ADMIN'){
					$user->activate = '';
				}else{
					$user->activate = $token;
				}
				$user->reset = $token;
				$user->liveDate = new \DateTime('now');
				$user->createdDate = new \DateTime('now');
				$user->updatedDate = new \DateTime('now');
				$em->persist($user);
				$em->flush();
				
				//Assign Domain Ownership
				$domainRepo->owner = $user->id;
				$domainRepo->layout = $layout;
				$domainRepo->reseller = $resellerDomainRepo->owner;
				$domainRepo->resellerRole = $resellerDomainRepo->resellerRole;
				$domainRepo->userRole = $resellerDomainRepo->userRole;
				$domainRepo->level = ($resellerDomainRepo->level + 1);
				if ($resellerDomainRepo->level == '0'){
					$domainRepo->recredit = $recredit;
				}else{
					$domainRepo->recredit = $resellerDomainRepo->recredit;
				}
				
				$domainRepo->smsPercentagePromo = $resellerDomainRepo->smsPercentagePromo;
				$domainRepo->smsPercentageTrans = $resellerDomainRepo->smsPercentageTrans;
				$domainRepo->smsPercentagePremi = $resellerDomainRepo->smsPercentagePremi;
				$domainRepo->voicePercentagePromo  = $resellerDomainRepo->voicePercentagePromo;
				$domainRepo->voicePercentageTrans  = $resellerDomainRepo->voicePercentageTrans;
				$domainRepo->voicePercentagePremi  = $resellerDomainRepo->voicePercentagePremi;
				$domainRepo->validity = $resellerDomainRepo->validity;
				$domainRepo->smppTps = $resellerValues['tps'];
				$domainRepo->smppBind = $resellerValues['bind'];
				$domainRepo->smppUser = $resellerValues['smppUser'];
				$domainRepo->transRoute  = $resellerDomainRepo->transRoute;
				$domainRepo->premiRoute  = $resellerDomainRepo->premiRoute;
				$domainRepo->promoRoute  = $resellerDomainRepo->promoRoute;
				$domainRepo->scrubRoute  = $resellerDomainRepo->scrubRoute;
				$domainRepo->smppTransRoute  = $resellerDomainRepo->smppTransRoute;
				$domainRepo->smppPremiRoute  = $resellerDomainRepo->smppPremiRoute;
				$domainRepo->smppPromoRoute  = $resellerDomainRepo->smppPromoRoute;
				$domainRepo->smppScrubRoute  = $resellerDomainRepo->smppScrubRoute;
				$em->persist($domainRepo);
				$em->flush();
				
				if ($resellerDomainRepo->level == '0'){
					$domainRepo->parentDomain = $domainRepo->id;					
				}else {
					$domainRepo->parentDomain = $resellerDomainRepo->parentDomain;					
				}

				$em->persist($domainRepo);
				$em->flush();
				
				$user->parentDomain = $domainRepo->parentDomain;
				$em->persist($user);
				$em->flush();
				
				
				$smsSenderManager = new SmsSenderIdManager();
				$smsSenderManager->createSenderId($user->id, 'MSGHUB', '2', $emSms);
				
				
				$voiceSenderManager = new VoiceSenderIdManager();
				$voiceSenderManager->createSenderId($user->id, '9999999999', '1', $emVoice);
				
				
				return $user;
			}
			
		}
		return false;
	}

	/**
	 * 
	 * @param unknown $id
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function blockUser($userId, $resellerId, $em)
	{
		if (isset($userId)){
			$user = $em->getRepository('modules\user\models\User')->findOneBy(array('id' => "$userId", 'reseller' => "$resellerId"));
			$user->status = '3';
			$em->persist($user);
			$em->flush();
				
			return $user;
		}
		return false;
	}
	
	/**
	 * 
	 * @param unknown $user
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function unblockUser($userId, $resellerId, $em)
	{
		if (isset($userId)){
			$user = $em->getRepository('modules\user\models\User')->findOneBy(array('id' => "$userId", 'reseller' => "$resellerId"));
			$user->status = '1';
			$em->persist($user);
			$em->flush();
		
			return $user;
		}
		return false;
	}
	
	public function ResellerActivateUser($userId, $resellerId, $em)
	{
		if (isset($userId)){
			$user = $em->getRepository('modules\user\models\User')->findOneBy(array('id' => "$userId", 'reseller' => "$resellerId"));
			$user->status = '1';
			$user->activate = '';
			$user->updatedDate = new \DateTime('now');
			$em->persist($user);
			$em->flush();
		
			return $user;
		}
		return false;
	}

	public function activateUser($token, $em)
	{
		$users = $em->getRepository('modules\user\models\User')->findBy(array('activate' => $token));
		foreach ($users as $user) {
			if ($user) {
				$user->status = '1';
				$user->activate = '';
				$user->updatedDate = new \DateTime('now');
				$em->persist($user);
				$em->flush();
					
				return $user;
			}
		}
		
		return false;
	}
	
	public function resetUser($token, $em)
	{
		$users = $em->getRepository('modules\user\models\User')->findBy(array('reset' => $token));
		foreach ($users as $user) {
			if ($user) {
				$user->reset = '';
				$user->updatedDate = new \DateTime('now');
				$em->persist($user);
				$em->flush();
					
				return $user;
			}
		}
	
		return false;
	}
	
	public function getCredit($userId, $accountType, $em)
	{
		if (isset($userId)){
			$user = $em->getRepository('modules\user\models\User')->find($userId);
			$voiceCredit = $user->account->voiceCredit; 
			
			switch ($accountType) {
				case 1:
					return $voiceCredit;
					break;
			}
			return $user;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $id
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function blockallUser($userId, $em)
	{
		if (isset($userId)){
			$user = $em->getRepository('modules\user\models\User')->findOneBy(array('id' => "$userId"));
			$user->status = '3';
			$em->persist($user);
			$em->flush();
	
			return $user;
		}
		return false;
	}
	
	public function unblockallUser($userId, $em)
	{
		if (isset($userId)){
			$user = $em->getRepository('modules\user\models\User')->findOneBy(array('id' => "$userId"));
			$user->status = '1';
			$em->persist($user);
			$em->flush();
	
			return $user;
		}
		return false;
	}
	
	public function ResellerActivateallUser($userId, $em)
	{
		if (isset($userId)){
			$user = $em->getRepository('modules\user\models\User')->findOneBy(array('id' => "$userId"));
			$user->status = '1';
			$user->activate = '';
			$user->updatedDate = new \DateTime('now');
			$em->persist($user);
			$em->flush();
	
			return $user;
		}
		return false;
	}
	
	public function updatePassword($userId, $newPassword, $em)
	{
		if (isset($userId)){
			
			$crypt = new \Smpp_Auth_Crypt();
			$salt = $crypt->generateSalt();
			$userRepo = $em->getRepository('modules\user\models\User')->find($userId);
			$password = $crypt->hashPassword($newPassword, $salt);
			
			$userRepo->password = $password;
			$userRepo->hash = $newPassword;
			$userRepo->salt = $salt;
			$userRepo->updatedDate = new \DateTime('now');
			$em->persist($userRepo);
			$em->flush();
			
			return $userRepo;
						
		}
		return false;
	}
	
	public function deleteUser($userId, $em)
	{
		if (isset($userId)){
			$userRepo = $em->getRepository('modules\user\models\User')->find($userId);
			
			$accountRepo = $em->getRepository('modules\user\models\Account')->find($userId->account);
			
			foreach ($userRepo->leads as $lead){
				if ($lead){
					$em->remove($lead);
					$em->flush();
				}
			}
			foreach ($userRepo->senderIds as $sendeid){
				if ($sendeid){
					$em->remove($sendeid);
					$em->flush();
				}				
			}			
			foreach ($userRepo->voiceFiles as $voiceFile){
				if ($voiceFile){
					$em->remove($voiceFile);
					$em->flush();
				}				
			}			
			foreach ($userRepo->voiceCampaigns as $voiceCampaign){
				if ($voiceCampaign){
					$em->remove($voiceCampaign);
					$em->flush();
				}				
			}
			foreach ($userRepo->smsCampaigns as $smsCampaign){
				if ($smsCampaign){
					$em->remove($smsCampaign);
					$em->flush();
				}				
			}			
			foreach ($userRepo->voiceReports as $voiceReport){
				if ($voiceReport){
					$em->remove($voiceReport);
					$em->flush();
				}				
			}
			foreach ($userRepo->smsReports as $smsReport){
				if ($smsReport){
					$em->remove($smsReport);
					$em->flush();
				}				
			}
			
			$em->remove($userRepo);
			$em->flush();						

			$em->remove($accountRepo);
			$em->flush();
			
			return $userRepo;
		}
		return false;
		
	}
	
	public function userRoute($mainType, $arrayUsername, $mode, $route, $type, $em){
		if (isset($arrayUsername)){	

			if ($mainType == 'SMS'){
				$transApiRoute = 'transApiRoute';
				$premiApiRoute = 'premiApiRoute';
				$promoApiRoute = 'promoApiRoute';
				$scrubApiRoute = 'scrubApiRoute';
				$interApiRoute = 'interApiRoute';

				$transRoute = 'transRoute';
				$premiRoute = 'premiRoute';
				$promoRoute = 'promoRoute';
				$scrubRoute = 'scrubRoute';
				$interRoute = 'interRoute';

			}elseif($mainType == 'VOICE'){
				$transApiRoute = 'transVoiceApiRoute';
				$premiApiRoute = 'premiVoiceApiRoute';
				$promoApiRoute = 'promoVoiceApiRoute';
				$scrubApiRoute = 'scrubVoiceApiRoute';
				$interApiRoute = 'interVoiceApiRoute';

				$transRoute = 'voiceTransRoute';
				$premiRoute = 'voicePremiRoute';
				$promoRoute = 'voicePromoRoute';
				$scrubRoute = 'voiceScrubRoute';
				$interRoute = 'voiceInterRoute';

			}elseif($mainType == 'SMPP'){
				$transRoute = 'smppTransRoute';
				$premiRoute = 'smppPremiRoute';
				$promoRoute = 'smppPromoRoute';
				$scrubRoute = 'smppScrubRoute';
				$interRoute = 'smppInterRoute';
			}

			$username = trim($arrayUsername['0']);
			$domain =  trim($arrayUsername['1']);
			$userDomainRepo = $em->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
			$userRepo = $em->getRepository('modules\user\models\User')->findOneBy(array('username' => "$username",'domain' => "$userDomainRepo->id"));
				if ($mode == '1'){
					if ($type == 'TRANSACTIONAL' && $mainType != 'SMPP'){
						$userRepo->$transApiRoute = $route;
					}elseif ($type == 'PREMIUM' && $mainType != 'SMPP'){
						$userRepo->$premiApiRoute = $route;
					}if ($type == 'PROMOTIONAL' && $mainType != 'SMPP'){
						$userRepo->$promoApiRoute = $route;
					}elseif ($type == 'TRANS-SCRUB' && $mainType != 'SMPP'){
						$userRepo->$scrubApiRoute = $route;
					}elseif ($type == 'INTERNATIONAL' && $mainType != 'SMPP'){
						$userRepo->$interApiRoute = $route;
					}
						
					$em->persist($userRepo);
					$em->flush();
					
						if ($mainType == 'SMS'){
							$smsRouteRepo = $em->getRepository('modules\user\models\SmsRoute')->findBy(array('type'=>"$type"));
							foreach ($smsRouteRepo as $smsRoute){
								$sql="DELETE FROM `users_sms_routes` WHERE `user_id` = '$userRepo->id' AND smsroute_id = '$smsRoute->id'";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();
							}
							$sql="INSERT INTO `users_sms_routes` (`user_id`,`smsroute_id`) VALUES ('$userRepo->id', '$route')";
							$stmt = $em->getConnection()->prepare($sql);
							$stmt->execute();
						}elseif($mainType == 'VOICE'){
							$voiceRouteRepo = $em->getRepository('modules\user\models\VoiceRoute')->findBy(array('type'=>"$type"));
							foreach ($voiceRouteRepo as $voiceRoute){
								$sql="DELETE FROM `users_voice_routes` WHERE `user_id` = '$userRepo->id' AND voiceroute_id = '$voiceRoute->id'";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();
							}
							$sql="INSERT INTO `users_voice_routes` (`user_id`,`voiceroute_id`) VALUES ('$userRepo->id', '$route')";
							$stmt = $em->getConnection()->prepare($sql);
							$stmt->execute();
						}elseif($mainType == 'SMPP'){
							$smppRouteRepo = $em->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>"$type"));
							foreach ($smppRouteRepo as $smppRoute){
								$sql="DELETE FROM `users_smpp_routes` WHERE `user_id` = '$userRepo->id' AND smpproute_id = '$smppRoute->id'";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();
							}				
							$sql="INSERT INTO `users_smpp_routes` (`user_id`,`smpproute_id`) VALUES ('$userRepo->id', '$route')";
							$stmt = $em->getConnection()->prepare($sql);
							$stmt->execute();			
						}


				}else {
					if ($type == 'TRANSACTIONAL' && $mainType != 'SMPP'){
						$userRepo->$transApiRoute = $route;
					}elseif ($type == 'PREMIUM' && $mainType != 'SMPP'){
						$userRepo->$premiApiRoute = $route;
					}if ($type == 'PROMOTIONAL' && $mainType != 'SMPP'){
						$userRepo->$promoApiRoute = $route;
					}elseif ($type == 'TRANS-SCRUB' && $mainType != 'SMPP'){
						$userRepo->$scrubApiRoute = $route;
					}elseif ($type == 'INTERNATIONAL' && $mainType != 'SMPP'){
						$userRepo->$interApiRoute = $route;
					}
					$em->persist($userRepo);
					$em->flush();
					/* 
					$sql="INSERT INTO `users_sms_routes` (`user_id`,`smsroute_id`) VALUES ('$userRepo->id', '$route')";
					$stmt = $em->getConnection()->prepare($sql);
					$stmt->execute(); 
					*/					
					if ($userRepo->role->role == 'RESELLER' || $userRepo->role->role == 'L1-RESELLER' || $userRepo->role->role == 'P1-RESELLER' || $userRepo->role->role == 'L1-ADMIN'){
						
						$domainRepo = $userRepo->domain;
						if ($type == 'TRANSACTIONAL'){
							$domainRepo->$transRoute = $route;
						}elseif ($type == 'PREMIUM'){
							$domainRepo->$premiRoute = $route;
						}elseif ($type == 'PROMOTIONAL'){
							$domainRepo->$promoRoute = $route;
						}elseif ($type == 'TRANS-SCRUB'){
							$domainRepo->$scrubRoute = $route;
						}elseif ($type == 'INTERNATIONAL'){
							$domainRepo->$interRoute = $route;
						}
						$em->persist($domainRepo);
						$em->flush();

						if ($mainType == 'SMS'){
							$smsRouteRepo = $em->getRepository('modules\user\models\SmsRoute')->findBy(array('type'=>"$type"));
							foreach ($smsRouteRepo as $smsRoute){
								$sql="DELETE FROM `users_sms_routes` WHERE `user_id` = '$userRepo->id' AND smsroute_id = '$smsRoute->id'";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();
							}
						}elseif($mainType == 'VOICE'){
							$voiceRouteRepo = $em->getRepository('modules\user\models\VoiceRoute')->findBy(array('type'=>"$type"));
							foreach ($voiceRouteRepo as $voiceRoute){
								$sql="DELETE FROM `users_voice_routes` WHERE `user_id` = '$userRepo->id' AND voiceroute_id = '$voiceRoute->id'";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();
							}
						}elseif($mainType == 'SMPP'){
							$smppRouteRepo = $em->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>"$type"));
							foreach ($smppRouteRepo as $smppRoute){
								$sql="DELETE FROM `users_smpp_routes` WHERE `user_id` = '$userRepo->id' AND smpproute_id = '$smppRoute->id'";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();
							}							
						}
						
						
						$underUsersRepo = $em->getRepository('modules\user\models\User')->findBy(array('reseller' => "$userRepo->id"));
						foreach ($underUsersRepo as $underUserRepo){
							if ($type == 'TRANSACTIONAL'){
								$underUserRepo->$transApiRoute = $route;
							}elseif ($type == 'PREMIUM'){
								$underUserRepo->$premiApiRoute = $route;
							}elseif ($type == 'PROMOTIONAL'){
								$underUserRepo->$promoApiRoute = $route;
							}elseif ($type == 'TRANS-SCRUB'){
								$underUserRepo->$scrubApiRoute = $route;
							}elseif ($type == 'INTERNATIONAL'){
								$underUserRepo->$interApiRoute = $route;
							}
						
							$em->persist($underUserRepo);
							$em->flush();
						
							if ($mainType == 'SMS'){
								$smsRouteRepo = $em->getRepository('modules\user\models\SmsRoute')->findBy(array('type'=>"$type"));
								foreach ($smsRouteRepo as $smsRoute){
									$sql="DELETE FROM `users_sms_routes` WHERE `user_id` = '$userRepo->id' AND smsroute_id = '$smsRoute->id'";
									$stmt = $em->getConnection()->prepare($sql);
									$stmt->execute();
								}
								
								$sql="INSERT INTO `users_sms_routes` (`user_id`,`smsroute_id`) VALUES ('$userRepo->id', '$route')";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();
							}elseif($mainType == 'VOICE'){
								$voiceRouteRepo = $em->getRepository('modules\user\models\VoiceRoute')->findBy(array('type'=>"$type"));
								foreach ($voiceRouteRepo as $voiceRoute){
									$sql="DELETE FROM `users_voice_routes` WHERE `user_id` = '$userRepo->id' AND voiceroute_id = '$voiceRoute->id'";
									$stmt = $em->getConnection()->prepare($sql);
									$stmt->execute();
								}
								
								$sql="INSERT INTO `users_voice_routes` (`user_id`,`voiceroute_id`) VALUES ('$userRepo->id', '$route')";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();
							}elseif($mainType == 'SMPP'){
								$smppRouteRepo = $em->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>"$type"));
								foreach ($smppRouteRepo as $smppRoute){
									$sql="DELETE FROM `users_smpp_routes` WHERE `user_id` = '$userRepo->id' AND smpproute_id = '$smppRoute->id'";
									$stmt = $em->getConnection()->prepare($sql);
									$stmt->execute();
								}
								
								$sql="INSERT INTO `users_smpp_routes` (`user_id`,`smpproute_id`) VALUES ('$userRepo->id', '$route')";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();				
							}
								
							$role = $underUserRepo->role->role;
							if ($role == 'RESELLER' || $role == 'L1-RESELLER' || $role == 'P1-RESELLER' || $role == 'L1-ADMIN'){
								$arrayUnderUsername = array();
								$arrayUnderUsername['0'] = $underUserRepo->username;
								$arrayUnderUsername['1'] = $underUserRepo->domain->domain;
								$this->userRoute($mainType, $arrayUnderUsername, $mode, $route, $type, $em);
							}
						}
						
					}else{

						if ($mainType == 'SMS'){
							$smsRouteRepo = $em->getRepository('modules\user\models\SmsRoute')->findBy(array('type'=>"$type"));
							foreach ($smsRouteRepo as $smsRoute){
								$sql="DELETE FROM `users_sms_routes` WHERE `user_id` = '$userRepo->id' AND smsroute_id = '$smsRoute->id'";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();
							}
							
							$sql="INSERT INTO `users_sms_routes` (`user_id`,`smsroute_id`) VALUES ('$userRepo->id', '$route')";
							$stmt = $em->getConnection()->prepare($sql);
							$stmt->execute();
						}elseif($mainType == 'VOICE'){
							$voiceRouteRepo = $em->getRepository('modules\user\models\VoiceRoute')->findBy(array('type'=>"$type"));
							foreach ($voiceRouteRepo as $voiceRoute){
								$sql="DELETE FROM `users_voice_routes` WHERE `user_id` = '$userRepo->id' AND voiceroute_id = '$voiceRoute->id'";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();
							}
							
							$sql="INSERT INTO `users_voice_routes` (`user_id`,`voiceroute_id`) VALUES ('$userRepo->id', '$route')";
							$stmt = $em->getConnection()->prepare($sql);
							$stmt->execute();
						}elseif($mainType == 'SMPP'){
							$smppRouteRepo = $em->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>"$type"));
							foreach ($smppRouteRepo as $smppRoute){
								$sql="DELETE FROM `users_smpp_routes` WHERE `user_id` = '$userRepo->id' AND smpproute_id = '$smppRoute->id'";
								$stmt = $em->getConnection()->prepare($sql);
								$stmt->execute();
							}
							
							$sql="INSERT INTO `users_smpp_routes` (`user_id`,`smpproute_id`) VALUES ('$userRepo->id', '$route')";
							$stmt = $em->getConnection()->prepare($sql);
							$stmt->execute();				
						}

						
					}
					
				}
	
			
			return $userRepo;
		}
		return false;
	}
	
	public function userPercentage($userRepo, $type, $mode, $route, $percentage, $em){
		if (isset($userRepo)){

			//$userRepo = $em->getRepository('modules\user\models\User')->findOneBy(array('username' => "$username", 'parentDomain' => "$parentDomain", 'domain' => $domainId));
			
			if ($type == '1'){
				if ($mode == '1'){
					if ($route == 'TRANSACTIONAL'){
						$userRepo->smsPercentageTrans = $percentage;
					}elseif ($route == 'PROMOTIONAL'){
						$userRepo->smsPercentagePromo = $percentage;
					}elseif ($route == 'TRANS-SCRUB'){
						$userRepo->smsPercentageScrub = $percentage;
					}elseif ($route == 'PREMIUM'){
						$userRepo->smsPercentagePremi = $percentage;
					}
					
					$em->persist($userRepo);
					$em->flush();
				}else {
					if ($route == 'TRANSACTIONAL'){
						$userRepo->smsPercentageTrans = $percentage;
					}elseif ($route == 'PROMOTIONAL'){
						$userRepo->smsPercentagePromo = $percentage;
					}elseif ($route == 'TRANS-SCRUB'){
						$userRepo->smsPercentageScrub = $percentage;
					}elseif ($route == 'PREMIUM'){
						$userRepo->smsPercentagePremi = $percentage;
					}
					
					$em->persist($userRepo);
					$em->flush();
					if ($userRepo->role->role == 'RESELLER' || $userRepo->role->role == 'L1-RESELLER'){
						$domainRepo = $userRepo->domain;
						if ($route == 'TRANSACTIONAL'){
							$domainRepo->smsPercentageTrans = $percentage;
						}elseif ($route == 'PROMOTIONAL'){
							$domainRepo->smsPercentagePromo = $percentage;
						}elseif ($route == 'TRANS-SCRUB'){
							$domainRepo->smsPercentageScrub = $percentage;
						}elseif ($route == 'PREMIUM'){
							$domainRepo->smsPercentagePremi = $percentage;
						}
						
						$em->persist($domainRepo);
						$em->flush();
					}
					$underUsersRepo = $em->getRepository('modules\user\models\User')->findBy(array('reseller' => "$userRepo->id"));
					foreach ($underUsersRepo as $underUserRepo){
						if ($route == 'TRANSACTIONAL'){
							$underUserRepo->smsPercentageTrans = $percentage;
						}elseif ($route == 'PROMOTIONAL'){
							$underUserRepo->smsPercentagePromo = $percentage;
						}elseif ($route == 'TRANS-SCRUB'){
							$underUserRepo->smsPercentageScrub = $percentage;
						}elseif ($route == 'PREMIUM'){
							$underUserRepo->smsPercentagePremi = $percentage;
						}
						
						$em->persist($underUserRepo);
						$em->flush();
							
						$role = $underUserRepo->role->role;
						if ($role == 'RESELLER' || $role == 'L1-RESELLER'){
							$this->userPercentage($underUserRepo, $type, $mode, $route, $percentage, $em);
						}
					}
				}
				
			}elseif($type == '2'){
				if ($mode == '1'){
					if ($route == 'TRANSACTIONAL'){
						$userRepo->voicePercentageTrans = $percentage;
					}elseif ($route == 'PROMOTIONAL'){
						$userRepo->voicePercentagePromo = $percentage;
					}elseif ($route == 'PREMIUM'){
						$userRepo->voicePercentagePremi = $percentage;
					}
					
					$em->persist($userRepo);
					$em->flush();
				}else {
					
					if ($route == 'TRANSACTIONAL'){
						$userRepo->voicePercentageTrans = $percentage;
					}elseif ($route == 'PROMOTIONAL'){
						$userRepo->voicePercentagePromo = $percentage;
					}elseif ($route == 'PREMIUM'){
						$userRepo->voicePercentagePremi = $percentage;
					}
					
					$em->persist($userRepo);
					$em->flush();
					if ($userRepo->role->role == 'RESELLER' || $userRepo->role->role == 'L1-RESELLER'){
						$domainRepo = $userRepo->domain;
						
						if ($route == 'TRANSACTIONAL'){
							$domainRepo->voicePercentageTrans = $percentage;
						}elseif ($route == 'PROMOTIONAL'){
							$domainRepo->voicePercentagePromo = $percentage;
						}elseif ($route == 'PREMIUM'){
							$domainRepo->voicePercentagePremi = $percentage;
						}
						$em->persist($domainRepo);
						$em->flush();
					}
					$underUsersRepo = $em->getRepository('modules\user\models\User')->findBy(array('reseller' => "$userRepo->id"));
					foreach ($underUsersRepo as $underUserRepo){
						if ($route == 'TRANSACTIONAL'){
							$underUserRepo->voicePercentageTrans = $percentage;
						}elseif ($route == 'PROMOTIONAL'){
							$underUserRepo->voicePercentagePromo = $percentage;
						}elseif ($route == 'PREMIUM'){
							$underUserRepo->voicePercentagePremi = $percentage;
						}
						
						$em->persist($underUserRepo);
						$em->flush();
							
						$role = $underUserRepo->role->role;
						if ($role == 'RESELLER' || $role == 'L1-RESELLER'){
							$this->userPercentage($underUserRepo, $type, $mode, $route, $percentage, $em);
						}
					}
				}
			}
			return $userRepo;
		}
		return false;
	}
	
	public function domainPercentage($domain, $type, $mode, $route, $percentage, $em){
		if (isset($domain)){
			$domainRepo = $em->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
			if ($type == '1'){
				if ($mode == '1'){
					if ($route == 'TRANSACTIONAL'){
						$domainRepo->smsPercentageTrans = $percentage;
					}elseif ($route == 'PROMOTIONAL'){
						$domainRepo->smsPercentagePromo = $percentage;
					}elseif ($route == 'TRANS-SCRUB'){
						$domainRepo->smsPercentageScrub = $percentage;
					}elseif ($route == 'PREMIUM'){
						$domainRepo->smsPercentagePremi = $percentage;
					}
					
					$em->persist($domainRepo);
					$em->flush();
				}else{
					
						if ($route == 'TRANSACTIONAL'){
							$domainRepo->smsPercentageTrans = $percentage;
						}elseif ($route == 'PROMOTIONAL'){
							$domainRepo->smsPercentagePromo = $percentage;
						}elseif ($route == 'TRANS-SCRUB'){
							$domainRepo->smsPercentageScrub = $percentage;
						}elseif ($route == 'PREMIUM'){
							$domainRepo->smsPercentagePremi = $percentage;
						}
					$em->persist($domainRepo);
					$em->flush();
					
					$underUsersRepo = $em->getRepository('modules\user\models\User')->findBy(array('domain' => "$domainRepo->id"));
					foreach ($underUsersRepo as $underUserRepo){
						$underUserRepo->smsPercentage = $percentage;
						$em->persist($underUserRepo);
						$em->flush();
					}
				}
				
			}elseif($type == '2'){
				if ($mode == '1'){
					
						if ($route == 'TRANSACTIONAL'){
							$domainRepo->voicePercentageTrans = $percentage;
						}elseif ($route == 'PROMOTIONAL'){
							$domainRepo->voicePercentagePromo = $percentage;
						}elseif ($route == 'PREMIUM'){
							$domainRepo->voicePercentagePremi = $percentage;
						}
						
					$em->persist($domainRepo);
					$em->flush();
				}else{
					
						if ($route == 'TRANSACTIONAL'){
							$domainRepo->voicePercentageTrans = $percentage;
						}elseif ($route == 'PROMOTIONAL'){
							$domainRepo->voicePercentagePromo = $percentage;
						}elseif ($route == 'PREMIUM'){
							$domainRepo->voicePercentagePremi = $percentage;
						}
					$em->persist($domainRepo);
					$em->flush();
					
					$underUsersRepo = $em->getRepository('modules\user\models\User')->findBy(array('domain_id' => "$domainRepo->id"));
					foreach ($underUsersRepo as $underUserRepo){
						
						if ($route == 'TRANSACTIONAL'){
							$underUserRepo->voicePercentageTrans = $percentage;	
						}elseif ($route == 'PROMOTIONAL'){
							$underUserRepo->voicePercentagePromo = $percentage;
						}elseif ($route == 'PREMIUM'){
							$underUserRepo->voicePercentagePremi = $percentage;
						}
						$em->persist($underUserRepo);
						$em->flush();
					}
				}
			}
			return $domainRepo;
		}
		return false;
	}
	
	public function setApi($apiValues, $userId, $em)
	{
		
		if (isset($em)){
			$apiRoute = $apiValues['type'];
			$routeType = $apiValues['smsType'];
			$routes = explode ('|', $routeType);
			$type = $routes['0'];
			$route = $routes['1'];	
			$routeId = false;
			
			$userRepo = $em->getRepository('modules\user\models\User')->find($userId);
			
			switch ($apiRoute) {
				case 'TL':
					if ($type == 'TRANSACTIONAL'){
						$routeRepo = $em->getRepository('modules\user\models\SmsRoute')->findOneBy(array('route' => "$route", 'type' => "$type", 'status' => '1'));
						$routeId = $routeRepo->id;
						if ($routeId){
							$userRepo->transApiRoute = $routeId;
						}
						
					}else{
						return array('Api' => array('isFailed' => 'Invalid Route requested'));
					}
					
					break;
				case 'PL':
					if ($type == 'PROMOTIONAL'){
						$routeRepo = $em->getRepository('modules\user\models\SmsRoute')->findOneBy(array('route' => "$route", 'type' => "$type", 'status' => '1'));
						$routeId = $routeRepo->id;
						if ($routeId){
							$userRepo->promoApiRoute = $routeId;
						}
						
					}else{
						return array('Api' => array('isFailed' => 'Invalid Route requested'));
					}
					
					break;
				case 'SL':
					if ($type == 'TRANS-SCRUB'){
						$routeRepo = $em->getRepository('modules\user\models\SmsRoute')->findOneBy(array('route' => "$route", 'type' => "$type", 'status' => '1'));
						$routeId = $routeRepo->id;
						if ($routeId){
							$userRepo->scrubApiRoute = $routeId;
						}
						
					}else{
						return array('Api' => array('isFailed' => 'Invalid Route requested'));
					}
					
					break;
				case 'IL':
						if ($type == 'INTERNATIONAL'){
							$routeRepo = $em->getRepository('modules\user\models\SmsRoute')->findOneBy(array('route' => "$route", 'type' => "$type", 'status' => '1'));
							$routeId = $routeRepo->id;
							if ($routeId){
								$userRepo->interApiRoute = $routeId;
							}
					
						}else{
							return array('Api' => array('isFailed' => 'Invalid Route requested'));
						}
							
						break;
				default:
					return array('Api' => array('isFailed' => 'Api Routing error failed'));
					break;
			}
			
			if ($routeId){
				
				$userRepo->updatedDate = new \DateTime('now');
				$em->persist($userRepo);
				$em->flush();
				
				//return $userRepo;
				return array('Api' => array('isSuccess' => 'Api route assigned','data' => $routeId, 'route' => $apiRoute));
			}else {
				return array('Api' => array('isFailed' => 'Api Route not found'));
			}
		}
		return false;
	}
	
}
