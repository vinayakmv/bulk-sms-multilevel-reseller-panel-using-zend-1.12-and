<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\DomainRepository")
 * @Table(name="domains")
 * @author Vinayak
 *
 */
class Domain 
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="domain", type="string", nullable=false)
	 * @var string
	 */
	private $domain;
	
	/**
	 * @OneToOne(targetEntity="DomainProfile", inversedBy="domain", cascade={"persist"})
	 * @JoinColumn(name="domain_profile_id", referencedColumnName="id")
	 * @var integer
	 */
	private $domainProfile;
	
	/**
	 * @Column(name="level", type="integer", nullable=true)
	 * @var integer
	 */
	private $level;
	
	/**
	 * @Column(name="parent_domain_id", type="integer", nullable=true)
	 * @var integer
	 */
	private $parentDomain;
	
	/**
	 * @Column(name="layout", type="string", nullable=true)
	 * @var string
	 */
	private $layout;	
	
	/**
	 * @Column(name="logo", type="string", nullable=true)
	 * @var string
	 */
	private $logo;
	
	/**
	 * @Column(name="footer", type="string", nullable=true)
	 * @var string
	 */
	private $footer;
		
	/**
	 * @Column(name="owner_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $owner;
	
	/**
	 * @column(name="reseller_id", type="integer", nullable="false")
	 * @var integer
	 */
	private $reseller;
	
	/**
	 * @column(name="voice_promo_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $voicePromoCredit;
	
	/**
	 * @column(name="voice_trans_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $voiceTransCredit;

	/**
	 * @column(name="voice_premi_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $voicePremiCredit;
	
	/**
	 * @column(name="sms_promo_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsPromoCredit;
	
	/**
	 * @column(name="sms_scrub_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsScrubCredit;
	
	/**
	 * @column(name="sms_trans_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsTransCredit;
	
	/**
	 * @column(name="sms_inter_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsInterCredit;
	
	/**
	 * @column(name="sms_premi_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsPremiCredit;
	
	/**
	 * @column(name="voice_promo_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $voicePromoApiCredit;
	
	/**
	 * @column(name="voice_trans_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $voiceTransApiCredit;

	/**
	 * @column(name="voice_premi_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $voicePremiApiCredit;
	
	/**
	 * @column(name="sms_promo_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsPromoApiCredit;
	
	/**
	 * @column(name="sms_scrub_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsScrubApiCredit;
	
	/**
	 * @column(name="sms_trans_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsTransApiCredit;	
	
	/**
	 * @column(name="sms_inter_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsInterApiCredit;
	
	/**
	 * @column(name="sms_premi_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsPremiApiCredit;
	
	/**
	 * @OneToMany(targetEntity="User", mappedBy="domain")
	 * @var integer
	 */
	private $users;
	
	/**
	 * @Column(name="reseller_role", type="integer", nullable=true)
	 * @var integer
	 */
	private $resellerRole;
			
	/**
	 * @Column(name="user_role", type="integer", nullable=true)
	 * @var integer
	 */
	private $userRole;
	
	/**
	 * @Column(name="validity", type="integer", nullable=true)
	 * @var integer
	 */
	private $validity;

	/**
	 * @Column(name="sms_percentage_promo", type="integer", nullable=true)
	 * @var integer
	 */
	private $smsPercentagePromo;

	/**
	 * @Column(name="sms_percentage_scrub", type="integer", nullable=true)
	 * @var integer
	 */
	private $smsPercentageScrub;
	
	/**
	 * @Column(name="sms_percentage_trans", type="integer", nullable=true)
	 * @var integer
	 */
	private $smsPercentageTrans;	

	/**
	 * @Column(name="sms_percentage_inter", type="integer", nullable=true)
	 * @var integer
	 */
	private $smsPercentageInter;

	/**
	 * @Column(name="sms_percentage_premi", type="integer", nullable=true)
	 * @var integer
	 */
	private $smsPercentagePremi;
	
	/**
	 * @Column(name="voice_percentage_promo", type="integer", nullable=true)
	 * @var integer
	 */
	private $voicePercentagePromo;
	
	/**
	 * @Column(name="voice_percentage_trans", type="integer", nullable=true)
	 * @var integer
	 */
	private $voicePercentageTrans;	

	/**
	 * @Column(name="voice_percentage_premi", type="integer", nullable=true)
	 * @var integer
	 */
	private $voicePercentagePremi;	
	
	/**
	 * @Column(name="sms_percentage_promo_main", type="integer", nullable=true)
	 * @var integer
	 */
	private $smsPercentagePromoMain;
	
	/**
	 * @Column(name="sms_percentage_scrub_main", type="integer", nullable=true)
	 * @var integer
	 */
	private $smsPercentageScrubMain;
	
	/**
	 * @Column(name="sms_percentage_trans_main", type="integer", nullable=true)
	 * @var integer
	 */
	private $smsPercentageTransMain;

	/**
	 * @Column(name="sms_percentage_premi_main", type="integer", nullable=true)
	 * @var integer
	 */
	private $smsPercentagePremiMain;
	
	/**
	 * @Column(name="voice_percentage_promo_main", type="integer", nullable=true)
	 * @var integer
	 */
	private $voicePercentagePromoMain;
	
	/**
	 * @Column(name="voice_percentage_trans_main", type="integer", nullable=true)
	 * @var integer
	 */
	private $voicePercentageTransMain;
	
	/**
	 * @Column(name="voice_percentage_premi_main", type="integer", nullable=true)
	 * @var integer
	 */
	private $voicePercentagePremiMain;
	
	/**
	 * @column(name="smpp_tps", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppTps;
	
	/**
	 * @column(name="smpp_bind", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppBind;
	
	/**
	 * @column(name="smpp_user", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppUser;
	
	/**
	 * @Column(name="smpp_trans_route", type="integer", nullable=true)
	 * @var string
	 */
	private $smppTransRoute;
        
        /**
	 * @Column(name="smpp_premi_route", type="integer", nullable=true)
	 * @var string
	 */
	private $smppPremiRoute;
	
	/**
	 * @Column(name="smpp_promo_route", type="integer", nullable=true)
	 * @var string
	 */
	private $smppPromoRoute;
	
	/**
	 * @Column(name="smpp_scrub_route", type="integer", nullable=true)
	 * @var string
	 */
	private $smppScrubRoute;

	/**
	 * @Column(name="smpp_inter_route", type="integer", nullable=true)
	 * @var string
	 */
	private $smppInterRoute;
	
	/**
	 * @Column(name="voice_trans_route", type="integer", nullable=true)
	 * @var string
	 */
	private $voiceTransRoute;	

	/**
	 * @Column(name="voice_premi_route", type="integer", nullable=true)
	 * @var string
	 */
	private $voicePremiRoute;
	
	/**
	 * @Column(name="voice_promo_route", type="integer", nullable=true)
	 * @var string
	 */
	private $voicePromoRoute;
	
	/**
	 * @Column(name="trans_route", type="integer", nullable=true)
	 * @var string
	 */
	private $transRoute;

	/**
	 * @Column(name="premi_route", type="integer", nullable=true)
	 * @var string
	 */
	private $premiRoute;
	
	/**
	 * @Column(name="promo_route", type="integer", nullable=true)
	 * @var string
	 */
	private $promoRoute;
	
	/**
	 * @Column(name="scrub_route", type="integer", nullable=true)
	 * @var string
	 */
	private $scrubRoute;

	/**
	 * @Column(name="inter_route", type="integer", nullable=true)
	 * @var string
	 */
	private $interRoute;

	/**
	 * @Column(name="status", type="integer", nullable=false)
	 * @var integer
	 */
	private $status;
	
	/**
	 * @Column(name="recredit", type="integer", nullable=false)
	 * @var integer
	 */
	private $recredit;	

	/**
	 * @Column(name="recredit_voice", type="integer", nullable=false)
	 * @var integer
	 */
	private $voiceRecredit;

	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}
