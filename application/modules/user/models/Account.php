<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\AccountRepository")
 * @Table(name="accounts")
 * @author Vinayak
 *
 */
class Account
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @column(name="smpp_promo_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppPromoCredit;
	
	/**
	 * @column(name="smpp_trans_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppTransCredit;
	
	/**
	 * @column(name="smpp_scrub_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppScrubCredit;
	
	/**
	 * @column(name="smpp_inter_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppInterCredit;
	
	/**
	 * @column(name="voice_promo_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $voicePromoCredit;
	
	/**
	 * @column(name="voice_trans_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $voiceTransCredit;

	/**
	 * @column(name="voice_premi_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $voicePremiCredit;
	
	/**
	 * @column(name="sms_premi_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsPremiCredit;
	
	/**
	 * @column(name="sms_promo_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsPromoCredit;
	
	/**
	 * @column(name="sms_scrub_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsScrubCredit;
	
	/**
	 * @column(name="sms_trans_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsTransCredit;
	
	/**
	 * @column(name="sms_inter_credit", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsInterCredit;
	
	/**
	 * @column(name="voice_promo_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $voicePromoApiCredit;
	
	/**
	 * @column(name="voice_trans_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $voiceTransApiCredit;
	
	/**
	 * @column(name="voice_premi_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $voicePremiApiCredit;
	
	/**
	 * @column(name="sms_premi_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsPremiApiCredit;
	
	/**
	 * @column(name="sms_promo_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsPromoApiCredit;
	
	/**
	 * @column(name="sms_scrub_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsScrubApiCredit;
	
	/**
	 * @column(name="sms_trans_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsTransApiCredit;
	
	/**
	 * @column(name="sms_inter_credit_api", type="integer", nullable="false")
	 * @var integer
	 */
	private $smsInterApiCredit;
	
	/**
	 * @column(name="smpp_tps", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppTps;
	
	/**
	 * @column(name="smpp_bind", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppBind;
	
	/**
	 * @column(name="smpp_user", type="integer", nullable="false")
	 * @var integer
	 */
	private $smppUser;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
	function toArray ()
   {
      return get_object_vars($this);
   }
	
}