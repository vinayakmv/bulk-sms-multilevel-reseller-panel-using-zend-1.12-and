<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\AccountLogRepository")
 * @Table(name="log_accounts")
 * @author Vinayak
 *
 */
class AccountLog
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @ManyToOne(targetEntity="User", inversedBy="accountLogs")
	 * @JoinColumn(name="user_id", referencedColumnName="id")
	 * @var integer
	 */
	private $user;

	/**
	 * @ManyToOne(targetEntity="User")
	 * @JoinColumn(name="client_id", referencedColumnName="id")
	 * @var integer
	 */
	private $client;
	
	/**
	 * @column(name="user_balance_before", type="integer", nullable="false") 
	 * @var unknown
	 */
	private $userBalanceBefore;
	
	/**
	 * @column(name="client_balance_before", type="integer", nullable="false") 
	 * @var unknown
	 */
	private $clientBalanceBefore;

	
	/**
	 * @column(name="user_balance_after", type="integer", nullable="false") 
	 * @var unknown
	 */
	private $userBalanceAfter;
	
	/**
	 * @column(name="client_balance_after", type="integer", nullable="false") 
	 * @var unknown
	 */
	private $clientBalanceAfter;
	
	/**
	 * @column(name="amount", type="integer", nullable="false") 
	 * @var unknown
	 */
	private $amount;
	
	/**
	 * @column(name="total", type="string", nullable="false") 
	 * @var unknown
	 */
	private $total;
	
	/**
	 * @column(name="tax", type="string", nullable="false") 
	 * @var unknown
	 */
	private $tax;
	
	/**
	 * @column(name="rate", type="string", nullable="false")
	 * @var unknown
	 */
	private $rate;
	
	/**
	 * @column(name="mode", type="string", nullable="false") 
	 * @var unknown
	 */
	private $mode;
	
	/**
	 * @column(name="type", type="integer", nullable="false")
	 * @var integer
	 */
	private $type;
		
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
