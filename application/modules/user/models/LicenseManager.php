<?php
namespace modules\user\models;

class LicenseManager extends \Smpp_Doctrine_BaseManager
{
	public function createLicense($getValues, $userId, $em)
	{
		$username =$getValues['selectUserName'];
		$company =$getValues['companyName'];
		$serverIp =$getValues['serverIP'];
		$validity =$getValues['validity'];
	 	if (isset($em)){
			$license = new License();
			$license->username=$username;
			$license->company=$company;
			$license->serverIp = $serverIp;
			$license->machineId = 'EMPTY';
			$license->validity = new \DateTime($validity);
			$license->status = '1';
			$license->createdBy = $userId;
			$license->createdDate = new \DateTime('now');
			$license->updatedDate = new \DateTime('now');
			
			$em->persist($license);
			$em->flush();
			
			return $license;
		}
		return false;
	}

	public function updateLicense($licenseRepo, $getValues, $userId, $em)
	{
		if (isset($em)){
			$serverIp =$getValues['serverIP'];
			$validity =$getValues['validity'];
			$licenseRepo->serverIp = $serverIp;
			$licenseRepo->updatedBy = $userId;
			$licenseRepo->validity = new \DateTime($validity);
			$license->updatedDate = new \DateTime('now');
			$em->persist($licenseRepo);
			$em->flush();
				
			return $licenseRepo;
		}
		return false;
	}
	
	public function deleteMachineId($licenseRepo,$userId, $em)
	{
		$licenseRepo->machineId = "EMPTY";
		$licenseRepo->updatedBy = $userId;
		$licenseRepo->updatedDate = new \DateTime('now');
		$em->persist($licenseRepo);
		$em->flush();
				
		return $licenseRepo;
	}
	
	public function unblockLicense($licenseRepo, $userId, $em)
	{
		$licenseRepo->status = 1;
		$licenseRepo->updatedBy = $userId;
		$licenseRepo->updatedDate = new \DateTime('now');
		$em->persist($licenseRepo);
		$em->flush();
	}
	
	public function blockLicense($licenseRepo, $userId, $em)
	{
		$licenseRepo->status = 0;
		$licenseRepo->updatedBy = $userId;
		$licenseRepo->updatedDate = new \DateTime('now');
		$em->persist($licenseRepo);
		$em->flush();
	}


}