<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\ApikeyRepository")
 * @Table(name="api_keys",indexes={
 * @Index(name="search_user",columns={"user_id","status"}),
 * @Index(name="search_api",columns={"api_key","status"})
 * });
 * @author Vinayak
 *
 */
class Apikey
{
     /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @column(name="user_id", type="integer", nullable="false")
	 * @var integer
	 */
	private $user;	
	
	/**
	 * @column(name="ip", type="string", nullable="false")
	 * @var string
	 */
	private $ip;
	
	/**
	 * @column(name="api_key", type="string", nullable="false")
	 * @var string
	 */	 
	private $apiKey;
	
	/**
	 * @column(name="status", type="integer", nullable="false")
	 * @var integer
	 */
	private $status;
	
	/**
	 * @Column(name="created_by", type="integer", nullable=false)
	 * @var integer
	 */
	private $createdBy;

	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;

	/**
	 * @column(name="country_code", type="string", nullable="false")
	 * @var string
	 */
	private $countryCode;
		
	/**
	 * @Column(name="updated_by", type="integer", nullable=false)
	 * @var integer
	 */
	private $updatedBy;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=true)
	 * @var datetime
	 */
	private $updatedDate;

	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
