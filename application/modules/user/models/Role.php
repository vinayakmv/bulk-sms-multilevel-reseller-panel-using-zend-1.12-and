<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\RoleRepository")
 * @Table(name="roles")
 * @author Vinayak
 *
 */
class Role
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")	 
     * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="parent_id", type="string", unique=false, nullable=true)
	 * @var string
	 */
	private $parent;
	
	/**
	 * @Column(name="role", type="string", unique=true, nullable=false)
	 * @var string
	 */
	private $role;
	
	/**
	 * @ManyToMany(targetEntity="Resource", inversedBy="roles")
	 * @JoinTable(name="roles_resources")
	 * @var integer
	 */
	private $resources;
	
	/**
	 * @OneToMany(targetEntity="User", mappedBy="role")
	 * @var integer
	 */
	private $users;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}
