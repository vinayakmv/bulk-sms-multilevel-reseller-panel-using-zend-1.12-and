<?php
namespace modules\user\models;

class DomainProfileManager extends \Smpp_Doctrine_BaseManager
{
	public function createDomain($domain, $em)
	{
		if (isset($domainName)) {
			$domain = new DomainProfile();
			$domain->domain = $domain;
			$domain->logo = null;
			$domain->footer = null;
			$domain->createdDate = new \DateTime('now');
			$domain->updatedDate = new \DateTime('now');
			$em->persist($domain);
			$em->flush();
				
			return $domain;
			
		}
		return false;
	}
	
	public function updateProfile($profileValues, $domainId, $em)
	{
		if (isset($em)){
			$domain = $em->getRepository('modules\user\models\DomainProfile')->findOneBy(array('domain' => "$domainId"));
			$domain->introduction = $profileValues['introduction'];
			$domain->about = $profileValues['about'];
			$domain->address = $profileValues['address'];
			
			$em->persist($domain);
			$em->flush();
	
			return $domain;
		}
		return false;
	}
	
}