<?php
namespace modules\user\models;

/**
 * @Entity(repositoryClass="modules\user\models\ResourceRepository")
 * @Table(name="resources")
 * @author Vinayak
 *
 */
class Resource
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="name", type="string", nullable=false)
	 * @var string
	 */
	private $name;
	
	/**
	 * @Column(name="resource", type="string", nullable=false)
	 * @var string
	 */
	private $resource;

	/**
	 * @ManyToMany(targetEntity="Role", mappedBy="resources")
	 * @var integer
	 */
	private $roles;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}