<?php
namespace modules\user\models;

class AccountLogManager extends \Smpp_Doctrine_BaseManager
{
	public function createLog($userRepo, $resellerRepo, $amount, $tax, $rate, $total, $userBalanceBefore, $userBalanceAfter, $clientBalanceBefore, $clientBalanceAfter, $mode, $type, $em)
	{
		if (isset($em)){
			$account = new AccountLog();
			$account->user = $resellerRepo;
			$account->client = $userRepo;
			$account->userBalanceBefore = $userBalanceBefore;
			$account->clientBalanceBefore = $clientBalanceBefore;
			$account->userBalanceAfter = $userBalanceAfter;
			$account->clientBalanceAfter = $clientBalanceAfter;
			
			$account->amount = $amount;
			$account->mode = $mode;
			$account->type = $type;
			$account->tax = $tax;
			$account->rate = $rate;
			$account->total = $total;
			$account->createdDate = new \DateTime('now');
			$account->updatedDate = new \DateTime('now');
			$em->persist($account);
			$em->flush();
				
			return $account;
		}
		return false;
	}
}
