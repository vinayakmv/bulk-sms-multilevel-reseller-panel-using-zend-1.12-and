<?php
namespace modules\user\models;
use Doctrine\ORM\EntityRepository;

/**
 * 
 * @author Vinayak
 *
 */
class UserRepository extends \Smpp_Doctrine_EntityRepository
{
	/**
	 * 
	 * @param string $username
	 * @param string $domain
	 */
	public function checkUser($username, $domain){
		$dql = "SELECT u FROM modules\user\models\User u JOIN u.domain d WHERE d.domain = '".$domain."' AND u.username = '".$username."'";
		
		$query = $this->getEntityManager()
					  ->createQuery($dql);
		return $query->getResult();
	}

	/**
	 * Get User STATUS  -  Blocked or not | Used in API Calling | Using Native SQL
	 * @param  String $userId User ID
	 * @return Array         
	 */
	public function getUserStatus($userId)
	{
		$sql="SELECT status FROM users WHERE id = '$userId'";
    	$stmt = $this ->getEntityManager()->getConnection()->prepare($sql);
    	$stmt->execute();
    	return $stmt->fetchAll();
	}
	
	public function getApiRoute($userId, $route)
	{
		if ($route == 'PL'){
			$sql="SELECT sms_promo_api_route as api_route FROM users WHERE id = '$userId'";
		}elseif ($route == 'TL'){
			$sql="SELECT sms_trans_api_route as api_route FROM users WHERE id = '$userId'";
		}elseif ($route == 'SL'){
			$sql="SELECT sms_scrub_api_route as api_route FROM users WHERE id = '$userId'";
		}elseif ($route == 'IL'){
			$sql="SELECT sms_inter_api_route as api_route FROM users WHERE id = '$userId'";
		}else{
			return false;
		}
		
		$stmt = $this ->getEntityManager()->getConnection()->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll();
	}
	
	public function getVoiceApiRoute($userId, $route)
	{
		if ($route == 'PL'){
			$sql="SELECT voice_promo_api_route as api_route FROM users WHERE id = '$userId'";
		}elseif ($route == 'TL'){
			$sql="SELECT voice_trans_api_route as api_route FROM users WHERE id = '$userId'";
		}
	
		$stmt = $this ->getEntityManager()->getConnection()->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll();
	}
	

	public function getUser($userId, $searchKey) 
	{
		//$dql = "SELECT A FROM modules\user\models\Users A WHERE A.reseller = $userId AND A.user LIKE :keyword";
		//$query = $this->getEntityManager()->createQuery($dql);
		//$query->setParameter("keyword", '%'.$searchKey.'%');
		//return $query->getResult();
		$queryBuilder = $this->getEntityManager()->createQueryBuilder()
							 ->select('u.username, d.domain')
							 ->from('modules\user\models\User', 'u')
							 ->innerJoin('u.domain', 'd')
							 ->where('u.username LIKE ?1 AND u.reseller = ?2 AND u.id != ?2')
							 ->setParameter("1", '%'.$searchKey.'%')
							 ->setParameter("2", "$userId");
		
		return $queryBuilder->getQuery()->getResult();
	}

}