<?php
namespace modules\user\models;
/**
 * 
 * @author Vinayak
 *
 */
class UserProfileManager extends \Smpp_Doctrine_BaseManager
{
	public function updateProfile($profileValues, $userRepo, $emUser)
	{
		if (isset($emUser)){
			$userRepo->profile->email = $profileValues['email'];
			$userRepo->profile->name = $profileValues['name'];
			$userRepo->profile->mobile = $profileValues['mobile'];
			$userRepo->profile->country = $profileValues['country'];
			$userRepo->profile->updatedDate = new \DateTime('now');
			$emUser->persist($userRepo);
			$emUser->flush();
			
			return $userRepo;
		}
		return false;
	}

}
