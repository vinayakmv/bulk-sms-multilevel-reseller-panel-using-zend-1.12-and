<?php

class User_Form_Contact extends Zend_Form
{

    public function init()
    {
        $this->setAction('create');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal");

    	$contact = new Zend_Form_Element_Text('contactName');
    	$contact->setDecorators(array('ViewHelper','Description'));
    	$contact->setLabel('Group Name');
    	$contact->class = "form-control";
    	$contact->setRequired(true);
    	$contact->addFilter(new Zend_Filter_HtmlEntities());
    	$contact->addFilter(new Zend_Filter_StripTags());
    	$contact->addFilter(new Zend_Filter_StringToUpper());
    	$this->addElement($contact);
    	
    	/* //Protection against CSRF attack
    	$token = new Zend_Form_Element_Hash('token',array('timeout' => '1800'));
    	$token->setSalt(md5(uniqid(rand(), TRUE)));
    	
    	//$token->setTimeout(Globals::getConfig()->authentication->timeout);
    	$token->setDecorators(array('ViewHelper','Description'));
    	$this->addElement($token); */

		
		$contactOptions = array("3" => "Paste",
								"2" => "Upload");
		$contacttype = new Zend_Form_Element_Radio('contactType');
		$contacttype->setMultiOptions($contactOptions)->setSeparator('')->setValue("3");
		$contacttype->setDecorators(array('ViewHelper','Description'));
		$contacttype->setLabel('Contact Type');
		$contacttype->setRequired(true);
		$this->addElement($contacttype);

		$uploadContact = new Zend_Form_Element_File('uploadContact', array('style'=>'display:none;', 'onchange'=>'$("#upload-file-primary").html($(this).val());'));
		$uploadContact->setDecorators(array('File'));
		$uploadContact->setLabel('Upload Contacts');
		$uploadContact->class = "file-upload-default";
		$uploadContact->setDestination('../contactFiles/');
		$uploadContact->addValidator('Count', false, 1);
		$uploadContact->addValidator('Extension', false, 'csv');
		$uploadContact->setMaxFileSize(10485760);
		$uploadContact->setRequired(false);
		$this->addElement($uploadContact);
				
		$textContact = new Zend_Form_Element_Textarea('textContact');
		$textContact->setDecorators(array('ViewHelper','Description'));
		$textContact->setLabel('Paste Contacts');
		$textContact->setAttrib('rows', '10');
		$textContact->setAttrib('cols', '30');
		$textContact->setAttrib('onKeyUp', "countLines(this)");
		$textContact->class = "form-control";
		$textContact->setRequired(false);
		$textContact->addFilter(new Zend_Filter_HtmlEntities());
		$textContact->addFilter(new Zend_Filter_StripTags());
		$this->addElement($textContact);
		       
		$this->addElement('submit', 'submit');
		$submitElement = $this->getElement('submit');
		$submitElement->setAttrib('class',"btn-primary btn");
		$submitElement->setDecorators(array('ViewHelper','Description'));
		$submitElement->setLabel('Create Group');
		 
		$this->setDecorators(array('FormElements',
								   'Form'
		));
    }


}

