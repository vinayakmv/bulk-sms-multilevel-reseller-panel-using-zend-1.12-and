<?php

class User_Form_Register extends Zend_Form
{

    public function init()
    {
        $this->setAction('whatsapp');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    
    	$username = new Zend_Form_Element_Text('username');
    	$username->setDecorators(array('ViewHelper','Description'));
    	$username->setLabel('Mobile');
    	$username->class = "form-control";
    	$username->setAttrib('placeholder', '910000000000');
    	//$username->addErrorMessage('Please enter a username');
    	$username->setRequired(true);
    
    	//Add Validator
    	$username->addValidator(new Zend_Validate_StringLength(12,12));
    	$username->addValidator(new Zend_Validate_Digits());
    	
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);
    	
    	$nickname = new Zend_Form_Element_Text('nickname');
    	$nickname->setDecorators(array('ViewHelper','Description'));
    	$nickname->setLabel('Nick');
    	$nickname->class = "form-control";
    	$nickname->setAttrib('placeholder', 'Marketing');
    	//$username->addErrorMessage('Please enter a username');
    	$nickname->setRequired(true);
    	
    	//Add Validator
    	$nickname->addValidator(new Zend_Validate_StringLength(0,10));
    	//$nickname->addValidator(new Zend_Validate_Digits());
    	 
    	//Add Filter
    	$nickname->addFilter(new Zend_Filter_HtmlEntities());
    	$nickname->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($nickname);
    	
    	//Create a submit button.
    	$this->addElement('submit', 'request');
    	$submitElement = $this->getElement('request');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors'));
    	$submitElement->setLabel('Request Code');    	
    	$this->setDecorators(array('FormElements',
					    			'Form'));
    }


}

