<?php

class User_Form_Profile extends Zend_Form
{

    public function init()
    {
        $this->setAction('/user/profile/index');
    	$this->setMethod('post');
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    	$this->setAttrib('class', "form-horizontal row-border");
    	//Create Password Object.
    	
    	$name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper',
                                        'Description'));
        $name->setLabel('Name');
        //$name->setAttrib('placeholder', 'Name');
        $name->setAttrib('required','required');
        $name->setAttrib('class', 'form-control');
        $name->addErrorMessage('Please enter a name');
        $name->setRequired(true);
        $name->addFilter(new Zend_Filter_HtmlEntities());
        $name->addFilter(new Zend_Filter_StripTags());
        //Add name Element
        $this->addElement($name);
    	
    	$mobile = new Zend_Form_Element_Text('mobile');
    	$mobile->setDecorators(array('ViewHelper','Description'));
    	$mobile->setAttrib('id', 'mobile');
    	//$mobile->setAttrib('placeholder', 'Mobile');
    	$mobile->addErrorMessage('Please enter a valid mobile number');
    	$mobile->setAttrib('required','required');
    	$mobile->setAttrib('class', 'form-control');
    	$mobile->addValidator(new Zend_Validate_StringLength(10,10));
    	$mobile->addValidator('Digits');
    	$mobile->addFilter(new Zend_Filter_HtmlEntities());
    	$mobile->addFilter(new Zend_Filter_StripTags());
    	$this->addElement($mobile);
    	
    	$country = new Zend_Form_Element_Text('country');
    	$country->setDecorators(array('ViewHelper','Description'));
    	//$country->setAttrib('placeholder', 'Place');
    	$country->setAttrib('id', 'country');
    	$country->setAttrib('class', 'form-control');
    	$country->addFilter(new Zend_Filter_HtmlEntities());
    	$country->addFilter(new Zend_Filter_StripTags());
    	$this->addElement($country);

    	$email = new Zend_Form_Element_Text('email');
    	$email->setDecorators(array('ViewHelper','Description'));
    	$email->setLabel('Username');
    	//$email->setAttrib('placeholder', 'Email');
    	$email->class = "form-control";
    	//$email->addErrorMessage('Please enter a username');
    	$email->setRequired(true);
    	 
    	$email->setAttrib('required','required');
    	 
    	//Add Validator
    	//$email->addValidator(new Zend_Validate_StringLength(6,20));
    	$email->addValidator(new Zend_Validate_EmailAddress(array('allow' => Zend_Validate_Hostname::ALLOW_DNS,'mx'    => false,'deep'  => false)));
    	//$email->addValidator(new Smpp_Validate_Db_NoUserExists());
    	
    	//Add Filter
    	$email->addFilter(new Zend_Filter_HtmlEntities());
    	$email->addFilter(new Zend_Filter_StripTags());
    	$email->addFilter(new Zend_Filter_StringToLower());
    	//Add Username Element
    	$this->addElement($email);
    	
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors'));
    	$submitElement->setLabel('Update');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'
					    	));
    }


}

