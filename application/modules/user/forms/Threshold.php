<?php

class User_Form_Threshold extends Zend_Form
{

    public function init()
    {
        $this->setAction('/user/profile/alert');
    	$this->setMethod('post');
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    	$this->setAttrib('class', "form-horizontal row-border");
    	$alert = new Zend_Form_Element_Radio('alert');
    	//Add Filter
    	$alert->addFilter(new Zend_Filter_HtmlEntities());
    	$alert->addFilter(new Zend_Filter_StripTags());
    	$alertOption = array(
    			'0' => 'Off',
    			'1' => 'Email Alert');
    	$alert->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors'));
    	$alert->addMultiOptions($alertOption)->setSeparator('&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp');
    	$alert->setValue('Off');
    	$alert->setRequired(true);
    	//Add Alert Element
    	$this->addElement($alert);
    	
    	$threshold = new Zend_Form_Element_Text('threshold');
    	$threshold->setDecorators(array('ViewHelper',
    			'Description'));
    	$threshold->setLabel('Threshold Credit');
    	$threshold->setAttrib('placeholder', 'Credits');
    	$threshold->class = "form-control";
    	//$username->addErrorMessage('Please enter a username');
    	$threshold->setRequired(true);
    	
    	//Add Validator
    	//$username->addValidator(new Zend_Validate_StringLength(4,20));
    	 
    	//Add Filter
    	$threshold->addFilter(new Zend_Filter_HtmlEntities());
    	$threshold->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($threshold);
    	    	
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors'));
    	$submitElement->setLabel('Update Threshold');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'
					    	));
    }


}

