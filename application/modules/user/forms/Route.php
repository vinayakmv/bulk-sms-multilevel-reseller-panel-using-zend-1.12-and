<?php

class User_Form_Route extends Zend_Form
{

	public function init()
    {
        $this->setAction('route');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
 
    	//Create Password Object.
    	$selectRole = new Zend_Form_Element_Select('type');
		$roles = array("TL" => "TL [TRANSACTIONAL]",
					   "PL" => "PL [PROMOTIONAL]", 
					   "SL" => "SL [TRANS-SCRUB]",
				       "IL" => "IL [INTERNATIONAL]"
		);
		$selectRole->addMultiOptions($roles);
		$selectRole->setDecorators(array('ViewHelper','Description'));
		$selectRole->setLabel('Select Route');
		$selectRole->class = "form-control select2 custom-select";
		
		$selectRole->setRequired(true);

		$selectRole->addFilter(new Zend_Filter_HtmlEntities());
		$selectRole->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectRole);
    	    	
		$smstype = new Zend_Form_Element_Select('smsType');
		$smstype->setDecorators(array('ViewHelper','Description'));
		$smstype->setLabel('Select Audio');
		$smstype->id = "smstype";
		$smstype->class = "form-control select2 custom-select";
		$smstype->setRequired(true);
		//$smstype->addMultiOptions(array("0" => "NO ROUTE"));
		$smstype->addFilter(new Zend_Filter_HtmlEntities());
		$smstype->addFilter(new Zend_Filter_StripTags());
		$this->addElement($smstype);
		
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors'));
    	$submitElement->setLabel('Assign');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'));
    }


}

