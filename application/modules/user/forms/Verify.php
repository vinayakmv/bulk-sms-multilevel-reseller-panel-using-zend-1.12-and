<?php

class User_Form_Verify extends Zend_Form
{

    public function init()
    {
       $this->setAction('whatsapp');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    	
    	$username = new Zend_Form_Element_Text('username');
    	$username->setDecorators(array('ViewHelper','Description'));
    	$username->setLabel('Mobile');
    	$username->class = "form-control";
    	$username->setAttrib('placeholder', '910000000000');
    	//$username->addErrorMessage('Please enter a username');
    	$username->setRequired(true);
    	
    	//Add Validator
    	$username->addValidator(new Zend_Validate_StringLength(12,12));
    	$username->addValidator(new Zend_Validate_Digits());
    	 
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);
    	$code = new Zend_Form_Element_Text('code');
    	$code->setDecorators(array('ViewHelper','Description'));
    	$code->setLabel('Code');
    	$code->class = "form-control";
    	$code->setAttrib('placeholder', '123456');
    	//$username->addErrorMessage('Please enter a username');
    	$code->setRequired(true);
    
    	//Add Validator
    	$code->addValidator(new Zend_Validate_StringLength(6,6));
    	$code->addValidator(new Zend_Validate_Digits());
    	
    	//Add Filter
    	$code->addFilter(new Zend_Filter_HtmlEntities());
    	$code->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($code);
    	
    	//Create a submit button.
    	$this->addElement('submit', 'register');
    	$submitElement = $this->getElement('register');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors'));
    	$submitElement->setLabel('Register Account');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'));
    }


}

