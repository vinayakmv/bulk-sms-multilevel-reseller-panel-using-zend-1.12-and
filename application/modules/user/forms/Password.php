<?php

class User_Form_Password extends Zend_Form
{

    public function init()
    {
        $this->setAction('/user/profile/password');
    	$this->setMethod('post');
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    	$this->setAttrib('class', "form-horizontal row-border");
    	//Create Password Object.
    	
    	$oldpassword = new Zend_Form_Element_Password('oldpassword');
    	$oldpassword->setDecorators(array('ViewHelper',
						    			'Description'));
    	$oldpassword->setLabel('Old Password');
    	//$oldpassword->setAttrib('placeholder', 'Old-Password');
    	$oldpassword->class = "form-control";
    	$oldpassword->setRequired(true);
    	//Add Validator
    	 
    	//Add Filter
    	$oldpassword->addFilter(new Zend_Filter_HtmlEntities());
    	$oldpassword->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($oldpassword);
    	
    	//Create Password Object.
    	$password = new Zend_Form_Element_Password('password');
    	$password->setDecorators(array('ViewHelper',
						    			'Description'));
    	$password->setLabel('New Password');
    	//$password->setAttrib('placeholder', 'New-Password');
    	$password->class = "form-control";
    	$password->setRequired(true);
    	//Add Validator
    	 
    	//Add Filter
    	$password->addFilter(new Zend_Filter_HtmlEntities());
    	$password->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($password);
    	
    	//Create Password Object.
    	$repassword = new Zend_Form_Element_Password('repassword');
    	$repassword->setDecorators(array('ViewHelper',
						    			'Description'));
    	$repassword->setLabel('Confirm');
    	//$repassword->setAttrib('placeholder', 'Confirm Password');
    	$repassword->class = "form-control";
    	$repassword->setRequired(true);
    	//Add Validator
    	$repassword->addValidator('Identical', false, array('token' => 'password'));
    	//Add Filter
    	$repassword->addFilter(new Zend_Filter_HtmlEntities());
    	$repassword->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($repassword);
    	    	
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors'));
    	$submitElement->setLabel('Update Password');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'
					    	));
    }


}

