<?php

class User_Form_Account extends Zend_Form
{

    public function init()
    {
        $this->setAction('/user/profile/account');
    	$this->setMethod('post');
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    	$this->setAttrib('class', "form-horizontal row-border");
    	$fname = new Zend_Form_Element_Text('fname');
    	$fname->setDecorators(array('ViewHelper',
						    			'Description'));
    	$fname->setLabel('First Name');
    	$fname->setAttrib('placeholder', 'First Name');
    	$fname->class = "form-control";
    	//$username->addErrorMessage('Please enter a username');
    	$fname->setRequired(true);
    
    	//Add Validator
    	//$username->addValidator(new Zend_Validate_StringLength(4,20));
    	
    	//Add Filter
    	$fname->addFilter(new Zend_Filter_HtmlEntities());
    	$fname->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($fname);
    	
    	//Create LName Object
    	$lname = new Zend_Form_Element_Text('lname');
    	$lname->setDecorators(array('ViewHelper',
    			'Description'));
    	$lname->setLabel('Last Name');
    	$lname->setAttrib('placeholder', 'Last Name');
    	$lname->class = "form-control";
    	//$username->addErrorMessage('Please enter a username');
    	$lname->setRequired(true);
    	
    	//Add Validator
    	//$username->addValidator(new Zend_Validate_StringLength(4,20));
    	 
    	//Add Filter
    	$lname->addFilter(new Zend_Filter_HtmlEntities());
    	$lname->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($lname);
    	
    	//Create Mobile Object.
    	$mobile = new Zend_Form_Element_Text('mobile');
    	$mobile->setDecorators(array('ViewHelper',
						    			'Description'));
    	$mobile->setLabel('Mobile');
    	$mobile->setAttrib('placeholder', 'Mobile');
    	$mobile->class = "form-control";
    	$mobile->setRequired(true);
    	//Add Validator
    	$mobile->addValidator(new Zend_Validate_Digits());
    	//Add Filter
    	$mobile->addFilter(new Zend_Filter_HtmlEntities());
    	$mobile->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($mobile);
    	    	
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors'));
    	$submitElement->setLabel('Update Account');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'
					    	));
    }


}

