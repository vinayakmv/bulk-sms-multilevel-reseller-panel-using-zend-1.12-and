<?php

class User_Form_Reseller extends Zend_Form
{

    public function init()
    {
        $this->setAction('create');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    
    	
    	
    	//Role Hidden element
    	$domain = new Zend_Form_Element_Text('domain');
    	$domain->setDecorators(array('ViewHelper',
						    			'Description'));
    	$domain->setLabel('Domain');
    	$domain->class = "form-control";
    	//$domain->setAttrib('placeholder', 'www.domain.com');
    	//$username->addErrorMessage('Please enter a username');
    	$domain->setRequired(true);
    	
    	//Add Validator
    	//$username->addValidator(new Zend_Validate_StringLength(4,20));
    	//$domain->addValidator(new Zend_Validate_Hostname());
    	$domain->addValidator(new Smpp_Validate_Db_NoDomainExists());
    	//$domain->addValidator(new Smpp_Validate_Db_NoDomainExists());
    	 
    	//Add Filter
    	$domain->addFilter(new Zend_Filter_HtmlEntities());
    	$domain->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($domain);
    	
    	$accounttype = new Zend_Form_Element_Select('theme');
    	$roles = array("1" => "DARK BOARD",
    			"2" => "LIGHT ADMIN",
                        "3" => "LIGHT FLAT"
    	);
    	$accounttype->addMultiOptions($roles);
    	$accounttype->class = "form-control select2 custom-select";
    	$accounttype->setLabel('Theme');
    	$accounttype->setDecorators(array('ViewHelper','Description'));
    	$accounttype->setRequired(true);
    	
    	//Add Validator
    	//Add Filter
    	$accounttype->addFilter(new Zend_Filter_HtmlEntities());
    	$accounttype->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($accounttype);
    	
    	$country = new Zend_Form_Element_Text('country');
    	$country->setDecorators(array('ViewHelper','Description'));
    	//$country->setAttrib('placeholder', 'Place');
    	$country->setAttrib('id', 'country');
    	$country->setAttrib('class', 'form-control');
    	$country->addFilter(new Zend_Filter_HtmlEntities());
    	$country->addFilter(new Zend_Filter_StripTags());
    	$this->addElement($country);
    	
    	$username = new Zend_Form_Element_Text('username');
    	$username->setDecorators(array('ViewHelper','Description'));
    	$username->setLabel('Username');
    	//$username->setAttrib('placeholder', 'Username');
    	$username->class = "form-control";
    	//$username->addErrorMessage('Please enter a username');
    	$username->setRequired(true);
    	
    	$username->setAttrib('required','required');
    	
    	//Add Validator
    	$username->addValidator(new Zend_Validate_StringLength(6,20));
    	//$username->addValidator(new Zend_Validate_EmailAddress(array('allow' => Zend_Validate_Hostname::ALLOW_DNS,'mx'    => true,'deep'  => true)));
    	$username->addValidator(new Smpp_Validate_Db_NoUserExists());
    	 
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	$username->addFilter(new Zend_Filter_StringToLower());
    	//Add Username Element
    	$this->addElement($username);
    	 
    	$email = new Zend_Form_Element_Text('email');
    	$email->setDecorators(array('ViewHelper','Description'));
    	$email->setLabel('Username');
    	//$email->setAttrib('placeholder', 'Email');
    	$email->class = "form-control";
    	//$email->addErrorMessage('Please enter a username');
    	$email->setRequired(true);
    	 
    	$email->setAttrib('required','required');
    	 
    	//Add Validator
    	//$email->addValidator(new Zend_Validate_StringLength(6,20));
    	$email->addValidator(new Zend_Validate_EmailAddress());
    	//$email->addValidator(new Smpp_Validate_Db_NoUserExists());
    	
    	//Add Filter
    	$email->addFilter(new Zend_Filter_HtmlEntities());
    	$email->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($email);
    	 
    	$mobile = new Zend_Form_Element_Text('mobile');
    	$mobile->setDecorators(array('ViewHelper','Description'));
    	$mobile->setAttrib('id', 'mobile');
    	//$mobile->setAttrib('placeholder', 'Mobile');
    	$mobile->addErrorMessage('Please enter a valid mobile number');
    	$mobile->setAttrib('required','required');
    	$mobile->setAttrib('class', 'form-control');
    	$mobile->addValidator(new Zend_Validate_StringLength(10,10));
    	$mobile->addValidator('Digits');
    	$mobile->addFilter(new Zend_Filter_HtmlEntities());
    	$mobile->addFilter(new Zend_Filter_StripTags());
    	$this->addElement($mobile);
    	
    	$name = new Zend_Form_Element_Text('name');
    	$name->setDecorators(array('ViewHelper','Description')); 	       
    	$name->setLabel('Name');
    	//$name->setAttrib('placeholder', 'Name');
    	        $name->class = "form-control";
    	        $name->setAttrib('required','required');
    	        $name->addErrorMessage('Please enter a name');
    	        $name->setRequired(true);
    	        $name->addFilter(new Zend_Filter_HtmlEntities());
    	        $name->addFilter(new Zend_Filter_StripTags());
    	        //Add name Element
    	        $this->addElement($name);
    	    	
    	    	//Create Password Object.
    	    	$password = new Zend_Form_Element_Password('password');
    	    	$password->setDecorators(array('ViewHelper',
    							    			'Description'));
    	    	$password->setLabel('Password');
    	    	//$password->setAttrib('placeholder', 'Password');
    	    	$password->class = "form-control";
    	    	$password->setRequired(true);
    	    	//Add Validator
    	        $password->setAttrib('required','required');
    	    	//Add Filter
    	    	$password->addFilter(new Zend_Filter_HtmlEntities());
    	    	$password->addFilter(new Zend_Filter_StripTags());
    	    	//Add Password Element
    	    	$this->addElement($password);
    	    	
    	    	//Create Password Object.
    	    	$repassword = new Zend_Form_Element_Password('repassword');
    	    	$repassword->setDecorators(array('ViewHelper',
    							    			'Description'));
    	    	$repassword->setLabel('Confirm');
    	    	//$repassword->setAttrib('placeholder', 'Confirm Password');
    	    	$repassword->class = "form-control";
    	    	$repassword->setRequired(true);
    	    	//Add Validator
    	    	$repassword->addValidator('Identical', false, array('token' => 'password'));
    	    	//Add Filter
    	    	$repassword->addFilter(new Zend_Filter_HtmlEntities());
    	    	$repassword->addFilter(new Zend_Filter_StripTags());
    	    	//Add Password Element
    	    	$this->addElement($repassword);
    	
    	$tps = new Zend_Form_Element_Text('tps');
    	$tps->setDecorators(array('ViewHelper','Description'));
    	$tps->setLabel('Throughput');
    	$tps->class = "form-control";
    	//$tps->setAttrib('placeholder', 'TPS');
    	//$tps->addErrorMessage('Please enter a username');
    	$tps->setRequired(true);
    	
    	//Add Validator
    	$tps->addValidator(new Zend_Validate_StringLength(1,4));
    	 
    	//Add Filter
    	$tps->addFilter(new Zend_Filter_HtmlEntities());
    	$tps->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($tps);
    	
    	$bind = new Zend_Form_Element_Text('bind');
    	$bind->setDecorators(array('ViewHelper','Description'));
    	$bind->setLabel('Bind/Session');
    	$bind->class = "form-control";
    	//$bind->setAttrib('placeholder', 'Session');
    	//$bind->addErrorMessage('Please enter a username');
    	$bind->setRequired(true);
    	 
    	//Add Validator
    	$bind->addValidator(new Zend_Validate_StringLength(1,4));
    	
    	//Add Filter
    	$bind->addFilter(new Zend_Filter_HtmlEntities());
    	$bind->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($bind);
    	
    	$smppUser = new Zend_Form_Element_Text('smppUser');
    	$smppUser->setDecorators(array('ViewHelper','Description'));
    	$smppUser->setLabel('Max SMPP User');
    	$smppUser->class = "form-control";
    	//$smppUser->setAttrib('placeholder', 'Users');
    	//$smppUser->addErrorMessage('Please enter a username');
    	$smppUser->setRequired(true);
    	
    	//Add Validator
    	$smppUser->addValidator(new Zend_Validate_StringLength(1,4));
    	 
    	//Add Filter
    	$smppUser->addFilter(new Zend_Filter_HtmlEntities());
    	$smppUser->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($smppUser);
    	
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper','Description','Errors'));
    	$submitElement->setLabel('Create');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'));
    }


}

