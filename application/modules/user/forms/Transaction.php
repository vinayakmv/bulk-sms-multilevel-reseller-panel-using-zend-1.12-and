<?php

class User_Form_Transaction extends Zend_Form
{

    public function init()
    {
        $this->setAction('/user/transaction/index');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	$username = new Zend_Form_Element_Select('username');
    	$username->setDecorators(array('ViewHelper',
						    			'Description'));
    	$username->setLabel('Username ');
    	$username->class = "form-control select2 custom-select";	
    	$username->setAttrib('id', 'user');
    	$username->setRequired(true);
    	
    	//Add Validator
    	//$username->addValidator(new Zend_Validate_StringLength(4,20));
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);
    	
    	$token = new Zend_Form_Element_Hash('token',array('timeout' => '1800'));
    	$token->setSalt(md5(uniqid(rand(), TRUE)));
    	 
    	//$token->setTimeout(Globals::getConfig()->authentication->timeout);
    	$token->setDecorators(array('ViewHelper','Description'));
    	$this->addElement($token);
    	
    	$amount = new Zend_Form_Element_Text('amount');
    	$amount->setDecorators(array('ViewHelper','Description'));
    	$amount->setLabel('Credits');
    	$amount->class = "form-control";
    	$amount->setRequired(true);
    	
    	//Add Validator
    	$amount->addValidator(new Zend_Validate_Float());
    	//Add Filter
    	$amount->addFilter(new Zend_Filter_HtmlEntities());
    	$amount->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($amount);

    	$price = new Zend_Form_Element_Text('rate');
    	$price->setDecorators(array('ViewHelper','Description'));
    	$price->setLabel('Rate');
    	$price->class = "form-control";
    	$price->setRequired(false);
    	
    	//Add Validator
    	$price->addValidator(new Zend_Validate_Float());
    	//Add Filter
    	$price->addFilter(new Zend_Filter_HtmlEntities());
    	$price->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($price);
    	
		$total = new Zend_Form_Element_Text('total');
		//$total->setAttrib('readonly', 'true');
    	$total->setDecorators(array('ViewHelper','Description'));
    	$total->setLabel('Credits');
    	$total->class = "form-control";
    	$total->setRequired(true);
    	
    	//Add Validator
    	$total->addValidator(new Zend_Validate_Float());
    	//Add Filter
    	$total->addFilter(new Zend_Filter_HtmlEntities());
    	$total->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($total);

		$tax = new Zend_Form_Element_Text('tax');
    	$tax->setDecorators(array('ViewHelper','Description'));
    	$tax->setLabel('Credits');
    	$tax->class = "form-control";
    	$tax->setRequired(false);
    	
    	//Add Validator
    	$tax->addValidator(new Zend_Validate_Float());
    	//Add Filter
    	$tax->addFilter(new Zend_Filter_HtmlEntities());
    	$tax->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($tax);
    	
    	//Create favorite radio buttons.
    	$accounttype = new Zend_Form_Element_Select('account');
    	$roles = array("2" => "SMPP TRANSACTIONAL",
    		       "1" => "SMPP-PROMOTIONAL",
    		       "3" => "SMPP-TRANS-SCRUB",
    		       "6" => "SMS-TRANSACTIONAL",
    				"12" => "SMS-TRANS-SCRUB",
    				"26" => "SMS-INTERNATIONAL",
    		       "7" => "SMS-PROMOTIONAL",
    		       "5" => "VOICE-TRANSACTIONAL",
    		       "4" => "VOICE-PROMOTIONAL"
    	);
    	$accounttype->addMultiOptions($roles);
		$accounttype->class = "form-control select2 custom-select";	
		$accounttype->setLabel('Account Type');
		$accounttype->setDecorators(array('ViewHelper','Description'));
		$accounttype->setRequired(true);
		
		//Add Validator
		//Add Filter
		$accounttype->addFilter(new Zend_Filter_HtmlEntities());
		$accounttype->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($accounttype);
		
		$typeOptions = array("1" => "Add",
				     "2" => "Deduct");
		$modetype = new Zend_Form_Element_Radio('mode');
		$modetype->addMultiOptions($typeOptions)->setSeparator('');
		$modetype->setDecorators(array('ViewHelper','Description'));
		$modetype->setLabel('Role Type');
		$modetype->setRequired(true);
		//Add Account type
		$this->addElement($modetype);
		
		/*$captcha = new Zend_Form_Element_Captcha(
				'captcha', // This is the name of the input field
				array(
						'captcha' => array( // Here comes the magic...
								// First the type...
								'captcha' => 'Image',
								// Length of the word...
								'wordLen' => 6,
								// Captcha timeout, 5 mins
								'timeout' => 300,
								// What font to use...
								'font' => '/var/www/html/webpanel/public/captcha/fonts/aller.ttf',
								// Where to put the image
								'imgDir' => '/var/www/html/webpanel/public/captcha/image',
								// URL to the images
								// This was bogus, here's how it should be... Sorry again :S
								'imgUrl' => '/captcha/image',
								'fontSize' => '24',
								'height' => '80',
								'width' => '185',
								'dotNoiseLevel' => '0',
    							'lineNoiseLevel' => '0',
    					)));
								$captcha->setDecorators(array('ViewHelper','Description'));
										$captcha->removeDecorator('ViewHelper','Description');
												$captcha->class = "form-control";
												 
														$this->addElement($captcha);*/
		
    	//Create a submit button.
    	$this->addElement('submit', 'transfer');
    	$submitElement = $this->getElement('transfer');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper','Description'));
		$submitElement->setLabel('Transfer');
    	
    	$this->setDecorators(array('FormElements',
    			'Form'
    	));
    }

}

