<?php

class User_Form_NewUser extends Zend_Form
{
    public function init()
    {
        $this->setAction('signup');
    	$this->setMethod('post');
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    
    	$username = new Zend_Form_Element_Text('username');
    	$username->setDecorators(array('ViewHelper',
						    			'Description',
						    			array(array('data'=>'HtmlTag'), array('tag' => 'td')),
						    			array('Label', array('tag' => 'td')),
						    			array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
						    	));
    	$username->setLabel('Email');
    	$username->setAttrib('size', 30);
    	//$username->setAttrib('placeholder', 'Email ID');
    	$username->class = "text-input";
    	//$username->addErrorMessage('Please enter a username');
    	$username->setRequired(true);
    
    	//Add Validator
    	//$username->addValidator(new Zend_Validate_StringLength(4,20));
    	$username->addValidator(new Zend_Validate_EmailAddress());
    	$username->addValidator(new Smpp_Validate_Db_NoUserExists());
    	
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);
    	
    	//Create Password Object.
    	$password = new Zend_Form_Element_Password('password');
    	$password->setDecorators(array('ViewHelper',
						    			'Description',
						    			 
						    			array(array('data'=>'HtmlTag'), array('tag' => 'td')),
						    			array('Label', array('tag' => 'td')),
						    			array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
						    	));
    	$password->setLabel('Password');
    	$password->setAttrib('size', 30);
    	//$password->setAttrib('placeholder', 'Password');
    	$password->class = "text-input";
    	$password->setRequired(true);
    	//Add Validator
    	 
    	//Add Filter
    	$password->addFilter(new Zend_Filter_HtmlEntities());
    	$password->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($password);
    	
    	//Create Password Object.
    	$repassword = new Zend_Form_Element_Password('repassword');
    	$repassword->setDecorators(array('ViewHelper',
						    			'Description',
						    	
						    			array(array('data'=>'HtmlTag'), array('tag' => 'td')),
						    			array('Label', array('tag' => 'td')),
						    			array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
						    	  ));
    	$repassword->setLabel('Confirm');
    	$repassword->setAttrib('size', 30);
    	//$repassword->setAttrib('placeholder', 'Confirm Password');
    	$repassword->class = "text-input";
    	$repassword->setRequired(true);
    	//Add Validator
    	$repassword->addValidator('Identical', false, array('token' => 'password'));
    	//Add Filter
    	$repassword->addFilter(new Zend_Filter_HtmlEntities());
    	$repassword->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($repassword);
    	
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"button");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors', array(array('data'=>'HtmlTag'), array('tag' => 'td',
							    					'colspan'=>'2','align'=>'right')),
							    			array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
							    	));
    	$submitElement->setLabel('Sign Up');
    	
    	$this->setDecorators(array('FormElements',
					    			array(array('data'=>'HtmlTag'),array('tag'=>'table')),
					    			'Form'
					    	));
    }


}

