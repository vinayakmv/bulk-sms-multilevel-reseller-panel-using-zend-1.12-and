<?php
use modules\user\models\DLRStatusManager;
use modules\user\models\ActivityLogManager;
use modules\smpp\models\SmppErrorCodeRequestManager;
use modules\smpp\models\SmppErrorMapTransManager;
use modules\smpp\models\SmppErrorMapPromoManager;
class System_DlrstatusController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $broadcast = $this->view->navigation()->findOneByLabel('Disposition');
        if ($broadcast) {
            $broadcast->setActive();
        }
        $action = $this->getRequest()->getActionName();
        $emUser = $this->getEntityManager('user');
        $domain = Smpp_Utility::getFqdn();
        $domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
        $domainLayout = $domainRepo->layout;
        if ($domainLayout != null){
        	$view = $domainLayout.'-'.$action;
        	$this->_helper->viewRenderer("$view");
        }
        parent::init();
    }

    public function indexAction()
    {
        $this->view->subject="ADD DLR STATUS";
        $page = $this->view->navigation()->findOneByUri('/system/dlrstatus/index');
        if ($page) {
            $page->setActive();
            $this->view->headTitle('Disposition | Sms');
        }

        $form = new System_Form_Dlrstatus();
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $emUser = $this->getEntityManager('user');
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId); 
        $this->view->form = $form;
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
                         
            $getValues = $form->getValues();
            //$errorCode=urlencode($getValues['errorcode']);
            //$getValues['errorcode'] = $errorCode;
            $dlrStatusManager = new DLRStatusManager();
            $insertDlrStatus = $dlrStatusManager->insertDlrStatus($getValues,$userId, $emUser);
            if ($insertDlrStatus) {
                $this->view->success = array('Campaign' => array('isCreated' => 'Campaign created successfully'));
            }
        }
        else{
            $this->view->errors = $form->getMessages();
        }
        $form->reset();
    }

    public function viewAction()
    {
        $this->view->subject="DLR STATUS VIEW";
         $page = $this->view->navigation()->findOneByUri('/system/dlrstatus/view');
        if ($page) {
            $page->setActive();
            $this->view->headTitle('Disposition | Sms List');
        }

        //$reportManager = new SendSmsManager();
        $emUser = $this->getEntityManager('user');
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        if ($this->getRequest()->isPost()) {

        }
        $queryBuilder = $emUser->createQueryBuilder();
        $queryBuilder->select('e')
        ->from('modules\user\models\DLRStatus', 'e')
        ->where('e.status = ?1')
        ->orderBy('e.id', 'DESC')
        ->setParameter(1, 1);
        $query = $queryBuilder->getQuery();
	  	$this->view->entries = $query->getResult();
        /*
        $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
        $currentPage = 1;
        $i = $this->_request->getParam('p');
        if(!empty($i)){   //Where i is the current page
            $currentPage = $this->_request->getParam('p');
        }
        $perPage = 10;
        $paginator->setCurrentPageNumber($currentPage)
        ->setItemCountPerPage($perPage);
        $this->view->result1 = $paginator;
        if($i==1 || $i=='')
        {
            $this->view->pageIndex = $i;
        }
        else
        {
            $this->view->pageIndex = (($i-1)*$perPage)+1;
        }   
    
        $this->view->entries = $paginator;
        */
        
        $this->view->userId = $userId;
    }

    public function voiceviewAction()
    {
        $this->view->subject="DLR STATUS VIEW";
         $page = $this->view->navigation()->findOneByUri('/system/dlrstatus/voiceview');
        if ($page) {
            $page->setActive();
            $this->view->headTitle('Disposition | Voice List');
        }

        //$reportManager = new SendSmsManager();
        $emUser = $this->getEntityManager('user');
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        if ($this->getRequest()->isPost()) {

        }
        $queryBuilder = $emUser->createQueryBuilder();
        $queryBuilder->select('e')
        ->from('modules\user\models\DispositionStatus', 'e')
        ->where('e.status = ?1')
        ->orderBy('e.id', 'DESC')
        ->setParameter(1, 1);
        $query = $queryBuilder->getQuery();
        $this->view->entries = $query->getResult();
        /*
        $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
        $currentPage = 1;
        $i = $this->_request->getParam('p');
        if(!empty($i)){   //Where i is the current page
            $currentPage = $this->_request->getParam('p');
        }
        $perPage = 10;
        $paginator->setCurrentPageNumber($currentPage)
        ->setItemCountPerPage($perPage);
        $this->view->result1 = $paginator;
        if($i==1 || $i=='')
        {
            $this->view->pageIndex = $i;
        }
        else
        {
            $this->view->pageIndex = (($i-1)*$perPage)+1;
        }   
    
        $this->view->entries = $paginator;
        */
        
        $this->view->userId = $userId;
    }
    
    public function smppAction()
    {
    	$this->view->subject="SMPP DLR";
    	$page = $this->view->navigation()->findOneByuri('/system/dlrstatus/smpp');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Disposition | Smpp Dlr');
    	}
    	
    	$form = new System_Form_DlrMap();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	/*
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	$transUsers =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId);
    	$smppRoutes = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>'TRANSACTIONAL'));
    	
    	foreach ($smppRoutes as $routes){
    		$route[$routes->route]=$routes->name;
    	}
    	
    	$user['all'] = "ALL";
    	foreach ($transUsers as $users)
    	{
    		$user[$users['system_id']]= $users['system_id'];
    	}
    	 
    	if (isset($user))
    	{
    		$form->getElement('user')->setMultiOptions($user);
    	}
    	if (isset($route))
    	{
    		$form->getElement('route')->setMultiOptions($route);
    	}
    	*/
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    	
    		$getValues = $form->getValues();
    		//$systemId=$getValues['user'];
    		//$getUserId =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId, $systemId);
    	
    		$SmppErrorManager = new SmppErrorCodeRequestManager();
    		$createCode = $SmppErrorManager->createCode($getValues, $userId, $emSmpp);
    	
    		if ($createCode) {
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Error Code Created Successfully', $this->_domain, $this->_ip, $this->_device, $createCode->id, 'CREATE', 'SMPP ERROR CODE', $emUser);
    			 
    			$this->view->success = 'smpp dlr created';
    		}
    		else{
    			$this->view->errors = 'failed';
    		}
    	}else{
    		$this->view->errors = $form->getMessages();
    	}
    	
    	$form->reset();
    }
    
    public function smppviewAction()
    {
    	$this->view->subject="SMPP DLR LIST";
    	
    	$page = $this->view->navigation()->findOneByuri('/system/dlrstatus/smppview');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Smpp Dlr List');
    	}
    	 
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$smppPrefixRouteRepo = $emSmpp->getRepository('modules\smpp\models\SmppErrorCodeRequest')->findAll();
    
    	$this->view->entries = $smppPrefixRouteRepo;
    	 
    	$smppUserTransIds = array();
    	$routes = array();
    	$esmes = '<option value="TRANSACTIONAL">TRANSACTIONAL</option>'
                . '<option value="PROMOTIONAL">PROMOTIONAL</option>'
                . '<option value="INTERNATIONAL">INTERNATIONAL</option>';
    	
    	$this->view->esmes = $esmes;
    	 
    	$routeTransRepo = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>'TRANSACTIONAL'));
        $routePromoRepo = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>'PROMOTIONAL'));
    	$routes = '<option value="all">all</option>';
        foreach ($routePromoRepo as $promoRoute){
    		$routes .= '<option value="'.$promoRoute->route.'|'.$promoRoute->type.'">'.$promoRoute->route.'|'.$promoRoute->type.'</option>';
    	}
    	foreach ($routeTransRepo as $transRoute){
    		$routes .= '<option value="'.$transRoute->route.'|'.$transRoute->type.'">'.$transRoute->route.'|'.$transRoute->type.'</option>';
    	}
        
    	$this->view->routes = $routes;
    	 
    	if ($this->getRequest()->isPost()) {
    		 
    		$getValues = $this->getRequest()->getPost();
    		
                $type =  $getValues['esme'];
                if ($type == 'TRANSACTIONAL'){
                    $SmppErrorManager=new SmppErrorMapTransManager();
                }else{
                    $SmppErrorManager=new SmppErrorMapPromoManager();
                }
    		
    		if ($getValues['submit'] == 'Activate'){
                    
    			$createPrefix = $SmppErrorManager->insert($getValues, $emSmpp);
    		}else{
    			$createPrefix = $SmppErrorManager->delete($getValues, $emSmpp);
    		}
    		
    	
    		if ($createPrefix) {
                    
    			$this->view->success = 'Prefix route added';
    		}else{
    			$this->view->errors = 'Failed to add prefix route';
    		}
    	}
    }

}





