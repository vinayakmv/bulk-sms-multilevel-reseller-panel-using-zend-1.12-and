<?php
use Doctrine\ORM\Query\ResultSetMapping;
use modules\user\models\LicenseManager;
class System_LicenseController extends Smpp_Controller_BaseController
{

  public function init()
  {
    $broadcast = $this->view->navigation()->findOneByLabel('License');
     if ($broadcast) {
      $broadcast->setActive();
    }
    parent::init();
  }

  public function indexAction()
  {
          // action body
  }

  public function createAction()
  {
    $this->view->subject="CREATE LICENSE ";
    $page = $this->view->navigation()->findOneByLabel('Create');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('License | Create');
    }
    $date=date('Y-m-d');
    $this->view->date=$date;
    $form = new System_Form_License();
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    $emUser = $this->getEntityManager('user');
    $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);

    //**************FIND USER DETAILS START ****************************\\  
    $usersRepo=$emUser->getRepository('Models\User')->findBy(array('reseller' =>$userId)); 

    $users[''] = 'Select';
    foreach ($usersRepo as $usersId)
    {

      $usersIdList=$usersId->id;
      $userName=$usersId->username;
      $domainName=$usersId->domain->domain;
      $users[$usersIdList] =$userName." - ".$domainName ;
    }
    if (isset($users))
    {
      $form->getElement('selectUserName')->setMultiOptions($users);
    }

        //**************FIND USER DETAILS STOP ****************************\\ 
    $this->view->form = $form;
    if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        //$getValues = $form->getValues();
      $getValues = $this->getRequest()->getPost();

      $LicenseManager = new LicenseManager();
      $licenseCreated = $LicenseManager->createLicense($getValues, $userId, $emUser);
      if($licenseCreated){
        $this->view->success = array('license' => array('isCreated' => 'License created successfully'));
      }
      else{
        $this->view->errors = array('license' => array('isFailed' => 'Wrong !! The license is not updated'));
      }

    }else{
      $this->view->errors = $form->getMessages();
    }
    $form->reset();


  }
  public function viewAction()
  {
   

    $this->view->subject="LICENSE VIEW";
    
    $page = $this->view->navigation()->findOneByuri('/system/license/view');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('License | View');
    }

          //$reportManager = new SendSmsManager();
    $emUser = $this->getEntityManager('user');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    if ($this->getRequest()->isPost()) {
      $getValues = $this->getRequest()->getPost();
      $licenseId=$getValues['uniqId'];
      $licenseRepo = $emUser->getRepository('modules\user\models\License')->find($licenseId);
      $LicenseManager = new LicenseManager();
      //print_r($getValues );exit();
      if(isset($getValues['delete'])){

        $deleteMachineId = $LicenseManager->deleteMachineId($licenseRepo, $userId, $emUser);
        $this->view->Success = array('Manage' => array('status' => 'Delete successfully'));

      }
      else if(isset($getValues['edit'])){
        $updateLicense = $LicenseManager->updateLicense($licenseRepo, $getValues, $userId, $emUser);
        $this->view->Success = array('Manage' => array('status' => 'Update successfully'));
      }
      else if(isset($getValues['block'])){
        $blockLicense = $LicenseManager->blockLicense($licenseRepo, $userId, $emUser);
        $this->view->Success = array('Manage' => array('status' => 'License blocked'));
      }
      else if(isset($getValues['unblock'])){
        $unblockLicense = $LicenseManager->unblockLicense($licenseRepo, $userId, $emUser);
        $this->view->Success = array('Manage' => array('status' => 'License unblocked'));
      }

    }
    $queryBuilder = $emUser->createQueryBuilder();
    $queryBuilder->select('e')
    ->from('modules\user\models\License', 'e')
    ->where('e.username = ?1')
    ->orderBy('e.id', 'DESC')
    ->setParameter(1, $userId);
    $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder ));
    $currentPage = 1;
    //$i = $this->_request->getParam('i');
    $i = $this->_request->getParam('p');
    if(!empty($i)){ //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i=='')
    {
      $this->view->pageIndex = $i;
    }
    else
    {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    }   

    $this->view->entries = $paginator;
    $this->view->userId = $userId;
  }
  public function viewallAction()
  {

    $this->view->subject="LICENSE VIEW";
    
    $page = $this->view->navigation()->findOneByuri('/system/license/viewall');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('License | View');
    }

          //$reportManager = new SendSmsManager();
    $emUser = $this->getEntityManager('user');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    if ($this->getRequest()->isPost()) {
      
      $getValues = $this->getRequest()->getPost();
      $licenseId=$getValues['uniqId'];
      $licenseRepo = $emUser->getRepository('modules\user\models\License')->find($licenseId);
      $LicenseManager = new LicenseManager();
      //print_r($getValues );exit();
      if(isset($getValues['delete'])){

        $deleteMachineId = $LicenseManager->deleteMachineId($licenseRepo, $userId, $emUser);
        $this->view->Success = array('Manage' => array('status' => 'Delete successfully'));

      }
      else if(isset($getValues['edit'])){
        $updateLicense = $LicenseManager->updateLicense($licenseRepo, $getValues, $userId, $emUser);
        $this->view->Success = array('Manage' => array('status' => 'Update successfully'));
      }
      else if(isset($getValues['block'])){
        $blockLicense = $LicenseManager->blockLicense($licenseRepo, $userId, $emUser);
        $this->view->Success = array('Manage' => array('status' => 'License blocked'));
      }
      else if(isset($getValues['unblock'])){
        $unblockLicense = $LicenseManager->unblockLicense($licenseRepo, $userId, $emUser);
        $this->view->Success = array('Manage' => array('status' => 'License unblocked'));
      }

    }
    $queryBuilder = $emUser->createQueryBuilder();
    $queryBuilder->select('e')
    ->from('modules\user\models\License', 'e')
    ->orderBy('e.id', 'DESC');
    $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder ));
    $currentPage = 1;
    //$i = $this->_request->getParam('i');
    $i = $this->_request->getParam('p');
    if(!empty($i)){ //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i==''){
      $this->view->pageIndex = $i;
    }else{
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    }   

    $this->view->entries = $paginator;
    $this->view->userId = $userId;
  }

}
