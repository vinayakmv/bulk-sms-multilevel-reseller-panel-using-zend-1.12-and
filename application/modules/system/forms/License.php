<?php

class System_Form_License extends Zend_Form
{

    public function init()
    {
        $this->setAction('/system/license/create');
        $this->setMethod('post');
        $this->setAttrib('class', 'form-adduser');

        //dropdown type
        $selectUserName = new Zend_Form_Element_Select('selectUserName');
        $selectUserName->setDecorators(array('ViewHelper','Description'));
        $selectUserName->setLabel('Select Audio');
        $selectUserName->setAttrib('tabindex', '1');
        $selectUserName->id = "selectUserName";
        $selectUserName->class = "form-control select2";
        $selectUserName->setRequired(true);
        $selectUserName->setAttrib('autofocus', 'autofucus');
        $selectUserName->addFilter(new Zend_Filter_HtmlEntities());
        $selectUserName->addFilter(new Zend_Filter_StripTags());
        $this->addElement($selectUserName);

        //textfield companyName
        $companyName = new Zend_Form_Element_Text('companyName');
        $companyName->class = "form-control";
        $companyName->setDecorators(array('ViewHelper','Description'));
        $companyName->setAttrib('placeholder', 'Enter companyName');
        $companyName->setAttrib('required', 'required');
        $companyName->addFilter(new Zend_Filter_HtmlEntities());
        $companyName->addFilter(new Zend_Filter_StripTags());
        $companyName->setAttrib('tabindex', '2');
        $companyName->setRequired(true);
        $this->addElement($companyName);

        //textfield serverIP
        $serverIP = new Zend_Form_Element_Text('serverIP');
        $serverIP->class = "form-control";
        $serverIP->setDecorators(array('ViewHelper','Description'));
        $serverIP->setAttrib('placeholder', 'Enter serverIP');
        $serverIP->setAttrib('required', 'required');
        $serverIP->addFilter(new Zend_Filter_HtmlEntities());
        $serverIP->addFilter(new Zend_Filter_StripTags());
        $serverIP->setAttrib('tabindex', '1');
        $serverIP->addValidator('Digits');
        $serverIP->setRequired(true);
        $this->addElement($serverIP);


          //textfield errorcode
       /* $errorcode = new Zend_Form_Element_Text('errorcode');
        $errorcode->class = "form-control";
        $errorcode->setDecorators(array('ViewHelper','Description'));
        $errorcode->setAttrib('placeholder', 'Enter Errorcode');
        $errorcode->setAttrib('required', 'required');
        $errorcode->addFilter(new Zend_Filter_HtmlEntities());
        $errorcode->addFilter(new Zend_Filter_StripTags());
        $errorcode->setAttrib('tabindex', '1');
        $errorcode->setRequired(true);
        $this->addElement($errorcode);*/


        $this->addElement('submit', 'submit');
        $submitElement = $this->getElement('submit');
        $submitElement->setAttrib('class',"btn-primary btn");
        $submitElement->setDecorators(array('ViewHelper','Description'));
        $submitElement->setLabel('Create');
         
        $this->setDecorators(array('FormElements',
                                   'Form'
        ));

    }


}

