<?php

class System_Form_Dlrstatus extends Zend_Form
{

    public function init()
    {
        $this->setAction('/system/dlrstatus');
        $this->setMethod('post');
        $this->setAttrib('class', 'form-adduser');

        //textfield errorcode
        $errorcode = new Zend_Form_Element_Text('errorcode');
        $errorcode->class = "form-control";
        $errorcode->setDecorators(array('ViewHelper','Description'));
       // $errorcode->setAttrib('placeholder', 'Enter Errorcode');
        $errorcode->setAttrib('required', 'required');
        $errorcode->addFilter(new Zend_Filter_HtmlEntities());
        $errorcode->addFilter(new Zend_Filter_StripTags());
        $errorcode->setAttrib('autofocus', 'autofucus');
        $errorcode->setAttrib('tabindex', '1');
        $errorcode->setRequired(true);
        $this->addElement($errorcode);

        //dropdown type
        $dlrstatus = new Zend_Form_Element_Text('dlrstatus');
        $dlrstatus->class = "form-control";
        $dlrstatus->setDecorators(array('ViewHelper','Description'));
        //$dlrstatus->setAttrib('placeholder', 'Enter Status');
        $dlrstatus->setAttrib('required', 'required');
        $dlrstatus->addFilter(new Zend_Filter_HtmlEntities());
        $dlrstatus->addFilter(new Zend_Filter_StripTags());
        $dlrstatus->setAttrib('tabindex', '2');
        $dlrstatus->setRequired(true);
        $this->addElement($dlrstatus);

        $this->addElement('submit', 'submit');
        $submitElement = $this->getElement('submit');
        $submitElement->setAttrib('class',"btn-primary btn");
        $submitElement->setDecorators(array('ViewHelper','Description'));
        $submitElement->setLabel('Create');
         
        $this->setDecorators(array('FormElements',
                                   'Form'
        ));

    }


}

