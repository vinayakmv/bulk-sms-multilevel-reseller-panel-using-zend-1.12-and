<?php

class Voice_Form_Wizone extends Zend_Form
{

    public function init()
    {
        $this->setAction('wiztwo');
    	$this->setMethod('post');
    
    	$campaign = new Zend_Form_Element_Text('campaign');
    	$campaign->setDecorators(array('ViewHelper',
						    			'Description',
						    	
						    			array(array('data'=>'HtmlTag'), array('tag' => 'td')),
						    			array('Label', array('tag' => 'td')),
						    			array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
						    	));
    	$campaign->setLabel('Campaign Name');
    	$campaign->class = "text-input";
    	$campaign->setAttrib('size', 30);
    	$campaign->setRequired(true);
    	
    	//Add Validator
    	//$campaign->addValidator(new Zend_Validate_StringLength(4,20));
    	//Add Filter
    	$campaign->addFilter(new Zend_Filter_HtmlEntities());
    	$campaign->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($campaign);
    	
    	//Create favorite radio buttons.
		$typeOptions = array("multiOptions" =>array("1" => "Upload File",
													"2" => "Select File",
													"3" => "Text2Speech"));
		$type = new Zend_Form_Element_Radio('Type',$typeOptions);
		$type->setDecorators(array('ViewHelper',
									'Description',
							
									array(array('data'=>'HtmlTag'), array('tag' => 'td')),
									array('Label', array('tag' => 'td')),
									array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
							));
		$type->setLabel('Audio Type');
		$type->setRequired(true);
		//Add Account type
		$this->addElement($type);
    	
		$uploadfile = new Zend_Form_Element_File('uploadfile');
		$uploadfile->setDecorators(array('File',
		
				array(array('data'=>'HtmlTag'), array('tag' => 'td')),
				array('Label', array('tag' => 'td')),
				array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
		));
		$uploadfile->setLabel('Upload File ');
		$uploadfile->class = "text-input";
		$uploadfile->setDestination('./audioFiles/');
		$uploadfile->addValidator('Count', false, 1);
		$uploadfile->addValidator('Extension', false, 'mp3,wav');
		$uploadfile->setMaxFileSize(2097152);
		$uploadfile->setRequired(true);
		
		$this->addElement($uploadfile);
		
		$selectfile = new Zend_Form_Element_Text('selectfile');
		$selectfile->setDecorators(array('ViewHelper',
				'Description',
		
				array(array('data'=>'HtmlTag'), array('tag' => 'td')),
				array('Label', array('tag' => 'td')),
				array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
		));
		$selectfile->setLabel('Select File ');
		$selectfile->setAttrib('size', 30);
		$selectfile->class = "text-input";
		
		$selectfile->setRequired(true);
		//Add Validator
		$selectfile->addValidator(new Zend_Validate_EmailAddress());
		//Add Filter
		$selectfile->addFilter(new Zend_Filter_HtmlEntities());
		$selectfile->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectfile);
		
		$text2speech = new Zend_Form_Element_Textarea('text2speech');
		$text2speech->setDecorators(array('ViewHelper',
				'Description',
		
				array(array('data'=>'HtmlTag'), array('tag' => 'td')),
				array('Label', array('tag' => 'td')),
				array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
		));
		$text2speech->setLabel('Text2speech');
		//$plaintext->setRequired(true);
		$text2speech->setAttrib('rows', '10');
		$text2speech->setAttrib('cols', '30');
		$text2speech->setAttrib('id', "plaintext");
		$text2speech->class = "plaintext-input";
		
		//Add Validator
		//Add Filter
		$text2speech->addFilter(new Zend_Filter_HtmlEntities());
		$text2speech->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($text2speech);
		
		//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"button");
    	$submitElement->setDecorators(array('ViewHelper',
											'Description',
											'Errors', array(array('data'=>'HtmlTag'), array('tag' => 'td',
											'colspan'=>'2','align'=>'right')),
											array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
										));
		$submitElement->setLabel('Next');
    	
    	$this->setDecorators(array('FormElements',
    			array(array('data'=>'HtmlTag'),array('tag'=>'table')),
    			'Form'
    	));
    }


}

