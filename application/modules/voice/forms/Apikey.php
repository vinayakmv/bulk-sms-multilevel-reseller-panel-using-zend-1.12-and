<?php

class Voice_Form_Apikey extends Zend_Form
{

    public function init()
    {
        $this->setAction('index');
    	$this->setMethod('post');
    	$this->setAttrib('class', 'form-apikey');

    	$ip = new Zend_Form_Element_Text('ip');
		$ip->class = "text login_input span3";
    	$ip->setDecorators(array('ViewHelper','Description'));
    	$ip->setAttrib('placeholder', '255.255.255.255');
		$ip->setAttrib('required', 'required');
    	$ip->setRequired(true);
    	$this->addElement($ip);
    }
}

