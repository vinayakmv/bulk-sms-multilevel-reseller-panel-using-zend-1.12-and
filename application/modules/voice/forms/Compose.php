<?php

class Voice_Form_Compose extends Zend_Form
{

    public function init()
    {
        $this->setAction('/voice/campaign');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	$campaign = new Zend_Form_Element_Text('campaignName');
    	$campaign->setDecorators(array('ViewHelper','Description'));
    	$campaign->setLabel('Campaign Name');
    	$campaign->class = "form-control";
    	$campaign->setRequired(false);
    	
    	//Add Validator
    	//$campaign->addValidator(new Zend_Validate_StringLength(4,20));
    	//Add Filter
    	$campaign->addFilter(new Zend_Filter_HtmlEntities());
    	$campaign->addFilter(new Zend_Filter_StripTags());
    	$campaign->addFilter(new Zend_Filter_StringToUpper());
    	//Add Username Element
    	$this->addElement($campaign);
    
    	//Protection against CSRF attack
    	$token = new Zend_Form_Element_Hash('token',array('timeout' => '1800'));
    	$token->setSalt(md5(uniqid(rand(), TRUE)));
    	 
    	//$token->setTimeout(Globals::getConfig()->authentication->timeout);
    	$token->setDecorators(array('ViewHelper','Description'));
    	$this->addElement($token);
    	/*
    	$captcha = new Zend_Form_Element_Captcha(
    			'captcha', // This is the name of the input field
    			array(
    					'captcha' => array( // Here comes the magic...
    							// First the type...
    							'captcha' => 'Image',
    							// Length of the word...
    							'wordLen' => 6,
    							// Captcha timeout, 5 mins
    							'timeout' => 300,
    							// What font to use...
    							'font' => '/var/www/html/webpanel/public/captcha/fonts/aller.ttf',
    							// Where to put the image
    							'imgDir' => '/var/www/html/webpanel/public/captcha/image',
    							// URL to the images
    							// This was bogus, here's how it should be... Sorry again :S
    							'imgUrl' => '/captcha/image',
    							'fontSize' => '24',
    							'height' => '80',
    							'width' => '165',
    							'dotNoiseLevel' => '0',
    							'lineNoiseLevel' => '0',
    					)));
    							$captcha->setDecorators(array('ViewHelper','Description'));
    									$captcha->removeDecorator('ViewHelper','Description');
    											$captcha->class = "form-control";
    												
						$this->addElement($captcha);
    	*/
    	$scheduleRadio = new Zend_Form_Element_Checkbox('scheduleRadio',array('onclick' =>
    	 	'if(this.checked){ 
    	 		document.getElementById("datepic").style.display="inline";
      			document.getElementById("timepic").style.display="inline";
      			document.getElementById("datePicker").required = true;
      			document.getElementById("timePicker").required = true;
      		}else{
      			document.getElementById("datepic").style.display="none";
  				document.getElementById("timepic").style.display="none";
      			document.getElementById("datePicker").required = false;
      			document.getElementById("timePicker").required = false;
      		}'
      	));
    	$scheduleRadio->setDecorators(array('ViewHelper','Description'));
    	$scheduleRadio->class = "form-check-input";
		$scheduleRadio->setCheckedValue("on");
		$scheduleRadio->setUncheckedValue("off");
		$this->addElement($scheduleRadio);
    	
    	$timePicker = new Zend_Form_Element_Text('timePicker');
        $timePicker->class = "form-control";
        $timePicker->setDecorators(array('ViewHelper','Description'));
        //$timePicker->setAttrib('readonly', 'readonly');
        //$timePicker->setAttrib('placeholder', date('H:i'));
        $timePicker->setAttrib('data-target', "#timePicker");
		$timePicker->class = "form-control datetimepicker-input timepicker-24";
        $timePicker->addFilter(new Zend_Filter_HtmlEntities());
        $timePicker->addFilter(new Zend_Filter_StripTags());
        $this->addElement($timePicker);
        
        $datePicker = new Zend_Form_Element_Text('datePicker');
        $datePicker->class = "form-control";
        //$datePicker->name = "from";
        $datePicker->setDecorators(array('ViewHelper','Description'));
        //$datePicker->setAttrib('placeholder', date('m/d/Y'));
       //$datePicker->setValue(date('m/d/Y'));
        $datePicker->addFilter(new Zend_Filter_HtmlEntities());
        $datePicker->addFilter(new Zend_Filter_StripTags());
        $this->addElement($datePicker);
    	
    	/*$voicetypeOptions = array("1" => "Promotional",
    			"2" => "Transactional");
    	$voicetype = new Zend_Form_Element_Radio('voiceType');
    	$voicetype->setMultiOptions($voicetypeOptions)->setSeparator('&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp')->setValue("1");
    	$voicetype->setDecorators(array('ViewHelper','Description'));
    	$voicetype->setLabel('Voice Route');
    	//$audiotype->id = "inlineradio";
    	$voicetype->setRequired(true);
    	//Add Account type
    	$this->addElement($voicetype);*/

    	$voicetype = new Zend_Form_Element_Select('voiceType');
		$voicetype->setDecorators(array('ViewHelper','Description'));
		$voicetype->setLabel('Select Audio');
		$voicetype->id = "voicetype";
		$voicetype->class = "form-control select2 custom-select";
		$voicetype->setRequired(true);
		//$voicetype->addMultiOptions(array("1" => "PROMOTIONAL","2" => "TRANSACTIONAL"));
		$voicetype->addFilter(new Zend_Filter_HtmlEntities());
		$voicetype->addFilter(new Zend_Filter_StripTags());
		$this->addElement($voicetype);
    	
    	$campaignOptions = array("1" => "Broadcast",
    			"2" => "Survey");
    	$campaigntype = new Zend_Form_Element_Radio('campaignType');
    	$campaigntype->setMultiOptions($campaignOptions)->setSeparator('')->setValue("1");
    	$campaigntype->setDecorators(array('ViewHelper','Description'));
    	$campaigntype->setLabel('Campaign Type');
    	//$audiotype->id = "inlineradio";
    	$campaigntype->setRequired(true);
    	//Add Account type
    	$this->addElement($campaigntype);
    	
    	$selectDuration = new Zend_Form_Element_Select('selectDuration');
    	$durations = array("1" => "30 seconds",
    			"2" => "60 seconds",
    			"3" => "90 seconds",
    			"4" => "120 seconds");
    	$selectDuration->addMultiOptions($durations);
    	$selectDuration->setDecorators(array('ViewHelper','Description'));
    	$selectDuration->setLabel('Select Duration');
    	$selectDuration->class = "form-control select2 custom-select";
    	$selectDuration->setRequired(false);
    	//Add Validator
    	//Add Filter
    	$selectDuration->addFilter(new Zend_Filter_HtmlEntities());
    	$selectDuration->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($selectDuration);
    	
    	//Create favorite radio buttons.
		$audioOptions = array("1" => "Select Audio",
							  "2" => "Upload Audio",
							  "3" => "Text2Speech");
		$audiotype = new Zend_Form_Element_Radio('audioType');
		$audiotype->setMultiOptions($audioOptions)->setSeparator('')->setValue("1");
		$audiotype->setDecorators(array('ViewHelper','Description'));
		$audiotype->setLabel('Audio Type');
		//$audiotype->id = "inlineradio";
		$audiotype->setRequired(true);
		//Add Account type
		$this->addElement($audiotype);
    	
		$audio = new Zend_Form_Element_Text('audioName');
		$audio->setDecorators(array('ViewHelper','Description'));
		$audio->setLabel('Audio Name');
		$audio->class = "form-control";
		$audio->setRequired(false);
		$audio->setAttrib('placeholder', 'AUDIO NAME');
			
		//Add Validator
		//$campaign->addValidator(new Zend_Validate_StringLength(4,20));
		//Add Filter
		$audio->addFilter(new Zend_Filter_HtmlEntities());
		$audio->addFilter(new Zend_Filter_StripTags());
		$audio->addFilter(new Zend_Filter_StringToUpper());
		//Add Username Element
		$this->addElement($audio);
		
		$uploadAudio = new Zend_Form_Element_File('uploadAudio',array('style'=>'display:none;', 'onchange'=>'$("#upload-file-primary2").html($(this).val());'));
		$uploadAudio->setDecorators(array('File','Errors'));		
		$uploadAudio->setLabel('Upload Audio ');
		//$uploadAudio->class = "form-control";
		$uploadAudio->setDestination('../audioFiles/');
		$uploadAudio->addValidator('Count', false, 1);
		$uploadAudio->class = "file-upload-default";
		$uploadAudio->addValidator('Extension', false, 'mp3,wav');
		$uploadAudio->setMaxFileSize(10485760);
		$uploadAudio->setRequired(false);
		
		$this->addElement($uploadAudio);
		
		//selectSenderId		
		$selectSenderId = new Zend_Form_Element_Select('selectSenderId');
		$selectSenderId->setDecorators(array('ViewHelper','Description'));
		$selectSenderId->setLabel('Select Sender');
		$selectSenderId->id = "selectSenderId";
		$selectSenderId->class = "form-control select2 custom-select";
		//$selectSenderId->setRequired(true);
		$selectSenderId->addFilter(new Zend_Filter_HtmlEntities());
		$selectSenderId->addFilter(new Zend_Filter_StripTags());
		$this->addElement($selectSenderId);
		
		$typeSenderid = new Zend_Form_Element_Text('typeSenderId');
		$typeSenderid->setDecorators(array('ViewHelper','Description'));
		$typeSenderid->setLabel('Select Sender');
		$typeSenderid->class = "form-control";
		//$typeSenderid->setRequired(true);
		$typeSenderid->addFilter(new Zend_Filter_HtmlEntities());
		$typeSenderid->addFilter(new Zend_Filter_StripTags());
		$this->addElement($typeSenderid);
		
		//Select Audio
		$selectAudio = new Zend_Form_Element_Select('selectAudio');
		$selectAudio->setDecorators(array('ViewHelper','Description'));
		$selectAudio->setLabel('Select Audio');
		$selectAudio->id = "selectAudio";
		$selectAudio->class = "form-control select2 custom-select";
		
		$selectAudio->setRequired(false);
		//Add Validator
		//Add Filter
		$selectAudio->addFilter(new Zend_Filter_HtmlEntities());
		$selectAudio->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectAudio);
		
		$text2speech = new Zend_Form_Element_Textarea('text2speech');
		$text2speech->setDecorators(array('ViewHelper','Description'));
		$text2speech->setLabel('Text2speech');
		//$plaintext->setRequired(true);
		$text2speech->setAttrib('rows', '10');
		//$text2speech->setAttrib('cols', '30');
		$text2speech->class = "form-control";
		
		$text2speech->setRequired(false);
		//Add Validator
		//Add Filter
		$text2speech->addFilter(new Zend_Filter_HtmlEntities());
		$text2speech->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($text2speech);
		
		$selectVoice = new Zend_Form_Element_Select('selectVoice');
		$voices = array("voice_nitech_us_slt_arctic_hts" => "FEMALE US");
		$selectVoice->addMultiOptions($voices);
		$selectVoice->setDecorators(array('ViewHelper',
										  'Description'));
		$selectVoice->setLabel('Select Voice');
		$selectVoice->class = "form-control select2 custom-select";
		$selectVoice->setAttrib('style', 'width:100%');
		
		$selectVoice->setRequired(false);
		//Add Validator
		//Add Filter
		$selectVoice->addFilter(new Zend_Filter_HtmlEntities());
		$selectVoice->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectVoice);
		
		


		$saveAudio = new Zend_Form_Element_Checkbox('saveAudio',array('onclick' =>
    	 	'if(this.checked){ 
    	 		document.getElementById("audioOpt").style.display="inline";
      			document.getElementById("audioName").required = true;
      		}else{
      			document.getElementById("audioOpt").style.display="none";
      			document.getElementById("audioName").required = false;
      		}'
      	));
    	$saveAudio->setDecorators(array('ViewHelper','Description'));
    	$saveAudio->class = "form-check-input";
		$saveAudio->setCheckedValue("Yes");
		$saveAudio->setUncheckedValue("No");
		$this->addElement($saveAudio);


		
		//Create favorite radio buttons.
		$contactOptions = array("3" => "Paste Contacts",
								"2" => "Upload Contacts",
								"1" => "Group Contacts");
		$contacttype = new Zend_Form_Element_Radio('contactType');
		$contacttype->setMultiOptions($contactOptions)->setSeparator('')->setValue("3");
		$contacttype->setDecorators(array('ViewHelper','Description'));
		$contacttype->setLabel('Contact Type');
		$contacttype->setRequired(true);
		//Add Account type
		$this->addElement($contacttype);
		
		$contact = new Zend_Form_Element_Text('contactName');
		$contact->setDecorators(array('ViewHelper','Description'));
		$contact->setLabel('Contact Name');
		$contact->class = "form-control";
		$contact->setRequired(false);
			
		//Add Validator
		//$campaign->addValidator(new Zend_Validate_StringLength(4,20));
		//Add Filter
		$contact->addFilter(new Zend_Filter_HtmlEntities());
		$contact->addFilter(new Zend_Filter_StripTags());
		$contact->addFilter(new Zend_Filter_StringToUpper());
		//Add Username Element
		$this->addElement($contact);
		
		$uploadContact = new Zend_Form_Element_File('uploadContact', array('style'=>'display:none;', 'onchange'=>'$("#upload-file-primary").html($(this).val());'));
		$uploadContact->setDecorators(array('File'));
		$uploadContact->setLabel('Upload Contacts');
		$uploadContact->class = "file-upload-default";
		$uploadContact->setDestination('../contactFiles/');
		$uploadContact->addValidator('Count', false, 1);
		$uploadContact->addValidator('Extension', false, 'csv');
		$uploadContact->setMaxFileSize(10485760);
		$uploadContact->setRequired(false);
		$this->addElement($uploadContact);
		
		$selectContact = new Zend_Form_Element_Textarea('selectContact');
		$selectContact->setDecorators(array('ViewHelper','Description'));
		$selectContact->setLabel('Select Contacts');
		$selectContact->class = "multi-select form-control";
		//$selectContact->class = "multi-select form-control";
		$selectContact->setAttrib('rows', '10');
		$selectContact->setAttrib('cols', '30');
		$selectContact->setRequired(false);
		$selectContact->addFilter(new Zend_Filter_HtmlEntities());
		$selectContact->addFilter(new Zend_Filter_StripTags());
		$this->addElement($selectContact);
		
		$textContact = new Zend_Form_Element_Textarea('textContact');
		$textContact->setDecorators(array('ViewHelper','Description'));
		$textContact->setLabel('Paste Contacts');
		$textContact->setAttrib('rows', '10');
		$textContact->setAttrib('cols', '30');
		$textContact->setAttrib('onKeyUp', "countLines(this)");
		$textContact->class = "form-control";
		$textContact->setRequired(false);
		$textContact->addFilter(new Zend_Filter_HtmlEntities());
		$textContact->addFilter(new Zend_Filter_StripTags());
		$this->addElement($textContact);
		
		
		//Save Checkbox
		/*$saveOptions = array("1" => "Yes",
		                     "0" => "No");
		$saveContact = new Zend_Form_Element_Radio('saveContact');
		$saveContact->setMultiOptions($saveOptions)->setSeparator('&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp');
		$saveContact->setDecorators(array('ViewHelper','Description'));
		$saveContact->setLabel('Save Contacts');
		//$plaintext->setRequired(true);		
		$saveContact->setRequired(false);
		//Add Validator
		//Add Filter
		$saveContact->addFilter(new Zend_Filter_HtmlEntities());
		$saveContact->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($saveContact);*/
		
		//Create a submit button.
		$this->addElement('submit', 'vsend');
		$submitElement = $this->getElement('vsend');
		$submitElement->setAttrib('class',"btn-primary btn");
		$submitElement->setDecorators(array('ViewHelper','Description'));
		$submitElement->setLabel('Create Campaign');
		 
		$this->setDecorators(array('FormElements',
								   'Form'
		));

    }


}

