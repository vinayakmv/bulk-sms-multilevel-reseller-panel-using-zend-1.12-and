<?php

class Voice_Form_Sender extends Zend_Form
{

    public function init()
    {
        $this->setAction('request');
        $this->setMethod('post');
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        $this->setAttrib('class', "form-horizontal");

        //text area
        $senderId = new Zend_Form_Element_Textarea('senderId');
        $senderId->addValidator(new Smpp_Validate_VoiceSender());
        $senderId->addValidator(new Smpp_Validate_Db_NoVoiceSenderExists());
        $senderId->setDecorators(array('ViewHelper','Description'));
        $senderId  ->setAttrib('cols', '40')
                    ->setAttrib('required', 'required')
                    ->setAttrib('rows', '4');
		$senderId->setRequired(true);
        $senderId->addFilter(new Zend_Filter_HtmlEntities());
        $senderId->class = "form-control";
        $this->addElement($senderId);
        
        //text area
        $userremark = new Zend_Form_Element_Textarea('userremark');
        
        $userremark->setDecorators(array('ViewHelper','Description'));
        $userremark ->setAttrib('cols', '40')
                    ->setAttrib('rows', '4');
        $userremark->addFilter(new Zend_Filter_HtmlEntities());
        $userremark->class = "form-control";
        $this->addElement($userremark);

        $this->addElement('submit', 'submit');
        $submitElement = $this->getElement('submit');
        $submitElement->setAttrib('class',"btn  btn-primary ");
        $submitElement->setDecorators(array('ViewHelper','Description'));
        $submitElement->setLabel('Submit');
        $this->setDecorators(array('FormElements','Form'));
    }


}

