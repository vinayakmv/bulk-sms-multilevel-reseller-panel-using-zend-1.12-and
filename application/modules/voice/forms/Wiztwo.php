<?php

class Voice_Form_Wiztwo extends Zend_Form
{

    public function init()
    {
        $this->setAction('wizthree');
    	$this->setMethod('post');
    	
    	//Create favorite radio buttons.
		$typeOptions = array("multiOptions" =>array("1" => "Upload Contacts",
													"2" => "Select Contacts",
													"3" => "Paste Contacts"));
		$type = new Zend_Form_Element_Radio('Type',$typeOptions);
		$type->setDecorators(array('ViewHelper',
									'Description',
							
									array(array('data'=>'HtmlTag'), array('tag' => 'td')),
									array('Label', array('tag' => 'td')),
									array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
							));
		$type->setLabel('Contacts Type');
		$type->setRequired(true);
		//Add Account type
		$this->addElement($type);
		
		$contact = new Zend_Form_Element_Text('contact');
		$contact->setDecorators(array('ViewHelper',
				'Description',
					
				array(array('data'=>'HtmlTag'), array('tag' => 'td')),
				array('Label', array('tag' => 'td')),
				array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
		));
		$contact->setLabel('Contact Name');
		$contact->class = "text-input";
		$contact->setAttrib('size', 30);
		$contact->setRequired(true);
		 
		//Add Validator
		//$campaign->addValidator(new Zend_Validate_StringLength(4,20));
		//Add Filter
		$contact->addFilter(new Zend_Filter_HtmlEntities());
		$contact->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($contact);
		
		$uploadfile = new Zend_Form_Element_File('uploadfile');
		$uploadfile->setDecorators(array('File',
		
				array(array('data'=>'HtmlTag'), array('tag' => 'td')),
				array('Label', array('tag' => 'td')),
				array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
		));
		$uploadfile->setLabel('Upload Contacts ');
		$uploadfile->class = "text-input";
		$uploadfile->setDestination('./audioFiles/');
		$uploadfile->addValidator('Count', false, 1);
		$uploadfile->addValidator('Extension', false, 'mp3,wav');
		$uploadfile->setMaxFileSize(2097152);
		$uploadfile->setRequired(true);
		
		$this->addElement($uploadfile);
		
		$selectfile = new Zend_Form_Element_Multiselect('selectfile');
		$contactOptions = array("SELECT SINGLE/MULTI CONTACTS" =>array("1" => "Trivandrum",
										   "2" => "Ernakulam",
										   "3" => "Kozhikode"));
		$selectfile->addMultiOptions($contactOptions);
		$selectfile->setDecorators(array('ViewHelper',
				'Description',
		
				array(array('data'=>'HtmlTag'), array('tag' => 'td')),
				array('Label', array('tag' => 'td')),
				array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
		));
		$selectfile->setLabel('Select Contacts ');
		$selectfile->setAttrib('style', 'width:260px');
		$selectfile->setAttrib('cols', '30');
		$selectfile->class = "text-input";
		
		$selectfile->setRequired(true);
		//Add Validator
		$selectfile->addValidator(new Zend_Validate_EmailAddress());
		//Add Filter
		$selectfile->addFilter(new Zend_Filter_HtmlEntities());
		$selectfile->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectfile);
		
		$text = new Zend_Form_Element_Textarea('text');
		$text->setDecorators(array('ViewHelper',
				'Description',
		
				array(array('data'=>'HtmlTag'), array('tag' => 'td')),
				array('Label', array('tag' => 'td')),
				array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
		));
		$text->setLabel('Paste Contacts');
		//$plaintext->setRequired(true);
		$text->setAttrib('rows', '10');
		$text->setAttrib('cols', '30');
		$text->class = "plaintext-input";
		
		//Add Validator
		//Add Filter
		$text->addFilter(new Zend_Filter_HtmlEntities());
		$text->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($text);
		
		//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"button");
    	$submitElement->setDecorators(array('ViewHelper',
											'Description',
											'Errors', array(array('data'=>'HtmlTag'), array('tag' => 'td',
											'colspan'=>'2','align'=>'right')),
											array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
										));
		$submitElement->setLabel('Next');
    	
    	$this->setDecorators(array('FormElements',
    			array(array('data'=>'HtmlTag'),array('tag'=>'table')),
    			'Form'
    	));
    }


}

