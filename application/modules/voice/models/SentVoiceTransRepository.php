<?php
namespace modules\voice\models;
use Doctrine\ORM\EntityRepository;

class SentVoiceTransRepository extends \Smpp_Doctrine_EntityRepository
{
	public function getCampaignReports($userId, $campaignId, $msgType)
	{
		return $this->_em->createQuery('SELECT u FROM modules\voice\models\SendVoice u WHERE u.momt = "$msgType" AND u.service = "$userId" AND u.account = "$campaignId"')->getResult();
	}
	
	public function getReports()
	{
		return $this->_em->createQuery('SELECT u FROM modules\voice\models\SendVoice u WHERE u.momt = "$msgType" AND u.service = "$userId" AND u.account = "$campaignId"
				AND u.time >= "$fromTime" AND u.time <= "$toTime"')->getResult();
	}
	
}
