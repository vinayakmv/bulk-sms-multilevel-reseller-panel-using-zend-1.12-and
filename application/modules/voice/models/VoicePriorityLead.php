<?php
namespace modules\voice\models;

/**
 * @Entity(repositoryClass="modules\voice\models\VoicePriorityLeadRepository")
 * @Table(name="priority_leads_voice")
 * @author Vinayak
 *
 */
class VoicePriorityLead
{

	/**
	 * @Column(name="number", type="bigint", nullable=false)
	 * @Id
	 * @var string
	 */
	private $number;
	
	/**
	 * @Column(name="user_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $user;

	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}