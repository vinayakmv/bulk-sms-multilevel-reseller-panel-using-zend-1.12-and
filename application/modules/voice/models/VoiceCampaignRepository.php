<?php
namespace modules\voice\models;
use Doctrine\ORM\EntityRepository;

class VoiceCampaignRepository extends \Smpp_Doctrine_EntityRepository
{
	public function getScheduledCampaigns($userId)
	{
		$cdate=date("Y-m-d H:i:s");
		$sql = "SELECT id,start_date,type,count,credit FROM campaigns_voice WHERE `start_date` > '$cdate' AND `status`='0' AND (`input`='DYNAMIC-SCHEDULED' OR `input`='SCHEDULED') AND user_id='$userId' ";
		$em = $this->getEntityManager('voice');
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll();
	}

}