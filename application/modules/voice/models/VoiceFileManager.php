<?php
namespace modules\voice\models;

class VoiceFileManager extends \Smpp_Doctrine_BaseManager
{
	public function createVoice($userId, $fileName, $saveAudio, $duration, $audioName, $em)
	{
		if (isset($em)) {
			$voiceFile = new VoiceFile();
			$voiceFile->user = $userId;
			$voiceFile->duration = $duration;
			$voiceFile->name = $audioName;
			$voiceFile->fileName = $fileName;
			if ($saveAudio){
				$voiceFile->status = '1';
			}else{
				$voiceFile->status = '0';
			}			
			$voiceFile->createdDate = new \DateTime('now');
			$voiceFile->updatedDate = new \DateTime('now');
			$em->persist($voiceFile);
			$em->flush();
			
			return $voiceFile;
		}
		return false;
	
	}
	
	public function createDynamicVoice($userId, $csvName, $audio, $message, $fileName, $route, $em)
	{
		$leadPath = '../contactFiles/';
		$dynamicCsv = 'dynamic_'.$csvName;
		$file = $leadPath.$dynamicCsv;
		$csv = $leadPath.$csvName;
		$csvUp = 'up_'.$csvName;
		$up = $leadPath.'aw_'.$csvName;
		$tmp = $leadPath.'tmp_'.$csvName;
		$dnd = $leadPath.'dnd_'.$csvName;
		$dyTmp = $leadPath.'dy_'.$csvName;
				
		$cmd = 'awk -F, \'{$1=substr($1,length($1)-9)}1\' OFS=, '."$csv > $up";
		shell_exec($cmd);
		if ($route == '1' || $route == '3') {
			if (($handle = fopen("$up", "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					$numbers[] = $data[0];
				}
			}
			$contacts = $this->checkDND($numbers, $em);
			if ($contacts) {
				$dnd = $this->createCsv($contacts, $dnd);
		
			}else {
				shell_exec("echo '' > $dnd");
			}
		
		}else{
			$dnd = $up;
		}
		shell_exec("awk 'NF' $dnd | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' | awk -F',' 'length($1)==10' > $tmp");
		//shell_exec("awk -F',' '!seen[$1]++' $dnd | awk 'NF' | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' | awk -F',' 'length($1)==10' > $tmp");
		//shell_exec("awk -F',' '!seen[$0]++' $csv | awk 'NF' | awk -F',' ' $1 ~ \"^[0-9][0-9]*$\" { print $1 }' > $tmp");
		//shell_exec("awk 'NF' $csv > $tmp && mv $tmp $csv");
		
		shell_exec("awk -F, 'NR==FNR{seen[$1]++;next} ($1 in seen)' $tmp $up > $csv");
		//shell_exec("awk -F',' '!seen[$1]++' $dyTmp > $csv");

		shell_exec("rm -f $tmp $up $dnd");
		
		//fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
		$columns = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$pattern = array();
		$replacement = array();
		$dynamic = array();
		$credit = 0;
		$counter = 0;
		$fp = fopen("$file", "w");
		if (($handle = fopen("$csv", "r")) !== FALSE) {
			
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				
				foreach ($data as $key => $value){
					
					$column = $columns[$key];
					$pattern[$key] = '/\['.$column.'\]/';
					
					$audioFile = $data['0'].$key.'.wav';
					$audioName = '+'.$audioFile.'+';
					if($key != '0'){						
						$this->text2wave($value,null,$audioFile);
						$replacement[$key] = $audioName;
					}else{
						$replacement[$key] = $audioName;
					}
					
				}
				
				ksort($pattern);
				ksort($replacement);
				$msg = preg_replace($pattern, $replacement, $message);
				//print_r($replacement);echo "\n";
				$value=NULL;
				$replacement=NULL;
				$msgData = explode("+",$msg);
				
				$msgData['0']=$data['0'];
				//array_pop($msgData);
				
			fputcsv($fp, $msgData);
	
			}
			}
			fclose($fp);
			$fp = fopen("$dyTmp", "w");
			$audioFiles = array();
			foreach ($audio as $audioKey => $fileAudio){
				$name = $this->generateName();				
				$audioFiles[] = $this->convertVoice($fileAudio, $name);
			}
			
			$replacement = array_values($audioFiles);
			if (($handle = fopen("$file", "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					foreach ($data as $key=>$value){
						$pattern[$key] = '/\['.$key.'\]/';
						
					}
					ksort($pattern);
					ksort($audio);	
						
					$msgData = preg_replace($pattern, $replacement, $data);
										
					fputcsv($fp, $msgData);
					
						
				}
			}
			fclose($fp);
			shell_exec("rm -f $tmp $up $dnd $file");
			shell_exec("mv $dyTmp $csv");
	
			$fp = file($csv, FILE_SKIP_EMPTY_LINES);
			if ($fp){
			
			$joinResult = $this->joinDynamicVoice($csv);
			
				if ($joinResult){
					$fileCsv = $leadPath.$joinResult;
					if (($handle = fopen("$fileCsv", "r")) !== FALSE) {
						while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
							$credit += $data['2'];
							$counter++;
						}
					}
					
					return array('count' => $counter, 'credit' => $credit, 'file' => $joinResult);
				}
			
			}
			return false;
			
		
	}
	
	public function joinDynamicVoice($csvFile){
		$voicePath = '../audioFiles/';
		$contactPath = '../contactFiles/';
		$tmpName = $this->generateCsvName();
		$tmp = $contactPath.$tmpName;
		$fp = fopen("$tmp", "w");
		if (($handle = fopen("$csvFile", "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$data = array_filter($data);
				$voiceFiles = "";
				$files="";
				foreach ($data as $key=>$value){
					
					$value = substr($value, 0, (strpos($value, '.wav'))+4);
					
					if ($key != '0'){
						$voiceFiles = $voicePath.$value.' ';
					}
						$output = $this->generateName();	
						
						$files .= $voiceFiles;
						
				}
				$out = $voicePath.$output;
				shell_exec("/usr/local/bin/sox $files $out");
				$voiceFiles = "";
				$files = "";
				$duration = $this->getDuration($output);
				if($duration){
					$credit = ceil($duration/30);
					
					$result = array($data[0],$duration,$credit, $output);
					fputcsv($fp, $result);
					
				}
			}
			fclose($fp);
			shell_exec("/bin/rm -f $csvFile");
			
			return $tmpName;
		}
		return false;
	}
	
	public function convertVoice($audioFile, $audioName)
	{
		$voicePath = '../audioFiles/';
		$oldVoice = $voicePath.$audioFile;
		$newVoice = $voicePath.$audioName;
		
		$convert = shell_exec("/usr/local/bin/sox $oldVoice -r 8000 -c1 $newVoice -q -G");
		if (!file_exists($newVoice)) {
			$convert = shell_exec("/usr/local/bin/sox $oldVoice -c1 $newVoice");
		}
		if (file_exists($newVoice)) {
			shell_exec("rm -f $oldVoice");
			return $audioName;
		}
		return false;
	}
	
	public function text2wave($text, $voice = NULL, $filename = NULL)
	{
		$voicePath = '../audioFiles/';
		if ($filename != NULL){
			$audioName = $filename;
		}else{
			$audioName = $this->generateName();
		}
		
		$audioFile = $voicePath.$audioName;
		if ($voice != NULL){
			$convert = shell_exec("/bin/echo $text | /usr/bin/text2wave -F 8000 -o $audioFile -eval '($voice)'");
		}else{
			$convert = shell_exec("/bin/echo $text | /usr/bin/text2wave -F 8000 -o $audioFile");			
		}		
		if (file_exists($audioFile)) {
			return $audioName;
		}
		return false;
	}
	
	public function renameVoice($oldName, $newName)
	{
		$voicePath = '../audioFiles/';
		$oldFile = $voicePath.$oldName;
		$newFile = $voicePath.$newName;
		$move = shell_exec("mv $oldFile $newFile");
		if (file_exists($newFile)) {
			return $newName;
		}
		return false;
	}
	
	public function getDuration($audioFile)
	{
		$voicePath = '../audioFiles/';
		$audioFile = $voicePath.$audioFile;
 		$duration = shell_exec("/usr/bin/exiftool -s -s -s -n -Duration $audioFile");
		$duration = ceil($duration);
		if ($duration) {
			return $duration;
		}
		return false;
	}
	
	public function generateName()
	{
		mt_srand((double)microtime()*10000);
		$charid = strtolower(md5(uniqid(rand(), true)));
		$name =  substr($charid, 0, 8).substr($charid, 8, 4).'.wav';
		return $name;
	}
	
	public function generateCsvName()
	{
		mt_srand((double)microtime()*10000);
		$charid = strtolower(md5(uniqid(rand(), true)));
		$name =  substr($charid, 0, 8).substr($charid, 8, 4).'.csv';
		return $name;
	}
}
