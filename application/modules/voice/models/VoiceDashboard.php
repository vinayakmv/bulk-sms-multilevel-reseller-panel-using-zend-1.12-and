<?php
namespace modules\voice\models;

/**
 * @Entity(repositoryClass="modules\voice\models\VoiceDashboardRepository")
 * @Table(name="voice_dashboard")
 * @author Vinayak
 *
 */
class VoiceDashboard
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
         * @Id
         * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @column(name="user_id", type="integer", nullable="false")
	 * @var integer
	 */
	private $user;
	
	/**
	 * @column(name="domain_id", type="integer", nullable="true")
	 * @var integer
	 */
	private $domain;
        
        /**
	 * @column(name="parent_domain_id", type="integer", nullable="true")
	 * @var integer
	 */
	private $parentDomain;
	
	/**
	 * @column(name="reseller_id", type="integer", nullable="true")
	 * @var integer
	 */
	private $reseller;
	
	/**
	 * @column(name="sent_voice_trans", type="integer", nullable="false")
	 * @var integer
	 */
	private $sentVoiceTrans;
	
	/**
	 * @column(name="delivered_voice_trans", type="integer", nullable="false")
	 * @var integer
	 */
	private $deliveredVoiceTrans;

	/**
	 * @column(name="failed_voice_trans", type="integer", nullable="false")
	 * @var integer
	 */
	private $failedVoiceTrans;

	/**
	 * @column(name="sent_voice_promo", type="integer", nullable="false")
	 * @var integer
	 */
	private $sentVoicePromo;
	
	/**
	 * @column(name="delivered_voice_promo", type="integer", nullable="false")
	 * @var integer
	 */
	private $deliveredVoicePromo;

	/**
	 * @column(name="failed_voice_promo", type="integer", nullable="false")
	 * @var integer
	 */
	private $failedVoicePromo;

	/**
	 * @Column(name="report_date", type="date", nullable=true)
	 * @var datetime
	 */
	private $reportDate;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
	function toArray ()
   {
      return get_object_vars($this);
   }
	
}
