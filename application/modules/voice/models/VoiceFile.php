<?php
namespace modules\voice\models;

/**
 * @Entity(repositoryClass="modules\voice\models\VoiceFileRepository")
 * @Table(name="files_voice")
 * @author Vinayak
 *
 */
class VoiceFile
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 * @Column(name="user_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $user;
	
	/**
	 * @Column(name="duration", type="integer", nullable=false)
	 * @var integer
	 */
	private $duration;
	
	/**
	 * @Column(name="name", type="string", nullable=true)
	 * @var string
	 */
	private $name;
	
	/**
	 * @Column(name="file_name", type="string", nullable=true)
	 * @var string
	 */
	private $fileName;
	
	/**
	 * @Column(name="status", type="integer", nullable=false)
	 * @var integer
	 */
	private $status;
		
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}