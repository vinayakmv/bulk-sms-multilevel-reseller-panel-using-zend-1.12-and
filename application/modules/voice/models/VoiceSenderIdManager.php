<?php
namespace modules\voice\models;

class VoiceSenderIdManager extends \Smpp_Doctrine_BaseManager
{
	public function createSenderId($userId, $name, $type, $em, $status=1, $description='AUTO GEN')
	{
		if (isset($em)){
			$nameArray = explode(chr(10), $name);
			
			$count = count($nameArray);
			if ($count > 100) {
				return array('SenderId' => array('senderCount' => 'more than 100 senderId is not allowed'));
			}else{
				foreach ($nameArray as $name){
					$sender = new VoiceSenderId();
					$sender->user = $userId;
					$sender->name = $name;
					$sender->type = $type;
					$sender->status = $status;
					$sender->remark = '';
					$sender->description = $description;
					$sender->createdDate = new \DateTime('now');
					$sender->updatedDate = new \DateTime('now');
					$em->persist($sender);
					$em->flush();
						
				}
				return array('SenderId' => array('isCreated' => "$count senderId requested"));
			}
		}
		return false;
		
	}
	public function manageSenderId($getValues,$status, $userId, $em)
	{
		if (isset($getValues)){
			
			
			if($status=='Approved'){
				$id=$getValues['id'];
				$remark=$getValues['remark'];
				$senderid = $em->getRepository('modules\voice\models\VoiceSenderId')->find($id);
				$senderid->status = '1';
				$senderid->remark = $remark;
				$senderid->updatedDate = new \DateTime('now');
			}
			else if($status=='Rejected'){
				$id=$getValues['id'];
				$remark=$getValues['remark'];
				$senderid = $em->getRepository('modules\voice\models\VoiceSenderId')->find($id);
				$senderid->status = '4';
				$senderid->remark = $remark;
				$senderid->updatedDate = new \DateTime('now');
			}else if($status=='Delete'){
				$id=$getValues['id'];
				$senderid = $em->getRepository('modules\voice\models\VoiceSenderId')->findOneBy(array('id' => $id, 'user' => $userId));
				$senderid->status = '5';
				$senderid->updatedDate = new \DateTime('now');
			}else if($status=='Default'){
				$id=$getValues['id'];
				$senderidRepos = $em->getRepository('modules\voice\models\VoiceSenderId')->findBy(array('user' => $userId));
				
				foreach ($senderidRepos as $senderids){
					$senderids->default = '0';
					$senderids->updatedDate = new \DateTime('now');
					$em->persist($senderids);
					$em->flush();
				}
				$senderid = $em->getRepository('modules\voice\models\VoiceSenderId')->findOneBy(array('id' => $id, 'user' => $userId));
				$senderid->default = '1';
				$senderid->updatedDate = new \DateTime('now');
				
			}
			$em->persist($senderid);
			$em->flush();
	
			return $senderid;
		}
		return false;
	}
	
	public function requestSenderId($userRepo, $name, $type, $em)
	{
		if (isset($em)){
			$sender = new VoiceSenderId();
			$sender->user = $userRepo;
			$sender->name = '+'.$name;
			$sender->type = $type;
			$sender->status = 2;
			$sender->createdDate = new \DateTime('now');
			$sender->updatedDate = new \DateTime('now');
			$em->persist($sender);
			$em->flush();
				
			return $sender;
		}
		return false;
	}
	
	public function blockSenderId($userId, $em)
	{
		if (isset($userId)){
			$user = $em->getRepository('modules\voice\models\VoiceSenderId')->findOneBy(array('id' => "$userId"));
			$user->status = '3';
			$em->persist($user);
			$em->flush();
	
			return $user;
		}
		return false;
	}
	
	public function unblockSenderId($userId, $em)
	{
		if (isset($userId)){
			$user = $em->getRepository('modules\voice\models\VoiceSenderId')->findOneBy(array('id' => "$userId"));
			$user->status = '1';
			$em->persist($user);
			$em->flush();
	
			return $user;
		}
		return false;
	}
	
	public function activateSenderId($userId, $em)
	{
		if (isset($userId)){
			$user = $em->getRepository('modules\voice\models\VoiceSenderId')->findOneBy(array('id' => "$userId"));
			$user->status = '1';
			$em->persist($user);
			$em->flush();
	
			return $user;
		}
		return false;
	}
	
}