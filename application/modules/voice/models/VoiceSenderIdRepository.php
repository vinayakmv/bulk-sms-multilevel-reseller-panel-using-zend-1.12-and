<?php
namespace modules\voice\models;
use Doctrine\ORM\EntityRepository;
/**
 *
 * @author Vinayak
 *
 */
class VoiceSenderIdRepository extends \Smpp_Doctrine_EntityRepository
{
	public function checkSender($sender, $user){
		$queryBuilder = $this->getEntityManager()->createQueryBuilder()
		->select('u.name')
		->from('modules\voice\models\VoiceSenderId', 'u')
		->where('u.name = ?1')
		->andWhere('u.user = ?2')
		->andWhere('u.status = ?3')
		->setParameter("1", $sender)
		->setParameter("2", $user)
		->setParameter("3", '1');
	
		return $queryBuilder->getQuery()->getResult();
	}
}