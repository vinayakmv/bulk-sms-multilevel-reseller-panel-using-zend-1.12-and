<?php
namespace modules\voice\models;

/**
 * @Entity(repositoryClass="modules\voice\models\VoiceCampaignRepository")
 * @Table(name="campaigns_voice",indexes={@Index(name="search_idx", columns={"status"})})
 * @author Vinayak
 *
 */
class VoiceCampaign
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="user_id", type="integer", nullable=false)
	 * @var integer
	 */
	private $user;
	
	/**
	 * @Column(name="name", type="string", nullable=true)
	 * @var string
	 */
	private $name;
	
	/**
	 * @Column(name="voice_priority", type="integer", nullable=true)
	 * @var string
	 */
	private $voicePriority;
	
	/**
	 * @Column(name="voice_file", type="string", nullable=false)
	 * @var integer
	 */
	private $voiceFile;
	
	/**
	 * @Column(name="leads_file", type="string", nullable=false)
	 * @var integer
	 */
	private $leadsFile;
	
	/**
	 * @Column(name="sender_id", type="string", nullable=false)
	 * @var integer
	 */
	private $senderId;
	
	/**
	 * @Column(name="context", type="string", nullable=false)
	 * @var integer
	 */
	private $context;
	
	/**
	 * @Column(name="duration", type="integer", nullable=false)
	 * @var integer
	 */
	private $duration;
	
	/**
	 * @Column(name="count", type="string", nullable=false)
	 * @var string
	 */
	private $count;
	
	/**
	 * @Column(name="route", type="string", nullable=false)
	 * @var integer
	 */
	private $route;
	
	/**
	 * @Column(name="route_name", type="string", nullable=true)
	 * @var string
	 */
	private $routeName;
	
	/**
	 * @Column(name="type", type="string", nullable=false)
	 * @var string
	 */
	private $type;
	
	/**
	 * @Column(name="method", type="integer", nullable=true)
	 * @var integer
	 */
	private $method;
	
	/**
	 * @Column(name="api_key", type="string", nullable=true)
	 * @var string
	 */
	private $apiKey;
	
	/**
	 * @Column(name="recredit", type="integer", nullable=false)
	 * @var integer
	 */
	private $recredit;
	
	/**
	 * @Column(name="input", type="string", nullable=false)
	 * @var string
	 */
	private $input;
	
	/**
	 * @Column(name="credit", type="string", nullable=true)
	 * @var string
	 */
	private $credit;
	
	/**
	 * @Column(name="fake_count", type="integer", nullable=false)
	 * @var string
	 */
	private $fakeCount;
	
	/**
	 * @Column(name="status", type="integer", nullable=false)
	 * @var integer
	 */
	private $status;
	
	/**
	 * @column(name="report_status", type="integer", nullable="false")
	 * @var integer
	 */
	private $reportStatus;
	
	/**
	 * @column(name="report_file", type="string", nullable="false")
	 * @var string
	 */
	private $reportFile;
	
	/**
	 * @Column(name="csv_gen_date", type="datetime", nullable=true)
	 * @var datetime
	 */
	private $csvGenDate;
	
	/**
	 * @Column(name="start_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $startDate;
	
	/**
	 * @Column(name="end_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $endDate;
		
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}