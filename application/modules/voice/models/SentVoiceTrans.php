<?php
namespace modules\voice\models;

/**
 * @Entity(repositoryClass="modules\voice\models\SentVoiceRepository")
 * @Table(name="sent_voice_trans")
 * @author Vinayak
 *
 */
class SentVoiceTrans
{
	/**
	 * @Column(name="sql_id", type="bigint", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")	 
	 * @var integer
	 */
	private $sqlId;
	
	/**
	 * @Column(name="sender", type="string", nullable=true)
	 * @var unknown
	 */
	private $sender;
	
	/**
	 * @Column(name="receiver", type="string", nullable=true)
	 * @var unknown
	 */
	private $receiver;
	
	/**
	 * @Column(name="voice_file", type="string", nullable=true)
	 * @var unknown
	 */
	private $voice;
	
	/**
	 * @Column(name="time", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $time;
	
	/**
	 * @Column(name="route", type="string", nullable=true)
	 * @var unknown
	 */
	private $route;
	
	/**
	 * @Column(name="user_id", type="string", nullable=true)
	 * @var unknown
	 */
	private $user;
	
	/**
	 * @Column(name="campaign_id", type="string", nullable=true)
	 * @var unknown
	 */
	private $campaign;
	
	/**
	 * @Column(name="context", type="string", nullable=true)
	 * @var unknown
	 */
	private $context;
	
	/**
	 * @Column(name="dlr_url", type="string", nullable=true)
	 * @var unknown
	 */
	private $dlrUrl;
	
	/**
	 * @Column(name="dlrdata", type="string", nullable=true)
	 * @var unknown
	 */
	private $dlrdata;
	
	/**
	 * @Column(name="duration", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $duration;

	/**
	 * @Column(name="priority", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $priority;
	
	/**
	 * @Column(name="data", type="integer", nullable=true)
	 * @var unknown
	 */
	private $data;
	
	/**
	 * @Column(name="status", type="integer", nullable=true)
	 * @var unknown
	 */
	private $status;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
