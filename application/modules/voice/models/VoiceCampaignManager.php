<?php
namespace modules\voice\models;

use modules\user\models\LeadManager;
class VoiceCampaignManager extends \Smpp_Doctrine_BaseManager
{
	public function createCampaign($campaignValues, $userId, $api, $apiKey, $emVoice, $emUser, $emSmpp)
	{
		
			if (isset($campaignValues)) {
				
				if ($api){
					$method = '2';
				}else{
					$method = '1';
				}
				
				$campaignName = $campaignValues['campaignName'];
				$campaignType = $campaignValues['campaignType'];
				$input = $campaignValues['input'];
				$callDuration = $campaignValues['selectDuration'];
				$audioType = $campaignValues['audioType'];
				$saveAudio = $campaignValues['saveAudio'];
				$contactType = $campaignValues['contactType'];
				
				$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
				$parentDomain = $userRepo->parentDomain;
				$parentDomainRepo = $emUser->getRepository('modules\user\models\Domain')->find($parentDomain);
				$parentRecredit = $parentDomainRepo->voiceRecredit;
				$dynamic = false;
				
				
					
				
				$routeType = $campaignValues['voiceType'];
				$routes = explode ('|', $routeType);
				
				switch ($routes['0']) {
					case 'TRANSACTIONAL':
						$route = 2;
						$fake = $userRepo->voicePercentageTrans;
						break;
					case 'PROMOTIONAL':
						$route = 1;
						$fake = $userRepo->voicePercentagePromo;
						break;
					default:
						return array('Campaign' => array('route' => 'Routing error failed'));
						break;
				}
				
				
				if ($campaignType == '1') {
					$context = 'playback';
				}elseif ($campaignType == '2') {
					$context = 'survey';
				}
				
				
				$date=date('Y-m-d', strtotime($campaignValues['datePicker']));
				$dateTime=$date.' '.$campaignValues['timePicker'];
				$duration='+15 seconds'; 
           		$after = date('Y-m-d H:i:s', strtotime($duration, strtotime($dateTime))); 
				$type = '1';
				
				$creditValue = $callDuration;
				
				$openSenderId = $userRepo->openSenderId;
				
				if ($openSenderId){
					$senderId = $campaignValues['typeSenderId'];
				}else{
					$senderId = trim($campaignValues['selectSenderId']);
				}
								
				if ($openSenderId == '0'){
					$senderMatch = '1';
					$senderIdRepos = $emVoice->getRepository('modules\voice\models\VoiceSenderId')->findOneBy(array('name' => $senderId,'user' => $userId, 'status' => '1'));
						
					if(isset($senderIdRepos)){
						$sender = $senderIdRepos->name;
						$senderMatch = strcmp($sender, $senderId);
					}
						
					if ($senderMatch != '0'){
						return array('Campaign' => array('sender' => 'SenderID is not approved'));
					}
				}
				
				if ($audioType == '1') {
					//Select Audio
					$audioId = $campaignValues['selectAudio'];
					$voiceFileRepo = $emVoice->getRepository('modules\voice\models\VoiceFile')->findOneBy(array('id' => $audioId, 'user' => $userId));
					$voiceFile = $voiceFileRepo->fileName;
				}elseif ($audioType == '2') {
					
					//Upload Audio
					$audioName = $campaignValues['audioName'];
					$uploadAudio = $campaignValues['uploadAudio'];
					if (!$uploadAudio) {
						//return false;
						return array('Campaign' => array('uploadAudio' => 'failed'));
					}
					$voiceFileManager = new VoiceFileManager();
					$duration = $voiceFileManager->getDuration($uploadAudio);
			
					
					if ($uploadAudio) {
						$uuid = $this->generateUuid();
						$audioFileName = $uuid.'.wav';
						
						$voiceFileManager->convertVoice($uploadAudio, $audioFileName);
						$voiceFileRepo = $voiceFileManager->createVoice($userRepo->id, $audioFileName, $saveAudio, $duration, $audioName, $emVoice);
						$voiceFile = $voiceFileRepo->fileName;
					}
				}elseif ($audioType == '3') {
					//Text2Speech
					$audioName = $campaignValues['audioName'];
			
					$text = $campaignValues['text2speech'];
					$selectVoice = $campaignValues['selectVoice'];
					$voiceFileManager = new VoiceFileManager();
					$textAudio = $voiceFileManager->text2wave($text, $selectVoice);
					$duration = $voiceFileManager->getDuration($textAudio);
			
					$uuid = $this->generateUuid();
					$audioFileName = $uuid.'.wav';
					$voiceFileRepo = $voiceFileManager->createVoice($userRepo->id, $audioFileName, $saveAudio, $duration, $audioName, $emVoice);
						
					$voiceFile = $voiceFileManager->convertVoice($textAudio, $audioFileName);
					$voiceFileRepo->fileName = $voiceFile;
					$emVoice->persist($voiceFileRepo);
					$emVoice->flush();
				}elseif ($audioType == '4') {						
					$voiceFile = 'DYNAMIC-AUDIO';
				}
			
				if ($contactType == '1') {
					//Select Contact
					//Select Contact
					$selectContact = $campaignValues['selectContact'];
					$groups = explode(',', $selectContact);
					$leadPath = '../contactFiles/';
					$uuid = $this->generateUuid();
					$newLead = $uuid.'.csv';
					$file = $leadPath.$newLead;
					$fp = fopen("$file", 'w');
					foreach ($groups as $group){
						
						if ($api){
							$leadRepos = $emUser->getRepository('modules\user\models\Lead')->findOneBy(array('id' => $group, 'user' => $userId));
						}else{
							$leadRepos = $emUser->getRepository('modules\user\models\Lead')->findOneBy(array('name' => $group, 'user' => $userId));
						}
						$leadId = $leadRepos->id;
						$leadDetailRepos = $emUser->getRepository('modules\user\models\LeadDetail')->findBy(array('lead' => $leadId));
						//$leadRepo = $em->getRepository('modules\user\models\Lead')->findById(array($selectContact));
						foreach ($leadDetailRepos as $leadDetail){
							$line = $leadDetail->mobile;	
							$val = explode(",",$line);
							fputcsv($fp, $val);
						}	
					}
					fclose($fp);
					$leadManager = new LeadManager();
					$count = $leadManager->countCsv($newLead, $route, $emSmpp);
					
					if (!$count){
						return array('Campaign' => array('contactCount' => 'groups is invalid'));
					}
				}elseif ($contactType == '2') {
					//Upload Contact
					$contactName = $campaignValues['contactName'];
					$uploadContact = $campaignValues['uploadContact'];
					//$saveContact = $campaignValues['saveContact'];
					if (!$uploadContact) {
						//return false;
						return array('Campaign' => array('uploadContact' => 'failed'));
					}
		
					$leadManager = new LeadManager();
					$count = $leadManager->countCsv($uploadContact, $route, $emSmpp);
					if (!$count){
						if ($route == '2'){
							return array('Campaign' => array('contactCount' => 'contacts is invalid'));
						}else{
							return array('Campaign' => array('contactCount' => 'contacts is ndnc registered or invalid'));
						}
						
					}else {
						if ($count > 50000) {
							return array('Campaign' => array('contactCount' => 'contacts is more than 50,000'));
						}
					}
					
		
					/* $leadRepo = $leadManager->createLead($userRepo, $type, $saveContact, $contactName, $em);
					if ($leadRepo) {
						$fileName = $leadRepo->id; */
						$uuid = $this->generateUuid();
						$fileName = $uuid.'.csv';
						$newLead = $leadManager->renameCsv($uploadContact, $fileName);
						//$count = $leadManager->countCsv($newLead, $route, $em);
					/* 		
						$leadRepo->count = $count;
						$em->persist($leadRepo);
						$em->flush();
					}
					$leadRepo = array($leadRepo); */
						
				}elseif ($contactType == '3') {
					//Text Contact
					$textContact = $campaignValues['textContact'];
					//$saveContact = $campaignValues['saveContact'];
					
					$uuid = $this->generateUuid();
					$fileName = $uuid.'.csv';
					$leadManager = new LeadManager();
					$newLead = $leadManager->createCsv($textContact, $fileName);
					$count = $leadManager->countCsv($newLead, $route, $emSmpp);
					
					if (!$count){
						if ($route == '2'){
							return array('Campaign' => array('contactCount' => 'contacts is invalid'));
						}else{
							return array('Campaign' => array('contactCount' => 'contacts is ndnc registered or invalid'));
						}
						
					}else {
						if ($count > 50000) {
							return array('Campaign' => array('contactCount' => 'contacts is more than 50,000'));
						}
					}
		
					/* $leadRepo = $leadManager->createLead($userRepo, $type, $saveContact, $contactName, $em);
					if ($leadRepo) {
						$newName = $leadRepo->id;
						$newLead = $leadManager->renameCsv($newLead, $newName);
							
						$leadRepo->count = $count;
						$em->persist($leadRepo);
						$em->flush();
					}
					$leadRepo = array($leadRepo); */
			
				}elseif ($contactType == '4') {
					$dynamic = true;
					//Upload Contact
					$text = urldecode($campaignValues['text']);
					$audio = $campaignValues['audio'];
					$uuid = $this->generateUuid();
					$audioFileName = $uuid.'.wav';
					
					if (isset($campaignValues['textContact'])){
							
						$textContact = $campaignValues['textContact'];
						$uuid = $this->generateUuid();
						$fileName = $uuid.'.csv';
						$leadManager = new LeadManager();
						$uploadContact = $leadManager->createCsv($textContact, $fileName);
							
					}else{
						$uploadContact = $campaignValues['uploadContact'];
					}
					
					//$saveContact = $campaignValues['saveContact'];
					if (!$uploadContact) {
						//return false;
						return array('Campaign' => array('uploadContact' => 'failed'));
					}
					$voiceFileManager = new VoiceFileManager();
					$dynamicData = $voiceFileManager->createDynamicVoice($userRepo->id, $uploadContact, $audio, $text, $fileName, $route, $emVoice);
					if (!$dynamicData){
						if ($route == '2'){
							return array('Campaign' => array('contactCount' => 'contacts is invalid'));
						}else{
							return array('Campaign' => array('contactCount' => 'contacts is ndnc registered or invalid'));
						}
							
					}else {
						$count = $dynamicData['count'];
						$creditValue = $dynamicData['credit'];
						$newLead = $dynamicData['file'];
						if ($count > 50000) {
							return array('Campaign' => array('contactCount' => 'contacts is more than 50,000'));
						}
					}
					
				}
				
				$fakeCount = ceil($count*($fake/100));
					
				$isCampaignAllowed = $this->isCampaignAllowed($userId, $count, $creditValue, $dynamic, $route, $api, $emUser);
				
				if ($isCampaignAllowed) {
					$isCampaignAllowed = false;
					
					$today =new \DateTime('now');
					$startDate = new \DateTime($dateTime);
					if ($count <= '7' && $startDate <= $today){
						//Create Campaign
						$voiceCampaign = new VoiceCampaign();
						$voiceCampaign->name = $campaignName;
						$voiceCampaign->user = $userRepo->id;
						$voiceCampaign->voiceFile = $voiceFile;
						$voiceCampaign->voicePriority = '3';
						$voiceCampaign->senderId = $senderId;
						$voiceCampaign->count = $count;
						$voiceCampaign->fakeCount = $fakeCount;
						$voiceCampaign->leadsFile = $newLead;
						$voiceCampaign->context = $context;
						$voiceCampaign->duration = $callDuration;
						$voiceCampaign->credit = $count*$callDuration;
						$voiceCampaign->route = $routes['1'];
						$voiceCampaign->routeName = $routes['2'];
						$voiceCampaign->type = $routes['0'];
						$voiceCampaign->input = $input;
						$voiceCampaign->startDate = new \DateTime($dateTime);
						$voiceCampaign->status = '4';
						$voiceCampaign->method = $method;
						$voiceCampaign->apiKey = $apiKey;
						$voiceCampaign->recredit = $parentRecredit;
						$voiceCampaign->endDate = new \DateTime($after);
						$voiceCampaign->createdDate = new \DateTime('now');
						$voiceCampaign->updatedDate = new \DateTime('now');
						$emVoice->persist($voiceCampaign);
						$emVoice->flush();
						
						shell_exec("/usr/bin/php /var/www/html/webpanel/cronjob/voice/cronJobNumberPriority.php $voiceCampaign->id 2> /dev/null > /dev/null  &");
							
					}else {
						//Create Campaign
						$voiceCampaign = new VoiceCampaign();
						$voiceCampaign->name = $campaignName;
						$voiceCampaign->user = $userRepo->id;
						$voiceCampaign->voiceFile = $voiceFile;
						$voiceCampaign->voicePriority = $userRepo->voicePriority;
						$voiceCampaign->senderId = $senderId;
						$voiceCampaign->count = $count;
						$voiceCampaign->fakeCount = $fakeCount;
						$voiceCampaign->leadsFile = $newLead;
						$voiceCampaign->context = $context;
						$voiceCampaign->duration = $callDuration;
						$voiceCampaign->credit = $count*$callDuration;
						$voiceCampaign->route = $routes['1'];
						$voiceCampaign->routeName = $routes['2'];
						$voiceCampaign->type = $routes['0'];
						$voiceCampaign->input = $input;
						$voiceCampaign->startDate = new \DateTime($dateTime);
						$voiceCampaign->status = '0';
						$voiceCampaign->method = $method;
						$voiceCampaign->apiKey = $apiKey;
						$voiceCampaign->recredit = $parentRecredit;
						$voiceCampaign->endDate = new \DateTime($after);
						$voiceCampaign->createdDate = new \DateTime('now');
						$voiceCampaign->updatedDate = new \DateTime('now');
						$emVoice->persist($voiceCampaign);
						$emVoice->flush();
					}
					
						
					//return $voiceCampaign;
					//return array('Campaign' => array('isSuccess' => 'Campaign created'));
					return array('Campaign' => array('isSuccess' => 'Campaign created','data' => $voiceCampaign->id, 'leads' => $count));
				}else {
					return array('Campaign' => array('credit' => 'Insufficient credit'));
				}
			}
			return false;
		
	}
	
	public function isCampaignAllowed($userId, $leadsCount, $creditValue, $dynamic, $route, $api, $em)
	{
		if ($route == '1'){ 
			if ($dynamic){
				$leads = $creditValue;
			}else {
				$leads = $leadsCount*$creditValue;
			}
			$userRepo = $em->getRepository('modules\user\models\User')->find($userId);
			if ($api){
				$userBalance = $userRepo->account->voicePromoApiCredit;
			}else{
				$userBalance = $userRepo->account->voicePromoCredit;
			}
			
			if ($userBalance >= $leads){
				$currentBalance = $userBalance - $leads;
				if ($api){
					$userRepo->account->voicePromoApiCredit = $currentBalance;
				}else{
					$userRepo->account->voicePromoCredit = $currentBalance;
				}
				
				$em->persist($userRepo);
				$em->flush();
			
				return true;
			}
			return false;
		}elseif ($route == '2'){
			if ($dynamic){
				$leads = $creditValue;
			}else {
				$leads = $leadsCount*$creditValue;
			}
			$userRepo = $em->getRepository('modules\user\models\User')->find($userId);
			if ($api){
				$userBalance = $userRepo->account->voiceTransApiCredit;
			}else{
				$userBalance = $userRepo->account->voiceTransCredit;
			}
			
			if ($userBalance >= $leads){
				$currentBalance = $userBalance - $leads;
				if ($api){
					$userRepo->account->voiceTransApiCredit = $currentBalance;
				}else{
					$userRepo->account->voiceTransCredit = $currentBalance;
				}
				
				$em->persist($userRepo);
				$em->flush();
				
				return true;
			}
			return false;
		}
		return false;
	}
	
	public function cancellCampaign($campaignId, $emVoice)
	{
		if (isset($emVoice)) {
			$voiceCampaign = $emVoice->getRepository('modules\voice\models\VoiceCampaign')->find($campaignId);
			if ($voiceCampaign->status == '0'){
				$voiceCampaign->status = 3;
				$emVoice->persist($voiceCampaign);
				$emVoice->flush();
	
				return $voiceCampaign;
			}
		}
		return false;
	}
	
}
