<?php
use Models\SenderIdManager;
class Voice_RegisterController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $senderid = $this->view->navigation()->findOneByLabel('Sender IDs');
    	if ($senderid) {
    		$senderid->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
    	parent::init();
    }

    public function indexAction()
    {
   		$page = $this->view->navigation()->findOneByLabel('Request Voice Sender-ID');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Broadcast | Request Voice Sender-ID');
    	}
    	$form = new Voice_Form_Register();
        $this->view->form = $form;
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        	$verifyValues = $form->getValues();
        	$userId = Zend_Auth::getInstance()->getIdentity()->id;
        	$em = $this->getEntityManager();
        	$type = '1';
        	$name = $verifyValues['username'];
        	$userRepo = $em->getRepository('Models\User')->find($userId);
        	$senderidManager = new SenderIdManager();
        	$sender = $senderidManager->requestSenderId($userRepo, $name, $type, $em);
        	if ($sender) {
        		$this->view->success = array('senderid' => array('isSuccess' => 'Sender-ID requested'));
        	}else {
        		$this->view->errors = array('senderid' => array('isFailed' => "Sender-ID request failed"));
        	}
        	
        }else {
        	$this->view->errors = $form->getMessages();
        }

        $form->reset();
    }

}

