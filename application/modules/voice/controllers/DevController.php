<?php
use Models\ApikeyManager;
class Voice_DevController extends Smpp_Controller_BaseController
{
    public function init()
    {
        $broadcast = $this->view->navigation()->findOneByLabel('Sms Broadcast');
        if ($broadcast) {
          $broadcast->setActive();
        }
        $action = $this->getRequest()->getActionName();
        $emUser = $this->getEntityManager('user');
        $domain = Smpp_Utility::getFqdn();
        $domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
        $domainLayout = $domainRepo->layout;
        if ($domainLayout != null){
        	$view = $domainLayout.'-'.$action;
        	$this->_helper->viewRenderer("$view");
        }
        parent::init();
    }
    public function indexAction()
    {
         $this->view->subject="API KEY MANAGEMENT";
    $page = $this->view->navigation()->findOneByLabel('API Management');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('Voice Broadcast | API Management');
    }
		$form=new Voice_Form_Apikey();
	    $this->view->form=$form;
        $domain = Smpp_Utility::getFqdn();
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
        $this->view->userId=$userId;
		$em = $this->getEntityManager();
        $apiRepo = $em->getRepository('Models\Apikey')->getAllApi($userId);
        $this->view->apiRepo=$apiRepo;
		$apiManager = new ApikeyManager();
		if ($this->getRequest()->isPost())
		{
            $apiValue1=$this->getRequest()->getPost();
            $apiValue = $form->getValues();
            $apiValue=array_merge($apiValue,$apiValue1);
            if(isset($apiValue['submitValue'])) $apiValue['submit']=$apiValue['submitValue'];
            if($apiValue['submit']=="addIP"){


                $addIp = $apiManager->addIp($apiValue, $em);
                //var_dump($addIp);
                if($addIp){
                    $this->view->Success = array('API' => array('Message' => 'Add IPs Successfully'));
                }
                else {
                    $this->view->errors = array('API' => array('Message' => 'Please Enter Correct IP Address'));
                   
                }
            }
            else if($apiValue['submit']=="generate"){
                
                $countApi = $em->getRepository('Models\Apikey')->countApi($userId);
                //print_r($countApi);
                /*echo $countApi['0']['count'];
                exit();*/

                if($countApi['0']['count']<='9'){
                    $api = $this->generateUuid();
                    $apiArray=array('api'=>$api);
                    $keyCreated = $apiManager->createApi($apiArray, $userId, $em, $em);

                    if ($keyCreated) {
                        $this->view->Success = array('API' => array('Message' => 'API Key Generated Successfully '));                        
                    }
                }
                else{
                    $this->view->errors = array('API' => array('Message' => 'Maximum Limit Reached'));
                }
                
            }
            else if($apiValue['submit']=="delete"){
                //print_r($apiValue);
                $apiId = $apiValue['apiId'];
                $keyDelete = $apiManager->deleteApi($apiId,$em);
                if ($keyDelete) { 
                    $this->view->Success = array('API' => array('Message' => 'Deleted Successfully'));    
                        
                }

            }
            else if($apiValue['submit']=="edit"){
                //print_r($apiValue);
                $apiId = $apiValue['apiId'];
                $api = $this->generateUuid();
                $keyEdit = $apiManager->editApi($apiId,$em,$userId,$api);
                
            }
            else
            {

            }
        }
        $form->reset();
    }
	
    public function docAction()
    {
		
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
		$domain = Smpp_Utility::getFqdn();
		$this->view->domain=$domain;
    }


}

