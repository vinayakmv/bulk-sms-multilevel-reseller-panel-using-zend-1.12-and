<?php

class Voice_FileController extends Smpp_Controller_BaseController
{

    public function init()
    {
    	$fileManage = $this->view->navigation()->findOneByLabel('File Manage');
    	if ($fileManage) {
    		$fileManage->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
        parent::init();
    }

    public function indexAction()
    {
 
    }
    
    public function voiceAction()
    {
    	$page = $this->view->navigation()->findOneByLabel('Voice File');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('File Manage | Voice File');
    	}
    	
    	//$reportManager = new SendSmsManager();
    	$em = $this->getEntityManager();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$queryBuilder = $em->createQueryBuilder();
    	$queryBuilder->select('e')
    	->from('Models\VoiceFile', 'e')
    	->where('e.user = ?1')
    	->orderBy('e.id', 'DESC')
    	->setParameter(1, $userId);
    	$paginator = new Zend_Paginator(new Sip_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	->setItemCountPerPage(20);
    	 
    	$this->view->entries = $paginator;
    }
    
    public function leadAction()
    {
    	$page = $this->view->navigation()->findOneByLabel('Lead File');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('File Manage | Lead File');
    	}
    	
    	$em = $this->getEntityManager();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$queryBuilder = $em->createQueryBuilder();
    	$queryBuilder->select('e')
			    	->from('Models\Lead', 'e')
			    	->where('e.user = ?1')
			    	->andwhere('e.status = 1')
			    	->orderBy('e.id', 'DESC')
    				->setParameter(1, $userId);
    	$paginator = new Zend_Paginator(new Sip_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	->setItemCountPerPage(20);
    	 
    	$this->view->entries = $paginator;
    }


}

