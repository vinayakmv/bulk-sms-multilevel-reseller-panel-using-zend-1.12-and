<?php

use modules\voice\models\VoiceCampaignManager;
class Voice_CampaignController extends Smpp_Controller_BaseController
{

    public function init()
    {
    	$broadcast = $this->view->navigation()->findOneByLabel('Voice');
    	if ($broadcast) {
    		$broadcast->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
    	parent::init();
    }

    public function indexAction()
    {
        $this->view->subject="CREATE CAMPAIGN";
    	$page = $this->view->navigation()->findOneByUri('/voice/campaign/index');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Voice | Quick');
    	}
    	
        $form = new Voice_Form_Compose();
        $currentDate = date("Y-m-d");
        $currentTime = date("H:i");
        
        $userId = Zend_Auth::getInstance()->getIdentity()->id;        
        $emUser = $this->getEntityManager('user');
        $emVoice = $this->getEntityManager('voice');
        $emSmpp = $this->getEntityManager('smpp');
        
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);   

        if ($userRepo->openSenderId){
    		$form->getElement('typeSenderId')->setRequired(true);
    		$this->view->senderId = $form->typeSenderId;
    	}else{
    		$senderIdRepo = $emVoice->getRepository('modules\voice\models\VoiceSenderId')->findBy(array('user'=>$userRepo->id,'type'=>'1','status'=>'1'),array('default' => 'asc'));
    		$DefaultSenderIdRepo = $emVoice->getRepository('modules\voice\models\VoiceSenderId')->findOneBy(array('user'=>$userRepo->id,'type'=>'1','status'=>'1', 'default' => '1'));
    		$default = array(); 
    		$sender = array();
    		if (isset($DefaultSenderIdRepo)){
    			$defaultSender = trim($DefaultSenderIdRepo->name);
    		}   
    		
    		foreach ($senderIdRepo as $senderId) {
    			
    			if (isset($defaultSender) && $senderId->name == $defaultSender){
    				$name = strval($senderId->name);
    				$default[$name]=$name;
    			}else{
    				$name = strval($senderId->name);
    				$sender[$name]=$name;
    			}
    			
    		}
    		
    		if (!empty($default)){
    			$sender = $default+$sender;
    		}
    		//reset($sender);
    		
    		if (isset($sender)) {
    			$form->getElement('selectSenderId')->setMultiOptions($sender)->setRequired(true);
    			//$form->getElement('selectSenderId')->setValue($defaultSender);
    			$this->view->senderId = $form->selectSenderId;
    		}
    	}
    	//**************FIND SENDER ID END ****************************\\   
        
        $voiceRepos = $emVoice->getRepository('modules\voice\models\VoiceFile')->findBy(array('user'=>$userRepo->id,'status'=>'1'));
        
        $audios['0'] = 'NULL';
        foreach ($voiceRepos as $voiceRepo) {
        	$audios[$voiceRepo->id] = $voiceRepo->name;
        }
        
        if (isset($audios)) {
        	$form->getElement('selectAudio')->setMultiOptions($audios);
        }
        
        $voiceRouteRepo = $userRepo->voiceRoutes;
        //$routeRepo = $em->getRepository('Models\Route')->findBy(array('status'=>'1'));
        //print_r($routeRepo);
        
        foreach ($voiceRouteRepo as $routes) {
        	$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
        }
        if (empty($route)){
        	$trans = $userRepo->domain->voiceTransRoute;
        	$promo = $userRepo->domain->voicePromoRoute;
        	if ($trans){
        		$routes = $emUser->getRepository('modules\user\models\VoiceRoute')->findOneBy(array('id'=>$trans,'status'=>'1'));
        		$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
        	}
        	if ($promo){
        		$routes = $emUser->getRepository('modules\user\models\VoiceRoute')->findOneBy(array('id'=>$promo,'status'=>'1'));
        		$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".' | '."$routes->name";
        	}
        }
        if (isset($route)) {
        	$form->getElement('voiceType')->setMultiOptions($route);
        }
        
        //**************FIND LEADS ID START ****************************\\
        $leadRepo = $emUser->getRepository('modules\user\models\Lead')->findBy(array('user'=>$userRepo->id,'status'=>'1','type'=>'2'));
        foreach ($leadRepo as $leads) {
        	$contacts[$leads->id] = $leads->name;
        }
        if (isset($contacts)) {
        	$form->getElement('selectContact')->setMultiOptions($contacts);
        }
        //**************FIND LEADS ID STOP ****************************\\
        
        $groupRepos = $emUser->getRepository('modules\user\models\Lead')->findBy(array('user' => $userId));
        $this->view->allgroups = $groupRepos;
        
        $this->view->form = $form;
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        	
        	$originalFilename = pathinfo($form->uploadAudio->getFileName());
        	$newFilename = 'uploadAudio-' . uniqid() . '.' . $originalFilename['extension'];
        	$form->uploadAudio->addFilter('Rename', $newFilename);
        	
        	$originalFilename1 = pathinfo($form->uploadContact->getFileName());
        	$newFilename1 = 'uploadContact-' . uniqid() . '.' . $originalFilename1['extension'];
        	$form->uploadContact->addFilter('Rename', $newFilename1);
        	
        	$campaignValues = $form->getValues();
        	
        	if($campaignValues['scheduleRadio']=='on'){
           		$getVal=$this->getRequest()->getPost();
        		if ($campaignValues['datePicker'] == ""){
        			$campaignValues['datePicker']=date('Y-m-d');
        		}else{
        			$campaignValues['datePicker']=date('Y-m-d',strtotime($getVal['datePicker']));
        		}
        		if ($campaignValues['timePicker'] == ""){
        			$campaignValues['timePicker']=date('H:i:s');
        		}else{
        			$campaignValues['timePicker']=date('H:i:s',strtotime($getVal['timePicker']));
        		}
        		$campaignValues['input'] = 'SCHEDULED';
        	}
        	else{
        		$campaignValues['datePicker']=date('Y-m-d');
        		$campaignValues['timePicker']=date('H:i:s');
        		$campaignValues['input'] = 'COMPOSE';
        	}
        	
        	
        	$campaignManager = new VoiceCampaignManager();
        	$campaignCreated = $campaignManager->createCampaign($campaignValues, $userId, false, 'WEB-PUSH', $emVoice, $emUser, $emSmpp);
        	
        	$form->reset();
        	
        	if (isset($campaignCreated['Campaign']['uploadAudio'])) {
        		$this->view->errors = array('Campaign' => array('isFailed' => 'Audio file is missing'));
        	}elseif (isset($campaignCreated['Campaign']['uploadContact'])){
        		$this->view->errors = array('Campaign' => array('isFailed' => 'Contact file is missing'));
        	}elseif (isset($campaignCreated['Campaign']['contactCount'])) {
        		$this->view->errors = array('Campaign' => array('isFailed' => 'Contacts must be between 1 and 50,000'));
        	}elseif (isset($campaignCreated['Campaign']['credit'])) {
        		$this->view->errors = array('Campaign' => array('isFailed' => 'Insufficient credit'));
        	}elseif (isset($campaignCreated['Campaign']['isSuccess'])) {
        		//Campaign Created
        		$this->view->success = array('Campaign' => array('isCreated' => 'Campaign created successfully'));
        	}else{
        		$this->view->errors = $campaignCreated;
        	}
        }else{
        	 if (count($form->getErrors('token')) > 0) {
        		$this->_redirect('/voice/campaign/index');
        	} 
        	$error = $form->getMessages();
    		if (!isset($error['captcha']['badCaptcha'])){
    			$form->reset();
    		}
    		
    		$this->view->errors = $error;
        }
        $this->view->form = $form;
        
    }


}
