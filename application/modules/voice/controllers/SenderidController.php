<?php
use modules\voice\models\VoiceSenderIdManager;
class Voice_SenderidController extends Smpp_Controller_BaseController
{

  public function init()
  {
    $broadcast = $this->view->navigation()->findOneByLabel('Senders');
    if ($broadcast) {
      $broadcast->setActive();
    }
    $action = $this->getRequest()->getActionName();
    $emUser = $this->getEntityManager('user');
    $domain = Smpp_Utility::getFqdn();
    $domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    $domainLayout = $domainRepo->layout;
    if ($domainLayout != null){
    	$view = $domainLayout.'-'.$action;
    	$this->_helper->viewRenderer("$view");
    }
    parent::init();
  }

  public function indexAction()
  {
    // action body
  }

  public function requestAction()
  {
    $this->view->subject="SENDER ID REQUEST";

    $page = $this->view->navigation()->findOneByUri('/voice/senderid/request');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('Senders | Voice Request');
    }

    $form = new Voice_Form_Sender();
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    
    //$emUser = $this->getEntityManager('user');
    $emVoice = $this->getEntityManager('voice');
    
    //$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    //$leadRepo = $em->getRepository('Models\Lead')->findBy(array('user'=>$userRepo->id,'status'=>'1','type'=>'1'));
    //$senderIdRepo = $emVoice->getRepository('modules\voice\models\VoiceSenderId')->findBy(array('user'=> $userId,'type'=>'1','status'=>'1'));
    $this->view->form = $form;

    if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {

      $getValues = $form->getValues();
      $name=$getValues['senderId'];
      $remark=$getValues['userremark'];
      $senderIdManager = new VoiceSenderIdManager();
      $createSenderId = $senderIdManager->createSenderId($userId, $name, '1', $emVoice, '0',$remark);

      if (isset($createSenderId['SenderId']['isCreated'])) {
        $this->view->success = array('SenderId' => array('isSuccess' => $createSenderId['SenderId']['isCreated']));
      }elseif (isset($createSenderId['SenderId']['senderCount'])) {
        $this->view->errors = array('SenderId' => array('isFailed' => $createSenderId['SenderId']['senderCount']));
      }else{
      	$this->view->errors = array('SenderId' => array('isFailed' => 'SenderId request failed'));
      }
    }else{
      $this->view->errors = $form->getMessages();
    }
    $form->reset();
  }
  public function approvalAction()
  {
    $this->view->subject="SENDER ID APPROVAL";
    $page = $this->view->navigation()->findOneByUri('/voice/senderid/approval');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('Senders | Voice Approval');
    }

    $emVoice = $this->getEntityManager('voice');
    $this->view->emUser = $this->getEntityManager('user');
    
    $userId = Zend_Auth::getInstance()->getIdentity()->id;

    if ($this->getRequest()->isPost()) {

      $getValues = $this->getRequest()->getPost();
      $senderIdManager = new VoiceSenderIdManager();

      if(isset($getValues['approved'])){
	        $manageSenderId = $senderIdManager->manageSenderId($getValues,'Approved', $userId, $emVoice);
	        $this->view->Success = array('approved' => array('success' => 'Approved successfully'));

      }elseif(isset($getValues['Rejected'])) {
	       $manageSenderId = $senderIdManager->manageSenderId($getValues,'Rejected', $userId, $emVoice);
	       $this->view->Success = array('rejected' => array('success' => 'Rejected Successfully'));
      }

   }

   $queryBuilder = $emVoice->createQueryBuilder();
   $queryBuilder->select('e')
   ->from('modules\voice\models\VoiceSenderId', 'e')
   ->where('e.status = ?1')
   ->orderBy('e.id', 'DESC')
   ->setParameter(1, '0');
   $query = $queryBuilder->getQuery();
   $this->view->entries = $query->getResult();
   /*
   $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
   $currentPage = 1;
  //$i = $this->_request->getParam('i');
   $i = $this->_request->getParam('p');
   if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i=='')
    {
      $this->view->pageIndex = $i;
    }
    else
    {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    } 

    $this->view->entries = $paginator;
    */
    $this->view->userId = $userId;
  }
  public function viewAction()
  {
    $this->view->subject="SENDER ID VIEW";
    $page = $this->view->navigation()->findOneByUri('/voice/senderid/view');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('Senders | Voice Senders');
    }

      //$reportManager = new SendSmsManager();
    $emVoice = $this->getEntityManager('voice');
    
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    if ($this->getRequest()->isPost()) {
    
    	$getValues = $this->getRequest()->getPost();
    	 
    	$senderIdManager = new VoiceSenderIdManager();
    	if($getValues['submit'] == 'Delete'){
    		$manageSenderId = $senderIdManager->manageSenderId($getValues,'Delete', $userId, $emVoice);
    		//$this->view->Success = array('approved' => array('success' => 'Approved successfully'));
    		 
    	}elseif ($getValues['submit'] == 'default') {
    		$manageSenderId = $senderIdManager->manageSenderId($getValues,'Default', $userId, $emVoice);
    		//$this->view->Success = array('rejected' => array('success' => 'Rejected Successfully'));
    	}
    }
    $queryBuilder = $emVoice->createQueryBuilder();
    $queryBuilder->select('e')
    ->from('modules\voice\models\VoiceSenderId', 'e')
    ->where('e.user = ?1')
    ->andwhere('e.type = ?2')
    ->andwhere('e.status != 5')
    ->orderBy('e.id', 'DESC')
    ->setParameter(1, $userId)
    ->setParameter(2, 1);
    $query = $queryBuilder->getQuery();
    $this->view->entries = $query->getResult();
    /*
    $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    $currentPage = 1;
  //$i = $this->_request->getParam('i');
    $i = $this->_request->getParam('p');
    if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i=='')
    {
      $this->view->pageIndex = $i;
    }
    else
    {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    } 

    $this->view->entries = $paginator;
    */
    $this->view->userId = $userId;
    
  }
  public function viewallAction()
  {
    $this->view->subject="SENDER ID VIEW";
    $page = $this->view->navigation()->findOneByUri('/voice/senderid/viewall');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('Senders | All Voice Senders ');
    }
    
      //$reportManager = new SendSmsManager();
    $emVoice = $this->getEntityManager('voice');
    
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    $queryBuilder = $emVoice->createQueryBuilder();
    $queryBuilder->select('e')
    ->from('modules\voice\models\VoiceSenderId', 'e')
    ->where('e.type = ?1')
    ->orderBy('e.id', 'DESC')
    ->setParameter(1, 1);
    $query = $queryBuilder->getQuery();
    $this->view->entries = $query->getResult();
    /*
    $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    $currentPage = 1;
  //$i = $this->_request->getParam('i');
    $i = $this->_request->getParam('p');
    if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i=='')
    {
      $this->view->pageIndex = $i;
    }
    else
    {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    } 

    $this->view->entries = $paginator;
    */
    $this->view->userId = $userId;
  }


}







