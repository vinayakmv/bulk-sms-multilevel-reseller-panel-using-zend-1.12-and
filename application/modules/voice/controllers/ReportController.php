<?php

use modules\voice\models\VoiceCampaignManager;
use Doctrine\ORM\Query\ResultSetMapping;
use modules\user\models\ActivityLogManager;
use modules\user\models\AccountManager;
class Voice_ReportController extends Smpp_Controller_BaseController
{

    public function init()
    {
    	$reports = $this->view->navigation()->findOneByLabel('Voice');
    	if ($reports) {
    		$reports->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
        parent::init();
    }

    public function indexAction()
    {
       $this->view->subject="CAMPAIGN REPORTS";
    	$page = $this->view->navigation()->findOneByUri('/voice/report/index');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Voice | Sent');
    	}
    	
     
	  	$sdate=date('m/d/Y',time());	
	  	$edate=date('m/d/Y',time());
	 // $emUser = $this->getEntityManager('user');
	  $emVoice = $this->getEntityManager('voice');
	  
	  $userId = Zend_Auth::getInstance()->getIdentity()->id;
	  if ($this->getRequest()->isPost()) {
	    $getValue=$this->getRequest()->getPost();
	
	              //  print_r($getValue);exit();
	    if(isset($getValue['search'])){
	      $sdate=$getValue['from'];
	      $edate=$getValue['to'];
	    }

	} 
 
  $queryBuilder = $emVoice->createQueryBuilder();

   $currentDate = date('Y-m-d H:i:s',time());
//   $yesterday = date('Y-m-d',strtotime($edate));
  $startDate=date('Y-m-d H:i:s',strtotime($sdate.'00:00:00'));
  $endDate=date('Y-m-d H:i:s',strtotime($edate.'23:59:59'));
  
  $diffDate = date_diff(date_create($startDate),date_create($endDate));
  
  $month = $diffDate->m;
  $day = $diffDate->d;
  
  if (($month == '1' && $day == '0') || ($month == '0' && $day <= '31')){
	  	$queryBuilder->select('e')
	  ->from('modules\voice\models\VoiceCampaign', 'e')
	  ->where('e.user = ?1')
	  ->andwhere('e.startDate >= ?2')
	  ->andwhere('e.startDate <= ?3')
	  ->andwhere('e.startDate <= ?4')
	 // ->andwhere('e.createdDate <= ?4')
	  ->orderBy('e.id', 'DESC')
	  ->setParameter(1, $userId)
	  //->setParameter(2, $currentDate)
	  //->setParameter(3, $yesterday);
	  ->setParameter(2, $startDate)
	  ->setParameter(4, $currentDate)
	  ->setParameter(3, $endDate);
	  $query = $queryBuilder->getQuery();
	  $this->view->entries = $query->getResult();
  }else{
  	$this->view->entries = array();
  	$this->view->errors = array('Report' => array('inSearch' => "Date range should not exceed 31 days"));
  }
  

 
  $this->view->pageIndex = 1;
 

    $this->view->daterange=$sdate.'^'.$edate;
    $this->view->userId = $userId;
    }
    
    public function scheduledAction()
    {
    	$this->view->subject="CAMPAIGN SCHEDULED";
    	$page = $this->view->navigation()->findOneByUri('/voice/report/scheduled');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Voice | Scheduled');
    	}
    
    	
    	$emVoice = $this->getEntityManager('voice');
    	$emUser = $this->getEntityManager('user');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	if ($this->getRequest()->isPost()) {
    		$getValue=$this->getRequest()->getPost();
    
    		if(isset($getValue['gid'])){
    
    			$generateId=$getValue['gid'];
    			$voiceCampaignManager = new VoiceCampaignManager();
    
    			$checkCampagin = $emVoice->getRepository('modules\voice\models\VoiceCampaign')->find($generateId);
    
    			if($checkCampagin->status == 0){
    				 
    				$cancelCampaign = $voiceCampaignManager->cancellCampaign($generateId, $emVoice);
    				if ($cancelCampaign){
    					$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    					$activityManager = new ActivityLogManager();
    					$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Campaign Cancelled Successfully', $this->_domain, $this->_ip, $this->_device, $generateId, 'CANCEL', 'CAMPAIGN', $emUser);
    
    					$accountManager = new AccountManager();	
    					if ($cancelCampaign->type == 'TRANSACTIONAL'){
    						if ($cancelCampaign->apiKey == 'WEB-PUSH'){
    							$account = '5';
    						}else{
    							$account = '10';
    						}
    
    					}elseif ($cancelCampaign->type == 'PROMOTIONAL'){
    
    						if ($cancelCampaign->apiKey == 'WEB-PUSH'){
    							$account = '4';
    						}else{
    							$account = '11';
    						}
    					}
    					$accountArray = $accountManager->reCredit($userId, $account, $cancelCampaign->credit, $emUser);
    					$credit = $accountArray['user'];
    					$resellerRepo = $accountArray['reseller'];
    					$logRepo = $accountArray['log'];
    					if ($credit) {
    						$activityManager = new ActivityLogManager();
    						$activityManager->createActivityLog($resellerRepo, $credit->id, $this->_module, $this->_controller, $this->_action, 'Recredit Transfered Successfully', $this->_domain, $this->_ip, $this->_device, $logRepo->id, 'RECREDIT', 'ACCOUNT', $emUser);
    					}
    					$this->view->success = array('Report' => array('isSuccess' => 'Campaign cancelled successfully'));
    					 
    				}else{
    					$this->view->errors = array('Report' => array('inProgress' => "Campaign cancel failed "));
    				}
    
    			}else{
    				$this->view->errors = array('Report' => array('inProgress' => "Campaign cancel failed "));
    			}
    		}
    	}
    
    	$queryBuilder = $emVoice->createQueryBuilder();
    
    	$currentDate=date('Y-m-d H:i:s',time());
    	$queryBuilder->select('e')
    	->from('modules\voice\models\VoiceCampaign', 'e')
    	->where('e.user = ?1')
    	->andwhere('e.startDate >= ?2')
    	->orderBy('e.id', 'DESC')
    	->setParameter(1, $userId)
    	->setParameter(2, $currentDate);
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();

    	$this->view->userId = $userId;
    }

    public function summaryAction()
    {
    	//$this->_helper->viewRenderer->setNoRender(true);
    	$this->_helper->layout->disableLayout();
    	$emVoice = $this->getEntityManager('voice');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$campId = $this->_request->getParam('p');
    	if ($campId){
    		$voiceCampagin = $emVoice->getRepository('modules\voice\models\VoiceCampaign')->find($campId);
    
    		$rsm = new ResultSetMapping();
    		// build rsm here
    		$dlrUrl = "$userId".'@'."$campId".'-';
    		$queryBuilder = $emVoice->createQueryBuilder();
    		if ($voiceCampagin->type == 'TRANSACTIONAL'){
    			$queryBuilder->select('u.sender,u.receiver,u.dlrdata,u.status')
    			->from('modules\voice\models\SentVoiceTrans', 'u')
    			->where('u.dlrUrl LIKE ?1')
    			->setParameter("1", $dlrUrl.'%');
    		}else {
    			$queryBuilder->select('u.sender,u.receiver,u.dlrdata,u.status')
    			->from('modules\voice\models\SentVoicePromo', 'u')
    			->where('u.dlrUrl LIKE ?1')
    			->setParameter("1", $dlrUrl.'%');
    		}
    
    
    		$results = $queryBuilder->getQuery()->getResult();
    		$send = 0;
    		$dlrReceived = 0;
    		$delivered = 0;
    		$failed = 0;
    
    		foreach ($results as $row){
    			$send++;
    			if ($row['status'] == '1'){
    				$dlrReceived++;
    				$dlrdata = urldecode($row['dlrdata']);
    				if (strpos($dlrdata, 'ANSWERED')!== FALSE ){
    					$delivered++;
    				}else{
    					$failed++;
    				}
    			}
    		}
    		$this->view->send = $send;
    		$this->view->dlrReceived = $dlrReceived;
    		$this->view->delivered = $delivered;
    		$this->view->failed = $failed;
    		$this->view->submit = $send-$dlrReceived;
    
    	}
    }

}

