<?php

class Domain_Form_Profile extends Zend_Form
{
   public function init()
    {
        $this->setAction('/domain/manage/index');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	
		$company = new Zend_Form_Element_Text('company');
		$company->setDecorators(array('ViewHelper','Description'));
		$company->setLabel('Company');
		$company->class = "form-control";
		$company->setRequired(false);
			
		//Add Validator
		//$campaign->addValidator(new Zend_Validate_StringLength(4,20));
		//Add Filter
		$company->addFilter(new Zend_Filter_HtmlEntities());
		$company->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($company);
		

		$intro = new Zend_Form_Element_Textarea('intro');
		$intro->setDecorators(array('ViewHelper','Description'));
		$intro->setLabel('Introduction');
		$intro->class = "form-control";
		$intro->setAttrib('rows', '10');
		$intro->setAttrib('cols', '30');
		$intro->setRequired(false);
		//Add Validator
		//$campaign->addValidator(new Zend_Validate_StringLength(4,20));
		//Add Filter
		//$intro->addFilter(new Zend_Filter_HtmlEntities());
		$intro->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($intro);
		
		
		$about = new Zend_Form_Element_Textarea('about');
		$about->setDecorators(array('ViewHelper','Description'));
		$about->setLabel('About');
		$about->class = "form-control";
		$about->setAttrib('rows', '10');
		$about->setAttrib('cols', '30');
		$about->setRequired(false);
		//Add Validator
		//$campaign->addValidator(new Zend_Validate_StringLength(4,20));
		//Add Filter
		//$about->addFilter(new Zend_Filter_HtmlEntities());
		$about->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($about);
		
                $accounttype = new Zend_Form_Element_Select('theme');
    	$template = array("1" => "DARK BOARD",
    			"2" => "LIGHT ADMIN",
                        "3" => "LIGHT LAB",
                        "4" => "LIGHT APP"
    	);
    	$accounttype->addMultiOptions($template);
    	$accounttype->class = "form-control select2 custom-select";
    	$accounttype->setLabel('Theme');
    	$accounttype->setDecorators(array('ViewHelper','Description'));
    	$accounttype->setRequired(true);
    	
    	//Add Validator
    	//Add Filter
    	$accounttype->addFilter(new Zend_Filter_HtmlEntities());
    	$accounttype->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($accounttype);
		 
		$uploadLogo = new Zend_Form_Element_File('uploadLogo', array('style'=>'display:none;', 'onchange'=>'$("#upload-file-primary").html($(this).val());'));
		$uploadLogo->setDecorators(array('File'));
		$uploadLogo->setLabel('Upload Logo');
		$uploadLogo->setDestination('template/images/users/');
		$uploadLogo->class = "file-upload-default";
		$uploadLogo->addValidator('Count', false, 1);
		$uploadLogo->addValidator('Extension', false, array('jpg','png'));
		$uploadLogo->setMaxFileSize(10485760);
		$uploadLogo->setRequired(false);
		$this->addElement($uploadLogo); 
		
		
		
		$address = new Zend_Form_Element_Textarea('address');
		$address->setDecorators(array('ViewHelper','Description'));
		$address->setLabel('Address');		
		$address->setAttrib('rows', '10');
		$address->setAttrib('cols', '30');
		$address->class = "form-control";
		$address->setRequired(false);
			
		//Add Validator
		//$campaign->addValidator(new Zend_Validate_StringLength(4,20));
		//Add Filter
		//$addrOne->addFilter(new Zend_Filter_HtmlEntities());
		$address->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($address);
		

		
		//Create a submit button.
		$this->addElement('submit', 'submit');
		$submitElement = $this->getElement('submit');
		$submitElement->setAttrib('class',"btn-primary btn");
		$submitElement->setDecorators(array('ViewHelper',
											'Description'));
		$submitElement->setLabel('Submit');
		 
		$this->setDecorators(array('FormElements','Form'
		));
    }



}

