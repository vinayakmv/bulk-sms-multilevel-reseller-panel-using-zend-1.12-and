<?php

use modules\user\models\DomainManager;
use modules\user\models\ActivityLogManager;
class Domain_ManageController extends Smpp_Controller_BaseController
{

    public function init()
    {
    	$broadcast = $this->view->navigation()->findOneByLabel('Profile');
    	if ($broadcast) {
    		$broadcast->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
    	parent::init();
    }

    public function indexAction()
    {
    	$this->view->subject="WEB PROFILE";
    	$page = $this->view->navigation()->findOneByUri('/domain/manage/index');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Profile | Domain');
    	}
    	 
    	$form = new Domain_Form_Profile();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId); 
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('owner' => "$userId"));
    	
    	$footer = $domainRepo->footer;
    	$introduction = $domainRepo->domainProfile->introduction;
    	$about = $domainRepo->domainProfile->about;
    	$logo = $domainRepo->logo;
    	$address = $domainRepo->domainProfile->address;
    	
        if ($domainRepo->layout == 'boardster'){
            $theme = '1';
        }elseif ($domainRepo->layout == 'adminto') {
            $theme = '2';
        }elseif ($domainRepo->layout == 'flatlab') {
            $theme = '3';
        }elseif ($domainRepo->layout == 'luna'){
            $theme = '4';
        }
        
        $form->getElement('theme')->setValue($theme);
        
    	$form->getElement('company')->setValue($footer);
    	$form->getElement('intro')->setValue(urldecode($introduction));
    	$form->getElement('about')->setValue(urldecode($about));
    	$form->getElement('address')->setValue(urldecode($address));
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {

    		@$originalFilename = pathinfo($form->uploadLogo->getFileName());
    		$newFilename = uniqid().'.'.$originalFilename['extension'];
    		$form->uploadLogo->addFilter('Rename', $newFilename);
    		    		
    		$profileValues = $form->getValues();
    		
    		$domainManager = new DomainManager();
    		$updateProfile = $domainManager->updateProfile($profileValues, $domainRepo->id, $userId, $emUser);
    		if ($updateProfile){
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userRepo->id, $this->_module, $this->_controller, $this->_action, "Web Profile Updated Successfully", $this->_domain, $this->_ip, $this->_device, $updateProfile->id, 'UPDATE', 'WEB PROFILE', $emUser);
    			$this->view->success = array('Spam' => array('isSuccess' => 'Spam check set successfully'));
    			$this->view->success = array('Profile' => array('isSuccess' => "Profile updated"));
    		}else{
    			$this->view->errors = array('Profile' => array('isFailed' => "Update failed"));
    		}
    		
    	}else{
    		$this->view->errors = $form->getMessages();
    		
    	}
    }


}

