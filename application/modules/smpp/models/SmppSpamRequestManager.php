<?php
namespace modules\smpp\models;
/**
 * 
 * @author Vinayak
 *
 */
class SmppSpamRequestManager  extends \Smpp_Doctrine_BaseManager
{
        public function createSpam($getValues, $userId, $em)
	{
            
		if (isset($em)){
			$name=$getValues['senderId'];	
			$fname=str_replace('+', '%2C', urlencode($name));
			$name1=str_replace("%2C","|",$fname);
			
						
			$sender = new SmppSpamRequest();
			$sender->user = $userId;
			$sender->name = $name1;
			$sender->description = $getValues['description'];
			$sender->status = '0';
			$sender->createdDate = new \DateTime('now');
			$sender->updatedDate = new \DateTime('now');
			$em->persist($sender);
			$em->flush();
				
			return $sender;
			
		}
		return false;
	}
	public function manageSpam($getValues,$status, $userId, $em)
	{
		
		if (isset($getValues)){
			$id=$getValues['uniqId'];    		
			$senderid = $em->getRepository('modules\smpp\models\SmppSpamRequest')->find($id);
			if($status=='approved'){
                            
				$senderid->status = '1';
				$senderid->updatedDate = new \DateTime('now');
				$getValues['smppUser']= $senderid->systemId;
				$getValues['regex']= $senderid->name;
				
				$smppSenderIdTransManager = new SmppSpamTransManager();
				$insertSenderId = $smppSenderIdTransManager->insertSenderId($getValues, $type, $em);

			}
			else if($status=='Rejected'){
				
				$senderid->status = '0';
				$senderid->updatedDate = new \DateTime('now');
			}
			
			
			$em->persist($senderid);
			$em->flush();
	
			return $senderid;
		}
		return false;
	}
}

