<?php
namespace modules\smpp\models;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class SmppUserTransRepository extends \Smpp_Doctrine_EntityRepository
{
	public function getSmppTrans($userId)
	{
       $rsm = new ResultSetMapping();
       $rsm->addEntityResult('modules\smpp\models\SmppUserTrans', 'u');
       $rsm->addFieldResult('u', 'system_id', 'systemId');
       $rsm->addFieldResult('u', 'default_smsc', 'defaultSmsc');
       $query = $this->getEntityManager()->createNativeQuery("SELECT * FROM users_smpp_users_trans JOIN smpp_users_trans ON smpp_users_trans.system_id =users_smpp_users_trans.system_id WHERE user_id = $userId AND smpp_users_trans.type = 'TRANSACTIONAL'", $rsm);

       return $query->getResult();
   }
   
   public function getSmppScrub($userId)
   {
   	$rsm = new ResultSetMapping();
   	$rsm->addEntityResult('modules\smpp\models\SmppUserTrans', 'u');
   	$rsm->addFieldResult('u', 'system_id', 'systemId');
   	$rsm->addFieldResult('u', 'default_smsc', 'defaultSmsc');
   	$query = $this->getEntityManager()->createNativeQuery("SELECT * FROM users_smpp_users_trans JOIN smpp_users_trans ON smpp_users_trans.system_id =users_smpp_users_trans.system_id WHERE user_id = $userId AND smpp_users_trans.type = 'TRANS-SCRUB'", $rsm);
   
   	return $query->getResult();
   }
   
   public function getEsmes($id, $systemId=NULL)
   {
   	if ($systemId){
   		$sql = "SELECT `system_id` FROM `users_smpp_users_trans` WHERE user_id ='$id' AND system_id = '$systemId'";
   	}else{
   		$sql = "SELECT `system_id` FROM `users_smpp_users_trans` WHERE user_id ='$id'";
   	}
    
    $emSmpp = $this->getEntityManager('smpp');
    $stmt1 = $emSmpp->getConnection()->prepare($sql);
    $stmt1->execute();
    $statement= $stmt1->fetchAll();
    
    return $statement;
    
	}
	
	public function getBalance($userId, $route, $systemId)
	{
		if ($route == 'TL'){
			$sql="SELECT smpp_users_trans.credit,smpp_users_trans.system_id FROM smpp_users_trans INNER JOIN users_smpp_users_trans ON users_smpp_users_trans.system_id=smpp_users_trans.system_id WHERE users_smpp_users_trans.user_id = '$userId' AND  users_smpp_users_trans.system_id = '$systemId'";
			
		}elseif ($route == 'PL'){
			$sql="SELECT smpp_users_promo.credit,smpp_users_promo.system_id FROM smpp_users_promo INNER JOIN users_smpp_users_promo ON users_smpp_users_promo.system_id=smpp_users_promo.system_id WHERE users_smpp_users_promo.user_id = '$userId' AND  users_smpp_users_promo.system_id = '$systemId'";
			
		}elseif ($route == 'IL'){
			$sql="SELECT smpp_users_inter.credit,smpp_users_inter.system_id FROM smpp_users_inter INNER JOIN users_smpp_users_inter ON users_smpp_users_inter.system_id=smpp_users_inter.system_id WHERE users_smpp_users_inter.user_id = '$userId' AND  users_smpp_users_inter.system_id = '$systemId'";
			
		}
		
		try {
			$stmt = $this ->getEntityManager()->getConnection()->prepare($sql);
			$stmt->execute();
		} catch ( \Doctrine\DBAL\DBALException  $e) {
			return false;
		}
		
		return $stmt->fetchAll();
	}

}