<?php
namespace modules\smpp\models;

class SmppErrorMapPromoManager extends \Smpp_Doctrine_BaseManager
{
	public function insert($getArray, $em)
	{
            
		$id = $getArray['id'];
                
                $routeArray = explode('|', $getArray['route']);
		$smscId = trim($routeArray['0']);
		@$routeType = trim($routeArray['1']);
                
                if ($getArray['route'] == 'all' || $getArray['esme'] == $routeType){
                    
                    
                        
                        $dlrRepo = $em->getRepository('modules\smpp\models\SmppErrorCodeRequest')->find($id);
                        
                        $opcoErrorCodes = $dlrRepo->opcoErrorCodes;
                        $opco = explode('|', $opcoErrorCodes);
                       
                       foreach ($opco as $key=>$value){
                        $error = new SmppErrorMapPromo();
                        $error->smscId = $smscId;
			$error->opcoErrorCode = $value;
                        $error->errorCodeMap = $dlrRepo->errorCode;
			$error->createdDate = new \DateTime('now');
			$error->updatedDate = new \DateTime('now');
			
                        $em->persist($error);
                        $em->flush();
                       }
                    
                    
		    $dlrRepo->status = '1';
                    $dlrRepo->smscId = $smscId;
                    $dlrRepo->type = $getArray['esme'];
                    $dlrRepo->updatedDate = new \DateTime('now');
                    
                    $em->persist($dlrRepo);
                    $em->flush();
                    
		    return $error;
                }
            
	     return false;	
		
	}
        
        public function delete($getArray, $em)
	{
		$id = $getArray['id'];
                
		$routeType = $getArray['esme'];
		$smscId = $getArray['route'];
                
                if (isset($getArray['route'])){
                    
                    
                        
                        $dlrRepo = $em->getRepository('modules\smpp\models\SmppErrorCodeRequest')->find($id);
                        
                        $opcoErrorCodes = $dlrRepo->opcoErrorCodes;
                        $opco = explode('|', $opcoErrorCodes);
                       
                       foreach ($opco as $key=>$value){
                           
                        $errorRepo = $em->getRepository('modules\smpp\models\SmppErrorMapPromo')->findOneBy(array('opcoErrorCode' => "$value", 'smscId' => $smscId));
                        			
                        $em->remove($errorRepo);
                        $em->flush();
                       }
                    
                    
		    $dlrRepo->status = '0';
                    $dlrRepo->smscId = '';
                    $dlrRepo->type = '';
                    $dlrRepo->updatedDate = new \DateTime('now');
                    
                    $em->persist($dlrRepo);
                    $em->flush();
                    
		    return $errorRepo;
                }
            
	     return false;	
		
	}
}