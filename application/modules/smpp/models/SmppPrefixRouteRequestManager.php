<?php
namespace modules\smpp\models;
/**
 * 
 * @author Vinayak
 *
 */
class SmppPrefixRouteRequestManager  extends \Smpp_Doctrine_BaseManager
{

	public function createPrefix($getValues, $userId, $em)
	{
		if (isset($em)){
			$name = str_replace(' ', ",", $getValues['prefix']);			
			$name = explode(",", $name);
			$prefix="";
			
			foreach ($name as $key => $value){
				
					
					$exp = explode('-',$value);
			
					if (!isset($exp[1])){
						$prefix .= $value.'|';
					}else{
			
						$start = $exp[0];
						$end = $exp[1];
						for ($i=$start;$i<=$end;$i++){
							$prefix .= $i.'|';
						}
					}
			}
					
			$sender = new SmppPrefixRouteRequest();
			$sender->user = $userId;
			$sender->name = rtrim($prefix,"|");
			$sender->description = $getValues['description'];
			$sender->status = '0';
			$sender->createdDate = new \DateTime('now');
			$sender->updatedDate = new \DateTime('now');
			$em->persist($sender);
			$em->flush();
				
			return $sender;
			
		}
		return false;
	}
	public function managePrefix($getValues,$status, $userId, $em)
	{
		
		if (isset($getValues)){
			$id=$getValues['uniqId'];    		
			$senderid = $em->getRepository('modules\smpp\models\SmppPrefixRouteRequest')->find($id);
			if($status=='approved'){
				$senderid->status = '1';
				$senderid->updatedDate = new \DateTime('now');
				$getValues['smppUser']= $senderid->systemId;
				$getValues['regex']= $senderid->name;
				
				$smppSenderIdTransManager = new SmppRouteTransManager();
				$insertSenderId = $smppSenderIdTransManager->insertSenderId($getValues, $type, $em);

			}
			else if($status=='Rejected'){
				
				$senderid->status = '0';
				$senderid->updatedDate = new \DateTime('now');
			}
			
			
			$em->persist($senderid);
			$em->flush();
	
			return $senderid;
		}
		return false;
	}
	
}
