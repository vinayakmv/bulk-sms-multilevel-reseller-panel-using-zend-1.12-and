<?php
namespace modules\smpp\models;
use Doctrine\ORM\EntityRepository;

/**
 * 
 * @author Ani
 *
 */
class SmppSenderIdRequestRepository extends \Smpp_Doctrine_EntityRepository
{
	public function addSenderId($getArray,$userId,$ubId)
	{
		
		$user=$getArray['user'];
		$template=$getArray['senderId'];
		$cdate=date("Y-m-d H:i:s");
		$sql = "INSERT INTO `smpp_sender_id_request` (`sender_id`, `ub_id`, `system_id`, `regex`,`status`,`remark`,`updated_date`) VALUES (NULL, '$ubId', '$user', '$template','0','request','$userId','$cdate')" ;
		$em = $this->getEntityManager('smppit');
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute(); 
		return $stmt;
	
	}
	public function updateRequestTemplate($getArray,$userId)
	{
		if(isset($getArray['rejected']))$status='2';
		else if(isset($getArray['approved']))$status='1';
		$id=$getArray['id'];
		$cdate=date("Y-m-d H:i:s");
		$remark=$getArray['remark'];
		$sql = "UPDATE `smpp_sender_id_request` SET `remark` = '$remark',`status`='$status',`updated_by`='$userId',`updated_date`='$cdate' WHERE `sender_id` = '$id'" ;
		$em = $this->getEntityManager('smppit');
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		return $stmt;
	
	}
	//Block or Unlock an senderId admin view
	public function updateBlock($getArray)
	{
		$uId=$getArray['id'];
		$block=$getArray['block'];
		$em = $this->getEntityManager();
		/*$fetchData =$em->getRepository('Models\SmppSenderId')->fetchData($uId);
		print_r($fetchData);
		exit();*/
		if($block=='Block')
		{
			$sql = "UPDATE `smpp_sender_id_request` SET `status` = '3' WHERE `sender_id` = '".$uId."'" ;
			$value="User ".$uId."blocked Successfully";
		}
		else if($block=='Unblock')
		{
			$sql = "UPDATE `smpp_sender_id_request` SET `status` = '1' WHERE `sender_id` = '".$uId."'" ;
			$value="User ".$uId."unblocked Successfully";
		}
		
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		return $value;
	}
	public function fetchData($user)
	{	
		if($date==''){
			$sdate=date("Y-m-d");
			$edate=date("Y-m-d");
		}
		else{
			$dates = explode(' - ',$date);
            $sdate = strtotime($dates[0].'00:00:00' );
            $edate =strtotime($dates[1].'23:59:59');
		}
		echo $sql = "SELECT * FROM smpp_sender_id_request WHERE `sender_id` = '".$uId."'";
		
		//echo "SELECT count(`sms_type`) AS success FROM smpp_log WHERE sms_type='2' AND service='".$userId."' AND time >= '$sdate' and time <= '$edate'";
		$em = $this->getEntityManager();
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		$statement= $stmt->fetchAll();
		$value=$statement['0']['success'];
		return $value;
	
	}
}