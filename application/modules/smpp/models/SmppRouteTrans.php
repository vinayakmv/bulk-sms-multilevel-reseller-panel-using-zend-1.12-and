<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppRouteTransRepository")
 * @Table(name="smpp_route_trans",indexes={
 * @Index(name="search_sendx", columns={"system_id","smsc_id"})
 * }))
 * @author Vinayak
 *
 */
class SmppRouteTrans
{
	/**
	 * @Column(name="route_id", type="bigint", nullable=true)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var bigint
	 */
	private $id;

	/**
	 * @Column(name="direction", type="integer")
	 * @var string
	 */
	private $direction;

	/**
	 * @Column(name="source_regex", type="text",length="65500", nullable=true)
	 * @var string
	 */
	private $sourceRegex;

	/**
	 * @Column(name="regex", type="text",length="65500", nullable=true)
	 * @var string
	 */
	private $regex;

	/**
	 * @Column(name="priority", type="integer")
	 * @var integer
	 */
	private $priority;
		
	/**
	 * @Column(name="cost", type="integer")
	 * @var text
	 */
	private $cost;

	/**
	 * @Column(name="system_id", type="string")
	 * @var string
	 */
	private $systemId;

	/**
	 * @Column(name="smsc_id", type="string")
	 * @var string
	 */
	private $smsc;
		
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
