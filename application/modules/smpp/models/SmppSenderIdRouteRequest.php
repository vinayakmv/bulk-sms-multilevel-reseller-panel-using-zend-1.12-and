<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppSenderIdRouteRequestRepository")
 * @Table(name="smpp_sender_route_request",indexes={
 * @Index(name="search_sendx", columns={"status","created_date"})
 * }))
 * @author Vinayak
 *
 */
class SmppSenderIdRouteRequest
{
	/**
	 * @Column(name="id", type="bigint", nullable=true)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var bigint
	 */
	private $id;

	/**
	 * @Column(name="user_id", type="string")
	 * @var string
	 */
	private $user;

	/**
	 * @Column(name="name", type="text")
	 * @var text
	 */
	private $name;
	
	/**
	 * @Column(name="smsc_id", type="string", nullable=true)
	 * @var text
	 */
	private $smscId;
	
	/**
	 * @Column(name="system_id", type="string", nullable=true)
	 * @var text
	 */
	private $systemId;
	
	/**
	 * @Column(name="status", type="integer")
	 * @var integer
	 */
	private $status;

	/**
	 * @Column(name="description", type="string", nullable="true")
	 * @var string
	 */
	private $description;
	
	/**
	 * @column(name="created_date", type="datetime", nullable="false")
	 * @var datetime
	*/	 
	private $createdDate;

	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	*/
	 
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}