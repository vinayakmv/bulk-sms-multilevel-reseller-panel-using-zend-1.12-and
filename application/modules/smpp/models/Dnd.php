<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\DndRepository")
 * @Table(name="smpp_ndnc",indexes={@Index(name="search_idx", columns={"number"})})
 * @author Vinayak
 *
 */
class Dnd
{
		
	/**
	 * @Column(name="number", type="bigint", nullable=false)
	 * @Id
	 * @var integer
	 */
	private $number;
	
	/**
	 * @Column(name="status", type="string", length="1")
	 * @var text
	 */
	private $status;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
}