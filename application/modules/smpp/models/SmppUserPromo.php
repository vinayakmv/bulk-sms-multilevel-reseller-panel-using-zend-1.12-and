<?php
namespace modules\smpp\models;

use Doctrine\DBAL\Types\IntegerType;
/**
 * @Entity(repositoryClass="modules\smpp\models\SmppUserPromoRepository")
 * @Table(name="smpp_users_promo")
 * @author Vinayak
 *
 */
class SmppUserPromo
{
	/**
	 * @Column(name="system_id", type="string", length="15", nullable=true)
	 * @Id
	 * @var string
	 */
	private $systemId;
	
	/**
	 * @Column(name="connect_allow_ip", type="string", nullable=true)
	 * @var text
	 */
	private $allowIp;
		
	/**
	 * @Column(name="password", type="string", length="64", nullable=true)
	 * @var string
	 */
	private $password;
        
        /**
	 * @Column(name="hash", type="string", length="64", nullable=true)
	 * @var string
	 */
	private $hash;
		
	/**
	 * @Column(name="throughput", type="float", nullable=true)
	 * @var float
	 */
	private $throughput;
		
	/**
	 * @Column(name="default_smsc", type="string", length="15", nullable=true)
	 * @var string
	 */
	private $defaultSmsc;

	/**
	 * @Column(name="ndnc_smsc", type="string", length="15", nullable=true)
	 * @var string
	 */
	private $ndncSmsc;
	
	/**
	 * @Column(name="dest_prefix", type="string", length="10", nullable=true)
	 * @var string
	 */
	private $destPrefix;
		
	/**
	 * @Column(name="default_cost", type="float", nullable=true)
	 * @var float
	 */
	private $defaultCost;
	
	/**
	 * @Column(name="enable_prepaid_billing", length="10", type="integer", nullable=true)
	 * @var integer
	 */
	private $enablePrepaidBilling;
		
	/**
	 * @Column(name="enable_check_ndnc", length="10", type="integer", nullable=true)
	 * @var integer
	 */
	private $enableCheckNdnc;
	
	/**
	 * @Column(name="enable_check_template", length="10", type="integer", nullable=true)
	 * @var integer
	 */
	private $enableCheckTemplate;
	
	/**
	 * @Column(name="enable_check_sender", length="10", type="integer", nullable=true)
	 * @var integer
	 */
	private $enableCheckSender;

	/**
	 * @Column(name="enable_check_blacklist", length="10", type="integer", nullable=true)
	 * @var integer
	 */
	private $enableCheckBlacklist;

	/**
	 * @Column(name="enable_ndnc_routing", length="10", type="integer", nullable=true)
	 * @var integer
	 */
	private $enableNdncRouting;
	
	/**
	 * @Column(name="credit", type="float", nullable=true)
	 * @var float
	 */
	private $credit;
	
	/**
	 * @Column(name="counter", type="integer", nullable=true)
	 * @var Integer
	 */
	private $count;
	
	/**
	 * @Column(name="callback_url", type="string", length="255", nullable=true)
	 * @var string
	 */
	private $callbackUrl;
		
	/**
	 * @Column(name="simulate", type="integer", length="1", nullable=true)
	 * @var Integer
	 */
	private $simulate;

	/**
	 * @Column(name="simulate_deliver_every", type="integer", nullable=true)
	 * @var integer
	 */
	private $simulateDeliverEvery;
		
	/**
	 * @Column(name="simulate_permanent_failure_every", type="integer", nullable=true)
	 * @var integer
	 */
	private $simulatePermanentFailureEvery;

	/**
	 * @Column(name="simulate_temporary_failure_every", type="integer", nullable=true)
	 * @var integer
	 */
	private $simulateTemporaryFailureEvery;
		
	/**
	 * @Column(name="simulate_mo_every", type="integer", nullable=true)
	 * @var integer
	 */
	private $simulateMoEvery;

	/**
	 * @Column(name="max_binds", type="integer", nullable=true)
	 * @var integer
	 */
	private $maxBinds;
	
	/**
	 * @Column(name="type", type="string", nullable=true)
	 * @var integer
	 */
	private $type;
	
	/**
	 * @Column(name="recredit_date", type="date", nullable=true)
	 * @var integer
	 */
	private $recreditDate;

	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}