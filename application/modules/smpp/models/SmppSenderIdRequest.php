<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppSenderIdRequestRepository")
 * @Table(name="smpp_sender_id_request",indexes={
 * @Index(name="search_sendx", columns={"status","created_date"})
 * }))
 * @author Vinayak
 *
 */
class SmppSenderIdRequest
{
	/**
	 * @Column(name="id", type="bigint", nullable=true)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var bigint
	 */
	private $id;

	/**
	 * @Column(name="user_id", type="string")
	 * @var string
	 */
	private $user;

	/**
	 * @Column(name="system_id", type="string")
	 * @var string
	 */
	private $systemId;
	/**
	 * @Column(name="status", type="integer")
	 * @var integer
	 */
	private $status;
		
	/**
	 * @Column(name="name", type="string")
	 * @var text
	 */
	private $name;

	/**
	 * @Column(name="remark", type="string")
	 * @var string
	 */
	private $remark;
	
	/**
	 * @column(name="created_date", type="datetime", nullable="false")
	 * @var datetime
	*/	 
	private $createdDate;

	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	*/
	 
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}