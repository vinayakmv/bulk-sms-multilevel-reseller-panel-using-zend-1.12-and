<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmscRecreditRepository")
 * @Table(name="smsc_recredit")
 * @author Vinayak
 *
 */
class SmscRecredit
{
	/**
	* @Column(name="id", type="integer", nullable=false)
	* @Id
	* @GeneratedValue(strategy="IDENTITY")
	* @var integer
	*/
	private $id;
	
	/**
	 * @column(name="smsc_id", type="string", nullable="false")
	 * @var integer
	 */
	private $smscId;
	
	/**
	 * @column(name="type", type="string", nullable="false")
	 * @var unknown
	 */
	private $type;
	
	/**
	 * @column(name="send", type="integer", nullable="false")
	 * @var unknown
	 */
	private $send;
		
	/**
	 * @column(name="not_send", type="integer", nullable="false")
	 * @var unknown
	 */
	private $notSend;
	
	/**
	 * @column(name="total_dlr", type="integer", nullable="false")
	 * @var unknown
	 */
	private $totalDlr;
	
	/**
	 * @column(name="awaiting_dlr", type="integer", nullable="false")
	 * @var unknown
	 */
	private $awaitingDlr;	

	/**
	 * @column(name="delivered", type="integer", nullable="false")
	 * @var unknown
	 */
	private $delivered;

	/**
	 * @column(name="total_recredit", type="integer", nullable="false")
	 * @var unknown
	 */
	private $totalRecredit;
	
	/**
	 * @column(name="status", type="integer", nullable="false")
	 * @var unknown
	 */
	private $status;
	
	/**
	 * @Column(name="recredit_date", type="date", nullable=true)
	 * @var datetime
	 */
	private $recreditDate;	
		
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
