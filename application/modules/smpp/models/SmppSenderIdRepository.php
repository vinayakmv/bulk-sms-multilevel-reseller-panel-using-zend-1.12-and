<?php
namespace modules\smpp\models;
use Doctrine\ORM\EntityRepository;

/**
 * 
 * @author Ani
 *
 */
class SmppSenderIdRepository extends \Smpp_Doctrine_EntityRepository
{
	public function insertSenderId($getArray)
	{

		$user=$getArray['smppUser'];
		$senderId=trim($getArray['regex']);
		$cdate=date("Y-m-d H:i:s");
		$sqlFetch = "SELECT * FROM smpp_sender WHERE `system_id`='$user'" ;
		$em = $this->getEntityManager();
		$stmtFetch = $em->getConnection()->prepare($sqlFetch);
		$stmtFetch->execute();
		$statement= $stmtFetch->fetchAll();
		if(count($statement)=='0'){
			$regSendrId="(".$senderId.")";
			$sql = "INSERT INTO `smpp_sender` (`sender_id`,`system_id`, `regex`) VALUES (NULL,'$user', '$regSendrId')" ;
		$em = $this->getEntityManager();
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute(); 
		return $stmt;
		}
		else{
			$senderId1=$statement['0']['regex'];
			$senderIdArray=explode( ')', $senderId1);
			$regSendrId=$senderIdArray['0']."|".$senderId.")";
			$sql = "UPDATE `smpp_sender`SET `regex`= '$regSendrId' WHERE `system_id`='$user'" ;
			
			$em = $this->getEntityManager();
			$stmt = $em->getConnection()->prepare($sql);
			$stmt->execute(); 
			return $stmt;

		}
	
	}
}