<?php
namespace modules\smpp\models;
/**
 * 
 * @author Vinayak
 *
 */
class SmppTemplateRequestManager  extends \Smpp_Doctrine_BaseManager
{

	public function addTemplate($getArray,$userId, $em)
	{
		
		$smppUser=$getArray['user'];
		$template=$getArray['text'];
		$cdate=date("Y-m-d H:i:s");
	
		$sql = "INSERT INTO `smpp_template_request`(`user_id`, `system_id`,`status`, `name`,`remark`,`updated_date`,`created_date`) VALUES ('$userId', '$smppUser','0','$template', NULL,'$cdate','$cdate')";
		
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		return $stmt;
	
	}
	public function updateRequestTemplate($getArray, $em)
	{
		$id=$getArray['id'];
		if(isset($getArray['reject'])){
			$status='2';
			$cdate=date("Y-m-d H:i:s");
			$remark=$getArray['remark'];
			$sql = "UPDATE `smpp_template_request` SET `remark` = '$remark',`status`='$status',`updated_date`='$cdate' WHERE `id` = '$id'" ;
			
		}
		else if(isset($getArray['approved'])){
			$status='1';
			$cdate=date("Y-m-d H:i:s");
			$remark=$getArray['remark'];
			$sql = "UPDATE `smpp_template_request` SET `remark` = '$remark',`status`='$status',`updated_date`='$cdate' WHERE `id` = '$id'" ;
			
		}elseif (isset($getArray['delete'])){
			$sql = "DELETE from `smpp_template_request` WHERE `id`='$id'";
		}
		
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		return $stmt;
	
	}
	
	
}
