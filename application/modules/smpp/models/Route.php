<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\RouteRepository")
 * @Table(name="smsc_routes")
 * @author Vinayak
 *
 */
class Route
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="smsc_id", type="string")
	 * @var text
	 */
	private $route;
	
	/**
	 * @Column(name="name", type="string")
	 * @var text
	 */
	private $name;
	
	/**
	 * @Column(name="type", type="string")
	 * @var text
	 */
	private $type;
	
	/**
	 * @Column(name="sms_view", type="integer")
	 * @var text
	 */
	private $smsView;
	
	/**
	 * @Column(name="smpp_view", type="integer")
	 * @var text
	 */
	private $smppView;
	
	/**
	 * @column(name="status", type="integer")
	 * @var integer
	 */
	private $status;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}