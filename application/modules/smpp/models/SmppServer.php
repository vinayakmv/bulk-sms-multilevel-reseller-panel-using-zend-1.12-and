<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppServerRepository")
 * @Table(name="smpp_server")
 * @author Vinayak
 *
 */
class SmppServer
{
	/**
	* @Column(name="id", type="integer", nullable=false)
	* @Id
	* @GeneratedValue(strategy="IDENTITY")
	* @var integer
	*/ 
	private $id;

	/**
	 * @column(name="user_id", type="integer", nullable="true")
	 * @var integer
	 */
	private $user;
	
	/**
	 * @column(name="name", type="string", nullable="true")
	 * @var integer
	 */
	private $name;
	
	/**
	 * @column(name="type", type="string", nullable="true")
	 * @var unknown
	 */
	private $type;
	
	/**
	 * @column(name="ip", type="string", nullable="true")
	 * @var unknown
	 */
	private $ip;
		
	/**
	 * @column(name="port", type="string", nullable="true")
	 * @var unknown
	 */
	private $port;
	
	/**
	 * @column(name="password", type="string", nullable="true")
	 * @var unknown
	 */
	private $password;
	
	/**
	 * @column(name="kannel_conf", type="string", nullable="true")
	 * @var unknown
	 */
	private $kannelConf;	
		
	/**
	 * @Column(name="sqlbox_conf", type="string", nullable=true)
	 * @var datetime
	 */
	private $sqlboxConf;

	/**
	 * @Column(name="vsmppbox_conf", type="string", nullable=true)
	 * @var datetime
	 */
	private $vsmppboxConf;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
