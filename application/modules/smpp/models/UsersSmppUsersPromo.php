<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\UsersSmppUsersPromoRepository")
 * @Table(name="users_smpp_users_promo")
 * @author Vinayak
 *
 */
class UsersSmppUsersPromo
{
	/**
	* @Column(name="system_id", type="string", length="15", nullable=true)
        * @Id
	 * @var integer
	 */
	private $systemId;
		
	/**
	 * @Column(name="user_id", type="integer", nullable=false)
	 * @var string
	 */
	private $user;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}