<?php
namespace modules\smpp\models;
/**
 * 
 * @author Vinayak
 *
 */
class SmppSpamTransManager  extends \Smpp_Doctrine_BaseManager
{

	public function insert($getArray,$type, $em)
	{
		$id = $getArray['id'];
                
                
                if ($getArray['esme'] != 'all'){
                    $esmeArray = explode('|', $getArray['esme']);                
                    $user = trim($esmeArray['0']);
                    $routeType = trim($esmeArray['1']);
                   
                }else{
                    $user = $getArray['esme'];
                    $routeType = 'all';
                }
              
                 if ($routeType == 'TRANSACTIONAL'){
                     $table = 'modules\smpp\models\UsersSmppUsersTrans';
                     $routeTable = 'smpp_spam_trans';
                     $serverType = 'TRANS-SMPP';
                }elseif ($routeType == 'PROMOTIONAL'){
                    $table = 'modules\smpp\models\UsersSmppUsersPromo';
                    $routeTable = 'smpp_spam_promo';
                    $serverType = 'PROMO-SMPP';
                }
                
		if ($type == 'spam'){
			$prefixRepo = $em->getRepository('modules\smpp\models\SmppSpamRequest')->find($id);
			$name2=trim($prefixRepo->name);
			$senderOrPrefix = 'source_regex';			

			$str2='';
			$str3 = explode('|',$name2);
			foreach ($str3 as $key => $string){
				$arr1 = str_split($string);
				foreach($arr1 as $key=>$value){
					$upper = strtoupper($value);
					$lower = strtolower($value);
						
					$str2 = $str2.'['.$upper.$lower.']';
				}
				$str2 = $str2.'|';
			}
			$name = rtrim($str2,"|");
		}		
		
		if ($user == 'all'){
                    
			$smppUsersPromo = $em->getRepository("modules\smpp\models\UsersSmppUsersPromo")->findAll();
                        $smppUsersTrans = $em->getRepository("modules\smpp\models\UsersSmppUsersTrans")->findAll();
			foreach ($smppUsersTrans as $smppUser){
				$esme = $smppUser->systemId;
				/* $sqlFetch = "SELECT * FROM `smpp_route_trans` WHERE `system_id`='$user' AND smsc_id = '$smscId'" ;
				
				$stmtFetch = $em->getConnection()->prepare($sqlFetch);
				$stmtFetch->execute();
				$statement= $stmtFetch->fetchAll();
				if(count($statement)=='0'){ */
					$regSendrId="(".$name.")";
					$sql = "INSERT INTO `smpp_spam_trans` (`system_id`, `regex`) VALUES ('$esme','$regSendrId')" ;
					
					$stmt = $em->getConnection()->prepare($sql);
					$stmt->execute();
				/* }else{
						
					$senderId1=$statement['0']['regex'];
					$senderIdArray=explode( ')', $senderId1);
					$regSendrId=$senderIdArray['0']."|".$name.")";
					$sqlUpdate = "UPDATE `smpp_route_trans` SET $senderOrPrefix = '$regSendrId' WHERE `system_id`='$user'AND smsc_id = '$smscId'" ;
						
					$stmt = $em->getConnection()->prepare($sqlUpdate);
					$stmt->execute();
				} */
			}
                        foreach ($smppUsersPromo as $smppUser){
				$esme = $smppUser->systemId;
				/* $sqlFetch = "SELECT * FROM `smpp_route_trans` WHERE `system_id`='$user' AND smsc_id = '$smscId'" ;
				
				$stmtFetch = $em->getConnection()->prepare($sqlFetch);
				$stmtFetch->execute();
				$statement= $stmtFetch->fetchAll();
				if(count($statement)=='0'){ */
					$regSendrId="(".$name.")";
					$sql = "INSERT INTO `smpp_spam_promo` (`system_id`, `regex`) VALUES ('$esme','$regSendrId')" ;
					
					$stmt = $em->getConnection()->prepare($sql);
					$stmt->execute();
				/* }else{
						
					$senderId1=$statement['0']['regex'];
					$senderIdArray=explode( ')', $senderId1);
					$regSendrId=$senderIdArray['0']."|".$name.")";
					$sqlUpdate = "UPDATE `smpp_route_trans` SET $senderOrPrefix = '$regSendrId' WHERE `system_id`='$user'AND smsc_id = '$smscId'" ;
						
					$stmt = $em->getConnection()->prepare($sqlUpdate);
					$stmt->execute();
				} */
			}
			
			
                        $prefixRepo->status = '1';
                        $prefixRepo->smscId = "";
                        $prefixRepo->systemId = $user;
			$em->persist($prefixRepo);
			$em->flush();
			
			return $prefixRepo;
			
		}else{
			/* $sqlFetch = "SELECT * FROM `smpp_route_trans` WHERE `system_id`='$user' AND smsc_id = '$smscId'" ;
				
			$stmtFetch = $em->getConnection()->prepare($sqlFetch);
			$stmtFetch->execute();
			$statement= $stmtFetch->fetchAll();
			if(count($statement)=='0'){ */
				$regSendrId="(".$name.")";
				$sql = "INSERT INTO `$routeTable` (`system_id`, `regex`) VALUES ('$user','$regSendrId')" ;
				$stmt = $em->getConnection()->prepare($sql);
				$stmt->execute();
			/* }else{
					
				$senderId1=$statement['0']['regex'];
				$senderIdArray=explode( ')', $senderId1);
				$regSendrId=$senderIdArray['0']."|".$name.")";
				$sql = "UPDATE `smpp_route_trans` SET `source_regex`= '$regSendrId' WHERE `system_id`='$user' AND smsc_id = '$smscId'" ;
					
				$stmt = $em->getConnection()->prepare($sql);
				$stmt->execute();
			} */

			$prefixRepo->status = '1';
                        $prefixRepo->smscId = "$routeType";
			$prefixRepo->systemId = $user;
			$em->persist($prefixRepo);
			$em->flush();
			
                        
			return $prefixRepo;
		}
             
	     return false;	
		
	}
	
	public function delete($getArray, $type, $em)
	{
		$id = $getArray['id'];
		$user=$getArray['esme'];
                
                
	
		if ($type == 'spam'){
			$prefixRepo = $em->getRepository('modules\smpp\models\SmppSpamRequest')->find($id);
			$name2=trim($prefixRepo->name);
			$senderOrPrefix = 'source_regex';
			
			$str2='';
			$str3 = explode('|',$name2);
			foreach ($str3 as $key => $string){
				$arr1 = str_split($string);
				foreach($arr1 as $key=>$value){
					$upper = strtoupper($value);
					$lower = strtolower($value);
			
					$str2 = $str2.'['.$upper.$lower.']';
				}
				$str2 = $str2.'|';
			}
			$name = rtrim($str2,"|");
		}
		$name = '('.$name.')';
		if ($user == 'all'){
				
			$smppUsersTrans = $em->getRepository('modules\smpp\models\UsersSmppUsersTrans')->findAll();
			foreach ($smppUsersTrans as $smppUser){
				$esme = $smppUser->systemId;
				$sqlFetch = "DELETE FROM `smpp_spam_trans` WHERE `system_id`='$esme'" ;
	
				$stmtFetch = $em->getConnection()->prepare($sqlFetch);
				$stmt = $stmtFetch->execute();
				/* $statement= $stmtFetch->fetchAll();
				if(count($statement)!='0'){
					
					$senderId1=$statement['0']['regex'];
					$regSendrId = str_replace($name, "", $senderId1);
					$sqlUpdate = "UPDATE `smpp_route_trans` SET $senderOrPrefix = '$regSendrId' WHERE `system_id`='$user'AND smsc_id = '$smscId'" ;
					
					$stmt = $em->getConnection()->prepare($sqlUpdate);
					$stmt->execute();
					
				} */
			}
                        $smppUsersPromo = $em->getRepository('modules\smpp\models\UsersSmppUsersPromo')->findAll();
                        foreach ($smppUsersPromo as $smppUser){
				$esme = $smppUser->systemId;
				$sqlFetch = "DELETE FROM `smpp_spam_promo` WHERE `system_id`='$esme'" ;
	
				$stmtFetch = $em->getConnection()->prepare($sqlFetch);
				$stmt = $stmtFetch->execute();
				/* $statement= $stmtFetch->fetchAll();
				if(count($statement)!='0'){
					
					$senderId1=$statement['0']['regex'];
					$regSendrId = str_replace($name, "", $senderId1);
					$sqlUpdate = "UPDATE `smpp_route_trans` SET $senderOrPrefix = '$regSendrId' WHERE `system_id`='$user'AND smsc_id = '$smscId'" ;
					
					$stmt = $em->getConnection()->prepare($sqlUpdate);
					$stmt->execute();
					
				} */
			}
			$prefixRepo->status = '0';
                        $prefixRepo->smscId = "";
			$prefixRepo->systemId = "";
			$em->persist($prefixRepo);
			$em->flush();
				
			return $prefixRepo;
				
		}else{
                    
                    $esmeArray = explode('|', $getArray['esme']);                
                    $user = trim($esmeArray['0']);
                    $routeType = trim($esmeArray['1']);
                    
                    
                    if ($routeType == 'TRANSACTIONAL'){
                        $table = 'modules\smpp\models\UsersSmppUsersTrans';
                        $routeTable = 'smpp_spam_trans';
                        $serverType = 'TRANS-SMPP';
                    }elseif ($routeType == 'PROMOTIONAL'){
                        $table = 'modules\smpp\models\UsersSmppUsersPromo';
                        $routeTable = 'smpp_spam_promo';
                        $serverType = 'PROMO-SMPP';
                    }
                
                    
				$sqlFetch = "DELETE FROM `$routeTable` WHERE `system_id`='$user'" ;
	
				$stmtFetch = $em->getConnection()->prepare($sqlFetch);
				$stmt = $stmtFetch->execute();
				
				/*$statement= $stmtFetch->fetchAll();
				 if(count($statement)!='0'){
					
					$senderId1=$statement['0']['regex'];
					$regSendrId = str_replace($name, "", $senderId1);
					$sqlUpdate = "UPDATE `smpp_route_trans` SET $senderOrPrefix = '$regSendrId' WHERE `system_id`='$user'AND smsc_id = '$smscId'" ;
					
					$stmt = $em->getConnection()->prepare($sqlUpdate);
					$stmt->execute();
					
				} */
	
			$prefixRepo->status = '0';
			$prefixRepo->smscId = "";
			$prefixRepo->systemId = "";
			$em->persist($prefixRepo);
			$em->flush();
				
			return $prefixRepo;
		}
	}
	
}
