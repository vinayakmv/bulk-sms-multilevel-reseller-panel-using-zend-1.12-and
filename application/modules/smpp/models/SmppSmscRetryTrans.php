<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppSmscRetryTransRepository")
 * @Table(name="smpp_smsc_retry_trans")
 * @author Vinayak
 *
 */
class SmppSmscRetryTrans
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @column(name="smsc_id", type="string", nullable="false")
	 * @var integer
	 */
	private $smscId;
        
         /**
	 * @column(name="smsc_id_retry", type="string", nullable="true")
	 * @var integer
	 */
	private $smscIdRetry;

	/**
	 * @column(name="service", type="string", nullable="true")
	 * @var integer
	 */
	private $service;
        
        /**
	 * @column(name="status", type="integer", nullable="true")
	 * @var integer
	 */
	private $status;
        	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
	function toArray ()
    {
      return get_object_vars($this);
   	}
	
}
