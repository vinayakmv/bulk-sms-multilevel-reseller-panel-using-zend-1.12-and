<?php
namespace modules\smpp\models;
/**
 * 
 * @author Vinayak
 *
 */
class SmppSenderIdTransManager  extends \Smpp_Doctrine_BaseManager
{
	public function insertSenderId($getArray, $em)
	{
		$user=$getArray['smppUser'];
		$senderId=trim($getArray['regex']);
		$cdate=date("Y-m-d H:i:s");
		$sqlFetch = "SELECT * FROM `smpp_sender_trans` WHERE `system_id`='$user'" ;
		
		$stmtFetch = $em->getConnection()->prepare($sqlFetch);
		$stmtFetch->execute();
		$statement= $stmtFetch->fetchAll();
		if(count($statement)=='0'){
			$regSendrId="(".$senderId.")";
			$sql = "INSERT INTO `smpp_sender_trans` (`sender_id`,`system_id`, `regex`) VALUES (NULL,'$user', '$regSendrId')" ;
			
			$stmt = $em->getConnection()->prepare($sql);
			$stmt->execute();
			return $stmt;
		}
		else{
			
			$senderId1=$statement['0']['regex'];
			$senderIdArray=explode( ')', $senderId1);
			$regSendrId=$senderIdArray['0']."|".$senderId.")";
			$sql = "UPDATE `smpp_sender_trans` SET `regex`= '$regSendrId' WHERE `system_id`='$user'" ;
				
			$stmt = $em->getConnection()->prepare($sql);
			$stmt->execute();
			return $stmt;	
		}
	
	  }
}
