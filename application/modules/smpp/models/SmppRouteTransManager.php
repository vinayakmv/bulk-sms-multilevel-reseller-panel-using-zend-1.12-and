<?php
namespace modules\smpp\models;
/**
 * 
 * @author Vinayak
 *
 */
class SmppRouteTransManager  extends \Smpp_Doctrine_BaseManager
{

	public function insert($getArray,$type, $em)
	{
		$id = $getArray['id'];
                
                $routeArray = explode('|', $getArray['route']);
		$smscId = trim($routeArray['0']);
		$routeType = trim($routeArray['1']);
                
                if ($getArray['esme'] != 'all'){
                    $esmeArray = explode('|', $getArray['esme']);                
                    $user = trim($esmeArray['0']);
                    $userType = trim($esmeArray['1']);
                }else{
                    $user = $getArray['esme'];
                    $userType = $routeType;
                }
                
                
              
                
            if ($routeType == $userType){
                 if ($routeType == 'TRANSACTIONAL'){
                     $table = 'modules\smpp\models\UsersSmppUsersTrans';
                     $routeTable = 'smpp_route_trans';
                     $serverType = 'TRANS-SMPP';
                }elseif ($routeType == 'PROMOTIONAL'){
                    $table = 'modules\smpp\models\UsersSmppUsersPromo';
                    $routeTable = 'smpp_route_promo';
                    $serverType = 'PROMO-SMPP';
                }
                
		if ($type == 'prefix'){
			$prefixRepo = $em->getRepository('modules\smpp\models\SmppPrefixRouteRequest')->find($id);
			$name=trim($prefixRepo->name);
			$senderOrPrefix = 'regex';
		}elseif ($type == 'sender'){
			$prefixRepo = $em->getRepository('modules\smpp\models\SmppSenderIdRouteRequest')->find($id);
			$name2=trim($prefixRepo->name);
			$senderOrPrefix = 'source_regex';			

			$str2='';
			$str3 = explode('|',$name2);
			foreach ($str3 as $key => $string){
				$arr1 = str_split($string);
				foreach($arr1 as $key=>$value){
					$upper = strtoupper($value);
					$lower = strtolower($value);
						
					$str2 = $str2.'['.$upper.$lower.']';
				}
				$str2 = $str2.'|';
			}
			$name = rtrim($str2,"|");
		}		
		
		if ($user == 'all'){
                    
			$smppUsersTrans = $em->getRepository("$table")->findAll();
			foreach ($smppUsersTrans as $smppUser){
				$esme = $smppUser->systemId;
				/* $sqlFetch = "SELECT * FROM `smpp_route_trans` WHERE `system_id`='$user' AND smsc_id = '$smscId'" ;
				
				$stmtFetch = $em->getConnection()->prepare($sqlFetch);
				$stmtFetch->execute();
				$statement= $stmtFetch->fetchAll();
				if(count($statement)=='0'){ */
					$regSendrId="^(".$name.")";
					$sql = "INSERT INTO `$routeTable` (`direction`, $senderOrPrefix, `priority`, `cost`, `system_id`, `smsc_id`) VALUES ('1','$regSendrId','0','1','$esme','$smscId')" ;
					
					$stmt = $em->getConnection()->prepare($sql);
					$stmt->execute();
				/* }else{
						
					$senderId1=$statement['0']['regex'];
					$senderIdArray=explode( ')', $senderId1);
					$regSendrId=$senderIdArray['0']."|".$name.")";
					$sqlUpdate = "UPDATE `smpp_route_trans` SET $senderOrPrefix = '$regSendrId' WHERE `system_id`='$user'AND smsc_id = '$smscId'" ;
						
					$stmt = $em->getConnection()->prepare($sqlUpdate);
					$stmt->execute();
				} */
			}
			$prefixRepo->status = '1';
			$prefixRepo->smscId = $smscId;
			$prefixRepo->systemId = $user;
			$em->persist($prefixRepo);
			$em->flush();
			
                        
                        $serverRepo = $em->getRepository('modules\smpp\models\SmppServer')->findOneBy(array('type'=> $serverType));
                        
                        $ip = $serverRepo->ip;
                        $port = $serverRepo->port;
                        $password = $serverRepo->password;
                        
                        file_get_contents("http://$ip:$port/rebuild-routes?password=$password");
			
                        return $prefixRepo;
			
		}else{
			/* $sqlFetch = "SELECT * FROM `smpp_route_trans` WHERE `system_id`='$user' AND smsc_id = '$smscId'" ;
				
			$stmtFetch = $em->getConnection()->prepare($sqlFetch);
			$stmtFetch->execute();
			$statement= $stmtFetch->fetchAll();
			if(count($statement)=='0'){ */
				$regSendrId="^(".$name.")";
				$sql = "INSERT INTO `$routeTable` (`direction`, $senderOrPrefix, `priority`, `cost`, `system_id`, `smsc_id`) VALUES ('1','$regSendrId','0','1','$user','$smscId')" ;	
				$stmt = $em->getConnection()->prepare($sql);
				$stmt->execute();
			/* }else{
					
				$senderId1=$statement['0']['regex'];
				$senderIdArray=explode( ')', $senderId1);
				$regSendrId=$senderIdArray['0']."|".$name.")";
				$sql = "UPDATE `smpp_route_trans` SET `source_regex`= '$regSendrId' WHERE `system_id`='$user' AND smsc_id = '$smscId'" ;
					
				$stmt = $em->getConnection()->prepare($sql);
				$stmt->execute();
			} */

			$prefixRepo->status = '1';
			$prefixRepo->smscId = $smscId;
			$prefixRepo->systemId = $user;
			$em->persist($prefixRepo);
			$em->flush();
			
                        $serverRepo = $em->getRepository('modules\smpp\models\SmppServer')->findBy(array('type'=> $serverType));
                        $ip = $serverRepo->ip;
                        $port = $serverRepo->port;
                        $password = $serverRepo->password;
                        
                        file_get_contents("http://$ip:$port/rebuild-routes?password=$password");
			
                        return $prefixRepo;
		}
             }     
                
	     return false;	
		
	}
	
	public function delete($getArray, $type, $em)
	{
		$id = $getArray['id'];
		$user=$getArray['esme'];
		$smscId=trim($getArray['route']);
	
		if ($type == 'prefix'){
			$prefixRepo = $em->getRepository('modules\smpp\models\SmppPrefixRouteRequest')->find($id);
			$name=trim($prefixRepo->name);
			$senderOrPrefix = 'regex';
		}elseif ($type == 'sender'){
			$prefixRepo = $em->getRepository('modules\smpp\models\SmppSenderIdRouteRequest')->find($id);
			$name2=trim($prefixRepo->name);
			$senderOrPrefix = 'source_regex';
			
			$str2='';
			$str3 = explode('|',$name2);
			foreach ($str3 as $key => $string){
				$arr1 = str_split($string);
				foreach($arr1 as $key=>$value){
					$upper = strtoupper($value);
					$lower = strtolower($value);
			
					$str2 = $str2.'['.$upper.$lower.']';
				}
				$str2 = $str2.'|';
			}
			$name = rtrim($str2,"|");
		}
		$name = '^('.$name.')';
		if ($user == 'all'){
				
			$smppUsersTrans = $em->getRepository('modules\smpp\models\UsersSmppUsersTrans')->findAll();
			foreach ($smppUsersTrans as $smppUser){
				$esme = $smppUser->systemId;
				$sqlFetch = "DELETE FROM `smpp_route_trans` WHERE `system_id`='$esme' AND smsc_id = '$smscId' AND $senderOrPrefix = '$name'" ;
	
				$stmtFetch = $em->getConnection()->prepare($sqlFetch);
				$stmt = $stmtFetch->execute();
				/* $statement= $stmtFetch->fetchAll();
				if(count($statement)!='0'){
					
					$senderId1=$statement['0']['regex'];
					$regSendrId = str_replace($name, "", $senderId1);
					$sqlUpdate = "UPDATE `smpp_route_trans` SET $senderOrPrefix = '$regSendrId' WHERE `system_id`='$user'AND smsc_id = '$smscId'" ;
					
					$stmt = $em->getConnection()->prepare($sqlUpdate);
					$stmt->execute();
					
				} */
			}
			$prefixRepo->status = '0';
			$prefixRepo->smscId = "";
			$prefixRepo->systemId = "";
			$em->persist($prefixRepo);
			$em->flush();
				
			return $prefixRepo;
				
		}else{
				$sqlFetch = "DELETE FROM `smpp_route_trans` WHERE `system_id`='$user' AND smsc_id = '$smscId' AND $senderOrPrefix = '$name'" ;
	
				$stmtFetch = $em->getConnection()->prepare($sqlFetch);
				$stmt = $stmtFetch->execute();
				
				/*$statement= $stmtFetch->fetchAll();
				 if(count($statement)!='0'){
					
					$senderId1=$statement['0']['regex'];
					$regSendrId = str_replace($name, "", $senderId1);
					$sqlUpdate = "UPDATE `smpp_route_trans` SET $senderOrPrefix = '$regSendrId' WHERE `system_id`='$user'AND smsc_id = '$smscId'" ;
					
					$stmt = $em->getConnection()->prepare($sqlUpdate);
					$stmt->execute();
					
				} */
	
			$prefixRepo->status = '0';
			$prefixRepo->smscId = "";
			$prefixRepo->systemId = "";
			$em->persist($prefixRepo);
			$em->flush();
				
			return $prefixRepo;
		}
	}
	
}
