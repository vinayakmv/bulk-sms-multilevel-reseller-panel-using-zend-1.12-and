<?php
namespace modules\smpp\models;

class SmppErrorCodeRequestManager extends \Smpp_Doctrine_BaseManager
{
	public function createCode($getValues, $userId, $em)
	{
		if (isset($em)){
			$name = str_replace(' ', ",", $getValues['opcoerrorcode']);			
			$name = explode(",", $name);
			$prefix="";
			
			foreach ($name as $key => $value){
				$prefix .= $value.'|';
                                	
			}	
			$sender = new SmppErrorCodeRequest();
			$sender->user = $userId;
                        $sender->errorCode = $getValues['errorcode'];
			$sender->opcoErrorCodes = rtrim($prefix,"|");
			$sender->description = $getValues['description'];
			$sender->status = '0';
			$sender->createdDate = new \DateTime('now');
			$sender->updatedDate = new \DateTime('now');
			$em->persist($sender);
			$em->flush();
				
			return $sender;
			
		}
		return false;
	}
	public function managePrefix($getValues, $status, $userId, $em)
	{
		
		if (isset($getValues)){
			$id=$getValues['uniqId'];    		
			$senderid = $em->getRepository('modules\smpp\models\SmppErrorCodeRequest')->find($id);
			if($status=='approved'){
				$senderid->status = '1';
				$senderid->updatedDate = new \DateTime('now');
				$getValues['smscId']= $senderid->smscId;
				$getValues['opco']= $senderid->opcoErrorCodes;
				
				$smppErrorMapManager = new SmppErrorMapManager();
				$insertError= $smppErrorMapManager->insertCodes($getValues, $type, $em);

			}
			else if($status=='Rejected'){
				
				$senderid->status = '0';
				$senderid->updatedDate = new \DateTime('now');
			}
			
			
			$em->persist($senderid);
			$em->flush();
	
			return $senderid;
		}
		return false;
	}
}