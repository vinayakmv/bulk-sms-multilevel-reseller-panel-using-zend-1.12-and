<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppSpamTransRepository")
 * @Table(name="smpp_spam_trans",indexes={
 * @Index(name="search_sendx", columns={"system_id"})
 * }))
 * @author Vinayak
 *
 */
class SmppSpamTrans
{
	/**
	 * @Column(name="regex", type="text",length="65500", nullable=true)
	 * @var string
	 */
	private $regex;

	/**
	 * @Column(name="system_id", type="string")
         * @Id
	 * @var string
	 */
	private $systemId;
		
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
