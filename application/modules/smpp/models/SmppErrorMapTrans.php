<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppErrorMapTransRepository")
 * @Table(name="error_map_trans")
 * @author Vinayak
 *
 */
class SmppErrorMapTrans
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @Column(name="opco_error_code", type="string")
	 * @var text
	 */
	private $opcoErrorCode;
	
	/**
	 * @Column(name="error_code_map", type="string")
	 * @var text
	 */
	private $errorCodeMap;
	
        /**
	 * @Column(name="smsc_id", type="string")
	 * @var text
	 */
	private $smscId;	
	
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}