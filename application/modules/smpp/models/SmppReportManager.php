<?php
namespace modules\smpp\models;

class SmppReportManager extends \Smpp_Doctrine_BaseManager
{
	public function createReport($userId, $esme, $type, $from, $to, $em)
	{
		if (isset($em)){
			$report = new SmppReport();
			$report->user = $userId;
			$report->esme = $esme;
			$report->type = $type;
			$report->status = '0';
			$report->fromDate = new \DateTime($from);;
			$report->toDate = new \DateTime($to);;
			$report->createdDate = new \DateTime('now');
			$report->updatedDate = new \DateTime('now');
			$em->persist($report);
			$em->flush();
				
			return $report;
		}
		return false;
	}

}
