<?php
namespace modules\smpp\models;

class SmppRecreditManager extends \Smpp_Doctrine_BaseManager
{
	public function createActivityLog($userRepo, $esme, $recreditDate, $type, $status, $em)
	{
		if (isset($em)){
			$recredit = new SmppRecredit();
			$recredit->user = $userRepo;
			$recredit->esme = $esme;
			$recredit->type = $type;
			$recredit->status = $status;
			$recredit->recreditDate = $recreditDate;
			$recredit->createdDate = new \DateTime('now');
			$recredit->updatedDate = new \DateTime('now');
			$em->persist($recredit);
			$em->flush();
				
			return $recredit;
		}
		return false;
	}

}
