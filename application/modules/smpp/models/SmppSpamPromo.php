<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppSpamPromoRepository")
 * @Table(name="smpp_spam_promo",indexes={
 * @Index(name="search_sendx", columns={"system_id"})
 * }))
 * @author Vinayak
 *
 */
class SmppSpamPromo
{
	/**
	 * @Column(name="regex", type="text",length="65500", nullable=true)
	 * @var string
	 */
	private $regex;

	/**
	 * @Column(name="system_id", type="string")
         * @Id
	 * @var string
	 */
	private $systemId;
		
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
