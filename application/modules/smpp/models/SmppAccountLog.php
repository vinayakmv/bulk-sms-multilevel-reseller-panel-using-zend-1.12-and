<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppAccountLogRepository")
 * @Table(name="log_accounts_smpp")
 * @author Vinayak
 *
 */
class SmppAccountLog
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @column(name="user_id", type="integer", nullable="false")
	 * @var integer
	 */
	private $user;

	/**
	 * @column(name="client_id", type="string", nullable="false") 
	 * @var unknown
	 */
	private $client;
	
	/**
	 * @column(name="user_balance_before", type="integer", nullable="false") 
	 * @var unknown
	 */
	private $userBalanceBefore;
	
	/**
	 * @column(name="client_balance_before", type="integer", nullable="false") 
	 * @var unknown
	 */
	private $clientBalanceBefore;

	
	/**
	 * @column(name="user_balance_after", type="integer", nullable="false") 
	 * @var unknown
	 */
	private $userBalanceAfter;
	
	/**
	 * @column(name="client_balance_after", type="integer", nullable="false") 
	 * @var unknown
	 */
	private $clientBalanceAfter;
	
	/**
	 * @column(name="amount", type="integer", nullable="false") 
	 * @var unknown
	 */
	private $amount;
	
	/**
	 * @column(name="total", type="string", nullable="true") 
	 * @var unknown
	 */
	private $total;
	
	/**
	 * @column(name="tax", type="string", nullable="true") 
	 * @var unknown
	 */
	private $tax;
	
	/**
	 * @column(name="rate", type="string", nullable="true")
	 * @var unknown
	 */
	private $rate;
	
	/**
	 * @column(name="mode", type="string", nullable="false") 
	 * @var unknown
	 */
	private $mode;
	
	/**
	 * @column(name="type", type="string", nullable="false")
	 * @var integer
	 */
	private $type;
		
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
