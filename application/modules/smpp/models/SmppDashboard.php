<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppDashboardRepository")
 * @Table(name="smpp_dashboard")
 * @author Vinayak
 *
 */
class SmppDashboard
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
         * @Id
         * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
	
	/**
	 * @column(name="user_id", type="integer", nullable="false")
	 * @var integer
	 */
	private $user;
	
	/**
	 * @column(name="domain_id", type="integer", nullable="true")
	 * @var integer
	 */
	private $domain;
        
        /**
	 * @column(name="parent_domain_id", type="integer", nullable="true")
	 * @var integer
	 */
	private $parentDomain;
	
	/**
	 * @column(name="reseller_id", type="integer", nullable="true")
	 * @var integer
	 */
	private $reseller;

	/**
	 * @column(name="system_id", type="integer", nullable="false")
	 * @var integer
	 */
	private $systemId;
	
	/**
	 * @column(name="sent_smpp_trans", type="integer", nullable="false")
	 * @var integer
	 */
	private $sentSmppTrans;
	
	/**
	 * @column(name="delivered_smpp_trans", type="integer", nullable="false")
	 * @var integer
	 */
	private $deliveredSmppTrans;

	/**
	 * @column(name="failed_smpp_trans", type="integer", nullable="false")
	 * @var integer
	 */
	private $failedSmppTrans;

	/**
	 * @column(name="sent_smpp_promo", type="integer", nullable="false")
	 * @var integer
	 */
	private $sentSmppPromo;
	
	/**
	 * @column(name="delivered_smpp_promo", type="integer", nullable="false")
	 * @var integer
	 */
	private $deliveredSmppPromo;

	/**
	 * @column(name="failed_smpp_promo", type="integer", nullable="false")
	 * @var integer
	 */
	private $failedSmppPromo;

	/**
	 * @column(name="sent_smpp_scrub", type="integer", nullable="false")
	 * @var integer
	 */
	private $sentSmppScrub;
	
	/**
	 * @column(name="delivered_smpp_scrub", type="integer", nullable="false")
	 * @var integer
	 */
	private $deliveredSmppScrub;

	/**
	 * @column(name="failed_smpp_scrub", type="integer", nullable="false")
	 * @var integer
	 */
	private $failedSmppScrub;

	/**
	 * @column(name="sent_smpp_inter", type="integer", nullable="false")
	 * @var integer
	 */
	private $sentSmppInter;
	
	/**
	 * @column(name="delivered_smpp_inter", type="integer", nullable="false")
	 * @var integer
	 */
	private $deliveredSmppInter;

	/**
	 * @column(name="failed_smpp_inter", type="integer", nullable="false")
	 * @var integer
	 */
	private $failedSmppInter;

	/**
	 * @Column(name="report_date", type="date", nullable=true)
	 * @var datetime
	 */
	private $reportDate;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
	function toArray ()
   {
      return get_object_vars($this);
   }
	
}
