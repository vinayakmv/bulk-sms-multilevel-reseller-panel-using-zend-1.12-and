<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppSenderReportRepository")
 * @Table(name="smpp_sender_report")
 * @author Vinayak
 *
 */
class SmppSenderReport
{
	/**
	 * @Column(name="id", type="bigint", nullable=true)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var bigint
	 */
	private $id;
		
	/**
	 * @Column(name="system_id", type="string", nullable=true)
	 * @var string
	 */
	private $systemId;
	
	/**
	 * @Column(name="user_id", type="string", nullable=true)
	 * @var string
	 */
	private $userId;
		
	/**
	 * @Column(name="sender_id", type="string", nullable=true)
	 * @var string
	 */
	private $senderId;
	
	/**
	 * @Column(name="send_count", type="integer", nullable=true)
	 * @var string
	 */
	private $sendCount;
	
	/**
	 * @Column(name="delivered_count", type="integer", nullable=true)
	 * @var string
	 */
	private $deliveredCount;
	
	/**
	 * @column(name="create_date", type="datetime", nullable="false")
	 * @var datetime
	 */
	private $createDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */	
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}