<?php
namespace modules\smpp\models;
use Doctrine\ORM\EntityRepository;

class SmsDashboardRepository extends \Smpp_Doctrine_EntityRepository
{

	public function getUser($userId, $searchKey) 
	{
		//$dql = "SELECT A FROM modules\user\models\Users A WHERE A.reseller = $userId AND A.user LIKE :keyword";
		//$query = $this->getEntityManager()->createQuery($dql);
		//$query->setParameter("keyword", '%'.$searchKey.'%');
		//return $query->getResult();
		$queryBuilder = $this->getEntityManager()->createQueryBuilder()
							 ->select('u.username, d.domain')
							 ->from('modules\user\models\User', 'u')
							 ->innerJoin('u.domain', 'd')
							 ->where('u.username LIKE ?1 AND u.reseller = ?2 AND u.id != ?2')
							 ->setParameter("1", '%'.$searchKey.'%')
							 ->setParameter("2", "$userId");
		
		return $queryBuilder->getQuery()->getResult();
	}
	
	public function getReseller($userId, $searchKey)
	{
		//$dql = "SELECT A FROM modules\user\models\Users A WHERE A.reseller = $userId AND A.user LIKE :keyword";
		//$query = $this->getEntityManager()->createQuery($dql);
		//$query->setParameter("keyword", '%'.$searchKey.'%');
		//return $query->getResult();
		
		
		$roleLRepo = $emUser->getRepository("modules\user\models\Role")->findOneBy(array('role' => 'L1-RESELLER'));
		 
		$rolePRepo = $emUser->getRepository("modules\user\models\Role")->findOneBy(array('role' => 'P1-RESELLER'));
		 
		$roleARepo = $emUser->getRepository("modules\user\models\Role")->findOneBy(array('role' => 'L1-ADMIN'));
		
		if (isset($rolePRepo,$roleLRepo,$roleARepo)){
			$rolePId = $rolePRepo->id;
			$roleLId = $roleLRepo->id;
			$roleAId = $roleARepo->id;
		
			$queryBuilder = $emUser->createQueryBuilder()
			->select('u.username, d.domain')
			->from('modules\user\models\User', 'u')
			->innerJoin('u.domain', 'd')
			->where('u.reseller = ?2 AND u.id != ?2 AND (u.role = ?3 OR u.role = ?4 OR u.role = ?5)')
			->setParameter("2", "$resellerId")
			->setParameter("3", "$roleLId")
			->setParameter("4", "$rolePId")
			->setParameter("5", "$roleAId");
			
			$queryBuilder = $this->getEntityManager()->createQueryBuilder()
			->select('u.username, d.domain')
			->from('modules\user\models\User', 'u')
			->innerJoin('u.domain', 'd')
			->where('u.username LIKE ?1 AND u.reseller = ?2 AND u.id != ?2 AND (u.role = ?3 OR u.role = ?4 OR u.role = ?5)')
			->setParameter("1", '%'.$searchKey.'%')
			->setParameter("2", "$userId")
			->setParameter("3", "$roleLId")
			->setParameter("4", "$rolePId")
			->setParameter("5", "$roleAId");
		}		
	
		return $queryBuilder->getQuery()->getResult();
	}
	
	public function getBalance($userId)
	{
		$sql="SELECT roles.role, accounts.voice_trans_credit, accounts.voice_promo_credit, accounts.voice_promo_credit_api, accounts.voice_trans_credit_api, accounts.smpp_scrub_credit, accounts.smpp_trans_credit, accounts.smpp_promo_credit, accounts.smpp_promo_credit_api, accounts.smpp_trans_credit_api, accounts.smpp_scrub_credit_api,
		domains.voice_trans_credit AS voice_trans_credit_main ,domains.voice_promo_credit AS voice_promo_credit_main, domains.voice_promo_credit_api AS voice_promo_credit_api_main ,domains.voice_trans_credit_api AS voice_trans_credit_api_main, 
		domains.smpp_scrub_credit AS smpp_scrub_credit_main ,domains.smpp_trans_credit AS smpp_trans_credit_main ,domains.smpp_promo_credit AS smpp_promo_credit_main, domains.smpp_promo_credit_api AS smpp_promo_credit_api_main ,domains.smpp_trans_credit_api AS smpp_trans_credit_api_main, domains.smpp_scrub_credit_api AS smpp_scrub_credit_api_main 
		FROM accounts INNER JOIN users ON accounts.id = users.account_id INNER JOIN roles ON users.role_id=roles.id INNER JOIN domains ON users.domain_id=domains.id WHERE users.id = '$userId'";
		$stmt = $this ->getEntityManager()->getConnection()->prepare($sql);
		$stmt->execute();
		
		return $stmt->fetchAll();
	}
	
}
