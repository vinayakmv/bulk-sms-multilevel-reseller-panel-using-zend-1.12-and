<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppSmsLogMoPromoRepository")
 * @Table(name="smpp_log_mo_promo")
 * @author Vinayak
 *
 */
class SmppSmsLogMoPromo
{
		/**
	 * @Column(name="global_id", type="bigint", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")	 
	 * @var integer
	 */
	private $globalId;

	/**
	 * @Column(name="momt", type="string", columnDefinition="ENUM('MO','MT','DLR')")
	 * @var unknown
	 */
	private $momt;
	
	/**
	 * @Column(name="sender", type="string", nullable=true)
	 * @var unknown
	 */
	private $sender;
	
	/**
	 * @Column(name="receiver", type="string", nullable=true)
	 * @var unknown
	 */
	private $receiver;
	
	/**
	 * @Column(name="udhdata", type="blob", nullable=true)
	 * @var unknown
	 */
	private $udhdata;
	
	/**
	 * @Column(name="msgdata", type="text", nullable=true)
	 * @var unknown
	 */
	private $msgdata;
	
	/**
	 * @Column(name="time", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $time;
	
	/**
	 * @Column(name="smsc_id", type="string", nullable=true)
	 * @var unknown
	 */
	private $smscId;
	
	/**
	 * @Column(name="service", type="string", nullable=true)
	 * @var unknown
	 */
	private $service;
	
	/**
	 * @Column(name="account", type="string", nullable=true)
	 * @var unknown
	 */
	private $account;
	
	/**
	 * @Column(name="id", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $id;
	
	/**
	 * @Column(name="sms_type", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $smsType;
	
	/**
	 * @Column(name="mclass", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $mclass;
	
	/**
	 * @Column(name="mwi", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $mwi;
	
	/**
	 * @Column(name="coding", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $coding;
	
	/**
	 * @Column(name="compress", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $compress;
	
	/**
	 * @Column(name="validity", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $validity;
	
	/**
	 * @Column(name="deferred", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $deferred;
	
	/**
	 * @Column(name="dlr_mask", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $dlrMask;
	
	/**
	 * @Column(name="dlr_url", type="string", nullable=true)
	 * @var unknown
	 */
	private $dlrUrl;
	
	/**
	 * @Column(name="pid", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $pid;
	
	/**
	 * @Column(name="alt_dcs", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $altDcs;
	
	/**
	 * @Column(name="rpi", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $rpi;
	
	/**
	 * @Column(name="charset", type="string", nullable=true)
	 * @var unknown
	 */
	private $charset;
	
	/**
	 * @Column(name="boxc_id", type="string", nullable=true)
	 * @var unknown
	 */
	private $boxcId;
	
	/**
	 * @Column(name="binfo", type="string", nullable=true)
	 * @var unknown
	 */
	private $binfo;
	
	/**
	 * @Column(name="meta_data", type="text", nullable=true)
	 * @var unknown
	 */
	private $metaData;

	/**
	 * @Column(name="priority", type="bigint", nullable=true)
	 * @var unknown
	 */
	private $priority;

	/**
	 * @Column(name="foreign_id", type="string", nullable=true)
	 * @var unknown
	 */
	private $foreignId;

	/**
	 * @Column(name="dlrdata", type="text", nullable=true)
	 * @var unknown
	 */
	private $dlrdata;
	
	/**
	 * @Column(name="status", type="integer", nullable=true)
	 * @var unknown
	 */
	private $status;

	/**
	 * @Column(name="cost", type="integer", nullable=true)
	 * @var unknown
	 */
	private $cost;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}