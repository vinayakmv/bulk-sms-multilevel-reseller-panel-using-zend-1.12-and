<?php
namespace modules\smpp\models;
/**
 * 
 * @author Ani
 *
 */
class SmppSenderIdRequestManager  extends \Smpp_Doctrine_BaseManager
{

	public function createSenderId($getValues, $userId, $em)
	{
		if (isset($em)){
			$name=$getValues['senderId'];
			$systemId=$getValues['user'];
			
			$remark=$getValues['userremark'];
			$status='0';
			
			$fname=str_replace('%0D%0A', '%2C', urlencode($name));
			$name1=explode(',',urldecode($fname));
			$flag1=0;//SUCCESS
			$flag2=0;//senderId length not a SIX
			$flag3=0;//senderId not a ALPHABET
			if(count($name1)<=1000){
				foreach ($name1 as $name) {
					$name=trim($name);
					if(strlen($name)==6){
						if(ctype_alpha($name)){
							$flag1++;
							$sender = new SmppSenderIdRequest();
							$sender->user = $userId;
							$sender->systemId = $systemId;
							$sender->name = $name;
							$sender->status = $status;
							$sender->remark = $remark;
							$sender->createdDate = new \DateTime('now');
							$sender->updatedDate = new \DateTime('now');
							$em->persist($sender);
							$em->flush();
				//exit();
						}
						else{
							$flag3++;
						}
					}
					else{
						$flag2++;
					}
				}
				$errorflag=$flag2+$flag3;
				$message = array('Success' => array('isCreated' => $flag1.' Sender id created successfully', 'isSkipped' =>$errorflag.'Entrys are skipped', 'isNotSix' =>$flag2.' Entrys are not meet string length', 'isNotAlphabet' =>$flag3.' Entrys are not a alphabet'));
				return $message;
			}
			else{
				$message = array('ErrorCreated' => array('countExceed' => 'Only 1000 entrys are allowed in one shot !!'));
				return $message;
			}
		}
		return false;
	}
	public function manageSenderId($getValues,$status, $userId, $em)
	{
		
		if (isset($getValues)){
			$id=$getValues['uniqId'];
    		$remark=$getValues['remark'];
    		
			$senderid = $em->getRepository('modules\smpp\models\SmppSenderIdRequest')->find($id);
			if($status=='approved'){
				$senderid->status = '1';
				$senderid->remark = $remark;
				$senderid->updatedDate = new \DateTime('now');
				$getValues['smppUser']= $senderid->systemId;
				$getValues['regex']= $senderid->name;
				//exit();
				/*$sender = new SmppSenderId();
				$sender->systemId = $smppUser;
				$sender->regex = $regex;
				$em->persist($sender);
				$em->flush();*/
				$smppSenderIdTransManager = new SmppSenderIdTransManager();
				$insertSenderId = $smppSenderIdTransManager->insertSenderId($getValues, $em);

			}
			else if($status=='Rejected'){
				
				$senderid->status = '4';
				$senderid->remark = $remark;
				$senderid->updatedDate = new \DateTime('now');
			}
			
			
			$em->persist($senderid);
			$em->flush();
	
			return $senderid;
		}
		return false;
	}
	
}
