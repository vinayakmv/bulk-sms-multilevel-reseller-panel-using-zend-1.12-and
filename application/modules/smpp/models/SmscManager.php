<?php 
namespace modules\smpp\models;

use modules\smpp\models\Smsc;
class SmscManager extends \Smpp_Doctrine_BaseManager
{
public function createSmsc($smscValues, $type, $em)
	{
		if (isset($em)) {
			
			$smsc = new Smsc();
			$smsc->systemId = $smscValues['username'];
			$smsc->password = $smscValues['password'];
			$smsc->throughput = $smscValues['tps'];
			$smsc->maxBinds = $smscValues['bind'];
                        $smsc->maxTx = $smscValues['Tx'];
                        $smsc->maxRx = $smscValues['Rx'];
			$smsc->server = $smscValues['ip'];
			$smsc->port = $smscValues['port'];
			$smsc->route = $smscValues['route'];
			$smsc->type  = $type;
			$smsc->status = '1';
			$smsc->createdDate = new \DateTime('now');
			$smsc->updatedDate = new \DateTime('now');
			
			if ($type == 'TRANSACTIONAL' || $type == 'TRANS-SCRUB'){
				$path = 'trans';
			}elseif ($type == 'PROMOTIONAL') {
				$path = 'promo';
			}elseif ($type == 'INTERNATIONAL'){
				$path = 'inter';
			}
			if ($smsc->maxBinds > '0'){
                            for ($i = 1; $i <= $smsc->maxBinds; $i++) {
			
				$fp = fopen("/usr/local/sbin/$path/smsc.d/$smsc->route-TRx-$i".'.conf', "w");
				fwrite($fp,"\ngroup = smsc\n");
				fwrite($fp,"smsc=smpp\n");
				//fwrite($fp,"alt-charset=ASCII\n");
				fwrite($fp,"smsc-id=$smsc->route\n");
				fwrite($fp,"host=$smsc->server\n");
				fwrite($fp,"port=$smsc->port\n");
				fwrite($fp,"reconnect-delay = 30\n");
				fwrite($fp,"smsc-username=$smsc->systemId\n");
				fwrite($fp,"smsc-password=$smsc->password\n");
				fwrite($fp,"source-addr-ton = 0\n");
				fwrite($fp,"source-addr-npi = 1\n");
				fwrite($fp,"transceiver-mode=1\n");
				fwrite($fp,"system-type=smpp\n");
				fwrite($fp,"preferred-smsc-id=$smsc->route\n");
				fwrite($fp,"throughput=$smsc->throughput\n");
				fwrite($fp,"wait-ack = 10\n");
				fwrite($fp,"wait-ack-expire=0x02\n");
				fwrite($fp,"flow-control=0\n");
				fwrite($fp,"window=10\n");
				fwrite($fp,"max-pending-submits=10\n");
				fwrite($fp,"allowed-smsc-id=$smsc->route\n");
				fclose($fp);
                            }
                        }
                        
                        if ($smsc->maxRx > '0'){
                            for ($i = 1; $i <= $smsc->maxRx; $i++) {
			
				$fp = fopen("/usr/local/sbin/$path/smsc.d/$smsc->route-Rx-$i".'.conf', "w");
				fwrite($fp,"\ngroup = smsc\n");
				fwrite($fp,"smsc=smpp\n");
				//fwrite($fp,"alt-charset=ASCII\n");
				fwrite($fp,"smsc-id=$smsc->route\n");
				fwrite($fp,"host=$smsc->server\n");
				fwrite($fp,"receive-port=$smsc->port\n");
				fwrite($fp,"reconnect-delay = 30\n");
				fwrite($fp,"smsc-username=$smsc->systemId\n");
				fwrite($fp,"smsc-password=$smsc->password\n");
				fwrite($fp,"source-addr-ton = 0\n");
				fwrite($fp,"source-addr-npi = 1\n");
				fwrite($fp,"transceiver-mode=0\n");
				fwrite($fp,"system-type=smpp\n");
				fwrite($fp,"preferred-smsc-id=$smsc->route\n");
				fwrite($fp,"throughput=$smsc->throughput\n");
				fwrite($fp,"wait-ack = 10\n");
				fwrite($fp,"wait-ack-expire=0x02\n");
				fwrite($fp,"flow-control=0\n");
				fwrite($fp,"window=10\n");
				fwrite($fp,"max-pending-submits=10\n");
				fwrite($fp,"allowed-smsc-id=$smsc->route\n");
				fclose($fp);
                            }
                        }
                        
                        if ($smsc->maxTx > '0'){
                            for ($i = 1; $i <= $smsc->maxTx; $i++) {
			
				$fp = fopen("/usr/local/sbin/$path/smsc.d/$smsc->route-Tx-$i".'.conf', "w");
				fwrite($fp,"\ngroup = smsc\n");
				fwrite($fp,"smsc=smpp\n");
				//fwrite($fp,"alt-charset=ASCII\n");
				fwrite($fp,"smsc-id=$smsc->route\n");
				fwrite($fp,"host=$smsc->server\n");
				fwrite($fp,"port=$smsc->port\n");
				fwrite($fp,"reconnect-delay = 30\n");
				fwrite($fp,"smsc-username=$smsc->systemId\n");
				fwrite($fp,"smsc-password=$smsc->password\n");
				fwrite($fp,"source-addr-ton = 0\n");
				fwrite($fp,"source-addr-npi = 1\n");
				fwrite($fp,"transceiver-mode=0\n");
				fwrite($fp,"system-type=smpp\n");
				fwrite($fp,"preferred-smsc-id=$smsc->route\n");
				fwrite($fp,"throughput=$smsc->throughput\n");
				fwrite($fp,"wait-ack = 10\n");
				fwrite($fp,"wait-ack-expire=0x02\n");
				fwrite($fp,"flow-control=0\n");
				fwrite($fp,"window=10\n");
				fwrite($fp,"max-pending-submits=10\n");
				fwrite($fp,"allowed-smsc-id=$smsc->route\n");
				fclose($fp);
                            }
                        }
			
			
			$em->persist($smsc);
			$em->flush();
			
			return $smsc;
		} 
		return false;
		
	}
	
	/**
	 *
	 * @param unknown $id
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function blockSmsc($id, $type, $em)
	{
		if (isset($id)){
			$user = $em->getRepository('modules\smpp\models\Smsc')->find($id);
			if ($type == 'TRANSACTIONAL' || $type == 'TRANS-SCRUB'){
				$path = 'trans';
			}elseif ($type == 'PROMOTIONAL') {
				$path = 'promo';
			}elseif ($type == 'INTERNATIONAL'){
				$path = 'inter';
			}
			$user->status = '0';
			$em->persist($user);
			$em->flush();
			shell_exec("rm -f /usr/local/sbin/$path/smsc.d/$user->route".'*.conf');
					
			return $user;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $user
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function unblockSmsc($id, $type, $em)
	{
		if (isset($id)){
			$smsc = $em->getRepository('modules\smpp\models\Smsc')->find($id);
			if ($type == 'TRANSACTIONAL' || $type == 'TRANS-SCRUB'){
				$path = 'trans';
			}elseif ($type == 'PROMOTIONAL') {
				$path = 'promo';
			}elseif ($type == 'INTERNATIONAL'){
				$path = 'inter';
			}
			$smsc->status = '1';
			$em->persist($smsc);
			$em->flush();
                        
                        if ($smsc->maxBinds > '0'){
                            for ($i = 1; $i <= $smsc->maxBinds; $i++) {
			
				$fp = fopen("/usr/local/sbin/$path/smsc.d/$smsc->route-TRx-$i".'.conf', "w");
				fwrite($fp,"\ngroup = smsc\n");
				fwrite($fp,"smsc=smpp\n");
				//fwrite($fp,"alt-charset=ASCII\n");
				fwrite($fp,"smsc-id=$smsc->route\n");
				fwrite($fp,"host=$smsc->server\n");
				fwrite($fp,"port=$smsc->port\n");
				fwrite($fp,"reconnect-delay = 30\n");
				fwrite($fp,"smsc-username=$smsc->systemId\n");
				fwrite($fp,"smsc-password=$smsc->password\n");
				fwrite($fp,"source-addr-ton = 0\n");
				fwrite($fp,"source-addr-npi = 1\n");
				fwrite($fp,"transceiver-mode=1\n");
				fwrite($fp,"system-type=smpp\n");
				fwrite($fp,"preferred-smsc-id=$smsc->route\n");
				fwrite($fp,"throughput=$smsc->throughput\n");
				fwrite($fp,"wait-ack = 10\n");
				fwrite($fp,"wait-ack-expire=0x02\n");
				fwrite($fp,"flow-control=0\n");
				fwrite($fp,"window=10\n");
				fwrite($fp,"max-pending-submits=10\n");
				fwrite($fp,"allowed-smsc-id=$smsc->route\n");
				fclose($fp);
                            }
                        }
                        
                        if ($smsc->maxRx > '0'){
                            for ($i = 1; $i <= $smsc->maxRx; $i++) {
			
				$fp = fopen("/usr/local/sbin/$path/smsc.d/$smsc->route-Rx-$i".'.conf', "w");
				fwrite($fp,"\ngroup = smsc\n");
				fwrite($fp,"smsc=smpp\n");
				//fwrite($fp,"alt-charset=ASCII\n");
				fwrite($fp,"smsc-id=$smsc->route\n");
				fwrite($fp,"host=$smsc->server\n");
				fwrite($fp,"receive-port=$smsc->port\n");
				fwrite($fp,"reconnect-delay = 30\n");
				fwrite($fp,"smsc-username=$smsc->systemId\n");
				fwrite($fp,"smsc-password=$smsc->password\n");
				fwrite($fp,"source-addr-ton = 0\n");
				fwrite($fp,"source-addr-npi = 1\n");
				fwrite($fp,"transceiver-mode=0\n");
				fwrite($fp,"system-type=smpp\n");
				fwrite($fp,"preferred-smsc-id=$smsc->route\n");
				fwrite($fp,"throughput=$smsc->throughput\n");
				fwrite($fp,"wait-ack = 10\n");
				fwrite($fp,"wait-ack-expire=0x02\n");
				fwrite($fp,"flow-control=0\n");
				fwrite($fp,"window=10\n");
				fwrite($fp,"max-pending-submits=10\n");
				fwrite($fp,"allowed-smsc-id=$smsc->route\n");
				fclose($fp);
                            }
                        }
                        
                        if ($smsc->maxTx > '0'){
                            for ($i = 1; $i <= $smsc->maxTx; $i++) {
			
				$fp = fopen("/usr/local/sbin/$path/smsc.d/$smsc->route-Tx-$i".'.conf', "w");
				fwrite($fp,"\ngroup = smsc\n");
				fwrite($fp,"smsc=smpp\n");
				//fwrite($fp,"alt-charset=ASCII\n");
				fwrite($fp,"smsc-id=$smsc->route\n");
				fwrite($fp,"host=$smsc->server\n");
				fwrite($fp,"port=$smsc->port\n");
				fwrite($fp,"reconnect-delay = 30\n");
				fwrite($fp,"smsc-username=$smsc->systemId\n");
				fwrite($fp,"smsc-password=$smsc->password\n");
				fwrite($fp,"source-addr-ton = 0\n");
				fwrite($fp,"source-addr-npi = 1\n");
				fwrite($fp,"transceiver-mode=0\n");
				fwrite($fp,"system-type=smpp\n");
				fwrite($fp,"preferred-smsc-id=$smsc->route\n");
				fwrite($fp,"throughput=$smsc->throughput\n");
				fwrite($fp,"wait-ack = 10\n");
				fwrite($fp,"wait-ack-expire=0x02\n");
				fwrite($fp,"flow-control=0\n");
				fwrite($fp,"window=10\n");
				fwrite($fp,"max-pending-submits=10\n");
				fwrite($fp,"allowed-smsc-id=$smsc->route\n");
				fclose($fp);
                            }
                        }
                        
                        
	
			return $smsc;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $user
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function deleteSmsc($id, $type, $em)
	{
		if (isset($id)){
			$user = $em->getRepository('modules\smpp\models\Smsc')->find($id);
			if ($type == 'TRANSACTIONAL' || $type == 'TRANS-SCRUB'){
				$path = 'trans';
			}elseif ($type == 'PROMOTIONAL') {
				$path = 'promo';
			}elseif ($type == 'INTERNATIONAL'){
				$path = 'inter';
			}
			$user->status = '0';
			shell_exec("rm -f /usr/local/sbin/$path/smsc.d/$user->route".'*.conf');
			$em->remove($user);
			$em->flush();
			
			
			return $user;
		}
		return false;
	}

	public function editSmsc($id, $systemId, $ip, $port, $password, $type, $bind, $tx, $rx, $tps, $em)
	{
		if (isset($id)){
			$smsc = $em->getRepository('modules\smpp\models\Smsc')->find($id);
			$smsc->systemId = $systemId;
			$smsc->password = $password;
			$smsc->server = $ip;
			$smsc->port = $port;
                        $smsc->maxBinds = $bind;
                        $smsc->maxTx = $tx;
                        $smsc->maxRx = $rx;
                        $smsc->throughput = $tps;
			$em->persist($smsc);
			$em->flush();
			
			return $smsc;
		}
		return false;
	}
	
}