<?php
namespace modules\smpp\models;
use Doctrine\ORM\EntityRepository;

/**
 * 
 * @author Ani
 *
 */
class SmppTemplateRepository extends \Smpp_Doctrine_EntityRepository
{
	public function insertTemplate($getArray)
	{
		//print_r($getArray);
		$user=$getArray['smppUser'];
		$temp = trim($getArray['regex']);
		$template="^".$temp."$";
		$cdate=date("Y-m-d H:i:s");
		$sql = "INSERT INTO `smpp_template` (`template_id`,`system_id`, `regex`) VALUES (NULL,'$user', '$template')" ;
		$em = $this->getEntityManager();
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute(); 
		return $stmt;
	
	}
}