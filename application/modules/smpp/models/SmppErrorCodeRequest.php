<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppErrorCodeRequestRepository")
 * @Table(name="error_map_request")
 * @author Vinayak
 *
 */
class SmppErrorCodeRequest
{
	/**
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var integer
	 */
	private $id;
        
        /**
	 * @Column(name="user_id", type="string")
	 * @var text
	 */
	private $user;
	
	/**
	 * @Column(name="opco_error_codes", type="string")
	 * @var text
	 */
	private $opcoErrorCodes;
	
	/**
	 * @Column(name="error_code_map", type="string")
	 * @var text
	 */
	private $errorCode;
        
        /**
	 * @Column(name="type", type="string", nullable=true)
	 * @var text
	 */
	private $type;
        
        /**
	 * @Column(name="smsc_id", type="string", nullable=true)
	 * @var text
	 */
	private $smscId;
        
        /**
	 * @Column(name="description", type="string", nullable=true)
	 * @var text
	 */
	private $description;
	
	
	/**
	 * @column(name="status", type="integer")
	 * @var integer
	 */
	private $status;
	
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}