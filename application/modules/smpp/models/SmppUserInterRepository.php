<?php
namespace modules\smpp\models;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class SmppUserInterRepository extends \Smpp_Doctrine_EntityRepository
{
	public function getSmpp($userId)
	{
       $rsm = new ResultSetMapping();
       $rsm->addEntityResult('modules\smpp\models\SmppUserInter', 'u');
       $rsm->addFieldResult('u', 'system_id', 'systemId');
       $rsm->addFieldResult('u', 'default_smsc', 'defaultSmsc');
       $query = $this->getEntityManager()->createNativeQuery("SELECT * FROM users_smpp_users_inter JOIN smpp_users_inter ON smpp_users_inter.system_id =users_smpp_users_inter.system_id WHERE user_id = $userId AND smpp_users_inter.type = 'INTERNATIONAL'", $rsm);

       return $query->getResult();
   }
   
   
   public function users($id,$type='SID')
   {
    $cdate=date("Y-m-d");
    if($type=='UID')
        $sqleer = "SELECT `system_id` FROM `users_smpp_users` WHERE user_id ='$id'";
    else if($type=='SID')
        $sqleer = "SELECT `user_id` FROM `users_smpp_users` WHERE system_id ='$id'";
    
    $em = $this->getEntityManager();
    $stmt1 = $em->getConnection()->prepare($sqleer);
    $stmt1->execute();
    $statement= $stmt1->fetchAll();
    return $statement;
    
}

}