<?php
namespace modules\smpp\models;

class SmppSmscRetryPromoManager  extends \Smpp_Doctrine_BaseManager
{
    public function createRoute($routeValues, $em)
	{
		if (isset($em)){
                    $route = new SmppSmscRetryPromo();
                    $route->smscId = $routeValues['route'];
                    $route->smscIdRetry = $routeValues['reroute'];
                    $route->service = $routeValues['esme'];
                    $route->status = '0';
                    $route->createdDate = new \DateTime('now');
                    $route->updatedDate = new \DateTime('now');
                    $em->persist($route);
                    $em->flush();

                    return $route;
	
		}
		return false;
	}	
        
public function delete($id, $em)
    {
        if (isset($em)){
           $routeRepo = $em->getRepository('modules\smpp\models\SmppSmscRetryPromo')->find($id);
           $em->remove($routeRepo);
           $em->flush();
        }
         
    }
    
    public function block($id, $em)
	{
		if (isset($id)){
			$user = $em->getRepository('modules\smpp\models\SmppSmscRetryPromo')->find($id);
			$user->status = '0';
			$em->persist($user);
			$em->flush();
				
			return $user;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $user
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function unblock($id, $em)
	{
		if (isset($id)){
			$user = $em->getRepository('modules\smpp\models\SmppSmscRetryPromo')->find($id);
			$user->status = '1';
			$em->persist($user);
			$em->flush();
				
			return $user;
		}
		return false;
	}
}