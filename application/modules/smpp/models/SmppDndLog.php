<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppDndLogRepository")
 * @Table(name="smpp_dnd_log")
 * @author Vinayak
 *
 */
class SmppDndLog
{
	/**
	* @Column(name="id", type="integer", nullable=false)
	* @Id
	* @GeneratedValue(strategy="IDENTITY")
	* @var integer
	*/
	private $id;

	/**
	 * @column(name="file_name", type="string", nullable="false")
	 * @var unknown
	 */
	private $fileName;
	
	/**
	 * @column(name="status", type="integer", nullable="false")
	 * @var unknown
	 */
	private $status;
		
	/**
	 * @Column(name="created_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $createdDate;
	
	/**
	 * @Column(name="updated_date", type="datetime", nullable=false)
	 * @var datetime
	 */
	private $updatedDate;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}
