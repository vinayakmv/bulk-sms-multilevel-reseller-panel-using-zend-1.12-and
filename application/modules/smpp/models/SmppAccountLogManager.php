<?php
namespace modules\smpp\models;

class SmppAccountLogManager extends \Smpp_Doctrine_BaseManager
{
	public function createLog($userRepo, $resellerRepo, $amount, $userBalanceBefore, $userBalanceAfter, $clientBalanceBefore, $clientBalanceAfter, $mode, $type, $em)
	{
		if (isset($em)){
			$account = new SmppAccountLog();
			$account->user = $resellerRepo;
			$account->client = $userRepo;
			$account->userBalanceBefore = $userBalanceBefore;
			$account->clientBalanceBefore = $clientBalanceBefore;
			$account->userBalanceAfter = $userBalanceAfter;
			$account->clientBalanceAfter = $clientBalanceAfter;
			
			$account->amount = $amount;
			$account->mode = $mode;
			$account->type = $type;
			$account->createdDate = new \DateTime('now');
			$account->updatedDate = new \DateTime('now');
			$em->persist($account);
			$em->flush();
				
			return $account;
		}
		return false;
	}
}
