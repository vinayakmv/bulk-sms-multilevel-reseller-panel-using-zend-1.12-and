<?php
namespace modules\smpp\models;

/**
 * @Entity(repositoryClass="modules\smpp\models\SmppTemplateRepository")
 * @Table(name="smpp_template")
 * @author Ani
 *
 */
class SmppTemplate
{
	/**
	 * @Column(name="template_id", type="bigint", nullable=true)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 * @var bigint
	 */
	private $templateId;
		
	/**
	 * @Column(name="system_id", type="string")
	 * @var string
	 */
	private $systemId;
		
	/**
	 * @Column(name="regex ", type="text",length="65500")
	 * @var text
	 */
	private $regex;
	
	/**
	 * Magic function to set properties
	 * @param string $property
	 * @param mixed $value
	 */
	function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Magic function to get values
	 * @param string $property
	 */
	function __get($property)
	{
		return $this->$property;
	}
	
}