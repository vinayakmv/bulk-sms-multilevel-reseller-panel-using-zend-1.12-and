<?php
namespace modules\smpp\models;

use Doctrine\ORM\Query\ResultSetMapping;
class SmppUserTransManager  extends \Smpp_Doctrine_BaseManager
{
public function createSmppUser($smppValues, $userId, $emUser, $emSmpp)
	{
		//exit();
		if (isset($userId)) {
			$routes = explode('|', $smppValues['route']);
			$type = $routes['0'];
			if ($routes['0'] == 'TRANSACTIONAL') {
				$route = $routes['1'];
				$ndnc = '0';
				$template = '0';
				$sender = '0';
			}elseif ($routes['0'] == 'PROMOTIONAL') {
				$route = $routes['1'];
				$ndnc = '1';
				$template = '0';
				$sender = '0';
			}elseif ($routes['0'] == 'TRANS-SCRUB') {
				$route = $routes['1'];
				$ndnc = '1';
				$template = '0';
				$sender = '0';
			}
			$username = $smppValues['username'];
				
			$smppRepo = $emSmpp->getRepository("modules\smpp\models\SmppUserTrans")->find($username);
				
			if(isset($smppRepo) && count($smppRepo->systemId) > 0) {
				return array('Esme' => array('isFailed' => 'Failed to create esme','data' => $smppRepo->systemId));
			}
			
			$userRepo = $emUser->getRepository("modules\user\models\User")->find($userId);
			$maxSmpp = $userRepo->account->smppUser;
			
			$sql="SELECT count(system_id) as count FROM users_smpp_users_trans WHERE user_id='$userId'";
    		$stmt = $emSmpp->getConnection()->prepare($sql);
    		$stmt->execute();
    		$esmeTrans = $stmt->fetchAll();
    		
			if($esmeTrans['0']['count'] >= $maxSmpp) {
				return array('Esme' => array('isFailed' => 'Failed to create esme, max allowed esme created','data' => $userRepo->account->smppUser));
			}
			
			
			$password = $smppValues['password'];
			$tps = $smppValues['tps'];
			$cost = $smppValues['cost'];
			$bind = $smppValues['bind'];
			$ip = $smppValues['ip'];
			
			$sql = "INSERT INTO `smpp_users_trans` (`system_id`, `password`, `hash`, `default_smsc`, `enable_prepaid_billing`, `callback_url`, `simulate`, `simulate_deliver_every`, `simulate_temporary_failure_every`, `throughput`, `default_cost`, `enable_check_ndnc`, `enable_check_sender`, `enable_check_template`, `credit`, `simulate_permanent_failure_every`, `simulate_mo_every`, `max_binds`,`connect_allow_ip`, `counter`, `recredit_date`, `type`) VALUES ('$username', PASSWORD('$password'), '$password', '$route', '1', NULL, '0', '0', '0', '$tps', '1', '$ndnc', '$sender', '$template', '0', '0', '0', '$bind', '$ip', '0', CURDATE(), '$type')";
			$stmt = $emSmpp->getConnection()->prepare($sql);
			$stmt->execute();
			
			$sql = "INSERT INTO `users_smpp_users_trans` (`system_id`, `user_id`) VALUES ('$username', '$userId')";
			$stmt = $emSmpp->getConnection()->prepare($sql);
			$stmt->execute();
			
			return array('Esme' => array('isSuccess' => 'Esme created successfully','data' => $stmt));
			
		} 
		return false;
		
	}
	
	public function setSmppCredit($transactionValues, $resellerId, $emUser, $emSmpp)
	{
		$amount = $transactionValues['amount'];
		$username = $transactionValues['username'];
		$resellerRepo = $emUser->getRepository('modules\user\models\User')->find($resellerId);
		
		
		//$routeRepo = $em->getRepository('Models\Route')->findOneBy(array('route' => "$userRepo->defaultSmsc"));
		$account = $transactionValues['type'];
		
			switch ($account) {
				 case 'TRANSACTIONAL':
					//TRANSACTIONAL
					$userRepo = $emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->find($username);
					if (isset($userRepo)){
						$resellerBalance = $resellerRepo->account->smppTransCredit;
						$userBalance = $userRepo->credit;
						if ($resellerBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance - $amount;
							$userBalanceAfter = $userBalance + $amount;
							$resellerRepo->account->smppTransCredit = $resellerBalanceAfter;
							$userRepo->credit = $userBalanceAfter;
							$emUser->persist($resellerRepo);
							$emUser->flush();
							$emSmpp->persist($userRepo);
							$emSmpp->flush();
						
							$accountLog = new SmppAccountLogManager();
							$createLog = $accountLog->createLog($userRepo->systemId, $resellerRepo->id, $amount, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $emSmpp);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						
						}
					}
					
					break; 
				/* case 'PROMOTIONAL':
					//PROMOTIONAL
					$userRepo = $em->getRepository('Models\SmppUserPromo')->find($username);
					$resellerBalance = $resellerRepo->account->smppPromoCredit;
					$userBalance = $userRepo->credit;
					if ($resellerBalance >= $amount){
						//Add Balance To User
						$resellerBalanceAfter = $resellerBalance - $amount;
						$userBalanceAfter = $userBalance + $amount;
						$resellerRepo->account->smppPromoCredit = $resellerBalanceAfter;
						$userRepo->credit = $userBalanceAfter;
						$em->persist($resellerRepo);
						$em->persist($userRepo);
						$em->flush();
	
						$accountLog = new SmppAccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						
					}
					break; */
				 case 'TRANS-SCRUB':
					//TRANS-SCRUB
					$userRepo = $emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->find($username);
					if (isset($userRepo)){
						$resellerBalance = $resellerRepo->account->smppScrubCredit;
						$userBalance = $userRepo->credit;
						if ($resellerBalance >= $amount){
							//Add Balance To User
							$resellerBalanceAfter = $resellerBalance - $amount;
							$userBalanceAfter = $userBalance + $amount;
							$resellerRepo->account->smppScrubCredit = $resellerBalanceAfter;
							$userRepo->credit = $userBalanceAfter;
							$emUser->persist($resellerRepo);
							$emUser->flush();
							$emSmpp->persist($userRepo);
							$emSmpp->flush();
						
							$accountLog = new SmppAccountLogManager();
							$createLog = $accountLog->createLog($userRepo->systemId, $resellerRepo->id, $amount, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'ADD', $account, $emSmpp);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						
						}
					}
					
					break; 
	
			}

			return false;
	}
	
	public function setSmppDebit($transactionValues, $resellerId, $emUser, $emSmpp)
	{
		$amount = $transactionValues['amount'];
		$username = $transactionValues['username'];
		$resellerRepo = $emUser->getRepository('modules\user\models\User')->find($resellerId);
		
		
		//$routeRepo = $em->getRepository('Models\Route')->findOneBy(array('route' => "$userRepo->defaultSmsc"));
		$account = $transactionValues['type'];
		
			switch ($account) {
				case 'TRANSACTIONAL':
					//TRANSACTIONAL
					$userRepo = $emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->find($username);
					if (isset($userRepo)){
						$resellerBalance = $resellerRepo->account->smppTransCredit;
						$userBalance = $userRepo->credit;
						if ($userBalance >= $amount){
							//Deduct Balance From User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->account->smppTransCredit = $resellerBalanceAfter;
							$userRepo->credit = $userBalanceAfter;
							$emUser->persist($resellerRepo);
							$emUser->flush();
							$emSmpp->persist($userRepo);
							$emSmpp->flush();
						
							$accountLog = new SmppAccountLogManager();
							$createLog = $accountLog->createLog($userRepo->systemId, $resellerRepo->id, $amount, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $emSmpp);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						
						}
					}
					
					break; 
				/* case 'PROMOTIONAL':
					//PROMOTIONAL
					$userRepo = $em->getRepository('Models\SmppUserPromo')->find($username);
					$resellerBalance = $resellerRepo->account->smppPromoCredit;
					$userBalance = $userRepo->credit;
					if ($userBalance >= $amount){
						//Deduct Balance From User
						$resellerBalanceAfter = $resellerBalance + $amount;
						$userBalanceAfter = $userBalance - $amount;
						$resellerRepo->account->smppPromoCredit = $resellerBalanceAfter;
						$userRepo->credit = $userBalanceAfter;
						$em->persist($resellerRepo);
						$em->persist($userRepo);
						$em->flush();
	
						$accountLog = new SmppAccountLogManager();
						$createLog = $accountLog->createLog($userRepo, $resellerRepo, $amount, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $em);
						return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						
					}
					break; */
				case 'TRANS-SCRUB':
					//TRANS-SCRUB
					$userRepo = $emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->find($username);
					if (isset($userRepo)){
						$resellerBalance = $resellerRepo->account->smppScrubCredit;
						$userBalance = $userRepo->credit;
						if ($userBalance >= $amount){
							//Deduct Balance From User
							$resellerBalanceAfter = $resellerBalance + $amount;
							$userBalanceAfter = $userBalance - $amount;
							$resellerRepo->account->smppScrubCredit = $resellerBalanceAfter;
							$userRepo->credit = $userBalanceAfter;
							$emUser->persist($resellerRepo);
							$emUser->flush();
							$emSmpp->persist($userRepo);
							$emSmpp->flush();
						
							$accountLog = new SmppAccountLogManager();
							$createLog = $accountLog->createLog($userRepo->systemId, $resellerRepo->id, $amount, $resellerBalance, $resellerBalanceAfter, $userBalance, $userBalanceAfter, 'DEDUCT', $account, $emSmpp);
							return array('user'=> $userRepo, 'reseller' => $resellerRepo, 'log' => $createLog);
						
						}
					}
					
					break;
	
			}
			return false;
	}
	
	/**
	 *
	 * @param unknown $id
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function blockUser($systemId, $em)
	{
		if (isset($systemId)){
			$user = $em->getRepository('modules\smpp\models\SmppUserTrans')->find($systemId);
			$user->simulate = '1';
			$user->simulatePermanentFailureEvery = '1';
			$em->persist($user);
			$em->flush();
	
			return $user;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $user
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function unblockUser($systemId, $em)
	{
		if (isset($systemId)){
			$user = $em->getRepository('modules\smpp\models\SmppUserTrans')->find($systemId);
			$user->simulate = '0';
			$user->simulatePermanentFailureEvery = '0';
			$em->persist($user);
			$em->flush();
	
			return $user;
		}
		return false;
	}
	
	/**
	 *
	 * @param unknown $user
	 * @param unknown $em
	 * @return unknown|boolean
	 */
	public function deleteUser($systemId, $userId, $em)
	{
		if (isset($systemId)){

			$sql = "DELETE FROM `users_smpp_users` WHERE `user_id` = :userid AND `system_id` = :systemid";
			$params = array('userid'=>$userId, 'systemid'=>$systemId);
				
			$stmt = $em->getConnection()->prepare($sql);
			$stmt->execute($params);
			
			$user = $em->getRepository('modules\smpp\models\SmppUserTrans')->find($systemId);
									
			$em->remove($user);
			$em->flush();
	
			return $user;
		}
		return false;
	}

	public function editUser($systemId, $ip, $route=false, $tps=false, $binds=false, $delivered, $failed, $em)
	{
		if (isset($systemId)){			
			$user = $em->getRepository('modules\smpp\models\SmppUserTrans')->find($systemId);
			$user->allowIp = trim($ip);
			if ($route){
				$user->defaultSmsc = trim($route);
			}
			if ($tps){
				$user->throughput = trim($tps);
			}
			if ($binds){
				$user->maxBinds = trim($binds);
			}
			$user->simulateDeliverEvery = "$delivered";
                        $user->simulatePermanentFailureEvery = "$failed";
			
			//$user->enableCheckSender = $checkSender;
			//$user->enableCheckTemplate = $checkTemplate;
			//$user->enableCheckNdnc = $checkNdnc;
			$em->persist($user);
			$em->flush();
	
			return $user;
		}
		return false;
	}
}
