<?php
namespace modules\smpp\models;

class SmscRecreditManager extends \Smpp_Doctrine_BaseManager
{
	public function createActivityLog($userRepo, $clientRepo, $recreditDate, $type, $status, $em)
	{
		if (isset($em)){
			$recredit = new SmscRecredit();
			$recredit->user = $userRepo;
			$recredit->client = $clientRepo;
			$recredit->type = $type;
			$recredit->status = $status;
			$recredit->recreditDate = $recreditDate;
			$recredit->createdDate = new \DateTime('now');
			$recredit->updatedDate = new \DateTime('now');
			$em->persist($recredit);
			$em->flush();
				
			return $recredit;
		}
		return false;
	}

}
