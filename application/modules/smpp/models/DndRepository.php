<?php
namespace modules\smpp\models;
use Doctrine\ORM\EntityRepository;
/**
 *
 * @author Vinayak
 *
 */
class DndRepository extends \Smpp_Doctrine_EntityRepository
{
	/**
	 *
	 * @param array $numbers
	 */
	public function getDnd($numbers){
		
		//$dql = "SELECT u.mobile FROM modules\user\models\Dnd u WHERE u.mobile IN (:numbers)";
		//$number = implode(',',$numbers);
		
		//$dql = "SELECT m FROM modules\user\models\Dnd m WHERE m.mobile IN($number)";	
		//$query = $this->getEntityManager()
		//				->createQuery($dql)
		//				->setParameter('numbers', implode(',',$numbers));	
		
		$query = $this->getEntityManager()->createQueryBuilder();
		$query->select('m');
		$query->from('modules\smpp\models\Dnd', 'm');
		$query->where($query->expr()->in('m.number', $numbers));
		$result = $query->getQuery()->getResult();
		
		//$result =  $query->getResult();
		$result = $query->getQuery()->getArrayResult();
		
		$dnd = array_map(function($value) { return $value['number']; }, $result);
		/*
		$dnd = array();
		foreach ($result as $value) {
			$dnd[] = $value->number;
		}
		*/ 
		
		return $dnd;
	}
}