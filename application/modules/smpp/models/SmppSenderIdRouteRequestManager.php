<?php
namespace modules\smpp\models;
/**
 * 
 * @author Vinayak
 *
 */
class SmppSenderIdRouteRequestManager  extends \Smpp_Doctrine_BaseManager
{

	public function createSenderId($getValues, $userId, $em)
	{
		if (isset($em)){
			$name=$getValues['senderId'];	
			$fname=str_replace('%0D%0A', '%2C', urlencode($name));
			$name1=str_replace("%2C","|",$fname);
			
						
			$sender = new SmppSenderIdRouteRequest();
			$sender->user = $userId;
			$sender->name = $name1;
			$sender->description = $getValues['description'];
			$sender->status = '0';
			$sender->createdDate = new \DateTime('now');
			$sender->updatedDate = new \DateTime('now');
			$em->persist($sender);
			$em->flush();
				
			return $sender;
			
		}
		return false;
	}
	public function manageSenderId($getValues,$status, $userId, $em)
	{
		
		if (isset($getValues)){
			$id=$getValues['uniqId'];    		
			$senderid = $em->getRepository('modules\smpp\models\SmppSenderIdRouteRequest')->find($id);
			if($status=='approved'){
				$senderid->status = '1';
				$senderid->updatedDate = new \DateTime('now');
				$getValues['smppUser']= $senderid->systemId;
				$getValues['regex']= $senderid->name;
				
				$smppSenderIdTransManager = new SmppRouteTransManager();
				$insertSenderId = $smppSenderIdTransManager->insertSenderId($getValues, $type, $em);

			}
			else if($status=='Rejected'){
				
				$senderid->status = '0';
				$senderid->updatedDate = new \DateTime('now');
			}
			
			
			$em->persist($senderid);
			$em->flush();
	
			return $senderid;
		}
		return false;
	}
	
}
