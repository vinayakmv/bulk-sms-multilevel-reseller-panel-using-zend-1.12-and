<?php
namespace modules\smpp\models;
/**
 * 
 * @author Vinayak
 *
 */
class SmppTemplateTransManager  extends \Smpp_Doctrine_BaseManager
{
	public function insertTemplate($getArray, $em)
	{
		$smppUser=$getArray['smppUser'];
		$template=$getArray['regex'];
		
		$template="^".$template."$";
		$cdate=date("Y-m-d H:i:s");
		$sql = "INSERT INTO `smpp_template_trans` (`system_id`, `regex`) VALUES ('$smppUser', '$template')";
	
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		
		return $stmt;
	
	}
	
	public function deleteTemplate($getArray, $em)
	{
		$smppUser=$getArray['smppUser'];
		$template=$getArray['template'];
	
		$template="^".$template."$";
		$sql = "DELETE FROM `smpp_template_trans` WHERE `system_id` = '$smppUser' AND `regex` = '$template'";
	
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
	
		return $stmt;
	
	}
}
