<?php

class Smpp_Form_Transaction extends Zend_Form
{

public function init()
    {
        $this->setAction('/smpp/transaction/index');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	
    	$selectRole = new Zend_Form_Element_Select('type');
    	$roles = array("SELECT SMPP TYPE" => "SELECT SMPP TYPE",
    					"TRANSACTIONAL" => "TRANSACTIONAL",
    				   "PROMOTIONAL" => "PROMOTIONAL",
    			       "TRANS-SCRUB" => "TRANS-SCRUB",
    				   "INTERNATIONAL" => "INTERNATIONAL"
    	);
    	$selectRole->addMultiOptions($roles);
    	$selectRole->setDecorators(array('ViewHelper','Description'));
    	$selectRole->setLabel('Select Route');
    	$selectRole->class = "form-control select2 custom-select";
    	
    	$selectRole->setRequired(true);
    	
    	$selectRole->addFilter(new Zend_Filter_HtmlEntities());
    	$selectRole->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($selectRole);
    	
    	$username = new Zend_Form_Element_Select('username');
    	$username->setRegisterInArrayValidator(false);
    	$username->setDecorators(array('ViewHelper',
						    			'Description'));
    	$users = array("SELECT ESME USER" => "SELECT ESME USER");
    	$username->addMultiOptions($users);
    	$username->setLabel('Username ');
    	$username->class = "form-control select2 custom-select";		
    	$username->setAttrib('id', 'smpp');
    	$username->setRequired(false);    	
    	
    	//Add Validator
    	//$username->addValidator(new Zend_Validate_StringLength(4,20));
    	//Add Filter
    	//$username->addFilter(new Zend_Filter_HtmlEntities());
    	//$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);
    	
    	$token = new Zend_Form_Element_Hash('token',array('timeout' => '1800'));
    	$token->setSalt(md5(uniqid(rand(), TRUE)));
    	
    	//$token->setTimeout(Globals::getConfig()->authentication->timeout);
    	$token->setDecorators(array('ViewHelper','Description'));
    	$this->addElement($token);
    	
    	$amount = new Zend_Form_Element_Text('amount');
    	$amount->setDecorators(array('ViewHelper',
					    			'Description'));
    	$amount->setLabel('Credits');
    	$amount->class = "form-control";
    	$amount->setRequired(true);
    	
    	//Add Validator
    	$amount->addValidator(new Zend_Validate_Digits());
    	//Add Filter
    	$amount->addFilter(new Zend_Filter_HtmlEntities());
    	$amount->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($amount);
    	
    	//Create favorite radio buttons.
    			
		$typeOptions = array("1" => "Transfer to User",
							"2" => "Transfer from User");
		$modetype = new Zend_Form_Element_Radio('mode');
		$modetype->addMultiOptions($typeOptions)->setSeparator('');
		$modetype->setDecorators(array('ViewHelper','Description'));
		$modetype->setLabel('Role Type');
		$modetype->setRequired(true);
		//Add Account type
		$this->addElement($modetype);
		
		/*$captcha = new Zend_Form_Element_Captcha(
				'captcha', // This is the name of the input field
				array(
						'captcha' => array( // Here comes the magic...
								// First the type...
								'captcha' => 'Image',
								// Length of the word...
								'wordLen' => 6,
								// Captcha timeout, 5 mins
								'timeout' => 300,
								// What font to use...
								'font' => '/var/www/html/webpanel/public/captcha/fonts/aller.ttf',
								// Where to put the image
								'imgDir' => '/var/www/html/webpanel/public/captcha/image',
								// URL to the images
								// This was bogus, here's how it should be... Sorry again :S
								'imgUrl' => '/captcha/image',
								'fontSize' => '24',
								'height' => '80',
								'width' => '185',
								'dotNoiseLevel' => '0',
    							'lineNoiseLevel' => '0',
    					)));
								$captcha->setDecorators(array('ViewHelper','Description'));
										$captcha->removeDecorator('ViewHelper','Description');
												$captcha->class = "form-control";
												 
						$this->addElement($captcha);*/
		
    	//Create a submit button.
    	$this->addElement('submit', 'transfer');
    	$submitElement = $this->getElement('transfer');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
											'Description',
											'Errors'));
		$submitElement->setLabel('Transfer');
    	
    	$this->setDecorators(array('FormElements',
    			'Form'
    	));
    }


}

