<?php

class Smpp_Form_Reroute extends Zend_Form
{

    public function init()
    {
        $this->setAction('reroute');
        $this->setMethod('post');
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        $this->setAttrib('class', "form-horizontal row-border");
                
        
        $selectRole = new Zend_Form_Element_Select('retype');
    	$roles = array("SELECT SMPP TYPE" => "SELECT SMPP TYPE",
    					"TRANSACTIONAL" => "TRANSACTIONAL",
    				   "PROMOTIONAL" => "PROMOTIONAL"
    	);
    	$selectRole->addMultiOptions($roles);
    	$selectRole->setDecorators(array('ViewHelper','Description'));
    	$selectRole->setLabel('Select Route');
    	$selectRole->class = "form-control select2 custom-select";
    	
    	$selectRole->setRequired(true);
    	
    	$selectRole->addFilter(new Zend_Filter_HtmlEntities());
    	$selectRole->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($selectRole);
        //text area
        $selectEsme = new Zend_Form_Element_Select('esme');
        $selectEsme->setRegisterInArrayValidator(false);
        $esmes = array("NO ESMES" => "NO ESMES");
        $selectEsme->addMultiOptions($esmes);
        $selectEsme->setDecorators(array('ViewHelper','Description'));
        $selectEsme->setLabel('Select Route');
        $selectEsme->class = "form-control select2 custom-select";

        //$selectEsme->setRequired(true);
        //Add Validator
        //Add Filter
        $selectEsme->addFilter(new Zend_Filter_HtmlEntities());
        $selectEsme->addFilter(new Zend_Filter_StripTags());
        //Add Username Element
        $this->addElement($selectEsme);
        
        $selectRoute = new Zend_Form_Element_Select('route');
        $selectRoute->setRegisterInArrayValidator(false);
        $routes = array("NO ROUTES" => "NO ROUTES");
        $selectRoute->addMultiOptions($routes);
        $selectRoute->setDecorators(array('ViewHelper','Description'));
        $selectRoute->setLabel('Select Route');
        $selectRoute->class = "form-control select2 custom-select";

        //$selectRoute->setRequired(true);
        //Add Validator
        //Add Filter
        $selectRoute->addFilter(new Zend_Filter_HtmlEntities());
        $selectRoute->addFilter(new Zend_Filter_StripTags());
        //Add Username Element
        $this->addElement($selectRoute);
        
        $selectResent = new Zend_Form_Element_Select('reroute');
        $selectResent->setRegisterInArrayValidator(false);
        $roless = array("NO ROUTES" => "NO ROUTES");
        $selectResent->addMultiOptions($roless);
        $selectResent->setDecorators(array('ViewHelper','Description'));
        $selectResent->setLabel('Select Route');
        $selectResent->class = "form-control select2 custom-select";

        //$selectResent->setRequired(true);
        //Add Validator
        //Add Filter
        $selectResent->addFilter(new Zend_Filter_HtmlEntities());
        $selectResent->addFilter(new Zend_Filter_StripTags());
        //Add Username Element
        $this->addElement($selectResent);
        
        
        $this->addElement('submit', 'submit');
        $submitElement = $this->getElement('submit');
        $submitElement->setAttrib('class',"btn  btn-primary ");
        $submitElement->setDecorators(array('ViewHelper','Description'));
        $submitElement->setLabel('Submit');
        $this->setDecorators(array('FormElements','Form'));
    }


}

