<?php

class Smpp_Form_Smpp extends Zend_Form
{

public function init()
    {
        $this->setAction('sentsms');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;')); 
    	  //dropdown user type
//         $user = new Zend_Form_Element_Select('user');
//         $user->setDecorators(array('ViewHelper','Description'));
//         $user->class = "chzn-select";
//         $user->setRequired(TRUE);
//         $this->addElement($user);
    	//Role Hidden element
    	$selectRole = new Zend_Form_Element_Select('smpp');
		$roles = array(null => "NO ESME");
		$selectRole->addMultiOptions($roles);
		$selectRole->setDecorators(array('ViewHelper',
										'Description'));
		$selectRole->setLabel('Select Esme');
		$selectRole->class = "form-control select2 custom-select";
		
		$selectRole->setRequired(true);
		//Add Validator
		//Add Filter
		$selectRole->addFilter(new Zend_Filter_HtmlEntities());
		$selectRole->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectRole);
		
		//Create a submit button.
		$this->addElement('submit', 'submit');
		$submitElement = $this->getElement('submit');
		$submitElement->setAttrib('class',"btn-primary btn");
		$submitElement->setDecorators(array('ViewHelper',
				'Description','Errors'));
		$submitElement->setLabel('Show');
    	    	
    	$this->setDecorators(array('FormElements',
					    			'Form'));
    }


}

