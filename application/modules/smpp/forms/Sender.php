<?php

class Smpp_Form_Sender extends Zend_Form
{

    public function init()
    {
        $this->setAction('request');
        $this->setMethod('post');
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
        $this->setAttrib('class', "form-horizontal row-border");
         //dropdown type
        $user = new Zend_Form_Element_Select('user');
        $user->setDecorators(array('ViewHelper','Description'));
        $user->class = "form-control select2 custom-select";
        $user->setRequired(TRUE)
             ->setAttrib('required', 'required');
        $this->addElement($user);
        //text area
        $senderId = new Zend_Form_Element_Textarea('senderId');
        
        $senderId->setRequired(TRUE);
        $senderId->setDecorators(array('ViewHelper','Description'));
        $senderId   ->setAttrib('cols', '40')
                    ->setAttrib('required', 'required')
                    ->setAttrib('rows', '4');
        $senderId->class = "form-control";
        $senderId->addFilter(new Zend_Filter_HtmlEntities());
        $this->addElement($senderId);
        
        //text area
        $userremark = new Zend_Form_Element_Textarea('userremark');
        
        $userremark ->setRequired(TRUE);
        $userremark ->setDecorators(array('ViewHelper','Description'));
        $userremark ->setAttrib('cols', '40')
                    ->setAttrib('required', 'required')
                    ->setAttrib('rows', '4');
        $userremark->class = "form-control";
        $userremark->addFilter(new Zend_Filter_HtmlEntities());
        $this->addElement($userremark);

        $this->addElement('submit', 'submit');
        $submitElement = $this->getElement('submit');
        $submitElement->setAttrib('class',"btn  btn-primary ");
        $submitElement->setDecorators(array('ViewHelper','Description'));
        $submitElement->setLabel('Submit');
        $this->setDecorators(array('FormElements','Form'));
    }


}

