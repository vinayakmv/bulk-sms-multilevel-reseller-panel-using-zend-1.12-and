<?php

class Smpp_Form_NewSmpp extends Zend_Form
{

    public function init()
    {
        $this->setAction('create');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    
    	$username = new Zend_Form_Element_Text('username');
    	$username->setDecorators(array('ViewHelper',
						    			'Description'));
    	$username->setLabel('Username');
    	$username->class = "form-control";
    	//$username->setAttrib('placeholder', 'System ID');
    	//$username->addErrorMessage('Please enter a username');
    	$username->setRequired(true);
    
    	//Add Validator
    	$username->addValidator(new Zend_Validate_StringLength(8, 8));
    	
    	//$username->addValidator(new Smpp_Validate_Db_NoSmppUserExists());
    	//$username->addValidator(new Smpp_Validate_Db_IsMaxSmppUser());
    	
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);
    	
    	//Create Password Object.
    	$password = new Zend_Form_Element_Text('password');
    	$password->setDecorators(array('ViewHelper',
						    			'Description'));
    	$password->setLabel('Password');
    	$password->class = "form-control";
    	//$password->setAttrib('placeholder', 'Password');
    	$password->setRequired(true);
    	//Add Validator
    	$passwordOpts = array('requireAlpha' => true,
    			'requireNumeric' => true,
    			'requireCapital' => true,
    			'requireSymbol' => true,
    			'minPasswordLength' => 8);
    	
    	$password->addValidator(new Smpp_Validate_Password($passwordOpts));
    	$password->addValidator(new Zend_Validate_StringLength(8, 8));
    	
    	//Add Filter
    	$password->addFilter(new Zend_Filter_HtmlEntities());
    	$password->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($password);
    	
    	//Create Password Object.
    	/*$repassword = new Zend_Form_Element_Password('repassword');
    	$repassword->setDecorators(array('ViewHelper',
						    			'Description'));
    	$repassword->setLabel('Confirm');
    	$repassword->class = "form-control";
    	$repassword->setAttrib('placeholder', 'Confirm Password');
    	$repassword->setRequired(true);
    	//Add Validator
    	$repassword->addValidator('Identical', false, array('token' => 'password'));
    	//Add Filter
    	$repassword->addFilter(new Zend_Filter_HtmlEntities());
    	$repassword->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($repassword);*/
    	
    	//Role Hidden element
    	$selectRole = new Zend_Form_Element_Select('route');
		$roles = array(null => "NO ROUTES");
		//$selectRole->addMultiOptions($roles);
		$selectRole->setDecorators(array('ViewHelper',
										'Description'));
		//$selectRole->setLabel('Select Route');
		$selectRole->class = "form-control select2 custom-select";
		
		$selectRole->setRequired(true);
		//Add Validator
		//Add Filter
		$selectRole->addFilter(new Zend_Filter_HtmlEntities());
		$selectRole->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectRole);
		
		$tps = new Zend_Form_Element_Text('tps');
		$tps->setDecorators(array('ViewHelper','Description'));
		$tps->setLabel('Throughput');
    	$tps->class = "form-control";
    	//$tps->setAttrib('placeholder', 'TPS');
    	//$tps->addErrorMessage('Please enter a username');
    	$tps->setRequired(true);
    
    	//Add Validator
    	$tps->addValidator(new Smpp_Validate_Db_IsMaxSmppTps());
    	
    	//Add Filter
    	$tps->addFilter(new Zend_Filter_HtmlEntities());
    	$tps->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($tps);
    	
    	$ip = new Zend_Form_Element_Text('ip');
    	$ip->class = "form-control";
    	//$ip->setAttrib('placeholder', 'Client IP');
    	$ip->addValidator('Ip', array('allowipv6' => false));
    	$ip->setDecorators(array('ViewHelper','Description'));
    	$ip->setRequired(true);
    	$this->addElement($ip);

    	$bind = new Zend_Form_Element_Text('bind');
    	$bind->setDecorators(array('ViewHelper','Description'));
    	$bind->setLabel('Bind/Session');
    	$bind->class = "form-control";
    	//$bind->setAttrib('placeholder', 'Session');
    	//$bind->addErrorMessage('Please enter a username');
    	$bind->setRequired(true);
    	
    	//Add Validator
    	$bind->addValidator(new Smpp_Validate_Db_IsMaxSmppBind());
    	
    	//Add Filter
    	$bind->addFilter(new Zend_Filter_HtmlEntities());
    	$bind->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($bind);
    	
    	$cost = new Zend_Form_Element_Text('cost');
    	$cost->setDecorators(array('ViewHelper','Description'));
    	$cost->setLabel('Sms Cost');
    	$cost->class = "form-control";
    	//$cost->setAttrib('placeholder', '0.00');
    	//$cost->addErrorMessage('Please enter a username');
    	$cost->setRequired(false);
    	 
    	//Add Validator
    	
    	//Add Filter
    	$cost->addFilter(new Zend_Filter_HtmlEntities());
    	$cost->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($cost);
    	
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors'));
    	$submitElement->setLabel('Create');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'));
    }


}