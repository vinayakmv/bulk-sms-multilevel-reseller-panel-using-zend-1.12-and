<?php

class Smpp_Form_Template extends Zend_Form
{

    public function init()
    {
    	$this->setAction('request');
    	$this->setMethod('post');
    	$this->setAttrib('class', "form-horizontal row-border");
        
        
    	        //dropdown type
        $user = new Zend_Form_Element_Select('user');
        $user->setDecorators(array('ViewHelper','Description'));

        $user->setRequired(TRUE);
        $user->class = "form-control select2 custom-select";
        $this->addElement($user);
        //text area
		$template = new Zend_Form_Element_Textarea('text');
        $template->setDecorators(array('ViewHelper','Description'));
        $template->setRequired(TRUE);
        $template->class = "form-control";
        
		$template	->setAttrib('cols', '40')
    				->setAttrib('rows', '4');
		$this->addElement($template);	
       
        //submit
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn  btn-primary ");
    	$submitElement->setDecorators(array('ViewHelper','Description'));
    	$submitElement->setLabel('Submit');
    	$this->setDecorators(array('FormElements','Form'));
    }


}

