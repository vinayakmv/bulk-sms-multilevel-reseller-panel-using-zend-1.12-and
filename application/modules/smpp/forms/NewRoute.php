<?php

class Smpp_Form_NewRoute extends Zend_Form
{

	public function init()
    {
        $this->setAction('create');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    
    	$username = new Zend_Form_Element_Text('route');
    	$username->setDecorators(array('ViewHelper','Description'));
    	$username->setLabel('Route');
    	$username->class = "form-control";
    	//$username->setAttrib('placeholder', 'SMSC ID');
    	//$username->addErrorMessage('Please enter a username');
    	$username->setRequired(true);
    
    	//Add Validator
    	$username->addValidator(new Zend_Validate_StringLength(4, 20));
    	    	
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//$username->addFilter(new Zend_Filter_StringToUpper());
    	//Add Username Element
    	$this->addElement($username);
    	
    	$name = new Zend_Form_Element_Text('name');
    	$name->setDecorators(array('ViewHelper','Description'));
    	$name->setLabel('Name');
    	$name->class = "form-control";
    	//$name->setAttrib('placeholder', 'Route Name');
    	//$username->addErrorMessage('Please enter a username');
    	$name->setRequired(true);
    	
    	//Add Validator
    	$name->addValidator(new Zend_Validate_StringLength(4, 20));
    	
    	//Add Filter
    	$name->addFilter(new Zend_Filter_HtmlEntities());
    	$name->addFilter(new Zend_Filter_StripTags());
    	$name->addFilter(new Zend_Filter_StringToUpper());
    	//Add Username Element
    	$this->addElement($name);
    	
    	//Create Password Object.
    	$selectRole = new Zend_Form_Element_Select('type');
		$roles = array("TRANSACTIONAL" => "TRANSACTIONAL",
					   "PROMOTIONAL" => "PROMOTIONAL",
					   "TRANS-SCRUB" => "TRANS-SCRUB",
				"INTERNATIONAL" => "INTERNATIONAL"
		);
		$selectRole->addMultiOptions($roles);
		$selectRole->setDecorators(array('ViewHelper','Description'));
		$selectRole->setLabel('Select Route');
		$selectRole->class = "form-control select2 custom-select";
		
		$selectRole->setRequired(true);

		$selectRole->addFilter(new Zend_Filter_HtmlEntities());
		$selectRole->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectRole);
    	    	
		$selectView = new Zend_Form_Element_MultiCheckbox('view');
		$views = array("SMS" => "Sms",
				"SMPP" => "Smpp");
		$selectView->addMultiOptions($views);
		$selectView->setDecorators(array('ViewHelper','Description'));
		$selectView->setLabel('Select Route');
		$selectView->class = "form-control select2 custom-select";

		//$selectView->setAttrib('data-plugin','switchery');
		$selectView->setRequired(false);
		
		$selectView->addFilter(new Zend_Filter_HtmlEntities());
		$selectView->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectView);
		
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description',
							    			'Errors'));
    	$submitElement->setLabel('Create');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'));
    }


}

