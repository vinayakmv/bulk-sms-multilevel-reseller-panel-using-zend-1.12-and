<?php

class Smpp_Form_Smsc extends Zend_Form
{

    public function init()
    {
        $this->setAction('status');
    	$this->setMethod('post');
    	$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
    	$this->setAttrib('class', "form-horizontal row-border");
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;')); 
    	
    	//Role Hidden element
    	$selectRole = new Zend_Form_Element_Select('smsc');
		$roles = array(null => "NO SMSC");
		$selectRole->addMultiOptions($roles);
		$selectRole->setDecorators(array('ViewHelper',
										'Description'));
		$selectRole->setLabel('Select Smsc');
		$selectRole->class = "form-control select2 custom-select";
		
		$selectRole->setRequired(true);
		//Add Validator
		//Add Filter
		$selectRole->addFilter(new Zend_Filter_HtmlEntities());
		$selectRole->addFilter(new Zend_Filter_StripTags());
		//Add Username Element
		$this->addElement($selectRole);


        $selectServer = new Zend_Form_Element_Select('server');
        $servers = array(null => "SELECT SERVER");
        $selectServer->addMultiOptions($servers);
        $selectServer->setDecorators(array('ViewHelper',
                                        'Description'));
        $selectServer->setLabel('Select Smsc');
        $selectServer->class = "form-control select2 custom-select";
        
        $selectServer->setRequired(true);
        //Add Validator
        //Add Filter
        $selectServer->addFilter(new Zend_Filter_HtmlEntities());
        $selectServer->addFilter(new Zend_Filter_StripTags());
        //Add Username Element
        $this->addElement($selectServer);

    	    	
    	$this->setDecorators(array('FormElements',
					    			'Form'));
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper',
    			'Description','Errors'));
    	$submitElement->setLabel('Show');
    	
    	$this->setDecorators(array('FormElements','Form'));
    }


}

