<?php
use modules\smpp\models\SmppSmscRetryTransManager;
use modules\smpp\models\SmppSmscRetryPromoManager;
use modules\user\models\SmppSpamManager;
use modules\user\models\UserManager;
use modules\user\models\ActivityLogManager;
use modules\smpp\models\SmppSpamRequestManager;
use modules\smpp\models\SmppSpamTransManager;
class Smpp_SpamController extends Smpp_Controller_BaseController
{

	public function init()
    {
    	$userManage = $this->view->navigation()->findOneByLabel('Spam');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }
   
    public function addAction()
    {
    	$this->view->subject="SMPP SPAM ADD";
    	$page = $this->view->navigation()->findOneByuri('/smpp/spam/add');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Smpp Sender');
    	}
    	
    	$form = new Smpp_Form_Spam();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	
    	/*
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	$transUsers =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId);
    	$smppRoutes = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>'TRANSACTIONAL'));
    	
    	foreach ($smppRoutes as $routes){
    		$route[$routes->route]=$routes->name;
    	}
    	
    	$user['all'] = "ALL";
    	foreach ($transUsers as $users)
    	{
    		$user[$users['system_id']]= $users['system_id'];
    	}
    	 
    	if (isset($user))
    	{
    		$form->getElement('user')->setMultiOptions($user);
    	}
    	if (isset($route))
    	{
    		$form->getElement('route')->setMultiOptions($route);
    	}
    	*/
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    	
    		$getValues = $form->getValues();
    		//$systemId=$getValues['user'];
    		//$getUserId =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId, $systemId);
    	
    		$SmppSpamRequestManager=new SmppSpamRequestManager();
    		$createSenderId = $SmppSpamRequestManager->createSpam($getValues, $userId, $emSmpp);
    	
    		if ($createSenderId) {
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Sender Route Created Successfully', $this->_domain, $this->_ip, $this->_device, $createSenderId->id, 'CREATE', 'SMPP SENDER ROUTE', $emUser);
    			
    			$this->view->success = 'sender route created';
    		}
    		else{
    			$this->view->errors = 'failed';
    		}
    	}else{
    		$this->view->errors = $form->getMessages();
    	}
    	
    	$form->reset();
    }
    
   
    
    public function viewAction()
    {
    	$this->view->subject="SMPP SPAM LIST";
    	
    	$page = $this->view->navigation()->findOneByuri('/smpp/spam/view');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Prefix Routes');
    	}
    	 
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$smppPrefixRouteRepo = $emSmpp->getRepository('modules\smpp\models\SmppSpamRequest')->findAll();
    
    	$this->view->entries = $smppPrefixRouteRepo;
    	 
    	$smppUsersTrans = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersTrans')->findAll();
        $smppUsersPromo = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersPromo')->findAll();
    	$smppUserTransIds = array();
    	$routes = array();
    	$esmes = '<option value="all">all</option>';
    	foreach ($smppUsersTrans as $smppUser){
    		$esmes .= '<option value="'.$smppUser->systemId.'|TRANSACTIONAL'.'">'.$smppUser->systemId.'|TRANSACTIONAL'.'</option>';
    	}
        foreach ($smppUsersPromo as $smppUser){
    		$esmes .= '<option value="'.$smppUser->systemId.'|PROMOTIONAL'.'">'.$smppUser->systemId.'|PROMOTIONAL'.'</option>';
    	}
    	$this->view->esmes = $esmes;
    	 
    	
    	 
    	if ($this->getRequest()->isPost()) {
    		 
    		$getValues = $this->getRequest()->getPost();
    		
    		$SmppSpamManager=new SmppSpamTransManager();
    		if ($getValues['submit'] == 'Activate'){
    			$createPrefix = $SmppSpamManager->insert($getValues, 'spam', $emSmpp);
    		}else{
    			$createPrefix = $SmppSpamManager->delete($getValues, 'spam', $emSmpp);
    		}
    		
    	
    		if ($createPrefix) {
                    
    			$this->view->success = 'Prefix route added';
    		}else{
    			$this->view->errors = 'Failed to add prefix route';
    		}
    	}
    }
    


    
}





