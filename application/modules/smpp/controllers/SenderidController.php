<?php

use modules\smpp\models\SmppSenderIdRequestManager;
use modules\user\models\ActivityLogManager;
class Smpp_SenderidController extends Smpp_Controller_BaseController
{

  public function init()
  {
    $broadcast = $this->view->navigation()->findOneByLabel('Senders');
    if ($broadcast) {
      $broadcast->setActive();
    }
    $action = $this->getRequest()->getActionName();
    $emUser = $this->getEntityManager('user');
    $domain = Smpp_Utility::getFqdn();
    $domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    $domainLayout = $domainRepo->layout;
    if ($domainLayout != null){
    	$view = $domainLayout.'-'.$action;
    	$this->_helper->viewRenderer("$view");
    }
    parent::init();
  }

  public function indexAction()
  {
    // action body
  }

  public function requestAction()
  {
    $this->view->subject="SENDER ID REQUEST";
    $page = $this->view->navigation()->findOneByuri('/smpp/senderid/request');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('Senders | Smpp Request');
    }

    $form = new Smpp_Form_Sender();
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    $emUser = $this->getEntityManager('user');
    $emSmpp = $this->getEntityManager('smpp');
    
    
    $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    $transUsers =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId);
    
     $user[0] = "Select";
        foreach ($transUsers as $users) 
        {
            $user[$users['system_id']]= $users['system_id'];
        }
           
        if (isset($user))
        {
            $form->getElement('user')->setMultiOptions($user);
        }
    $this->view->form = $form;

    if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {

      $getValues = $form->getValues();
      $systemId=$getValues['user'];
      $getUserId =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId, $systemId);
      
      $SmppSenderIdRequestManager=new SmppSenderIdRequestManager;
      $createSenderId = $SmppSenderIdRequestManager->createSenderId($getValues, $userId, $emSmpp);

      if (isset($createSenderId['Success'])) {
      	$activityManager = new ActivityLogManager();
      	$activityManager->createActivityLog($userRepo, $systemId, $this->_module, $this->_controller, $this->_action, "Smpp SenderId Requested Successfully", $this->_domain, $this->_ip, $this->_device, "DATA NOT AVAILABLE", 'REQUEST', 'SMPP SENDERID', $emUser);
      	
        $this->view->success = $createSenderId['Success'];
      }
      else{
        $this->view->errors = $createSenderId['ErrorCreated'];
      }
    }else{
      $this->view->errors = $form->getMessages();
    }
    
    $form->reset();
  }
  public function approvalAction()
  {

        $this->view->subject="SENDER ID APPROVAL";
    $page = $this->view->navigation()->findOneByuri('/smpp/senderid/approval');
    if ($page) {
      $page->setActive();
      $this->view->headTitle('Senders | Smpp Approval');
    }

    $emUser = $this->getEntityManager('user');
    $emSmpp = $this->getEntityManager('smpp');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    
    if ($this->getRequest()->isPost()) {

      $getValues = $this->getRequest()->getPost();

      $senderIdManager = new SmppSenderIdRequestManager();

      if(isset($getValues['approved'])){
        $manageSenderId = $senderIdManager->manageSenderId($getValues,'approved', $userId, $emSmpp);
        $activityManager = new ActivityLogManager();
        $activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, "Smpp SenderId Approved Successfully", $this->_domain, $this->_ip, $this->_device, $getValues['uniqId'], 'APPROVE', 'SMPP SENDERID', $emUser);
         
        $this->view->Success = array('approved' => array('success' => 'Approved successfully'));

      }
      else if (isset($getValues['Rejected'])) {
       $manageSenderId = $senderIdManager->manageSenderId($getValues,'Rejected', $userId, $emSmpp);
       $activityManager = new ActivityLogManager();
       $activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, "Smpp SenderId Rejected Successfully", $this->_domain, $this->_ip, $this->_device, $getValues['uniqId'], 'REJECT', 'SMPP SENDERID', $emUser);
       
       $this->view->Success = array('rejected' => array('success' => 'Rejected Successfully'));
     }

   }

   $queryBuilder = $emSmpp->createQueryBuilder();
   $queryBuilder->select('e')
   				->from('modules\smpp\models\SmppSenderIdRequest', 'e')
  				->where('e.status = ?1')
   				->orderBy('e.id', 'DESC')
   				->setParameter(1, '0');
   $query = $queryBuilder->getQuery();
   
   $this->view->entries = $query->getResult();
   $this->view->emUser = $emUser;
   /*
   $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
   $currentPage = 1;
  //$i = $this->_request->getParam('i');
   $i = $this->_request->getParam('p');
   if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i=='')
    {
      $this->view->pageIndex = $i;
    }
    else
    {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    } 

    $this->view->entries = $paginator;
    */
    $this->view->userId = $userId;
  }
  
  public function viewAction()
  {
  	$this->view->subject="SENDER ID VIEW";
  	$page = $this->view->navigation()->findOneByuri('/smpp/senderid/view');
  	if ($page) {
  		$page->setActive();
  		$this->view->headTitle('Senders | Smpp Senders');
  	}
  
  	//$reportManager = new SendSmsManager();
  	$emSmpp = $this->getEntityManager('smpp');
  	$userId = Zend_Auth::getInstance()->getIdentity()->id;
  	if ($this->getRequest()->isPost()) {
  
  		$getValues = $this->getRequest()->getPost();
  
  		$senderIdManager = new SmppSenderIdRequestManager();
  		if($getValues['submit'] == 'Delete'){
  			$manageSenderId = $senderIdManager->manageSenderId($getValues,'Delete', $userId, $emSmpp);
  			//$this->view->Success = array('approved' => array('success' => 'Approved successfully'));
  			 
  		}elseif ($getValues['submit'] == 'default') {
  			$manageSenderId = $senderIdManager->manageSenderId($getValues,'Default', $userId, $emSmpp);
  			//$this->view->Success = array('rejected' => array('success' => 'Rejected Successfully'));
  		}
  	}
  	$queryBuilder = $emSmpp->createQueryBuilder();
  	$queryBuilder->select('e')
  					->from('modules\smpp\models\SmppSenderIdRequest', 'e')
  					->where('e.user = ?1')
  					->andwhere('e.status != 5')
  					->orderBy('e.id', 'DESC')
  					->setParameter(1, $userId);
  	$query = $queryBuilder->getQuery();
  	$this->view->entries = $query->getResult();
  	/*
  	 $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
  	 $currentPage = 1;
  	 //$i = $this->_request->getParam('i');
  	 $i = $this->_request->getParam('p');
  	 if(!empty($i))
  	 { //Where i is the current page
  	 $currentPage = $this->_request->getParam('p');
  	 }
  	 $perPage = 10;
  	 $paginator->setCurrentPageNumber($currentPage)
  	 ->setItemCountPerPage($perPage);
  	 $this->view->result1 = $paginator;
  	 if($i==1 || $i=='')
  	 {
  	 $this->view->pageIndex = $i;
  	 }
  	 else
  	 {
  	 $this->view->pageIndex = (($i-1)*$perPage)+1;
  	 }
  
  	 $this->view->entries = $paginator;
  	*/
  	$this->view->userId = $userId;
  
  }
  public function viewallAction()
  {
  	$this->view->subject="SENDER ID VIEW";
  	$page =  $this->view->navigation()->findOneByuri('/smpp/senderid/viewall');
  	if ($page) {
  		$page->setActive();
  		$this->view->headTitle('Senders | All Smpp Senders');
  	}
  
  	//$reportManager = new SendSmsManager();
  	$emSmpp = $this->getEntityManager('smpp');
  	$userId = Zend_Auth::getInstance()->getIdentity()->id;
  	$queryBuilder = $emSmpp->createQueryBuilder();
  	$queryBuilder->select('e')
  				 	->from('modules\smpp\models\SmppSenderIdRequest', 'e')
  					->orderBy('e.id', 'DESC');
  	$query = $queryBuilder->getQuery();
  	$this->view->entries = $query->getResult();
  	/*
  	 $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
  	 $currentPage = 1;
  	 //$i = $this->_request->getParam('i');
  	 $i = $this->_request->getParam('p');
  	 if(!empty($i))
  	 { //Where i is the current page
  	 $currentPage = $this->_request->getParam('p');
  	 }
  	 $perPage = 10;
  	 $paginator->setCurrentPageNumber($currentPage)
  	 ->setItemCountPerPage($perPage);
  	 $this->view->result1 = $paginator;
  	 if($i==1 || $i=='')
  	 {
  	 $this->view->pageIndex = $i;
  	 }
  	 else
  	 {
  	 $this->view->pageIndex = (($i-1)*$perPage)+1;
  	 }
  
  	 $this->view->entries = $paginator;
  	*/
  	$this->view->userId = $userId;
  }


}







