<?php

use modules\smpp\models\SmppTemplateRequestManager;
use modules\user\models\ActivityLogManager;
use modules\smpp\models\SmppTemplateTransManager;
class Smpp_TemplateController   extends Smpp_Controller_BaseController
{

    public function init()
    {
      $broadcast = $this->view->navigation()->findOneByLabel('Templates');
      if ($broadcast) {
        $broadcast->setActive();
      }
      $action = $this->getRequest()->getActionName();
      $emUser = $this->getEntityManager('user');
      $domain = Smpp_Utility::getFqdn();
      $domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
      $domainLayout = $domainRepo->layout;
      if ($domainLayout != null){
      	$view = $domainLayout.'-'.$action;
      	$this->_helper->viewRenderer("$view");
      }
      parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function requestAction()
    {

       $this->view->subject="TEMPLATE REQUEST";
      $page = $this->view->navigation()->findOneByuri('/smpp/template/request');
      if ($page) {
          $page->setActive();
          $this->view->headTitle('Smpp Manage | Template Request');
      }

      $emSmpp = $this->getEntityManager('smpp');
      $emUser = $this->getEntityManager('user');
      
      $form=new Smpp_Form_Template();
      $this->view->form=$form;
      $userId = Zend_Auth::getInstance()->getIdentity()->id;
      
      $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
      $transUsers =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId);
      
      $user[0] = "Select";
      foreach ($transUsers as $users) 
      {
        $user[$users['system_id']]= $users['system_id'];
      }
           
      if (isset($user))
      {
        $form->getElement('user')->setMultiOptions($user);
      }
      $this->view->form = $form;
      if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        $templateDetail=$this->getRequest()->getPost();
        $systemId=$templateDetail['user'];
        if($systemId!='0')
        {
          $getUserId =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId, $systemId);
      
          $templateDetail['smppMainUser']=$userId;
          
          $SmppTemplateRequestManager=new SmppTemplateRequestManager();
          
          $addTemplate =$SmppTemplateRequestManager->addTemplate($templateDetail,$userId, $emSmpp);
          if(isset($addTemplate))
          {
          	$activityManager = new ActivityLogManager();
          	$activityManager->createActivityLog($userRepo, $systemId, $this->_module, $this->_controller, $this->_action, "Smpp Template Requested Successfully", $this->_domain, $this->_ip, $this->_device, "DATA NOT AVAILABLE", 'REQUEST', 'SMPP TEMPLATE', $emUser);
          	 
            $this->view->success = array('Template' => array('IsAdded' => 'Template requested successfully'));
             
          }
        }else{
            $this->view->error = array('Template' => array('noUser' => 'Please Select User'));
           
        }
        $form->reset();
      }
    }
    
    public function approvalAction()
    {
    	
       $this->view->subject="TEMPLATE APPROVAL";
      $page = $this->view->navigation()->findOneByuri('/smpp/template/approval');
      if ($page) {
        $page->setActive();
        $this->view->headTitle('Smpp Manage | Template Approval');
      }

          //$reportManager = new SendSmsManager();
      $emSmpp = $this->getEntityManager('smpp');
      $emUser = $this->getEntityManager('user');
      
      $userId = Zend_Auth::getInstance()->getIdentity()->id;
      $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
      
      if ($this->getRequest()->isPost()) {
        $templateDetail=$this->getRequest()->getPost();
        $templateRequestManager = new SmppTemplateRequestManager();
        $templateTransManager = new SmppTemplateTransManager();

        if(isset($templateDetail['approved'])){
          $id=$templateDetail['id'];
          $templateRepo = $emSmpp->getRepository('modules\smpp\models\SmppTemplateRequest')->find($id);
          $templateDetail['smppUser']=$templateRepo->systemId;
          $templateDetail['regex']=$templateRepo->name;
          
          $updateRequestTemplate =$templateRequestManager->updateRequestTemplate($templateDetail, $emSmpp);
          if($updateRequestTemplate){
              $insertTemplate =$templateTransManager->insertTemplate($templateDetail, $emSmpp);
              if(isset($insertTemplate)){
              	$activityManager = new ActivityLogManager();
              	$activityManager->createActivityLog($userRepo, $templateDetail['smppUser'], $this->_module, $this->_controller, $this->_action, "Smpp Template Approved Successfully", $this->_domain, $this->_ip, $this->_device, $templateDetail['id'], 'APPROVE', 'SMPP TEMPLATE', $emUser);
              	
                $this->view->success = array('Template' => array('isApproved' => 'Template approved successfully'));
              }

          }
          
        }elseif(isset($templateDetail['reject'])){
          $updateRequestTemplate =$templateRequestManager->updateRequestTemplate($templateDetail,$emSmpp);
          if(isset($updateRequestTemplate)){
            	$activityManager = new ActivityLogManager();
              	$activityManager->createActivityLog($userRepo, $templateDetail['smppUser'], $this->_module, $this->_controller, $this->_action, "Smpp Template Rejected Successfully", $this->_domain, $this->_ip, $this->_device, $templateDetail['id'], 'REJECT', 'SMPP TEMPLATE', $emUser);
              	
                $this->view->success = array('Template' => array('isRejected' => 'Template rejected successfully'));
          }
        }  
      }
      $queryBuilder = $emSmpp->createQueryBuilder();
      $queryBuilder->select('e')->from('modules\smpp\models\SmppTemplateRequest', 'e')
                                ->where('e.status = ?1')
                                ->orderBy('e.id', 'DESC')
                                ->setParameter(1, 0);
      $query = $queryBuilder->getQuery();
      $this->view->entries = $query->getResult();
      /*
      $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
      $currentPage = 1;
        //$i = $this->_request->getParam('i');
      $i = $this->_request->getParam('p');
      if(!empty($i))
      { //Where i is the current page
        $currentPage = $this->_request->getParam('p');
      }
      $perPage = 10;
      $paginator->setCurrentPageNumber($currentPage)
      ->setItemCountPerPage($perPage);
      $this->view->result1 = $paginator;
      if($i==1 || $i=='')
      {
        $this->view->pageIndex = $i;
      }
      else
      {
        $this->view->pageIndex = (($i-1)*$perPage)+1;
      }   

      $this->view->entries = $paginator;
      */
      $this->view->userId = $userId;
    }
    public function viewAction(){

       $this->view->subject="TEMPLATE VIEW";
      $page = $this->view->navigation()->findOneByuri('/smpp/template/view');
      if ($page) {
        $page->setActive();
        $this->view->headTitle('Smpp Manage | Template View');
      }

          //$reportManager = new SendSmsManager();
    	  $emUser = $this->getEntityManager('user');
	      $emSmpp = $this->getEntityManager('smpp');
	      $userId = Zend_Auth::getInstance()->getIdentity()->id;
	      $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
	      
	      if ($this->getRequest()->isPost()) {
		      	$templateDetail=$this->getRequest()->getPost();
		      	$templateRequestManager = new SmppTemplateRequestManager();
		      	$templateTransManager = new SmppTemplateTransManager();
		      	
			      if(isset($templateDetail['delete'])){
			      	
			      	$updateRequestTemplate =$templateRequestManager->updateRequestTemplate($templateDetail,$emSmpp);
			      	if(isset($updateRequestTemplate)){
			      		$insertTemplate =$templateTransManager->deleteTemplate($templateDetail, $emSmpp);
			      		if(isset($insertTemplate)){
				      		$activityManager = new ActivityLogManager();
				      		$activityManager->createActivityLog($userRepo, $templateDetail['smppUser'], $this->_module, $this->_controller, $this->_action, "Smpp Template Rejected Successfully", $this->_domain, $this->_ip, $this->_device, $templateDetail['template'], 'DELETE', 'SMPP TEMPLATE', $emUser);
				      		 
				      		$this->view->success = array('Template' => array('isDeleted' => 'Template deleted successfully'));
			      		}
			      	}
			      	
			      }
	      }
     

        
      $queryBuilder = $emSmpp->createQueryBuilder();
      $queryBuilder->select('e')->from('modules\smpp\models\SmppTemplateRequest', 'e')
                                ->where('e.user = ?1')
                                ->orderBy('e.id', 'DESC')
                                ->setParameter(1, $userId);
      $query = $queryBuilder->getQuery();
      $this->view->entries = $query->getResult();
      /*
      $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
      $currentPage = 1;
        //$i = $this->_request->getParam('i');
      $i = $this->_request->getParam('p');
      if(!empty($i))
      { //Where i is the current page
        $currentPage = $this->_request->getParam('p');
      }
      $perPage = 10;
      $paginator->setCurrentPageNumber($currentPage)
      ->setItemCountPerPage($perPage);
      $this->view->result1 = $paginator;
      if($i==1 || $i=='')
      {
        $this->view->pageIndex = $i;
      }
      else
      {
        $this->view->pageIndex = (($i-1)*$perPage)+1;
      }   

      $this->view->entries = $paginator;
      */
      $this->view->userId = $userId;
    
    }
    public function viewallAction(){
      
       $this->view->subject="TEMPLATE VIEW ALL";
      $page = $this->view->navigation()->findOneByuri('/smpp/template/viewall');
      if ($page) {
        $page->setActive();
        $this->view->headTitle('Smpp Manage | Template View All');
      }

          //$reportManager = new SendSmsManager();
	      $emUser = $this->getEntityManager('user');
	      $emSmpp = $this->getEntityManager('smpp');
	      $userId = Zend_Auth::getInstance()->getIdentity()->id;
	      $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
	      
	      if ($this->getRequest()->isPost()) {
		      	$templateDetail=$this->getRequest()->getPost();
		      	$templateRequestManager = new SmppTemplateRequestManager();
		      	$templateTransManager = new SmppTemplateTransManager();
		      	
			      if(isset($templateDetail['delete'])){
			      	
			      	$updateRequestTemplate =$templateRequestManager->updateRequestTemplate($templateDetail,$emSmpp);
			      	if(isset($updateRequestTemplate)){
			      		$insertTemplate =$templateTransManager->deleteTemplate($templateDetail, $emSmpp);
			      		if(isset($insertTemplate)){
				      		$activityManager = new ActivityLogManager();
				      		$activityManager->createActivityLog($userRepo, $templateDetail['smppUser'], $this->_module, $this->_controller, $this->_action, "Smpp Template Rejected Successfully", $this->_domain, $this->_ip, $this->_device, $templateDetail['template'], 'DELETE', 'SMPP TEMPLATE', $emUser);
				      		 
				      		$this->view->success = array('Template' => array('isDeleted' => 'Template deleted successfully'));
			      		}
			      	}
			      	
			      }
	      }
      $queryBuilder = $emSmpp->createQueryBuilder();
      $queryBuilder->select('e')->from('modules\smpp\models\SmppTemplateRequest', 'e')
                                ->orderBy('e.id', 'DESC');
      $query = $queryBuilder->getQuery();
      $this->view->entries = $query->getResult();
      /*
      $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
      $currentPage = 1;
        //$i = $this->_request->getParam('i');
      $i = $this->_request->getParam('p');
      if(!empty($i))
      { //Where i is the current page
        $currentPage = $this->_request->getParam('p');
      }
      $perPage = 10;
      $paginator->setCurrentPageNumber($currentPage)
      ->setItemCountPerPage($perPage);
      $this->view->result1 = $paginator;
      if($i==1 || $i=='')
      {
        $this->view->pageIndex = $i;
      }
      else
      {
        $this->view->pageIndex = (($i-1)*$perPage)+1;
      }   

      $this->view->entries = $paginator;
      */
      $this->view->userId = $userId;
    
    }

/*public function viewAction()
{
    $em = $this->getEntityManager('smppit');
    $em = $this->getEntityManager('user');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;

    $usersRepo = $em->getRepository('Models\Users')->find($userId);
    if($usersRepo->userType=='admin')
    { 
        $condition="";
    }
    else
    {
        $condition= "AND ub_id='$userId'";
    }

    $queryBuilder = $em->createQueryBuilder();
   $queryBuilder->select('e')
   ->from('Models\SmppSenderIdRequest', 'e')
   ->where('e.status = ?1')
   ->orderBy('e.senderId', 'DESC')
   ->setParameter(1, '0');
   $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
   $currentPage = 1;
  //$i = $this->_request->getParam('i');
   $i = $this->_request->getParam('p');
   if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
    }
    $perPage = 10;
    $paginator->setCurrentPageNumber($currentPage)
    ->setItemCountPerPage($perPage);
    $this->view->result1 = $paginator;
    if($i==1 || $i=='')
    {
      $this->view->pageIndex = $i;
    }
    else
    {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
    } 

    $this->view->entries = $paginator;
    $this->view->userId = $userId;

    }

    public function requestAction()
    {
        $em = $this->getEntityManager('smppit');
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        if ($this->getRequest()->isPost())
        {
            $templateDetail=$this->getRequest()->getPost();
           // print_r($templateDetail);
            if(isset($templateDetail['approved']))
            {
                $updateRequestTemplate =$em->getRepository('Models\SmppTemplateRequest')->updateRequestTemplate($templateDetail,$userId);
                if(isset($updateRequestTemplate))
                {
                    $insertTemplate =$em->getRepository('Models\SmppTemplate')->insertTemplate($templateDetail);
                    if(isset($insertTemplate))
                    {
                        ?>
                        <body onLoad="alertSuccess('Template approved successfully','/smppit/template/request');"></body>
                        <?php
                    }

                }
            }
            else if(isset($templateDetail['rejected']))
            {
                $updateRequestTemplate =$em->getRepository('Models\SmppTemplateRequest')->updateRequestTemplate($templateDetail,$userId);
                if(isset($updateRequestTemplate))
                {

                    ?>
                    <body onLoad="alertSuccess('Template reject successfully','/smppit/template/request');"></body>
                    <?php
                }
            }

        }

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('e')
        ->from('Models\SmppSenderIdRequest', 'e')
        ->where('e.status = ?1')
        ->orderBy('e.senderId', 'DESC')
        ->setParameter(1, '0');
        $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
        $currentPage = 1;
  //$i = $this->_request->getParam('i');
        $i = $this->_request->getParam('p');
        if(!empty($i))
    { //Where i is the current page
      $currentPage = $this->_request->getParam('p');
  }
  $perPage = 10;
  $paginator->setCurrentPageNumber($currentPage)
  ->setItemCountPerPage($perPage);
  $this->view->result1 = $paginator;
  if($i==1 || $i=='')
  {
      $this->view->pageIndex = $i;
  }
  else
  {
      $this->view->pageIndex = (($i-1)*$perPage)+1;
  } 

  $this->view->entries = $paginator;
  $this->view->userId = $userId;
}
*/

}