<?php
use modules\smpp\models\SmppReportManager;
use modules\smpp\models\SmppUserPromoManager;
use modules\smpp\models\SmppUserTransManager;
use modules\smpp\models\SmppUserInterManager;
use modules\user\models\ActivityLogManager;
class Smpp_EsmeController extends Smpp_Controller_BaseController
{

    public function init()
    {
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function createAction()
    {

        $this->view->subject="CREATE ESME USER";
    	$userManage = $this->view->navigation()->findOneByLabel('Smpp');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$page = $this->view->navigation()->findOneByUri('/smpp/esme/create');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Smpp | Create');
    	}

    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	
        $form = new Smpp_Form_NewSmpp();
        $smsRouteRepo = $userRepo->smppRoutes;
    	foreach ($smsRouteRepo as $routes) {    		
    		$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".'|'."$routes->name";
    	}
    	if (empty($route)){
    		$trans = $userRepo->domain->smppTransRoute;
    		$promo = $userRepo->domain->smppPromoRoute;
    		$scrub = $userRepo->domain->smppScrubRoute;
    		$inter = $userRepo->domain->smppInterRoute;
    		if ($trans){
    			$routes = $emUser->getRepository('modules\user\models\smppRoute')->findOneBy(array('id'=>$trans,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".'|'."$routes->name";
    			}
    		}
    		if ($promo){
    			$routes = $emUser->getRepository('modules\user\models\smppRoute')->findOneBy(array('id'=>$promo,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".'|'."$routes->name";
    			}
    		}
    		if ($scrub){
    			$routes = $emUser->getRepository('modules\user\models\smppRoute')->findOneBy(array('id'=>$scrub,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".'|'."$routes->name";
    			}
    		}
    		if ($inter){
    			$routes = $emUser->getRepository('modules\user\models\smppRoute')->findOneBy(array('id'=>$inter,'status'=>'1'));
    			if ($routes){
    				$route[$routes->type.'|'.$routes->route.'|'.$routes->name] = "$routes->type".'|'."$routes->name";
    			}
    		}
    	}
        if (isset($route)) {
        	$form->getElement('route')->setMultiOptions($route);
        }
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        	$smppValues = $form->getValues();   
        	$userId = Zend_Auth::getInstance()->getIdentity()->id;
        	$routeArray = $smppValues['route'];
        	$route = explode('|', $routeArray);
        	if ($route['0'] == 'TRANSACTIONAL' || $route['0'] == 'TRANS-SCRUB'){
        		$smppManager = new SmppUserTransManager();
        	}elseif ($route['0'] == 'PROMOTIONAL'){
        		$smppManager = new SmppUserPromoManager();
        	}elseif ($route['0'] == 'INTERNATIONAL'){
        		$smppManager = new SmppUserInterManager();
        	}
        	$smppCreated = $smppManager->createSmppUser($smppValues, $userId, $emUser, $emSmpp);
        	
        	if (isset($smppCreated['Esme']['isFailed'])) {
        		$this->view->errors = array('user' => array('isFailed' => 'Esme already exists'));
        	}elseif (isset($smppCreated['Esme']['isSuccess'])){
        		$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
        		$esmeRepo = $smppCreated['Esme']['data'];
        		$activityManager = new ActivityLogManager();
        		$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Esme Created Successfully', $this->_domain, $this->_ip, $this->_device, $smppValues['username'].'|'.$route['0'], 'CREATE', 'ESME USER', $emUser);
        			
        		$this->view->success = array('user' => array('isSuccess' => 'Esme user created'));
        	}
        }else {
        	$this->view->errors = $form->getMessages();     
        }   
        //$form->reset();
    	
    }

    public function esmesAction()
    {

        $this->view->subject="ESME LIST";
    	$userManage = $this->view->navigation()->findOneByLabel('Smpp');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$page = $this->view->navigation()->findOneByUri('/smpp/esme/esmes');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Smpp | Esmes');
    	}
    	
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');

    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	// Block,Unblock,Activate Users,Resellers
    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
             $button = $this->getRequest()->getPost();

             if (isset($button['edit'])) {
                //Block User
                if ($button['type'] == 'TRANSACTIONAL' || $button['type'] == 'TRANS-SCRUB'){
                     $serverType = 'TRANS-SMPP';
                    $smppManager = new SmppUserTransManager();
                }elseif ($button['type'] == 'PROMOTIONAL'){
                     $serverType = 'PROMO-SMPP';
                    $smppManager = new SmppUserPromoManager();
                }elseif ($button['type'] == 'INTERNATIONAL'){
                    $serverType = 'INTER-SMPP';
                    $smppManager = new SmppUserInterManager();
                }

                $edited =  $smppManager->editUser($button['edit'], $button['ip'], FALSE, FALSE, FALSE, $emSmpp);
                if ($edited){
                    $activityManager = new ActivityLogManager();
                    $activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Esme Edited Successfully', $this->_domain, $this->_ip, $this->_device, $systemId.'|'.$route['0'], 'EDIT', 'ESME USER', $emUser);
                               
                }
                         
                        
             }

             
            if (isset($button['active'])) {
                //Block User
                if ($button['type'] == 'TRANSACTIONAL' || $button['type'] == 'TRANS-SCRUB'){
                     $serverType = 'TRANS-SMPP';
                    $smppManager = new SmppUserTransManager();
                }elseif ($button['type'] == 'PROMOTIONAL'){
                     $serverType = 'PROMO-SMPP';
                    $smppManager = new SmppUserPromoManager();
                }elseif ($button['type'] == 'INTERNATIONAL'){
                    $serverType = 'INTER-SMPP';
                    $smppManager = new SmppUserInterManager();
                }

                $reset = $smppManager->blockUser($button['active'], $emSmpp);
                $systemId = $button['active'];
                if (isset($reset)) {
                    $this->setErrorHandler();
                    try {
                        //$send = file_get_contents("http://127.0.0.1:10660/unbind?password=VEv0xT3l&system-id=$systemId");
                        $activityManager = new ActivityLogManager();
                        $activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Esme Blocked Successfully', $this->_domain, $this->_ip, $this->_device, $systemId.'|'.$route['0'], 'BLOCK', 'ESME USER', $emUser);
                         
                        $this->view->success = 'SUCCESS';
                    }
                    catch(Exception $e) {
                        $this->view->errors = $e->getMessage();
                    }
                    restore_error_handler();
                }else {
                    $this->view->errors = 'FAILED';
                }
            }
             
            if (isset($button['blocked'])) {
                //Unblock User
                if ($button['type'] == 'TRANSACTIONAL' || $button['type'] == 'TRANS-SCRUB'){
                     $serverType = 'TRANS-SMPP';
                    $smppManager = new SmppUserTransManager();
                }elseif ($button['type'] == 'PROMOTIONAL'){
                     $serverType = 'PROMO-SMPP';
                    $smppManager = new SmppUserPromoManager();
                }elseif ($button['type'] == 'INTERNATIONAL'){
                    $serverType = 'INTER-SMPP';
                    $smppManager = new SmppUserInterManager();
                }
                $reset = $smppManager->unblockUser($button['blocked'], $emSmpp);
                $systemId = $button['blocked'];
                if (isset($reset)) {
                    $this->setErrorHandler();
                    try {
                        //$send = file_get_contents("http://127.0.0.1:10660/unbind?password=VEv0xT3l&system-id=$systemId");
                        $this->view->success = 'SUCCESS';
                        $activityManager = new ActivityLogManager();
                        $activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Esme UnBlocked Successfully', $this->_domain, $this->_ip, $this->_device, $systemId.'|'.$route['0'], 'UNBLOCK', 'ESME USER', $emUser);
                        
                    }
                    catch(Exception $e) {
                        $this->view->errors = $e->getMessage();
                    }
                    restore_error_handler();
                }else {
                    $this->view->errors = 'FAILED';
                }
            }
    
            if (isset($button['delete'])) {
                //Delete User
                $smppManager = new SmppUserTransManager();
                $reset = $smppManager->deleteUser($button['delete'], $userId, $emUser);
                $systemId = $button['delete'];
                $activityManager = new ActivityLogManager();
                $activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Esme Deleted Successfully', $this->_domain, $this->_ip, $this->_device, $systemId.'|'.$route['0'], 'DELETE', 'ESME USER', $emUser);
                        
            }


                $serverRepo = $emSmpp->getRepository('modules\smpp\models\SmppServer')->findBy(array('type' => $serverType));
                $type = $serverRepo->type;
                $ip = $serverRepo->ip;
                $port = $serverRepo->port;
                $password = $serverRepo->password;

                file_get_contents("http://$ip:$port/unbind?password=$password&system-id=$systemId");
             
        }
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$smppUsersTrans = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersTrans')->findBy(array('user' => $userId));
    	$smppUsersPromo = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersPromo')->findBy(array('user' => $userId));
    	$smppUsersInter = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersInter')->findBy(array('user' => $userId));
    	 
    	foreach ($smppUsersTrans as $smppUser){
    		$smppUserTransIds[] = "'".$smppUser->systemId."'";
    	}
    	foreach ($smppUsersPromo as $smppUser){
    		$smppUserPromoIds[] = "'".$smppUser->systemId."'";
    	}
    	foreach ($smppUsersInter as $smppUser){
    		$smppUserInterIds[] = "'".$smppUser->systemId."'";
    	}
    	 
    	if (@$smppUserTransIds){
    		$trans = implode(",", $smppUserTransIds);
    		$queryEsme = "SELECT * FROM smpp_users_trans WHERE system_id IN ($trans)";
                $connection = $emSmpp->getConnection();
    		$query = $connection->prepare("$queryEsme");
    		$query->execute();
    		$resultTrans = $query->fetchAll();
                $this->view->Transentries = $resultTrans;
    	}else{
            $resultTrans = array();
        }
        if (@$smppUserPromoIds){
    		$promo = implode(",", $smppUserPromoIds);
    		$queryEsme = "SELECT * FROM smpp_users_promo WHERE system_id IN ($promo)";
                $connection = $emSmpp->getConnection();
    		$query = $connection->prepare("$queryEsme");
    		$query->execute();
    		$resultPromo = $query->fetchAll();
                $this->view->Promoentries = $resultPromo;
    	}else{
            $resultPromo = array();
        }
        if (@$smppUserInterIds){
    		$inter = implode(",", $smppUserInterIds);
    		$queryEsme = "SELECT * FROM smpp_users_promo WHERE system_id IN ($inter)";
                $connection = $emSmpp->getConnection();
    		$query = $connection->prepare("$queryEsme");
    		$query->execute();
    		$resultInter = $query->fetchAll();
                $this->view->Interentries = $resultInter;
    	}else{
                $resultInter = array();
        }
    	/*
    	if (@$smppUserTransIds && @$smppUserPromoIds){
    		$trans = implode(",", $smppUserTransIds);
    		$promo = implode(",", $smppUserPromoIds);
    		$queryEsme = $queryEsme.' UNION '."SELECT * FROM smpp_users_promo WHERE system_id IN ($promo)";
    	}elseif(@$smppUserPromoIds){
    		$promo = implode(",", $smppUserPromoIds);
    		$queryEsme = "SELECT * FROM smpp_users_promo WHERE system_id IN ($promo)";
    	}
    	
    	if (@$smppUserTransIds && @$smppUserPromoIds && @$smppUserInterIds){
    		$trans = implode(",", $smppUserTransIds);
    		$promo = implode(",", $smppUserPromoIds);
    		$inter = implode(",", $smppUserInterIds);
    		$queryEsme = $queryEsme.' UNION '."SELECT * FROM smpp_users_inter WHERE system_id IN ($inter)";
    	}elseif (@$smppUserTransIds && @$smppUserInterIds){
    		$trans = implode(",", $smppUserTransIds);
    		$inter = implode(",", $smppUserInterIds);
    		$queryEsme = $queryEsme.' UNION '."SELECT * FROM smpp_users_inter WHERE system_id IN ($inter)";
    	}elseif (@$smppUserPromoIds && @$smppUserInterIds){
    		$promo = implode(",", $smppUserPromoIds);
    		$inter = implode(",", $smppUserInterIds);
    		$queryEsme = $queryEsme.' UNION '."SELECT * FROM smpp_users_inter WHERE system_id IN ($inter)";
    	}elseif (@$smppUserInterIds){
    		$inter = implode(",", $smppUserInterIds);
    		$queryEsme = "SELECT * FROM smpp_users_inter WHERE system_id IN ($inter)";
    	}
    	
    	if ((@$smppUserTransIds) || (@$smppUserPromoIds) || (@$smppUserInterIds)){
    		$connection = $emSmpp->getConnection();
    		$query = $connection->prepare("$queryEsme");
    		$query->execute();
    		$result = $query->fetchAll();
    	}else{
    		$result = array();
    	}
    	*/
        
    	$this->view->entries = array_merge($resultTrans, $resultPromo,$resultInter);
    }
    
    public function allesmesAction()
    {
    
    	$this->view->subject="ESME LIST ALL";
    	$userManage = $this->view->navigation()->findOneByLabel('Smpp');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$page = $this->view->navigation()->findOneByUri('/smpp/esme/allesmes');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Smpp | All Esme');
    	}
    	 
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$this->view->emUser = $emUser ;
    	
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	// Block,Unblock,Activate Users,Resellers
    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		 $button = $this->getRequest()->getPost();

    		 if (isset($button['edit'])) {
                //Block User
                if ($button['type'] == 'TRANSACTIONAL' || $button['type'] == 'TRANS-SCRUB'){
                     $serverType = 'TRANS-SMPP';
                    $smppManager = new SmppUserTransManager();
                }elseif ($button['type'] == 'PROMOTIONAL'){
                     $serverType = 'PROMO-SMPP';
                    $smppManager = new SmppUserPromoManager();
                }elseif ($button['type'] == 'INTERNATIONAL'){
                    $serverType = 'INTER-SMPP';
                    $smppManager = new SmppUserInterManager();
                }

                $edited =  $smppManager->editUser($button['edit'], $button['ip'], $button['smsc'], $button['tps'], $button['bind'],$button['simulate_deliver_every'], $button['simulate_permanent_failure_every'], $emSmpp);
                if ($edited){
                    $activityManager = new ActivityLogManager();
                    $activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Esme Edited Successfully', $this->_domain, $this->_ip, $this->_device, $button['edit'].'|'.$button['type'], 'EDIT', 'ESME USER', $emUser);
                               
                }
                         
                        
             }

             
    		if (isset($button['active'])) {
    			//Block User
    			if ($button['type'] == 'TRANSACTIONAL' || $button['type'] == 'TRANS-SCRUB'){
                     $serverType = 'TRANS-SMPP';
    				$smppManager = new SmppUserTransManager();
    			}elseif ($button['type'] == 'PROMOTIONAL'){
                     $serverType = 'PROMO-SMPP';
    				$smppManager = new SmppUserPromoManager();
    			}elseif ($button['type'] == 'INTERNATIONAL'){
                    $serverType = 'INTER-SMPP';
    				$smppManager = new SmppUserInterManager();
    			}

    			$reset = $smppManager->blockUser($button['active'], $emSmpp);
    			$systemId = $button['active'];
    			if (isset($reset)) {
    				$this->setErrorHandler();
    				try {
    					//$send = file_get_contents("http://127.0.0.1:10660/unbind?password=VEv0xT3l&system-id=$systemId");
    					$activityManager = new ActivityLogManager();
    					$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Esme Blocked Successfully', $this->_domain, $this->_ip, $this->_device, $systemId.'|'.$route['0'], 'BLOCK', 'ESME USER', $emUser);
    					 
    					$this->view->success = 'SUCCESS';
    				}
    				catch(Exception $e) {
    					$this->view->errors = $e->getMessage();
    				}
    				restore_error_handler();
    			}else {
    				$this->view->errors = 'FAILED';
    			}
    		}
    		 
    		if (isset($button['blocked'])) {
    			//Unblock User
    			if ($button['type'] == 'TRANSACTIONAL' || $button['type'] == 'TRANS-SCRUB'){
                     $serverType = 'TRANS-SMPP';
                    $smppManager = new SmppUserTransManager();
                }elseif ($button['type'] == 'PROMOTIONAL'){
                     $serverType = 'PROMO-SMPP';
                    $smppManager = new SmppUserPromoManager();
                }elseif ($button['type'] == 'INTERNATIONAL'){
                    $serverType = 'INTER-SMPP';
                    $smppManager = new SmppUserInterManager();
                }
    			$reset = $smppManager->unblockUser($button['blocked'], $emSmpp);
    			$systemId = $button['blocked'];
    			if (isset($reset)) {
    				$this->setErrorHandler();
    				try {
    					//$send = file_get_contents("http://127.0.0.1:10660/unbind?password=VEv0xT3l&system-id=$systemId");
    					$this->view->success = 'SUCCESS';
                        $activityManager = new ActivityLogManager();
                        $activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Esme UnBlocked Successfully', $this->_domain, $this->_ip, $this->_device, $systemId.'|'.$route['0'], 'UNBLOCK', 'ESME USER', $emUser);
                        
    				}
    				catch(Exception $e) {
    					$this->view->errors = $e->getMessage();
    				}
    				restore_error_handler();
    			}else {
    				$this->view->errors = 'FAILED';
    			}
    		}
    
    		if (isset($button['delete'])) {
    			//Delete User
    			$smppManager = new SmppUserTransManager();
    			$reset = $smppManager->deleteUser($button['delete'], $userId, $emUser);
                $systemId = $button['delete'];
                $activityManager = new ActivityLogManager();
                $activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Esme Deleted Successfully', $this->_domain, $this->_ip, $this->_device, $systemId.'|'.$route['0'], 'DELETE', 'ESME USER', $emUser);
                        
    		}


                $serverRepo = $emSmpp->getRepository('modules\smpp\models\SmppServer')->findBy(array('type' => $serverType));
                $type = $serverRepo->type;
                $ip = $serverRepo->ip;
                $port = $serverRepo->port;
                $password = $serverRepo->password;

                file_get_contents("http://$ip:$port/unbind?password=$password&system-id=$systemId");
    		 
    	}

    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$smppUsersTrans = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersTrans')->findAll();
    	$smppUsersPromo = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersPromo')->findAll();
    	$smppUsersInter = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersInter')->findAll();
    	
    	foreach ($smppUsersTrans as $smppUser){
            $smppUserTransIds[] = "'".$smppUser->systemId."'";
        }
        foreach ($smppUsersPromo as $smppUser){
            $smppUserPromoIds[] = "'".$smppUser->systemId."'";
        }
        foreach ($smppUsersInter as $smppUser){
            $smppUserInterIds[] = "'".$smppUser->systemId."'";
        }
        
        if (@$smppUserTransIds){
    		$trans = implode(",", $smppUserTransIds);
    		$queryEsme = "SELECT * FROM smpp_users_trans WHERE system_id IN ($trans)";
                $connection = $emSmpp->getConnection();
    		$query = $connection->prepare("$queryEsme");
    		$query->execute();
    		$resultTrans = $query->fetchAll();
                $this->view->Transentries = $resultTrans;
    	}else{
            $resultTrans = array();
        }
        if (@$smppUserPromoIds){
    		$promo = implode(",", $smppUserPromoIds);
    		$queryEsme = "SELECT * FROM smpp_users_promo WHERE system_id IN ($promo)";
                $connection = $emSmpp->getConnection();
    		$query = $connection->prepare("$queryEsme");
    		$query->execute();
    		$resultPromo = $query->fetchAll();
                $this->view->Promoentries = $resultPromo;
    	}else{
            $resultPromo = array();
        }
        if (@$smppUserInterIds){
    		$inter = implode(",", $smppUserInterIds);
    		$queryEsme = "SELECT * FROM smpp_users_promo WHERE system_id IN ($inter)";
                $connection = $emSmpp->getConnection();
    		$query = $connection->prepare("$queryEsme");
    		$query->execute();
    		$resultInter = $query->fetchAll();
                $this->view->Interentries = $resultInter;
    	}else{
                $resultInter = array();
        }
        /* 
        if (@$smppUserTransIds){
            $trans = implode(",", $smppUserTransIds);
            $queryEsme = "SELECT * FROM smpp_users_trans WHERE system_id IN ($trans)";
        }
        
        if (@$smppUserTransIds && @$smppUserPromoIds){
            $trans = implode(",", $smppUserTransIds);
            $promo = implode(",", $smppUserPromoIds);
            $queryEsme = $queryEsme.' UNION '."SELECT * FROM smpp_users_promo WHERE system_id IN ($promo)";
        }elseif(@$smppUserPromoIds){
            $promo = implode(",", $smppUserPromoIds);
            $queryEsme = "SELECT * FROM smpp_users_promo WHERE system_id IN ($promo)";
        }
        
        if (@$smppUserTransIds && @$smppUserPromoIds && @$smppUserInterIds){
            $trans = implode(",", $smppUserTransIds);
            $promo = implode(",", $smppUserPromoIds);
            $inter = implode(",", $smppUserInterIds);
            $queryEsme = $queryEsme.' UNION '."SELECT * FROM smpp_users_inter WHERE system_id IN ($inter)";
        }elseif (@$smppUserTransIds && @$smppUserInterIds){
            $trans = implode(",", $smppUserTransIds);
            $inter = implode(",", $smppUserInterIds);
            $queryEsme = $queryEsme.' UNION '."SELECT * FROM smpp_users_inter WHERE system_id IN ($inter)";
        }elseif (@$smppUserPromoIds && @$smppUserInterIds){
            $promo = implode(",", $smppUserPromoIds);
            $inter = implode(",", $smppUserInterIds);
            $queryEsme = $queryEsme.' UNION '."SELECT * FROM smpp_users_inter WHERE system_id IN ($inter)";
        }elseif (@$smppUserInterIds){
            $inter = implode(",", $smppUserInterIds);
            $queryEsme = "SELECT * FROM smpp_users_inter WHERE system_id IN ($inter)";
        }
        
        if ((@$smppUserTransIds) || (@$smppUserPromoIds) || (@$smppUserInterIds)){
            $connection = $emSmpp->getConnection();
            $query = $connection->prepare("$queryEsme");
            $query->execute();
            $result = $query->fetchAll();
        }else{
            $result = array();
        }
        */ 
        $this->view->entries = array_merge($resultTrans, $resultPromo,$resultInter);
    	
    }

    public function statusAction()
    {

    	$this->view->subject="MONITOR";
    	$userManage = $this->view->navigation()->findOneByLabel('Monitor');
    	if ($userManage) {
    		$userManage->setActive();
    	}    
        $page = $this->view->navigation()->findOneByUri('/smpp/esme/status');
    	if ($page && $userManage) {
    		$userManage->setActive();
    		$page->setActive();
    		$this->view->headTitle('Monitor | Esme Status');
    	}
    	$form = new Smpp_Form_Smsc();
        $emUser = $this->getEntityManager('user');
        $emSmpp = $this->getEntityManager('smpp');
        $smscRepos = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('status' => '1'));
        foreach ($smscRepos as $smsc) {
            $smscs[$smsc->route] = $smsc->route;
        }
        if (isset($smscs)) {
            $form->getElement('smsc')->setMultiOptions($smscs);
        }

        $serverRepos = $emSmpp->getRepository('modules\smpp\models\SmppServer')->findBy(array('type' => 'TRANS-SMPP'));
        $servers['0'] = 'SELECT SERVER';
        foreach ($serverRepos as $server) {
            $servers[$server->id] = $server->name;
        }

        $serverPromoRepos = $emSmpp->getRepository('modules\smpp\models\SmppServer')->findBy(array('type' => 'PROMO-SMPP'));
        foreach ($serverPromoRepos as $server) {
            $servers[$server->id] = $server->name;
        }

        $serverInterRepos = $emSmpp->getRepository('modules\smpp\models\SmppServer')->findBy(array('type' => 'INTER-SMPP'));
        foreach ($serverInterRepos as $server) {
            $servers[$server->id] = $server->name;
        }

        if (isset($servers)) {
            $form->getElement('server')->setMultiOptions($servers);
        }

    	
    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
            $this->view->server = $button['server'];
            $form->getElement('server')->setValue($button['server']);
            $serverRepo = $emSmpp->getRepository('modules\smpp\models\SmppServer')->find($button['server']);
            $type = $serverRepo->type;
            $ip = $serverRepo->ip;
            $port = $serverRepo->port;
            $password = $serverRepo->password;
            $kannelConf = $serverRepo->kannelConf;
            $sqlboxConf = $serverRepo->sqlboxConf;  
            $vsmppboxConf = $serverRepo->vsmppboxConf;  

           if ($type == 'TRANS-SMPP'){
                $this->view->emSmpp = $emSmpp->getRepository('modules\smpp\models\SmppUserTrans');
           }elseif($type == 'PROMO-SMPP'){
                $this->view->emSmpp = $emSmpp->getRepository('modules\smpp\models\SmppUserPromo');
           }else{
                $this->view->emSmpp = $emSmpp->getRepository('modules\smpp\models\SmppUserInter');
           }

    		switch ($button['command']) {
    			case "start":
    				shell_exec("/usr/local/sbin/vsmppbox -v 4 /usr/local/sbin/$vsmppboxConf > /dev/null 2> /dev/null &");
                    sleep (10);
    				//$this->_redirect('/smpp/esme/status');
    				break;
    			case "shutdown":
    				shell_exec("/bin/kill -9 $(ps -ef | grep $vsmppboxConf | grep -v grep | awk '{print $2}')");
    				//$this->_redirect('/smpp/esme/status');
    				break;
                case "unbind":
                    $systemId = $button['system_id'];
                    file_get_contents("http://$ip:$port/unbind?system-id=$systemId&password=$password");
                    sleep (10);
                    //$this->_redirect('/smpp/esme/status');
                    break;
    		}

             $this->view->status = file_get_contents("http://$ip:$port/status.xml?password=$password");

    	}
        
    	$ip = $this->_request->getServer('SERVER_ADDR');
    	$this->view->form = $form;
    }

    public function sentsmsAction()
    {
        
        $this->view->subject="SENT SMS";
    	$userManage = $this->view->navigation()->findOneByLabel('Transactions');
    	if ($userManage) {
    		$userManage->setActive();
    	}
        $page = $this->view->navigation()->findOneByUri('/smpp/esme/sentsms');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Smpp | Sent Log');
    	}
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	
    	$form = new Smpp_Form_Smpp();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$smppUsersTrans = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersTrans')->findBy(array('user' => $userId));
    	$smppUsersPromo = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersPromo')->findBy(array('user' => $userId));
    	
    	
    	foreach ($smppUsersTrans as $smppUser){
    		$smppUserSystemId[$smppUser->systemId] = $smppUser->systemId.' | TRANSACTIONAL';
    	}
    	foreach ($smppUsersPromo as $smppUser){
    		$smppUserSystemId[$smppUser->systemId] = $smppUser->systemId.' | PROMO/SCRUB';
    	}
    	if (isset($smppUserSystemId)) {
    		$form->getElement('smpp')->setMultiOptions($smppUserSystemId);
    	}
    	$this->view->form = $form;
    	 
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        	$smppValues = $form->getValues();  
        	        	
        	$systemId = $smppValues['smpp'];
    	    	
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$queryBuilder = $emSmpp->createQueryBuilder();
    	$queryBuilder->select('e')
    	             ->from('modules\smpp\models\SmppRecredit', 'e')
    	             ->where('e.esme = ?1')
    	             ->andWhere('e.client = ?2')
    	             ->orderBy('e.id', 'DESC')
    				 ->setParameter(1, $systemId)
    				 ->setParameter(2, $userId);
    	$query = $queryBuilder->getQuery();
    	
    	$this->view->entries = $query->getResult();
    	
    	}else{
    		$this->view->entries = array();
    	}
    	
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    			  ->setItemCountPerPage(20);
    	 
    	$this->view->entries = $paginator;
    	*/
    }

    public function sentsmsallAction()
    {
        
        $this->view->subject="SENT SMS";
        $userManage = $this->view->navigation()->findOneByLabel('Transactions');
        if ($userManage) {
            $userManage->setActive();
        }
        $page = $this->view->navigation()->findOneByUri('/smpp/esme/sentsmsall');
        if ($page) {
            $page->setActive();
            $this->view->headTitle('Smpp | All Smpp Recredit');
        }
        $emUser = $this->getEntityManager('user');
        $emSmpp = $this->getEntityManager('smpp');
        
        $form = new Smpp_Form_Smpp();
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $smppUsersTrans = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersTrans')->findAll();
        $smppUsersPromo = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersPromo')->findAll();
        
        
        foreach ($smppUsersTrans as $smppUser){
            $smppUserSystemId[$smppUser->systemId] = $smppUser->systemId.' | TRANSACTIONAL';
        }
        foreach ($smppUsersPromo as $smppUser){
            $smppUserSystemId[$smppUser->systemId] = $smppUser->systemId.' | PROMO/SCRUB';
        }
        if (isset($smppUserSystemId)) {
            $form->getElement('smpp')->setMultiOptions($smppUserSystemId);
        }
        $this->view->form = $form;
         
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
            $smppValues = $form->getValues();  
                        
            $systemId = $smppValues['smpp'];
                
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $queryBuilder = $emSmpp->createQueryBuilder();
        $queryBuilder->select('e')
                     ->from('modules\smpp\models\SmppRecredit', 'e')
                     ->where('e.esme = ?1')
                     ->andWhere('e.client = ?2')
                     ->orderBy('e.id', 'DESC')
                     ->setParameter(1, $systemId)
                     ->setParameter(2, $userId);
        $query = $queryBuilder->getQuery();
        
        $this->view->entries = $query->getResult();
        
        }else{
            $this->view->entries = array();
        }
        
        /*
        $paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
        $currentPage = 1;
        $i = $this->_request->getParam('i');
        
        if(!empty($i)){ //Where i is the current page
            $currentPage = $this->_request->getParam('i');
        }
        $paginator->setCurrentPageNumber($currentPage)
                  ->setItemCountPerPage(20);
         
        $this->view->entries = $paginator;
        */
    }

    public function reportAction()
    {

       $this->view->subject="SMPP REPORTS";
       $userManage = $this->view->navigation()->findOneByLabel('Smpp');
        if ($userManage) {
            $userManage->setActive();
        }
       $page = $this->view->navigation()->findOneByUri('/smpp/esme/report');
       if ($page) {
        $page->setActive();
        $this->view->headTitle('Smpp | Sent');
       }

     
        $sdate=date('m/d/Y',time());
        $edate=date('m/d/Y',time());
  
      $emUser = $this->getEntityManager('user');
      $emSms = $this->getEntityManager('sms');
      $emSmpp = $this->getEntityManager('smpp');


        $form = new Smpp_Form_Smpp();
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $smppUsersTrans = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersTrans')->findBy(array('user' => $userId));
        $smppUsersPromo = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersPromo')->findBy(array('user' => $userId));
        
        
        foreach ($smppUsersTrans as $smppUser){
            $smppUserSystemId[$smppUser->systemId.'|TRANS/SCRUB'] = $smppUser->systemId.' | TRANS/SCRUB';
        }
        foreach ($smppUsersPromo as $smppUser){
            $smppUserSystemId[$smppUser->systemId.'|PROMOTIONAL'] = $smppUser->systemId.' | PROMOTIONAL';
        }
        if (isset($smppUserSystemId)) {
            $form->getElement('smpp')->setMultiOptions($smppUserSystemId);
        }
        $this->view->form = $form;

  if ($this->getRequest()->isPost()) {
    $getValue=$this->getRequest()->getPost();

      $sdate=$getValue['from'];
      $edate=$getValue['to'];
      $currentDate = date('Y-m-d H:i:s',time());
      $startDate=date('Y-m-d H:i:s',strtotime($sdate.'00:00:00'));
      $endDate=date('Y-m-d H:i:s',strtotime($edate.'23:59:59'));

      $sTimestamp = strtotime($sdate.'00:00:00');
      $eTimestamp = strtotime($edate.'23:59:59');

      $esme = $getValue['smpp'];

      $esmes = explode('|', $esme);
      $smppUser = $esmes[0];
      $route = $esmes[1];
      $reportManager = new SmppReportManager();
      $requestReport = $reportManager->createReport($userId, $smppUser, $route, $startDate, $endDate, $emSmpp);
      if ($requestReport){
        $id = $requestReport->id;
        shell_exec("/usr/bin/php /var/www/html/webpanel/cronjob/smpp/generateSmppReportPagination.php $smppUser $route $sTimestamp $eTimestamp $id > /dev/null 2> /dev/null &");
      }

  }

    $queryBuilder = $emSmpp->createQueryBuilder();
    $queryBuilder->select('e')
                     ->from('modules\smpp\models\SmppReport', 'e')
                     ->where('e.user = ?2')
                     ->orderBy('e.id', 'DESC')
                     ->setParameter(2, $userId);
      
      
      $query = $queryBuilder->getQuery();
        
      $this->view->entries = $query->getResult();
 
    $this->view->pageIndex = 1;
 
    $this->view->daterange=$sdate.'^'.$edate;
    $this->view->userId = $userId;

    }

    public function reportallAction()
    {

       $this->view->subject="SMPP REPORTS";
       $userManage = $this->view->navigation()->findOneByLabel('Smpp');
        if ($userManage) {
            $userManage->setActive();
        }
       $page = $this->view->navigation()->findOneByUri('/smpp/esme/reportall');
       if ($page) {
        $page->setActive();
        $this->view->headTitle('Smpp | Sent');
       }

     
        $sdate=date('m/d/Y',time());
        $edate=date('m/d/Y',time());
  
      $emUser = $this->getEntityManager('user');
      $emSms = $this->getEntityManager('sms');
      $emSmpp = $this->getEntityManager('smpp');


        $form = new Smpp_Form_Smpp();
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $smppUsersTrans = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersTrans')->findAll();
        $smppUsersPromo = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersPromo')->findAll();
        
        
        foreach ($smppUsersTrans as $smppUser){
            $smppUserSystemId[$smppUser->systemId.'|TRANS/SCRUB'] = $smppUser->systemId.' | TRANS/SCRUB';
        }
        foreach ($smppUsersPromo as $smppUser){
            $smppUserSystemId[$smppUser->systemId.'|PROMOTIONAL'] = $smppUser->systemId.' | PROMOTIONAL';
        }
        if (isset($smppUserSystemId)) {
            $form->getElement('smpp')->setMultiOptions($smppUserSystemId);
        }
        $this->view->form = $form;

  if ($this->getRequest()->isPost()) {
    $getValue=$this->getRequest()->getPost();

      $sdate=$getValue['from'];
      $edate=$getValue['to'];
      $currentDate = date('Y-m-d H:i:s',time());
      $startDate=date('Y-m-d H:i:s',strtotime($sdate.'00:00:00'));
      $endDate=date('Y-m-d H:i:s',strtotime($edate.'23:59:59'));

      $sTimestamp = strtotime($sdate.'00:00:00');
      $eTimestamp = strtotime($edate.'23:59:59');

      $esme = $getValue['smpp'];

      $esmes = explode('|', $esme);
      $smppUser = $esmes[0];
      $route = $esmes[1];
      $reportManager = new SmppReportManager();
      $requestReport = $reportManager->createReport($userId, $smppUser, $route, $startDate, $endDate, $emSmpp);
      if ($requestReport){
        $id = $requestReport->id;
        shell_exec("/usr/bin/php /var/www/html/webpanel/cronjob/smpp/generateSmppReportPagination.php $smppUser $route $sTimestamp $eTimestamp $id < /dev/null&");
      }

  }

    $queryBuilder = $emSmpp->createQueryBuilder();
    $queryBuilder->select('e')
                     ->from('modules\smpp\models\SmppReport', 'e')
                     ->orderBy('e.id', 'DESC');
      
      
      $query = $queryBuilder->getQuery();
        
      $this->view->entries = $query->getResult();
 
    $this->view->pageIndex = 1;
 
    $this->view->daterange=$sdate.'^'.$edate;
    $this->view->userId = $userId;

    }

    public function summaryAction()
    {
        $this->_helper->layout->disableLayout();
        $smpp = $this->_request->getParam('smpp');
        $to = $this->_request->getParam('to');
        $from = $this->_request->getParam('from');

    $emUser = $this->getEntityManager('user');
    $emSmpp = $this->getEntityManager('smpp');
    $userId = Zend_Auth::getInstance()->getIdentity()->id;
    $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    
    $esme = explode('|', $smpp);
    $esmeUser = $esme['0'];
    $route = $esme['1'];

    $startDate=strtotime($from.'00:00:00');
    $endDate=strtotime($to.'23:59:59');

    if ($esmeUser){
        
        
        $queryBuilder = $emSmpp->createQueryBuilder();
        if (($route == 'TRANS/SCRUB')){
            
            $sendsql="SELECT count(*) as send FROM smpp_log_mt_trans WHERE time >= $startDate AND time <= $endDate AND service = '$esmeUser'";
	    $sendstmt = $emSmpp->getConnection()->prepare($sendsql);
	    $sendstmt->execute();
            $send = $sendstmt->fetchAll();
		
            $deliveredsql="SELECT count(*) as delivered FROM smpp_log_mt_trans WHERE time >= $startDate AND time <= $endDate AND dlrdata LIKE '%DELIVRD%' AND status='1' AND service = '$esmeUser'";
	    $deliveredstmt = $emSmpp->getConnection()->prepare($deliveredsql);
	    $deliveredstmt->execute();
            $delivered = $deliveredstmt->fetchAll();
            
            $awaitingsql="SELECT count(*) as awaiting FROM smpp_log_mt_trans WHERE time >= $startDate AND time <= $endDate AND status='0' AND service = '$esmeUser'";
	    $awaitingstmt = $emSmpp->getConnection()->prepare($awaitingsql);
	    $awaitingstmt->execute();
            $awaiting = $awaitingstmt->fetchAll();
            
            $failedsql="SELECT count(*) as failed FROM smpp_log_mt_trans WHERE time >= $startDate AND time <= $endDate AND dlrdata NOT LIKE '%DELIVRD%' AND status='1' AND service = '$esmeUser'";
	    $failedstmt = $emSmpp->getConnection()->prepare($failedsql);
	    $failedstmt->execute();
            $failed = $failedstmt->fetchAll(); 
            
            $nocreditsql="SELECT count(*) as nocredit FROM smpp_log_mt_trans WHERE time >= $startDate AND time <= $endDate AND dlrdata = 'NACK%2F0x00000460%2FNo+credit+failed' AND status='1' AND service = '$esmeUser'";
	    $nocreditstmt = $emSmpp->getConnection()->prepare($nocreditsql);
	    $nocreditstmt->execute();
            $nocredit = $nocreditstmt->fetchAll();
            
            ////
            $sendsql2="SELECT count(*) as send FROM smpp_log_mt_trans_1 WHERE time >= $startDate AND time <= $endDate AND service = '$esmeUser'";
	    $sendstmt2 = $emSmpp->getConnection()->prepare($sendsql2);
	    $sendstmt2->execute();
            $send2 = $sendstmt2->fetchAll();
		
            $deliveredsql2="SELECT count(*) as delivered FROM smpp_log_mt_trans_1 WHERE time >= $startDate AND time <= $endDate AND dlrdata LIKE '%DELIVRD%' AND status='1' AND service = '$esmeUser'";
	    $deliveredstmt2 = $emSmpp->getConnection()->prepare($deliveredsql2);
	    $deliveredstmt2->execute();
            $delivered2 = $deliveredstmt2->fetchAll();
            
            $awaitingsql2="SELECT count(*) as awaiting FROM smpp_log_mt_trans_1 WHERE time >= $startDate AND time <= $endDate AND status='0' AND service = '$esmeUser'";
	    $awaitingstmt2 = $emSmpp->getConnection()->prepare($awaitingsql2);
	    $awaitingstmt2->execute();
            $awaiting2 = $awaitingstmt2->fetchAll();
            
            $failedsql2="SELECT count(*) as failed FROM smpp_log_mt_trans_1 WHERE time >= $startDate AND time <= $endDate AND dlrdata NOT LIKE '%DELIVRD%' AND status='1' AND service = '$esmeUser'";
	    $failedstmt2 = $emSmpp->getConnection()->prepare($failedsql2);
	    $failedstmt2->execute();
            $failed2 = $failedstmt2->fetchAll(); 
            
            $nocreditsql2="SELECT count(*) as nocredit FROM smpp_log_mt_trans_1 WHERE time >= $startDate AND time <= $endDate AND dlrdata = 'NACK%2F0x00000460%2FNo+credit+failed' AND status='1' AND service = '$esmeUser'";
	    $nocreditstmt2 = $emSmpp->getConnection()->prepare($nocreditsql2);
	    $nocreditstmt2->execute();
            $nocredit2 = $nocreditstmt2->fetchAll();
            ////
            
            //REROUTE
            $sendReroutesql="SELECT count(*) as send FROM smpp_log_mt_trans WHERE time >= $startDate AND time <= $endDate AND service = '$esmeUser' AND `resent_smsc_id` != 'NULL'";
	    $sendReroutestmt = $emSmpp->getConnection()->prepare($sendReroutesql);
	    $sendReroutestmt->execute();
            $sendReroute = $sendReroutestmt->fetchAll();
		
            $deliveredReroutesql="SELECT count(*) as delivered FROM smpp_log_mt_trans WHERE time >= $startDate AND time <= $endDate AND dlrdata LIKE '%DELIVRD%' AND status='1' AND service = '$esmeUser' AND `resent_smsc_id`  != 'NULL'";
	    $deliveredReroutestmt = $emSmpp->getConnection()->prepare($deliveredReroutesql);
	    $deliveredReroutestmt->execute();
            $deliveredReroute = $deliveredReroutestmt->fetchAll();
            
            $awaitingReroutesql="SELECT count(*) as awaiting FROM smpp_log_mt_trans WHERE time >= $startDate AND time <= $endDate AND status='0' AND service = '$esmeUser' AND `resent_smsc_id`  != 'NULL'";
	    $awaitingReroutestmt = $emSmpp->getConnection()->prepare($awaitingReroutesql);
	    $awaitingReroutestmt->execute();
            $awaitingReroute = $awaitingReroutestmt->fetchAll();
            
            $failedReroutesql="SELECT count(*) as failed FROM smpp_log_mt_trans WHERE time >= $startDate AND time <= $endDate AND dlrdata NOT LIKE '%DELIVRD%' AND status='1' AND service = '$esmeUser' AND `resent_smsc_id`  != 'NULL'";
	    $failedReroutestmt = $emSmpp->getConnection()->prepare($failedReroutesql);
	    $failedReroutestmt->execute();
            $failedReroute = $failedReroutestmt->fetchAll();
                
            
            
        }elseif ($smsCampagin->type == 'INTERNATIONAL'){
            
            $sendsql="SELECT count(*) as send FROM smpp_log_mt_inter WHERE time >= $startDate AND time <= $endDate AND service = '$esmeUser'";
	    $sendstmt = $emSmpp->getConnection()->prepare($sendsql);
	    $sendstmt->execute();
            $send = $sendstmt->fetchAll();
		
            $deliveredsql="SELECT count(*) as delivered FROM smpp_log_mt_inter WHERE time >= $startDate AND time <= $endDate AND dlrdata LIKE '%DELIVRD%' AND status='1' AND service = '$esmeUser'";
	    $deliveredstmt = $emSmpp->getConnection()->prepare($deliveredsql);
	    $deliveredstmt->execute();
            $delivered = $deliveredstmt->fetchAll();
            
            $awaitingsql="SELECT count(*) as awaiting FROM smpp_log_mt_inter WHERE time >= $startDate AND time <= $endDate AND status='0' AND service = '$esmeUser'";
	    $awaitingstmt = $emSmpp->getConnection()->prepare($awaitingsql);
	    $awaitingstmt->execute();
            $awaiting = $awaitingstmt->fetchAll();
            
            $failedsql="SELECT count(*) as failed FROM smpp_log_mt_inter WHERE time >= $startDate AND time <= $endDate AND dlrdata NOT LIKE '%DELIVRD%' AND status='1' AND service = '$esmeUser'";
	    $failedstmt = $emSmpp->getConnection()->prepare($failedsql);
	    $failedstmt->execute();
            $failed = $failedstmt->fetchAll();
        }else {
            $sendsql="SELECT count(*) as send FROM smpp_log_mt_promo WHERE time >= $startDate AND time <= $endDate AND service = '$esmeUser'";
	    $sendstmt = $emSmpp->getConnection()->prepare($sendsql);
	    $sendstmt->execute();
            $send = $sendstmt->fetchAll();
		
            $deliveredsql="SELECT count(*) as delivered FROM smpp_log_mt_promo WHERE time >= $startDate AND time <= $endDate AND dlrdata LIKE '%DELIVRD%' AND status='1' AND service = '$esmeUser'";
	    $deliveredstmt = $emSmpp->getConnection()->prepare($deliveredsql);
	    $deliveredstmt->execute();
            $delivered = $deliveredstmt->fetchAll();
            
            $awaitingsql="SELECT count(*) as awaiting FROM smpp_log_mt_promo WHERE time >= $startDate AND time <= $endDate AND status='0' AND service = '$esmeUser'";
	    $awaitingstmt = $emSmpp->getConnection()->prepare($awaitingsql);
	    $awaitingstmt->execute();
            $awaiting = $awaitingstmt->fetchAll();
            
            $failedsql="SELECT count(*) as failed FROM smpp_log_mt_promo WHERE time >= $startDate AND time <= $endDate AND dlrdata NOT LIKE '%DELIVRD%' AND status='1' AND service = '$esmeUser'";
	    $failedstmt = $emSmpp->getConnection()->prepare($failedsql);
	    $failedstmt->execute();
            $failed = $failedstmt->fetchAll();
            
        }
        
        

        $this->view->send = $send['0']['send'] + $send2['0']['send'];
        $this->view->delivered = $delivered['0']['delivered'] + $delivered2['0']['delivered'];
        $this->view->failed = ($failed['0']['failed'] - $nocredit['0']['nocredit']) + ($failed2['0']['failed'] - $nocredit2['0']['nocredit']);
        $this->view->nocredit = $nocredit['0']['nocredit'] + $nocredit2['0']['nocredit'];
        $this->view->submit = $awaiting['0']['awaiting'] + $awaiting2['0']['awaiting'];
        
        $this->view->sendReroute = $sendReroute['0']['send'];
        $this->view->deliveredReroute = $deliveredReroute['0']['delivered'];
        $this->view->failedReroute = $failedReroute['0']['failed'];
        $this->view->submitReroute = $awaitingReroute['0']['awaiting'];

    }
    }



}



