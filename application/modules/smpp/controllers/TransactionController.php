<?php
use modules\user\models\AccountManager;
use modules\smpp\models\SmppUserPromoManager;
use modules\smpp\models\SmppUserTransManager;
use modules\user\models\Account;
use modules\smpp\models\SmppUserPromo;
use modules\smpp\models\SmppUserTrans;
use Doctrine\ORM\Query\ResultSetMapping;
use modules\user\models\ActivityLogManager;
use modules\smpp\models\SmppUserInterManager;
class Smpp_TransactionController extends Smpp_Controller_BaseController
{

public function init()
    {
        $creditManage = $this->view->navigation()->findOneByLabel('Transactions');
		if ($creditManage) {
		  $creditManage->setActive();
		}
		$action = $this->getRequest()->getActionName();
		$emUser = $this->getEntityManager('user');
		$domain = Smpp_Utility::getFqdn();
		$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
		$domainLayout = $domainRepo->layout;
		if ($domainLayout != null){
			$view = $domainLayout.'-'.$action;
			$this->_helper->viewRenderer("$view");
		}
		parent::init();
    }

    public function indexAction()
    {

        $this->view->subject="CREDIT TRANSFER";
    	$page = $this->view->navigation()->findOneByLabel('Smpp Transfer');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Transactions | Smpp Transfer');
    	}

    	
   		$form = new Smpp_Form_Transaction();
        $this->view->form = $form;
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        	$transactionValues = $form->getValues();
        	
        	$mode = $transactionValues['mode'];
        	//$accountManager = new AccountManager();
        	
        	if ($transactionValues['type'] == 'TRANSACTIONAL' || $transactionValues['type'] == 'TRANS-SCRUB'){
        		$smppManager = new SmppUserTransManager();
        	}elseif ($transactionValues['type'] == 'INTERNATIONAL'){
        		$smppManager = new SmppUserInterManager();
        	}else{
        		$smppManager = new SmppUserPromoManager();
        	}
        	
        	$resellerId = Zend_Auth::getInstance()->getIdentity()->id;
        	$emUser = $this->getEntityManager('user');
        	$emSmpp = $this->getEntityManager('smpp');
        	if ($mode == '1') {
        		//Credit
        		$accountArray = $smppManager->setSmppCredit($transactionValues, $resellerId, $emUser, $emSmpp);
        		$credit = $accountArray['user'];
        		$resellerRepo = $accountArray['reseller'];
        		$logRepo = $accountArray['log'];
        		if ($credit) {
        					$balance = $credit->credit;
        					$activityManager = new ActivityLogManager();
        					$activityManager->createActivityLog($resellerRepo, $credit->systemId, $this->_module, $this->_controller, $this->_action, 'Credit Transfered Successfully', $this->_domain, $this->_ip, $this->_device, $logRepo->id, 'ADD', 'SMPP CREDIT', $emUser);
        					
        					$this->view->success = array('Credit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));
        		}else{
        			$this->view->errors = array('Credit' => array('isFailed' => 'Transaction failed'));
        		}
        	}elseif ($mode == '2') {
        		//Debit
        		$accountArray = $smppManager->setSmppDebit($transactionValues, $resellerId, $emUser, $emSmpp);
        		$debit = $accountArray['user'];
        		$resellerRepo = $accountArray['reseller'];
        		$logRepo = $accountArray['log'];
        		if ($debit) {
        			$balance = $debit->credit;
        			$activityManager = new ActivityLogManager();
        			$activityManager->createActivityLog($resellerRepo, $debit->systemId, $this->_module, $this->_controller, $this->_action, 'Credit Transfered Successfully', $this->_domain, $this->_ip, $this->_device, $logRepo->id, 'DEDUCT', 'SMPP CREDIT', $emUser);
        			 
        			$this->view->success = array('Debit' => array('isSuccess' => 'Transaction completed'),'Balance' => array('isUpdated' => "$balance"));       				
        		}else{
        			$this->view->errors = array('Debit' => array('isFailed' => 'Transaction failed'));
        		}
        	}        	
        }else{
        	if (count($form->getErrors('token')) > 0) {
        		$this->_redirect('/smpp/transaction/index');
        	}
       		$this->view->errors = $form->getMessages();
        }
        $form->reset();
    }

    public function autosuggestAction()
    {
        $this->_helper->layout()->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    	
    	$type = $this->getRequest()->getParam('type');
    	$returnString = array();
    	
    		$emSmpp = $this->getEntityManager('smpp');
    		$userId = Zend_Auth::getInstance()->getIdentity()->id;
    		if ($type == 'TRANSACTIONAL'){
    			$users = $emSmpp->getRepository("modules\smpp\models\SmppUserTrans")->getSmppTrans("$userId");
    		}elseif($type == 'TRANS-SCRUB'){
    			$users = $emSmpp->getRepository("modules\smpp\models\SmppUserTrans")->getSmppScrub("$userId");
    		}elseif ($type == 'PROMOTIONAL'){
    			$users = $emSmpp->getRepository("modules\smpp\models\SmppUserPromo")->getSmpp("$userId");
    		}elseif ($type == 'INTERNATIONAL'){
    			$users = $emSmpp->getRepository("modules\smpp\models\SmppUserInter")->getSmpp("$userId");
    		}
    		if($users){
    			foreach($users as $user) {
    				$systemId = $user->systemId;
    				//$route = $user->defaultSmsc;
    				
    				$returnString[] = $systemId;
    			}
    		}
    	
    	
    	//echo ($returnString);
    	echo Zend_Json::encode($returnString);
    }
    
    public function logAction()
    {
         $this->view->subject="CREDIT LOG";
    	$page = $this->view->navigation()->findOneByLabel('Smpp Log');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Transactions | Smpp Log');
    	}
    	 
    	//$reportManager = new SendSmsManager();
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$queryBuilder = $emUser->createQueryBuilder();
    	$queryBuilder->select('e')
    	->from('modules\smpp\models\SmppAccountLog', 'e')
    	->where('e.user = ?1')
    	->orderBy('e.id', 'DESC')
    	->setParameter(1, $userId);
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	->setItemCountPerPage(20);
    
    	$this->view->entries = $paginator;
    	*/
    }
    
    public function inAction()
    {
    	$this->view->subject="CREDIT LOG";
    	$page = $this->view->navigation()->findOneByLabel('Smpp In');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Transactions | Smpp In');
    	}
    	 
    	//$reportManager = new SendSmsManager();
    	//$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	//$roleId = Zend_Auth::getInstance()->getIdentity()->role_id;
    	//$role = $emUser->getRepository("Models\Role")->find($roleId);
    	$this->view->userId = $userId;
    	$queryBuilder = $emSmpp->createQueryBuilder();
    	$queryBuilder->select('e')
    	->from('modules\smpp\models\SmppAccountLog', 'e')
    	->where('e.user = ?1 AND e.mode = ?2')
    	->orderBy('e.id', 'DESC')
    	->setParameter(1, $userId)
    	->setParameter(2, 'DEDUCT');
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	->setItemCountPerPage(20);
    
    	$this->view->entries = $paginator;
    	*/
    }
    
    public function outAction()
    {
    	$this->view->subject="CREDIT LOG";
    	$page = $this->view->navigation()->findOneByLabel('Smpp Out');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Transactions | Smpp Out');
    	}
    
    	//$reportManager = new SendSmsManager();
    	//$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$this->view->userId = $userId;
    	$queryBuilder = $emSmpp->createQueryBuilder();
    	$queryBuilder->select('e')
    	->from('modules\smpp\models\SmppAccountLog', 'e')
    	->where('e.user = ?1 AND e.mode = ?2')
    	->orderBy('e.id', 'DESC')
    	->setParameter(1, $userId)
    	->setParameter(2, 'ADD');
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	->setItemCountPerPage(20);
    
    	$this->view->entries = $paginator;
    	*/
    }


}

