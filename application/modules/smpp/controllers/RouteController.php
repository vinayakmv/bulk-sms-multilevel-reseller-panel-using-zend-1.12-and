<?php
use modules\smpp\models\SmppSmscRetryTransManager;
use modules\smpp\models\SmppSmscRetryPromoManager;
use modules\user\models\SmppRouteManager;
use modules\user\models\UserManager;
use modules\user\models\ActivityLogManager;
use modules\smpp\models\SmppSenderIdRouteRequestManager;
use modules\smpp\models\SmppPrefixRouteRequestManager;
use modules\smpp\models\SmppRouteTransManager;
class Smpp_RouteController extends Smpp_Controller_BaseController
{

	public function init()
    {
    	$userManage = $this->view->navigation()->findOneByLabel('Routes');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function createAction()
    {
        $this->view->subject="CREATE SMSC ROUTES";
    	$page = $this->view->navigation()->findOneByUri('/smpp/route/create');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Create Smpp');
    	}
        $form = new Smpp_Form_NewRoute();
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $emUser = $this->getEntityManager('user');
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        	$routeValues = $form->getValues();        	
        	
        	
        	$smppManager = new SmppRouteManager();
        	$smppCreated = $smppManager->createRoute($routeValues, $emUser);
        	if ($smppCreated) {
        		$activityManager = new ActivityLogManager();
        		$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Route Created Successfully', $this->_domain, $this->_ip, $this->_device, $smppCreated->id, 'CREATE', 'SMPP ROUTE', $emUser);
        		 
        		$this->view->success = array('route' => array('isSuccess' => 'Route created successfully'));
        	}else {
        		$this->view->errors = array('route' => array('isFailed' => 'Route create failed'));
        	}
        }else {
        	$this->view->errors = $form->getMessages();        
    	}
    	$form->reset();
    }

    public function routesAction()
    {
        $this->view->subject="SMPP ROUTE LIST";
    	$page = $this->view->navigation()->findOneByUri('/smpp/route/routes');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Route Manage | Smpp Routes');
    	}
    	
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
        $emUser = $this->getEntityManager('user');
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	          ->setItemCountPerPage(20);
    	*/
    	
    	
    	// Block,Unblock,Activate Users,Resellers
    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
    	
    		if (isset($button['active'])) {
    			//Block User
    			$smppManager = new SmppRouteManager();
    			$reset = $smppManager->blockSmsc($button['active'], $emUser);  
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Route Blocked Successfully', $this->_domain, $this->_ip, $this->_device, $button['active'], 'BLOCK', 'SMPP ROUTE', $emUser);
    			
    		}
    	
    		if (isset($button['blocked'])) {
    			//Unblock User
    			$smppManager = new SmppRouteManager();
    			$reset = $smppManager->unblockSmsc($button['blocked'], $emUser);    	
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Route Unblocked Successfully', $this->_domain, $this->_ip, $this->_device, $button['blocked'], 'UNBLOCK', 'SMPP ROUTE', $emUser);
    			 
    		}
    		
    		if (isset($button['delete'])) {
    			//Delete User
    			$smppManager = new SmppRouteManager();
    			$reset = $smppManager->deleteSmsc($button['delete'], $emUser);
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Route Deleted Successfully', $this->_domain, $this->_ip, $this->_device, $button['delete'], 'DELETE', 'SMPP ROUTE', $emUser);
    			 
    		}
    	
    	}
    	
    	$queryBuilder = $emUser->createQueryBuilder();
    	$queryBuilder->select('e')->from('modules\user\models\SmppRoute', 'e');
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    }
    
    public function senderAction()
    {
    	$this->view->subject="SENDER ID ROUTE";
    	$page = $this->view->navigation()->findOneByuri('/smpp/route/sender');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Smpp Sender');
    	}
    	
    	$form = new Smpp_Form_Sroute();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	
    	/*
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	$transUsers =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId);
    	$smppRoutes = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>'TRANSACTIONAL'));
    	
    	foreach ($smppRoutes as $routes){
    		$route[$routes->route]=$routes->name;
    	}
    	
    	$user['all'] = "ALL";
    	foreach ($transUsers as $users)
    	{
    		$user[$users['system_id']]= $users['system_id'];
    	}
    	 
    	if (isset($user))
    	{
    		$form->getElement('user')->setMultiOptions($user);
    	}
    	if (isset($route))
    	{
    		$form->getElement('route')->setMultiOptions($route);
    	}
    	*/
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    	
    		$getValues = $form->getValues();
    		//$systemId=$getValues['user'];
    		//$getUserId =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId, $systemId);
    	
    		$SmppSenderIdRequestManager=new SmppSenderIdRouteRequestManager;
    		$createSenderId = $SmppSenderIdRequestManager->createSenderId($getValues, $userId, $emSmpp);
    	
    		if ($createSenderId) {
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Sender Route Created Successfully', $this->_domain, $this->_ip, $this->_device, $createSenderId->id, 'CREATE', 'SMPP SENDER ROUTE', $emUser);
    			
    			$this->view->success = 'sender route created';
    		}
    		else{
    			$this->view->errors = 'failed';
    		}
    	}else{
    		$this->view->errors = $form->getMessages();
    	}
    	
    	$form->reset();
    }
    
    public function prefixAction()
    {
    	$this->view->subject="PREFIX ROUTE";
    	$page = $this->view->navigation()->findOneByuri('/smpp/route/prefix');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Smpp Prefix');
    	}
    	
    	$form = new Smpp_Form_Proute();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	/*
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	$transUsers =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId);
    	$smppRoutes = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>'TRANSACTIONAL'));
    	
    	foreach ($smppRoutes as $routes){
    		$route[$routes->route]=$routes->name;
    	}
    	
    	$user['all'] = "ALL";
    	foreach ($transUsers as $users)
    	{
    		$user[$users['system_id']]= $users['system_id'];
    	}
    	 
    	if (isset($user))
    	{
    		$form->getElement('user')->setMultiOptions($user);
    	}
    	if (isset($route))
    	{
    		$form->getElement('route')->setMultiOptions($route);
    	}
    	*/
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    	
    		$getValues = $form->getValues();
    		//$systemId=$getValues['user'];
    		//$getUserId =$emSmpp->getRepository('modules\smpp\models\SmppUserTrans')->getEsmes($userId, $systemId);
    	
    		$SmppSenderIdRequestManager=new SmppPrefixRouteRequestManager;
    		$createSenderId = $SmppSenderIdRequestManager->createPrefix($getValues, $userId, $emSmpp);
    	
    		if ($createSenderId) {
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Sender Route Created Successfully', $this->_domain, $this->_ip, $this->_device, $createSenderId->id, 'CREATE', 'SMPP SENDER ROUTE', $emUser);
    			 
    			$this->view->success = 'prefix route created';
    		}
    		else{
    			$this->view->errors = 'failed';
    		}
    	}else{
    		$this->view->errors = $form->getMessages();
    	}
    	
    	$form->reset();
    }
    
    
    
	public function sviewAction()
    {
    	$this->view->subject="SENDERID ROUTE LIST";
    	
    	$page = $this->view->navigation()->findOneByuri('/smpp/route/sview');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Sender Routes');
    	}
    	 
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$smppPrefixRouteRepo = $emSmpp->getRepository('modules\smpp\models\SmppSenderIdRouteRequest')->findAll();
    
    	$this->view->entries = $smppPrefixRouteRepo;
    	 
    	$smppUsersTrans = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersTrans')->findAll();
    	$smppUserTransIds = array();
    	$routes = array();
    	$esmes = '<option value="all">all</option>';
    	foreach ($smppUsersTrans as $smppUser){
    		$esmes .= '<option value="'.$smppUser->systemId.'">'.$smppUser->systemId.'</option>';
    	}
    	$this->view->esmes = $esmes;
    	 
    	$routeRepo = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>'TRANSACTIONAL'));
    	$routes = '';
    	
        foreach ($routeRepo as $transRoute){
    		$routes .= '<option value="'.$transRoute->route.'|'.$transRoute->type.'">'.$transRoute->route.'|'.$transRoute->type.'</option>';
    	}
    	$this->view->routes = $routes;
    	 
    	if ($this->getRequest()->isPost()) {
    		 
    		$getValues = $this->getRequest()->getPost();
    		
    		$SmppRouteManager=new SmppRouteTransManager();
    		if ($getValues['submit'] == 'Activate'){
    			$createPrefix = $SmppRouteManager->insert($getValues, 'sender', $emSmpp);
    		}else{
    			$createPrefix = $SmppRouteManager->delete($getValues, 'sender', $emSmpp);
    		}
    		
    	
    		if ($createPrefix) {
    			$this->view->success = 'Prefix route added';
    		}else{
    			$this->view->errors = 'Failed to add prefix route';
    		}
    	}
    }

    public function pviewAction()
    {
    	$this->view->subject="PREFIX ROUTE LIST";
    	
    	$page = $this->view->navigation()->findOneByuri('/smpp/route/pview');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Prefix Routes');
    	}
    	 
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$smppPrefixRouteRepo = $emSmpp->getRepository('modules\smpp\models\SmppPrefixRouteRequest')->findAll();
    
    	$this->view->entries = $smppPrefixRouteRepo;
    	 
    	$smppUsersTrans = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersTrans')->findAll();
        $smppUsersPromo = $emSmpp->getRepository('modules\smpp\models\UsersSmppUsersPromo')->findAll();
    	$smppUserTransIds = array();
    	$routes = array();
    	$esmes = '<option value="all">all</option>';
    	foreach ($smppUsersTrans as $smppUser){
    		$esmes .= '<option value="'.$smppUser->systemId.'|TRANSACTIONAL'.'">'.$smppUser->systemId.'|TRANSACTIONAL'.'</option>';
    	}
        foreach ($smppUsersPromo as $smppUser){
    		$esmes .= '<option value="'.$smppUser->systemId.'|PROMOTIONAL'.'">'.$smppUser->systemId.'|PROMOTIONAL'.'</option>';
    	}
    	$this->view->esmes = $esmes;
    	 
    	$routeTransRepo = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>'TRANSACTIONAL'));
        $routePromoRepo = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('type'=>'PROMOTIONAL'));
    	$routes = '';
        foreach ($routePromoRepo as $promoRoute){
    		$routes .= '<option value="'.$promoRoute->route.'|'.$promoRoute->type.'">'.$promoRoute->route.'|'.$promoRoute->type.'</option>';
    	}
    	foreach ($routeTransRepo as $transRoute){
    		$routes .= '<option value="'.$transRoute->route.'|'.$transRoute->type.'">'.$transRoute->route.'|'.$transRoute->type.'</option>';
    	}
        
    	$this->view->routes = $routes;
    	 
    	if ($this->getRequest()->isPost()) {
    		 
    		$getValues = $this->getRequest()->getPost();
    		
    		$SmppRouteManager=new SmppRouteTransManager();
    		if ($getValues['submit'] == 'Activate'){
    			$createPrefix = $SmppRouteManager->insert($getValues, 'prefix', $emSmpp);
    		}else{
    			$createPrefix = $SmppRouteManager->delete($getValues, 'prefix', $emSmpp);
    		}
    		
    	
    		if ($createPrefix) {
                    
    			$this->view->success = 'Prefix route added';
    		}else{
    			$this->view->errors = 'Failed to add prefix route';
    		}
    	}
    }
    
    
    public function rerouteAction()
    {
        $this->view->subject="PREFIX ROUTE";
    	$page = $this->view->navigation()->findOneByuri('/smpp/route/reroute');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Smpp Reroute');
    	}
    	
    	$form = new Smpp_Form_Reroute();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	
    	$this->view->form = $form;
    	
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
               $transactionValues = $form->getValues();
                
               $type = $transactionValues['retype'];
               $esme = $transactionValues['esme'];
               $route = $transactionValues['route'];
               $reroute = $transactionValues['reroute'];
               
               if (!empty($esme) && !empty($route) && !empty($reroute)){
                   if ($type == 'TRANSACTIONAL' || $type == 'TRANS-SCRUB'){
                        $retryManager = new SmppSmscRetryTransManager();
                        $created = $retryManager->createRoute($transactionValues, $emSmpp);
                    }elseif ($type == 'PROMOTIONAL'){
                        $retryManager = new SmppSmscRetryPromoManager();
                        $created = $retryManager->createRoute($transactionValues, $emSmpp);
                    }
                    
                    if ($created) {
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Reroute Created Successfully', $this->_domain, $this->_ip, $this->_device, $created->id, 'CREATE', 'SMPP REROUTE', $emUser);
    			 
    			$this->view->success = 'smsc rerouting created';
                    }else{
                            $this->view->errors = 'failed';
                    }
               }else{
                            $this->view->errors = 'failed';
               }
               
    		
    		
    	}else{
    		$this->view->errors = $form->getMessages();
                
    	}
    	
    	$form->reset();
    }
    
    public function reviewAction()
    {
    	$this->view->subject="PREFIX ROUTE LIST";
    	
    	$page = $this->view->navigation()->findOneByuri('/smpp/route/review');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Routes | Reroute Routes');
    	}
    	 
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
        $userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	
    	 
    	if ($this->getRequest()->isPost()) {
    		 
    		$getValues = $this->getRequest()->getPost();
                $type = $getValues['type'];
                $id = $getValues['route'];
    		
    		if ($type == 'TRANSACTIONAL' || $type == 'TRANS-SCRUB'){
                    $retryManager = new SmppSmscRetryTransManager();
                    if ($getValues['submit'] == 'Delete'){
                        $created = $retryManager->delete($id, $emSmpp);
                    }elseif($getValues['submit'] == 'Activate'){
                        $created = $retryManager->unblock($id, $emSmpp);
                    }elseif($getValues['submit'] == 'Block'){
                        $created = $retryManager->block($id, $emSmpp);
                    }
                    
                        
                        
                    }elseif ($type == 'PROMOTIONAL'){
                        $retryManager = new SmppSmscRetryPromoManager();
                        if ($getValues['submit'] == 'Delete'){
                            $created = $retryManager->delete($id, $emSmpp);
                        }elseif($getValues['submit'] == 'Activate'){
                            $created = $retryManager->unblock($id, $emSmpp);
                        }elseif($getValues['submit'] == 'Block'){
                            $created = $retryManager->block($id, $emSmpp);
                        }
                    }
                    
                    if ($created) {
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Reroute Deleted'.$getValues['submit'].' Successfully', $this->_domain, $this->_ip, $this->_device, $created->id, $getValues['submit'], 'SMPP REROUTE', $emUser);
    			 
    			$this->view->success = 'smsc rerouting deleted';
                    }else{
                            $this->view->errors = 'failed';
                    }
    	}
        
        $smppUsersTrans = $emSmpp->getRepository('modules\smpp\models\SmppSmscRetryTrans')->findAll();
        $smppUsersPromo = $emSmpp->getRepository('modules\smpp\models\SmppSmscRetryPromo')->findAll();
    	
    	$routes = array();
        
    	foreach ($smppUsersTrans as $smppUserTrans){
            
                $routes[] .= $smppUserTrans->service.'|'."TRANSACTIONAL".'|'.$smppUserTrans->smscId.'|'.$smppUserTrans->smscIdRetry.'|'.$smppUserTrans->id.'|'.$smppUserTrans->status;
        }
        foreach ($smppUsersPromo as $smppUserPromo){
                $routes[] .= $smppUserPromo->service.'|'."PROMOTIONAL".'|'.$smppUserPromo->smscId.'|'.$smppUserPromo->smscIdRetry.'|'.$smppUserPromo->id.'|'.$smppUserPromo->status;
         }
        
    	$this->view->entries = $routes;
    }
    
     public function autosuggestAction()
    {
        $this->_helper->layout()->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    	
    	$type = $this->getRequest()->getParam('type');
    	$returnString = array();
    	
    		$emSmpp = $this->getEntityManager('smpp');
    		$userId = Zend_Auth::getInstance()->getIdentity()->id;
    		if ($type == 'TRANSACTIONAL'){
    			$users = $emSmpp->getRepository("modules\smpp\models\SmppUserTrans")->findAll();
    		}elseif($type == 'TRANS-SCRUB'){
    			$users = $emSmpp->getRepository("modules\smpp\models\SmppUserTrans")->findAll();
    		}elseif ($type == 'PROMOTIONAL'){
    			$users = $emSmpp->getRepository("modules\smpp\models\SmppUserPromo")->findAll();
    		}elseif ($type == 'INTERNATIONAL'){
    			$users = $emSmpp->getRepository("modules\smpp\models\SmppUserInter")->findAll();
    		}
    		if($users){
    			foreach($users as $user) {
    				$systemId = $user->systemId;
    				//$route = $user->defaultSmsc;
    				
    				$returnString[] = $systemId;
    			}
    		}
    	
    	
    	//echo ($returnString);
    	echo Zend_Json::encode($returnString);
    }
    
    public function autosuggestsmscAction()
    {
        $this->_helper->layout()->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    	
    	$type = $this->getRequest()->getParam('type');
    	$returnString = array();
    	
    		$emSmpp = $this->getEntityManager('user');
    		$userId = Zend_Auth::getInstance()->getIdentity()->id;
    		
    		$users = $emSmpp->getRepository("modules\user\models\SmppRoute")->findBy(array('type' => "$type"));
    		
    		if($users){
    			foreach($users as $user) {
    				$systemId = $user->route;
    				$name = $user->name;
    				
    				$returnString["$systemId"] = $name;
    			}
    		}
    	
    	
    	//echo ($returnString);
    	echo Zend_Json::encode($returnString);
    }
}





