<?php

use modules\user\models\ActivityLogManager;
use modules\smpp\models\SmscManager;
class Smpp_SmscController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $action = $this->getRequest()->getActionName();
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	if ($domainLayout != null){
    		$view = $domainLayout.'-'.$action;
    		$this->_helper->viewRenderer("$view");
    	}
        parent::init();
    }

    public function indexAction()
    {
        // action body
    }

    public function createAction()
    {
        $this->view->subject="CREATE SMPP SMSC";
    	$userManage = $this->view->navigation()->findOneByLabel('Smsc');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$page = $this->view->navigation()->findOneByUri('/smpp/smsc/create');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Smsc | Create Smpp');
    	}      	
        $emUser = $this->getEntityManager('user');
        $emSmpp = $this->getEntityManager('smpp');
        
        $userId = Zend_Auth::getInstance()->getIdentity()->id;
        $form = new Smpp_Form_NewSmsc();
        $routeRepos = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('status' => '1'));
         
        foreach ($routeRepos as $routes) {
        	$route[$routes->route] = $routes->route;
        }
        if (isset($route)) {
        	$form->getElement('route')->setMultiOptions($route);
        }
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        	$smppValues = $form->getValues();  

        	$routeRepo = $emUser->getRepository('modules\user\models\SmppRoute')->findOneBy(array('route' => $smppValues['route']));
        	$type = $routeRepo->type;
        	
        	$smppManager = new SmscManager();
        	$smppCreated = $smppManager->createSmsc($smppValues, $type, $emSmpp);
        	if (isset($smppCreated)) {
        		$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
        		
        		$activityManager = new ActivityLogManager();
        		$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smsc Created Successfully', $this->_domain, $this->_ip, $this->_device, $smppCreated->id, 'CREATE', 'SMPP SMSC ACCOUNT', $emUser);
			  	
        		$this->view->success = $this->view->success = array('user' => array('isSuccess' => 'Smpp Smsc account created'));
        	}
        }else {
        	$this->view->errors = $form->getMessages();        
    	}
    	//$form->reset();
    }

    public function smscsAction()
    {

        $this->view->subject="SMPP SMSC LIST";
    	$userManage = $this->view->navigation()->findOneByLabel('Smsc');
    	if ($userManage) {
    		$userManage->setActive();
    	}
    	$page = $this->view->navigation()->findOneByUri('/smpp/smsc/smscs');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Smsc | Smpp Smscs');
    	}
    	
    	$emSmpp = $this->getEntityManager('smpp');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	$emUser = $this->getEntityManager('user');
    	$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    	/*
    	$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    	$currentPage = 1;
    	$i = $this->_request->getParam('i');
    	if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    	}
    	$paginator->setCurrentPageNumber($currentPage)
    	          ->setItemCountPerPage(20);
    	
    	$this->view->entries = $paginator;
    	*/
    	// Block,Unblock,Activate Users,Resellers
    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
    	
    		if (isset($button['active'])) {
    			//Block User
    			$smppManager = new SmscManager();
    			$type = $button['type'];
    			$reset = $smppManager->blockSmsc($button['active'], $type, $emSmpp); 
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Smsc Blocked Successfully', $this->_domain, $this->_ip, $this->_device, $button['active'], 'BLOCK', 'SMPP SMSC', $emUser);
    			 
    		}
    	
    		if (isset($button['blocked'])) {
    			//Unblock User
    			$smppManager = new SmscManager();
    			$type = $button['type'];
    			$reset = $smppManager->unblockSmsc($button['blocked'], $type, $emSmpp);    
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Smsc Activated Successfully', $this->_domain, $this->_ip, $this->_device, $button['blocked'], 'ACTIVATE', 'SMPP SMSC', $emUser);
    			 
    		}
    		
    		if (isset($button['delete'])) {
    			//Delete User
    			$smppManager = new SmscManager();
    			$type = $button['type'];
    			$reset = $smppManager->deleteSmsc($button['delete'], $type, $emSmpp);    	
    			$activityManager = new ActivityLogManager();
    			$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Smsc Deleted Successfully', $this->_domain, $this->_ip, $this->_device, $button['delete'], 'DELETE', 'SMPP SMSC', $emUser);
    			 
    		}
            if (isset($button['edit'])) {
                //Delete User
                $smppManager = new SmscManager();
                $type = $button['type'];
                $reset = $smppManager->editSmsc($button['edit'], $button['system_id'], $button['ip'], $button['port'], $button['password'], $button['type'], $button['bind'],$button['Tx'],$button['Rx'], $button['tps'], $emSmpp);       
                $activityManager = new ActivityLogManager();
                $activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'Smpp Smsc Edited Successfully', $this->_domain, $this->_ip, $this->_device, $button['edit'], 'EDIT', 'SMPP SMSC', $emUser);
                 
            }
    	
    	}
    	$queryBuilder = $emSmpp->createQueryBuilder();
    	$queryBuilder->select('e')->from('modules\smpp\models\Smsc', 'e');
    	$query = $queryBuilder->getQuery();
    	$this->view->entries = $query->getResult();
    }

    public function statusAction()
    {
        $this->view->subject="MONITOR";
    	$userManage = $this->view->navigation()->findOneByLabel('Monitor');
    	if ($userManage) {
    		$userManage->setActive();
    	}
        $page = $this->view->navigation()->findOneByUri('/smpp/smsc/status');
    	if ($page && $userManage) {
    		$userManage->setActive();
    		$page->setActive();
    		$this->view->headTitle('Monitor | Smsc Operator');
    	}
    	$form = new Smpp_Form_Smsc();
    	$emUser = $this->getEntityManager('user');
        $emSmpp = $this->getEntityManager('smpp');
    	$smscRepos = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('status' => '1'));
    	foreach ($smscRepos as $smsc) {
        	$smscs[$smsc->route] = $smsc->route;
        }
        if (isset($smscs)) {
        	$form->getElement('smsc')->setMultiOptions($smscs);
        }

        $serverRepos = $emSmpp->getRepository('modules\smpp\models\SmppServer')->findBy(array('type' => 'TRANS-OPERATOR'));
        $servers['0'] = 'SELECT SERVER';
        foreach ($serverRepos as $server) {
            $servers[$server->id] = $server->name;
        }

        $serverPromoRepos = $emSmpp->getRepository('modules\smpp\models\SmppServer')->findBy(array('type' => 'PROMO-OPERATOR'));
        foreach ($serverPromoRepos as $server) {
            $servers[$server->id] = $server->name;
        }

        $serverInterRepos = $emSmpp->getRepository('modules\smpp\models\SmppServer')->findBy(array('type' => 'INTER-OPERATOR'));
        foreach ($serverInterRepos as $server) {
            $servers[$server->id] = $server->name;
        }

        if (isset($servers)) {
            $form->getElement('server')->setMultiOptions($servers);
        }

    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
            $this->view->server = $button['server'];
            $form->getElement('server')->setValue($button['server']);
            $serverRepo = $emSmpp->getRepository('modules\smpp\models\SmppServer')->find($button['server']);
            $ip = $serverRepo->ip;
            $port = $serverRepo->port;
            $password = $serverRepo->password;
            $kannelConf = $serverRepo->kannelConf;
            $sqlboxConf = $serverRepo->sqlboxConf;

           
    		if (isset($button['command'])) {
    			switch ($button['command']) {
    				case "start":
    					shell_exec("/usr/local/sbin/bearerbox -v 4 /usr/local/sbin/$kannelConf > /dev/null 2> /dev/null &");
                        sleep (10);
    					//$this->_redirect('/smpp/smsc/status');
    					break;
    				case "resume":
    					file_get_contents("http://$ip:$port/resume?password=$password");
    					break;
    				case "restart":
    					file_get_contents("http://$ip:$port/restart?password=$password");
    					break;
    				case "isolate":
    					file_get_contents("http://$ip:$port/isolate?password=$password");
    					break;
    				case "suspend":
    					file_get_contents("http://$ip:$port/suspend?password=$password");
    					break;
    				case "shutdown":
    					shell_exec("/bin/kill -9 $(ps -ef | grep $kannelConf | grep -v grep | awk '{print $2}')");
    					break;
    			}
    		}elseif (isset($button['smsc-command'])) {
    			$smsc = $button['smsc'];
    			switch ($button['smsc-command']) {
    				case "start":
    					file_get_contents("http://$ip:$port/start-smsc?password=$password&smsc=$smsc");
    					break;
    				case "stop":
    					file_get_contents("http://$ip:$port/stop-smsc?password=$password&smsc=$smsc");
    					break;
    				case "add":
    					file_get_contents("http://$ip:$port/add-smsc?password=$password&smsc=$smsc");
    					break;
    				case "remove":
    					file_get_contents("http://$ip:$port/remove-smsc?password=$password&smsc=$smsc");
    					break;
    			}
    		}elseif (isset($button['sqlbox-command'])) {
    			switch ($button['sqlbox-command']) {
	    				case "start":
	    					shell_exec("/usr/local/sbin/pluginbox -v 4 /usr/local/sbin/$sqlboxConf > /dev/null 2> /dev/null &");
	    					sleep (10);
	    					break;
    					case "shutdown":
    						shell_exec("/bin/kill -9 $(ps -ef | grep $sqlboxConf | grep -v grep | awk '{print $2}')");
    						break;
                                        case "reload":
    						shell_exec("/bin/kill -9 $(ps -ef | grep $sqlboxConf | grep -v grep | awk '{print $2}')");
                                                shell_exec("/usr/local/sbin/pluginbox -v 4 /usr/local/sbin/$sqlboxConf > /dev/null 2> /dev/null &");
	    					sleep (10);
                                                break;
    			}
    		}	

             $this->view->status = file_get_contents("http://$ip:$port/status.xml?password=$password");
						
    	}
    	$ip = $this->_request->getServer('SERVER_ADDR');

    	$this->view->form = $form;
    	
    	 
    }

    public function cstatusAction()
    {
        $this->view->subject="MONITOR";
    	$userManage = $this->view->navigation()->findOneByLabel('Monitor');
    	if ($userManage) {
    		$userManage->setActive();
    	}
        $page = $this->view->navigation()->findOneByUri('/smpp/smsc/cstatus');
    	if ($page && $userManage) {
    		$userManage->setActive();
    		$page->setActive();
    		$this->view->headTitle('Monitor | Smsc Client');
    	}
    	$form = new Smpp_Form_Smsc();
    	$emUser = $this->getEntityManager('user');
        $emSmpp = $this->getEntityManager('smpp');
    	$smscRepos = $emUser->getRepository('modules\user\models\SmsRoute')->findBy(array('status' => '1'));
    	foreach ($smscRepos as $smsc) {
        	$smscs[$smsc->route] = $smsc->route;
        }
        if (isset($smscs)) {
        	$form->getElement('smsc')->setMultiOptions($smscs);
        }

        $serverRepos = $emSmpp->getRepository('modules\smpp\models\SmppServer')->findBy(array('type' => 'TRANS-CLIENT'));
        $servers['0'] = 'SELECT SERVER';
        foreach ($serverRepos as $server) {
            $servers[$server->id] = $server->name;
        }

        $serverPromoRepos = $emSmpp->getRepository('modules\smpp\models\SmppServer')->findBy(array('type' => 'PROMO-CLIENT'));
        foreach ($serverPromoRepos as $server) {
            $servers[$server->id] = $server->name;
        }

        $serverInterRepos = $emSmpp->getRepository('modules\smpp\models\SmppServer')->findBy(array('type' => 'INTER-CLIENT'));
        foreach ($serverInterRepos as $server) {
            $servers[$server->id] = $server->name;
        }

        if (isset($servers)) {
            $form->getElement('server')->setMultiOptions($servers);
        }

    	if($this->getRequest()->isPost() && $this->getRequest()->getPost()){
    		$button = $this->getRequest()->getPost();
            $this->view->server = $button['server'];
            $form->getElement('server')->setValue($button['server']);
            $serverRepo = $emSmpp->getRepository('modules\smpp\models\SmppServer')->find($button['server']);
            $ip = $serverRepo->ip;
            $port = $serverRepo->port;
            $password = $serverRepo->password;
            $kannelConf = $serverRepo->kannelConf;
            $sqlboxConf = $serverRepo->sqlboxConf;

           
    		if (isset($button['command'])) {
    			switch ($button['command']) {
    				case "start":
    					shell_exec("/usr/local/sbin/bearerbox -v 4 /usr/local/sbin/$kannelConf > /dev/null 2> /dev/null &");
                                        sleep (10);
    					//$this->_redirect('/smpp/smsc/status');
    					break;
    				case "resume":
    					file_get_contents("http://$ip:$port/resume?password=$password");
    					break;
    				case "restart":
    					file_get_contents("http://$ip:$port/restart?password=$password");
    					break;
    				case "isolate":
    					file_get_contents("http://$ip:$port/isolate?password=$password");
    					break;
    				case "suspend":
    					file_get_contents("http://$ip:$port/suspend?password=$password");
    					break;
    				case "shutdown":
    					shell_exec("/bin/kill -9 $(ps -ef | grep $kannelConf | grep -v grep | awk '{print $2}')");
                                        sleep (5);
    					break;
    			}
    		}elseif (isset($button['smsc-command'])) {
    			$smsc = $button['smsc'];
    			switch ($button['smsc-command']) {
    				case "start":
    					file_get_contents("http://$ip:$port/start-smsc?password=$password&smsc=$smsc");
    					break;
    				case "stop":
    					file_get_contents("http://$ip:$port/stop-smsc?password=$password&smsc=$smsc");
    					break;
    				case "add":
    					file_get_contents("http://$ip:$port/add-smsc?password=$password&smsc=$smsc");
    					break;
    				case "remove":
    					file_get_contents("http://$ip:$port/remove-smsc?password=$password&smsc=$smsc");
    					break;
    			}
    		}elseif (isset($button['sqlbox-command'])) {
    			switch ($button['sqlbox-command']) {
	    				case "start":
	    					shell_exec("/usr/local/sbin/sqlbox -v 4 /usr/local/sbin/$sqlboxConf > /dev/null 2> /dev/null &");
	    					sleep (10);
	    					break;
    					case "shutdown":
    						shell_exec("/bin/kill -9 $(ps -ef | grep $sqlboxConf | grep -v grep | awk '{print $2}')");
                                                sleep (5);
    						break;
    			}
    		}	

             $this->view->status = file_get_contents("http://$ip:$port/status.xml?password=$password");
						
    	}
    	$ip = $this->_request->getServer('SERVER_ADDR');

    	$this->view->form = $form;
        
         
    }
    
    public function sentsmsAction()
    {
    	$this->view->subject="SENT SMS";
        $userManage = $this->view->navigation()->findOneByLabel('Smsc');
        if ($userManage) {
            $userManage->setActive();
        }
    	$page = $this->view->navigation()->findOneByUri('/smpp/smsc/sentsms');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Smsc | Smsc Recredit');
    	}
    	
    	$form = new Smpp_Form_Smsc();
    	$emUser = $this->getEntityManager('user');
    	$emSmpp = $this->getEntityManager('smpp');
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	
    	$smppRouteRepo = $emUser->getRepository('modules\user\models\SmppRoute')->findBy(array('status' => '1'));
    	 
    	 
    	foreach ($smppRouteRepo as $smppRoute){
    		$smppRoutes[$smppRoute->route] = $smppRoute->route.' | '.$smppRoute->type;
    	}
    	if (isset($smppRoutes)) {
    		$form->getElement('smsc')->setMultiOptions($smppRoutes);
    	}
    	$this->view->form = $form;
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
    		$smppValues = $form->getValues();
    	
    		$smscId = $smppValues['smsc'];
    		
    		$queryBuilder = $emSmpp->createQueryBuilder();
    		$queryBuilder->select('e')
    		->from('modules\smpp\models\SmscRecredit', 'e')
    		->where('e.smscId = ?1')
    		->orderBy('e.id', 'DESC')
    		->setParameter(1, $smscId);
    		$query = $queryBuilder->getQuery();
    		$this->view->entries = $query->getResult();
    		$this->view->emUser = $emUser;
    		
    	}else{
    		$this->view->entries = array();
    	}
    
    	
    	/*
    		$paginator = new Zend_Paginator(new Smpp_Paginator_Adapter_Doctrine($queryBuilder));
    		$currentPage = 1;
    		$i = $this->_request->getParam('i');
    		if(!empty($i)){ //Where i is the current page
    		$currentPage = $this->_request->getParam('i');
    		}
    		$paginator->setCurrentPageNumber($currentPage)
    		->setItemCountPerPage(20);
    
    		$this->view->entries = $paginator;
    		*/
    }
    
    public function searchAction()
    {
     $this->view->subject="SEARCH SENT SMS";
    	$userManage = $this->view->navigation()->findOneByLabel('Smpp');
    	if ($userManage) {
    		$userManage->setActive();
    	}
        $page = $this->view->navigation()->findOneByUri('/smpp/smsc/search');
    	if ($page) {
    		$page->setActive();
    		$this->view->headTitle('Smpp | Search');
    	}
    	$emSmpp = $this->getEntityManager('smpp');
    	
    	$form = new Smpp_Form_Search();
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;

    	$form->getElement('datePicker')->setValue(date('m/d/Y'));
    	$this->view->form = $form;
    	 
    	if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
        		$smppValues = $form->getValues();  
        	
        		$sdate = $smppValues['datePicker']; 	
        		$mobile = trim($smppValues['mobile']);
        		$sender = trim($smppValues['sender']);
        		$type = $smppValues['type'];
        		$startDate=strtotime($sdate.'00:00:00');
        		$endDate=strtotime($sdate.'23:59:59');
        		
        		if ($type == 'TRANSACTIONAL'){
        			//$table = 'modules\smpp\models\SmppSmsLogMtTrans';
                            $table = 'smpp_log_mt_trans';
        		}elseif ($type == 'PROMOTIONAL'){
        			//$table = 'modules\smpp\models\SmppSmsLogMtPromo';
                             $table = 'smpp_log_mt_promo';
        		}elseif ($type == 'INTERNATIONAL'){
        			//$table = 'modules\smpp\models\SmppSmsLogMtInter';
                             $table = 'smpp_log_mt_inter';
        		}
        		
		    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
		    	$queryBuilder = $emSmpp->createQueryBuilder();
    			if ((isset($sender) && isset($mobile)) && ($mobile != "" && $sender !== "")){
    				
//		    		$queryBuilder->select('e')
//		    		->from("$table", 'e')
//		    		->where('e.receiver LIKE ?1')
//		    		->andwhere('e.sender = ?4')
//		    		->andwhere('e.time >= ?2')
//		    		->andwhere('e.time <= ?3')
//		    		->setParameter(2, $startDate)
//		    		->setParameter(3, $endDate)
//		    		->setParameter(1, '%'.$mobile)
//		    		->setParameter(4, $sender);
//                                $queryBuilder->setMaxResults('10');
//		    		$query = $queryBuilder->getQuery();
//		    		$this->view->entries = $query->getResult();
                            
                                $query = $emSmpp->getConnection();
                                $result = $query->query("SELECT * FROM `$table` WHERE `sender` = '$sender' AND `receiver` = '$mobile' AND `time` >= '$startDate' AND time <= '$endDate' LIMIT 100")->fetchAll();
		    		$this->view->entries = $result;
		    		
		    	}elseif(isset($mobile) && $mobile != ""){
		    		
//		    		$queryBuilder->select('e')
//		    		->from("$table", 'e')
//		    		->where('e.receiver LIKE ?1')
//		    		->andwhere('e.time >= ?2')
//		    		->andwhere('e.time <= ?3')
//		    		->setParameter(2, $startDate)
//		    		->setParameter(3, $endDate)
//		    		->setParameter(1, '%'.$mobile);
//
//		    		$query = $queryBuilder->getQuery();
//		    		$this->view->entries = $query->getResult();
                            
                                $query = $emSmpp->getConnection();
                                $result = $query->query("SELECT * FROM `$table` WHERE receiver LIKE '%$mobile' AND `time` >= '$startDate' AND time <= '$endDate' LIMIT 100")->fetchAll();
		    		$this->view->entries = $result;
		    		
		    	}elseif (isset($sender) && $sender != ""){
		    		
//		    		$queryBuilder->select('e')
//		    		->from("$table", 'e')
//		    		->where('e.sender = ?1')
//		    		->andwhere('e.time >= ?2')
//		    		->andwhere('e.time <= ?3')
//		    		->setParameter(2, $startDate)
//		    		->setParameter(3, $endDate)
//		    		->setParameter(1, $sender);
//
//		    		$query = $queryBuilder->getQuery();
//                              $this->view->entries = $query->getResult();
                                
                                $query = $emSmpp->getConnection();
                                $result = $query->query("SELECT * FROM `$table` WHERE sender = '$sender' AND `time` >= '$startDate' AND time <= '$endDate' LIMIT 100")->fetchAll();
		    		
		    		$this->view->entries = $result;
		    		
		    	}else{
		    		$this->view->entries = array();
		    	}	
		    	
        	
    	}else{
    		$this->view->entries = array();
    	}
    }

}