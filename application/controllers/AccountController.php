<?php

use modules\user\models\UserManager;
use modules\user\models\UserRepository;
use Doctrine\ORM\Mapping\Id;
use modules\user\models\ActivityLogManager;
class AccountController extends Smpp_Controller_BaseController
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_redirect('/account/signin');
    }

    public function signinAction()
    {
    	$this->_redirectLoggedInUser();
    	$this->_helper->layout->disableLayout();
    	
    	$i = $this->_request->getParam('p');
    	
    	if(!empty($i)){ 
    		if ($i == 'ERR210'){
    			
    			$this->view->account = array('session' => array('timeout' => 'Session expired'));
    		}elseif ($i == 'ERR280'){
    			$this->view->account = array('csrf' => array('forbidden' => 'Token invalid'));
    		}
    	}
    	
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();    	
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	
    	if ($domainLayout){
    		$view = $domainLayout.'-signin';
    		$this->_helper->viewRenderer("$view");
    	}
    	
        $form = new Application_Form_Login();
        $this->view->form = $form;
        if ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())) {
			$userValues = $form->getValues();
			$identity = $userValues['username'];
			$password = $userValues['password'];
			
			$userAuth = new \Smpp_Auth_User();
			$emUser = $this->getEntityManager('user');
			$login = $userAuth->login($identity, $password, $emUser);
			
			if ($login == '1') {
				$activityManager = new ActivityLogManager();
				$userId = Zend_Auth::getInstance()->getIdentity()->id;
				$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
				$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'User Logged In Successfully', $this->_domain, $this->_ip, $this->_device, NULL, 'LOGIN', 'ACCOUNT', $emUser);
				
				$this->_redirect('/user/dashboard/index');
			}elseif ($login == '2') {
				$this->view->errors = array('user' => array('notActivated' => 'User account is not activated'));
			}elseif ($login == '3') {
				$this->view->errors = array('user' => array('isBlocked' => 'User account is blocked'));
			}else {
				$this->view->errors = array('user' => array('invalid' => 'Username or password is incorrect'));
			}
        }else{
        	//Protection against CSRF attack
        	if (count($form->getErrors('token')) > 0) {
        		//return $this->_forward('csrf-forbidden', 'error');
        		//throw new Zend_Controller_Dispatcher_Exception('CSRF Forbidden');
        		$this->_redirect('/account/signin/ERR280');
        	}
        	$this->view->errors = $form->getMessages();
        }
    }

    public function signupAction()
    {
    	$form = new Application_Form_User();
    	$this->_helper->layout->disableLayout();

    	$i = $this->_request->getParam('p');
    	 
    	if(!empty($i)){
    		if ($i == 'ERR210'){    			 
    			$this->view->account = array('session' => array('timeout' => 'Session expired'));
    		}elseif ($i == 'ERR280'){
    			$this->view->account = array('csrf' => array('forbidden' => 'Token invalid'));
    		}
    	}
    	
    	$emUser = $this->getEntityManager('user');
    	$domain = Smpp_Utility::getFqdn();
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	$domainLayout = $domainRepo->layout;
    	 
    	if ($domainLayout != null){
    		$view = $domainLayout.'-signup';
    		$this->_helper->viewRenderer("$view");
    	}
    	
        $this->view->form = $form;
        if($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getPost())){
			$userValues = $form->getValues();
			        	
        	//Create User
        	$userManager = new UserManager();
        	$userCreated = $userManager->createUser($userValues,'USER', $emUser);        
        	if (isset($userCreated)) {
        		$username = $userCreated->username;
        		$token = $userCreated->activate;
        		$domain = $userCreated->domain->domain;
        		$activityManager = new ActivityLogManager();
        		$activityManager->createActivityLog($userCreated, $userCreated->id, $this->_module, $this->_controller, $this->_action, 'User Signup Successfully', $this->_domain, $this->_ip, $this->_device, NULL, 'SIGNUP', 'ACCOUNT', $emUser);
        		
        		/*
        		$send = $this->sendToken($token, $username, $domain);
        		if ($send){
        			$this->view->success = array('mail' => array('isSend' => 'Activation username send successfully',
        														 'username' => "$username"));
        		}
        		*/
        		$this->view->success = array('mail' => array('isSend' => 'User account created successfully',
        				'username' => "$username"));
        		
        		//$this->_redirect('/account/signin');
        	}

        }else{
        	if (count($form->getErrors('token')) > 0) {
        		//return $this->_forward('csrf-forbidden', 'error');
        		//throw new Zend_Controller_Dispatcher_Exception('CSRF Forbidden');
        		$this->_redirect('/account/signup/ERR280');
        	}
        	$this->view->errors = $form->getMessages();
        }
    }

    private function _redirectLoggedInUser()
    {
    	if (Zend_Auth::getInstance()->hasIdentity()) {
    		$this->_redirect('/user/dashboard/index');
    		Zend_Auth::getInstance();
    		return true;
    	}
    }

    public function signoutAction()
    {
    	@$userId = Zend_Auth::getInstance()->getIdentity()->id;
    	if(isset($userId)){
    		$emUser = $this->getEntityManager('user');
    		$activityManager = new ActivityLogManager();
    		$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
    		$activityManager->createActivityLog($userRepo, $userId, $this->_module, $this->_controller, $this->_action, 'User Logged Out Successfully', $this->_domain, $this->_ip, $this->_device, NULL, 'LOGOUT', 'ACCOUNT', $emUser);
    		 
    	}
    	
        $auth = new \Smpp_Auth_User();
        $logout = $auth->logout(); 
        $i = $this->_request->getParam('p');
        if(!empty($i)){ 
        	$this->_redirect("/account/signin/$i");
        }else {
        	$this->_redirect("/");
        }
        
    }

    public function activateAction()
    {
        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);
        $token = $this->_request->getParam('token');
        $emUser = $this->getEntityManager('user');
        $userManager = new UserManager();
        $activate = $userManager->activateUser($token, $emUser);
        
        if ($activate) {
        	$this->view->success = 'Account has been activated';
        }else{
        	$this->view->errors = 'Account activation failed';
        }
    }
    
    public function resetAction()
    {
    	$this->_helper->layout->disableLayout();
    	//$this->_helper->viewRenderer->setNoRender(true);
    	$token = $this->_request->getParam('token');
    	$emUser = $this->getEntityManager('user');
    	$userManager = new UserManager();
    	$reset = $userManager->resetUser($token, $emUser);
    	$domain = \Zend_Registry::get('fqdn');
    	
    	if ($reset) {
    		$userId = $reset->id;
    		$username =  $reset->profile->email;
    		$crypt = new \Smpp_Auth_Crypt();
    		$password = $crypt->generateToken();
    		$userManager = new UserManager();
    		$updatePassword = $userManager->updatePassword($userId, $password, $emUser);    		 
    		if ($updatePassword) {
    			$send = $this->sendAccount($password, $username, $domain);
    			if ($send){
    				$this->view->success = 'Password resetted successfully, please check your email for account details'; 
    			}   			
    		}else {
    			//Error
    			$this->view->errors = 'Password resetting failed';    			
    		}    		
    	}else {
    		$this->view->errors = 'Password resetting failed';
    	}
    	if ($this->getRequest()->isPost()) {
    		$postData = $this->getRequest()->getPost();
    		
    		$username = $postData['email'];
    		$crypt = new \Smpp_Auth_Crypt();
    		$token = $crypt->generateToken();
    		$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => $domain));
    		$userProfileRepo = $emUser->getRepository('modules\user\models\UserProfile')->findOneBy(array('email' => $username));
    		
    		if ($userProfileRepo->id){
    			$userRepo = $emUser->getRepository('modules\user\models\User')->findOneBy(array('profile' => $userProfileRepo->id ,'domain' => $domainRepo->id));
    			
    			if ($userRepo) {
    				$userRepo->reset = $token;
    				$userRepo->updatedDate = new \DateTime('now');
    				$emUser->persist($userRepo);
    				$emUser->flush();
    				
    				$send = $this->resetToken($token, $username, $domain);
    				if ($send){
    					$this->view->success = 'Please check your email to reset password';
    				}
    				 
    			}
    		}else {
    			$this->view->errors = 'User account not found';
    		}
    		
    	}
    }

    public function stopImpersonateAction()
    {
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(true);
    	$this->stopImpersonateUser();
    
    }

}