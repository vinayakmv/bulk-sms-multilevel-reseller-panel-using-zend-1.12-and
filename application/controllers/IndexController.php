<?php

class IndexController extends Smpp_Controller_BaseController
{

    public function init()
    {
        $this->_helper->layout->disableLayout();
        /* $fqdn = Smpp_Utility::getFqdn();
        if ($fqdn != 'localhost') {
        	$this->_redirect('/account/signin');
        } */
        parent::init();
    }

    public function indexAction()
    {
    	$domain = Smpp_Utility::getFqdn();
    	$emUser = $this->getEntityManager('user');
    	$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
    	@$landingIndex = $domainRepo->layout;
    	if ($landingIndex != null){
    		 $this->render("$landingIndex");
    	}
       
    }

    public function deniedAction()
    {
        // action body
    }

    public function licenseAction()
    {
        // action body
    }

    public function contactAction()
    {
        // action body
    }


}









