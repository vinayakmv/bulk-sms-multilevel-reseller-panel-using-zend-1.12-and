<?php

class ErrorController extends Smpp_Controller_BaseController
{

    public function errorAction()
    {
    	$this->_helper->layout()->disableLayout();
        $errors = $this->_getParam('error_handler');
        $fqdn = $this->getRequest()->getServer();
        $fqdn = $fqdn['HTTP_HOST'];
      
        $exception = $errors->exception;
        $log = new Zend_Log(
        		new Zend_Log_Writer_Stream(
        				"/var/www/html/webpanel/logs/$fqdn-applicationException.log"
        		)
        );
        $log->debug($exception->getMessage() . "\n" .
        		$exception->getTraceAsString());
        
        if (!$errors || !$errors instanceof ArrayObject) {
            $this->view->message = 'You have reached the error page';
            return;
        }
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $this->view->message = 'Page not found';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $priority = Zend_Log::CRIT;
                $this->view->message = 'Application error';
                break;
        }
        
        // Log exception, if logger available
        if ($log = $this->getLog()) {
            $log->log($this->view->message, $priority, $errors->exception);
            $log->log('Request Parameters', $priority, $errors->request->getParams());
        }
        
        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }
        
        $this->view->request   = $errors->request;
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }

    public function domainAction()
    {   
    	$this->_helper->layout->disableLayout();
        $this->view->message = 'Domain is blocked or not activated, Please contact support';        
    }


}



