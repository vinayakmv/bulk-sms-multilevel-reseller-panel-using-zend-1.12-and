<?php

class Application_Form_User extends Zend_Form
{

    public function init()
    {
        $this->setAction('signup');
    	$this->setMethod('post');
    	$this->setAttrib('class', 'pt-3');

        $mobile = new Zend_Form_Element_Text('mobile');
        $mobile->setDecorators(array('ViewHelper','Description'));
        $mobile->setAttrib('id', 'mobile');
        //$mobile->setAttrib('placeholder', 'Mobile');
        $mobile->addErrorMessage('Please enter a valid mobile number');
        $mobile->setAttrib('required','required');
        $mobile->setAttrib('class', 'form-control form-control-lg border-left-0');
        $mobile->addValidator(new Zend_Validate_StringLength(10,10));
        $mobile->addValidator('Digits');
        $mobile->addFilter(new Zend_Filter_HtmlEntities());
        $mobile->addFilter(new Zend_Filter_StripTags());
        $this->addElement($mobile);

        $country = new Zend_Form_Element_Text('country');
        $country->setDecorators(array('ViewHelper','Description'));
        //$country->setAttrib('placeholder', 'Place');
        $country->setAttrib('id', 'country');
        $country->setAttrib('class', 'form-control form-control-lg border-left-0');
        $country->addFilter(new Zend_Filter_HtmlEntities());
        $country->addFilter(new Zend_Filter_StripTags());
        $this->addElement($country);

    	$username = new Zend_Form_Element_Text('username');
    	$username->setDecorators(array('ViewHelper','Description'));
    	$username->setLabel('Username');
    	//$username->setAttrib('placeholder', 'Username');
    	$username->class = "form-control form-control-lg border-left-0";
    	//$username->addErrorMessage('Please enter a username');
    	$username->setRequired(true);

        $username->setAttrib('required','required');
        
    	//Add Validator
    	$username->addValidator(new Zend_Validate_StringLength(6,20));
    	//$username->addValidator(new Zend_Validate_EmailAddress(array('allow' => Zend_Validate_Hostname::ALLOW_DNS,'mx'    => true,'deep'  => true)));
    	$username->addValidator(new Smpp_Validate_Db_NoUserExists());
    	
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	$username->addFilter(new Zend_Filter_StringToLower());
    	//Add Username Element
    	$this->addElement($username);
    	
    	$email = new Zend_Form_Element_Text('email');
    	$email->setDecorators(array('ViewHelper','Description'));
    	$email->setLabel('Username');
    	//$email->setAttrib('placeholder', 'Email');
    	$email->class = "form-control form-control-lg border-left-0";
    	//$email->addErrorMessage('Please enter a username');
    	$email->setRequired(true);
    	
    	$email->setAttrib('required','required');
    	
    	//Add Validator
    	//$email->addValidator(new Zend_Validate_StringLength(6,20));
    	//$email->addValidator(new Zend_Validate_EmailAddress());
    	//$email->addValidator(new Smpp_Validate_Db_NoUserExists());
    	 
    	//Add Filter
    	$email->addFilter(new Zend_Filter_HtmlEntities());
    	$email->addFilter(new Zend_Filter_StripTags());
    	$email->addFilter(new Zend_Filter_StringToLower());
    	//Add Username Element
    	$this->addElement($email);
    	
        $name = new Zend_Form_Element_Text('name');
        $name->setDecorators(array('ViewHelper',
                                        'Description'));
        $name->setLabel('Name');
        //$name->setAttrib('placeholder', 'Name');
        $name->class = "form-control form-control-lg border-left-0";
        $name->setAttrib('required','required');
        $name->addErrorMessage('Please enter a name');
        $name->setRequired(true);
        $name->addFilter(new Zend_Filter_HtmlEntities());
        $name->addFilter(new Zend_Filter_StripTags());
        //Add name Element
        $this->addElement($name);
    	
    	//Create Password Object.
    	$password = new Zend_Form_Element_Password('password');
    	$password->setDecorators(array('ViewHelper',
						    			'Description'));
    	$password->setLabel('Password');
    	//$password->setAttrib('placeholder', 'Password');
    	$password->class = "form-control form-control-lg border-left-0";
    	$password->setRequired(true);
    	//Add Validator
        $password->setAttrib('required','required');
    	//Add Filter
    	$password->addFilter(new Zend_Filter_HtmlEntities());
    	$password->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($password);
    	
    	//Create Password Object.
    	$repassword = new Zend_Form_Element_Password('repassword');
    	$repassword->setDecorators(array('ViewHelper',
						    			'Description'));
    	$repassword->setLabel('Confirm');
    	//$repassword->setAttrib('placeholder', 'Confirm Password');
    	$repassword->class = "form-control form-control-lg border-left-0";
    	$repassword->setRequired(true);
    	//Add Validator
    	$repassword->addValidator('Identical', false, array('token' => 'password'));
    	//Add Filter
    	$repassword->addFilter(new Zend_Filter_HtmlEntities());
    	$repassword->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($repassword);
    	
    	//Protection against CSRF attack
    	$token = new Zend_Form_Element_Hash('token');
    	$token->setSalt(md5(uniqid(rand(), TRUE)));
    	
    	//$token->setTimeout(Globals::getConfig()->authentication->timeout);
    	$token->setDecorators(array('ViewHelper','Description'));
    	$this->addElement($token);
    	
    	//Role Hidden element
    	/*
    	$roleId = new Zend_Form_Element_Hidden('role');
    	$roleId->setValue('user');
    	$roleId->setDecorators(array('ViewHelper',
						    			'Description',
						    	
						    			array(array('data'=>'HtmlTag'), array('tag' => 'td')),
						    			array('Label', array('tag' => 'td')),
						    			array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
						    	  ));
    	$this->addElement($roleId);
    	*/
    	
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn-primary btn");
    	$submitElement->setDecorators(array('ViewHelper','Description','Errors'));
    	$submitElement->setLabel('SignUp');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'));
    }


}

