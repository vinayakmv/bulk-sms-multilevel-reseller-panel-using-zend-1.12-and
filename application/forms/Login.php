<?php

class Application_Form_Login extends Zend_Form
{
	
    public function init()
    {
        $this->setAction('signin');
    	$this->setMethod('post');
    	$this->setAttrib('class', "pt-3");
    	$username = new Zend_Form_Element_Text('username');
    	$username->setDecorators(array('ViewHelper',
						    			'Description'));
    	//$username->setLabel('Username');
    	//$username->setAttrib('placeholder', 'Username');
		$username->setAttrib('required', 'required');
    	$username->class = "form-control form-control-lg border-left-0";
    	//$username->addErrorMessage('Please enter a username');
    	$username->setRequired(true);
    
    	//Add Validator
    	//$username->addValidator(new Zend_Validate_StringLength(6,20));
    	//$username->addValidator(new Zend_Validate_EmailAddress());
    	
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);
    	
    	//Create Password Object.
    	$password = new Zend_Form_Element_Password('password');
    	$password->setDecorators(array('ViewHelper',
						    			'Description'));
    	//$password->setLabel('Password');
    	//$password->setAttrib('placeholder', 'Password');
		$password->setAttrib('required', 'required');
    	$password->class = "form-control form-control-lg border-left-0";
    	$password->setRequired(true);
    	//Add Validator
    	 
    	//Add Filter
    	$password->addFilter(new Zend_Filter_HtmlEntities());
    	$password->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($password);
    	
    	//Protection against CSRF attack
    	$token = new Zend_Form_Element_Hash('token');
    	$token->setSalt(md5(uniqid(rand(), TRUE)));
		
    	//$token->setTimeout(Globals::getConfig()->authentication->timeout);
    	$token->setDecorators(array('ViewHelper','Description'));
    	$this->addElement($token);
    	
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"btn btn-lg btn-primary btn-block");
    	$submitElement->setDecorators(array('ViewHelper',
							    			'Description'));
    	$submitElement->setLabel('Sign In');
    	
    	$this->setDecorators(array('FormElements',
				'Form'
		));
    }


}

