<?php

class Application_Form_NewUser extends Zend_Form
{

    public function init()
    {
        $this->setAction('signup');
    	$this->setMethod('post');
    	//$this->setDecorators(array('row'=>'HtmlTag'),array('tag'=>'div', 'style' => 'clear:both;'));
    
    	$username = new Zend_Form_Element_Text('username');
    	$username->setDecorators(array('ViewHelper','Description'));
    	//$username->setLabel('Username');
    	$username->setAttrib('size', 30);
    	//$username->setAttrib('placeholder', 'Username');
    	$username->class = "text-input";
    	//$username->addErrorMessage('Please enter a username');
    	$username->setRequired(true);
    
    	//Add Validator
    	$username->addValidator(new Zend_Validate_StringLength(6,20));
    	//$username->addValidator(new Zend_Validate_EmailAddress(array('allow' => Zend_Validate_Hostname::ALLOW_DNS,'mx'=> true,'deep'=> true)));
    	$username->addValidator(new Smpp_Validate_Db_NoUserExists());
    	
    	//Add Filter
    	$username->addFilter(new Zend_Filter_HtmlEntities());
    	$username->addFilter(new Zend_Filter_StripTags());
    	//Add Username Element
    	$this->addElement($username);
    	
    	//Create Password Object.
    	$password = new Zend_Form_Element_Password('password');
    	$password->setDecorators(array('ViewHelper','Description'));
    	//$password->setLabel('Password');
    	$password->setAttrib('size', 30);
    	//$password->setAttrib('placeholder', 'Password');
    	$password->class = "text-input";
    	$password->setRequired(true);
    	//Add Validator
    	 
    	//Add Filter
    	$password->addFilter(new Zend_Filter_HtmlEntities());
    	$password->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($password);
    	
    	//Create Password Object.
    	$repassword = new Zend_Form_Element_Password('repassword');
    	$repassword->setDecorators(array('ViewHelper','Description'));
    	//$repassword->setLabel('Confirm');
    	$repassword->setAttrib('size', 30);
    	//$repassword->setAttrib('placeholder', 'Confirm Password');
    	$repassword->class = "text-input";
    	$repassword->setRequired(true);
    	//Add Validator
    	$repassword->addValidator('Identical', false, array('token' => 'password'));
    	//Add Filter
    	$repassword->addFilter(new Zend_Filter_HtmlEntities());
    	$repassword->addFilter(new Zend_Filter_StripTags());
    	//Add Password Element
    	$this->addElement($repassword);
    	
    	//Role Hidden element
    	$roleId = new Zend_Form_Element_Hidden('role');
    	$roleId->setValue('4de9927310');
    	$roleId->setDecorators(array('ViewHelper','Description'));
    	$this->addElement($roleId);
    	
    	//Create a submit button.
    	$this->addElement('submit', 'submit');
    	$submitElement = $this->getElement('submit');
    	$submitElement->setAttrib('class',"button");
    	$submitElement->setDecorators(array('ViewHelper','Description'));
    	$submitElement->setLabel('Sign Up');
    	
    	$this->setDecorators(array('FormElements',
					    			'Form'
					    	));
    }


}

