<?php
include 'init.php';

// Entity manager.
$em = $container->getEntityManager();

$sm = $container->getConnection()->getSchemaManager();
$fKeys = array();

// Data fixtures
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Loader;

$loader = new Loader();
$loader->loadFromDirectory(APPLICATION_PATH . '/../database/fixtures');

$fixtures = $loader->getFixtures();

$purger = new ORMPurger();

// Drop all foreign keys before purging
dropAllForeignKeys();

$executor = new ORMExecutor($em, $purger);

// Purge
$executor->purge();

// Recreate all foreign keys after data loading
createAllForeignKeys();

// Load data
$executor->execute($loader->getFixtures(), true);


echo 'Successfully loaded all fixtures.';
//----------------------- End Script -----------------------


/*
 * Functions
 */
function dropAllForeignKeys()
{
    global $fKeys, $sm;
    
    $tables = $sm->listTableNames();
    foreach ($tables as $table)
    {
        $foreignKeys = $sm->listTableForeignKeys($table);
        $constraints = array();
        foreach ($foreignKeys as $foreignKey)
        {
            array_push($constraints, $foreignKey);
            $sm->dropForeignKey($foreignKey, $table);
        }
        
        $fKeys[$table] = $constraints;
    }
}

function createAllForeignKeys()
{
    global $fKeys, $sm;
    
    foreach ($fKeys as $table => $fKeyArr)
    {
        foreach ($fKeyArr as $fKey)
        {
            $sm->createForeignKey($fKey, $table);
        }
    }
}