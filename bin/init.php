<?php
date_default_timezone_set('Asia/Kolkata');

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'setup'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/../database/fixtures'),
    realpath(APPLICATION_PATH),
    get_include_path(),
)));

// Zend_Application
require_once 'Zend/Config/Ini.php';
require_once 'Zend/Loader/Autoloader.php';

// Configure the resource autoloader
$autoloader = Zend_Loader_Autoloader::getInstance();
$resourceLoader = new Zend_Loader_Autoloader_Resource(
  array(
        'basePath' => realpath(APPLICATION_PATH . '/../library'),
        'namespace' => '',
    /*     'resourceTypes' => array(
        'sip' => array(
            'path' => '',
            'namespace' => 'Sip',
        )
    ) */
  )
);

$autoloader->setFallbackAutoloader(TRUE);

// Creating application
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

// Bootstrapping resources
$bootstrap = $application->bootstrap()->getBootstrap();
$bootstrap->bootstrap('Doctrine');
//Pass Doctrine --em=[environment]

    $environmentName = null;

    foreach ($_SERVER['argv'] as $index => $arg) {
        $e = explode('=', $arg);
        $key = str_replace('-', '', $e[0]);

        if ('em' == $key) {
            $environmentName = $e[1];
            unset($_SERVER['argv'][$index]);
        }
    }

// Retrieve Doctrine Container resource
$container = $application->getBootstrap()->getResource('doctrine');

// Register BLOB type
$entityManager = $container->getEntityManager($environmentName);
Doctrine\DBAL\Types\Type::addType('blob', 'Doctrine\DBAL\Types\BlobType');
$entityManager->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('BLOB', 'blob');
$entityManager->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('ENUM', 'string');
