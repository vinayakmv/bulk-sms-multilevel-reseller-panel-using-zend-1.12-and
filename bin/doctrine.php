<?php
include 'init.php';

// Console
$cli = new \Symfony\Component\Console\Application(
    'Doctrine Command Line Interface',
    \Doctrine\Common\Version::VERSION
);

try {
    // Bootstrapping Console HelperSet
    $helperSet = array();
    if (empty($environmentName) && !isset($container->defaultConnection)) {
        throw new \InvalidArgumentException('Try adding --em=[environment] option');
    }

    if (($dbal = $container->getConnection($environmentName ?: $container->defaultConnection)) !== null) {
        $helperSet['db'] = new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($dbal);
    }

    if (($em = $container->getEntityManager($environmentName ?: $container->defaultEntityManager)) !== null) {
        $helperSet['em'] = new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em);
    }

    $helperSet['dialog'] = new \Symfony\Component\Console\Helper\DialogHelper();
} catch (\Exception $e) {
    $cli->renderException($e, new \Symfony\Component\Console\Output\ConsoleOutput());
}

use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\Configuration\Configuration;

// Migration settings
$connection = $container->getConnection($environmentName ?: $container->defaultConnection);
$migConfig = new Configuration($connection);
$migConfig->setMigrationsTableName('schema_versions');
$migConfig->setMigrationsNamespace('Unrenovated\Migrations');
$migConfig->setMigrationsDirectory('../database/migrations/versions');
$migConfig->registerMigrationsFromDirectory('../database/migrations/versions');

$diffCmd = new \Doctrine\DBAL\Migrations\Tools\Console\Command\DiffCommand();
$migCmd = new \Doctrine\DBAL\Migrations\Tools\Console\Command\MigrateCommand();

$diffCmd->setMigrationConfiguration($migConfig);
$migCmd->setMigrationConfiguration($migConfig);

$cli->setCatchExceptions(true);
$cli->setHelperSet(new \Symfony\Component\Console\Helper\HelperSet($helperSet));

$cli->addCommands(array(
    // DBAL Commands
    new \Doctrine\DBAL\Tools\Console\Command\RunSqlCommand(),
    new \Doctrine\DBAL\Tools\Console\Command\ImportCommand(),

    // ORM Commands
    new \Doctrine\ORM\Tools\Console\Command\ClearCache\MetadataCommand(),
    new \Doctrine\ORM\Tools\Console\Command\ClearCache\ResultCommand(),
    new \Doctrine\ORM\Tools\Console\Command\ClearCache\QueryCommand(),
    new \Doctrine\ORM\Tools\Console\Command\SchemaTool\CreateCommand(),
    new \Doctrine\ORM\Tools\Console\Command\SchemaTool\UpdateCommand(),
    new \Doctrine\ORM\Tools\Console\Command\SchemaTool\DropCommand(),
    new \Doctrine\ORM\Tools\Console\Command\EnsureProductionSettingsCommand(),
    new \Doctrine\ORM\Tools\Console\Command\ConvertDoctrine1SchemaCommand(),
    new \Doctrine\ORM\Tools\Console\Command\GenerateRepositoriesCommand(),
    new \Doctrine\ORM\Tools\Console\Command\GenerateEntitiesCommand(),
    new \Doctrine\ORM\Tools\Console\Command\GenerateProxiesCommand(),
    new \Doctrine\ORM\Tools\Console\Command\ConvertMappingCommand(),
    new \Doctrine\ORM\Tools\Console\Command\RunDqlCommand(),
    
    // Migration Commands
	$diffCmd,
	$migCmd
));

$cli->run();
