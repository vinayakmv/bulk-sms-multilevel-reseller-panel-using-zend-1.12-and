<?php
require_once '/usr/share/pear/Mail.php';
require_once '/usr/share/pear/Mail/mime.php';
class Smpp_Service_Email
{
	protected $_mail = NULL;
	
    protected $_view = NULL;
    
    protected $_viewScriptPath = '/views/scripts/emails/';
    
	public function __construct()
	{
		$this->_setupView();
		$this->_setupSendmailTransport();
		$this->_mail = new Zend_Mail();
	}
	
	protected function _setupView ()
	{
		$this->_view = new Zend_View();
		$this->_view->setScriptPath(APPLICATION_PATH . $this->_viewScriptPath);
	}
	
	protected function _setupSendmailTransport ()
	{
		/*
		$returnpath = 'bounce@evoxtel.com';
		$transport = new Zend_Mail_Transport_Sendmail("-f $returnpath");
		//$transport = new Zend_Mail_Transport_Sendmail();
		Zend_Mail::setDefaultTransport($transport);
		*/
	}
	
	public function send($template, $data, $domain, $sendTo, $subject)
	{
		$ip = '';
		$fromEmail = 'noreply@'."evoxtel.com";
		$fromName = 'Service';
		
		$htmlMessage = $this->_view->partial(
				$template.'-html.phtml',
				array("user" => $sendTo,
						"domain" => $domain,
						"data" => $data)
		);
		
		$plaintextMessage = $this->_view->partial(
				$template.'-text.phtml',
				array("user" => $sendTo,
						"domain" => $domain,
						"data" => $data)
		);
		
		$host =     "in-v3.mailjet.com";
		$port =     "587";
		$username = "2d27f030e7dba120043cf57487e1a719";
		$password = "256ecdd2fc50ae03c37a5c96bd730f58";
		
		$crlf = "\n";
		
		$headers = array(
				'To' => "",
				'Bcc' => "",
				'From' => "$fromName<$fromEmail>",
				"replyto" => "$fromName<$fromEmail>",
				'Subject' => "$subject",
				'Importance' => 'high',
				'X-Abuse-Reports-To' =>'abuse@merabt.com',
				'X-Mailer: MERABT' => phpversion(),
				'MIME-Version'  => '1.0');
		
		$mime = new Mail_mime(array('eol' => $crlf));
		
		$mime->setTXTBody($plaintextMessage);
		$mime->setHTMLBody($htmlMessage);
		//$mime->addAttachment($file, 'text/plain');
		
		$body = $mime->get();
		$headers = $mime->headers($headers);
		
		$smtp = Mail::factory('smtp',
				array (
						'host' => $host,
						'port' => $port,
						'auth' => true,
						'username' => $username,
						'password' => $password));
		
		$result = $smtp->send($sendTo, $headers, $body);
		if ($result){
			return true;
		}
			return false;
		
		//if (PEAR::isError($mail)) die($mail->getMessage());
		/*
		$this->_mail = new Zend_Mail();
		$this->_mail->setBodyText($plaintextMessage);
		$this->_mail->setBodyHtml($htmlMessage);
		$this->_mail->setFrom($fromEmail, $fromName);
		$this->_mail->addTo($sendTo, '');
		$this->_mail->setSubject($subject);
		$this->_mail->setReplyto("$fromEmail");
		$this->_mail->addHeader('Importance', 'high');
		$this->_mail->addHeader('X-Priority', '1');
		$this->_mail->addHeader('X-Mailer', 'Mail 4.1');
		$this->_mail->addHeader('X-Verification', 'Verified');
		
		//send the message
		try {
			$result = $this->_mail->send();
			return true;
		} catch (Exception $e) {
			return false;
		}
		*/
	}
}