<?php
class Smpp_Acl_User extends Zend_Acl
{
	
	/**
	 * 
	 * @param unknown $em
	 * @param unknown $role
	 * @return boolean
	 */
	public function __construct($em, $role) 
	{
		
			$this->loadRoles($em);
			$this->loadResources($em);
			$this->loadPermissions($role, $em);
			
			return true;
	}
	
	/**
	 * 
	 * @param unknown $em
	 * @return boolean
	 */
	public function loadRoles($em) 
	{
		$allRoles = $em->getRepository('modules\user\models\Role')->findAll();
		foreach ($allRoles as $role) {
			$this->addRole(new Zend_Acl_Role($role->role));
		}
		return true;
	}
	
	/**
	 * 
	 * @param unknown $em
	 * @return boolean
	 */
	public function loadResources($em) 
	{
		$allResources = $em->getRepository('modules\user\models\Resource')->findAll();
		foreach ($allResources as $resource) {
			if (!$this->has($resource->resource)) {
				$this->addResource(new Zend_Acl_Resource($resource->resource));
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @param unknown $role
	 * @param unknown $em
	 * @return boolean
	 */	
	public function loadPermissions($role, $em)
	{
		$roleRepo = $em->getRepository('modules\user\models\Role')->findOneBy(array('id' => $role));
		$userRole = $roleRepo->role;
		$parentRole = $roleRepo->parent;
		$parentRoles = explode('-',$parentRole);
	
	
		//Deny all resources
		$allResources = $em->getRepository('modules\user\models\Resource')->findAll();
		foreach ($allResources as $resource) {
			$this->deny($userRole,$resource->resource);
		}
		//Allow only enabled resources
		if ($parentRole == '') {
			$userResources = $roleRepo->resources;
			foreach ($userResources as $resource) {
				$this->allow($userRole,$resource->resource);
			}
		}else {
			foreach ($parentRoles as $parentRoleId) {
				$parentRoleRepo = $em->getRepository('modules\user\models\Role')->findOneBy(array('id' => $parentRoleId));
				$userResources = $parentRoleRepo->resources;
				foreach ($userResources as $resource) {
					$this->allow($userRole,$resource->resource);
				}
			}
		}
	
		return true;
	}
	
}