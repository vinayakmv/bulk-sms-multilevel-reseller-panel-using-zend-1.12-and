<?php
class Smpp_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
	const MODULE_NO_AUTH='default';
	const MODULE_NO_AUTH_LICENSE='license';
	const MODULE_NO_AUTH_API='api';
	
	private $_controller;
	
	private $_module;
	
	private $_action;
	
	private $_role;
	 
	/**
	 * preDispatch
	 *
	 * @param Zend_Controller_Request_Abstract $request
	 */
	public function preDispatch (Zend_Controller_Request_Abstract $request)
	{
		$this->_controller = $this->getRequest()->getControllerName();
		$this->_module= $this->getRequest()->getModuleName();
		$this->_action= $this->getRequest()->getActionName();

		if ($this->_module == 'api') {
	        return true; // do not execute any more plugin code
	    }
		
		$fqdn = Smpp_Utility::getFqdn();
		$doctrine = Zend_Registry::get('doctrine');
		$em = $doctrine->getEntityManager('user');
		$domainRepo = $em->getRepository('modules\user\models\Domain');
		$domain = $domainRepo->findOneBy(array('domain' => $fqdn));
		
		if ((!isset($domain) || $domain->status != '1' || $domain->domain != $fqdn) && $fqdn != 'license.evoxtel.com') {
			$request->setModuleName('default');
			$request->setControllerName('error');
			$request->setActionName('domain');			
		}		
		
		$auth= Zend_Auth::getInstance();
		$redirect=true;
		if ($this->_module != self::MODULE_NO_AUTH && $this->_module != self::MODULE_NO_AUTH_LICENSE && $this->_module != self::MODULE_NO_AUTH_API) {
			if ($this->_isAuth($auth)) {
				$user= $auth->getStorage()->read();
				$this->_role = $user->role_id;
				
				//Checking ACL
				$acl= new Smpp_Acl_User($em,$this->_role);
				
				if ($this->_isAllowed($auth ,$acl, $em)) {
					$redirect=false;
				}
			}
		} else {
			$redirect=false;
		}
		 
		if ($redirect) {
			/*
			$request->setModuleName('default');
			$request->setControllerName('index');
			$request->setActionName('denied');
			*/
			$request->setModuleName('default');
			$request->setControllerName('account');
			$request->setActionName('signin');
		}
	}
	/**
	 * Check user identity using Zend_Auth
	 *
	 * @param Zend_Auth $auth
	 * @return boolean
	 */
	private function _isAuth (Zend_Auth $auth)
	{
		if (!empty($auth) && ($auth instanceof Zend_Auth)) {
			return $auth->hasIdentity();
		}
		return false;
	}
	/**
	 * Check permission using Zend_Auth and Zend_Acl
	 *
	 * @param Zend_Auth $auth
	 * @param Zend_Acl $acl
	 * @return boolean
	 */
	private function _isAllowed(Zend_Auth $auth, Zend_Acl $acl ,$em)
	{
		if (empty($auth) || empty($acl) ||
		!($auth instanceof Zend_Auth) ||
		!($acl instanceof Zend_Acl)) {
			return false;
		}
		$resources= array (
				'*/*/*',
				$this->_module.'/*/*',
				$this->_module.'/'.$this->_controller.'/*',
				$this->_module.'/'.$this->_controller.'/'.$this->_action
		);
		$result=false;
		$role = $em->getRepository('modules\user\models\Role')->findOneBy(array('id' => $this->_role));
		foreach ($resources as $res) {
			if ($acl->has($res)) {
				$result= $acl->isAllowed($role->role,$res);
			}
		}

		return $result;
	}
}