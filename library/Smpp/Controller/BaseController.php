<?php

use modules\user\models\UserManager;
use modules\user\models\Account;
class Smpp_Controller_BaseController extends Zend_Controller_Action
{	
	public $_controller;
		
	public $_module;
	
	public $_action;
	
	public $_domain;
	
	public $_ip;
	
	public $_device;
	
	/**
	 * (non-PHPdoc)
	 * @see Zend_Controller_Action::init()
	 */
	public function init()
	{
		$emUser = $this->getEntityManager('user');
		@$userId = Zend_Auth::getInstance()->getIdentity()->id;
		@$roleId = Zend_Auth::getInstance()->getIdentity()->role_id;
		@$domain = Smpp_Utility::getFqdn();
		
		//$userAgent = new Zend_Http_UserAgent();
		//$device    = $userAgent->getDevice();
   		$bootstrap = $this->getInvokeArg('bootstrap');
    	$userAgent = $bootstrap->getResource('useragent');
		

		$this->_controller = $this->getRequest()->getControllerName();
		$this->_module = $this->getRequest()->getModuleName();
		$this->_action = $this->getRequest()->getActionName();
		$this->_domain = $domain;
		$this->_ip = $_SERVER['REMOTE_ADDR'];
		$this->_device = $userAgent;

		$userManager = new UserManager();
		if (isset($userId)){
			$userRepo = $emUser->getRepository('modules\user\models\User')->find($userId);
			$domainRepo = $userRepo->domain;
		}else{
			$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => "$domain"));
		}
		if (isset($domainRepo)){
			$domainLevel = $domainRepo->level;
			$domainOwner = $domainRepo->owner;
			
			$this->view->email = $emUser->getRepository('modules\user\models\User')->findOneBy(array('id' => $domainRepo->owner))->profile->email;
			$this->view->whatsapp = $emUser->getRepository('modules\user\models\User')->findOneBy(array('id' => $domainRepo->owner))->profile->mobile;
			$this->view->footer = urldecode($domainRepo->footer);
			$this->view->logo = urldecode($domainRepo->logo);
			$this->view->introduction = urldecode($domainRepo->domainProfile->introduction);
			$this->view->address = urldecode($domainRepo->domainProfile->address);
			$this->view->about = urldecode($domainRepo->domainProfile->about);
			
			$this->view->domain = $domain;
		}
		
		if ($userId) {				 

			$roleRepo = $emUser->getRepository('modules\user\models\Role')->find($roleId);
			$resources = $roleRepo->resources;
			foreach ($resources as $resource){
				$res[$resource->id] = $resource->resource;
				$name = $resource->name;
				
			}
			$esmeCreate = array_search('smpp/esme/create', $res);
			$smsCompose = array_search('sms/campaign/index', $res);
			$voiceCompose = array_search('voice/campaign/index', $res);


			if (($domainLevel == '1' || $domainLevel == '0') && $domainOwner == $userId) {
				if ($smsCompose){
                                        $this->view->sms_main = true;
					$this->view->smsMainPromo = $domainRepo->smsPromoCredit;
					$this->view->smsMainTrans = $domainRepo->smsTransCredit;
					$this->view->smsMainScrub = $domainRepo->smsScrubCredit;
					$this->view->smsMainInter = $domainRepo->smsInterCredit;
					$this->view->smsMainPremi = $domainRepo->smsPremiCredit;
					$this->view->smsMainPromoApi = $domainRepo->smsPromoApiCredit;
					$this->view->smsMainTransApi = $domainRepo->smsTransApiCredit;
					$this->view->smsMainScrubApi = $domainRepo->smsScrubApiCredit;
					$this->view->smsMainInterApi = $domainRepo->smsInterApiCredit;
					$this->view->smsMainPremiApi = $domainRepo->smsPremiApiCredit;
				}else{
                                    $this->view->sms_main = false;
                                }

				if ($voiceCompose){
                                        $this->view->voice_main = true;
					$this->view->voiceMainPromo = $domainRepo->voicePromoCredit;
					$this->view->voiceMainTrans = $domainRepo->voiceTransCredit;
					$this->view->voiceMainPremi = $domainRepo->voicePremiCredit;
					$this->view->voiceMainPromoApi = $domainRepo->voicePromoApiCredit;
					$this->view->voiceMainTransApi = $domainRepo->voiceTransApiCredit;
					$this->view->voiceMainPremiApi = $domainRepo->voicePremiApiCredit;
				}else{
                                    $this->view->voice_main = false;
                                }
			}
			if ($smsCompose){
                                $this->view->sms_module = true;
				$this->view->smsPromo = $userRepo->account->smsPromoCredit;
				$this->view->smsTrans = $userRepo->account->smsTransCredit;
				$this->view->smsScrub = $userRepo->account->smsScrubCredit;
				$this->view->smsInter = $userRepo->account->smsInterCredit;
				$this->view->smsPremi = $userRepo->account->smsPremiCredit;
				$this->view->smsPromoApi = $userRepo->account->smsPromoApiCredit;
				$this->view->smsTransApi = $userRepo->account->smsTransApiCredit;
				$this->view->smsScrubApi = $userRepo->account->smsScrubApiCredit;
				$this->view->smsInterApi = $userRepo->account->smsInterApiCredit;
				$this->view->smsPremiApi = $userRepo->account->smsPremiApiCredit;
			}else{
                            $this->view->sms_module = false;
                        }
			if ($voiceCompose){
                                $this->view->voice_module = true;
				$this->view->voicePromo = $userRepo->account->voicePromoCredit;
				$this->view->voiceTrans = $userRepo->account->voiceTransCredit;
				$this->view->voicePremi = $userRepo->account->voicePremiCredit;
				$this->view->voicePromoApi = $userRepo->account->voicePromoApiCredit;
				$this->view->voiceTransApi = $userRepo->account->voiceTransApiCredit;
				$this->view->voicePremiApi = $userRepo->account->voicePremiApiCredit;
			}else{
                             $this->view->voice_module = false;
                        }
			if ($esmeCreate){
                                $this->view->smpp_module = true;
				$this->view->smppPromo = $userRepo->account->smppPromoCredit;
				$this->view->smppTrans = $userRepo->account->smppTransCredit;
				$this->view->smppScrub = $userRepo->account->smppScrubCredit;
				$this->view->smppInter = $userRepo->account->smppInterCredit;

				$this->view->maxTps = $userRepo->account->smppTps;
				$this->view->maxBind = $userRepo->account->smppBind;
				$this->view->maxEsme = $userRepo->account->smppUser;
			}else{
                             $this->view->smpp_module = false;
                        }
			$this->view->username = $userRepo->username;
//			$usersForActivation = $emUser->getRepository('modules\user\models\User')->findBy(array('reseller' => "$userId", 'status' => "2"));
//			$usersBlocked = $emUser->getRepository('modules\user\models\User')->findBy(array('reseller' => "$userId", 'status' => "3"));
//			$usersActive = $emUser->getRepository('modules\user\models\User')->findBy(array('reseller' => "$userId", 'status' => "1", 'role' => '1'));
//			$resellersActive = $emUser->getRepository('modules\user\models\User')->findBy(array('reseller' => "$userId", 'status' => "1", 'role' => '2'));
//			$this->view->usersForActivation = count($usersForActivation);
//			$this->view->usersBlocked = count($usersBlocked);
//			$this->view->usersActive = count($usersActive);
//			$this->view->resellersActive = count($resellersActive);
		}
		
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Zend_Controller_Action::preDispatch()
	 */
	public function preDispatch()
	{
		$domain = Smpp_Utility::getFqdn();
		Zend_Registry::set('fqdn', $domain);
	}
	
	/**
	 * Returns our Doctrine Entity Manager.
	 */
	protected function getEntityManager($module) {
		$doctrine = Zend_Registry::get('doctrine');
		return $doctrine->getEntityManager($module);
	}
	
	
	
	public function sendToken($token, $username, $domain)
	{
		//Send Activation Email
		$template = 'activation';
		$subject = 'User Account Verification';
		$emailService = new \Smpp_Service_Email();
		$send = $emailService->send($template, $token, $domain, $username, $subject);
		return $send;
	}
	
	public function resetToken($token, $username, $domain)
	{
		//Send Reset Email
		$template = 'reset';
		$subject = 'User Account Reset';
		$emailService = new \Smpp_Service_Email();
		$send = $emailService->send($template, $token, $domain, $username, $subject);
		return $send;
	}
	
	public function sendAccount($token, $username, $domain)
	{
		//Send Account Deatails Email
		$template = 'sendpassword';
		$subject = 'User Account Details';
		$emailService = new \Smpp_Service_Email();
		$send = $emailService->send($template, $token, $domain, $username, $subject);
		return $send;
	}
	
	public function startImpersonateUser($impersonateId)
	{
		$emUser = $this->getEntityManager('user');
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
		$session = new Zend_Session_Namespace('ImpersonateUser');
		$session->user_id = $userId;
		$session->role_id = Zend_Auth::getInstance()->getIdentity()->role_id;
		$session->username = Zend_Auth::getInstance()->getIdentity()->username;
		
		if ($session->role_id == '3') {
			$impersonateRepo = $emUser->getRepository('modules\user\models\User')->find($impersonateId);
			
		}else {
			$impersonateRepo = $emUser->getRepository('modules\user\models\User')->findOneBy(array('id' => $impersonateId, 'reseller' => $userId));
		}
		if (isset($impersonateRepo)) {
			Zend_Auth::getInstance()->getIdentity()->id = $impersonateRepo->id;
			Zend_Auth::getInstance()->getIdentity()->role_id = $impersonateRepo->role->id;
			Zend_Auth::getInstance()->getIdentity()->username = $impersonateRepo->username;
		}
	
		$this->_redirect('account/signin');
	}
	
	public function stopImpersonateUser()
	{
		$emUser = $this->getEntityManager('user');
		$session = new Zend_Session_Namespace('ImpersonateUser');
	
		$userRepo = $emUser->getRepository('modules\user\models\User')->find($session->user_id);
		Zend_Auth::getInstance()->getIdentity()->id = $userRepo->id;
		Zend_Auth::getInstance()->getIdentity()->role_id = $userRepo->role->id;
		Zend_Auth::getInstance()->getIdentity()->username = $userRepo->username;
		
		Zend_Session::namespaceUnset('ImpersonateUser');
		$this->_redirect('account/signin');
	}
	public function generateUuid()//Api Key generation
	{
		mt_srand((double)microtime()*10000);
		$charid = strtolower(md5(uniqid(rand(), true)));
		$salt =  substr($charid, 0, 8).substr($charid, 8, 4).substr($charid,12, 4).substr($charid,16, 4).substr($charid,20,12);
		return $salt;
	}
	
	public function setErrorHandler()
	{
		set_error_handler(
			create_function(
				'$severity, $message, $file, $line',
				'throw new ErrorException($message, $severity, $severity, $file, $line);'
			)
		);
	}



		
}
