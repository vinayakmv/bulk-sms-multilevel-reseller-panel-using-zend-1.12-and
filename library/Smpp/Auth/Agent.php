<?php
class Smpp_Auth_Agent
{
	protected $_authAdapter;
	
	protected $_userDbTableName = 'agents';
	
	protected $_identityColumn = 'username';
	
	/**
	 * 
	 * @param string $credentialColumn
	 * @return Zend_Auth_Adapter_DbTable
	 */
	protected function _getAuthAdapter($credentialColumn = 'password') 
	{
		$emUser = \Smpp_Utility::getEntityManager('user');
		//$emIvr = \Smpp_Utility::getEntityManager('ivr');
		$domain = \Zend_Registry::get('fqdn');
		$domainRepo = $emUser->getRepository('modules\user\models\Domain')->findOneBy(array('domain' => $domain));
		$domainId = $domainRepo->id;
		if ($this->_authAdapter === NULL) {
			$this->_authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Registry::get('agent'));
			$this->_authAdapter->setTableName($this->_userDbTableName)
			                   ->setIdentityColumn($this->_identityColumn)
			                   ->setAmbiguityIdentity(true)
			                   ->setCredentialColumn('password')
			                   ->setCredentialTreatment("SHA1(CONCAT(?,salt)) AND domain_id = $domainId");
		}
	
		return $this->_authAdapter;
	}
	
	/**
	 * 
	 * @param unknown $identity
	 * @param unknown $password
	 * @return boolean
	 */
	protected function _isCredentialsValid($identity, $password) 
	{
		$this->_getAuthAdapter()->setIdentity($identity)
		                        ->setCredential($password);
	
		$result = Zend_Auth::getInstance()->authenticate($this->_getAuthAdapter());
		
		if ($result->isValid()) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param unknown $identity
	 * @param unknown $password
	 * @param unknown $em
	 * @param string $data_field
	 * @return boolean
	 */
	public function login($identity, $password, $emUser, $data_field=null) 
	{

		if ($data_field != null) {
			$this->_identityColumn = $data_field;
		}
		if ($this->_isCredentialsValid($identity, $password)) {
			$user = $this->_getAuthAdapter()->getResultRowObject(array('status','domain_id'), 'password');
						
			if ($user->status == 1) {
				$this->_storeUserDataToZendAuth();
				return $user->status;
			}else {
				Zend_Auth::getInstance()->clearIdentity();				
				return $user->status;
			}
		}
	
		return false;
	}
	
	/**
	 * 
	 */
	protected function _storeUserDataToZendAuth() 
	{
		$userRow = $this->_getAuthAdapter()->getResultRowObject(null, array('salt', 'password'));
		Zend_Auth::getInstance()->getStorage()->write($userRow);
	}

	/**
	 * 
	 */
	public function logout() 
	{
		$auth = Zend_Auth::getInstance();
		$auth->clearIdentity();
		Zend_Session::destroy();
	}
}