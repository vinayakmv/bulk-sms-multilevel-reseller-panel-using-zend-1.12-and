<?php
class Smpp_Auth_Crypt
{
	/**
	 * 
	 * @return string
	 */
	public function generateSalt()
	{
		mt_srand((double)microtime()*10000);
		$charid = strtolower(md5(uniqid(rand(), true)));
		$salt =  substr($charid, 0, 8).substr($charid, 8, 4).substr($charid,12, 4).substr($charid,16, 4).substr($charid,20,12);
		return $salt;
	}
	
	/**
	 * 
	 * @param unknown $password
	 * @param unknown $salt
	 * @return string
	 */
	public function hashPassword($password, $salt)
	{
		$hashedPassword = SHA1($password.$salt);
		return $hashedPassword;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function generateToken()
	{
		mt_srand((double)microtime()*10000);
		$charid = strtolower(md5(uniqid(rand(), true)));
		$hyphen = chr(45);// "-"
		$salt =  substr($charid, 0, 8).$hyphen.substr($charid,12, 4).$hyphen.substr($charid,16, 4);
		return $salt;
	}
}