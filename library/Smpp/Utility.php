<?php
class Smpp_Utility
{
	public static function getFqdn($fqdn = "")
	{
		//$fqdn = $this->getRequest()->getServer();
		if (!$fqdn){
			@$fqdn = $_SERVER['HTTP_HOST'];
		}
		$domain = explode(chr(46),$fqdn);
		if($domain['0'] == 'www'){
			if(count($domain) > 3){
				return  $domain['1'].chr(46).$domain['2'].chr(46).$domain['3'];
			}
			return  $domain['1'].chr(46).$domain['2'];
		}else{
			return $fqdn;
		}
	}
	
	/**
	 * Returns our Doctrine Entity Manager.
	 */
	public static function getEntityManager($module) 
	{
		$doctrine = Zend_Registry::get('doctrine');
		return $doctrine->getEntityManager($module);
	}
	
	public static function secondsToTime($seconds) {
	    $dtF = new \DateTime('@0');
	    $dtT = new \DateTime("@$seconds");
	    return $dtF->diff($dtT)->format('%ad %hh %im %ss');
	}
}