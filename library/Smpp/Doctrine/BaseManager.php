<?php

class Smpp_Doctrine_BaseManager
{
	public function generateUuid()//Api Key generation
	{
		mt_srand((double)microtime()*10000);
		$charid = strtolower(md5(uniqid(rand(), true)));
		$salt =  substr($charid, 0, 8).substr($charid, 8, 4).substr($charid,12, 4);
		
		return $salt;
	}
	
	public function templateMatch($template = null, $message = null){
	
		$template = urldecode($template);
		$message = urldecode($message);
		
		$msgArray = $this->msgWordCount($message,'1');
		$tempArray = $this->msgWordCount($template,'1');
	
		$result = array_udiff($msgArray, $tempArray, 'strcasecmp');
		similar_text($message, $template, $percentage);
		$same = array_intersect($msgArray,$tempArray);
		$count = count($result);
		$tempCount = count($tempArray);
		$sameCount = count($same);
		$percentage = floor($percentage);
		
		//var_dump($count.'---'.$percentage.'-------'.$sameCount.'===='.$tempCount);
		if (($count <= '10') && ($percentage >= '60') && ($tempCount == $sameCount)){
			
			return true;
		}
	
		return false;
	
	}
	
	public function msgSpamCheck($keywords, $message){
		
		$keywords = urldecode($keywords);
		$message = urldecode($message);
		
		$msgArray = array_map('strtolower', $this->msgWordCount($message,'1'));
		$keywArray = array_map('strtolower', $this->msgWordCount($keywords,'1'));
		
		$result = array_intersect($msgArray, $keywArray);
		if (count($result)){
			return true;
		}
		return false;
	}
	
	
	public function msgWordCount($string, $format = null){
		if ($format != 1 && $format != 2 && $format !== null) {
			trigger_error("msgWordCount() The specified format parameter, '$format' is invalid", E_USER_WARNING);
			return false;
		}
	
		//$word_string = preg_replace('/[0-9]+/', '', $string);
		$word_array  = preg_split('/[^A-Za-z0-9_\']+/', $string, -1, PREG_SPLIT_NO_EMPTY);
	
		switch ($format) {
			case null:
				return count($word_array);
				break;
	
			case 1:
				return $word_array;
				break;
	
			case 2:
				$lastmatch = 0;
				$word_assoc = array();
				foreach ($word_array as $word) {
					$word_assoc[$lastmatch = strpos($string, $word, $lastmatch)] = $word;
				}
				return $word_assoc;
				break;
		}
	}
}