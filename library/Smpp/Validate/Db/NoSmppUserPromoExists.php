<?php
class Smpp_Validate_Db_NoSmppUserPromoExists extends Zend_Validate_Abstract
{
	const ERROR_USER_EXISTS = 'userExists';
	
	protected $_messageTemplates = array(
			self::ERROR_USER_EXISTS => "'%value%' is already in use"
	);
	
	public function isValid($value)
	{
		$this->_setValue($value);
		$valid = true;
		$em = Smpp_Utility::getEntityManager('smpp');
		$smppRepo = $em->getRepository("modules\smpp\models\SmppUserPromo")->find($value);
				
		if(isset($smppRepo) && count($smppRepo->systemId) > 0) {
			$this->_error(self::ERROR_USER_EXISTS);
			$valid = false;
			return $valid;
		}
		
		return $valid;
	}
}