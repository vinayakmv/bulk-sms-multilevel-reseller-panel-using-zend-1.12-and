<?php
class Smpp_Validate_Db_NoDomainExists extends Zend_Validate_Abstract
{
	const ERROR_DOMAIN_EXISTS = 'domainExists';
	
	protected $_messageTemplates = array(
			self::ERROR_DOMAIN_EXISTS => "'%value%' is already in use"
	);
	
	public function isValid($value)
	{
		$value = Smpp_Utility::getFqdn($value);
		$this->_setValue($value);
		$valid = true;
		$em = Smpp_Utility::getEntityManager('user');
		$domainRepo = $em->getRepository("modules\user\models\Domain");
		$domains = $domainRepo->getDomainByFqdn($value);
	
		if(count($domains) > 0) {
			$this->_error(self::ERROR_DOMAIN_EXISTS);
			$valid = false;
			return $valid;
		}
	
		return $valid;
	}
}