<?php
class Smpp_Validate_Db_IsMaxSmppUserPromo extends Zend_Validate_Abstract
{
	const ERROR_MAX_USER = 'maxSmppUser';
	
	protected $_messageTemplates = array(
			self::ERROR_MAX_USER => "maximum allowed smpp users reached"
	);
	
	public function isValid($value)
	{
		$valid = true;
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
		$em = Smpp_Utility::getEntityManager('user');
		$userRepo = $em->getRepository("modules\user\models\User")->find($userId);
		$maxSmpp = $userRepo->account->smppUser;
		
		if(count($userRepo->smppUsersPromo) >= $maxSmpp) {
			$this->_error(self::ERROR_MAX_USER);
			$valid = false;
		}
		
		return $valid;
	}
}