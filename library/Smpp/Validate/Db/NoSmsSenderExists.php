<?php
class Smpp_Validate_Db_NoSmsSenderExists extends Zend_Validate_Abstract
{
	const ERROR_SENDER_EXISTS = 'senderExists';
	
	protected $_messageTemplates = array(
			self::ERROR_SENDER_EXISTS => "'%value%' is already in use"
	);
	
	public function isValid($value)
	{
		$valueArray = explode(chr(10), $value);
		 
		foreach ($valueArray as $value){
			$value = (string)$value;
			$value = trim($value);
			
			$this->_setValue($value);
			$valid = true;
			$emSms = Smpp_Utility::getEntityManager('sms');
			$userId = Zend_Auth::getInstance()->getIdentity()->id;
			$senderRepo = $emSms->getRepository("modules\sms\models\SmsSenderId");
			$senders = $senderRepo->checkSender($value, $userId);
			
			if(count($senders) > 0) {
				$this->_error(self::ERROR_SENDER_EXISTS);
				return false;
			}
			
		}
		return $valid;
	}
}