<?php
class Smpp_Validate_Db_IsMaxSmppTps extends Zend_Validate_Abstract
{
	const ERROR_MAX_TPS = 'maxSmppTps';
	
	protected $_messageTemplates = array(
			self::ERROR_MAX_TPS => "'%value%' is more than allowed throughput"
	);
	
	public function isValid($value)
	{
		$this->_setValue($value);
		$valid = true;
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
		$em = Smpp_Utility::getEntityManager('user');
		$userRepo = $em->getRepository("modules\user\models\User")->find($userId);
		$maxSmpp = $userRepo->account->smppTps;
		
		if($value > $maxSmpp) {
			$this->_error(self::ERROR_MAX_TPS);
			$valid = false;
		}
		
		return $valid;
	}
}