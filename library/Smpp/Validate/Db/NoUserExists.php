<?php
class Smpp_Validate_Db_NoUserExists extends Zend_Validate_Abstract
{
	const ERROR_USER_EXISTS = 'userExists';
	
	protected $_messageTemplates = array(
			self::ERROR_USER_EXISTS => "'%value%' is already in use"
	);
	
	public function isValid($value)
	{
		$this->_setValue($value);
		$valid = true;
		$domain = Zend_Registry::get('fqdn');
		$em = Smpp_Utility::getEntityManager('user');
		$userRepo = $em->getRepository("modules\user\models\User");
		$users = $userRepo->checkUser($value, $domain);
		
		if(count($users) > 0) {
			$this->_error(self::ERROR_USER_EXISTS);
			$valid = false;
		}
		
		return $valid;
	}
}