<?php
class Smpp_Validate_Db_IsGroupExists extends Zend_Validate_Abstract
{
	const ERROR_USER_EXISTS = 'groupExists';
	
	protected $_messageTemplates = array(
			self::ERROR_USER_EXISTS => "'%value%' is already in use"
	);
	
	public function isValid($value)
	{
		$this->_setValue($value);
		$valid = true;
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
		$em = Smpp_Utility::getEntityManager('user');
		$groupRepo = $em->getRepository("modules\user\models\Lead")->findOneBy(array('user' => $userId, 'name' => $value));
		//$groups = $groupRepo->checkUser($value, $userId);
		
		if(count($groupRepo) > 0) {
			$this->_error(self::ERROR_USER_EXISTS);
			$valid = false;
		}
		
		return $valid;
	}
}