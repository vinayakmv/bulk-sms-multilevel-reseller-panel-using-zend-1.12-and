<?php
class Smpp_Validate_Db_NoVoiceSenderExists extends Zend_Validate_Abstract
{
	const ERROR_SENDER_EXISTS = 'senderExists';
	
	protected $_messageTemplates = array(
			self::ERROR_SENDER_EXISTS => "'%value%' is already in use"
	);
	
	public function isValid($value)
	{
		$valueArray = explode(chr(10), $value);
		 
		foreach ($valueArray as $value){
			$value = (string)$value;
			$value = trim($value);
			
			$this->_setValue($value);
			$valid = true;
			$em = Smpp_Utility::getEntityManager('voice');
			$userId = Zend_Auth::getInstance()->getIdentity()->id;
			$senderRepo = $em->getRepository('modules\voice\models\VoiceSenderId');
			$senders = $senderRepo->checkSender($value, $userId);
			
			if(count($senders) > 0) {
				$this->_error(self::ERROR_SENDER_EXISTS);
				return false;
			}
			
		}
		return $valid;
	}
}