<?php
class Smpp_Validate_Db_IsMaxSmppBind extends Zend_Validate_Abstract
{
	const ERROR_MAX_BIND = 'maxSmppBind';
	
	protected $_messageTemplates = array(
			self::ERROR_MAX_BIND => "'%value%' is more than allowed bind"
	);
	
	public function isValid($value)
	{
		$this->_setValue($value);
		$valid = true;
		$userId = Zend_Auth::getInstance()->getIdentity()->id;
		$em = Smpp_Utility::getEntityManager('user');
		$userRepo = $em->getRepository("modules\user\models\User")->find($userId);
		$maxSmpp = $userRepo->account->smppBind;
		
		if($value > $maxSmpp) {
			$this->_error(self::ERROR_MAX_BIND);
			$valid = false;
		}
		
		return $valid;
	}
}