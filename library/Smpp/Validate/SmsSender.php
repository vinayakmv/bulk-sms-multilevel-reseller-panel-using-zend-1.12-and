<?php

class Smpp_Validate_SmsSender extends Zend_Validate_Abstract
{
    const ALL_WHITESPACE = 'allWhitespace';
    const NOT_LONG       = 'notLong';
    const NO_NUMERIC     = 'noNumeric';
    const NO_ALPHA       = 'noAlpha';
    const NO_CAPITAL     = 'noCapital';
    const NO_SYMBOL      = 'noSymbol';

    protected $_minLength = 6;
    protected $_maxLength = 11;
    protected $_requireNumeric    = false;
    protected $_checkAlpha      = false;
    protected $_requireCapital    = false;
    protected $_checkSymbol    = false;

    protected $_messageTemplates = array(
        self::ALL_WHITESPACE => 'Sender cannot consist of all whitespace',
        self::NOT_LONG       => 'Sender must be in between %len% and 11 characters in length',
        self::NO_NUMERIC     => 'Sender must contain only numeric character',
        self::NO_ALPHA       => 'Sender must contain only alphabetic character',
        self::NO_CAPITAL     => 'Sender must contain at least one capital letter',
    	self::NO_SYMBOL     => 'Sender must not contain special character',
    );

    public function __construct($options = array())
    {        
        $this->_messageTemplates[self::NOT_LONG] = str_replace('%len%', $this->_minLength, $this->_messageTemplates[self::NOT_LONG]);

        if (isset($options['minLength'])
            && Zend_Validate::is($options['minLength'], 'Digits')
            && (int)$options['minLength'] > 3)
            $this->_minLength = $options['minLength'];

        if (isset($options['requireNumeric'])) $this->_requireNumeric = (bool)$options['requireNumeric'];
        if (isset($options['requireAlpha']))   $this->_checkAlpha   = (bool)$options['requireAlpha'];
        if (isset($options['requireCapital'])) $this->_requireCapital = (bool)$options['requireCapital'];
        if (isset($options['requireSymbol'])) $this->requireSymbol = (bool)$options['requireSymbol'];

    }

    /**
     * Validate a password with the set requirements
     * 
     * @see Zend_Validate_Interface::isValid()
     * @return bool true if valid, false if not
     */
    public function isValid($value, $context = null)
    {
    	$valueArray = explode(chr(10), $value);
    	
    	foreach ($valueArray as $value){
    		$value = (string)$value;
    		$value = trim($value);
    		$this->_setValue($value);
    		
    		if (trim($value) == '') {
    			$this->_error(self::ALL_WHITESPACE);
    		} else if (strlen($value) < $this->_minLength || strlen($value) > $this->_maxLength) {
    			$this->_error(self::NOT_LONG, $this->_minLength);
    		} else if ($this->_requireNumeric == true && preg_match('/\d/', $value) == false) {
    			$this->_error(self::NO_NUMERIC);
    		} else if ($this->_checkAlpha == true && preg_match('/^[A-z]+$/', $value) == false) {
    			$this->_error(self::NO_ALPHA);
    		} else if ($this->_requireCapital == true && preg_match('/[A-Z]/', $value) == false) {
    			$this->_error(self::NO_CAPITAL);
    		}else if ($this->_checkSymbol == true && preg_match('/[\W]/', $value) == true) {
    			$this->_error(self::NO_SYMBOL);
    		}
    		
    		if (sizeof($this->_errors) > 0) {
    			return false;
    		}
    		
    	}
        return true;
        
    }

    /**
     * Return a string explaining the current password requirements such as length and character set
     * 
     * @return string The printable message explaining password requirements
     */
    public function getRequirementString()
    {
        $parts = array();

        $parts[] = 'Sender must be at least ' . $this->_minLength . ' characters long';

        if ($this->_requireNumeric) $parts[] = 'contain one digit';
        if ($this->_checkAlpha)   $parts[] = 'contain one alpha character';
        if ($this->_requireCapital) $parts[] = 'have at least one uppercase letter';

        if (sizeof($parts) == 1) {
            return $parts[0] . '.';
        } else if (sizeof($parts) == 2) {
            return $parts[0] . ' and ' . $parts[1] . '.';
        } else {
            $str = $parts[0];
            for ($i = 1; $i < sizeof($parts) - 1; ++$i) {
                $str .= ', ' . $parts[$i];
            }

            $str .= ' and ' . $parts[$i];

            return $str . '.';
        }
    }
}