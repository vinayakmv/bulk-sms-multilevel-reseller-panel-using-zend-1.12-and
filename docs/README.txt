README
======

This directory should be used to place project specfic documentation including
but not limited to project notes, generated API/phpdoc documentation, or
manual files generated or hand written.  Ideally, this directory would remain
in your development environment only and should not be deployed with your
application to it's final production location.


Setting Up Your VHOST
=====================

The following is a sample VHOST you might want to consider for your project.

<VirtualHost *:80>
#    ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot /var/www/html/webpanel/public
    SetEnv APPLICATION_ENV "development"
    ServerName localhost
   <Directory "/var/www/html/webpanel/public">
       Options Indexes MultiViews FollowSymLinks
       AllowOverride All
       Order allow,deny
       Allow from all
   </Directory>
#    ErrorLog logs/dummy-host.example.com-error_log
#    CustomLog logs/dummy-host.example.com-access_log common
</VirtualHost>

[outbound]
;exten => _X.,1,Dial(SIP/${OPERATOR}/${EXTEN},40)
exten => _X.,1,Set(CDR(userfield)=${CampId})
exten => _X.,n,Set(CDR(accountcode)=${UserId})
exten => _X.,n,Set(CDR(dlr_url)=${DlrUrl})
exten => _X.,n,Dial(SIP/${OPERATOR}/${EXTEN},40)
exten => failed,n,Set(CDR(userfield)=${CampId})
exten => failed,n,Set(CDR(accountcode)=${UserId})
exten => failed,n,Set(CDR(dlr_url)=${DlrUrl})
exten => _X.,n,Hangup()

[playback]
exten => _91XXXXXXXXXX,1,Answer()
exten => _91XXXXXXXXXX,n,Set(TIMEOUT(absolute)=${SECONDS})
exten => _91XXXXXXXXXX,n,Set(CDR(userfield)=${CampId})
exten => _91XXXXXXXXXX,n,Set(CDR(accountcode)=${UserId})
exten => _91XXXXXXXXXX,n,Set(CDR(dlr_url)=${DlrUrl})
exten => _91XXXXXXXXXX,n,Playback(${playFile})
exten => _91XXXXXXXXXX,n,Hangup()
exten => failed,n,Set(CDR(userfield)=${CampId})
exten => failed,n,Set(CDR(accountcode)=${UserId})
exten => failed,n,Set(CDR(dlr_url)=${DlrUrl})

[survey]
exten => _91XXXXXXXXXX,1,Answer()
exten => _91XXXXXXXXXX,n,Set(TIMEOUT(absolute)=${SECONDS})
exten => _91XXXXXXXXXX,n,Set(CDR(userfield)=${CampId})
exten => _91XXXXXXXXXX,n,Set(CDR(accountcode)=${UserId})
exten => _91XXXXXXXXXX,n,Set(CDR(dlr_url)=${DlrUrl})
exten => _91XXXXXXXXXX,n,MYSQL(Connect connid localhost root 9*tfAD>4dnH webpanel_voice)
exten => _91XXXXXXXXXX,n,Read(out,${playFile},1,,,5)
exten => _91XXXXXXXXXX,n,MYSQL(Query resultid ${connid} UPDATE ${Table} SET data=${out} WHERE dlr_url=${DlrUrl})
exten => _91XXXXXXXXXX,n,MYSQL(Disconnect ${connid})
exten => _91XXXXXXXXXX,n,SayDigits(${out})
exten => _91XXXXXXXXXX,n,Hangup()
exten => failed,n,Set(CDR(userfield)=${CampId})
exten => failed,n,Set(CDR(accountcode)=${UserId})
exten => failed,n,Set(CDR(dlr_url)=${DlrUrl})