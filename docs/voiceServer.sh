#!/bin/bash

PID_V=$(ps -ef | grep -v grep | grep /voice/cronJobNumberInit.php)
RET_V=$?
if [ $RET_V -ne 0 ] ; then
        $(/usr/bin/php /var/www/html/webpanel/cronjob/voice/cronJobNumberInit.php < /dev/null > /dev/null &)
fi

PID_VT=$(ps -ef | grep -v grep | grep cronJobNumberOutboundTrans.php)
RET_VT=$?
if [ $RET_VT -ne 0 ] ; then
        $(/usr/bin/php /var/www/html/webpanel/cronjob/voice/cronJobNumberOutboundTrans.php < /dev/null > /dev/null &)
fi

PID_VP=$(ps -ef | grep -v grep | grep cronJobNumberOutboundPromo.php)
RET_VP=$?
if [ $RET_VP -ne 0 ] ; then
        $(/usr/bin/php /var/www/html/webpanel/cronjob/voice/cronJobNumberOutboundPromo.php < /dev/null > /dev/null &)
fi

PID_VP=$(ps -ef | grep -v grep | grep cronJobMoveCallFile.php)
RET_VP=$?
if [ $RET_VP -ne 0 ] ; then
        $(/usr/bin/php /var/www/html/webpanel/cronjob/voice/cronJobMoveCallFile.php < /dev/null > /dev/null &)
fi

PID_AU=$(mount -l | grep audioFiles | grep -v grep)
RET_AU=$?
if [ $RET_AU -ne 0 ] ; then
        $(/usr/bin/sshfs -p2892 root@94.130.134.249:/var/www/html/webpanel/audioFiles/ /var/www/html/webpanel/audioFiles/ 2> /dev/null &)
fi

PID_CO=$(mount -l | grep contactFiles | grep -v grep)
RET_CO=$?
if [ $RET_CO -ne 0 ] ; then
        $(/usr/bin/sshfs -p2892 root@94.130.134.249:/var/www/html/webpanel/contactFiles/ /var/www/html/webpanel/contactFiles/ 2> /dev/null &)
fi

PID_OU=$(mount -l | grep outgoing | grep -v grep)
RET_OU=$?
if [ $RET_OU -ne 0 ] ; then
        $(/usr/bin/sshfs -p2892 root@94.130.134.249:/var/spool/asterisk/outgoing/ /var/spool/asterisk/web-outgoing/ 2> /dev/null &)
fi
