CREATE TRIGGER voice_report_update_trans AFTER INSERT ON asterisk_cdr FOR EACH ROW 
UPDATE sent_voice_trans INNER JOIN asterisk_cdr ON sent_voice_trans.dlr_url = asterisk_cdr.dlr_url SET sent_voice_trans.dlrdata = asterisk_cdr.disposition,sent_voice_trans.status=1 ;

CREATE TRIGGER voice_report_update_promo AFTER INSERT ON asterisk_cdr FOR EACH ROW 
UPDATE sent_voice_promo INNER JOIN asterisk_cdr ON sent_voice_promo.dlr_url = asterisk_cdr.dlr_url SET sent_voice_promo.dlrdata = asterisk_cdr.disposition,sent_voice_promo.status=1 ;


