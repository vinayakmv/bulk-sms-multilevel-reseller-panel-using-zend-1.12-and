#!/usr/bin/php
<?php

require_once '/var/www/html/webpanel/cronjob/voice/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");
$today = date("d-m-y");

while (true) {
  $cdateTime =date("Y-m-d H:i:s");
  $totalProcess = 5;
  $freeProcess = 0;
  $runningProcess = shell_exec("ps -ef | grep cronJobNumberFinal.php | grep -v grep | wc -l");
  $freeProcess = $totalProcess - $runningProcess;

  $select_distinct_campaign = "SELECT DISTINCT `user_id` FROM `campaigns_voice` WHERE `status` ='0' AND `start_date`<='$cdateTime'";
  $select_distinct_campaign_result = $voiceDb->rawQuery($select_distinct_campaign);
  $distinct_count = $voiceDb->count;
  if ($freeProcess) {
      if ($freeProcess < $distinct_count) {
		  $output = $select_distinct_campaign_result;
		  for ($x = $freeProcess; $x >= 0; $x--) {
			  $userId = $output[$x];
			  $userId = $userId['user_id'];
			  
			  $voiceDb->where ('status', 0);
			  $voiceDb->where ('user_id', $userId);
			  $row = $voiceDb->getOne('campaigns_voice');
			  $totalLeads = $voiceDb->count;

			 // echo "SINGLE - campaigID = $userId = $totalLeads\sn";
			  $id = $row['id'];
			  $data = Array ('status' => '2');
			  $voiceDb->where('id', $id);
			  $voiceDb->where ('start_date', $cdateTime, "<=");
			  $voiceDb->update ('campaigns_voice', $data);

			  if ($totalLeads > 0) {
			      shell_exec('/usr/bin/php /var/www/html/webpanel/cronjob/voice/cronJobNumberFinal.php '.$id.' 2>> /var/log/webpanel/voicecron-final-error-'.$today.'.log >> /var/log/webpanel/voicecron-final-success-'.$today.'.log  &');
			      	
			  }
			}
    	}else {

	      if ($distinct_count > 0) {
			  $select_per_campaign = ceil($freeProcess/$distinct_count);
			  foreach ($select_distinct_campaign_result as $distinct_row) {
			      $userId = $distinct_row['user_id'];
			     
			      $voiceDb->where ('status', 0);
			      $voiceDb->where ('user_id', $userId);
			      $voiceDb->where ('start_date', $cdateTime, "<=");
			      $results = $voiceDb->get('campaigns_voice', Array(0, $select_per_campaign));
			      $totalLeads = $voiceDb->count;

			     // echo "FULL - campaigID = $userId = $totalLeads\n";
			      if ($totalLeads > 0) {
					  foreach ($results as $row) {
					  	  $id = $row['id'];
					      $data = Array ('status' => '2');
					  	  $voiceDb->where('id', $id);
					  	  $voiceDb->update ('campaigns_voice', $data);
					  	  
					  	  shell_exec('/usr/bin/php /var/www/html/webpanel/cronjob/voice/cronJobNumberFinal.php '.$id.' 2>> /var/log/webpanel/voicecron-final-error-'.$today.'.log >> /var/log/webpanel/voicecron-final-success-'.$today.'.log  &');
					  	   
					  }
		           }
		   		}
			}
   		}
   	}
		
}
