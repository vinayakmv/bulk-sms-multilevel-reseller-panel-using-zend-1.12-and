#!/usr/bin/php
<?php 
//sleep(5);
require_once '/var/www/html/webpanel/cronjob/voice/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");
while(true){
	$sentVoice = 'sent_voice_promo';
	$totalChannel = 25;
	$freeChannel = 0;
	$usedChannel = shell_exec("ls /var/spool/asterisk/outgoing | wc -w");
	$freeChannel = $totalChannel - $usedChannel;
	
	$select_distinct_user = "SELECT DISTINCT `user_id` FROM `send_voice_promo` WHERE `status` = '0'";
	$select_distinct_user_result = $voiceDb->rawQuery($select_distinct_user);
	$distinct_count = $voiceDb->count;
	if ($freeChannel) {
		if ($freeChannel < $distinct_count) {
			$output = $select_distinct_user_result;
			for ($x = $freeChannel; $x >= 0; $x--) {
				$userId = $output[$x];
				$userId = $userId['user_id'];
				
				$voiceDb->where ('status', '0');
				$voiceDb->where ('user_id', $userId);
				$row = $voiceDb->getOne('send_voice_promo');
				$totalLeads = $voiceDb->count;
				//echo "SINGLE - userID = $userId = $totalLeads\n";
				if ($totalLeads > 0) {
					$id = $row['sql_id'];
					$destination = $row['receiver'];
					$exten = substr($destination, -12);
					$voiceFile = $row['voice_file'];
					$router = $row['route'];
					$explodeVoice = explode('.', $voiceFile);
  					$voices = $explodeVoice['0'];
    				$voice = "/var/www/html/webpanel/audioFiles/$voices";
					$userId = $row['user_id'];
					$campId = $row['campaign_id'];
					$dlrUrl = $row['dlr_url'];
					$context = $row['context'];
					$sender = $row['sender'];
					$callDuration = $row['duration'];
					
					if ($router == 'eben'){
						$destination = substr($destination, -10);
						$sender = '1087';
					}else{
						$destination = '91'.$destination;
						$sender = '+91'.$sender;
					}
					//echo "==========SINGLE-OUTGOING===========\n";
						$fp = fopen("/var/www/html/webpanel/public/$destination.call", "w");
 						fwrite($fp,"Channel: Local/$destination@outbound\n");
						//fwrite($fp,"Channel: SIP/$router/$destination\n");
						//fwrite($fp,"Channel: SIP/5200$destination@198.199.127.175\n");
						fwrite($fp,"CallerID: $sender\n");
						fwrite($fp,"Set: playFile=$voice\n");
						fwrite($fp,"MaxRetries: 1\n");
						fwrite($fp,"RetryTime: 30\n");
						fwrite($fp,"WaitTime: 60\n");
						fwrite($fp,"Set: OPERATOR=$router\n");
						fwrite($fp,"Set: CampId=$campId\n");
						fwrite($fp,"Set: UserId=$userId\n");
						fwrite($fp,"Set: Table=$sentVoice\n");
						fwrite($fp,"Set: DlrUrl=$dlrUrl\n");
		      			fwrite($fp,"Set: SECONDS=$callDuration\n");
		      			fwrite($fp,"Context: $context\n");
						fwrite($fp,"Extension: $destination\n");
						fwrite($fp,"Priority: 1");
						fclose($fp);
						
					$dataOutgoing = Array ( "user_id" => "$userId",
							"voice_file" => $row['voice_file'],
							"sender" => $row['sender'],
							"receiver" => $destination,
							"time" => $row['time'],
							"route" => $row['route'],
							"campaign_id" => $row['campaign_id'],
							"context" => $row['context'],
							"priority" => $row['priority'],
							"duration" => $row['duration'],
							"dlr_url" => $row['dlr_url'],
							"status" => '1',
					);
					//$voiceDb->insert ('sent_voice_promo', $dataOutgoing);
					
					$voiceDb->where('sql_id', $id);
					$voiceDb->delete('send_voice_promo');
					shell_exec("mv /var/www/html/webpanel/public/$destination.call /var/spool/asterisk/outgoing/");
				}
			}
		}else {
			if ($distinct_count > 0) {
				$select_per_campaign = ceil($freeChannel/$distinct_count);
				// output data of each row
				foreach ($select_distinct_user_result as $distinct_row) {
					$userId = $distinct_row['user_id'];
					
					$voiceDb->where ('status', 0);
					$voiceDb->where ('user_id', $userId);
					$results = $voiceDb->get('send_voice_promo', Array(0, $select_per_campaign));
					$totalLeads = $voiceDb->count;
					//echo "FULL - userID = $userId = $totalLeads\n";
					if ($totalLeads > 0) {
						// output data of each row
						foreach ($results as $row) {
							$id = $row['sql_id'];
							$destination = $row['receiver'];
							$exten = substr($destination, -12);
							$voiceFile = $row['voice_file'];
							$router = $row['route'];
							$explodeVoice = explode('.', $voiceFile);
	  						$voices = $explodeVoice['0'];
	    					$voice = "/var/www/html/webpanel/audioFiles/$voices";
							$userId = $row['user_id'];
							$campId = $row['campaign_id'];
							$dlrUrl = $row['dlr_url'];
							$context = $row['context'];
							$sender = $row['sender'];
							$callDuration = $row['duration'];
								
							if ($router == 'eben'){
								$destination = substr($destination, -10);
								$sender = '1087';
							}else{
								$destination = '91'.$destination;
								$sender = '+91'.$sender;
							}
							//echo "==========FULL-OUTGOING===========\n";
							$fp = fopen("/var/www/html/webpanel/public/$destination.call", "w");
							fwrite($fp,"Channel: Local/$destination@outbound\n");
							//fwrite($fp,"Channel: SIP/$router/$destination\n");
							//fwrite($fp,"Channel: SIP/5200$destination@198.199.127.175\n");
							fwrite($fp,"CallerID: $sender\n");
							fwrite($fp,"Set: playFile=$voice\n");
							fwrite($fp,"MaxRetries: 1\n");
							fwrite($fp,"RetryTime: 30\n");
							fwrite($fp,"WaitTime: 60\n");
							fwrite($fp,"Set: OPERATOR=$router\n");
							fwrite($fp,"Set: CampId=$campId\n");
							fwrite($fp,"Set: UserId=$userId\n");
							fwrite($fp,"Set: Table=$sentVoice\n");
							fwrite($fp,"Set: DlrUrl=$dlrUrl\n");
							fwrite($fp,"Set: SECONDS=$callDuration\n");
							fwrite($fp,"Context: $context\n");
							fwrite($fp,"Extension: $destination\n");
							fwrite($fp,"Priority: 1");
							fclose($fp);
							
							$dataOutgoing = Array ( "user_id" => "$userId",
									"voice_file" => $row['voice_file'],
									"sender" => $row['sender'],
									"receiver" => $destination,
									"time" => $row['time'],
									"route" => $row['route'],
									"campaign_id" => $row['campaign_id'],
									"context" => $row['context'],
									"priority" => $row['priority'],
									"duration" => $row['duration'],
									"dlr_url" => $row['dlr_url'],
									"status" => '1',
							);
							//$voiceDb->insert ('sent_voice_promo', $dataOutgoing);
							
							$voiceDb->where('sql_id', $id);
							$voiceDb->delete('send_voice_promo');
							shell_exec("mv /var/www/html/webpanel/public/$destination.call /var/spool/asterisk/outgoing/");
						}
							
					}
				}
			}
		
		}
	}	
}
?>
