#!/usr/bin/php
<?php
//sleep(5);
require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");


if (isset($argv[1], $argv[2], $argv[3])){

	$parentDomainId = $argv[1];
	$systemId = $argv[2];
	$type = $argv[3];
	
	$userDb->where ("id", "$parentDomainId");
	$domain = $userDb->getOne ("domains");
	$owner = $domain['owner_id'];
	
	$smppDb->where ("system_id", "$systemId");
	if ($type == 'TRANSACTIONAL' || $type == 'TRANS-SCRUB'){
		$user = $smppDb->getOne ("users_smpp_users_trans");
	}else {
		$user = $smppDb->getOne ("users_smpp_users_promo");
	}
	
	
	if ($smppDb->count){
		
		$userId = $user['user_id'];
				
        $recreditDay = date('Y-m-d', strtotime('- 3 days'));
        $startBefore3Days = date("Y-m-d 00:00:00", strtotime('- 3 days'));
        $startBefore3DaysTimestamp = strtotime($startBefore3Days);

        $endBefore3Days = date("Y-m-d 23:59:59", strtotime('- 3 days'));
        $endBefore3DaysTimestamp = strtotime($endBefore3Days);
                
        $delivered = '%3ADELIVRD+err%';
        $nocredit = '%NACK%2F0x00000460%2FNo+credit+failed%';
        $sender = '%NACK%2F0x00000471%2FSource+not+allowed+failed%';
        $template = '%NACK%2F0x00000468%2FTemplate+not+allowed+failed%';
        $ndnc = '%NACK%2F0x00000462%2FDnd+destination+failed%';
        
        if ($type == 'TRANSACTIONAL' || $type == 'TRANS-SCRUB'){
		/* 
		 * TRANSACTIONAL RECREDIT COUNT
		 */       
	        
	        //Failed SMS
	        $smppDb->where ("service", "$systemId");
	        $smppDb->where ("dlrdata", "$delivered", 'not like');
	        $smppDb->where ("dlrdata", "$nocredit", 'not like');
	        $smppDb->where ("dlrdata", "$sender", 'not like');
	        $smppDb->where ("dlrdata", "$template", 'not like');
	        $smppDb->where ("dlrdata", "$ndnc", 'not like');
	        $smppDb->where ("time", "$startBefore3DaysTimestamp", '>=');
	        $smppDb->where ("time", "$endBefore3DaysTimestamp", '<=');
	        $smppDb->where ('status', '1');
	        $transFailed = $smppDb->getOne("smpp_log_mt_trans", 'count(*) as failed');
	        $smsTransFailed = $transFailed['failed'];
	        
	        //Awaiting DLR
	        $smppDb->where ("service", "$systemId");
	        $smppDb->where ("time", "$startBefore3DaysTimestamp", '>=');
	        $smppDb->where ("time", "$endBefore3DaysTimestamp", '<=');
	        $smppDb->where ('status', '0');
	        $transAwaitingDlr = $smppDb->getOne("smpp_log_mt_trans", 'count(*) as awaiting');
	        $smsTransAwaiting = $transAwaitingDlr['awaiting'];
	        
	        //Delivered
	        $smppDb->where ("service", "$systemId");
	        $smppDb->where ("dlrdata", "$delivered", 'like');
	        $smppDb->where ("time", "$startBefore3DaysTimestamp", '>=');
	        $smppDb->where ("time", "$endBefore3DaysTimestamp", '<=');
	        $smppDb->where ('status', '1');
	        $transDelivered = $smppDb->getOne("smpp_log_mt_trans", 'count(*) as delivered');
	        $smsTransDelivered = $transDelivered['delivered'];
	        
	        $data = Array (	"user_id" => "$owner",
	        		"client_id" => "$userId",
	        		"esme" => "$systemId",
	        		"type" => $type,
	        		"status" => '0',
	        		"failed" => "$smsTransFailed",
	        		"awaiting_dlr" => "$smsTransAwaiting",
	        		"delivered" => "$smsTransDelivered",
	        		"total_recredit" => $smsTransFailed+$smsTransAwaiting,
	        		"recredit_date" => "$recreditDay",
	        		"created_date" => $smppDb->now(),
	        		"updated_date" => $smppDb->now()
	        );
	        $smppDb->insert ('smpp_recredit', $data);
        
        } else {        
		/*
		 * PROMOTIONAL RECREDIT COUNT
		 */       
	        
	        //Failed SMS
	        $smppDb->where ("service", "$systemId");
	        $smppDb->where ("dlrdata", "$delivered", 'not like');
	        $smppDb->where ("dlrdata", "$nocredit", 'not like');
	        $smppDb->where ("dlrdata", "$sender", 'not like');
	        $smppDb->where ("dlrdata", "$template", 'not like');
	        $smppDb->where ("dlrdata", "$ndnc", 'not like');
	        $smppDb->where ("time", "$startBefore3DaysTimestamp", '>=');
	        $smppDb->where ("time", "$endBefore3DaysTimestamp", '<=');
	        $smppDb->where ('status', '1');
	        $promoFailed = $smppDb->getOne("smpp_log_mt_promo", 'count(*) as failed');
	        $smsPromoFailed = $promoFailed['failed'];
	        
	        //Awaiting DLR
	        $smppDb->where ("service", "$systemId");
	        $smppDb->where ("time", "$startBefore3DaysTimestamp", '>=');
	        $smppDb->where ("time", "$endBefore3DaysTimestamp", '<=');
	        $smppDb->where ('status', '0');
	        $promoAwaitingDlr = $smppDb->getOne("smpp_log_mt_promo", 'count(*) as awaiting');
	        $smsPromoAwaiting = $promoAwaitingDlr['awaiting'];
	        
	        //Delivered
	        $smppDb->where ("service", "$systemId");
	        $smppDb->where ("dlrdata", "$delivered", 'like');
	        $smppDb->where ("time", "$startBefore3DaysTimestamp", '>=');
	        $smppDb->where ("time", "$endBefore3DaysTimestamp", '<=');
	        $smppDb->where ('status', '1');
	        $promoDelivered = $smppDb->getOne("smpp_log_mt_promo", 'count(*) as delivered');
	        $smsPromoDelivered = $promoDelivered['delivered'];
	        
	        $data = Array (	"user_id" => "$owner",
	        			   	"client_id" => "$userId",
	        				"esme" => "$systemId",
	        			   	"type" => $type,
			        		"status" => '0',
			        		"failed" => "$smsPromoFailed",
			        		"awaiting_dlr" => "$smsPromoAwaiting",
			        		"delivered" => "$smsPromoDelivered",
			        		"total_recredit" => $smsPromoFailed+$smsPromoAwaiting,
			        		"recredit_date" => "$recreditDay",
			        		"created_date" => $smppDb->now(),
			        		"updated_date" => $smppDb->now()
	        				);
	        $smppDb->insert ('smpp_recredit', $data);
        }
	}
	
	
}
?>
