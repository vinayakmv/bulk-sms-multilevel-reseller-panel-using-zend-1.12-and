#!/usr/bin/php
<?php
//sleep(5);
require_once '/var/www/html/webpanel/cronjob/smpp/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");

$recreditDay = date('Y-m-d', strtotime('- 3 days'));
$startBefore3Days = date("Y-m-d 00:00:00", strtotime('- 3 days'));
$startBefore3DaysTimestamp = strtotime($startBefore3Days);

$endBefore3Days = date("Y-m-d 23:59:59", strtotime('- 3 days'));
$endBefore3DaysTimestamp = strtotime($endBefore3Days);
$type = 'TRANSACTIONAL';


$smscRoute = $smppDb->rawQueryOne ("SELECT DISTINCT smsc_id FROM smpp_log_mt_trans WHERE time >= '$startBefore3DaysTimestamp' AND time <= '$endBefore3DaysTimestamp'"); 

	
foreach ($smscRoute as $route) {

	$smscId = $route['smsc_id'];
	
	    $delivered = '%3ADELIVRD+err%';
        $nocredit = '%NACK%2F0x00000460%2FNo+credit+failed%';
        $sender = '%NACK%2F0x00000471%2FSource+not+allowed+failed%';
        $template = '%NACK%2F0x00000468%2FTemplate+not+allowed+failed%';
        $ndnc = '%NACK%2F0x00000462%2FDnd+destination+failed%';
        $black = '%NACK%2F0x00000434%2FBlacklist+destination+failed%';
        
	$smscReport = $smppDb->rawQueryOne ("SELECT (SELECT count(*) FROM `smpp_log_mt_trans` WHERE time >= '$startBefore3DaysTimestamp' AND time <= '$endBefore3DaysTimestamp' AND smsc_id='$smscId' AND status='1' AND dlrdata LIKE '%DELIVRD%') AS DELIVERED,(SELECT count(*) FROM `smpp_log_mt_trans` WHERE time >= '$startBefore3DaysTimestamp' AND time <= '$endBefore3DaysTimestamp' AND  smsc_id='$smscId' AND status='1') AS DLR_RECEIVED,(SELECT count(*) FROM `smpp_log_mt_trans` WHERE time >= '$startBefore3DaysTimestamp' AND time <= '$endBefore3DaysTimestamp' AND  smsc_id='$smscId' AND status='0') AS AWAITING_DLR,(SELECT count(*) FROM `smpp_log_mt_trans` WHERE time >= '$startBefore3DaysTimestamp' AND time <= '$endBefore3DaysTimestamp' AND  smsc_id='$smscId') AS SEND,(SELECT count(*) FROM `smpp_log_mt_trans` WHERE time >= '$startBefore3DaysTimestamp'  AND time <= '$endBefore3DaysTimestamp' AND  smsc_id='$smscId' AND (dlrdata  LIKE '$nocredit' OR dlrdata LIKE '$ndnc' OR dlrdata  LIKE '$sender' OR dlrdata LIKE '$black')) AS NOT_SEND");

	        $smsTransNotSend = $smscReport['NOT_SEND'];
	        $smsTransSend = $smscReport['SEND'];
	        $smsTransDlrReceived = $smscReport['DLR_RECEIVED'];
	        $smsTransAwaiting = $smscReport['AWAITING_DLR'];
	        $smsTransDelivered = $smscReport['DELIVERED'];
	        
	        $recredit = ($smsTransSend-$smsTransNotSend)-$smsTransDelivered;
	        
	        $data = Array ("smsc_id" => "$smscId",
	        		"type" => $type,
	        		"status" => '0',
	        		"send" => "$smsTransSend",
	        		"not_send" => "$smsTransNotSend",
	        		"total_dlr" => "$smsTransDlrReceived",
	        		"awaiting_dlr" => "$smsTransAwaiting",
	        		"delivered" => "$smsTransDelivered",
	        		"total_recredit" => $recredit,
	        		"recredit_date" => "$recreditDay",
	        		"created_date" => $smppDb->now(),
	        		"updated_date" => $smppDb->now()
	        );
	        $smppDb->insert ('smsc_recredit', $data);
        
        
	}

?>
