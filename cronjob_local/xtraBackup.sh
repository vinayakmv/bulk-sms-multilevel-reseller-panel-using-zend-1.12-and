#!/bin/bash
x=$(date -d "0 days" +%Y-%m-%d)

innobackup()
{
        $(/usr/bin/innobackupex --user=root --password='' --no-timestamp /var/backup/xtra-backup/$x/ 2>> /var/backup/xtra-bcp/$x"_create")
        $(/usr/bin/innobackupex --apply-log /var/backup/xtra-backup/$x/ 2 >> /var/backup/xtra-bcp/$x"_prepare")
}

mail()
{
        $(/bin/echo "$msg" | /bin/mail -s "Xtra-Backup Notification" "vinayakvasu.mv@gmail.com")
}

if df -h | grep -q /var/backup/; then
        innobackup
    else
        msg="Backup disk is unmounted"
        mail
        $(/usr/bin/sshfs root@ip:/backup /var/backup/ 2> /dev/null)
        if df -h | grep -q /mnt/logs; then
           innobackup
        else
                msg="Unable to mount disk, backup failed"
                mail
            exit 112
        fi
fi

