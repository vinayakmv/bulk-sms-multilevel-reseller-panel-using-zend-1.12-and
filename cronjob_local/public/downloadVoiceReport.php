<?php 
require_once '/var/www/html/webpanel/cronjob/voice/Db/initDb.php';

date_default_timezone_set("Asia/Kolkata");
$cdateTime =date("Y-m-d H:i:s");
if (isset($_POST['campid']) && isset($_POST['userid'])){

  $id = $_POST['campid']; //campaign Id
  $ubid = $_POST['userid'];// user Id
  $i=1;
  $j=1;
  //-------------------------------SET ERROR CODE IN ARRAY START--------------------------------\\
    $errorCode=array();//error code set as null
    $error_res = "SELECT *  FROM disposition_status where status='1'";
    $error = $userDb->rawQuery($error_res);

    foreach($error as $reportResult) {
       //$errorCode[$reportResult['status_dlr']]=$reportResult['error_code'];
       $errorCode[$reportResult['error_code']]=$reportResult['status_dlr'];
    }
    //-------------------------------SET compose detail from campaigns_voice--------------------------------\\
    $composeDetail = "SELECT *  FROM `campaigns_voice` WHERE `id` = '$id'";
    $composeDetailQuery = $voiceDb->rawQuery($composeDetail);
    foreach($composeDetailQuery as $DetailsFetch){
    	
    	$senderID=$DetailsFetch['sender_id'];
    	$type = $DetailsFetch['type'];
    	$creditCount=$DetailsFetch['duration'];
    	$context = $DetailsFetch['context'];
    	 
    	if ($creditCount == '1') {
    		$callDuration = '30';
    	}elseif ($creditCount == '2') {
    		$callDuration = '60';
    	}elseif ($creditCount == '3') {
    		$callDuration = '90';
    	}elseif ($creditCount == '4') {
    		$callDuration = '120';
    	}else {
    		$callDuration = '0';
    	}
    	$totalCount=$DetailsFetch['count'];
    	$oldFileName= $DetailsFetch['report_file'];
    	$start_datetime = $DetailsFetch['start_date'];
    }
     
  //-------------------------------SET ERROR CODE IN ARRAY END--------------------------------\\
    $csvName=strtotime("now").$id.".csv";
    $leadPath = '/var/www/html/webpanel/files/voice/report/';

        $file = $csvName;
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$file}");
        header("Expires: 0");
        header("Pragma: public");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        
        $fp = @fopen( 'php://output', 'w' );
        //fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
		fputcsv($fp, array('Sender ID','Credit Count','Duration','Total Sent','Sent Time'), ',', '"');
		$rowDetail[] = $senderID;
		$rowDetail[] = $creditCount;
		$rowDetail[] = $callDuration.' sec';
		$rowDetail[] = $totalCount;
		$rowDetail[] = $start_datetime;
		fputcsv($fp, $rowDetail, ',', '"');
		
		fputcsv($fp, array('','',''), ',', '"');
		if ($context == 'survey'){
			fputcsv($fp, array('#','Destination','Status','Survey Data'), ',', '"');
		}else{
			fputcsv($fp, array('#','Destination','Status'), ',', '"');
		}
		
     	$csvgenTime =date("Y-m-d H:i:s");

  //-------------------------------Values fetch ina variable $DetailsFetch['']-------------------------------\\
      if ($type == 'TRANSACTIONAL') {
      	$sentVoice = 'sent_voice_trans';
      }else {
      	$sentVoice = 'sent_voice_promo';
      }

    	$dlr_url=$ubid.'@'.$id.'-';// create Dlr URL first part
	  	$SuccessDLR_res = "SELECT `sender`,`receiver`,`voice_file`,`dlr_url`,`dlrdata`,`status`,`time`,`data` FROM $sentVoice WHERE `dlr_url` LIKE '$dlr_url%'";
	  	$SuccessDLR = $voiceDb->rawQuery($SuccessDLR_res);
    	foreach($SuccessDLR as $SuccessDLRfetch){	
			$time = $SuccessDLRfetch['time'];
        	$delTime=date('d/m/Y H:i:s', $time);
        	$fetch = $SuccessDLRfetch['dlrdata'];
        	$statusMt = $SuccessDLRfetch['status'];
    			foreach ( $errorCode as  $key => $keyword ){
    	          	if (strpos($fetch, $key)!== FALSE ){ 
    	               	$status = $keyword;
    	               	break;
    		        }else{
    		           	$status = 'Unknown';
    		        }
    	       	}
    	        $message='';
                if(isset($status) && !empty($status)){
                 // continue;
                }else{
                  $status = 'Unknown';
                }
                if ($statusMt == '0'){
                	$status = 'Awaiting DLR';
                }
                $row[] = $i++;
                $row[] = $SuccessDLRfetch['receiver'];  
                //$row[] = $SuccessDLRfetch['sender'];
                //$row[] = $message;
                //$row[] = $messageMain;
                $row[] = $status;
                if ($context == 'survey'){
                	$row[] = $SuccessDLRfetch['data'];
                }
                //$row[] = $delTime;
                //$row[] = $dlrTime1;
                //$row[] = $creditCount;
                //$row[] = $smsCount;
                
                fputcsv($fp, $row, ',', '"'); 
                $row = array(); // We must clear the previous values
          }


          fclose($fp);
		  $data = Array ('report_file' => $csvName, 'report_status' => 1, 'csv_gen_date' => $csvgenTime);
        
          $voiceDb->where('id', $id);
          $voiceDb->update ('campaigns_voice', $data);

}