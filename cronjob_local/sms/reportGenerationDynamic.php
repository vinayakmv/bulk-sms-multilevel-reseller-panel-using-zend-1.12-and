#!/usr/bin/php
<?php 
require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';
sleep(5);
date_default_timezone_set("Asia/Kolkata");
$cdateTime =date("Y-m-d H:i:s");
if (isset($argv[1]) && isset($argv[2])){

  $id = $argv[1]; //campaign Id
  $ubid = $argv[2];// user Id
  $i=1;
  $j=1;
  //-------------------------------SET ERROR CODE IN ARRAY START--------------------------------\\
    $errorCode=array();//error code set as null
    $error_res = "SELECT *  FROM dlr_status where status='1'";
    $error = $db->rawQuery($error_res);

    foreach($error as $reportResult) {
       $errorCode[$reportResult['error_code']]=$reportResult['status_dlr'];
    }
    //-------------------------------SET compose detail from campaigns_sms--------------------------------\\
    $composeDetail = "SELECT *  FROM `campaigns_sms` WHERE `id` = '$id'";
    $composeDetailQuery = $db->rawQuery($composeDetail);
    foreach($composeDetailQuery as $DetailsFetch){
    	$messageMain=$DetailsFetch['text'];
    	$senderID=$DetailsFetch['sender_id'];
    	$type = $DetailsFetch['type'];
    	//$creditCount=$DetailsFetch['sms_count'];
    	//$smsCount= $DetailsFetch['char_count'];
    	$totalCount=$DetailsFetch['count'];
    	$leadFile = $DetailsFetch['leads_file'];
    	$oldFileName= $DetailsFetch['report_file'];
    	$start_datetime = $DetailsFetch['start_date'];
    }
  //-------------------------------SET ERROR CODE IN ARRAY END--------------------------------\\
    $csvName=strtotime("now").$id.".csv";
    $leadPath = '/var/www/html/webpanel/files/sms/report/';
    $contactPath = '/var/www/html/webpanel/contactFiles/';

        $file = $leadPath.$csvName;
    	
		$fp = fopen("$file", 'w');
		fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
		fputcsv($fp, array('Sender ID','Message','Total Sent','Sent Time'), ',', '"');
		$rowDetail[] = $senderID;
		$rowDetail[] = $messageMain;
		$rowDetail[] = $totalCount;
		$rowDetail[] = $start_datetime;
		fputcsv($fp, $rowDetail, ',', '"');
		
		fputcsv($fp, array('','',''), ',', '"');
		fputcsv($fp, array('#','Destination','Message','Status','Credits Count','SMS Length'), ',', '"');
     	$csvgenTime =date("Y-m-d H:i:s");
    
     
  //-------------------------------Values fetch ina variable $DetailsFetch['']-------------------------------\\
      if ($type == 'TRANSACTIONAL') {
      	$sentSmsMt = 'sent_sms_trans_mt';
      	$sentSmsDlr = 'sent_sms_trans_dlr';
      }else {
      	$sentSmsMt = 'sent_sms_promo_mt';
      	$sentSmsDlr = 'sent_sms_promo_dlr';
      }

    	$dlr_url=$ubid.'@'.$id.'-';// create Dlr URL first part
    	
    	if (($handle = fopen('/var/www/html/webpanel/contactFiles/'.$leadFile, "r")) !== FALSE) {
    		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
    			$destination = $data[0];
    			$messageDyn = $data[1];
    			$creditCount = $data[2];
    			$smsCount = $data[3];
    			
    			$SuccessDLR_res = "SELECT `sender`,`receiver`,`msgdata`,`dlr_url`,`dlrdata`,`status`,`time` FROM $sentSmsMt WHERE `receiver`='$destination' AND `dlr_url` LIKE '$dlr_url%'";
    			$SuccessDLR = $db->rawQuery($SuccessDLR_res);
    			foreach($SuccessDLR as $SuccessDLRfetch){
    				$time = $SuccessDLRfetch['time'];
    				$delTime=date('d/m/Y H:i:s', $time);
    				$fetch = $SuccessDLRfetch['dlrdata'];
    				$statusMt = $SuccessDLRfetch['status'];
	    			foreach ( $errorCode as  $key => $keyword ){
	    	          	if (strpos($fetch, $key)!== FALSE ){ 
	    	               	$status = $keyword;
	    	               	break;
	    		        }else{
	    		           	$status = 'Unknown';
	    		        }
	    	       	}
	    	        $message='';
	                if(isset($status) && !empty($status)){
	                 // continue;
	                }else{
	                  $status = 'Unknown';
	                }
	                if ($statusMt == '0'){
	                	$status = 'Awaiting DLR';
	                }
	                
    				$row[] = $i++;
    				$row[] = $SuccessDLRfetch['receiver'];
    				//$row[] = $SuccessDLRfetch['sender'];
    				//$row[] = $message;
    				$row[] = $messageDyn;
    				$row[] = $status;
    				//$row[] = $delTime;
    				// $row[] = $dlrTime1;
    				$row[] = $creditCount;
    				$row[] = $smsCount;
    			
    				fputcsv($fp, $row, ',', '"');
    				$row = array(); // We must clear the previous values
    			}
    			 
    		}
    		fclose($fp);
    	}
    	
		  $data = Array ('report_file' => $csvName, 'report_status' => 1, 'csv_gen_date' => $csvgenTime);
        
          $db->where('id', $id);
          $db->update ('campaigns_sms', $data);

		$oldFile = $leadPath.$oldFileName;
		if($oldFileName && file_exists($oldFile)){
			unlink($oldFile);
		}
}