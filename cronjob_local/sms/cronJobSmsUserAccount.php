  <?php
  require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';
  date_default_timezone_set("Asia/Kolkata"); 
  
  //$csvActualName=strtotime("now").'ACTUAL.csv';
  //$csvVirtualName=strtotime("now").'VIRTUAL.csv';
  $csvName=strtotime("now").'ACCOUNT.csv';
  $leadPath = '/var/www/html/webpanel/files/sms/accounts/';
  
  //$fileActual = $leadPath.$csvActualName;
  //$fpA = fopen("$fileActual", 'w');
  
  //$fileVirtual = $leadPath.$csvActualName;
  //$fpV = fopen("$fileVirtual", 'w');
  
  $file = $leadPath.$csvName;
  $fp = fopen("$file", 'w');

	
	$selectL1 = "SELECT * FROM `domains` WHERE (`level`='1' OR `level`='0')";
  	$resultL1 = $userDb->rawQuery($selectL1);
  	
	foreach ($resultL1 as $row){
		$domainId = $row['id'];
		$domain = $row['domain'];
		$smsTransCredit = $row['sms_trans_credit'];
		$smsPromoCredit = $row['sms_promo_credit'];
		$smsScrubCredit = $row['sms_scrub_credit'];
		
		$smsTransApiCredit = $row['sms_trans_credit_api'];
		$smsPromoApiCredit = $row['sms_promo_credit_api'];
		$smsScrubApiCredit = $row['sms_scrub_credit_api'];
		
		$smsADomainPercentagPromo = $row['sms_percentage_promo'];
		$smsADomainPercentagTrans = $row['sms_percentage_trans'];
		$smsADomainPercentagScrub = $row['sms_percentage_scrub'];
		
		//echo '['.$domain.'-'.$domainId.']---TRANS='.$smsTransCredit.'---TRANS-API='.$smsTransApiCredit.'---PROMO='.$smsPromoCredit.'---PROMO-API='.$smsPromoApiCredit.'---SCRUB='.$smsScrubCredit.'---SCRUB-API='.$smsScrubApiCredit."\n";
		fputcsv($fp, array('DOMAIN','TRANS-PERCENT','PROMO-PERCENT','SCRUB-PERCENT','A-TRANS-CREDIT','A-TRANS-API-CREDIT','A-PROMO-CREDIT','A-PROMO-API-CREDIT','A-SCRUB-CREDIT','A-SCRUB-API-CREDIT'), ',', '"');
		 
		$rowDetail[] = $domain;
		$rowDetail[] = $smsADomainPercentagTrans;
		$rowDetail[] = $smsADomainPercentagPromo;
		$rowDetail[] = $smsADomainPercentagScrub;
		$rowDetail[] = $smsTransCredit;
		$rowDetail[] = $smsTransApiCredit;
		$rowDetail[] = $smsPromoCredit;
		$rowDetail[] = $smsPromoApiCredit;
		$rowDetail[] = $smsScrubCredit;
		$rowDetail[] = $smsScrubApiCredit;
		fputcsv($fp, $rowDetail, ',', '"');
		$rowDetail = array();
		
		fputcsv($fp, array('','',''), ',', '"');
		
		fputcsv($fp, array('LOGIN','USERNAME','DOMAIN-TRANS-PERCENT','DOMAIN-PROMO-PERCENT','DOMAIN-SCRUB-PERCENT','TRANS-PERCENT','PROMO-PERCENT','SCRUB-PERCENT','V-TRANS-CREDIT','AFTER-TRANS-CREDIT','V-TRANS-API-CREDIT','V-PROMO-CREDIT','AFTER-PROMO-CREDIT','V-PROMO-API-CREDIT','V-SCRUB-CREDIT','AFTER-SCRUB-CREDIT','V-SCRUB-API-CREDIT'), ',', '"');
		
		
		$selectParent = "SELECT * FROM `users` where `parent_domain_id` = $domainId";
		$resultParent = $userDb->rawQuery($selectParent);
		foreach ($resultParent as $rowParent){
			$userId = $rowParent['id'];
			$accountId = $rowParent['account_id'];
			$username = $rowParent['username'];
			$loginDomainId = $rowParent['domain_id'];
			
			$selectLoginDomain = "SELECT * FROM `domains` where `id` = $loginDomainId LIMIT 1";
			$resultLoginDomain = $userDb->rawQuery($selectLoginDomain);
			
			$loginDomain = $resultLoginDomain['0']['domain'];

			$smsDomainPercentagPromo = $resultLoginDomain['0']['sms_percentage_promo'];
			$smsDomainPercentagTrans = $resultLoginDomain['0']['sms_percentage_trans'];
			$smsDomainPercentagScrub = $resultLoginDomain['0']['sms_percentage_scrub'];
			
			
			$smsUserPercentagPromo = $rowParent['sms_percentage_promo'];
			$smsUserPercentagTrans = $rowParent['sms_percentage_trans'];
			$smsUserPercentagScrub = $rowParent['sms_percentage_scrub'];
			
			$selectAccount = "SELECT * FROM `accounts` where `id` = $accountId LIMIT 1";
			$resultAccount = $userDb->rawQuery($selectAccount);
			
			
				
				$smsATransCredit = $resultAccount['0']['sms_trans_credit'];
				$smsAPromoCredit = $resultAccount['0']['sms_promo_credit'];
				$smsAScrubCredit = $resultAccount['0']['sms_scrub_credit'];
				
				$smsATransApiCredit = $resultAccount['0']['sms_trans_credit_api'];
				$smsAPromoApiCredit = $resultAccount['0']['sms_promo_credit_api'];
				$smsAScrubApiCredit = $resultAccount['0']['sms_scrub_credit_api'];
									
				//echo '['.$loginDomain.'--'.$username.']---TRANS='.$smsATransCredit.'---TRANS-API='.$smsATransApiCredit.'---PROMO='.$smsAPromoCredit.'---PROMO-API='.$smsAPromoApiCredit.'---SCRUB='.$smsAScrubCredit.'---SCRUB-API='.$smsAScrubApiCredit."\n";
				
				$fcreditTrans = ceil($smsATransCredit*$smsUserPercentagTrans/100);
				$fcreditPromo = ceil($smsAPromoCredit*$smsUserPercentagPromo/100);
				$fcreditScrub = ceil($smsAScrubCredit*$smsUserPercentagScrub/100);
				
				$rowDetail[] = $loginDomain;
				$rowDetail[] = $username;
				$rowDetail[] = $smsDomainPercentagTrans;
				$rowDetail[] = $smsDomainPercentagPromo;
				$rowDetail[] = $smsDomainPercentagScrub;
				$rowDetail[] = $smsUserPercentagTrans;
				$rowDetail[] = $smsUserPercentagPromo;
				$rowDetail[] = $smsUserPercentagScrub;
				$rowDetail[] = $smsATransCredit;
				$rowDetail[] = ($smsATransCredit-$fcreditTrans);
				$rowDetail[] = $smsATransApiCredit;
				$rowDetail[] = $smsAPromoCredit;
				$rowDetail[] = ($smsAPromoCredit-$fcreditPromo);
				$rowDetail[] = $smsAPromoApiCredit;
				$rowDetail[] = $smsAScrubCredit;
				$rowDetail[] = ($smsAScrubCredit-$fcreditScrub);
				$rowDetail[] = $smsAScrubApiCredit;
				fputcsv($fp, $rowDetail, ',', '"');
				$rowDetail = array();
			
			
		}
	}
	fclose($fp);