<?php 
require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';

date_default_timezone_set("Asia/Kolkata");
$cdateTime =date("Y-m-d H:i:s");
if (isset($_POST['campid']) && isset($_POST['userid'])){

  $id = $_POST['campid']; //campaign Id
  $ubid = $_POST['userid'];// user Id
  $i=1;
  $j=1;
  //-------------------------------SET ERROR CODE IN ARRAY START--------------------------------\\
    $errorCode=array();//error code set as null
    $error_res = "SELECT *  FROM dlr_status where status='1'";
    $error = $userDb->rawQuery($error_res);

    foreach($error as $reportResult) {
       //$errorCode[$reportResult['status_dlr']]=$reportResult['error_code'];
       $errorCode[$reportResult['error_code']]=$reportResult['status_dlr'];
    }
    //-------------------------------SET compose detail from campaigns_sms--------------------------------\\
    $composeDetail = "SELECT *  FROM `campaigns_sms` WHERE `id` = '$id'";
    $composeDetailQuery = $smsDb->rawQuery($composeDetail);
    foreach($composeDetailQuery as $DetailsFetch){
    	$messageMain=$DetailsFetch['text'];
    	$senderID=$DetailsFetch['sender_id'];
    	$type = $DetailsFetch['type'];
    	$creditCount=$DetailsFetch['sms_count'];
    	$totalCount=$DetailsFetch['count'];
    	$smsCount= $DetailsFetch['char_count'];
    	$oldFileName= $DetailsFetch['report_file'];
    	$start_datetime = $DetailsFetch['start_date'];
    }
     
  //-------------------------------SET ERROR CODE IN ARRAY END--------------------------------\\
    $csvName=strtotime("now").$id.".csv";
    $leadPath = '/var/www/html/webpanel/files/sms/report/';

        $file = $csvName;
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$file}");
        header("Expires: 0");
        header("Pragma: public");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        
        
        $fp = @fopen( 'php://output', 'w' );
        //fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
		fputcsv($fp, array('Sender ID','Message','Credit Count','SMS Length','Total Sent','Sent Time'), ',', '"');
		$rowDetail[] = $senderID;
		$rowDetail[] = $messageMain;
		$rowDetail[] = $creditCount;
		$rowDetail[] = $smsCount;
		$rowDetail[] = $totalCount;
		$rowDetail[] = $start_datetime;
		fputcsv($fp, $rowDetail, ',', '"');
		
		fputcsv($fp, array('','',''), ',', '"');
		
		fputcsv($fp, array('#','Destination','Status'), ',', '"');
     	$csvgenTime =date("Y-m-d H:i:s");

  //-------------------------------Values fetch ina variable $DetailsFetch['']-------------------------------\\
      if ($type == 'TRANSACTIONAL') {
      	$sentSmsMt = 'sent_sms_trans_mt';
      	$sentSmsDlr = 'sent_sms_trans_dlr';
      }elseif ($type == 'INTERNATIONAL') {
      	$sentSmsMt = 'sent_sms_inter_mt';
      	$sentSmsDlr = 'sent_sms_inter_dlr';
      }else{
      	$sentSmsMt = 'sent_sms_promo_mt';
      	$sentSmsDlr = 'sent_sms_promo_dlr';
      }

    	$dlr_url=$ubid.'@'.$id.'-';// create Dlr URL first part
	  	$SuccessDLR_res = "SELECT `sender`,`receiver`,`msgdata`,`dlr_url`,`dlrdata`,`status`,`time` FROM $sentSmsMt WHERE `dlr_url` LIKE '$dlr_url%'";
	  	$SuccessDLR = $smsDb->rawQuery($SuccessDLR_res);
    	foreach($SuccessDLR as $SuccessDLRfetch){	
			$time = $SuccessDLRfetch['time'];
        	$delTime=date('d/m/Y H:i:s', $time);
        	$fetch = $SuccessDLRfetch['dlrdata'];
        	$statusMt = $SuccessDLRfetch['status'];
    			foreach ( $errorCode as  $key => $keyword ){
    	          	if (strpos($fetch, $key)!== FALSE ){ 
    	               	$status = $keyword;
    	               	break;
    		        }else{
    		           	$status = 'Unknown';
    		        }
    	       	}
    	        $message='';
                if(isset($status) && !empty($status)){
                 // continue;
                }else{
                  $status = 'Unknown';
                }
                if ($statusMt == '0'){
                	$status = 'Awaiting DLR';
                }
                $row[] = $i++;
                $row[] = $SuccessDLRfetch['receiver'];  
                //$row[] = $SuccessDLRfetch['sender'];
                //$row[] = $message;
                //$row[] = $messageMain;
                $row[] = $status;
                //$row[] = $delTime;
                //$row[] = $dlrTime1;
                //$row[] = $creditCount;
                //$row[] = $smsCount;
                
                fputcsv($fp, $row, ',', '"'); 
                $row = array(); // We must clear the previous values
          }


          fclose($fp);
		  $data = Array ('report_file' => $csvName, 'report_status' => 1, 'csv_gen_date' => $csvgenTime);
        
          $smsDb->where('id', $id);
          $smsDb->update ('campaigns_sms', $data);

			$oldFile = $leadPath.$oldFileName;
			if($oldFileName && file_exists($oldFile)){
				unlink($oldFile);
			}
}