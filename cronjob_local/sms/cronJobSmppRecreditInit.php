#!/usr/bin/php
<?php

require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';

//while (true) {

//sleep(5);

  date_default_timezone_set("Asia/Kolkata");
  $today =date("Y-m-d");
  $totalProcess = 10000;
  $freeProcess = 0;

  $selectParent = "SELECT `id` FROM `domains` WHERE (`level` = '1' OR `level` = '0') AND `recredit` = '1'";
  $result = $userDb->rawQuery($selectParent);

  foreach ($result as $row){
        $parentDomainId = $row['id'];

        $selectChild = "SELECT `id` FROM `users` WHERE `parent_domain_id` = '$parentDomainId'";

        $resultChild = $userDb->rawQuery($selectChild);
        $count = $userDb->count;
        foreach ($resultChild as $userRow){
                $userId = $userRow['id'];

                $selectSmppTUsers = "SELECT `system_id` FROM `users_smpp_users_trans` WHERE `user_id` = '$userId'";
                $resultSmppTUsers = $smppDb->rawQuery($selectSmppTUsers);
                
                foreach ($resultSmppTUsers as $smppTRow){
                	$systemId = $smppTRow['system_id'];
                	
                	$selectSmppTSystem = "SELECT `system_id`, `type` FROM `smpp_users_trans` WHERE `system_id` = '$systemId' AND `recredit_date` < '$today'";
                	$resultSmppTSystem = $smppDb->rawQuery($selectSmppTSystem);
                	
                	foreach ($resultSmppTSystem as $smppTSystemRow){
                		$systemId = $smppTSystemRow['system_id'];
                		$type = $smppTSystemRow['type'];
                		
                		$runningProcess = shell_exec("ps -ef | grep cronJobSmppRecreditFinal.php | grep -v grep | wc -l");
                		$freeProcess = $totalProcess - $runningProcess;
                		 
                		if ($freeProcess){
                			//Update recredit date of user
                			$data = Array ('recredit_date' => $today);
                			$smppDb->where('system_id', $systemId);
                			$smppDb->update ('smpp_users_trans', $data);
                			 
                			//Process recredit count and transaction in background
                			shell_exec("/usr/bin/php /var/www/html/webpanel/cronjob/sms/cronJobSmppRecreditFinal.php $parentDomainId $systemId $type 2>/dev/null >/dev/null  &");
                		}
                	} 
                }
                
                $selectSmppPUsers = "SELECT `system_id` FROM `users_smpp_users_promo` WHERE `user_id` = '$userId'";
                $resultSmppPUsers = $smppDb->rawQuery($selectSmppPUsers);
                
        		foreach ($resultSmppPUsers as $smppPRow){
                	$systemId = $smppPRow['system_id'];
                	
                	$selectSmppPSystem = "SELECT `system_id`, `type` FROM `smpp_users_promo` WHERE `system_id` = '$systemId' AND `recredit_date` < '$today'";
                	$resultSmppPSystem = $smppDb->rawQuery($selectSmppPSystem);
                	
                	foreach ($resultSmppPSystem as $smppPSystemRow){
                		$systemId = $smppPSystemRow['system_id'];
                		$type = $smppPSystemRow['type'];
                		
                		$runningProcess = shell_exec("ps -ef | grep cronJobSmppRecreditFinal.php | grep -v grep | wc -l");
                		$freeProcess = $totalProcess - $runningProcess;
                		 
                		if ($freeProcess){
                			//Update recredit date of user
                			$data = Array ('recredit_date' => $today);
                			$smppDb->where('system_id', $systemId);
                			$smppDb->update ('smpp_users_promo', $data);
                			 
                			//Process recredit count and transaction in background
                			shell_exec("/usr/bin/php /var/www/html/webpanel/cronjob/sms/cronJobSmppRecreditFinal.php $parentDomainId $systemId $type 2>/dev/null >/dev/null  &");
                		}
                	} 
                }

        }
  }

//}