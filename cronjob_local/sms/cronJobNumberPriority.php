#!/usr/bin/php
<?php 
//sleep(5);
require_once '/var/www/html/webpanel/cronjob/sms/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");

if (isset($argv[1]))
{

  $cdateTime =date("Y-m-d H:i:s");
  $cdte1 = strtotime($cdateTime);
  $id = $argv[1];
  
  $smsDb->where ('status', 4);
  $smsDb->where ('id', $id);
  $row = $smsDb->getOne('campaigns_sms');
  $smsDb->count;
  if ($smsDb->count > 0) {
    //*********** find the csv name ***************
    
    /* $smsDb->where ('smscampaign_id', $id);
    $row1 = $smsDb->getOne('campaigns_sms_leads');
    $lead = $row1['lead_id'].'.csv'; */
  	
    //****** $lead is CSV name ********************END
    //*********** find the SenderId ***************
    //$smsDb->join("campaigns_sms c", "l.smscampaign_id=c.id", "LEFT");
    //$smsDb->where("c.id", $id);
    /* 
     * $senderIdRepo = $smsDb->get ("sms_campaigns_sender_ids l", null, "l. senderid_id");
    $senderId=$senderIdRepo[0]['senderid_id'];
    
    $smsDb->where ('id', $senderId);
    $rowSender = $smsDb->getOne('sender_ids');
    $sender = $rowSender['name'];  
    */
    $lead = $row['leads_file'];
    $sender = $row['sender_id'];
    //****** $sender is name of sendrId ********************END
    //*********** find the route or smsc_id ***************
    $router = $row['route']; 
    $method = $row['method'];
    $smsPriority = $row['sms_priority'];
    $smsCount = $row['sms_count'];
    $type = $row['type'];
    $input = $row['input'];
    
    //****** $router is name of route ********************END
    $cdte = date('Y-m-d H:i:s');
    $userId = $row['user_id'];
    $text = $row['text'];      
    $campId = $row['id']; 
    $encode = $row['encoding'];
    if ($type == 'PROMOTIONAL' || $type == 'TRANS-SCRUB'){
    	$validity = null;
    }else{
    	$validity = ($row['validity'] ? $row['validity'] : null);
    }
	$flash = ($row['flash'] ? '0' : null);
    if($encode == 'UTF16') {
      $text = mb_convert_encoding($row['text'], "UTF-16", "auto");
      $unpack = unpack('H*', $text);
      $text = array_shift($unpack);
      $ecoding = '2';
    }else {
      $text =urlencode($text);
      $ecoding = '0';
    }

    if (($handle = fopen('/var/www/html/webpanel/contactFiles/'.$lead, "r")) !== FALSE) {

      $fp = file('/var/www/html/webpanel/contactFiles/'.$lead);
      $count = count($fp);

		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				if ($input == 'DYNAMIC-COMPOSE' || $input == 'DYNAMIC-SCHEDULED'){
					$text = $data['1'];
					$smsCount = $data['2'];
					if($encode == 'UTF16') {
						$text = mb_convert_encoding($data['1'], "UTF-16", "auto");
						$unpack = unpack('H*', $text);
						$text = array_shift($unpack);
						$ecoding = '2';
					}else {
						$text =urlencode($text);
						$ecoding = '0';
					}
				}
		
		      	$userDb->where ('id', $userId);
		      	$rowUser = $userDb->getOne('users');
		      	
		      	$userStatus = $rowUser['status'];
		      	$domainId = $rowUser['domain_id'];
		      	$parentDomainId = $rowUser['parent_domain_id'];
		      	
		      	$userDb->where ('id', $parentDomainId);
		      	$rowDomain = $userDb->getOne('domains');
		      	$domainStatus = $rowDomain['status'];
		      	
				if ($type == 'TRANSACTIONAL') {
					if ($method == '2'){
						$credit = $rowDomain['sms_trans_credit_api'];
					}else{
						$credit = $rowDomain['sms_trans_credit'];
					}
		      		
		      		$sendSms = 'send_sms_trans';
		      		$sentSmsMt = 'sent_sms_trans_mt';
      				$sentSmsDlr = 'sent_sms_trans_dlr';
		      	}elseif ($type == 'TRANS-SCRUB') {
		      		if ($method == '2'){
						$credit = $rowDomain['sms_scrub_credit_api'];
					}else{
						$credit = $rowDomain['sms_scrub_credit'];
					}
		      		$sendSms = 'send_sms_promo';
		      		$sentSmsMt = 'sent_sms_promo_mt';
      				$sentSmsDlr = 'sent_sms_promo_dlr';
		      	}elseif ($type == 'PROMOTIONAL') {
		      		if ($method == '2'){
						$credit = $rowDomain['sms_promo_credit_api'];
					}else{
						$credit = $rowDomain['sms_promo_credit'];
					}
		      		$sendSms = 'send_sms_promo';
		      		$sentSmsMt = 'sent_sms_promo_mt';
      				$sentSmsDlr = 'sent_sms_promo_dlr';
		      	}elseif ($type == 'INTERNATIONAL') {
		      		if ($method == '2'){
						$credit = $rowDomain['sms_inter_credit_api'];
					}else{
						$credit = $rowDomain['sms_inter_credit'];
					}
		      		$sendSms = 'send_sms_inter';
		      		$sentSmsMt = 'sent_sms_inter_mt';
      				$sentSmsDlr = 'sent_sms_inter_dlr';
		      	}     	
		
		      	mt_srand((double)microtime()*10000);
		      	$charid = strtolower(md5(uniqid(rand(), true)));
		      	$hyphen = chr(45);// "-"
		      	$dlr_url = $userId.'@'.$campId.'-'.substr($charid, 0, 8).substr($charid,16, 4).substr($charid,20,12);
		      	 
		      	$datetime = date('Y-m-d H:i:s');
		      	$timestamp = strtotime($datetime);
		      	
		      	if (($domainStatus == '1') && ($userStatus == '1')){
		      		if ($credit >= $smsCount){     
		      			if ($type == 'TRANSACTIONAL') {
		      				if ($method == '2'){

		      					$creditData = Array ('sms_trans_credit_api' => $userDb->dec($smsCount));
		      				}else{
		      					$creditData = Array ('sms_trans_credit' => $userDb->dec($smsCount));
		      				}
		      				
		      			}elseif ($type == 'TRANS-SCRUB') {
		      				if ($method == '2'){
		      					$creditData = Array ('sms_scrub_credit_api' => $userDb->dec($smsCount));
		      				}else{
		      					$creditData = Array ('sms_scrub_credit' => $userDb->dec($smsCount));
		      				}
		      			}elseif ($type == 'PROMOTIONAL') {
		      				if ($method == '2'){
		      					$creditData = Array ('sms_promo_credit_api' => $userDb->dec($smsCount));
		      				}else{
		      					$creditData = Array ('sms_promo_credit' => $userDb->dec($smsCount));
		      				}
		      			}elseif ($type == 'INTERNATIONAL') {
		      				if ($method == '2'){
		      					$creditData = Array ('sms_inter_credit_api' => $userDb->dec($smsCount));
		      				}else{
		      					$creditData = Array ('sms_inter_credit' => $userDb->dec($smsCount));
		      				}
		      			}      		
		      				
		      			$userDb->where('id', $parentDomainId);
		      			$userDb->update ('domains', $creditData);
		      			
		      			if ($method != '2'){	      				 
		      				 
		      				$dataInsert = Array ("number" => $data['0'],
		      						"user_id" => "$userId",
		      				);
		      				$insertPriority = $smsDb->setQueryOption ('LOW_PRIORITY')->replace('priority_leads_sms', $dataInsert);
		      				
		      			}		      			
	      			
	      				$data1 = Array ("momt" => "MT",
	      						"sender" => "$sender",
	      						"smsc_id" => "$router",
	      						"receiver" => $data['0'],
	      						"msgdata" => "$text",
	      						"meta_data" =>"",
	      						"sms_type" => '2',
	      						"time" => "$timestamp",
	      						"dlr_mask" => '19',
	      						"dlr_url" => "$dlr_url",
	      						"coding" => "$ecoding",
								"validity" => $validity,
	      						"priority" => "$smsPriority",
	      						"mclass" => $flash,
	      						"binfo" => "$smsCount",
	      						"account" => "$method",
	      				);
	      				$insertSendTable= $smsDb->insert("$sendSms", $data1);
		      			
		      		}else {
		      			$data1 = Array ("momt" => "MT",
		      					"sender" => "$sender",
		      					"smsc_id" => "$router",
		      					"receiver" => $data['0'],
		      					"msgdata" => "$text",
		      					"meta_data" =>"",
		      					"sms_type" => '2',
		      					"time" => "$timestamp",
		      					"dlr_mask" => '19',
		      					"dlr_url" => "$dlr_url",
		      					"coding" => "$ecoding",
								"validity" => $validity,
		      					"priority" => "$smsPriority",
		      					"mclass" => $flash,
		      					"binfo" => "$smsCount",
		      					"dlrdata" => urlencode('stat:NOCREDIT err:BBBB text:').mb_substr($text, 0, 13),
		      					"status" => '1',
		      					"account" => "$method",
		      			);
		      			$insertSendTable= $smsDb->insert("$sentSmsMt", $data1);
		      			
		      			$data1 = Array ("momt" => "DLR",
		      					"sender" => "$sender",
		      					"smsc_id" => "$router",
		      					"receiver" => $data['0'],
		      					"msgdata" => urlencode('stat:NOCREDIT err:BBBB text:').mb_substr($text, 0, 13),
		      					"meta_data" =>"",
		      					"sms_type" => '3',
		      					"time" => "$timestamp",
		      					"dlr_mask" => '19',
		      					"dlr_url" => "$dlr_url",
		      					"coding" => "$ecoding",
								"validity" => $validity,
		      					"priority" => "$smsPriority",
		      					"mclass" => $flash,
		      					"binfo" => "$smsCount",
		      					"dlrdata" => "",
		      					"status" => '1',
		      					"account" => "$method",
		      			);
		      			$insertSendTable= $smsDb->insert("$sentSmsDlr", $data1);
		      		}
		      	}else {
		      		$data1 = Array ("momt" => "MT",
		      				"sender" => "$sender",
		      				"smsc_id" => "$router",
		      				"receiver" => $data['0'],
		      				"msgdata" => "$text",
		      				"meta_data" =>"",
		      				"sms_type" => '2',
		      				"time" => "$timestamp",
		      				"dlr_mask" => '19',
		      				"dlr_url" => "$dlr_url",
		      				"coding" => "$ecoding",
							"validity" => $validity,
		      				"priority" => "$smsPriority",
		      				"mclass" => $flash,
		      				"binfo" => "$smsCount",
		      				"dlrdata" => urlencode('stat:BLOCKED err:DDDD text:').mb_substr($text, 0, 13),
		      				"status" => '1',
		      				"account" => "$method",
		      		);
		      		$insertSendTable= $smsDb->insert("$sentSmsMt", $data1);
		      		
		      		$data1 = Array ("momt" => "DLR",
		      				"sender" => "$sender",
		      				"smsc_id" => "$router",
		      				"receiver" => $data['0'],
		      				"msgdata" => urlencode('stat:BLOCKED err:DDDD text:').mb_substr($text, 0, 13),
		      				"meta_data" =>"",
		      				"sms_type" => '3',
		      				"time" => "$timestamp",
		      				"dlr_mask" => '19',
		      				"dlr_url" => "$dlr_url",
		      				"coding" => "$ecoding",
							"validity" => $validity,
		      				"priority" => "$smsPriority",
		      				"mclass" => $flash,
		      				"binfo" => "$smsCount",
		      				"dlrdata" => "",
		      				"status" => '1',
		      				"account" => "$method",
		      		);
		      		$insertSendTable= $smsDb->insert("$sentSmsDlr", $data1);
		      	}
		      	
		
		                    
		}
fclose($handle);


$id = $row['id'];
$data = Array ('status' => '1','report_status'=>'0');
$smsDb->where('id', $id);
$smsDb->update ('campaigns_sms', $data);    

}
}
}

?>
