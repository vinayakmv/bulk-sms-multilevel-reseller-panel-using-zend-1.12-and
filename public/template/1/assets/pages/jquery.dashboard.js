
/**
* Theme: Adminto Admin Template
* Author: Coderthemes
* Dashboard
*/

!function($) {
    "use strict";

    var Dashboard1 = function() {
    	this.$realData = []
    };

    //creates Bar chart
    Dashboard1.prototype.createBarChart  = function(element, data, xkey, ykeys, labels, lineColors) {
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            hideHover: 'auto',
            resize: true, //defaulted to true
            gridLineColor: '#eeeeee',
            barSizeRatio: 0.2,
            barColors: lineColors
        });
    },

    
    Dashboard1.prototype.init = function() {

        //creating bar chart
        var $barData  = [
            { y: '2010', a: 75 },
            { y: '2011', a: 42 },
            { y: '2012', a: 75 },
            { y: '2013', a: 38 },
            { y: '2014', a: 19 },
            { y: '2015', a: 93 }
        ];
        this.createBarChart('users-today', $barData, 'y', ['a'], ['Statistics'], ['#188ae2']);
        
        //creating bar chart
        var $barData  = [
            { y: '2010', a: 75 },
            { y: '2011', a: 42 },
            { y: '2012', a: 75 },
            { y: '2013', a: 38 },
            { y: '2014', a: 19 },
            { y: '2015', a: 93 }
        ];
        this.createBarChart('users-yesterday', $barData, 'y', ['a'], ['Statistics'], ['#188ae2']);
        
        //creating bar chart
        var $barData  = [
            { y: '2010', a: 75 },
            { y: '2011', a: 42 },
            { y: '2012', a: 75 },
            { y: '2013', a: 38 },
            { y: '2014', a: 19 },
            { y: '2015', a: 93 }
        ];
        this.createBarChart('users-week', $barData, 'y', ['a'], ['Statistics'], ['#188ae2']);

  
    },
    //init
    $.Dashboard1 = new Dashboard1, $.Dashboard1.Constructor = Dashboard1
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.Dashboard1.init();
}(window.jQuery);