(function($) {
  'use strict';
  if ($("#timePicker").length) {
    $('#timePicker').datetimepicker({
      format: 'H:m'
    });
  }
  if ($(".color-picker").length) {
    $('.color-picker').asColorPicker();
  }
  if ($("#datePicker").length) {
    $('#datePicker').datepicker({
      enableOnReadonly: true,
      todayHighlight: true,
    });
  }

    if ($("#from").length) {
    $('#from').datepicker({
      enableOnReadonly: true,
      todayHighlight: true,
    });
  }

    if ($("#to").length) {
    $('#to').datepicker({
      enableOnReadonly: true,
      todayHighlight: true,
    });
  }

  if ($("#inline-datepicker").length) {
    $('#inline-datepicker').datepicker({
      enableOnReadonly: true,
      todayHighlight: true,
    });
  }
  if ($(".datepicker-autoclose").length) {
    $('.datepicker-autoclose').datepicker({
      autoclose: true
    });
  }
  if ($('input[name="daterange"]').length) {
    $('input[name="daterange"]').daterangepicker();
  }
  if ($('input[name="daterange"]').length) {
    $('input[name="daterange"]').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY h:mm A'
      }
    });
  }
})(jQuery);