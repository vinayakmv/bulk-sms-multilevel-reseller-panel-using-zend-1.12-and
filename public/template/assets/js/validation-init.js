var Script = function () {

/*    $.validator.setDefaults({
        //submitHandler: function() { alert("submitted!"); }
    });*/
        
    
    $().ready(function() {
        // validate the comment form when it is submitted
       // $("#commentForm").validate();
    	//Signup Form Validation
    	$("#SignupForm").validate({
            rules: {
                username: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                password: {
                    required: true,
                    minlength: 5
                },
                repassword: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                }
            },
            messages: {
                username: {
                    required: "Please enter a valid username"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                repassword: "Please confirm password"
            }
        });
        // validate signin form on keyup and submit
    	$("#LoginForm").validate({
					

            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                username: {
                    required: "Please enter a username"
                },
                password: {
                    required: "Please provide a password"
                }
            }
        });
    	
        $("#ResetForm").validate({
            rules: {
            	email: {
                    required: true,
                    email: true
                }
            },
            messages: {
            	email: {
                    required: "Please enter email-id"
                }
            }
        });
		
		 $("#RegisterForm").validate({
            rules: {
            	username: {
                    required: true,
                     minlength: 10
                }
            },
            messages: {
            	username: {
                    required: "Please enter valid mobile number"
                }
            }
        });
		
		
    	// Validate CreateForm 
    	$("#CreateForm").validate({
    		rules: {
                username: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                email: {
                	 required: true,
                     email: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                repassword: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                },
                domain: {
                	required: true
                }
            },
            messages: {
                username: {
                    required: "Please enter a username"
                },
                email: {
                    required: "Please enter email-id"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long"
                },
                repassword: {
                	required: "Please confirm password"
                },
                domain: {
                	required: "Please enter a domain name"
                }
            }
        });
    	
    	// Validate Compose form
    	$("#VoiceComposeForm").validate({
            rules: {
            	campaignName: {
                    required: false
                },
                startDatetime: {
                    required: true
                },
                campaignType: {
                    required: true
                },
                audioType: {
                    required: true
                },
                contactType: {
                    required: true
                },
                textContact: {
                	required: "#contactType-3:checked"
                	},
                uploadContact: {
                	required: "#contactType-2:checked"	
                	},
                selectContact: {
                     required: "#contactType-1:checked"	
                    },
                contactName: {
                     required: "#saveContact-1:checked"	
                    },
                    
                    
                    selectAudio: {
                    	required: "#audioType-1:checked"
                    	},
                    uploadAudio: {
                    	required: "#audioType-2:checked"	
                    	},
                    text2speech: {
                         required: "#audioType-3:checked"	
                        },
                    audioName: {
                         required: "#saveAudio-1:checked"
                        },
                     selectSenderId: {
                    	 required: true
                     }
            },
            
            messages: {
            	campaignName: {
                    required: "Please enter a campaign name"
                },
                startDatetime: {
                    required: "Please select start date and time"
                },
                campaignType: {
                    required: ""
                },
                audioType: {
                    required: ""
                },
                contactType: {
                    required: ""
                },
                textContact : {
                	required: "Please paste contacts"
                },
                uploadContact : {
                	required: "Please upload a csv file"
                },
                selectContact : {
                	required: "Please select a contact"
                },
                contactName : {
                	required: "Please enter a contact name"
                },
                
                selectAudio : {
                	required: "Please select an audio"
                },
                uploadAudio : {
                	required: "Please upload mp3 file"
                },
                text2speech : {
                	required: "Please enter text"
                },
                audioName : {
                	required: "Please enter an audio name"
                },
                selectSenderId: {
                	required: "Please select a sender-id"
                }
            }
        });
    	
    	// Validate Compose form
    	$("#SmsComposeForm").validate({
            rules: {
            	campaignName: {
                    required: false
                },
                text: {
                	required: true
                },
                textContact: {
                	required: "#contactType-3:checked"
                	},
                uploadContact: {
                	required: "#contactType-2:checked"	
                	},
                selectContact: {
                     required: "#contactType-1:checked"	
                    },
                contactName: {
                     required: "#saveContact-1:checked"	
                    },
                    
                    
                    selectSenderId: {
                    	required: "#campaignType-1:checked"
                    	},
                    smsName: {
                         required: "#saveSms-1:checked"
                        },
                     selectSenderId: {
                    	 required: true
                     }
            },
            
            messages: {
            	campaignName: {
                    required: "Please enter a campaign name"
                },
                textContact : {
                	required: "Please paste contacts"
                },
                uploadContact : {
                	required: "Please upload a csv file"
                },
                selectContact : {
                	required: "Please select a contact"
                },
                contactName : {
                	required: "Please enter a contact name"
                },
                text : {
                	required: "Please enter text"
                },
                smsName : {
                	required: "Please enter text name"
                },
                selectSenderId: {
                	required: "Please select a sender-id"
                }
            }
        });
    	
    	
    	//Transfer form
    	$("#TransferForm").validate({
    		 rules: {
                 username: {
                     required: true
                 },
                 amount: {
                     required: true,
                     digits: true
                 },
                 account: {
                     required: true
                 },
                 mode: {
                     required: true
                 }
             },
           messages: {
                 username: {
                     required: "Please enter a username"
                 },
                 amount: {
                 	required: "Please enter an amount"
                 },
                 account: {
                     required: ""
                 },
                 mode: {
                 	required: ""
                 }
             }
        });
    	//Chanage Password form
    	$("#ChanagePasswordForm").validate({
    		 rules: {
    			 oldpassword: {
                     required: true
                 },
                 password: {
                     required: true,
                     minlength: 5
                 },
                 repassword: {
                     required: true,
                     minlength: 5,
                     equalTo: "#password"
                 }
             },
           messages: {
        	   oldpassword: {
                     required: "Please enter current password"
                 },
                 password: {
                 	required: "Please provide a password"
                 },
                 repassword: {
                     required: "Please confirm password"
                 }
             }
        });
    	//RolePermissionForm form
    	$("#RolePermissionForm").validate({
    		 rules: {
    			 selectRole: {
                     required: "#roleType-1:checked"
                 },
                 roleName: {
                     required: "#roleType-2:checked"
                 },
                 my_multi_select3: {
                     required: true,
                     needsSelection: true
                 }
             },
           messages: {
        	   selectRole: {
                     required: "Please select a role"
                 },
                 roleName: {
                 	required: "Please enter a role name"
                 },
                 my_multi_select3: {
                     required: "Please select resources"
                 }
             }
        });
    	
    	//Search Report form
    	$("#searchReport").validate({
    		 rules: {
    			 to: {
    				 required: true
                 },
                 from: {
                	 required: true
                 }
             },
           messages: {
        	   to: {
                     required: ""
                 },
               from: {
                 	required: ""
                 }
             }
        });
    	
    });


}();
