
//allow only numeric and dots. 
// back % one dot is allowed. 
// after dot only 2 digits allowed. 
$("#price").add('#tax').keydown(function(event) 
{
	//alert(event.keyCode);
	if (event.shiftKey == true) 
	{
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57) || 
        (event.keyCode >= 96 && event.keyCode <= 105) || 
        event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
        event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 ||
        event.keyCode == 110 || event.keyCode == 35 || event.keyCode == 36) {

    } else {
        event.preventDefault();
    }

    if($(this).val().indexOf('.') !== -1)
    {
        if(event.keyCode == 190 || event.keyCode == 110)
        {
        	event.preventDefault(); 
        }
        else
        {
            $arr = $(this).val().split('.');
            if($arr[1].length >= 2)
            {
            	if (event.keyCode == 8 || event.keyCode == 46 || 
                        event.keyCode == 36 || event.keyCode == 35 || 
                        event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 9) {

                    } else {
                        event.preventDefault();
                    }
            }
        }
    }
});


$("#rate").add('#tax').add('#amount').keyup(function() 
{
	calculate();
});

function calculate()
{
	var volume = parseInt($('#amount').val());
	if($('#amount').val() == '' || $('#amount').val() == null)
	{
		volume = parseInt("0");
	}
	var pricing =  parseFloat($('#rate').val());
	if($('#rate').val() == '' || $('#rate').val() == null)
	{
		pricing = parseFloat("0.00");
	}
	var tax = parseFloat($('#tax').val());
	if($('#tax').val() == '' || $('#tax').val() == null)
	{
		tax = parseFloat("0.00");
	}
	var rs = parseFloat((volume * pricing) / 100);
	var rstax = parseFloat(rs * (tax / 100));
	var total = parseFloat(rs + rstax);		
	$('#total').val(total.toFixed(2));
}



$(function(){
	var currentTextVal = $("#text").val();
    $("#selectData").on("change", function(){
    	currentTextVal = $("#text").val();
        $("#selectData option:selected").each(function(){
        	if (this.value != 'SELECT COLUMN'){
        		$("#text").val( currentTextVal+"["+$(this).text()+"]"); 
        	}
        });  
    });        
});


$(document).ready(function () {
    $("#SmsComposeForm").submit(function (e) {
        //stop submitting the form to see the disabled button effect
        //e.preventDefault();
        //disable the submit button
        $("#send").attr("disabled", true);
        
        return true;
    });
    
});

$(document).ready(function () {
    $("#VoiceComposeForm").submit(function (e) {
        //stop submitting the form to see the disabled button effect
        //e.preventDefault();
        //disable the submit button
        $("#vsend").attr("disabled", true);
        
        return true;
    });
    
});

$(document).ready(function () {
    $("#TransferForm").submit(function (e) {
        //stop submitting the form to see the disabled button effect
        //e.preventDefault();
        //disable the submit button
        $("#transfer").attr("disabled", true);
        
        return true;
    });
    
});

/*
$(document).ready(function () {   
	$('input#send').on('click', function () {   
		var mySForm = $("form#SmsComposeForm");   
		if (mySForm) {   
			if ($('#SmsComposeForm').valid()) { 
				$(this).prop('disabled', 'true'); 
	        	$('#SmsComposeForm').submit();
	        } else {
	        	//$(this).prop('disabled', false);
	        }
			return false;
		}   
		});   
}); 

$(document).ready(function () {   
	$('input#vsend').on('click', function () {   
		var mySForm = $("form#VoiceComposeForm");   
		if (mySForm) {   
			if ($('#VoiceComposeForm').valid()) { 
				$(this).prop('disabled', 'disabled'); 
	        	$('#VoiceComposeForm').submit();
	        } else {
	        	$(this).prop('disabled', false);
	        }
			return false;
		}   
		});   
}); 
$(document).ready(function () {   
	$('input#transfer').on('click', function () {   
		var myTForm = $("form#TransferForm");   
		if (myTForm) {   
			if ($('#TransferForm').valid()) { 
				$(this).prop('disabled', 'disabled'); 
	        	$('#TransferForm').submit();
	        } else {
	        	$(this).prop('disabled', false);
	        }
			return false;
		}   
		});   
});
*/
$("input[name='allgroups[]']").click(function()
		{
			var check_values = new Array();
			$.each($("input[name='allgroups[]']:checked"), function()
			{
				check_values.push($(this).val());
			});
			if(check_values.length > 0){
		        for(var i=0;i<check_values.length;i++){
		        	if(i == 0){
		        		$lists = check_values[i];
		        	}else{
		        		$lists = $lists+','+check_values[i];
		        	}
		        }
		        $('#selectContact').val($lists);
			}else{
				$('#selectContact').val('');
			}
			
		});



/*
function sanitize(s) {
	  return s.replace(/[\u00cb\u202d\u202c\u00ad\u00A0\u1680​\u180e\u2000-\u2009\u200a​\u200b​\u202f\u205f​\u3000]/g,''); 
	};

	$(function() {
	 $(":text, textarea").bind("input paste", function(e) {
	   try {
	     clipboardData.setData("text",
	       sanitize(clipboardData.getData("text"))
	     );
	   } catch (e) {
	     $(this).val( sanitize( $(this).val() ) );
	   }
	 });
	});
*/
$(document).ready(function(){
	
	$(document).on('click', '#getSmppSummary', function(e){
		
		e.preventDefault();
		
		var uid = $(this).data('id');   // it will get id of clicked row
		var smpp = $("#smpp").val();
		var to = $("#to").val();
		var from = $("#from").val();

		$('#dynamic-content').html(''); // leave it blank before ajax call
		$('#modal-loader').show();      // load ajax loader
		console.log(uid);
		$.ajax({
			url: '/smpp/esme/summary/',
			type: 'GET',
			data: 'smpp='+smpp+'&to='+to+'&from='+from,
			dataType: 'html'
		})
		.done(function(data){
				
			$('#dynamic-content').html('');    
			$('#dynamic-content').html(data); // load response 
			$('#modal-loader').hide();		  // hide ajax loader	
		})
		.fail(function(){
			$('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#modal-loader').hide();
		});
		
	});
	
});


//Url Summary
$(document).ready(function(){
	
	$(document).on('click', '#getUrlSummary', function(e){
		
		e.preventDefault();
		
		var uid = $(this).data('id');   // it will get id of clicked row
		
		$('#dynamic-content').html(''); // leave it blank before ajax call
		$('#modal-loader').show();      // load ajax loader
		console.log(uid);
		$.ajax({
			url: '/url/report/summary/',
			type: 'GET',
			data: 'p='+uid,
			dataType: 'html'
		})
		.done(function(data){
				
			$('#dynamic-content').html('');    
			$('#dynamic-content').html(data); // load response 
			$('#modal-loader').hide();		  // hide ajax loader	
		})
		.fail(function(){
			$('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#modal-loader').hide();
		});
		
	});
	
});

//Summary Report 
$(document).ready(function(){
	
	$(document).on('click', '#getUser', function(e){
		
		e.preventDefault();
		
		var uid = $(this).data('id');   // it will get id of clicked row
		
		$('#dynamic-content').html(''); // leave it blank before ajax call
		$('#modal-loader').show();      // load ajax loader
		
		$.ajax({
			url: '/sms/report/summary/',
			type: 'GET',
			data: 'p='+uid,
			dataType: 'html'
		})
		.done(function(data){
			console.log(data);	
			$('#dynamic-content').html('');    
			$('#dynamic-content').html(data); // load response 
			$('#modal-loader').hide();		  // hide ajax loader	
		})
		.fail(function(){
			$('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#modal-loader').hide();
		});
		
	});
	
});

//Summary Voice Report 
$(document).ready(function(){
	
	$(document).on('click', '#getVoiceUser', function(e){
		
		e.preventDefault();
		
		var uid = $(this).data('id');   // it will get id of clicked row
		
		$('#dynamic-content').html(''); // leave it blank before ajax call
		$('#modal-loader').show();      // load ajax loader
		
		$.ajax({
			url: '/voice/report/summary/',
			type: 'GET',
			data: 'p='+uid,
			dataType: 'html'
		})
		.done(function(data){
			console.log(data);	
			$('#dynamic-content').html('');    
			$('#dynamic-content').html(data); // load response 
			$('#modal-loader').hide();		  // hide ajax loader	
		})
		.fail(function(){
			$('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#modal-loader').hide();
		});
		
	});
	
});

//Contact Views
$(document).ready(function(){
	
	$(document).on('click', '#getContact', function(e){
		
		e.preventDefault();
		
		var uid = $(this).data('id');   // it will get id of clicked row
		
		$('#dynamic-content').html(''); // leave it blank before ajax call
		$('#modal-loader').show();      // load ajax loader
		
		$.ajax({
			url: '/user/contact/view/',
			type: 'GET',
			data: 'id='+uid,
			dataType: 'html'
		})
		.done(function(data){
			console.log(data);	
			$('#dynamic-content').html('');    
			$('#dynamic-content').html(data); // load response 
			$('#modal-loader').hide();		  // hide ajax loader	
		})
		.fail(function(){
			$('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#modal-loader').hide();
		});
		
	});
	
});
/*
//Autocomplete Transaction
		$(function() {	
			$( "#user" ).autocomplete({
				source: "/user/transaction/autosuggest/",
				messages: {
			        noResults: '',
			        results: function() {}
			    }
					
			});
		});
		
		//Autocomplete Transaction
		$(function() {	
			$( "#reseller" ).autocomplete({
				source: "/user/transaction/mainsuggest/",
				messages: {
			        noResults: '',
			        results: function() {}
			    }
					
			});
		});		
*/		
$(function(){
    $("#type").on("change", function(){
    	$('#smpp').html('');  
    	var types =$('#type').val();
    	$.ajax({
			url: '/smpp/transaction/autosuggest/',
			type: 'GET',
			data: 'type='+types,
			dataType: 'json'
		})
		.done(function(data){
			console.log(data);	
			$('#smpp').html('');    
			 $.each(data, function(value, key) {
				 $('#smpp').append($("<option></option>")
                         .attr("value", key).text(key));
             });	 	
		})
    });        
});

$(function(){
    $("#retype").on("change", function(){
    	$('#esme').html('');  
    	var types =$('#retype').val();
    	$.ajax({
			url: '/smpp/route/autosuggest/',
			type: 'GET',
			data: 'type='+types,
			dataType: 'json'
		})
		.done(function(data){
			//console.log(data);	
			$('#esme').html('');    
			 $.each(data, function(value, key) {
				 $('#esme').append($("<option></option>")
                         .attr("value", key).text(key));
             });	 	
		})
    });        
});


$(function(){
    $("#retype").on("change", function(){
    	$('#route').html('');  
        $('#reroute').html('');
    	var types =$('#retype').val();
    	$.ajax({
			url: '/smpp/route/autosuggestsmsc/',
			type: 'GET',
			data: 'type='+types,
			dataType: 'json'
		})
		.done(function(data){
			console.log(data);	
			$('#route').html(''); 
                        $('#reroute').html(''); 
			 $.each(data, function(value, key) {
				 $('#route').append($("<option></option>")
                         .attr("value", value).text(key));
                        $('#reroute').append($("<option></option>")
                         .attr("value", value).text(key));
                 
             });	 	
		})
    });        
});




$(function(){
    $("#rtype").on("change", function(){
    	$('#route').html('');  
    	var types =$('#rtype').val();
    	$.ajax({
			url: '/sms/route/autosuggest/',
			type: 'GET',
			data: 'rtype='+types,
			dataType: 'json'
		})
		.done(function(data){
			console.log(data);	
			$('#route').html('');    
			 $.each(data, function(value, key) {
				 $('#route').append($("<option></option>")
                         .attr("value", value).text(key));
             });	 	
		})
    });        
});
/*//Autocomplete Transaction
		$(function() {	
			$( "#smpp" ).autocomplete({
				source: function(request, response) {
				    $.getJSON("/smpp/transaction/autosuggest/?type="+$('#type').val()+"&smpp="+$('#smpp').val(), 
				              response);
				  },
				messages: {
			        noResults: '',
			        results: function() {}
			    }
			});
		});
	*/	
		//Autocomplete Transaction
		$(function() {	
			$( "#centUser" ).autocomplete({
				source: "/user/percentage/autouser/",
				messages: {
			        noResults: '',
			        results: function() {}
			    }
			});
		});
		
		//Autocomplete Transaction
		$(function() {	
			$( "#centDomain" ).autocomplete({
				source: "/user/percentage/autodomain/",
				messages: {
			        noResults: '',
			        results: function() {}
			    }
			});
		});
        //Show hide contacts type in compose 
        $(document).ready(function() {
        	
        	$("#translControl").hide("scale", 1); 
        	//alert("xdvf");
        	//Default Upload Image
        	$("#wupload").hide("scale", 1); 
            $("#wtext").show("scale", 1);
			        	
			//Default Select Contacts
            $("#select").hide("scale", 1); 
            $("#cname").hide("scale", 1);
            $("#paste").show("scale", 1); 
            $("#upload").hide("scale", 1);
            $("#save").show("scale", 1);

            //Default Audio
            $("#aselect").show("scale", 1);
            $("#bname").hide("scale", 1);
            $("#vselect").hide("scale", 1); 
            $("#aname").hide("scale", 1);
            $("#apaste").hide("scale", 1); 
            $("#aupload").hide("scale", 1);
            $("#survey").hide("scale", 1);

            //Default Role 
            $("#names").show("scale", 1);

            //Default saveSMS
            $("#tname").hide("scale", 1);

            $('#msgType-1').click(function(){

            	$("#wupload").hide("scale", 1); 
                $("#wtext").show("scale", 1);

            });
            
            $('#msgType-2').click(function(){

            	$("#wupload").show("scale", 1); 
                $("#wtext").hide("scale", 1);

            });
            //Audio

            $('#audioType-1').click(function(){

                $("#aselect").show("scale", 1); 
                $("#vselect").hide("scale", 1); 
                $("#aname").hide("scale", 1);
                $("#apaste").hide("scale", 1); 
                $("#aupload").hide("scale", 1);
                $("#bname").hide("scale", 1);

            });

            $('#audioType-2').click(function(){

                $("#aselect").hide("scale", 1); 
                $("#vselect").hide("scale", 1); 
                $("#aname").show("scale", 1);
                $("#apaste").hide("scale", 1); 
                $("#aupload").show("scale", 1);

            });

            $('#audioType-3').click(function(){

                $("#aselect").hide("scale", 1); 
                $("#vselect").show("scale", 1); 
                $("#aname").show("scale", 1);
                $("#apaste").show("scale", 1); 
                $("#aupload").hide("scale", 1);

            });
            
            $('#saveAudio-1').click(function(){

                $("#bname").show("scale", 1); 

            });

            $('#saveAudio-0').click(function(){

                $("#bname").hide("scale", 1); 

            });
            
            $('#saveSms-1').click(function(){

                $("#tname").show("scale", 1); 

            });

            $('#saveSms-0').click(function(){

                $("#tname").hide("scale", 1); 

            });
            
            //Contacts
            
            $('#saveContact-1').click(function(){

                $("#cname").show("scale", 1); 

            });

            $('#saveContact-0').click(function(){

                $("#cname").hide("scale", 1); 

            });
            
          //attach click event to buttons
            $('#coding-1').click(function(){
                $("#translControl").hide("scale", 1); 
            });

            $('#coding-2').click(function(){
            	$("#translControl").show("scale", 1); 
            });
            
            $('#coding-3').click(function(){
            	$("#translControl").hide("scale", 1); 
            });
            
            //attach click event to buttons
             $('#roleType-1').click(function(){

                $("#names").show("scale", 1); 
                $("#select").hide("scale", 1);

            });

            $('#roleType-2').click(function(){

            	$("#select").show("scale", 1); 
                $("#names").hide("scale", 1);

            });

            $('#urlType-1').click(function(){

                $("#select").show("scale", 1); 
                $("#cname").hide("scale", 1);
                $("#paste").show("scale", 1); 
                $("#upload").hide("scale", 1);
                $("#save").hide("scale", 1);
                $("#survey").hide("scale", 1); 

            });

            $('#urlType-2').click(function(){

            	$("#select").hide("scale", 1); 
                $("#cname").hide("scale", 1);
                $("#paste").hide("scale", 1); 
                $("#upload").show("scale", 1);
                $("#save").show("scale", 1);
                $("#survey").hide("scale", 1);

            });
            
            $('#urlType-3').click(function(){

                $("#select").hide("scale", 1); 
                $("#cname").hide("scale", 1);
                $("#paste").hide("scale", 1); 
                $("#upload").hide("scale", 1);
                $("#save").hide("scale", 1);
                $("#survey").show("scale", 1);

            });

            $('#contactType-1').click(function(){

                $("#select").show("scale", 1); 
                $("#cname").hide("scale", 1);
                $("#paste").hide("scale", 1); 
                $("#upload").hide("scale", 1);
                $("#save").hide("scale", 1);

            });

            $('#contactType-2').click(function(){

            	$("#select").hide("scale", 1); 
                $("#cname").hide("scale", 1);
                $("#paste").hide("scale", 1); 
                $("#upload").show("scale", 1);
                $("#save").show("scale", 1);

            });

            $('#contactType-3').click(function(){

            	$("#select").hide("scale", 1); 
                $("#cname").hide("scale", 1);
                $("#paste").show("scale", 1); 
                $("#upload").hide("scale", 1);
                $("#save").show("scale", 1); 

            });
            
            $('#roleType-1').click(function(){

            	$("#selectrole").show("scale", 1); 
                $("#rolename").hide("scale", 1);

            });
            $('#roleType-2').click(function(){

            	$("#selectrole").hide("scale", 1); 
                $("#rolename").show("scale", 1);

            });
        });
        
        
//SMS Count
        function substringcount(string,sub)
		{
			var sstr = sub;
			var c = 0;
			var str = string;			
			for(var i=0; i<str.length; i++)
			{
				if(sstr==str.substr(i,sstr.length))
				{
			       c++;
			    }
			}
			return c;
		}
		
		$('#text').keyup(function() 
		{
			var message = $('#text').val();
			var type = $('#smstype').val();
			var restype = type.split('|');
			type = restype['0'];
			//console.log(type.match(/INTERNATIONAL/g));
			if (type.match(/INTERNATIONAL/g)){
				var unicodeFirst = 60;
				var unicodeMulti = 67;
				
				var normalFirst = 150;
				var normalMulti = 153;
			}else{
				var unicodeFirst = 70;
				var unicodeMulti = 67;
				
				var normalFirst = 160;
				var normalMulti = 153;
			}
			
			if ("message".match(/[\u0900-\u097F\u0980-\u09FF\u0A80-\u0AFF\u0B00-\u0B7F\u0B80-\u0BFF\u0C00-\u0C7F\u0C80-\u0CFF\u0D00-\u0D7F]+/g)) {
				var msgleg = message.length;
				
				var totchar = msgleg;			
				$('#chr_cnt').html('<strong>'+totchar+'</strong>');
				if(totchar == 0)
				{
					$('#sms_cnt').html('<strong>0</strong>');
				}
				else if(totchar > 0 && totchar <= unicodeFirst)
				{
					$('#sms_cnt').html('<strong>1</strong>');
				}
				else
				{				
					var smscount = Math.ceil(totchar/unicodeMulti);
					$('#sms_cnt').html('<strong>'+smscount+'</strong>');
				}
				
			}else if (message.match(/[\u0900-\u097F\u0980-\u09FF\u0A80-\u0AFF\u0B00-\u0B7F\u0B80-\u0BFF\u0C00-\u0C7F\u0C80-\u0CFF\u0D00-\u0D7F]+/g) == null){
				
				var msgleg = message.length;
				var spechar = ['^' , '|' , '{' , '}' , '[' , ']' , '~' , '\\'];
				var speccount = 0;
				for(var i=0; i < spechar.length; i++)
				{
					var cnt = substringcount(message , spechar[i]);
					speccount = speccount + cnt;
				}			
				var totchar = msgleg + speccount;			
				$('#chr_cnt').html('<strong>'+totchar+'</strong>');
				if(totchar == 0)
				{
					$('#sms_cnt').html('<strong>0</strong>');
				}
				else if(totchar > 0 && totchar <= normalFirst)
				{
					$('#sms_cnt').html('<strong>1</strong>');
				}
				else
				{				
					var smscount = Math.ceil(totchar/normalMulti);
					$('#sms_cnt').html('<strong>'+smscount+'</strong>');
				}
			}
						
		});

		$('#text').keydown(function() 
		{
				
			var message = $('#text').val();
			var type = $('#smstype').val();
			var restype = type.split('|');
			type = restype['0'];
			//console.log(type.match(/INTERNATIONAL/g));
			if (type.match(/INTERNATIONAL/g)){
				var unicodeFirst = 60;
				var unicodeMulti = 67;
				
				var normalFirst = 150;
				var normalMulti = 153;
			}else{
				var unicodeFirst = 70;
				var unicodeMulti = 67;
				
				var normalFirst = 160;
				var normalMulti = 153;
			}
			
			if (message.match(/[\u0900-\u097F\u0980-\u09FF\u0A80-\u0AFF\u0B00-\u0B7F\u0B80-\u0BFF\u0C00-\u0C7F\u0C80-\u0CFF\u0D00-\u0D7F]+/g)) {
				var msgleg = message.length;
				
				var totchar = msgleg;		
				$('#chr_cnt').html('<strong>'+totchar+'</strong>');
				if(totchar == 0)
				{
					$('#sms_cnt').html('<strong>0</strong>');
				}
				else if(totchar > 0 && totchar <= unicodeFirst)
				{
					$('#sms_cnt').html('<strong>1</strong>');
				}
				else
				{				
					var smscount = Math.ceil(totchar/unicodeMulti);
					$('#sms_cnt').html('<strong>'+smscount+'</strong>');
				}
			}else if (message.match(/[\u0900-\u097F\u0980-\u09FF\u0A80-\u0AFF\u0B00-\u0B7F\u0B80-\u0BFF\u0C00-\u0C7F\u0C80-\u0CFF\u0D00-\u0D7F]+/g) == null){

				var msgleg = message.length;
				var spechar = ['^' , '|' , '{' , '}' , '[' , ']' , '~' , '\\'];
				var speccount = 0;
				for(var i=0; i < spechar.length; i++)
				{
					var cnt = substringcount(message , spechar[i]);
					speccount = speccount + cnt;
				}			
				var totchar = msgleg + speccount;			
				$('#chr_cnt').html('<strong>'+totchar+'</strong>');
				if(totchar == 0)
				{
					$('#sms_cnt').html('<strong>0</strong>');
				}
				else if(totchar > 0 && totchar <= normalFirst)
				{
					$('#sms_cnt').html('<strong>1</strong>');
				}
				else
				{				
					var smscount = Math.ceil(totchar/normalMulti);
					$('#sms_cnt').html('<strong>'+smscount+'</strong>');
				}
			}
		});
//Contacts count
	
		function countLines(theArea){
			var theLines = theArea.value.replace((new RegExp(".{"+theArea.cols+"}","g")),"\n").split("\n");
			if(theLines[theLines.length-1]=="") theLines.length--;
			//theArea.form.lineCount.value = theLines.length;
			$('#cont_cnt').html('<strong>'+theLines.length+'</strong>');
			}

		// Load the Google Transliteration API
	      google.load("elements", "1", {
	            packages: "transliteration"
	          });

	      function onLoaddisable() {
	          var options = {
	            sourceLanguage: 'en',
	            destinationLanguage: ['hi','kn','ml','ta','te','mr','gu','bn','pa','ar','or'],
	            shortcutKey: 'ctrl+g',
	            transliterationEnabled: false
	          };
	          // Create an instance on TransliterationControl with the required
	          // options.
	          var control =
	              new google.elements.transliteration.TransliterationControl(options);

	          // Enable transliteration in the textfields with the given ids.
	          var ids = [ "text"];
	          //control.disableTransliteration(ids);
	        }
	        function onLoad() {
	            var options = {
	              sourceLanguage: 'en',
	              destinationLanguage: ['hi','kn','ml','ta','te','mr','gu','bn','pa','ar','or'],
	              shortcutKey: 'ctrl+g',
	              transliterationEnabled: false
	            };

	            // Create an instance on TransliterationControl with the required
	            // options.
	            var control =
	                new google.elements.transliteration.TransliterationControl(options);

	            // Enable transliteration in the textfields with the given ids.
	            var ids = [ "text"];
	            control.makeTransliteratable(ids);

	            // Show the transliteration control which can be used to toggle between
	            // English and Hindi and also choose other destination language.
	            control.showControl('translControl');
	          }
	        google.setOnLoadCallback(onLoad);
	     