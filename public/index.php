<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
date_default_timezone_set('Asia/Calcutta');

// Define path to application directory
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));


/** Zend_Application */
require_once 'Zend/Application.php';
// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

/** Routing Info **/
$FrontController = Zend_Controller_Front::getInstance();
$Router = $FrontController->getRouter();

$Router->addRoute("dlrView",
		new Zend_Controller_Router_Route(
				"system/dlrstatus/view/:p",
				array("module" => "system",
						"controller" => "dlrstatus",
						"action" => "view",
						"p" => '1'
				)));

$Router->addRoute("creditoutView",
		new Zend_Controller_Router_Route(
				"user/transaction/out/:i",
				array("module" => "user",
						"controller" => "transaction",
						"action" => "out",
						"i" => '1'
				)));



$Router->addRoute("creditinView",
		new Zend_Controller_Router_Route(
				"user/transaction/in/:i",
				array("module" => "user",
						"controller" => "transaction",
						"action" => "in",
						"i" => '1'
				)));



$Router->addRoute("creditsmppoutView",
		new Zend_Controller_Router_Route(
				"smpp/transaction/out/:i",
				array("module" => "smpp",
						"controller" => "transaction",
						"action" => "out",
						"i" => '1'
				)));



$Router->addRoute("creditsmppinView",
		new Zend_Controller_Router_Route(
				"smpp/transaction/in/:i",
				array("module" => "smpp",
						"controller" => "transaction",
						"action" => "in",
						"i" => '1'
				)));



$Router->addRoute("recreditView",
		new Zend_Controller_Router_Route(
				"user/recredit/:i",
				array("module" => "user",
						"controller" => "recredit",
						"action" => "index",
						"i" => '1'
				)));



$Router->addRoute("recreditViewall",
		new Zend_Controller_Router_Route(
				"user/recredit/viewall/:i",
				array("module" => "user",
						"controller" => "recredit",
						"action" => "viewall",
						"i" => '1'
				)));



$Router->addRoute("licenseView",
		new Zend_Controller_Router_Route(
				"system/license/view/:p",
				array("module" => "system",
						"controller" => "license",
						"action" => "view",
						"p" => '1'
				)));



$Router->addRoute("licenseViewAll",
		new Zend_Controller_Router_Route(
				"system/license/viewall/:p",
				array("module" => "system",
						"controller" => "license",
						"action" => "viewall",
						"p" => '1'
				)));



$Router->addRoute("domainlist",
		new Zend_Controller_Router_Route(
				"user/manage/domains/:i",
				array("module" => "user",
						"controller" => "manage",
						"action" => "domains",
						"i" => '1'
				)));



$Router->addRoute("userlist",
		new Zend_Controller_Router_Route(
				"user/manage/users/:i",
				array("module" => "user",
						"controller" => "manage",
						"action" => "users",
						"i" => '1'
				)));



$Router->addRoute("creditlog",
		new Zend_Controller_Router_Route(
				"log/logger/credit/:i",
				array("module" => "log",
						"controller" => "logger",
						"action" => "credit",
						"i" => '1'
				)));



$Router->addRoute("activation",
		new Zend_Controller_Router_Route(
				"account/activate/:token",
				array("module" => "default",
						"controller" => "account",
						"action" => "activate",
						"token" => '0'
				)));



$Router->addRoute("reset",
		new Zend_Controller_Router_Route(
				"account/reset/:token",
				array("module" => "default",
						"controller" => "account",
						"action" => "reset",
						"token" => '0'
				)));



$Router->addRoute("alldomainlist",
		new Zend_Controller_Router_Route(
				"user/manage/alldomains/:i",
				array("module" => "user",
						"controller" => "manage",
						"action" => "domains",
						"i" => '1'
				)));



$Router->addRoute("alluserlist",
		new Zend_Controller_Router_Route(
				"user/manage/clients/:i",
				array("module" => "user",
						"controller" => "manage",
						"action" => "clients",
						"i" => '1'
				)));



$Router->addRoute("startImpersonate",
		new Zend_Controller_Router_Route(
				"/user/manage/start-impersonate/:id",
				array("module" => "user",
						"controller" => "manage",
						"action" => "start-impersonate",
						"id" => ''
				)));



$Router->addRoute("manageEsmes",
		new Zend_Controller_Router_Route(
				"/smpp/esme/esmes/:i",
				array("module" => "smpp",
						"controller" => "esme",
						"action" => "esmes",
						"i" => ''
				)));



$Router->addRoute("sentSmsEsme",
		new Zend_Controller_Router_Route(
				"/smpp/esme/sentsms/:i",
				array("module" => "smpp",
						"controller" => "esme",
						"action" => "sentsms",
						"i" => ''
				)));

$Router->addRoute("smsReportdownpromocsv",
		new Zend_Controller_Router_Route(
				"/sms/report/downcsv/:file",
				array("module" => "sms",
						"controller" => "report",
						"action" => "downcsv",
						"file" => ''
				)));

$Router->addRoute("smsReportPagination",
		new Zend_Controller_Router_Route(
				"/sms/report/index/:p",
				array("module" => "sms",
						"controller" => "report",
						"action" => "index",
						"p" => '1'
				)));

$Router->addRoute("smsTemplateViewPagination",
		new Zend_Controller_Router_Route(
				"/sms/template/view/:p",
				array("module" => "sms",
						"controller" => "template",
						"action" => "view",
						"p" => '1'
				)));

$Router->addRoute("smsTemplateViewAllPagination",
		new Zend_Controller_Router_Route(
				"/sms/template/viewall/:p",
				array("module" => "sms",
						"controller" => "template",
						"action" => "viewall",
						"p" => '1'
				)));

$Router->addRoute("smsTemplateApprovalPagination",
		new Zend_Controller_Router_Route(
				"/sms/template/approval/:p",
				array("module" => "sms",
						"controller" => "template",
						"action" => "approval",
						"p" => '1'
				)));

$Router->addRoute("smsSenderIdViewPagination",
		new Zend_Controller_Router_Route(
				"/sms/senderid/view/:p",
				array("module" => "sms",
						"controller" => "senderid",
						"action" => "view",
						"p" => '1'
				)));

$Router->addRoute("smsSenderIdViewAllPagination",
		new Zend_Controller_Router_Route(
				"/sms/senderid/viewall/:p",
				array("module" => "sms",
						"controller" => "senderid",
						"action" => "viewall",
						"p" => '1'
				)));

$Router->addRoute("smsSenderIdApprovalPagination",
		new Zend_Controller_Router_Route(
				"/sms/senderid/approval/:p",
				array("module" => "sms",
						"controller" => "senderid",
						"action" => "approval",
						"p" => '1'
				)));


$Router->addRoute("smppTemplateViewPagination",
		new Zend_Controller_Router_Route(
				"/smpp/template/view/:p",
				array("module" => "smpp",
						"controller" => "template",
						"action" => "view",
						"p" => '1'
				)));

$Router->addRoute("smppTemplateViewAllPagination",
		new Zend_Controller_Router_Route(
				"/smpp/template/viewall/:p",
				array("module" => "smpp",
						"controller" => "template",
						"action" => "viewall",
						"p" => '1'
				)));

$Router->addRoute("smppTemplateApprovalPagination",
		new Zend_Controller_Router_Route(
				"/smpp/template/approval/:p",
				array("module" => "smpp",
						"controller" => "template",
						"action" => "approval",
						"p" => '1'
				)));

$Router->addRoute("smppSenderIdViewPagination",
		new Zend_Controller_Router_Route(
				"/smpp/senderid/view/:p",
				array("module" => "smpp",
						"controller" => "senderid",
						"action" => "view",
						"p" => '1'
				)));

$Router->addRoute("smppSenderIdViewAllPagination",
		new Zend_Controller_Router_Route(
				"/smpp/senderid/viewall/:p",
				array("module" => "smpp",
						"controller" => "senderid",
						"action" => "viewall",
						"p" => '1'
				)));

$Router->addRoute("smppSenderIdApprovalPagination",
		new Zend_Controller_Router_Route(
				"/smpp/senderid/approval/:p",
				array("module" => "smpp",
						"controller" => "senderid",
						"action" => "approval",
						"p" => '1'
				)));



$Router->addRoute("signErr",
		new Zend_Controller_Router_Route(
				"/account/signin/:p",
				array("module" => "default",
						"controller" => "account",
						"action" => "signin",
						"p" => null
				)));

$Router->addRoute("signupErr",
		new Zend_Controller_Router_Route(
				"/account/signup/:p",
				array("module" => "default",
						"controller" => "account",
						"action" => "signup",
						"p" => null
				)));


$Router->addRoute("logoutErr",
		new Zend_Controller_Router_Route(
				"/account/signout/:p",
				array("module" => "default",
						"controller" => "account",
						"action" => "signout",
						"p" => null
				)));

$Router->addRoute("summarySMS",
		new Zend_Controller_Router_Route(
				"/sms/report/summary/:p",
				array("module" => "sms",
						"controller" => "report",
						"action" => "summary",
						"p" => null
				)));



$Router->addRoute("contactView",
		new Zend_Controller_Router_Route(
				"/user/contact/view/:id",
				array("module" => "user",
						"controller" => "contact",
						"action" => "view",
						"id" => null
				)));

$Router->addRoute("summaryURL",
		new Zend_Controller_Router_Route(
				"/url/report/summary/:p",
				array("module" => "url",
						"controller" => "report",
						"action" => "summary",
						"p" => null
				)));

$application->bootstrap()
            ->run();
