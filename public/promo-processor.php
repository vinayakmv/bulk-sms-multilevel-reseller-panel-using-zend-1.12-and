<?php
require_once '/var/www/html/webpanel/plugin/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");
if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
        $headers = '';
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}


$original_headers = getallheaders();

$headers = array();
foreach($original_headers as $key => $value) {
  $key = strtolower($key);
  $headers[$key] = urldecode($value);
}

$sender = $headers['x-kannel-plugin-msg-sms-sender'];
$receiver = $headers['x-kannel-plugin-msg-sms-receiver'];
$smsType = $headers['x-kannel-plugin-msg-type'];
$msgType = $headers['x-kannel-plugin-msg-sms-sms_type'];
$dlrUrl = $headers['x-kannel-plugin-msg-sms-dlr_url'];

if ($msgType == '3'){
    $smscId = $headers['x-kannel-plugin-msg-smsc-id'];
    $dlrData = $headers['x-kannel-plugin-msg-sms-msgdata'];
    
     $smppDb->where('dlr_url', $dlrUrl);
     $smppDb->update('smpp_log_mt_promo', ['opcodata' => $dlrData]);
            
    $reg = '/err:(\w+)/';
    if (preg_match($reg, $dlrData, $dlr) ){
        $opcoCode = $dlr['1'];
        $opcoErr = $dlr['0'];
        
        $selectDlr = "SELECT * FROM `error_map_promo` WHERE `opco_error_code` = '$opcoCode' AND (smsc_id = 'all' OR smsc_id = '$smscId')";
        $resultDlr = $smppDb->rawQueryOne($selectDlr);
        
        if ($resultDlr){
            $finalErrCode = $resultDlr['error_code_map'];
            $dlrDataOpco = preg_replace("/($opcoCode)/", $finalErrCode, $dlrData);

            header("x-kannel-plugin-msg-sms-msgdata: ".urlencode("$dlrDataOpco"));

            exit(0);
        }else{
                $dlrDataOpco = preg_replace("/($opcoCode)/", '069', $dlrData);
                header("x-kannel-plugin-msg-sms-msgdata: ".urlencode("$dlrDataOpco"));
        }
    }else{
        $reg = '/NACK\/(\w+)/';
        if (preg_match($reg, $dlrData, $dlr) ){
            $opcoCode = $dlr['1'];
            $opcoErr = $dlr['0'];

            $selectDlr = "SELECT * FROM `error_map_promo` WHERE `opco_error_code` = '$opcoCode' AND (smsc_id = 'all' OR smsc_id = '$smscId')";
            $resultDlr = $smppDb->rawQueryOne($selectDlr);
            
            if($resultDlr){
                $finalErrCode = $resultDlr['error_code_map'];
                $dlrDataOpco = preg_replace("/($opcoCode)/", $finalErrCode, $dlrData);

                header("x-kannel-plugin-msg-sms-msgdata: ".urlencode("$dlrDataOpco"));

                exit(0);
            }else{
                $dlrDataOpco = preg_replace("/($opcoCode)/", '0x00000045', $dlrData);
                header("x-kannel-plugin-msg-sms-msgdata: ".urlencode("$dlrDataOpco"));
            }
            
        }else{
            exit(0);
        }
        
    }
}   

