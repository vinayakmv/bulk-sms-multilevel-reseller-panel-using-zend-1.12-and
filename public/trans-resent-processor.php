<?php
require_once '/var/www/html/webpanel/plugin/Db/initDb.php';
date_default_timezone_set("Asia/Kolkata");
if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
        $headers = '';
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}


$original_headers = getallheaders();

$headers = array();
foreach($original_headers as $key => $value) {
  $key = strtolower($key);
  $headers[$key] = urldecode($value);
}

$sender = $headers['x-kannel-plugin-msg-sms-sender'];
$receiver = $headers['x-kannel-plugin-msg-sms-receiver'];
$smsType = $headers['x-kannel-plugin-msg-type'];
$msgType = $headers['x-kannel-plugin-msg-sms-sms_type'];
$dlrUrl = $headers['x-kannel-plugin-msg-sms-dlr_url'];
$service = $headers['x-kannel-plugin-msg-sms-service'];

if ($msgType == '3'){
    $smscId = $headers['x-kannel-plugin-msg-sms-smsc_id'];
    $dlrData = $headers['x-kannel-plugin-msg-sms-msgdata'];
    $foreignId = $headers['x-kannel-plugin-msg-sms-foreign_id'];
    
    $checkResent = $smppDb->rawQueryOne("SELECT * FROM `smpp_smsc_retry_trans` WHERE `smsc_id`= '$smscId' AND `service` = '$service' AND `status` = '1'");
    if ($checkResent){
        $smscResent = $checkResent['smsc_id_retry'];
        
        $pos = strpos($dlrData, 'DELIVRD');
        
        if ($pos === false) {
            header("HTTP/1.0 404 Not Found");

            $msgs = $smppDb->rawQueryOne("SELECT * FROM `smpp_log_mt_trans` WHERE `dlr_url` = '$dlrUrl'");
            $msgdata = urldecode($msgs['msgdata']);
            $sender = $msgs['sender'];
            $receiver = $msgs['receiver'];
            $coding = $msgs['coding'];
            
                
            $smppDb->rawQueryOne("UPDATE `smpp_log_mt_trans` SET `resent_smsc_id`= '$smscResent', `reroutedata` = '$dlrData'  WHERE `dlr_url` = '$dlrUrl'");

            $exp = explode("|","$dlrUrl");
            $service = $exp['0'];
            $time = $exp['1'];
            $fid = $exp['2'];


            $smppDb->rawQuery("INSERT INTO `smpp_store_trans` ( `sender`, `receiver`, `udhdata`, `msgdata`, `time`, `smsc_id`, `smsc_number`, `foreign_id`, `service`, `account`, `id`, `sms_type`, `mclass`, `mwi`, `coding`, `compress`, `validity`, `deferred`, `dlr_mask`, `dlr_url`, `pid`, `alt_dcs`, `rpi`, `charset`, `boxc_id`, `binfo`, `msg_left`, `priority`, `resend_try`, `resend_time`, `meta_data`) VALUES  ('$sender', '$receiver', NULL, '$msgdata', '$time', '$smscResent', NULL, NULL, '$service', NULL, '$fid', 2, -1, -1, $coding, 0, -1, -1, 19, '$dlrUrl', 0, 0, -1, NULL, NULL, '1', -1, 3, -1, -1, '?smpp?')");

            exit(0);
        }else {
            exit(0);
        }
        
    }
    
}  