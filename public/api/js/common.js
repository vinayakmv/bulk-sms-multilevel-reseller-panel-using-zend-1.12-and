$(document).ready(function(){

	prettyPrint();
	
	$('.languageSelect ul li a').click(function(event){
		selectedLang = $(this).attr('href');
	
		$.ajax({
			url: '/docs/php/set-lang.php',
			type: "post",
			data: {lang: selectedLang.replace('#', '')}
		}).done(function(data){
			location.reload();
		});
		
		event.preventDefault();
	});
	
	$('ul#mainMenu li.hasSubMenu a').click(function(event){
		if(!$(this).parent('li').parent('ul').hasClass('subMenu')){
			clickedEl = $(this);
		
			$('.subMenu:visible').each(function(){
				if($(this) != clickedEl){
					$(this).slideUp(function(){$(this).parent('li').children('a').removeClass('subActive');});
				}
			});
		
			if(clickedEl.parent().children('ul').is(':visible')) {
				clickedEl.parent().children('ul').slideUp(function(){clickedEl.removeClass('subActive');});
			} else {
				clickedEl.addClass('subActive');
				clickedEl.parent().children('ul').slideDown();
			}
			
			event.preventDefault();
		}
	});
	
	$('.showMenu').click(function(event){
		if($(this).children('a').hasClass('active')){
			$(this).children('a').removeClass('active');
			$('#menuColumn').removeClass('expand');
		} else {
			$(this).children('a').addClass('active');
			$('#menuColumn').addClass('expand');
		}
		
		event.preventDefault();
	});
	
	if($('.subMenu li a').hasClass('active')){
		$('.subMenu li a.active').parent('li').parent('ul').parent('li').children('a').addClass('subActive');
		$('.subMenu li a.active').parent('li').parent('ul').show();
	}
	
	$('.tabbedPickerContainer .btn-group button').click(function(event){
		clickedType = $(this).attr('tab-type');
		sampleType = $(this).parents('.tabbedPickerContainer').attr('sample-type');
		console.log(clickedType);
		console.log(sampleType);
		
		$(this).parent().children('button').each(function(){
			$(this).removeClass('active');
		});
		$(this).addClass('active');
		
		$(this).parents('.tabbedPickerContainer').children('[tabbed-type]').hide(0, function(){
			$(this).parents('.tabbedPickerContainer').find('[tabbed-type="' + clickedType + '"]').show();
		});
		
		$.ajax({
			url: '/docs/php/set-' + sampleType + '-format.php',
			type: "post",
			data: {format: clickedType}
		});
		
		event.preventDefault();
	});
	
	$(document).scroll(function(){
		if($(document).scrollTop() > 60) $('#menuColumn').addClass('pageDown');
		else $('#menuColumn').removeClass('pageDown');
	});
	
});