<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
    	$userAgent = $bootstrap->getResource('useragent');

    	$this->_device = $userAgent;
    }

    public function indexAction()
    {
        
        if ($this->getRequest()->isGet()) {
            
            $id = $this->getRequest()->getParams();
           
       		unset($id['action']);
       		unset($id['module']);
       		unset($id['controller']);

       		$tinyUrl = key($id);
            if (isset($tinyUrl)) {
		$ip = $_SERVER['REMOTE_ADDR'];				
                $report = new Application_Model_DbTable_Report();
                $url = $report->addData($tinyUrl, $ip);

                if ($url['type'] == '1'){
                	header('Location: '.$url['url']);
                }elseif($url['type'] == '2'){
                	header('Location: http://mylnk.info/urlFiles/'.$url['url']);
                }elseif($url['type'] == '3'){
                        $this->view->form = $url['url'];
                        $this->view->campaign = $url['campaign_id'];
                        $this->view->user = $url['user_id'];
                        $this->view->receiver = $url['receiver'];
                }                                            
            } else {
                //$this->_helper->redirector('index');
            }
        }
    }
    
    public function reportAction()
    {
        if ($this->getRequest()->isPost()) {
            
            $id = $this->getRequest()->getParams();
            unset($id['action']);
            unset($id['module']);
            unset($id['controller']);
                
            $report = new Application_Model_DbTable_Survey();
            $url = $report->addReport($id);
        }
    }

    public function getUserAgent($data)
    {    
    	$serializer = Zend_Serializer::factory('PhpSerialize');
    	$unserialized = $serializer->unserialize($data);
    	
    	return $unserialized;
    }


}

